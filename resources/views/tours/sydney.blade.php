@extends('layouts.static')


@section('custom-css')

@stop

@section('content')



    <section class="inner-page content-wrapper homepage-activity">
        <h1 class="hide"></h1>
        <div class="map-wrapper transport-map-wrapper">
            <!-- <div id="map"></div> -->

            <div class="mapIcons-wrapper">

                <div class="location-wrapper">
                    <div id="scroll-box">
                        <div class="location-inner m-t-20">
                            <div class="loc-icon">
                                <svg width="25px" height="25px" viewBox="0 0 25 25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-21.000000, -59.000000)" stroke="#212121">
                                            <g id="Group-4" transform="translate(22.000000, 60.000000)">
                                                <circle id="Oval-2" stroke-width="2" cx="7.66666667" cy="7.66666667" r="7.66666667"></circle>
                                                <path d="M13.0333333,13.0333333 L22.3602359,22.3602359" id="Line" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <div class="location-content">
                                <h4 class="loc-title">Search Region</h4>
                                <div class="m-t-20 black-box">
                                    <p><label class="radio-checkbox label_check" for="checkbox-01"><input type="checkbox" id="checkbox-01" value="1">Africa / Middle East (80)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-02"><input type="checkbox" id="checkbox-02" value="1" checked>Asia / Pacific (164)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-03"><input type="checkbox" id="checkbox-03" value="1">Latin America (109)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-04"><input type="checkbox" id="checkbox-04" value="1">UK / Europe (228)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-05"><input type="checkbox" id="checkbox-05" value="1">USA / Canada (27)</label></p>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="location-inner m-t-20">
                            <div class="loc-icon"></div>
                            <div class="location-content">
                                <h4 class="loc-title">Search Country</h4>
                                <div class="m-t-20 black-box">
                                    <p><label class="radio-checkbox label_check" for="checkbox-06"><input type="checkbox" id="checkbox-06" value="1">Australia (132)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-07"><input type="checkbox" id="checkbox-07" value="1" checked>Bhutan (18)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-08"><input type="checkbox" id="checkbox-08" value="1">Brunei (7)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-09"><input type="checkbox" id="checkbox-09" value="1">Burma (Myanmar) (21)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-10"><input type="checkbox" id="checkbox-10" value="1">Cambodia (30)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-11"><input type="checkbox" id="checkbox-11" value="1">India (54)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-12"><input type="checkbox" id="checkbox-12" value="1">Indonesia (89)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-13"><input type="checkbox" id="checkbox-13" value="1">Japan (109)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-14"><input type="checkbox" id="checkbox-14" value="1">Laos (21)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-15"><input type="checkbox" id="checkbox-15" value="1">Malaysia (56)</label></p>
                                </div>
                                <p class="m-t-20"><a href="#">View More Locations</a></p>
                            </div>
                        </div>
                        <hr/>
                        <div class="location-inner">
                            <div class="loc-icon">
                                <svg width="20px" height="24px" viewBox="0 0 20 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="45---EROA007-V4.1-Generic-(Create-Itinerary)-01" transform="translate(-22.000000, -780.000000)" fill="#212121">
                                            <g id="(Generic)-Icon---PAX-Preferences" transform="translate(22.000000, 780.000000)">
                                                <g id="(eRoam)-Icon---PAX-Preferences">
                                                    <path d="M13.4646063,13.9881818 C13.6438724,14.1654545 13.7459162,14.4109091 13.7459162,14.6645455 C13.7459162,14.9154545 13.6438724,15.1609091 13.4646063,15.3381818 C13.2853401,15.5154545 13.0343675,15.6163636 12.780637,15.6163636 C12.5269065,15.6163636 12.2786918,15.5154545 12.0994257,15.3381818 C11.9174016,15.1609091 11.8153578,14.9154545 11.8153578,14.6645455 C11.8153578,14.4109091 11.9174016,14.1654545 12.0994257,13.9881818 C12.4579579,13.6336364 13.106074,13.6336364 13.4646063,13.9881818 L13.4646063,13.9881818 Z M5.28841544,6.13636364 C5.28841544,5.60918182 5.72030894,5.18181818 6.25369466,5.18181818 L12.8184208,5.18181818 C13.3515307,5.18181818 13.7837,5.60918182 13.7837,6.13636364 C13.7837,6.66354545 13.3515307,7.09090909 12.8184208,7.09090909 L6.25369466,7.09090909 C5.72030894,7.09090909 5.28841544,6.66354545 5.28841544,6.13636364 L5.28841544,6.13636364 Z M5.28841544,10.2272727 C5.28841544,9.70009091 5.72030894,9.27272727 6.25369466,9.27272727 L12.8184208,9.27272727 C13.3515307,9.27272727 13.7837,9.70009091 13.7837,10.2272727 C13.7837,10.7544545 13.3515307,11.1818182 12.8184208,11.1818182 L6.25369466,11.1818182 C5.72030894,11.1818182 5.28841544,10.7544545 5.28841544,10.2272727 L5.28841544,10.2272727 Z M5.13507394,14.5909091 C5.13507394,14.0637273 5.56724324,13.6363636 6.10035316,13.6363636 L9.3605148,13.6363636 C9.89362472,13.6363636 10.325794,14.0637273 10.325794,14.5909091 C10.325794,15.1180909 9.89362472,15.5454545 9.3605148,15.5454545 L6.10035316,15.5454545 C5.56724324,15.5454545 5.13507394,15.1180909 5.13507394,14.5909091 L5.13507394,14.5909091 Z M2.52385574,21.8181818 L16.8651471,21.8181818 L16.8651471,2.18181818 L2.52385574,2.18181818 L2.52385574,21.8181818 Z M17.9683233,0 L1.42067948,0 C0.811450392,0 0.317503223,0.488454545 0.317503223,1.09090909 L0.317503223,22.9090909 C0.317503223,23.5115455 0.811450392,24 1.42067948,24 L17.9683233,24 C18.5775524,24 19.0714996,23.5115455 19.0714996,22.9090909 L19.0714996,1.09090909 C19.0714996,0.488454545 18.5775524,0 17.9683233,0 L17.9683233,0 Z" id="document"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <div class="location-content black-box">
                                <h4 class="loc-title">Traveller Details</h4>
                                <div class="m-t-20">
                                    <div class="input-field">
                                        <input id="field4" type="text">
                                        <label for="field4">Number of Adults</label>
                                    </div>
                                    <div class="input-field">
                                        <input id="field5" type="text">
                                        <label for="field5">Number of Children</label>
                                    </div>
                                    <div class="input-field">
                                        <input id="field5" type="text">
                                        <label for="field5">Number of Rooms</label>
                                    </div>
                                </div>
                                <button type="submit" name="" class="btn btn-primary btn-block">Update Travel Preferences</button>
                            </div>
                        </div>
                        <hr/>
                        <div class="location-inner">
                            <div class="loc-icon">
                                <svg width="28px" height="27px" viewBox="0 0 28 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-20.000000, -995.000000)" fill="#212121">
                                            <g id="(Generic)-Icon---Location-Preferences" transform="translate(20.000000, 995.000000)">
                                                <g id="(eRoam)-Icon---Location-Preferences">
                                                    <path d="M7.53597816,15.6938325 C6.41269472,15.6938325 5.49904215,14.7574227 5.49904215,13.6064237 C5.49904215,12.4554248 6.41269472,11.519015 7.53597816,11.519015 C8.65926159,11.519015 9.57337087,12.4554248 9.57337087,13.6064237 C9.57337087,14.7574227 8.65926159,15.6938325 7.53597816,15.6938325 M7.53597816,9.92609431 C5.53146871,9.92609431 3.90054977,11.5770428 3.90054977,13.6064237 C3.90054977,15.6358046 5.53146871,17.2867531 7.53597816,17.2867531 C9.54071596,17.2867531 11.1718633,15.6358046 11.1718633,13.6064237 C11.1718633,11.5770428 9.54071596,9.92609431 7.53597816,9.92609431 M7.53506473,24.6908759 C6.91781832,24.0081956 5.94250961,22.8703952 4.97108295,21.4975251 C2.91405161,18.5904449 1.82684844,15.9841991 1.82684844,13.9605072 C1.82684844,8.2633127 6.19644154,7.82048075 7.5357498,7.82048075 C12.8329252,7.82048075 13.2446512,12.5200518 13.2446512,13.9605072 C13.2446512,17.9671578 9.15410916,22.8967921 7.53506473,24.6908759 M12.6621149,7.80637203 C11.3504377,6.64171947 9.52975487,6 7.5357498,6 C5.54174473,6 3.72106191,6.64171947 2.40938473,7.80637203 C0.833042887,9.20632173 0,11.3342362 0,13.9605072 C0,19.6809129 6.6061123,26.383923 6.8872186,26.6663251 C7.058714,26.8388156 7.29232224,26.9355287 7.5357498,26.9355287 C7.77917735,26.9355287 8.0127856,26.8388156 8.18428099,26.6663251 C8.4653873,26.383923 15.0714996,19.6809129 15.0714996,13.9605072 C15.0714996,11.3342362 14.2384567,9.20632173 12.6621149,7.80637203" id="location"></path>
                                                    <path d="M19.5359782,9.69383246 C18.4126947,9.69383246 17.4990421,8.75742267 17.4990421,7.60642371 C17.4990421,6.45542476 18.4126947,5.51901497 19.5359782,5.51901497 C20.6592616,5.51901497 21.5733709,6.45542476 21.5733709,7.60642371 C21.5733709,8.75742267 20.6592616,9.69383246 19.5359782,9.69383246 M19.5359782,3.92609431 C17.5314687,3.92609431 15.9005498,5.57704279 15.9005498,7.60642371 C15.9005498,9.63580463 17.5314687,11.2867531 19.5359782,11.2867531 C21.540716,11.2867531 23.1718633,9.63580463 23.1718633,7.60642371 C23.1718633,5.57704279 21.540716,3.92609431 19.5359782,3.92609431 M19.5350647,18.6908759 C18.9178183,18.0081956 17.9425096,16.8703952 16.971083,15.4975251 C14.9140516,12.5904449 13.8268484,9.98419914 13.8268484,7.96050722 C13.8268484,2.2633127 18.1964415,1.82048075 19.5357498,1.82048075 C24.8329252,1.82048075 25.2446512,6.52005182 25.2446512,7.96050722 C25.2446512,11.9671578 21.1541092,16.8967921 19.5350647,18.6908759 M24.6621149,1.80637203 C23.3504377,0.641719466 21.5297549,0 19.5357498,0 C17.5417447,0 15.7210619,0.641719466 14.4093847,1.80637203 C12.8330429,3.20632173 12,5.33423617 12,7.96050722 C12,13.6809129 18.6061123,20.383923 18.8872186,20.6663251 C19.058714,20.8388156 19.2923222,20.9355287 19.5357498,20.9355287 C19.7791774,20.9355287 20.0127856,20.8388156 20.184281,20.6663251 C20.4653873,20.383923 27.0714996,13.6809129 27.0714996,7.96050722 C27.0714996,5.33423617 26.2384567,3.20632173 24.6621149,1.80637203" id="location"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <div class="location-content">
                                <h4 class="loc-title">Activity Preferences</h4>
                                <label>Price Range Per Day</label>
                                <!-- <div>
                                  <b>€ 10</b> <input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> <b>€ 1000</b>
                                </div> -->

                                <div class="layout-slider">
                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                    <input id="Slider1" type="slider" name="price" value="10;300" />
                    </span>
                                </div>

                                <label>Number of Total Days</label>
                                <div class="layout-slider">
                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                    <input id="Slider2" type="slider" name="price1" value="1;500" />
                    </span>
                                </div>

                                <div class="m-t-20 black-box">
                                    <p><label class="radio-checkbox label_check" for="checkbox-33"><input type="checkbox" id="checkbox-33" value="1">I don’t need accommodation within starting City</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-34"><input type="checkbox" id="checkbox-34" value="1">I want to self drive between locations</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-35"><input type="checkbox" id="checkbox-35" value="1">I already have a travel pass</label></p>
                                </div>

                                <div class="m-t-20 black-box">
                                    <label>Tour Type</label>
                                    <p class="m-t-20"><label class="radio-checkbox label_check" for="checkbox-16"><input type="checkbox" id="checkbox-16" value="1">Small Groups (2)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-17"><input type="checkbox" id="checkbox-17" value="1">Tailormade (4)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-18"><input type="checkbox" id="checkbox-18" value="1">Private Departures (5)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-19"><input type="checkbox" id="checkbox-19" value="1">Backpacking</label></p>
                                </div>
                                <div class="m-t-20 black-box">
                                    <label>Tour Themes</label>
                                    <p class="m-t-20"><label class="radio-checkbox label_check" for="checkbox-20"><input type="checkbox" id="checkbox-20" value="1">Discovery (2)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-21"><input type="checkbox" id="checkbox-21" value="1">Walking &amp; Trekking (2)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-22"><input type="checkbox" id="checkbox-22" value="1">Wildlife (5)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-23"><input type="checkbox" id="checkbox-23" value="1">Festival (2)</label></p>
                                    <p><label class="radio-checkbox label_check" for="checkbox-24"><input type="checkbox" id="checkbox-24" value="1">Beyond (2)</label></p>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="create-strip">
                    <a href="#" class="arrow-btn open left_side_arrow_btn"><i class="fa fa-angle-left"></i></a>
                </div>

            </div>


            <div>
                <div class="tabs-wrapper padding-right-0">
                    <div class="tabs-container tabs-container-new" style="padding-top: 48px;">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="panel-inner">
                                    <div class="panel-icon">

                                        <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                                            <desc>Created with Sketch.</desc>
                                            <defs>
                                                <polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
                                            </defs>
                                            <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-503.000000, -69.000000)">
                                                    <g id="placeholder" transform="translate(503.000000, 69.000000)">
                                                        <g id="Group">
                                                            <mask id="mask-2" fill="white">
                                                                <use xlink:href="#path-1"></use>
                                                            </mask>
                                                            <g id="Clip-2"></g>
                                                            <path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="panel-container">
                                        <h4>Bhutan: 11 Tours Found</h4>
                                        <p class="m-t-5">
                                            <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-535.000000, -95.000000)" fill="#212121">
                                                        <g id="Page-1" transform="translate(535.000000, 95.000000)">
                                                            <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
                                                            <path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            All Dates Selected</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 m-t-10">
                                <div class="input-group search-control">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-inner">
                            <hr/>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="custom-tabs new-tabs" data-example-id="togglable-tabs">
                                    <ul class="nav nav-tabs" id="myTabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#top-picks" id="top-picks-tab" role="tab" data-toggle="tab" aria-controls="top-picks" aria-expanded="true">TOP PICKS</a>
                                        </li>

                                        <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop3" data-toggle="dropdown" aria-controls="myTabDrop3-contents">LOWEST PRICE <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="myTabDrop3" id="myTabDrop3-contents">
                                                <li><a href="#lowest-price1" role="tab" id="lowest-price1-tab" data-toggle="tab" aria-controls="lowest-price1">@fat1</a></li>
                                            </ul>
                                        </li>

                                        <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents">RATING <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                                <li><a href="#provider1" role="tab" id="provider1-tab" data-toggle="tab" aria-controls="provider1">@fat</a></li> </ul>
                                        </li>

                                        <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop2" data-toggle="dropdown" aria-controls="myTabDrop2-contents">STARS <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="myTabDrop2" id="myTabDrop2-contents">
                                                <li><a href="#transport1" role="tab" id="transport1-tab" data-toggle="tab" aria-controls="transport1">@fat1</a></li></ul>
                                        </li>

                                        <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop4" data-toggle="dropdown" aria-controls="myTabDrop4-contents">DISTANCE FROM CITY <span class="caret"></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="myTabDrop4" id="myTabDrop4-contents">
                                                <li><a href="#duration1" role="tab" id="duration1-tab" data-toggle="tab" aria-controls="duration1">duration1</a></li>
                                            </ul>
                                        </li>

                                        <!-- <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop5" data-toggle="dropdown" aria-controls="myTabDrop5-contents">STOPS <span class="caret"></span></a>
                                          <ul class="dropdown-menu" aria-labelledby="myTabDrop5" id="myTabDrop5-contents">
                                            <li><a href="#stops1" role="tab" id="stops1-tab" data-toggle="tab" aria-controls="stops1">duration1</a></li>
                                          </ul>
                                        </li> -->

                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-2 content-toogle-icon">
                                <a href="#" class="list-icon">
                                    <svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.45">
                                            <g id="04.03-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-1325.000000, -501.000000)">
                                                <g id="menu" transform="translate(1322.000000, 495.000000)">
                                                    <rect id="bounds" x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M3,18 L21,18 L21,16 L3,16 L3,18 L3,18 Z M3,13 L21,13 L21,11 L3,11 L3,13 L3,13 Z M3,6 L3,8 L21,8 L21,6 L3,6 L3,6 Z" id="Shape" fill="#000000"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <a href="#" class="grid-icon">
                                    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="04.03-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-1373.000000, -499.000000)">
                                                <g id="apps" transform="translate(1369.000000, 495.000000)">
                                                    <path d="M4,8 L8,8 L8,4 L4,4 L4,8 L4,8 Z M10,20 L14,20 L14,16 L10,16 L10,20 L10,20 Z M4,20 L8,20 L8,16 L4,16 L4,20 L4,20 Z M4,14 L8,14 L8,10 L4,10 L4,14 L4,14 Z M10,14 L14,14 L14,10 L10,10 L10,14 L10,14 Z M16,4 L16,8 L20,8 L20,4 L16,4 L16,4 Z M10,8 L14,8 L14,4 L10,4 L10,8 L10,8 Z M16,14 L20,14 L20,10 L16,10 L16,14 L16,14 Z M16,20 L20,20 L20,16 L16,16 L16,20 L16,20 Z" id="Shape" fill="#000000"></path>
                                                    <rect id="bounds" x="0" y="0" width="24" height="24"></rect>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="tabs-content-container white-bg">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab">
                                <div class="tour-wrapper tabs-content-container-new">
                                    <div class="list-box">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                    <h4>12 Day Brisbane, Gold Coast Or Byron Bay To Cairns Flexi</h4>
                                                <p>
                                                    <svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                        <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                                        <desc>Created with Sketch.</desc>
                                                        <defs></defs>
                                                        <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">
                                                                <g id="Group" transform="translate(501.000000, 542.000000)">
                                                                    <g id="Group-16">
                                                                        <path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    Tour Code: ADV 11221   |   Location: Australia
                                                </p>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="row">
                                                    <div class="col-xs-9 m-t-10 text-right border-right">
                                                        <p>From <strong>AU$ 1619</strong> Per Person</p>
                                                        <ul class="rating">
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-3 text-center">
                                                        <h3 class="transport-price"><strong>12</strong></h3>
                                                        Days
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div>
                                            <div class="clearfix">
                                                <div class="logo-img">
                                                    <img src="images/tour-logo.png" alt="" class="img-responsive" />
                                                </div>
                                                <div class="logo-details">
                                                    <div class="row">
                                                        <div class="col-md-2"><strong>Countries:</strong></div>
                                                        <div class="col-md-9">Australia</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2"><strong>Route:</strong></div>
                                                        <div class="col-md-9">
                                                            Brisbane - Gold Coast or Byron Bay to Fraser Island - Airlie Beach &amp; Whitsunday’s - Cairns &amp; Great Barrier Reef - Cape Tribulation &amp; Daintree National Park - Cairns
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="myCarousel" class="carousel slide m-t-20" data-ride="carousel">
                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                    <li data-target="#myCarousel" data-slide-to="0"></li>
                                                    <li data-target="#myCarousel" data-slide-to="1" class = 'active'></li>
                                                    <li data-target="#myCarousel" data-slide-to="2"></li>


                                                </ol>

                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">
                                                    <div class="item">
                                                        <div class="row">

                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour11.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour22.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour33.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour11.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                        </div>
                                                    </div>

                                                    <div class="item active">
                                                        <div class="row">

                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour11.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour22.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour33.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour11.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                        </div>
                                                    </div>

                                                    <div class="item">
                                                        <div class="row">

                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour11.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour22.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour33.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="{{ url( 'images/tour11.jpg')}}" alt="Image" style="max-width:100%;"></a></div>
                                                        </div>
                                                    </div>

                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><div id="map4" class="map-box"></div></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="images/tour11.jpg" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="images/tour22.jpg" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="images/tour33.jpg" alt="Image" style="max-width:100%;"></a></div>
                                                        </div>
                                                    </div>

                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail">
                                                                    <div id="map5" class="map-box"></div>
                                                                </a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="images/tour11.jpg" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="images/tour22.jpg" alt="Image" style="max-width:100%;"></a></div>
                                                            <div class="col-sm-3 col-xs-6"><a href="#" class="thumbnail"><img src="images/tour33.jpg" alt="Image" style="max-width:100%;"></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-t-20 row">
                                                <div class="col-sm-4 m-t-10">
                                                    <button type="submit" name="" class="btn btn-primary btn-block">BOOK ITINERARY</button>
                                                </div>
                                                <div class="col-sm-4 m-t-10">
                                                    <button type="submit" name="" class="btn btn-primary btn-block">CUSTOMISE ITINERARY</button>
                                                </div>
                                                <div class="col-sm-4 m-t-10">
                                                    <button type="submit" name="" class="btn btn-primary btn-block">SAVE ITINERARY</button>
                                                </div>
                                            </div>
                                            <div class="custom-tabs m-t-20" data-example-id="togglable-tabs">
                                                <ul class="nav nav-tabs" id="myTabs1" role="tablist">
                                                    <li role="presentation" class="active"><a href="#overview" id="overview-tab" role="tab" data-toggle="tab" aria-controls="overview" aria-expanded="true">OVERVIEW</a>
                                                    </li>

                                                    <li role="presentation"><a href="#review" id="review-tab" role="tab" data-toggle="tab" aria-controls="review" aria-expanded="true">REVIEWS (356)</a>
                                                    </li>

                                                    <li role="presentation"><a href="#details" id="details-tab" role="tab" data-toggle="tab" aria-controls="details" aria-expanded="true">DETAILS</a>
                                                    </li>

                                                    <li role="presentation"><a href="#qa" id="qa-tab" role="tab" data-toggle="tab" aria-controls="qa" aria-expanded="true">Q&amp;A (6)</a>
                                                    </li>

                                                    <li role="presentation"><a href="#location1" id="location1-tab" role="tab" data-toggle="tab" aria-controls="location1" aria-expanded="true">LOCATION</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tabs-content-container">
                                                <div class="tab-content" id="myTabContent1">
                                                    <div class="tab-pane fade in active" role="tabpanel" id="overview" aria-labelledby="overview-tab">
                                                        <div class="m-t-10">
                                                            <h5>Terms &amp; Conditions - The fine print </h5>
                                                            <p class="m-t-10">Our Flexi tours are a great way to experience Australia's backpacker and adventure destinations. This package is made up of a selection of guided &amp; self guided legs that include transportation, accommodation, tours and some meals. We use a combination of public transport and private transfers to travel between each destination. The accommodation is based on dorm share, in clean, centrally and conveniently located hostels and all of our local tours are operated by reputable companies with a proven history of excellence, quality and value for money.</p>
                                                            <h5><strong>Included</strong></h5>
                                                            <ul class="tour-list m-t-10">
                                                                <li>11 Nights Dorm Accommodation</li>
                                                                <li>All coach transport</li>
                                                                <li>3 Lunches</li>
                                                                <li>Day trip to Fraser Island</li>
                                                                <li>Full day sailing the Whitsunday's</li>
                                                                <li>Great Barrier Reef Cruise</li>
                                                                <li>Daintree River cruise</li>
                                                                <li>Overnight Cape Tribulation</li>
                                                            </ul>
                                                            <div class="m-t-20">
                                                                <h5><strong>Itinerary</strong></h5>
                                                            </div>
                                                            <div class="m-t-10">
                                                                <p><strong>Day 1, 2, 3 &amp; 4: Brisbane, Gold Coast or Byron Bay to Fraser Island</strong></p>
                                                                <p>Once you have met everyone coming along for the journey, we will all hop inside the bus and travel north through Southeast Queensland and the Sunshine Coast before we arrive at the beautiful Coastal Village of Hervey Bay. Famous for its awesome whale watching opportunities as well as serving as the gateway to Fraser Island, we are certain that you will absolutely love it here! After an exploration of both Hervey Bay and Fraser Island, we head to the Tropic of Capricorn in Central Queensland area where you can explore the coast, enjoy the perfect temperatures and soak up that warm Australian sunshine and beach relaxation you have been dreaming of.</p>
                                                                <p class="error">Optional Activities: 3 Day / 2 Night Fraser Island Wilderness Lodge Upgrade</p>
                                                            </div>
                                                            <div class="m-t-10">
                                                                <p><strong>Day 5, 6 &amp; 7: Airlie Beach &amp; Whitsunday’s</strong></p>
                                                                <p>Ever wondered what it would be like to sail the waters of the Whitsunday’s? Well, on days 5-7, you are about to find out! A stunning archipelago of 74 tropical islands awaits for you to explore and discover what makes Whitsunday’s such a sought after location for all travelers. Not only will you love being out on the water but also undoubtedly enjoy the resort located on the main strip featuring a swimming pool, bar and night club ready for you to have some serious fun! </p>
                                                                <p class="error">Optional Activities: 3 Day liveaboard Sailing, 3 Days Island hopping
                                                                </p>
                                                            </div>
                                                            <div class="m-t-10">
                                                                <p><strong>Day 8 &amp; 9: Cairns &amp; Great Barrier Reef</strong></p>
                                                                <p>This part of the journey will take you to Australia’s adventure playground, Cairns, where you can choose to join in on some fun and thrilling adventures such as bungee jumping, white water rafting, kayaking, ballooning and some incredible diving! Explore the magnificent underwater world that the Great Barrier Reef is world famous for while cruising around on one of the fastest vessels, making sure you have enough time to explore all that the reef has to offer! Once the sun goes down, it is time for some refreshing beverages to get the night started off just right. </p>
                                                                <p class="error">Optional Activities: Overnight dive / snorkel on Great Barrier Reef, PADI Learn to Dive, Bungy Jump, White Water Rafting</p>
                                                            </div>
                                                            <div class="m-t-10">
                                                                <p><strong>Day 10 &amp; 11: Cape Tribulation &amp; Daintree National Park  </strong></p>
                                                                <p>The last final couple of days of your Flexi Tour will take you on a journey through the majestic rainforests, pristine rivers and white sandy beaches of the World Heritage listed site of Cape Tribulation as well as Daintree National Park. Watch your fingers as you cruise down the Daintree River and spot crocodiles and simply take in all of the natural beauty and wildlife that seems to exude every inch of this place. The next morning, spend even more time exploring the area on your own and soaking up the natural wonders before heading out to the final destination.</p>
                                                            </div>
                                                            <div class="m-t-10">
                                                                <p><strong>Day 12: END Cairns</strong></p>
                                                                <p>Although you may not want it to end quite yet, day 12 will bring you to the final leg of the adventures where you will say your farewells to all of your new friends you made along the way. Fortunately, Cairns makes for the perfect spot to depart and has plenty of sites to see and things to do with the option of course to grab a final bite to eat and some last-day exploring. Walking away from this trip will conclude a full 12 days of thrilling excitement, crazy stories to look back on and memories that will surely last for many years to come. </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" role="tabpanel" id="review" aria-labelledby="review-tab">
                                                        <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                                                    </div>
                                                    <div class="tab-pane fade" role="tabpanel" id="details" aria-labelledby="details-tab">
                                                        <p>Adventure Travel Pty Ltd ABN 45117444292 and Travel License VIC32783 is an Inbound Tour Wholesaler and, in the provision of holidays or travel products acts solely as an agent on behalf of the Operator Services.  Adventure Travel Pty Ltd is not liable to you for any act, default or neglect of any kind by any Operational Services.</p>
                                                        <p>Operator Services are all travel, accommodation and other associated services provided to you by a Third Party tour, travel, accommodation or other operator, booked on your behalf by the Travel Agent.</p>
                                                        <p><strong>Disclaimer:</strong> Due to conditions outside of our control such as natural disaster, weather, politics or operational problems (e.g. Flooding, road and park closures), AdventureTravel.Com.Au Pty Ltd and its suppliers have the right to cancel a sector / sectors and or alter the itinerary. In these circumstances, it is not a guarantee that missed or cancelled sectors will be fully refunded. This will be at the suppliers discretion and AdventureTravel.Com.Au Pty Ltd will charge an administration fee. In circumstances where the cancellation / amendment is due to external events outside our reasonable control, refunds will be less any unrecoverable costs and applicable administration fees. We are not responsible for any incidental expenses that may be incurred as a result of amending / cancelling the booking including but not limited to Travel Insurance excess, non-refundable flights or  alternative travel arrangements i.e.: connecting travel services, additional accommodation, tours or upgrading. This will be at the clients expense. AdventureTravel.Com.Au Pty Ltd will endeavour to organise new arrangements for travel during any of these circumstances. ( Amendment fees will apply as per our usual terms and conditions) However, it will be ultimately be the clients own responsibility. AdventureTravel.Com.Au Pty Ltd cannot be held responsible for any forward travel arrangements that are affected by any of the above. Change or cancellation of forward travel booked through AdventureTravel.Com.Au Pty Ltd will be treated under our normal terms and conditions.</p>
                                                        <p>In the unlikely hood that AdventureTravel.Com.Au Pty Ltd or its suppliers, are forced to cancel a tour due to low numbers, AdventureTravel.Com.Au Pty Ltd or its suppliers, will offer an alternative itinerary of similar inclusions. Any extra costs incurred from the alternative arrangements, such as additional meals etc will be at the clients expense.  Passengers accepting the change of itinerary will be bound by the normal terms and conditions. In the circumstance that AdventureTravel.Com.Au Pty Ltd is acting as an agent only, any dispute will be handled direct by the Supplier. The itinerary provided for each trip is representative of the types of activities contemplated, but it is understood that the route, schedules, itineraries, amenities and mode of transport may be subject to alteration without prior notice due to local circumstances or events.</p>
                                                        <p>In the circumstance that AdventureTravel.Com.Au Pty Ltd is the operator, the dispute will be handled by AdventureTravel.Com.Au Pty Ltd and is bound by Victorian law (Australia) where any disputes will be settled.</p>
                                                        <p><i>Prices are subject to change.</i></p>
                                                        <p><strong>Refunds:</strong> Refunds are made in AUD only and AdventureTravel.Com.Au Pty Ltd cannot be held responsible for loss due to bank fees or fluctuations in currency exchange rates. Refunds may take up to 30 days to process.</p>
                                                        <p><strong>Cancellation Fees:</strong> In the event of cancellation the following cancellation fees will be payable by the client:</p>
                                                        <ul class="tour-list">
                                                            <li>Outside 55 days - Deposit only</li>
                                                            <li>42 to 55 days prior - 35% of tour cost plus full Greyhound Pass cost if applicable</li>
                                                            <li>28 to 41 days prior - 50% of tour cost plus full Greyhound Pass cost if applicable</li>
                                                            <li>14 to 27 days prior - 75% of tour cost plus full Greyhound Pass cost if applicable</li>
                                                            <li>Less than 14 days prior - 100% of tour cost, if the deposit is greater than the cancellation fee then the deposit will be forfeited. Amendment Fees: If a date change or itinerary change is done at a Clients request, a fee from $50 to $200 per passenger per change will be charged on top of any applicable supplier fees.
                                                                If these changes fall within the cancellation period, then any applicable cancellation fees will also apply.</li>
                                                        </ul>
                                                        <p class="m-t-10"><strong>Travel Insurance:</strong> We strongly recommend clients to purchase travel insurance at the same time as booking travel arrangements to suitably cover medical, personal effects, travel delays and cancellation costs.</p>
                                                        <p>AdventureTravel.Com.Au Pty Ltd is the booking agent for Trek Australia, Backpacker Tours Australia &amp; AdventureTravel.Com.Au</p>
                                                        <p>All cancellations or disputes should be sent by registered post to:</p>
                                                        <ul class="tour-list">
                                                            <li><i>Operations</i></li>
                                                            <li><i>AdventureTravel.Com.Au Pty Ltd</i></li>
                                                            <li><i>7 Davies Street</i></li>
                                                            <li><i>Mt Eliza VIC 3930</i></li>
                                                        </ul>
                                                        <p class="m-t-10"><strong>Consumer Protection:</strong></p>
                                                        <p>In Australia we have very strict regulations to protect consumers against fraudulent operations or trading insolvent. To be an agent in Australia you need to operate a client trust account which is independently audited each year. You also need to have money guarantees in place to protect consumer's funds. This is government regulated. Before you book with any tour operator in Australia you should check to see that you are protected by the TCF (Travel Compensation Fund). To check to see if a company is operating legally you should visit: http://www.tcf.org.au</p>
                                                        <p>Our company name is AdventureTravel.Com.Au - 1/7 Davies Ave, MT ELIZA, VIC, 3930 - Licence No. VIC32783</p>
                                                        <p>To do a quick search you can just enter our Post Code "3930" and scroll down to "AdventureTravel.com.au".</p>
                                                    </div>
                                                    <div class="tab-pane fade" role="tabpanel" id="qa" aria-labelledby="qa-tab">
                                                        <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                                                        <p><strong>Definitions:</strong></p>
                                                        <ul class="tour-list">
                                                            <li><u>Supplier</u> - A supplier is the third party tour operator or service provider that operates the travel arrangements or services for the Client.</li>
                                                            <li><u>Client</u> - The client is the person that has made the booking with AdventureTravel.Com.Au Pty Ltd and is responsible for adhering to the booking conditions and acts as the sole correspondent for the client and the client's party.</li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pane fade" role="tabpanel" id="location1" aria-labelledby="location1-tab">
                                                        <p>1Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" role="tabpanel" id="provider1" aria-labelledby="provider1-tab">
                                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="transport1" aria-labelledby="transport1-tab">
                                <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="lowest-price1" aria-labelledby="lowest-price1-tab">
                                <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="duration1" aria-labelledby="duration1-tab">
                                <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                            </div>
                            <!-- <div class="tab-pane fade" role="tabpanel" id="stops1" aria-labelledby="stops1-tab">
                              <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                            </div>  -->

                        </div>
                    </div>
                </div>
            </div>
        </div>



        </div>
    </section>


@stop

@section( 'custom-js' )

    <script type="text/javascript">
        $(document).ready(function () {

            $('#myCarousel').carousel({
                interval: 5000
            })

            $("#Slider1").slider({
                from: 10, to: 500, step: 10, smooth: true, round: 0, dimension: "&nbsp;$", skin: "round", limits: false
            });

            $("#Slider2").slider({
                from: 1, to: 500, step: 1, smooth: true, round: 0, dimension: "&nbsp;Day", skin: "round", limits: false
            });

            $("#Slider3").slider({
                from: 10, to: 500, step: 10, smooth: true, round: 0, dimension: "&nbsp;$", skin: "round", limits: false
            });

            $("#Slider4").slider({
                from: 1, to: 500, step: 1, smooth: true, round: 0, dimension: "&nbsp;Day", skin: "round", limits: false
            });

            $('#scroll-box').slimScroll({
                height: '100%',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

            $('.tabs-content-container-new').slimScroll({
                height: '840px',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });
        });

        function leftpanel() {
            var w = $(document).width();
            if (w > 1199) {
                $('.arrow-btn').click(function () {
                    if ($('.arrow-btn').hasClass('open')) {
                        $('.location-wrapper').css('display', 'none');
                        $(this).removeClass('open');
                        $('.tabs-wrapper').css('margin-left', '49px');
                        // $('.location-wrapper').animate({
                        //       marginLeft: 49px
                        // }, 500)
                    } else {
                        $('.location-wrapper').css('display', 'block');
                        $(this).addClass('open');
                        $('.tabs-wrapper').css('margin-left', '431px');
                    }
                });
            } else {
                $('.arrow-btn').click(function () {
                    if ($('.arrow-btn').hasClass('open')) {
                        $('.location-wrapper').css('display', 'block');
                        $(this).removeClass('open');
                        $('.tabs-wrapper').css('margin-left', '431px');
                    } else {
                        $('.location-wrapper').css('display', 'none');
                        $(this).addClass('open');
                        $('.tabs-wrapper').css('margin-left', '49px');
                    }
                });
            }
        }

        $(document).ready(function () {
            //sidepanel();
            leftpanel();
            // $('.arrow-btn').click(function(){
            //     if($('.arrow-btn').hasClass('open')){
            //       $('.location-wrapper').css('display', 'none');
            //       $(this).removeClass('open');
            //       $('.tabs-wrapper').css('margin-left', '49px');

            //     } else{
            //       $('.location-wrapper').css('display', 'block');
            //       $(this).addClass('open');
            //       $('.tabs-wrapper').css('margin-left', '431px');
            //     }
            // });
        });

        $(window).resize(function () {
            //sidepanel();
            leftpanel();
        });


    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#cust_date").datepicker({
                format: 'D dd M yyyy',
                autoclose: true,
            });
        });
    </script>


@stop