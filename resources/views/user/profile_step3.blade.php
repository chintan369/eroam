@extends('layouts.profileLayout')
@section('content')  
<script src="{{ url( 'js/theme/jquery.min.js' ) }}"></script>  
<section>
<div class="profile_page">
  @include('pages.search_banner') 
</div>
<section>
    @include('user.profile_sidebar')
    
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    @include('user.profile_header')
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    @if(session()->has('profile_step3_success'))
                        <p class="success-box m-t-30">
                            {{ session()->get('profile_step3_success') }}
                        </p>
                    @endif
                    @if(session()->has('profile_step3_error'))
                        <p class="danger-box m-t-30">
                            {{ session()->get('profile_step3_error') }}
                        </p>
                    @endif
                    <div class="m-t-30 profile-prefrence">
                <h2 class="profile-title">Personal Preferences</h2>
                <form class="form-horizontal m-t-20" method="post" id="profile_step3_form"> 
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Age Group</label>
                    <select id="age-group" name="age_group" class="form-control age_group_id">
                        <option value="">Select Age Group</option>
                        @foreach ($travel_preferences['age_groups'] as $age_group)
                            <option value="{{ $age_group['id'] }}" {{ $user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : '' }}>{{ $age_group['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Transport Type</label>    
                      <select class="form-control transport_type_optionsp" name="transport_types[]" id="dropdown-transport">
                      <option value="">Select Transport Type</option>                
                        <option value="1" {{ $user->customer->pref_transport_types == 1 ? 'selected' : '' }}>Flight</option>
                        <option value="25" {{ $user->customer->pref_transport_types == 25 ? 'selected' : '' }}>Overland</option>
                        <?php /*{!! $transport_type_options !!}*/ ?>
                      </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Gender</label>
                    <select class="form-control gender_sel" name="gender">
                        <option value="">Select Gender</option>
                        <option value="male" {{ $user->customer->pref_gender == 'male' ? 'selected' : '' }}>Male</option>
                        <option value="female" {{ $user->customer->pref_gender == 'female' ? 'selected' : '' }}>Female</option>
                        <option value="other" {{ $user->customer->pref_gender == 'other' ? 'selected' : '' }}>Other</option>
                    </select>
                  </div>

                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Accommodation Type</label>

                    <select class="form-control hotel_category"  name="hotel_category_id[]">
                        <option>Select Accommodation Type</option>
                        <option value="1" {{ $user->customer->pref_hotel_categories && in_array(1, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 1 Star</option>
                        <option value="5" {{ $user->customer->pref_hotel_categories && in_array(5, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 2 Star</option>
                        <option value="2" {{ $user->customer->pref_hotel_categories && in_array(2, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 3 Star</option>
                        <option value="3" {{ $user->customer->pref_hotel_categories && in_array(3, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 4 Star</option>
                        <option value="9" {{ $user->customer->pref_hotel_categories && in_array(9, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 5 Star</option>
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Nationality</label>                    
                    <select id="nationality"  name="nationality" class="form-control nationality_id" >
                        <option value="">Select Nationality</option>
                        @foreach ($travel_preferences['nationalities']['featured'] as $nationality)
                            <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                        @endforeach
                            <option disabled> ======================================== </option>
                        @foreach ($travel_preferences['nationalities']['not_featured'] as $nationality)
                            <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Room Type</label>
                    <select class="form-control room_type_optionsp" name="room_type">
                        <option>Select Room Type</option>
                        <option value="2" {{ $user->customer->pref_hotel_room_types && in_array(2, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Single</option>
                        <option value="1" {{ $user->customer->pref_hotel_room_types && in_array(1, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Twin</option>
                        <option value="8" {{ $user->customer->pref_hotel_room_types && in_array(8, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Triple</option>
                        <option value="22" {{ $user->customer->pref_hotel_room_types && in_array(22, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Quad</option>
                    </select>
                  </div>                  
                  <div class="m-t-20">
                <h4 class="modal-title" id="myModalLabel">Activity Preferences</h4>
                <p>Please drag your preferred activities into the boxes below. Highest priority begins on the left.</p> 

                <div class="m-t-10">
                <ul class="drop-list drop-lists" id="drop-listss">

                  <?php 
                      $imgArr = array(
                        15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                        5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                        13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                        29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                        11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                        20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                         //'Expeditions','Astronomy','Food','Short-Breaks'
                        );

                      $ij = 0;
                      if( count($interest_ids) > 0 ){
                        foreach ($interest_ids as $j => $interest_id){
                          $key = array_search($interest_id, array_column($labels, 'id'));
                          $label = $labels[$key];
                          $ij++;
                  ?>
                            <li ondrop="dropp(event)" ondragover="allowDropp(event)" class="interest-button-active" id="interestp-{{ $label['id'] }}" data-sequence="{{ $ij }}" data-name="{{ $label['name'] }}" style="width:81px !important;height: 81px !important">
                              <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="dragp(event)" id="dragp_{{ $label['id'] }}" class="img-responsive" alt="{{ $j }} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                              <input type="hidden" id="inputValuep_{{ $label['id'] }}" class="input-interest" name="interests[{{$j + 1}}]" value="{{ $label['id'] }}">
                            </li>
                  <?php  } } ?>

                  <?php 
                    for($k = $ij; $k < 8; $k++){
                      echo '<li ondrop="dropp(event)" ondragover="allowDropp(event)" class="blankInterests" data-sequence="'.($k + 1).'"  style="width:81px !important;height: 81px !important"></li>';
                    }
                  ?>
                </ul>
                <div class="clearfix"></div>
              </div>

              <hr/>
              <div class="m-t-10">

                <ul class="drag-list interest-lists first">
                  @if( count($labels) > 0 )
                    @foreach ($labels as $i => $label)

                      @if( empty($label['name']) )
                        @continue
                      @endif
                      <li ondrop="dropp(event)" ondragover="allowDropp(event)" data-value="{{$label['id']}}" class="{{ in_array($label['id'], $interest_ids ) ? ' interestMove' : ''  }}" id="interestp-{{ $label['id'] }}">
                        <?php
                          $namedis = 'block';
                          if(!in_array($label['id'], $interest_ids )){
                            $namedis = 'none';
                        ?>
                          <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="dragp(event)" id="dragp_{{ $label['id'] }}" class="img-responsive" alt="{{ $i}} - {{ $label['name'] }}" data-value="{{$label['id']}}"  data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                          <input type="hidden" id="inputValuep_{{ $label['id'] }}" class="input-interest" name="interests[]" value="">
                        <?php } ?>
                          <span style="display:{{ $namedis }};" id="namep_{{ $label['id'] }}">{{ $label['name'] }}</span>
                          
                      </li>
                    @endforeach
                  @endif
                </ul>
                <div class="clearfix"></div>
              </div>
              </div>
                <div class="m-t-20">
                    <button type="submit" name="" id="save_travel_preference" class="btn btn-black btn-block">UPDATE PROFILE</button>
                </div>
                <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
              </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<script type="text/javascript">
  var activitySequence = [{{ join(', ', $interest_ids) }}];
</script>
@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_leftmenu.js?v='.$version.'') }}"></script>
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_step3.js?v='.$version.'') }}"></script>
@stop