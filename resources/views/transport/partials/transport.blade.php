<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="container-fluid">
        <div class="accommodation_top pt-4 pb-3 ">
            <div class="pl-3">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
                        <ul>
                            <li class="d-inline pr-2 pl-4"><i class="ic-place"></i> <strong> {{ $data['city_name'] }}, {{ $data['country_name']}} <span class="trans-count"></span> </strong> </li>
                        </ul>
                        <p class="pt-3"><i class="ic-calendar"></i> {{ convert_date($data['date_from'], 'new_eroam_format') }} - {{ convert_date($data['date_to'], 'new_eroam_format') }}</p>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-7">
                        <div class="form-group">
                            <div class="fildes_outer pt-0">
                                <input type="text" class="form-control pt-0" aria-describedby="emailHelp" placeholder="11 Nights">
                                <span class="arrow_down"><i class="fa fa-search"></i> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-4">
                        <div class="transport_filter">
                            <ul class="nav nav-pills nav-fill">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" href="#">@lang('home.transport_filter_lable1')  </a>
                                    <ul class="dropdown-menu border-0 rounded-0" id="stop-tab">
                                      <li><a class="dropdown-item" id="searchStop">@lang('home.all_text')</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" href="#">@lang('home.transport_filter_lable2') </a>
                                    <ul class="dropdown-menu border-0 rounded-0" id="provider-tab">
                                        <li><a href="#" class="dropdown-item" id="searchProvider">@lang('home.all_text')</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" href="#">@lang('home.transport_filter_lable3')  </a>
                                    <ul class="dropdown-menu border-0 rounded-0" id="transport-tab">
                                        <li><a class="dropdown-item" id="transportProvider"  href="#">@lang('home.all_text')</a></li>
                                        <li><a class="dropdown-item" id="transportProvider" href="#">@lang('home.transport_filter_lable_10')</a></li>
                                        <li><a class="dropdown-item" id="transportProvider" href="#">@lang('home.transport_filter_lable9')</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="durationAsc" href="#">@lang('home.transport_filter_lable4') <i class="fa fa-caret-down" aria-hidden="true"></i> </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" id="priceSortAsc">@lang('home.transport_filter_lable5') <i class="fa fa-caret-down" aria-hidden="true"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="itinerary_page pt-2">
            <div class="container-fluid">
                <div class="transportsfound" >
                    <div id="accordion">
                        <div class="" id="transportListSelected" role="tablist" aria-multiselectable="true">
                        </div>
                        <div class="tabs-content-wrapper" id="transportList" role="tablist" aria-multiselectable="true">
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>