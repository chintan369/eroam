<?php //echo "<pre>";print_r(session()->get('orderData'));
$data = session()->get('orderData');
//exit; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>:: eroam ::</title>

</head>
<body style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; color: #212121;">
<table style="width: 700px; margin: 0 auto; border: none;">
	<tr>
		<td>
			<table width="100%" id="header" style="vertical-align: middle; text-align: center; background-color: #212121; padding: 5px 0px 1px 0;">
				<tr>
					<td>
						<img src="{{ url( 'images/eRoam_LogoNew.png' ) }}" alt="" width="120px"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<h1 style="font-size: 20px; color: #212121; letter-spacing: 0.71px; line-height: 18px; margin-top: 20px; margin-bottom: 20px;">Booking Information</h1>
		</td>
	</tr>
	<tr>
		<td>
			<table style="width: 100%;">
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;">
							<tr>
								<td colspan="2">
									<h4 style="font-size: 20px; line-height: 22px; font-weight: 500; margin-top: 0px; margin-bottom: 20px;"><strong>Personal Details</strong></h4>
								</td>
							</tr>
							<tr>
								<td colspan="2"><strong>Booking Id:</strong> <?php echo $data['invoiceNumber'];?></td>
							</tr>
							<tr>
								<td width="50%"><strong>Customer Name:</strong> <?php echo $data['passenger_info'][0]['passenger_first_name'].' '. $data['passenger_info'][0]['passenger_last_name'];?></td>
								<td width="50%"><strong>Customer Email:</strong> <?php echo $data['passenger_info'][0]['passenger_email'];?></td>
							</tr>
							<tr>
								<td width="50%"><strong>Customer Contact:</strong> <?php echo $data['passenger_info'][0]['passenger_contact_no'];?></td>
								
							</tr>
							<tr>
								<td width="50%"><strong>Customer Dob:</strong> <?php echo $data['passenger_info'][0]['passenger_dob'];?></td>
								<!-- <td width="50%"><strong>Customer Country:</strong> <?php //echo $data['passenger_info'][0]['passenger_country_name'];?></td> -->
							</tr>
							<tr>
								<td width="50%"><strong>Days:</strong> <?php echo $data['total_days'];?></td>
								<td width="50%"><strong>Total Amount:</strong> AUD <?php echo $data['cost_per_day'];?> per day (TOTAL AUD <?php echo $data['total_per_person'];?> (per person))</td>
							</tr>
							<tr>
								<td width="50%"><strong>Date:</strong> <?php echo date("j M Y", strtotime($data['from_date']));?> - <?php echo date("j M Y", strtotime($data['to_date']));?></td>
								<td width="50%"><strong>Travellers:</strong> <?php echo $data['travellers'];?></td>
							</tr>
						</table>
					</td>
				</tr>
                <?php

                $last_key = count($data['leg']) -1;
                foreach ($data['leg'] as $key => $value) {

                    ?>
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;">
							<tr>
								<td colspan="2">
									<h4 style="font-size: 20px; line-height: 22px; margin-top: 0px; margin-bottom: 20px;">{{$value['from_city_name']}}, {{$value['country_code']}}</h4>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/hotel-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;"/> &nbsp; Accommodation</h5></td>
							</tr>
                            <?php
                            if(isset($value['hotel']) && !empty($value['hotel'])){
                            ?>
							<tr>
								<td width="50%">Accommodation Name: {{$value['hotel'][0]['leg_name']}}</td>
								<td width="50%">{{$value['hotel'][0]['nights']}} Nights</td>
							</tr>
							<tr>
								<td width="50%">Check-in: {{ date( 'j M Y', strtotime($value['hotel'][0]['checkin']))}}</td>
								<td width="50%">Check-out: {{ date( 'j M Y', strtotime($value['hotel'][0]['checkout']))}}</td>
							</tr>
							<tr>
								<td width="50%">Price: {{$value['hotel'][0]['currency']}} {{ceil($value['hotel'][0]['price'])}}</td>
								<?php if(!empty($value['hotel'][0]['provider_booking_status'])){ ?>
								<td width="50%">Booking Id: {{$value['hotel'][0]['booking_id']}}</td>
								<?php }else{ ?>
								<td width="50%">Booking Status: Not Confirmed *</td>
								<?php } ?>
							</tr>

							<?php }else{ ?>
							<tr>
								<td colspan="2">Own Arrangement</td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/activity-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;"/> &nbsp; Activity Summary</h5></td>
							</tr>
							<?php
                            if(isset($value['activities']) && !empty($value['activities'])){
                            foreach ($value['activities'] as $key1 => $value1) {
								?>

							<tr>
								<td width="50%">Activity Name: {{$value1['leg_name']}}</td>
								<td width="50%">Date: {{ date( 'j M Y', strtotime($value1['from_date']))}}</td>
							</tr>
							<tr>
								<td width="50%">Price: {{$value1['currency']}} {{ceil($value1['price'])}}</td>
                                <?php if($value1['provider_booking_status'] == 'CONFIRMED'){ ?>
								<td width="50%">Booking Id: {{$value1['booking_id']}}</td>
								<?php }else{ ?>
								<td width="50%">Booking Status: Not Confirmed *</td>
								<?php } ?>
							</tr>
							<tr>
								<td colspan="2">Provider Name: Viator</td>

							</tr>
                            <?php } }else{ ?>
							<tr>
								<td colspan="2">Own Arrangement</td>
							</tr>
                            <?php } ?>
							<tr>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/transport-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;"/> &nbsp;Transport</h5></td>
							</tr>
							<?php if(isset($value['transport']) && !empty($value['transport'])){ ?>
                            <?php
                            $depart = $value['transport'][0]['departure_text'];
                            $arrive = $value['transport'][0]['arrival_text'];
                            if($value['transport'][0]['leg_name'] == 'Flight'){
                                if(!empty($value['transport'][0]['booking_summary_text'])){
                                    $leg_depart = explode("Depart:",$value['transport'][0]['booking_summary_text'],2);
                                    $leg_arrive = explode("Arrive:",$value['transport'][0]['booking_summary_text'],2);

                                    if(isset($leg_depart[1])){
                                        $depart = explode("</small>",$leg_depart[1],2);
                                        $depart = $depart[0];
                                    }else{
                                        $depart = '';
                                    }
                                    if(isset($leg_arrive[1])){
                                        $arrive = explode("</small>",$leg_arrive[1],2);
                                        $arrive = $arrive[0];
                                    }else{
                                        $arrive = '';
                                    }
                                }else{
                                    $depart = '';
                                    $arrive = '';
                                }
                            }
                            if($value['transport'][0]['provider'] == 'busbud'){


                                $depart = $value['transport'][0]['departure_text'];
                                $arrive = $value['transport'][0]['arrival_text'];

                            }

                            ?>
                            <?php
                            $leg_name = explode('Depart:', $value['transport'][0]['booking_summary_text']);
                            if(isset($value['transport'][0]['booking_summary_text']) && !empty($value['transport'][0]['booking_summary_text'])){
                                $leg_name = trim(strip_tags($leg_name[0]));
                            }else{
                                $leg_name = strip_tags($value['transport'][0]['leg_name']);
                            }
                            ?>
							<tr>
								<td colspan="2">Service:{{$leg_name}}</td>
							</tr>
							<tr>
								<td width="50%">Depart: {{strip_tags($depart)}}</td>
								<td width="50%">Arrive: {{strip_tags($arrive)}}</td>
							</tr>
							<tr>
								<td width="50%">Price: {{$value['transport'][0]['currency']}} {{ceil($value['transport'][0]['price'])}}</td>
								<?php if($value['transport'][0]['provider_booking_status'] == 'CONFIRMED'){ ?>
								<td width="50%">Booking Id: {{$value['transport'][0]['booking_id']}}</td>
								<?php }else{ ?>
								<td width="50%">Booking Status: Not Confirmed *</td>
								<?php } ?>
							</tr>

							<?php }else{ ?>
							<tr>
								<td colspan="2">Own Arrangement</td>
							</tr>
							<?php } ?>
						</table>
					</td>
				</tr>
<?php } ?>

			</table>
		</td>
	</tr>

	<tr>
		<td>
			<table width="100%" id="footer" style="color: #fff; padding: 9px 10px 6px 10px; vertical-align: middle; text-align: center; background-color: #212121;">
				<tr>
					<td><img src="{{ url( 'images/footer-logoNew.png' ) }}" alt="" style="width: 100px;"/></td>
					<td style="font-size: 14px; text-align: right; font-size: 13px;">
						Powered by eRoam © Copyright 2016 - 2018. All Rights Reserved. Patent pending AU2016902466
					</td>
				</tr>
			</table>
		</td>
	</tr>

</table>
</body>
</html>