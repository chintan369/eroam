
var AEController = new function() {


	function checkHotelAvailability( aeRQ, callbackSuccess, callbackFailure )
	{
		var aeApiCall = eroam.apiDeferred('arabian/explorer/hotelAvailabilitySearch', 'POST', aeRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( aeApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}


	function makeReservation( aeRQ, callbackSuccess, callbackFailure )
	{
		var aeApiCall = eroam.apiDeferred('api-ae/get/excursion-availability', 'POST', aeRQ, 'rawResponse', true );
		eroam.apiPromiseHandler( aeApiCall, function( rs ){
			( rs.success ) ? callbackSuccess( rs.data ) : callbackFailure( rs.error );
		});
	}


	return {
		checkHotelAvailability : checkHotelAvailability
	};

};