<?php
	$btnName = ($data['select'] == 'true')?'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
	$btnClass = ($data['select'] == 'true')?'selected-activity' : '';
	$selectDiv = '<div class="col-12 col-sm-12 col-md-12 col-lg-3 pt-5">
			  <input type="text"  '.$data['attributes'].' class="datetimepick" id="view_datepick-'.$data['code'].'" value="">
			  <a href="#" class="btn btns_input_blue transform d-block w-100 act-btn act-select-btn '.$btnClass.'" data-view= "view" data-provider="'.$provider.'" data-city-id="'.$data['cityid'].'" data-index="'.$code.'" data-activity-id="'.$code.'" data-is-selected="'.$data['select'].'" data-price="'.$data['price'].'"  data-price-id=""  data-name="'.$data['activity_name'].'" data-currency="'.session()->get( 'search' )['currency'].'" data-startdate="'.$data['startDate'].'" data-enddate="'.$data['endDate'].'" data-description="" data-images="" data-duration="'.$data['duration'].'" data-label="" data-button-id="'.$data['code'].'">'.$btnName.'</a>
			</div>';
?>

 


<div class="itinerary_page pt-2">
	<div class="container-fluid">
		<div class="activities mb-5">
			<div class="activities_list pt-4 pb-4 pr-3 pl-3 mb-4">
			
			<a href="#" class="hotel-close act-btn-less " data-provider="{{ $provider }}" data-code="{{ $code }}"><i class=" ic-clear"></i> </a>
				<div class="mb-3">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">
							<h4>{{ $data['activity_name'] }}</h4>
							<p> <svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121"><g id="Group" transform="translate(501.000000, 542.000000)"><g id="Group-16"><path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path></g></g></g></g></svg> Tour Code: {{ $data['code'] }} |&nbsp;Location: @if($data['address']){{ $data['address'] }}, @endif {{ $data['city_name'] }}
							</p>
						</div>
						<div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">
							<div class="text-right">
								<p>From <strong>{{ session()->get( 'search' )['currency'].' '.$data['price'] }}</strong> @lang('home.activity_label9')</p>
								<p>Duration : {{ $data['duration'] }}</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12">
						<div class="owl-carousel owl-theme">
							<?php 
								$i=0;
								foreach($data['images'] as $key => $image){
									$i++
							?>
								<div class="item"><img src="{{ $image }}" alt="Image" /> </div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="border-bottom border-top   pt-1 mt-3">
					<ul class="nav nav-tabs border-0 active_detiles_page" id="myTab" role="tablist">
						<li class="nav-item ">
							<a class="nav-link active border-right-0 border-top-0 border-left-0  " id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="home" aria-selected="true">
							@lang('home.overview_text_heading')
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link border-right-0 border-top-0 border-left-0" id="importantinfo-tab" data-toggle="tab" href="#importantinfo" role="tab" aria-controls="profile" aria-selected="false">@lang('home.activity_tab1') </a>
						</li>

					</ul>
				</div>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
						<div class="pt-4 pr-3 pl-3 pb-3 ">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-9">
									<p><strong>@lang('home.proposed_itinerary_label4')</strong> </p>
									<p>{!! $data['activity_description'] !!}</p>
									<?php if($data['highlights']){ ?> 
										<p class="m-b-0"><strong>@lang('home.proposed_itinerary_label7') </strong></p>
										<ul class="m-l-30">
											<?php foreach ($data['highlights'] as $key => $value) { ?>
												<li><?php echo $value?></li>
											<?php } ?>
										</ul>
									<?php } ?>

									<?php if($data['description']){ ?> 
										<p class="m-b-0"><strong>@lang('home.activity_summery_label1') </strong></p>
										<p><?php echo $data['description']; ?></p>
									<?php } ?>

									

									<?php if($data['itinerary']){ ?> 
										<p class="m-b-0"><strong>@lang('home.activity_label_11') </strong></p>
										<p><?php echo $data['itinerary']; ?></p>
									<?php } ?>
								</div>
								
								<?php echo $selectDiv; ?>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="importantinfo" role="tabpanel" aria-labelledby="importantinfo-tab">
						<div class="pt-4 pr-3 pl-3 pb-3 ">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-9">
									<p><strong> @lang('home.profile_details_text')</strong></p>
									<?php if($data['inclusions']){ ?> 
										<p class="mb-0">@lang('home.activity_label_21')</p>
											<?php foreach ($data['inclusions'] as $key => $value) { ?>
												<?php echo $value?>
											<?php } ?>
									<?php } ?>

									<?php if($data['exclusions']){ ?> 
										<p class="m-b-0">@lang('home.activity_label_20')</p>
										<?php foreach ($data['exclusions'] as $key => $value) { ?>
											<li><?php echo $value?></li>
										<?php } ?>
									<?php } ?>

									<?php if($data['additionalInfo']){ ?> 
										<p class="mb-0">@lang('home.activity_label_19')</p>
										<ul class="ml-4">
											<?php foreach ($data['additionalInfo'] as $key => $value) { ?>
												<li><?php echo $value?></li>
											<?php } ?>
										</ul>
									<?php } ?>

									<?php if($data['voucherRequirements']){ ?> 
										<p class="m-b-0">@lang('home.activity_label_18')</p>
										<p><?php echo $data['voucherRequirements']; ?></p>
									<?php } ?>

									<?php if($provider == 'viator') { ?> 
										<p class="m-b-0 m-t-10">@lang('home.activity_label_17')</p>
										<p>Complete Operator information, including local telephone numbers at your destination, are included on your Confirmation Voucher. Our Product Managers select only the most experienced and reliable operators in each destination, removing the guesswork for you, and ensuring your peace of mind.</p>

										<p class="m-b-0 m-t-10"><strong>@lang('home.activity_label_16')</strong></p>
										<p>If you cancel at least 7 day(s) in advance of the scheduled departure, there is no cancellation fee.<br>
										If you cancel between 3 and 6 day(s) in advance of the scheduled departure, there is a 50 percent cancellation fee.<br>
										If you cancel within 2 day(s) of the scheduled departure, there is a 100 percent cancellation fee.</p>

										<p class="m-b-0 m-t-10"><strong>@lang('home.activity_label_15')</strong></p>
										<p>Click the link below to check pricing & availability on your preferred travel date. Our pricing is constantly updated to ensure you always receive the lowest price possible - we 100% guarantee it. Your currency is set to USD. Click here to change your currency.</p>
									<?php } ?>
									<?php if($data['departurePoint']){ ?> 
										<p class="m-b-0"><strong>@lang('home.activity_label_14')</strong></p>
										<p><?php echo $data['departurePoint']; ?></p>
									<?php } ?>

									<?php if($data['departureTime']){ ?> 
										<p class="m-b-0"><strong>@lang('home.activity_label_13')</strong></p>
										<p><?php echo $data['departureTime'].'<br>'.$data['departureTimeComments']; ?></p>
									<?php } ?>

									<?php if($data['duration']){ ?> 
										<p class="m-b-0"><strong>@lang('home.duration_text')</strong></p>
										<p><?php echo $data['duration']; ?></p>
									<?php } ?>

									<?php if($data['returnDetails']){ ?> 
										<p class="m-b-0"><strong>@lang('home.activity_label_12')asdasd</strong></p>
										<p><?php echo $data['returnDetails']; ?></p>
									<?php } ?>
								</div>
								<?php echo $selectDiv; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	 $(document).ready(function() {
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			margin: 10,
			loop: false,
			dots: true,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 3
				},
				1000: {
					items: 5
				}
			}
		})
	})
</script>