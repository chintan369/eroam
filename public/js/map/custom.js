$( window ).load(eMap.init);

$('.route-container').click(function(){
    var key = $(this).data('key');
    eMap.switchRoute(key);
});

$('.switch-manual-btn').click(function(event){
    event.preventDefault();
    eMap.switchToManual();
});

function openNav(idattr, event) {
    event.preventDefault();
    $('.routesidenav').show();
    $('.overlay_div').removeClass('d-none').addClass('d-block');
    $("#"+idattr).animate({marginLeft: '48px'});
}

function closeNav(idattr = null) {
    $('.overlay_div').removeClass('d-block').addClass('d-none');
    if (idattr == null) {
        $("#myroutSidenav").animate({marginLeft: '-350px'});
        $("#myroutSidenavtwo").animate({marginLeft: '-350px'});
    }else{
        $("#"+idattr).animate({marginLeft: '-350px'});
    }
    $('.routesidenav').hide();
}

$('#auto-sort a').click(function(e) {
    e.preventDefault();
    $(this).find('i').toggleClass('fa-toggle-off fa-toggle-on');
    var autoSort = $(this).find('i').attr('class') == 'fa fa-2x fa-toggle-off' ? 'off' : 'on';

    eroam.ajax('post', 'map/update', { auto_sort: autoSort }, function() {
        window.location.reload();
    });
});