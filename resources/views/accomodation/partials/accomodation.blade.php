<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="container-fluid">
    	@include('accomodation.partials.filters')
    	<div class="itinerary_page pt-2">
            <div class="container-fluid">
            	<div id="hotels-container1" class="loader_container">
	                <div id="hotel-loader">
	                    <div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.loading_hotel_text')  </div>
	                </div>
	            </div>
	            <div class="hotel-list-selected">
	            </div>
	            <div class="hotel-grid-selected" style="display: none;">
	            </div>
	            <div class="hotel-list1" id="listview">  
	            </div>
				<div class="hotel-list2 row" id="gridview" style="display: none;">  
	            </div>
            </div>
        </div>
    </div>
</div>