@extends('itinenary.booking-layout')

@section('booking-content')
@php
$total = $totalCost * $travellers;
$gst = $total * 0.025;
$finalTotal = number_format($total + $gst, 2, '.', ',');
$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
$travellers = session()->get('search')['travellers'];
$total_childs = session()->get('search')['child_total'];
$rooms = session()->get('search')['rooms'];
@endphp
<div class="itinerary_right">
       @include('itinenary.partials.sidebarstrip')
       <div class="container-fluid">
           @include('itinenary.partials.payment-filter-option')

           <div class="itinerary_page pt-2">
               <div class="container-fluid">
                   <div class="row">
                       <div class="col-12">
                           <div class=" m-1">
                               <div class="tripDetails mb-3">
                                   <div class="card panel border-0 rounded-0 p-3">
                                       <div class="row">
                                           <div class="col-sm-7 col-xl-8">
                                               <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                               <div class="media">
                                                   <i class="ic-calendar mr-2"></i>
                                                   <div class="media-body pb-3 mb-0">
                                                       {{$startDate}} - {{$endDate}}
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-sm-5 col-xl-4">
                                               <div class="row align-items-end">
                                                   <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                                       <ul class="list-unstyled mb-0">
                                                           <li class="list-inline-item">
                                                               <i class=" ic-local_hotel"></i>
                                                           </li>
                                                           <li class="list-inline-item">
                                                               <i class="ic-local_activity"></i>
                                                           </li>
                                                           <li class="list-inline-item">
                                                               <i class="ic-directions_bus"></i>
                                                           </li>
                                                       </ul>
                                                   </div>
                                                   <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                                       <h2 class="font-weight-bold mb-0">{{$totalDays}}</h2>
                                                       Nights
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                  @php
                  $i = 1;
                  $activities = '';
                  $transport = '';
                  $hotel = '';
                  $total_pax = session()->get('search')['travellers'];
                  $itineraryNumber = 0;
                  @endphp
                   <div class="row">
                       <div class="col-12">
                           <div class="save_itinerary  m-1">
                               <div class="itinerary_list p-3 pb-1">
                                @if (session()->get('search'))
                                  @foreach (session()->get('search')['itinerary'] as $key => $leg)
                                   <table class="table mb-0">
                                       <thead>
                                           <tr>
                                               <th scope="col" class="border-0" width="40%">{{$leg['city']['name']}}</th>
                                               <th scope="col" class="text-center border-0" width="20%">Cost</th>
                                               <th scope="col" class="text-center border-0" width="20%">Quantity</th>
                                               <th scope="col" class="text-right border-0" width="20%">Total Cost</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                        @if(isset($leg['hotel']) && !empty($leg['hotel']))
                                          @php 
                                            $leg['hotel'] = json_decode(json_encode($leg['hotel']), true);
                                            $singleRate = '@nightlyRateTotal';
                                            $singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];

                                            $singleRate = number_format(($singleRate * $eroamPercentage) / 100 + $singleRate, 2);
                                            $subTotal = $singleRate;
                                            $taxes = 0;

                                            $totalNights = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['NightlyRatesPerRoom']['@size'];

                                            if (isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])):
                                              $taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                                              $taxesPerDay = $taxes / $totalNights;
                                            endif;

                                            $ratePerDay = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageBaseRate'];
                                            $ratePerDay = number_format(($ratePerDay * $eroamPercentage) / 100 + $ratePerDay + $taxesPerDay, 2);

                                            $selectedRate = $subTotal + $taxes;
                                          @endphp
                                        
                                           <tr>
                                               <td>
                                                   <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
                                                   <div class="tour_info cityboxDetails">
                                                       <p><strong>{{ $leg['hotel']['name'] }}</strong></p>

                                                       <p> {{ $leg['hotel']['checkin'] }} - {{ $leg['hotel']['checkout'] }}<br/>
                                                      {{ $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'] }}<br/>

                                                      {{ ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['nonRefundable']) ? "Non-Refundable" : "" }}</p>
                                                   </div>
                                               </td>
                                               <td class="text-center">${{$currency}} {{$ratePerDay}}</td>
                                               <td class="text-center">
                                                   <div class="price">
                                                       <input class="form-control form-control-lg shadow-none rounded-0 text-center qty-inputControl" type="text" placeholder="" value="{{$total_pax}}"></div>
                                               </td>
                                               <td>
                                                   <div class="text-right">
                                                       <strong> ${{$currency}} {{$selectedRate}}</strong><br>
                                                       <a href="#">Change Dates</a><br>
                                                       @if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']) && ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'] != ""))
                                                       <a href="#" data-target="#cancellationPolicy{{$i}}" data-toggle="modal">Cancellation Policy</a><br/>
                                                       @endif 
                                                       <a href="{{ url('/remove-itinerary/'.$itineraryNumber.'/hotel/') }}">Remove From Itinerary</a>


                                                   </div>
                                               </td>
                                           </tr>
                                          @if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']) && ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'] != ""))
                                            <div class="modal fade in" id="cancellationPolicy{{$i}}" tabindex="-1" role="dialog" style="display: none;">
                                              <div class="modal-dialog modal-md" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
                                                    <h4 class="modal-title" id="gridSystemModalLabel">Cancellation Policy</h4>
                                                  </div>
                              
                                                    <div class="modal-body">
                                                        <div class="roomType-inner m-t-20">
                                                          <div class="m-t-20">
                                                            <p>{{$leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']}}</p>
                                                          </div>
                                     
                                                          <div class="m-t-30 text-right">
                                                            <a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
                                                          </div>
                                                        </div>
                                                      </div>
                                                  </div>
                                              </div> 
                                            </div>
                                          @endif
                                          <?php $i++;?>
                                        @endif
                                        @if(isset($leg['activities']) && !empty($leg['activities']))
                                        @php $j = 0;@endphp
                                          @foreach($leg['activities'] as $activity)
                                            <tr>
                                                <td>
                                                    <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
                                                    <div class="tour_info cityboxDetails">
                                                        <p><strong>{{$activity['name']}}</strong></p>

                                                        <p> {{date('d M Y', strtotime($activity['date_selected']))}}<br/>
                                                        Duration: {{$activity['duration1']}} (approx.)</p>
                                                    </div>
                                                </td>
                                                <td class="text-center">${{$currency}} {{$activity['price'][0]['price']}}</td>
                                                <td class="text-center">
                                                    <div class="price">
                                                        <input class="form-control form-control-lg shadow-none rounded-0 text-center qty-inputControl" type="text" placeholder="" value="1"></div>
                                                </td>
                                                <td>
                                                    <div class="text-right">
                                                        <strong> ${{$currency}} {{$activity['price'][0]['price']}}</strong><br>
                                                        <a href="#">Change Dates</a><br>
                                                        <a href="{{ url('/remove-itinerary/'.$itineraryNumber.'/activities/'.$j) }}">Remove From Itinerary</a>


                                                    </div>
                                                </td>
                                            </tr>
                                          @php $i++; $j++; @endphp
                                          @endforeach
                                        @endif

                                        @if(isset($leg['transport']) && !empty($leg['transport']))
                                          @php 
                                            $travelDate = explode('<br/>', $leg['transport']['departure_text']);
                                            $travelDate = explode('@', $travelDate[1]);
                                            $transport_price = '';
                                            if (isset($leg['transport']['price']['0']['price']) && !empty($leg['transport']['price']['0']['price'])) {
                                              $transport_price = number_format($leg['transport']['price']['0']['price'], 2);
                                            }
                                          @endphp
                                          <tr>
                                              <td>
                                                  <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
                                                  <div class="tour_info cityboxDetails">
                                                      <p><strong>{{$leg['transport']['supplier']['name']}}</strong></p>
                                                      <p> {{  date('d M Y', strtotime($travelDate[0])) }}<br/>Duration: {{ str_replace('+', '', $leg['transport']['duration']) }}</p>
                                                  </div>
                                              </td>
                                              <td class="text-center">${{$currency}} {{ $transport_price }}</td>
                                              <td class="text-center">
                                                  <div class="price">
                                                      <input class="form-control form-control-lg shadow-none rounded-0 text-center qty-inputControl" type="text" placeholder="" value="1"></div>
                                              </td>
                                              <td>
                                                  <div class="text-right">
                                                      <strong> ${{$currency}} {{$transport_price}}</strong><br>
                                                      <a href="#">Change Dates</a><br>
                                                      <a href="{{ url('/remove-itinerary/'.$itineraryNumber.'/transport/') }}">Remove From Itinerary</a>
                                                  </div>
                                              </td>
                                          </tr>

                                        @php $i++;@endphp
                                        @endif
                                      </tbody>
                                  </table>
                                  @php $itineraryNumber++; @endphp
                                  @endforeach
                                @endif
                                  <table class="table mb-0">
                                       <thead>
                                           <tr>
                                               <td>

                                                   <div class="pl-5">
                                                       <p> <a href="#">Save Itinerary</a></p>

                                                       <p> <a href="#">View Saved Itineraries</a></p>

                                                       <p> <a href="#">Enter Promo Code</a> </p>

                                                       <p class="pt-2">Contact Us <a href="#">+61 (0)3 9999 6774 </a></p>
                                                   </div>
                                               </td>
                                               <td colspan="2" class="text-right">
                                                   <p>Total Per Person</p>
                                                   <p>Sub Total Amount</p>
                                                   <p>Credit Card Fee 2.5%</p>
                                                   <p class="pt-2"><strong>Total Amount</strong></p>

                                               </td>

                                               <td class="text-right">
                                                   <p>${{$currency}} {{number_format($totalCost, 2, '.', ',')}}</p>
                                                   <p>${{$currency}} {{number_format($total, 2, '.', ',')}}</p>
                                                   <p>${{$currency}} {{number_format($gst, 2, '.', ',')}}</p>

                                                   <p class="pt-2"><strong>$AUD {{$finalTotal}}</strong></p>

                                               </td>
                                           </tr>

                                       </tbody>
                                   </table>
                                   <div class="p-2">
                                       <a href="{{url('/signin-guest-checkout/')}}" class="btn  btns_input_dark transform d-block w-100">CHECKOUT</a>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

@endsection

@push('scripts')
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush