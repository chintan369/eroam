<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use StdClass;
use App\Libraries\Filter;
use App\Libraries\EroamSession;
use App\Libraries\ApiCache;
use PDF;
use Mail;
use Cache;
use Validator;
use App\Libraries\Map;
use Carbon\Carbon;
use Config;
use Guzzle;
use Session;
use Redirect;


use App\Components\FlashMessages;

class ToursController extends Controller
{
	private $cities;
	private $countries;
	private $tourCount;
	private $headers = [];

	public function __construct() {
        $this->session = new EroamSession;
		$this->cities = Cache::get( 'cities' );
		$this->countries = Cache::get( 'countries' );
		$this->tourCount = countryTourCount();

		$this->headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];
	}

    public function search() {
        
        $countryName     = request()->has('project') ?  request()->input('project') : '' ;        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');
        $departure_date  = request()->input('departure_date');
        $tourType        = request()->input('tour_type');
        $countryIDRemove = [];

        $cType           = 0;
        if(is_array($tourType))
        {
            $cType       = count($tourType);
        }

	    
        if($tourType == 'single' || $tourType == 'multi'){	
            $tour_type = $tourType;
        }elseif($cType == 1){
            $tour_type = $tourType[0];
        }else{
            $tour_type = "both";
        }        

        $tourCounts = countryToursCount(date("Y-m-d", strtotime($departure_date)),$tour_type);

        $cData = array(
            'tour_type' => $tour_type
        );

        if(!empty($departure_date) && $departure_date!= '1970-01-01'){
            $cData = array(
                'date' => date("Y-m-d", strtotime($departure_date)),
                'tour_type' => $tour_type              
            );
        }        
        
        $tourRegions = http('post', 'getTourCountriesAvailables', $cData, $this->headers);
       
        if($search_type == 'city'){
            $city_info       = get_city_by_id($search_value);
            $cityIds[]       = $city_info['id'];
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name']; 
            $countryIDRemove = $countryIds;

            if(empty($city_info['country_id'])){
                $countryIds[]    = request()->input('countryId');
            }            
        }elseif($search_type == 'country'){
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
            foreach ($countryIds as $country_id) {                
                $city_info       = get_cities_by_country_id($country_id);
            }
            
            foreach ($city_info as $city) {
                $cityIds[]       = $city['id'];
            }

        }else{
            $RegionNames   = [];
            $regionIds     = [];
            $countryNames  = [];            
            $cityIds       = array_unique(request()->input('city')); 
            $countries     = request()->input('region');              
            
            if(empty(request()->input('region'))){
                foreach ($cityIds as $city) {
                    $city_info       = get_city_by_id($city);
                    $countryIds[]    = $city_info['country_id'];
                }
                $countryIds    = array_unique($countryIds);
            }else{
                $countryIds    = array_unique($countries);
            }
            
            foreach ($countryIds as $country_id) {
                $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
                $data       = $tourRegions[$search_arr];
                array_push($RegionNames,$data['rname']);
                array_push($regionIds,$data['region_id']);
                array_push($countryNames,$data['name']);                
            }
            $search_type   = 'city';
            $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        }

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = @$cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;        

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }        

        if(!empty(request()->input('from_page'))){       
            session()->forget('tourCountryData');
            $tourCountrydata[0] = $tourCountry; 
            $this->session->set_tourCountry_session($tourCountrydata);                
        }

        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);

        $cr = array_unique($countryIDRemove);

        return view(
            'layouts.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $tourCounts,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' => $totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,
                'cityIds'           => @$cityIds,
                'country_id_remove' => $cr,
                'remove_country_id' => implode("#",$cr),
                'country_tour_data' => tourCounts(date("Y-m-d", strtotime($departure_date)),$tour_type),
                'dep_date'          => date("Y-m-d", strtotime($departure_date)),
                'search_date'       => $departure_date,
                'country_id_remove_push'=> implode(",",$cr),
                'tour_type'         => $tour_type,
            ]
        );
    }

    public function set_session_tour()
    {
        $RegionNames   = [];
        $regionIds     = [];
        $countryNames  = [];     
        $cityIds       = array_unique(request()->input('modelcity'));
        $countries     = request()->input('modelRegion');
        $tourRegions = http('post', 'getTourCountriesAvailables', [], $this->headers);

        if(empty(request()->input('modelRegion')))
        {
            foreach ($cityIds as $city) {
                $city_info       = get_city_by_id($city);
                $countryIds[]    = $city_info['country_id'];
            }
            $countryIds    = array_unique($countryIds);
        }
        else
        {
            $countryIds    = array_unique($countries);
        }

        foreach ($countryIds as $country_id) {
            $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
            $data       = $tourRegions[$search_arr];
            array_push($RegionNames,$data['rname']);
            array_push($regionIds,$data['region_id']);
            array_push($countryNames,$data['name']);                
        }
        $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = @$cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;             
        $tourCountry['removeCountry']   = $countryIDRemove; 

        session()->forget('tourCountryData');
        $tourCountrydata[0] = $tourCountry; 
        $this->session->set_tourCountry_session($tourCountrydata);
        return \Response::json(['msg' => 'success']);
    }

	public function existsImage($uri = ''){
		
        if(request()->input('url'))
        {
            $uri = request()->input('url');
        }

	    $ch = curl_init($uri);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    return $code;
	}

	public function tourDetail(Request $request){
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		$id       = $request->id;
        $url      = $request->url;
        $default_selected_city = session()->get( 'default_selected_city' );
        
        $response = http('post', 'tourDetail', ['id' => $id, 'url' => $url,'default_selected_city'=>$default_selected_city], $this->headers);

		$data                               = $response['data'];
        $viatorDates                        = [];
        $start_date                         = '';

        if(strpos($response['data']['results'][0]['tour_code'],"VIAT") !== false) 
        {
            $start_date         = $request->start_date5;
            $month              = date("m");
            $year               = date("Y");
            if(isset($request->start_date5))
            {
                $month          = date("m", strtotime($request->start_date5));
                $year           = date("Y", strtotime($request->start_date5));
            }
            $alldata                   = [
                "productCode"   => $response['data']['results'][0]['code'],
                "month"         => $month,
                "year"          => $year,
                "currencyCode"  => "AUD"
            ];
            $activities                = http('post', 'tour/dates', $alldata);
            if(!empty($activities))
            {
                $viatorDates    = $activities['dates'];
            }
           
        }

		$data['detail']   	                = $response['data']['results'][0];
        $tourCountry['countryIds']          = [];
        $tourCountry['countryNames']        = [];
        $tourCountry['countryRegions']      = [];
        $tourCountry['countryRegionNames']  = [];
        $regionIds = '';

        $getAllDates = [];

        if($data['detail']['supplier_id'] == 2)
        {
            $path       = "tour_dossiers/".$data['detail']['code']."/departures";           
           
            $dates      = http_gad('get', $path);
          
            $allDates   = $dates['results'];
            $start      = strtotime('2015-12-25');
            
            foreach($allDates as $date) {
                $timestamp = strtotime($date['start_date']);        
                if($timestamp >= $start) {
                    array_push($getAllDates,$date);
                } 
            }
        }  


		if( session()->has('tourCountryData') ){
            $tourCountry = session()->get('tourCountryData');
            $tourCountry = reset( $tourCountry );
        }

        $countryIds = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];
        $regionIds = isset($tourCountry['regionIds']) ? $tourCountry['regionIds'] : [];

        $season = array();
        $t_return = array();
        

        $default_selected_city = session()->get( 'default_selected_city' );
        if(isset($data['tourFlightResult']) && !empty($data['tourFlightResult'])){
            foreach ($data['tourFlightResult'] as $key => $value) {
                if($default_selected_city == $value['flight_depart_city'] && $value['flightPrice'] > 0)
                {
                    $season[$value['season_id']] = $value['flightPrice'];
                    $t_return[$value['season_id']] = $value['flightReturn'];
                }
            }
        }

        $tourDate = 0;
        if(session()->has('tourDate') && session()->get('tourDate') == 'Yes') {  
          $tourDate = 1;
        }

        $tourSession = 0;
        if(session()->has('tourSession') && session()->get('tourSession') == 'Yes') {  
          $tourSession = 1;
        }

        $payError = 0; 
        if(Session::has('error')){
            $payError = 1;
        }

        $is_DateRadioIdValue = 0;
        if(session()->has('DateRadioIdValue')){ 
            $is_DateRadioIdValue = session()->get('DateRadioIdValue');
        }

        $allCountry = '';
        foreach ($data['countries'] as $country) {
            $allCountry .= ucwords(strtolower($country['country_name'])).', ';
        }
        $allCountry = substr($allCountry, 0,-2);

        $tripActivities = '';
        if(!empty($data['detail']['tripActivities'])){
            $tripActivities = explode(',',$data['detail']['tripActivities']);
            $tripActivities = $tripActivities[0];
        }

        $total_duration = ''; 
        if($data['detail']['durationType'] == 'd'){
            $total_duration     = 'Day';
            if(ceil($data['detail']['no_of_days']) > 1){
                $total_duration = 'Days';
            }
        }else if($data['detail']['durationType'] == 'h'){
            $total_duration     = 'Hour';
            if(ceil($data['detail']['no_of_days']) > 1){
                $total_duration = 'Hours';
            }
        } 

		return view(
			'pages.tourDetail',
			[
				'cities'       		=> $this->cities,
                'countries'    		=> $this->countries,
                'tourCount'			=> $this->tourCount,
                'countryName'  		=> '',//$countryName,
				'default_currency'  => 'AUD',
				'regionIds'  		=> $regionIds,
				'countryIds'  		=> $countryIds,
				'tourCountries'		=> $tourCountry,
				'url'				=> $url,
				'data'				=> $data,
                'page_will_expire'  => 1,
                'allCountry'        => $allCountry,
                'tripActivities'    => $tripActivities,
                'getAllDates'       => $getAllDates,
                'viatorDates'       => $viatorDates,
                'start_date'        => $start_date,
                'total_duration'    => $total_duration,
                'season'            => $season,
                't_return'          => $t_return
			]
		);	
	}

	public function payment(){
        if (!session()->has('initial_authentication')) {
            return redirect('app/login');
        }

        require base_path().'/vendor/autoload.php';
        require_once app_path() . '\eway-rapid-php-master\include_eway.php';
        $digits_needed=8;
        $random_number=''; // set up a blank string
        $count=0;
        while ( $count < $digits_needed ) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        $invoiceNumber = 'EROAM-'.$random_number;
        //Payment configuration start here
        //SANDBOX Credentials
        $apiKey = 'F9802Cyu/LaPEuVH6DayevjOq0xvO2Ppyf/qGM60sSJGOBiTLz2NZvK+D5ZpVV1eaFCxWY';
        $apiPassword = '9NAW7KKQ';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        //Production Credentials
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $name = request()->input()['first_name'].' '.request()->input()['last_name'];
        $find = array(",",".");
        $totalAmount = str_replace($find,'',request()->input()['totalAmount']);
        $transaction = [
            'Customer' => [
                'CardDetails' => [
                    'Name' => $name,
                    'Number' => request()->input()['card_number'],
                    'ExpiryMonth' => request()->input()['month'],
                    'ExpiryYear' => request()->input()['year'],
                    'CVN' => request()->input()['cvv'],
                ]
            ],
            'Payment' => [
                'TotalAmount' => $totalAmount,
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        ];
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
       if ($response->TransactionStatus === true) {
            
            $data['tour_id'] = request()->input()['tour_id'];
            $data['tour_title']   = '';//request()->input()['Title'];
            $data['Title']   = request()->input()['passenger_title'][1];
            $data['departure_date'] = request()->input()['departure_date'];
            $data['return_date'] = request()->input()['return_date'];
            $data['provider'] = request()->input()['provider'];
            $data['code'] = request()->input()['code'];
            $data['price'] = request()->input()['singleAmount'] + request()->input()['twinAmount'];
            $data['singletotal'] = request()->input()['singleAmount'];
            $data['twintotal'] = request()->input()['twinAmount'];
            $data['sub_total'] = request()->input()['totalAmount'];
            $data['grand_total'] = request()->input()['totalAmount'];
            $data['booking_currency'] = request()->input()['currency'];
            $data['FName']   = request()->input()['passenger_first_name'][1];
            $data['SurName'] = request()->input()['passenger_last_name'][1];
            $data['gender'] = request()->input()['passenger_gender'][1];
            $data['DOB'] = date('Y-m-d H:i:s', strtotime(request()->input()['passenger_dob'][1]));
            $data['contact'] =  request()->input()['passenger_contact_no'];
            $data['email'] = request()->input()['passenger_email'];
            $data['address'] = request()->input()['passenger_country'];
            $data['card_holder_name'] = request()->input()['first_name'].' '.request()->input()['last_name'];
            $data['card_holder_address'] = request()->input()['country'];
            $data['eway_authcode'] = $response->AuthorisationCode;
            $data['eway_transaction_no'] = $response->TransactionID;
            $data['eway_txn_number'] = $response->TransactionID;
            $data['credit_card_fee'] = request()->input()['creditCardFee'];
            $data['postal_code'] = request()->input()['postalcode'];
            $data['request_date'] = date('Y-m-d H:i:s');
            $data['status'] = 0; //Pending
            $data['Agent_id'] = 1;
            $data['payment_method'] = 1;
            $data['BookingType'] = 'Direct into account';
            $data['domain_id'] = 1;
            $data['addon_total_price'] = 0;
            $data['eway_result'] = 'Confirmed';
            $data['domain'] =  $_SERVER['SERVER_NAME'];


            
            $result = http('post', 'tourBooking', [$data], $this->headers);
            echo 'Payment successful! ID: '.$response->TransactionID;
            $mail_content = [
                'email' => $data['email'],
                'name' => ucfirst($data['FName']).' '.ucfirst($data['SurName']),
                'tour_title' => request()->input()['Title'],
                'booking_id' => $response->TransactionID,
                'total_pax' => request()->input()['NoOfPass'],
                'single_room_total' => $data['singletotal'],
                'twin_room_total' => $data['twintotal'],
                'total_amount' => $data['grand_total']
            ];
            $email = $data['email'];
            
            Mail::send(['html' => 'mail.booking'],$mail_content, function($message) use($email,$invoiceNumber){
                $message->to($email)->subject('Your eRoam Booking '.$invoiceNumber.'');
                $message->from('res@eroam.com');
            });
            return redirect('tourOrderSuccess');
        }else{
            session()->put('error', "Your Payment is declined. Please check your credit card details and try again." );
            session()->put('DateRadioIdValue', request()->input()['DateRadioIdValue']);
            return back();
        }
        exit;

    }
    public function orderSuccess(){

        return view('pages.success');
    }

    public function searchHotels(){   
        $api_key       = "y6de92swh5uw6akrufnaumgm";
        $shared_secret = "cshzMEXApR";
        $signature     = hash("sha256", $api_key.$shared_secret.time());
        $header        = [ 
            "Api-Key"      => $api_key,
            "X-Signature"  => $signature,
            "Content-Type" => "application/json",
            "Accept"       => "application/json",
            "Accept-Encoding" => "gzip"
            ];
        $url           = 'http://api.test.hotelbeds.com/hotel-api/1.0';
        $url_content   = 'https://api.test.hotelbeds.com/hotel-content-api/1.0/';           

        $body = '{"stay":{"checkIn":"2017-11-15","checkOut":"2017-11-18","shiftDays":3},"occupancies":[{"rooms":"1","adults":"2","children":"0",
        "paxes":[{"type":"AD","age":"30"},{"type":"AD","age":"30"}]}],"filter":{"maxHotels":20,"maxRooms":4,"maxRatesPerRoom":3,"minCategory":1,"maxCategory":5,"paymentType":"BOTH","maxRate":800},
        "destination":{"code":"SYD"}}';
        $url = $url.'/hotels';
        $method = 'post';
 

        $body_array                               = [];
        $body_array['stay']['checkIn']            = "2017-11-15";
        $body_array['stay']['checkOut']           = "2017-11-18";
        $body_array['stay']['shiftDays']          = 3;
        $body_array['occupancies'][0]['rooms']    = "1";
        $body_array['occupancies'][0]['adults']   = "2";
        $body_array['occupancies'][0]['children'] = "0";
        $body_array['occupancies'][0]['paxes'][0]['type'] = "AD";
        $body_array['occupancies'][0]['paxes'][0]['age']  = "30";
        $body_array['occupancies'][0]['paxes'][1]['type'] = "AD";
        $body_array['occupancies'][0]['paxes'][1]['age']  = "30";
        $body_array['filter']['maxHotels']       = 20;
        $body_array['filter']['maxRooms']        = 4;
        $body_array['filter']['maxRatesPerRoom'] = 3;
        $body_array['filter']['minCategory']     = 1;
        $body_array['filter']['maxCategory']     = 5;
        $body_array['filter']['paymentType']     = "BOTH";
        $body_array['filter']['maxRate']         = 800;
        $body_array['destination']['code']       = "SYD";
        $body = json_encode($body_array);


        $endpoint = "https://api.test.hotelbeds.com/hotel-api/1.0/hotels";

        
        // Example of call to the API
        try
        {   
            // Get cURL resource
            $curl = curl_init();
            // Set some options 
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $endpoint,
                CURLOPT_HTTPHEADER => ['Content-Type:application/json' , 'Accept:application/json' ,'Accept-Encoding:gzip' , 'Api-key:'.$api_key.'', 'X-Signature:'.$signature.'']
            ));

            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Check HTTP status code
            if (!curl_errno($curl)) {
                switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                    case 200:  # OK
                        echo "Server JSON Response:";

                        $resp = json_decode($resp);
                        echo '<pre>'; print_r($resp->hotels->hotels);
                        break;
                    default:
                        echo 'Unexpected HTTP code: ', $http_code, "\n";
                        echo $resp;
                }
            }
            // Close request to clear up some resources
            curl_close($curl);
        } catch (Exception $ex) {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }

        die;
    }

    public function sendTourEnquiry( Request $request ){

        $firstname= $request->input('firstname');
        $lastname = $request->input('lastname');
        $email    = $request->input('email');
        $phone    = $request->input('phone');
        $tourname = $request->input('tourname');
        $dates    = $request->input('dates');
        $enquiryContent = [
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'phone' => $phone,
                'tourname' => $tourname,
                'dates' => $dates,
            ];

            Mail::send(['html' => 'mail.TourEnquiry'],$enquiryContent, function($message){
                $message->to('rekha@corigami.com.au')->subject('Contact Us');
                $message->from('andy@corigami.com.au');
            });

            return 1;
    }

    public function tourPayment(){
        /*if (!session()->has('initial_authentication')) {
             return redirect('app/login');
        }*/
        require base_path().'/vendor/autoload.php';
        require_once app_path() . '\eway-rapid-php-master\include_eway.php';
        //echo "<pre>"; print_r(request()->input()); //die;
        //generate random Invoice Number
        $digits_needed=8;
        $random_number=''; // set up a blank string
        $count=0;
        while ( $count < $digits_needed ) {
            $random_digit = mt_rand(0, 9);
            $random_number .= $random_digit;
            $count++;
        }
        $invoiceNumber = 'EROAM-'.$random_number;
         //Payment configuration start here
        //SANDBOX Credentials
        $apiKey = 'F9802Cyu/LaPEuVH6DayevjOq0xvO2Ppyf/qGM60sSJGOBiTLz2NZvK+D5ZpVV1eaFCxWY';
        $apiPassword = '9NAW7KKQ';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        //Production Credentials
        //$apiKey = 'A1001Af7PV+/+3/402NLEV7Ftr/0dvVvxKqhScRUc6BcWEt/M=';
        //$apiPassword = 'Ra3UVVdX';
        //$apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION;
         //$apiEndpoint = 'https://api.ewaypayments.com/encrypt';
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $name = request()->input()['first_name'].' '.request()->input()['last_name'];
        $find = array(",");
        $totalAmount = str_replace($find,'',request()->input()['totalAmount']);
        $amount = explode(".", $totalAmount);
        $totalAmount = $amount[0];
        $expiration = explode("-",request()->input()['expiration_date']);
        $exp_month  = $expiration[0];
        $exp_year   = $expiration[1];
         
         
        $transaction = [
            'Customer' => [
                'CardDetails' => [
                    'Name' => $name,
                    'Number' => request()->input()['card_number'],
                    //'Number' => '4111111111111111',
                    'ExpiryMonth' => $exp_month,
                    'ExpiryYear' => $exp_year,
                    //'CVN' => request()->input()['EWAY_CARDCVN'],
                    'CVN' => request()->input()['cvv'],
                ]
            ],
            'Payment' => [
                'TotalAmount' => $totalAmount,
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        ];
         
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
         
        if ($response->TransactionStatus === true) {

            $data['tour_id'] = request()->input()['tour_id'];
            $data['tour_title']   = '';//request()->input()['Title'];
            //$data['Title']   = request()->input()['passenger_title'][1];
            $data['departure_date'] = date("Y-m-d H:i:s",strtotime(request()->input()['departure_date']));
            $data['return_date'] = date("Y-m-d H:i:s",strtotime(request()->input()['return_date']));
            $data['provider'] = request()->input()['provider'];
            $data['code'] = request()->input()['code'];
            $data['price'] = request()->input()['singleAmount'] + request()->input()['twinAmount'];
            $data['singletotal'] = request()->input()['singleAmount'];
            $data['twintotal'] = request()->input()['twinAmount'];
            $data['sub_total'] = request()->input()['totalAmount'];
            $data['grand_total'] = request()->input()['totalAmount'];
            $data['booking_currency'] = request()->input()['currency'];
            $data['FName']   = request()->input()['passenger_first_name'];
            $data['SurName'] = request()->input()['passenger_family_name'];
            //$data['gender'] = request()->input()['passenger_gender'][1];
            //$data['DOB'] = date('Y-m-d H:i:s', strtotime(request()->input()['passenger_dob'][1]));
            $data['contact'] =  request()->input()['passenger_contact_no'];
            $data['email'] = request()->input()['passenger_email'];
            //$data['address'] = request()->input()['passenger_country'];
            $data['card_holder_name'] = request()->input()['first_name'].' '.request()->input()['last_name'];
            //$data['card_holder_address'] = request()->input()['country'];
            $data['eway_authcode'] = $response->AuthorisationCode;
            $data['eway_transaction_no'] = $response->TransactionID;
            $data['eway_txn_number'] = $response->TransactionID;
            $data['credit_card_fee'] = request()->input()['creditCardFee'];
            $data['postal_code'] = request()->input()['billing_postcode'];
            $data['billing_first_name'] = request()->input()['billing_firstname'];
            $data['billing_family_name'] = request()->input()['billing_familyname'];
            $data['billing_email'] = request()->input()['billing_email'];
            $data['billing_contact'] = request()->input()['billing_contact_no'];
            $data['billing_company'] = request()->input()['billing_company'];
            $data['billing_street_addr'] = request()->input()['billing_street'];
            $data['billing_additional_addr'] = request()->input()['billing_additional'];
            $data['suburb'] = request()->input()['billing_suburb'];
            $data['state'] = request()->input()['billing_state'];
            $data['country'] = request()->input()['billing_country'];

            $data['request_date'] = date('Y-m-d H:i:s');
            $data['status'] = 0; //Pending
            $data['Agent_id'] = 1;
            $data['payment_method'] = 1;
            $data['BookingType'] = 'Direct into account';
            $data['domain_id'] = 1;
            $data['addon_total_price'] = 0;
            $data['eway_result'] = 'Confirmed';
            $data['domain']     =  $_SERVER['SERVER_NAME'];
            $data['user_id']    =  request()->input()['user_id'];;
            $add_data   = [];  
            
            if(isset(request()->input()['guest_type']))
            {
                $guest_type = request()->input()['guest_type'];
                 
                for ($i=0; $i < count($guest_type); $i++) { 
                    $j = $i+2;

                    $guest_title        = "";
                    $guest_email        = "";
                    $guest_contact_no   = "";
                    $child_age          = "";

                    if($guest_type[$i] == "adult")
                    {
                        $guest_title        = request()->input()['guest_title_'.$j];
                        $guest_email        = request()->input()['guest_email_'.$j];
                        $guest_contact_no   = request()->input()['guest_contact_no_'.$j];
                    }
                    if($guest_type[$i] == "child")
                    {
                        $child_age          = request()->input()['guest_cild_age_'.$j];
                    }

                    $add_data[$i]['tour_id'] = request()->input()['tour_id'];
                    $add_data[$i]['type'] = $guest_type[$i];
                    $add_data[$i]['title'] = $guest_title;
                    $add_data[$i]['first_name'] = request()->input()['guest_firstname_'.$j];
                    $add_data[$i]['family_name'] = request()->input()['guest_familyname_'.$j];
                    $add_data[$i]['email'] = $guest_email;
                    $add_data[$i]['contact'] = $guest_contact_no;
                    $add_data[$i]['child_age'] = $child_age;
                     
                 }
             }
             //dd($data);
             $allData = [
                     'tour_all_data' => $data,
                     'additional_person' => $add_data
                 ];


            $result = http('post', 'tourBookingNew', [$allData], $this->headers);
           
            echo 'Payment successful! ID: '.$response->TransactionID;
            $mail_content = [
                'email' => $data['email'],
                'name' => ucfirst($data['FName']).' '.ucfirst($data['SurName']),
                'tour_title' => request()->input()['Title'],
                'booking_id' => $response->TransactionID,
                'total_pax' => request()->input()['NoOfPass'],
                'single_room_total' => $data['singletotal'],
                'twin_room_total' => $data['twintotal'],
                'total_amount' => $data['grand_total']
                 //'twin_qty' => request()->input()['NoOfPass'] - request()->input()['NoOfSingleRoom'],
                 //'single_qty' => request()->input()['NoOfSingleRoom']
            ];
            
            $email = $data['email'];
             
            Mail::send(['html' => 'mail.booking'],$mail_content, function($message) use($email,$invoiceNumber){
                $message->to($email)->subject('Your eRoam Booking '.$invoiceNumber.'');
                $message->from('res@eroam.com');
            });
            return redirect('tourOrderSuccess');
        }else{           
            session()->put('error', "Your Payment is declined. Please check your credit card details and try again." );
            session()->put('DateRadioIdValue', request()->input()['DateRadioIdValue']);
            return back();

            //Redirect::back();
        }
        exit;
    }

    
}
