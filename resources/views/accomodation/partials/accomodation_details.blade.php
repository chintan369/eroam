<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    @php 
    	$selectedPrice = 0;
	  	$hotelID = 0;
	  	$rateCode = 0;
	  	$search_session = json_decode(json_encode( session()->get( 'search' ) ) , FALSE );
	  	$travellers = $search_session->travellers;
	  	$searchRooms = $search_session->rooms;
	  	if($data->provider == 'expedia'){
	    	$total = '@total';
	    	if($search_session->itinerary[$data->leg]->hotel){
	      		$selectedPrice = ceil($search_session->itinerary[$data->leg]->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$total);
	      		$hotelID = $search_session->itinerary[$data->leg]->hotel->hotelId;
	      		$rateCode = ceil($search_session->itinerary[$data->leg]->hotel->RoomRateDetailsList->RoomRateDetails->rateCode);
	    	}
	  	}
	  
	  	$surcharge_components = array(
	    	'TaxAndServiceFee'=>'Tax & Service Fee',
	    	'ExtraPersonFee'=>'Extra Person Fee',
	    	'Tax'=>'Tax',
	    	'ServiceFee'=>'Service Fee',
	    	'SalesTax'=>'Sales Tax',
	    	'HotelOccupancyTax'=>'Hotel Occupancy Tax',
	    	'PropertyFee'=>'Property Fee'
	    );

	  	$i = 0;

	  	if(!isset($data->HotelRoomResponse->rateCode)){
	    	$temp_array = $data->HotelRoomResponse;
	     	foreach ($data->HotelRoomResponse as $key => $value) {
		      	if($value->rateCode == $rateCode){
		        	if($key != 0){
		          		$temp_array[$i] = $data->HotelRoomResponse[$key];
		          		$temp_array[$key] = $data->HotelRoomResponse[$i];
		        	}
		      	}
	    	}
	    	$data->HotelRoomResponse = $temp_array;
	  	}else{
	    	$temp_array[0] = $data->HotelRoomResponse;
	    	$data->HotelRoomResponse = $temp_array;
	  	}
        
        $travellers = session()->get( 'search' )['travellers'];   
        $total_childs = session()->get( 'search' )['child_total']; 
        $rooms = session()->get( 'search' )['rooms']; 
        $room_text = "Room";

        $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
		$nights = 0;

		if (session()->get('search')){
		    $nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
        }

        if($rooms > 1){
        	$room_text = "Rooms";
        }
    @endphp
    <div class="container-fluid">
    	<div class="pl-5">
            <div class="accommodation_top pt-4 pb-3 border-bottom">
    			<div class="row">
        			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <a href="{{ url( $data->searchCity.'/hotels?leg='.$data->leg ) }}"><i class="ic-navigate_before"></i> <strong>@lang('home.accommodation_back_link')</strong></a>
        			</div>
        			<div class="col-12 col-sm-12 col-md-6 col-lg-6 text-sm-left text-lg-right">
            			<i class="ic-place"></i>{{$data->searchCity}}, {{$data->country_name}}: {{date('l d M Y',strtotime($data->arrivalDate))}} - {{date('l d M Y',strtotime($data->departureDate))}} ({{$rooms}} {{$room_text}})
        			</div>
    			</div>
            </div>
            <div class="hotel_listtitle mt-4">
                <div class="hotlename pb-2   mb-3 ">
                    <div class="row">
                        <div class="col-12 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                            <div class="cat_name"> {{$data->hotelName}} </div>
                            <div class="pt-3"><i class="ic-place"></i> @lang('home.location'): {{$data->country_name}}, {{$data->searchCity}} / {{$data->hotelCity}}</div>
                        </div>
                        <div class="col-12 col-sm-8 col-md-6 col-lg-6 col-xl-6">
                            <div class="float-right">
                                <div class="night_charge">
                                    <p class="text-right pr-2 pt-5 mb-0 "> @lang('home.activity_label8')<strong> $AUD {{$data->hotelPrice}} </strong> P.P (Per Night)</p>
                                </div>
                                <div class="dayes text-center border-left ">
                                    <span> {{$nights}}</span> {{($nights > 1 ? 'Nights':'Night')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel owl-theme">
                    	@if($data->HotelImages->HotelImage)
                    		@if(is_array($data->HotelImages->HotelImage))
		                     	@foreach ($data->HotelImages->HotelImage as $key => $value)
		                        	@if ($key <= 30)
				                        <div class="item"><img src="{{$value->url}}" alt=" " /> </div>
		                        	@endif
		                      	@endforeach
                    		@else
                    			<div class="item"><img src="{{$data->HotelImages->HotelImage->url}}" alt=" " /> </div>
                    		@endif
	                    @endif
                    </div>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 accommodation_tabs">
                    <ul class="nav nav-tabs border-0 justify-content-md-center" id="myTab" role="tablist">
                        <li class="nav-item pr-2  text-center">
                            <a class="nav-link border-0  text-uppercase" id="home-tab" data-toggle="tab" href="#room_rates" role="tab" aria-controls="room_rates" aria-selected="true"> @lang('home.accommodation_heading1')  </a>
                        </li>
                        <li class="nav-item pr-2 pl-2 text-center">
                            <a class="nav-link border-0 active text-uppercase" id="profile-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="false">@lang('home.overview_text_heading')</a>
                        </li>
                        <li class="nav-item  pl-2 text-center">
                            <a class="nav-link border-0 text-uppercase" id="contact-tab" data-toggle="tab" href="#infomportant" role="tab" aria-controls="infomportant" aria-selected="false">@lang('home.activity_tab1')</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12">
                    <div class="tab-content accommodation_tabs_content pb-5" id="myTabContent">
                        <div class="tab-pane fade" id="room_rates" role="tabpanel" aria-labelledby="home-tab">
                            <div class="pt-3 pt-5 pb-5 ">
                                <div class="row room_rates_filter_block">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-10">
                                        <div class="row">
                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.create_itenerary_checkin')</label>
                                                    <input type="text" class="form-control disabled" id="checkin-date" value="{{date('d M Y',strtotime($data->arrivalDate))}}" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                                    <span class="arrow_down"><i class="ic-calendar"></i> </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.map_label_13')</label>
                                                    <input type="text" class="form-control disabled" id="checkout-date" value="{{date('d M Y',strtotime($data->departureDate))}}" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                                    <span class="arrow_down"><i class="ic-calendar"></i> </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-4 col-xl-2">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.map_label_14')</label>
                                                    <div class="custom-select">
                                                        <select class="disabled" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
					                                         <option value="{{$rooms}}">{{$rooms}}</option>
					                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-4 col-xl-2">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.adults')</label>
                                                    <div class="custom-select">
                                                        <select class="disabled" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                         					<option value="{{$travellers}}">{{$travellers}}</option>
				                                      	</select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-4 col-xl-2">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.childrens')</label>
                                                    <div class="custom-select">
                                                        <select class="disabled" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                                         	<option value="{{$total_childs}}">{{$total_childs}}</option>
                                                     	</select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-2">
                                        <div class="row">
                                            <div class="form-group col-6 col-sm-4 col-md-3 col-lg-3 col-xl-12">
                                                <button type="button" class="btn  btns_input_dark transform d-block w-100">@lang('home.profile_update_text_small')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="accordion accommodation_accordion" id="accordionExample">
                                        	@if($data->provider=="expedia")
							                    @if($data->HotelRoomResponse)
							                    	@foreach($data->HotelRoomResponse as $key => $RoomResponse)
							                    		@php
								                        	$singleRate = '@nightlyRateTotal';
								                        	$singleRate = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$singleRate;
								                        	$singleRate = round(($singleRate * $eroamPercentage) / 100 + $singleRate,2); 
								                        	$taxes = $RoomResponse->RateInfos->RateInfo->taxRate;
								                        	$total = $singleRate + $taxes;
								                        	$displaySymbol = '';
								                        	$disabled_status = '';

								                          	if($rateCode == $RoomResponse->rateCode){
								                            	$displaySymbol = '+';
								                            	$differencePrice =0;
								                            	$disabled_status = 'disabled';
								                          	}else{
									                            $differencePrice = abs($total - $selectedPrice);
									                            if($total < $selectedPrice){
									                            	$displaySymbol ='-';
									                            }else{
									                            	$displaySymbol = '+';
									                            }
								                          	}
								                          
								                          	$currencyCode = '@currencyCode';
								                          	$currencyCode = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$currencyCode;
								                        @endphp
			                                            <div class="card rounded-0 border-0 mt-3">
			                                                <div class="card-header border-0 rounded-0 " id="headingTwo">
			                                                    <div data-toggle="collapse" data-target="#collapseTwohotel{{$key}}" aria-expanded="false" aria-controls="collapseTwohotel{{$key}}">
			                                                        <div class="row">
			                                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
			                                                                <div class="thumb">
			                                                                    <img src="images/room_thumb.png" alt="" />
			                                                                </div>
			                                                                <div class="roomes">
			                                                                    <strong class="d-inline-block pb-1">{{$RoomResponse->rateDescription}}</strong>
			                                                                    <p class="mb-0">Room sleeps {{$RoomResponse->rateOccupancyPerRoom}} Guests. {{($RoomResponse->RateInfos->RateInfo->nonRefundable == 0 ? 'Refundable':'Non-Refundable')}}</p>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
			                                                                <div class="text-right pricebox">
			                                                                    <div class="price">$AUD {{number_format($total / $travellers,2)}}</div>
			                                                                    <p>Per Person For {{$nights}} {{($nights > 1 ? 'Nights':'Night')}}</p>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                                <div id="collapseTwohotel{{$key}}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
			                                                    <div class="card-body mb-2">
			                                                        <div class="row">
			                                                            <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6">
			                                                                @php
										                                    	$nights = 0;
										                                    @endphp
										                                    @if (session()->get('search'))
										                                    	@php
										                                        	$nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
										                                      	@endphp
										                                    @endif
			                                                                <ul class="deluxe_room">
			                                                                    <li><strong class="d-inline-block">  @lang('home.proposed_itinerary_label2') :</strong> {{date('d M Y',strtotime($data->arrivalDate))}}</li>
			                                                                    <li><strong class="d-inline-block">@lang('home.proposed_itinerary_label3') :</strong> {{date('d M Y',strtotime($data->departureDate))}} </li>
			                                                                    <li><strong class="d-inline-block">@lang('home.duration_text') :</strong> {{$nights}} {{($nights > 1 ? 'Night(s)':'Night')}} </li>
			                                                                </ul>
			                                                                <?php /*?><p class="mt-3">@lang('home.room_sleep_text')  {{$RoomResponse->rateOccupancyPerRoom}} @lang('home.guests_text') .<br/>{{($RoomResponse->RateInfos->RateInfo->nonRefundable == 0 ? 'Refundable':'Non-Refundable')}}</p><?php */?>
			                                                                <p>{!!html_entity_decode($RoomResponse->RoomType->descriptionLong)!!}</p>
			                                                            </div>
			                                                            <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6">
			                                                                <table class="table borderless">
			                                                                    <thead>
			                                                                        <tr>
			                                                                            <th scope="col"><strong> @lang('home.guests_text') :</strong> </th>
			                                                                            <th scope="col" class="text-right"><strong> {{$travellers}} x @lang('home.adult_text_label') </strong></th>
			                                                                        </tr>
			                                                                    </thead>
			                                                                    <tbody>
			                                                                    	@php
												                                    	$NightlyRatesPerRoom = json_decode(json_encode($RoomResponse),true);
												                                        if(isset($NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'])){
												                                            if(isset($NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'][0])){
												                                            	$temp_rate = $NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'];
												                                            }else{
												                                            	$temp_rate[0] = $NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'];
												                                            }
												                                            foreach($temp_rate as $chargeKey => $charges){
												                                              	$rate = '@rate';
												                                              	$chargesRate = 0;
												                                              	if(isset($charges['ChargeableNightlyRates'][0])){
												                                                	foreach($charges['ChargeableNightlyRates'] as $c){
												                                                    	$chargesRate = $chargesRate + $c[$rate];
												                                                  	}
												                                              	}else{
												                                                 	$chargesRate = $charges['ChargeableNightlyRates'][$rate];
												                                              	}
												                                             
												                                              	$chargesRate = round(($chargesRate * $eroamPercentage) / 100 + $chargesRate,2); 
												                                              	@endphp
												                                              	<tr>
												                                               		<td><strong>@lang('home.room_text_label') - {{$chargeKey+1}}:</strong></td>
												                                                	<td class="text-right">$AUD <span class="d-inline-block">{{$chargesRate}}</span></td>
												                                              	</tr>
												                                              	@php 
												                                            }
												                                        }
												                                    @endphp
			                                                                        <tr>
			                                                                            <td><strong> @lang('home.sub_total_text'):</strong></td>
			                                                                            <td class="text-right">$AUD <span class="d-inline-block">{{$singleRate}}</span>
			                                                                            </td>
			                                                                        </tr>
			                                                                        @php
												                                    	$sur_charges = json_decode(json_encode($RoomResponse),true);
												                                        if(isset($sur_charges['RateInfos']['RateInfo']['ChargeableRateInfo']['Surcharges'])){
												                                            $type = '@type';
												                                            $amount = '@amount';
												                                            $size = '@size';
												                                            
												                                            $temp_surcharge = $sur_charges['RateInfos']['RateInfo']['ChargeableRateInfo']['Surcharges']['Surcharge'];
												                                            if(!isset($temp_surcharge[0])){
												                                            	$tempSurcharge[0] = $temp_surcharge;
												                                            	unset($temp_surcharge);
												                                            	$temp_surcharge = $tempSurcharge;
												                                            }
												                                            foreach($temp_surcharge as $charges){
												                                            	@endphp
												                                              	<tr>
												                                              		<td><strong>{{(isset($surcharge_components[$charges[$type]]) ? $surcharge_components[$charges[$type]]:'')}}:</strong></td>
												                                              		<td class="text-right">$AUD <span class="d-inline-block">{{$charges[$amount]}}</span></td>
												                                              	</tr>
												                                              	@php 
												                                            }
												                                        }else{
												                                        @endphp
												                                          	<tr>
												                                              	<td><strong>Taxes:</strong></td>
												                                              	<td class="text-right">$AUD <span class="d-inline-block">{{$taxes}}</span></td>
												                                          	</tr> 
												                                          	@php
												                                        }
												                                    @endphp
			                                                                    </tbody>
			                                                                </table>
			                                                                <div class="price_outer">
			                                                                    <div class="price text-right">
			                                                                        $AUD {{$total}}
			                                                                    </div>
			                                                                    <p class="text-right pt-3">{{$nights}} {{($nights > 1 ? 'Nights':'Night')}}<br/>Rates are quoted in Australian Dollars.</p>
			                                                                </div>
                                  											<button {{$disabled_status}} type="button" name="" class="btn btn-secondary active btn-block m-t-20 selected-room" data-provider="{{$data->provider}}" data-hotel="{{$data->hotelId}}" data-room="{{json_encode($RoomResponse)}}">@lang('home.add_to_itinerary_link')</button>
			                                                            </div>
			                                                        </div>
			                                                        <div class="row">
			                                                            <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-8">
			                                                            	<strong><a href="#" data-toggle="modal" data-target="#roomTypeModal{{$key}}" class="more-info moreInfo"><i class="fa fa-info-circle"></i> @lang('home.more_detail_text')</a></strong>
			                                                            </div>
			                                                        </div>
			                                                        <div class="modal fade" class="roomTypeModal" id="roomTypeModal{{$key}}" tabindex="-1" role="dialog">
																	    <div class="modal-dialog modal-md" role="document">
																	        <div class="modal-content">
																	            <a href="#" class="modal-close" data-dismiss="modal">x</a>
																	            <div class="connected-carousels">
																	                <div class="stage">
																	                    <div class="carousel carousel-stage">
																	                        <ul>
																	                            @if($data->HotelImages->HotelImage)
																	                            @foreach ($data->HotelImages->HotelImage as $key => $value)
																	                            @if ($key <= 30)
																	                            <li><img src="{{$value->url}}" alt="" width="600" height="400"></li>
																	                            @endif
																	                            @endforeach
																	                            @endif
																	                        </ul>
																	                    </div>
																	                </div>
																	                <div class="navigation">
																	                    <a href="#" class="prev prev-navigation">&lsaquo;</a>
																	                    <a href="#" class="next next-navigation">&rsaquo;</a>
																	                    <div class="carousel carousel-navigation">
																	                        <ul>
																	                            @if($data->HotelImages->HotelImage)
																	                            @foreach ($data->HotelImages->HotelImage as $key => $value)
																	                            @if ($key <= 30)
																	                            <li><img src="{{$value->url}}" alt="" width="80" height="80"></li>
																	                            @endif
																	                            @endforeach
																	                            @endif
																	                        </ul>
																	                    </div>
																	                </div>
																	            </div>
																	            <div class="modal-body">
																	                <div class="roomType-inner m-t-20">
																	                    <div class="m-t-20">
																	                        <p>{!!html_entity_decode($RoomResponse->RoomType->descriptionLong)!!}</p>
																	                    </div>
																	                    <div class="m-t-30 text-right">
																	                        <a href="#" data-dismiss="modal" class="modalLink-blue">@lang('home.close_link') </a>
																	                    </div>
																	                </div>
																	            </div>
																	        </div>
																	    </div>
																	</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        @endforeach
                    							@endif
                  							@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="locations_map pt-3 pt-5 pb-5 ">
                                    	<div id="map1" style="height: 400px; width:100%; position: relative !important;" class="map_detail"></div>
                    					<input id="pac-input" class="form-control full-width" type="text" readonly data-lat="{{ $data->latitude != '' ? $data->latitude : '' }}" data-lng="{{ $data->longitude != '' ? $data->longitude : '' }}" value="{{ $data->google_map_location != '' ? $data->google_map_location : '' }}" placeholder="Search Box" style="display:none;"> 
                                    	<div id="infowindow-content">
					                    	<img src="" width="16" height="16" id="place-icon">
					                    	<span id="place-name" class="place-title"></span><br>
					                    	<span id="place-address"></span>
					                    </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-12">
                                	@if(isset($data->HotelDetails->locationDescription))
	                                    <div class="pt-3">
	                                        <strong> Location</strong>
						                    <p>{{$data->HotelDetails->locationDescription}}</p>
	                                    </div>
				                    @else
				                      	<div class="pt-3">
	                                        <strong>@lang('home.no_data_text') </strong>
	                                    </div>
				                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="infomportant" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="pt-3 pt-5 pb-5">
                            	@if($data->provider=="expedia")
                                <p>{{$data->hotelName}} <br> {{$data->country_name}}, {{$data->searchCity}} / {{$data->hotelCity}} </p>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                        <div class="pb-4">
                                            <p class="mb-2"><strong>@lang('home.hotel_amenities_text') </strong></p>
                                            <div class="amenities">
                                                <ul class="pl-4 pt-0">
                                                	@if($data->PropertyAmenities->PropertyAmenity)
							                            @foreach ($data->PropertyAmenities->PropertyAmenity as $key => $value)
							                               @if ($key % 2 == 0)
							                                <li>{{$value->amenity}}</li>
							                               @endif
							                            @endforeach
							                        @endif
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                                        @if($data->checkInInstructions)
                                        <div class="pb-4">
                                            <p class="mb-2"><strong>@lang('home.hotel_checkin_instruction_text') </strong></p>
                                            <p>
	                                    		@php
							                        $data->checkInInstructions = strstr($data->checkInInstructions, '<p><b>Fees</b>');
							                        $data->checkInInstructions = str_replace('<p><b>Fees</b>','<p><strong>Fees / Optional Extras</strong>',$data->checkInInstructions);
							                    @endphp
							                    {!!html_entity_decode($data->checkInInstructions)!!}
							                </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        @endif
                                        @if(isset($data->HotelDetails->diningDescription))
                                        <div class="pb-4">
                                            <p class="mb-2"><strong>@lang('home.hotel_detials_label1') </strong></p>
                                            <div class="amenities">
                                                <p>{{$data->HotelDetails->diningDescription}}</p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>