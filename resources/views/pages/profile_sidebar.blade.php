<?php $currentPath= Route::getFacadeRoot()->current()->uri();?>
<div class="page-sidebar-wrapper">
    <nav class="pushy pushy-left" style="visibility: visible;">
        <h2 class="profile-title">My Account</h2>
        <div class="pushy-height">
            <ul>
                <li <?php if($currentPath == 'profile/step1'){?> class="active" <?php }?>><a href="{{url('profile/step1')}}"><i class="icon-account"></i><span>Profile</span></a></li>
                <li <?php if($currentPath == 'profile/step2'){?> class="active" <?php }?>><a href="{{url('profile/step2')}}"><i class="icon-address"></i><span>Addresses</span></a></li>
                <li <?php if($currentPath == 'profile/step3'){?> class="active" <?php }?>><a href="{{url('profile/step3')}}"><i class="icon-fingerprint"></i><span>Personal Preferences</span></a></li>
                <li <?php if($currentPath == 'profile/step4'){?> class="active" <?php }?>><a href="{{url('profile/step4')}}"><i class="icon-email"></i><span>Contact Preferences</span></a></li>
                <li <?php if($currentPath == 'manage/trips'){?> class="active" <?php }?>><a data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot"><i class="icon-manage-trip"></i><span>Manage Trips</span></a></li>
                <!-- <li <?php if($currentPath == 'manage/trips'){?> class="active" <?php }?>><a href="{{url('manage/trips')}}"><i class="icon-manage-trip"></i><span>Manage Trips</span></a></li> -->
                <li><a href="#"><i class="icon-favorite-trip"></i><span>Saved Trips</span></a></li>
            </ul>
        </div>
    </nav>
</div>

   <!-- Modal -->
    <div class="modal fade" id="manualModeModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Manual Itinerary</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-4 popup-map">
                 <img src="{{ url( 'images/auto-map.jpg' ) }}" alt="" class="img-responsive" />
              </div>
              <div class="col-sm-8">
                
                <p>Manual Itinerary allows the user to plot a multi-city itinerary by clicking on an interactive map. As you add each new location ‘trending’ data will recommended the best tours, transport and hotels to suit ythe travellers profile. Once this itinerary has been created, you have the ability to edit any segment using the interactive map, or booking summary panel.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
