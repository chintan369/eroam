@extends( 'layouts.home' )
@section('content')
<div class="itinerary_block">
    <div class="container-fluid">
        <div class="row mt-4 tour-details">
            <div class="col-xl-10 offset-xl-1">
                <form class="form-horizontal m-t-20" action="{{ url('tour/payment') }}" method="post" name="tour_form" id="tour_form_id"> 
                                {{ csrf_field() }}
                <div class="card panel p-3 mb-3">
                    <div class="row">
                        <div class="col-sm-8 col-xl-9">                            
                                <h5 class="font-weight-bold">{{$data['tour_name']}}</h5>
                                <div class="media">
                                    <img src="{{ url('images/tour.svg') }}">
                                    <div class="media-body pb-3 mb-0">                  
                                        {{date('d F Y', strtotime($data['start_date']))}} - 
                                        {{date('d F Y', strtotime($data['finishDate']))}}
                                    </div>
                                </div>
                        </div>
                        <div class="col-sm-4 col-xl-3 text-left text-sm-right mb-3 mb-sm-0">
                            <h2 class="font-weight-bold mb-0">{{$data['days']}}</h2>
                            Days
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <div class="fildes_outer">
                                <label>No. of Passengers</label>
                                <div class="custom-select">
                                    <select class="form-control" name="NoOfPass" id="NoOfPass">
                                      @for($i=1;$i<=9;$i++)
                                      <option value="{{$i}}">{{$i}}</option>
                                      @endfor
                                    </select>
                                </div>
                            </div>
                            <small>&nbsp;</small>
                        </div>
                        @if($data['from'] == '')
                        <div class="col-sm-3  mb-3 mb-sm-0">
                            <div class="fildes_outer">
                                <label>Single Room</label>
                                <select class="form-control" id="NoOfSingleRoom" name="NoOfSingleRoom" <?php if($data['no_of_days'] == 1 || $data['durationType'] == 'h'){ ?>disabled<?php } ?>>
                                    <option value="">Single Room</option>
                                    <option value="1" selected>1</option>
                                </select>
                            </div>
                            <label for="NoOfSingleRoom" generated="true" class="error"></label>
                            <small class="select-note" id="SinglePricePP">${{ $data['default_currency'].' '.number_format($data['singlePrice'],2)}} (Per Person)</small>
                        </div>
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <div class="fildes_outer">
                                <label>Twin Room</label>
                                <select class="form-control disabled" id="NoOfTwinRoom" name="NoOfTwinRoom" readonly disabled>
                                  <option value="">Twin Room</option>
                                  <option value="0" selected>0</option>
                                </select>
                            </div>
                            <label for="NoOfTwinRoom" generated="true" class="error"></label>
                            <small class="select-note" id="pricePP">${{ $data['default_currency'].' '.$data['twinPrice']}} (Per Person)</small>
                        </div>
                        @endif
                        <div class="col-sm-3 mt-3">
                            <strong>Total Amount</strong>  <span id="TotalPricePP">${{ $data['default_currency'].' '.number_format($data['singlePrice'],2)}}</span>
                        </div>
                    </div>
                    <input type="hidden" value="<?php if(session()->has('DateRadioIdValue')){ echo session()->get('DateRadioIdValue'); }?>" name="DateRadioIdValue" id="DateRadioIdValue"> 
                    <input type="hidden"  name="singlePrice" id="singlePrice" value="{{$data['singlePrice'
                    ]}}" >
                    <input type="hidden" class="dateRedio" name="price" id="radio-d" value="{{$data['twinPrice']}}" >                          
                     
                    @if($data['payError'] == 1)                        
                        <p style="text-align: center; color: red;padding-top: 10px;" id="error-msg">{{Session::get('error')}}</p>
                    @endif
                    
                </div>
                <div class="mb-3">
                    <div class="accordion mt-2 dateRates-section" id="accordionExample">
                        <div class="card panel mb-3">
                            <div class="border-bottom accordion-title" id="headingOne">
                                <div class="px-3 py-3 mb-0 row align-items-center" data-toggle="collapse" data-target="#guestInfo" aria-expanded="true" aria-controls="guestInfo">
                                    <div class="col-md-11 col-sm-10 col-9">
                                        <h5 class="mb-0">Lead Guest Information</h5>
                                    </div>
                                    <div class="col-md-1 col-sm-2 col-3 text-right">
                                        <a role="button" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div id="guestInfo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="px-3 pb-3 pt-4">
                                    <p>Your contact details are collected in case we need to contact you about your booking. Additional personal information for each guest maybe required post the booking before vouchers can be issued.</p>
                      
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label class="label-control">First Name*</label>
                                            <input type="text" name="passenger_first_name" class="form-control" value="{{$data_arr['first_name']}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label class="label-control">Family Name (As Shown on Passport)</label>
                                            <input type="text" name="passenger_family_name" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label class="label-control">Email Address* </label>
                                            <input type="text" name="passenger_email" class="form-control" value="{{$data_arr['email']}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label class="label-control">Contact Number*</label>
                                            <input type="text" name="passenger_contact_no" class="form-control" value="{{$data_arr['contact_no']}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card panel mb-3 additional_guests"  style="display:none">
                        <div class="border-bottom accordion-title" id="headingTwo">
                            <div class="px-3 py-3 mb-0 row collapsed align-items-center" data-toggle="collapse" data-target="#additionalGuest" aria-expanded="true" aria-controls="collapseTwo">
                                <div class="col-md-11 col-sm-10 col-9">
                                    <h5 class="mb-0">Additional Guests</h5>
                                </div>
                                <div class="col-md-1 col-sm-2 col-3 text-right">
                                    <a role="button" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="additionalGuest" class="collapse otherPersons" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        </div>
                    </div>

                    <div class="card panel mb-3">
                        <div class="border-bottom accordion-title" id="headingThree">
                            <div class="px-3 py-3 mb-0 row collapsed align-items-center" data-toggle="collapse" data-target="#payment" aria-expanded="true" aria-controls="collapseThree">
                                <div class="col-md-11 col-sm-10 col-9">
                                    <h5 class="mb-0">Payment </h5>
                                </div>
                                <div class="col-md-1 col-sm-2 col-3 text-right">
                                    <a role="button" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                </div>
                            </div>
                        </div>

                        <div id="payment" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="px-3 pt-4 pb-3">
                            <p><strong>Billing Contact Details</strong></p>

                            <div class="form-group mt-2">
                                <div class="fildes_outer">
                                    <label class="label-control">First Name*</label>
                                    <input type="text" name="billing_firstname" class="form-control" value="{{$data_arr['first_name']}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="fildes_outer">
                                    <label class="label-control">Family Name (As Shown on Passport)*</label>
                                    <input type="text" name="billing_familyname" class="form-control" value="{{$data_arr['last_name']}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="fildes_outer">
                                    <label class="label-control">Email Address* </label>
                                    <input type="text" name="billing_email" class="form-control" value="{{$data_arr['email']}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="fildes_outer">
                                    <label class="label-control">Contact Number</label>
                                    <input type="text" name="billing_contact_no" class="form-control" value="{{$data_arr['contact_no']}}">
                                </div>
                            </div>

                            <div class="mt-3">
                                <p><strong>Billing Address</strong></p>
                                <div class="form-group mt-2">
                                    <div class="fildes_outer">
                                        <label class="label-control">Company Name (Optional)</label>
                                        <input type="text" name="billing_company" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label class="label-control">Street Address</label>
                                        <input type="text" name="billing_street" class="form-control" value="{{ $data_arr['bill_address_1'] }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label class="label-control">Additional Address Infomation (Optional)</label>
                                        <input type="text" name="billing_additional" class="form-control" value="{{ $data_arr['bill_address_2'] }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label class="label-control">Suburb / Town</label>
                                                <input type="text" name="billing_suburb" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label class="label-control">State / Territory / Region</label>
                                            <input type="text" name="billing_state" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label class="label-control">Postcode / Area Code*</label>
                                            <input type="text" name="billing_postcode" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="fildes_outer">
                                    <label class="label-control">Country</label>
                                    <div class="custom-select">
                                        <select class="form-control" name="billing_country">
                                            @foreach ($all_countries as $country)
                                                <option value="{{ $country['id'] }}" {{ $data_arr['bill_country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5">
                            <div class="row align-items-center">
                                <div class="col-sm-7">
                                    <strong>Payment Method</strong>
                                </div>
                                <div class="col-sm-5 text-left text-sm-right mt-3 mt-sm-0">
                                    <img src="images/visa.png" alt="" class="mr-2" />
                                    <img src="images/masterCard.png" alt="" class="img-responsive" />
                                </div>
                            </div>
                            <div class="black-checkbox mt-3">
                                <p class="custom_check d-block">I agree to the terms and conditions.
                                <input type="checkbox" id="checkbox-03" value="3" checked name="terms"><span class="check_indicator">&nbsp;</span> <a href="#">View Terms and Conditions here</a>.</p>
                                    <p class="custom_check d-block mt-2">This package pricing is inclusive of flights <input type="checkbox" id="checkbox-04" value="3" checked name="package_price"><span class="check_indicator">&nbsp;</span></p>
                            </div>
                            <div class="form-group mt-2">
                                <div class="fildes_outer">
                                    <label class="label-control">Credit Card Number*</label>
                                    <input type="text" name="card_number" class="form-control" placeholder="XXXX-XXXX-XXXX-XXXX" minlength="16" maxlength="19">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 mb-2 mb-sm-0">
                                    <div class="fildes_outer">
                                        <label>Expiration Date*</label>        
                                        <div class="input-group">
                                            <input id="checkin-date" type="text" placeholder="MMM YYYY" class="form-control" data-date="02-2012" data-date-format="mm-yyyy" name="expiration_date">
                                            <span class="input-group-addon px-2 py-2"><i class="ic-calendar fa-lg"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-2 mb-sm-0">
                                    <div class="fildes_outer">
                                        <label>Security Code / CSV*</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="XXX" class="form-control" maxlength="3" name="cvv">
                                            <span class="input-group-addon"><a href="#"><i class="fa fa-question-circle-o"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-sm-4">
                                <button type="submit" name="" class="btn btn-black btn-block pay_now_btn">PAY ${{ $data['default_currency'].' '.$data['singlePrice'],2}} NOW</button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="first_name" value="{{$data_arr['first_name']}}">
                <input type="hidden" name="last_name" value="{{$data_arr['last_name']}}">
                <input type="hidden" name="tour_id" value="{{$data['tour_id']}}">
                <input type="hidden" name="totalAmount" id="totalAmount" value="{{ $data['singlePrice'] }}">
                 <input type="hidden" name="Title" value="{{ $data['tour_name'] }}">
                <input type="hidden" name="code" value="{{ $data['code'] }}">
                <input type="hidden" name="provider" value="{{ $data['provider'] }}">
                <input type="hidden" name="departure_date" id="departure_date" value="{{ $data['start_date'] }}">
                <input type="hidden" name="return_date" id="return_date" value="{{ $data['finishDate'] }}">
                <input type="hidden" name="creditCardFee" id="creditCardFee" value="0">
                <input type="hidden" name="singleAmount" id="singleAmount" value="{{ $data['singlePrice'] }}">
                <input type="hidden" name="currency" id="currency" value="${{$data['default_currency']}}">
                <input type="hidden" name="twinAmount" id="twinAmount" value="0">
                <input type="hidden" name="user_id" id="user_id" value="{{$data_arr['user_id']}}">
                </form>
            </div>
        </div>            
      </div>      	
</section>

@stop
@section( 'custom-js' )
<script type="text/javascript">
    var no_of_days      = "<?php echo $data['no_of_days']; ?>";
    var durationType    = "<?php echo $data['durationType']; ?>";
    var from            = "<?php echo $data['from'] ?>";
    var childPrice      = "<?php echo $data['childPrice']; ?>";
    var infantPrice     = "<?php echo $data['infantPrice']; ?>";
    var no_of_days      = "<?php echo $data['no_of_days']; ?>";
    var default_currency= "<?php echo $data['default_currency']; ?>";
</script>
<script type="text/javascript" src="{{ url('js/checkout.js') }}"></script>
@stop