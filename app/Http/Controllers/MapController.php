<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\EroamSession;
use App\Libraries\Map;
use Cache;

class MapController extends Controller
{

	private $cities;
	private $session;
	private $map;

	public function __construct() {
		$this->cities = Cache::get( 'cities' );
		$this->session = new EroamSession;
		$this->map = new Map;
	}

	public function search() {
		$formatted_date = date('Y-m-d', strtotime(request()->input('start_date')));
		$filtered_interests = request()->has('interests') ? array_filter(request()->input('interests')) : [] ;
		ksort($filtered_interests);
		$preferences = session()->get('travel_preferences');
		request()->merge( ['start_date' => $formatted_date, 'interests' => $filtered_interests,'travel_preferences'=>$preferences] );

		$num_of_rooms = request()->input('rooms'); // No of rooms
		$num_of_adults = array_sum(request()->input('num_of_adults')); // total Adults
		$num_of_children = array_sum(request()->input('num_of_children')); // Total Childrens
		$child = request()->input('child'); // Total Childrens
		if(!isset($child) && empty($child)){
			$child = array();
		}
		
		
		request()->merge( ['child'=>$child,'rooms'=>$num_of_rooms,'childrens' => $num_of_children, 'travellers' => $num_of_adults] );
				

		$this->map->handle();
		return redirect( 'map' );
	}

	public function update() {

		if (request()->input('city_ids')) {
			$city_ids = request()->input( 'city_ids' );
			$cities = [];
			foreach ( $city_ids as $key => $city_id ) {
				$cities[] = get_city_by_id( $city_id );
				$cities[$key]['lat'] = $cities[$key]['latlong']['lat'];
				$cities[$key]['lng'] = $cities[$key]['latlong']['lng'];
			}
			$auto_sort = session()->get('map_data')['auto_sort'];
		} else if (request()->input('auto_sort')) {
			$auto_sort = request()->input('auto_sort');
			$cities = session()->get('map_data')['cities'];
		}
		$this->session->set_map( ['type' => 'manual', 'cities' => $cities, 'auto_sort' => $auto_sort] );

		if (request()->input('update_search') && request()->input('update_search') == true) {
			
			$this->map->default = [
				'city_ids' => $city_ids,
				'date_from' => session()->get('search_input')['start_date'],
				'auto_populate' => session()->get('search_input')['auto_populate'],
				'traveller_number' => session()->get('search_input')['travellers'],
				'child_number' => session()->get('search_input')['total_children'],			
				'search_input' => session()->get('search_input'),
				'rooms' => session()->get('search_input')['rooms'],
				'child' => session()->get('search_input')['child'],
				'pax_information' => []
			];
			$this->map->search_default($this->map->default);
			$response = [
				'map_data' => session()->get('map_data'),
				'search_session' => session()->get('search')
			];
			return $response;
		} else {

			return session()->get('map_data');
		}
	}

	public function add_cities_to_auto_map() {
		$routes = session()->get('map_data')['routes'];

		$cities = json_decode(request()->input('cities'));
		$routes[0]['cities'] = $cities;
		$city_ids = [];

		foreach ($cities as $city) {
			$city_ids[] = $city->id;
		}
		
		$this->map->default = [
			'city_ids' => $city_ids,
			'date_from' => session()->get('search_input')['start_date'],
			'auto_populate' => session()->get('search_input')['auto_populate'],
			'traveller_number' => session()->get('search_input')['travellers'],
			'child_number' => session()->get('search_input')['total_children'],			
			'search_input' => session()->get('search_input'),
			'rooms' => session()->get('search_input')['rooms'],
			'child' => session()->get('search_input')['child'],
			'pax_information' => []
		];
		// save sessions
		$this->session->set_map(['type' => 'auto', 'routes' => $routes, 'auto_sort' => 'on']);
		
		$this->map->search_default($this->map->default);

		$response = [
			'routes' => $routes,
			'search_session' => session()->get('search')
		];
		return $response;
	}

	public function map() {
		if ( session()->get( 'search_input' ) == null ) {
			return redirect( '/' );
		}
		$current_route = [];
		$alternative_routes = [];
		if ( session()->get( 'map_data' )['type'] == 'auto' ) {
			$routes = session()->get( 'map_data' )['routes'];

			foreach ( $routes as $key => $route ) {
				if ( $route['default'] == 'no' ) {
					$alternative_routes[$key]['cities'] = $route['cities'];
					$alternative_routes[$key]['duration'] = $route['duration']; 
					$alternative_routes[$key]['price_per_person'] = $route['price_per_person']; 
				} else {
					// default
					$current_route = $route['cities'];
				}
			}
		}
		$page_will_expire = 1;
		
		return view( 'map.map' )->with( compact( 'alternative_routes', 'current_route', 'page_will_expire' ) );
	}

	public function switch_route() {
	
		$routes = session()->get( 'map_data' )['routes'];
		$city_ids = [];
		foreach ( $routes as $key => $route ) {
			$routes[$key]['default'] = $key == request()->input( 'key' ) ? 'yes' : 'no';
			
		}
		$this->session->set_map( ['type' => 'auto', 'routes' => $routes, 'auto_sort' => 'on'] );
		// Update Search Session
		foreach ( $routes[request()->input( 'key' )]['cities'] as $city ) {
			$city_ids[] = $city['id'];
		}
		$data = [
				'city_ids' => $city_ids,
				'date_from' => session()->get('search_input')['start_date'],
				'auto_populate' => session()->get('search_input')['auto_populate'],
				'traveller_number' => session()->get('search_input')['travellers'],
				'child_number' => session()->get('search_input')['total_children'],			
				'search_input' => session()->get('search_input'),
				'rooms' => session()->get('search_input')['rooms'],
				'child' => session()->get('search_input')['child'],
				'pax_information' => []
		];
		
		$itinerary = http( 'post', 'map/city-defaults', $data );
		$last_leg = count($itinerary) - 1;
		$add_days = 0;
		
		$temp_date_from = $data['date_from'];
		foreach ( $itinerary as $key => $value ) {
			if($value['hotel']){
				$returnRoom = $this->map->getSelectedHotelRoom($value['hotel'],$data);
				$itinerary[$key]['hotel']['RoomRateDetailsList']['RoomRateDetails'] = $returnRoom;
			}
		
			if (empty($value['hotel'])) { //check if hotel is null
				$return_hotel = $this->map->set_default_hotel_api($data, $value['city']);
				$itinerary[$key]['hotel'] = $return_hotel;
			}
			//activity handler jayson
			
			if( $key != 0 ){
				$temp_date_from = date('Y-m-d', strtotime( $temp_date_from. ' +'.$itinerary[$key - 1]['city']['default_nights']. ' days' ) );
			}
			if(empty($value['activities'])) {
				$return_activity = $this->map->set_viator_default_activity($data, $value['city']);
				if( isset( $return_activity['extra_nights']) ){
					$itinerary[$key]['city']['days_to_add']= 1;
					$itinerary[$key]['city']['add_after_date'] = $temp_date_from;
					$itinerary[$key]['city']['default_nights'] = (int)$value['city']['default_nights'] + (int)$return_activity['extra_nights'];
				}
				$itinerary[$key]['activities'] = $return_activity ? [ $return_activity ] : null ;
			}
			
			// added by miguel to determine if own arrangement is set as the transport type preference by the user. If so, remove all transports 
			$trans_own_arrangement = FALSE;
			$search_preferences = session()->get('search_input');

			if( isset( $search_preferences['transport_types'] ) )
			{
				// check if the transport_type_id is equal to 25(own arrangement)
				if( $search_preferences['transport_types'][0] == 25 )
				{
					$trans_own_arrangement = TRUE;
				}
			}
			if($last_leg == $key){
				$trans_own_arrangement = TRUE;
			}
			// TRANSPORT HANDLER
			if( $data['search_input']['option'] == 'auto' && $key == 0 )
			{
				if ( $key != count( $itinerary ) - 1 )
				{

					if( $trans_own_arrangement != TRUE )
					{
						$transport        = $value['transport'];
						$origin_city      = $value['city'];
						$destination_city = $itinerary[ $key + 1 ]['city'];
						$date_from        = add_str_time( '+'.$add_days.' days' , $data['date_from'], 'Y-m-d' );
						$add_days         += intval( $value['city']['default_nights'] );
						$date_to          = add_str_time( '+'.$add_days.' days', $data['date_from'], 'Y-m-d' );
						$options = [
							'date_from'        => $date_from,
							'date_to'          => $date_to,
							'origin_city'      => $origin_city,
							'destination_city' => $destination_city,
							'traveller_number' => $data['traveller_number'],
							'leg'              => $key
						];
						$itinerary[ $key ]['transport'] = $this->map->get_transport_api_data( $transport, $options );
					}
					else
					{
						$itinerary[ $key ]['transport'] = NULL;
					}
				}
			}

		}
		$this->session->set_search( $itinerary, $data );
	

	}
}
