@extends('layouts.common')
@section('content')
 @include('partials.banner')
 @include('partials.search')
   
   <div class="account-block">
   		 @include('users.partials.sidebar')
		
		<div class="account-right p-4 pl-5">
		@include('users.partials.complete_profile')

		@if($link != '')
		<div class="mt-4 row">
		<div class="col-xl-4 offset-xl-8 col-sm-6 offset-sm-6">
		  <div class="row">
			<div class="col-sm-5 col-5">
			  <a href="{{ $link }}" class="btn btns_input_dark btn-block">UPDATE</a>
			</div>
			<div class="col-sm-7 col-7">Update your personal travel preferences.</div>
		  </div>
		</div>
		</div>
		@endif
		<hr class="mt-5">
		@if(session()->has('profile_step1_success'))
		    <div class="alert alert-success text-center" role="alert">
				{{ session()->get('profile_step1_success') }}
			</div>
		@endif
		@if(session()->has('profile_step1_error'))
		    
			<div class="alert alert-danger" role="alert">
				{{ session()->get('profile_step1_error') }}
			</div>
		@endif
		 
		 
		  <h5 class="mt-4">Your Profile</h5>
		  <form class="mt-3" method="post" action="{{url('profile_step1')}}" id="profile_step1_form">
			<div class="form-group">
			  <div class="fildes_outer">
				  <label>Title *</label>
				  <div class="custom-select">
					<select class="form-control" name="title">
						<option value="">Select Title</option>
						<option value="mr" {{ $user->customer->title == 'mr' ? 'selected' : '' }}>Mr</option>
						<option value="mrs" {{ $user->customer->title == 'mrs' ? 'selected' : '' }} >Mrs</option>
						<option value="ms" {{ $user->customer->title == 'ms' ? 'selected' : '' }} >Ms</option>
					</select>
				  </div>
			  </div>
			  <label for="title" generated="true" class="text-danger mt-1 error" style="display: inline-block;"></label>
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				<label>First Name *</label>
				<input type="text" name="first_name" value="{{ $user->customer->first_name }}" class="form-control" placeholder="" />  
				@if ($errors->has('first_name')) 
				 <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('first_name')}}</label>
				@endif

			  </div>
			</div>
			<div class="form-group">    
			  <div class="fildes_outer">  
				<label>Last Name *</label>
				 <input type="text" name="last_name" value="{{ $user->customer->last_name }}" class="form-control" placeholder="" /> 
				 @if ($errors->has('last_name')) 
				<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('last_name')}}</label>
			  @endif
			  </div>
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				<label>Email Address</label>
				 <input type="text" name="email" value="{{ $user->customer->email }}" class="form-control" placeholder="" />
			  </div>
			    <label for="email" generated="true" class="error"></label>
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				<label>Contact Number *</label>
				 <input type="text" name="contact_no" class="form-control" placeholder="" value="{{ $user->customer->contact_no}}"/>
				  @if ($errors->has('contact_no')) 
				<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('contact_no')}}</label>
			  @endif
			  </div>
			
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				<label>New Password</label>
				 <input type="password" name="old_password" class="form-control" placeholder="" />
				  @if ($errors->has('old_password')) 
					<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('old_password')}}</label>
				 @endif
			  </div>
			</div>
			<div class="form-group">
			  <div class="fildes_outer mb-1">
				<label>Confirm New Password</label>
				<input type="password" name="new_password" id="new_password" class="form-control" placeholder="" />
				@if ($errors->has('new_password')) 
					<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('new_password')}}</label>
				@endif
			  </div>
			  Must be at least 8 characters, and ideally contain some or all of: CAPS and lower case, numbers, random characters like #!*.
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				<label>Enter Existing Password</label>
				 <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="" />  
				  @if ($errors->has('confirm_password')) 
					<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('confirm_password')}}</label>
				  @endif
			  </div>
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				  <label>Gender</label>
				  <div class="custom-select">
					<select class="form-control" name="pref_gender">
						<option value="">Select Gender *</option>
						<option value="male" {{ $user->customer->pref_gender == 'male' ? 'selected' : '' }}>Male</option>
						<option value="female" {{ $user->customer->pref_gender == 'female' ? 'selected' : '' }}>Female</option>
						<option value="other" {{ $user->customer->pref_gender == 'other' ? 'selected' : '' }}>Other</option>
					</select> 
				  	</div>
				  	 @if ($errors->has('pref_gender')) 
						<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_gender')}}</label>
						@endif
			  </div>
			  
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				  <label>Age Group *</label>
				  <div class="custom-select">
					 <select id="age-group" name="pref_age_group_id" class="form-control">
						<option value="">Select Age Group</option>
						@foreach ($travel_preferences['age_groups'] as $age_group)
							<option value="{{ $age_group['id'] }}" {{ $user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : '' }}>{{ $age_group['name'] }}</option>
						@endforeach
					</select> 
				  </div>
				  @if ($errors->has('pref_age_group_id')) 
				<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_age_group_id')}}</label>
				@endif
			  </div>
			  
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				  <label>Nationality *</label>
				  <div class="custom-select">
					<select id="nationality" name="pref_nationality_id" class="form-control">
						<option value="">Select Nationality</option>
						@foreach ($travel_preferences['nationalities']['featured'] as $nationality)
							<option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
						@endforeach
							<option disabled> ======================================== </option>
						@foreach ($travel_preferences['nationalities']['not_featured'] as $nationality)
							<option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
						@endforeach
					</select>  
				  </div>
				 	 @if ($errors->has('pref_nationality_id')) 
						<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_nationality_id')}}</label>
					@endif
			  </div>
				
			</div>
			<div class="form-group">
			  <div class="fildes_outer">
				  <label>Default Currency *</label>
				  <div class="custom-select">
					<select class="form-control" id="select-currency" name="currency">
						<option value="">Select Default Currency</option>
						@foreach (session()->get('all_currencies') as $currency)
							<option value="{{ $currency['code'] }}" {{ $currency['code'] == $user->customer->currency ? 'selected' : '' }}>({{ $currency['code'] }}) {{ $currency['name'] }}</option>
						@endforeach
					</select>  
				  </div>
				   @if ($errors->has('currency')) 
				<label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('currency')}}</label>
				@endif
			  </div>
			</div>
			<input type="hidden" name="user_id" value="{{ $user->id }}">
			<input type="hidden" name="step" value="1">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
			<input type="hidden" id="registered_user_id" value="{{ $user->customer->email }}">
			<button type="submit" class="btn btns_input_dark btn-block pt-2 pb-2 mt-5">UPDATE PROFILE</button>
		  </form>
		</div>
	  </div>

@stop