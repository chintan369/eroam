@extends('layouts.common')
@section('content')
    <div class="body_sec">
        <div class="itinerary_block">
            <input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
            <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
            <div class="booking-summary">
                <span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>
            </div>
            @include('map.partials.map')
        </div>
    </div>
@endsection
@push('scripts') 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
    <script src="{{url('js/markerlabel.js') }}"></script>
    <script src="{{url('js/infobox.js')}}"></script>
    <script src="{{url('js/api-aot.js')}}"></script>
    <script src="{{url('js/api-mystifly.js')}}"></script>
    <script src="{{url('js/api-hb.js')}}"></script>
    <script src="{{url('js/api-ae.js')}}"></script>
    <script type="text/javascript">
        var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
        var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}";
        var listOfCurrencies = JSON.parse($('#currency-layer').val());
        @if( session()->has('search_input') )
        <?php $input = session()->get('search_input');?>
        var travel_pref = [{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}];
        @else
        var travel_pref = [];
        @endif
    </script>
    <script src="{{url('js/itinerary/common.js')}}"></script>
    <script src="{{url('js/booking-summary.js')}}"></script>
    <script src="{{url('js/moment.js')}}"></script>
    <script src="{{url('js/eroam-map.js')}}"></script>
    <script src="{{url('js/map/custom.js')}}"></script>
    <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush