@extends('layouts.profileLayout')
@section('content')  
<script src="{{ url( 'js/theme/jquery.min.js' ) }}"></script>  
    <!-- <div class="profile_page"> -->
    @include('pages.search_banner') 
    <!-- </div> -->

    @include('user.profile_sidebar')
    
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    @include('user.profile_header')
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    @if(session()->has('profile_step1_success'))
                        <p class="success-box m-t-30">
                            {{ session()->get('profile_step1_success') }}
                        </p>
                    @endif
                    @if(session()->has('profile_step1_error'))
                        <p class="danger-box m-t-30">
                            {{ session()->get('profile_step1_error') }}
                        </p>
                    @endif
                    <div class="m-t-30 profile-prefrence">
                        <h2 class="profile-title">Your Profile</h2>
                        <form class="form-horizontal m-t-20" method="post" action="{{url('profile_step1')}}" id="profile_step1_form">
                            
                            <div class="form-group">
                                <div class="panel-form-group form-group">
                                    <label class="label-control">Title *</label>
                                    <select class="form-control" name="title">
                                        <option value="">Select Title</option>
                                        <option value="mr" {{ $user->customer->title == 'mr' ? 'selected' : '' }}>Mr</option>
                                        <option value="mrs" {{ $user->customer->title == 'mrs' ? 'selected' : '' }} >Mrs</option>
                                        <option value="ms" {{ $user->customer->title == 'ms' ? 'selected' : '' }} >Ms</option>
                                    </select>
                                </div>
                                <label for="title" generated="true" class="error"></label>
                            </div>
                            <!--total /working hours /9 *1.5 -->
                            <div class="form-group">                                
                                <div class="panel-form-group input-control">
                                    <label class="label-control">First Name *</label>
                                    <input type="text" name="first_name" value="{{ $user->customer->first_name }}" class="form-control" placeholder="" />                                    
                                </div>
                                @if ($errors->has('first_name')) 
                                    <span class="error active">{{$errors->first('first_name')}}</span>
                                @endif
                                <label for="first_name" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">                                
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Last Name *</label>
                                    <input type="text" name="last_name" value="{{ $user->customer->last_name }}" class="form-control" placeholder="" />                                    
                                </div>
                                @if ($errors->has('last_name')) 
                                    <span class="error active">{{$errors->first('first_name')}}</span>
                                @endif
                                <label for="last_name" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Email Address</label>
                                    <input type="text" name="email" value="{{ $user->customer->email }}" class="form-control" placeholder="" />                                
                                </div>
                                <label for="email" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Contact Number *</label>
                                    <input type="text" name="contact_no" class="form-control" placeholder="" value="{{ $user->customer->contact_no}}"/>
                                    
                                </div>
                                @if ($errors->has('contact_no')) 
                                    <span class="error active">{{$errors->first('contact_no')}}</span>
                                @endif
                                <label for="contact_no" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Enter Existing Password</label>
                                    <input type="password" name="old_password" class="form-control" placeholder="" />                                                                  
                                </div>
                                @if ($errors->has('old_password')) 
                                    <span class="error active">{{$errors->first('old_password')}}</span>
                                @endif
                                <label for="old_password" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">New Password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control" placeholder="" />                                    
                                </div>
                                @if ($errors->has('new_password')) 
                                    <span class="error active">{{$errors->first('new_password')}}</span>
                                @endif
                                <label for="new_password" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Confirm New Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="" />                                    
                                </div>
                                @if ($errors->has('confirm_password')) 
                                <span class="error active">{{$errors->first('confirm_password')}}</span>
                                @endif
                                <label for="confirm_password" generated="true" class="error"></label>
                            </div>
                            <p><em>Must be at least 8 characters, and ideally contain some or all of: CAPS and lower case, numbers, random characters like #!*.</em></p>
                            
                            <div class="form-group">
                                <div class="panel-form-group">
                                    <label class="label-control">Gender *</label>
                                    <select class="form-control" name="pref_gender">
                                        <option value="">Select Gender</option>
                                        <option value="male" {{ $user->customer->pref_gender == 'male' ? 'selected' : '' }}>Male</option>
                                        <option value="female" {{ $user->customer->pref_gender == 'female' ? 'selected' : '' }}>Female</option>
                                        <option value="other" {{ $user->customer->pref_gender == 'other' ? 'selected' : '' }}>Other</option>
                                    </select>                                    
                                </div>
                                @if ($errors->has('pref_gender')) 
                                    <span class="error active">{{$errors->first('pref_gender')}}</span>
                                @endif
                                <label for="pref_gender" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group ">
                                    <label class="label-control">Age Group *</label>
                                    <select id="age-group" name="pref_age_group_id" class="form-control">
                                        <option value="">Select Age Group</option>
                                        @foreach ($travel_preferences['age_groups'] as $age_group)
                                            <option value="{{ $age_group['id'] }}" {{ $user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : '' }}>{{ $age_group['name'] }}</option>
                                        @endforeach
                                    </select>                                    
                                </div>
                                @if ($errors->has('pref_age_group_id')) 
                                    <span class="error active">{{$errors->first('pref_age_group_id')}}</span>
                                @endif
                                <label for="age-group" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group ">
                                    <label class="label-control">Nationality *</label>
                                    <select id="nationality" name="pref_nationality_id" class="form-control">
                                        <option value="">Select Nationality</option>
                                        @foreach ($travel_preferences['nationalities']['featured'] as $nationality)
                                            <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                                        @endforeach
                                            <option disabled> ======================================== </option>
                                        @foreach ($travel_preferences['nationalities']['not_featured'] as $nationality)
                                            <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                                        @endforeach
                                    </select>                                    
                                </div>
                                @if ($errors->has('pref_nationality_id')) 
                                    <span class="error active">{{$errors->first('pref_nationality_id')}}</span>
                                @endif
                                <label for="nationality" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group ">
                                    <label class="label-control">Default Currency *</label>
                                    <select class="form-control" id="select-currency" name="currency">
                                        <option value="">Select Default Currency</option>
                                        @foreach (session()->get('all_currencies') as $currency)
                                            <option value="{{ $currency['code'] }}" {{ $currency['code'] == $user->customer->currency ? 'selected' : '' }}>({{ $currency['code'] }}) {{ $currency['name'] }}</option>
                                        @endforeach
                                    </select>                                    
                                </div>
                                @if ($errors->has('currency')) 
                                    <span class="error active">{{$errors->first('currency')}}</span>
                                @endif
                                <label for="select-currency" generated="true" class="error"></label>
                            </div>
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <input type="hidden" name="step" value="1">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
                            <input type="hidden" id="registered_user_id" value="{{ $user->customer->email }}">
                            <div class="m-t-20">
                                <button type="submit" name="" class="btn btn-black btn-block">UPDATE PROFILE</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_leftmenu.js?v='.$version.'') }}"></script>
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_step1.js?v='.$version.'') }}"></script>
@stop