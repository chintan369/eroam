<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use StdClass;
use App\Libraries\Filter;
use App\Libraries\EroamSession;
use App\Libraries\ApiCache;
use PDF;
use Mail;
use Cache;
use Validator;
use App\Libraries\Map;
use Carbon\Carbon;
use Config;


class ExpediaApiController extends Controller {

	private $host;
	private $apikey;
	private $secret;
	private $cid;
	private $minorRev;
	private $ver;
	private $path;
	private $locale;
	private $currencyCode;
	private $timestamp;
	private $sig;
	private $query;

	public function __construct(){
        parent::__construct();
		$this->eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
		$this->host 		= 'http://api.ean.com/';
		$this->book_host 		= 'https://book.api.ean.com/';
		$this->apiKey      = "1jn4t7g731vbgetaifib3skism";
		$this->secret 		= "5c6s845m2vhid";
		$this->cid 			= '502698';
		$this->minorRev 	= '30';
		$this->supplierType 	= 'E';
		$this->ver 			= 'v3/';
		$this->path 		= "ean-services/rs/hotel/{$this->ver}";
		$this->locale 		= 'en_AU';
		$this->currencyCode = 'AUD';
		$this->rateType = 'MerchantPackage';
		$this->timestamp 	= gmdate('U');
		$this->sig 			= md5($this->apiKey . $this->secret . $this->timestamp);
		$this->customerIpAddress  = $_SERVER['REMOTE_ADDR'];
		$this->query 		= "?cid={$this->cid}&apikey={$this->apiKey}&sig={$this->sig}&minorRev={$this->minorRev}&locale={$this->locale}&currencyCode={$this->currencyCode}&customerIpAddress={$this->customerIpAddress}";		      
	}

	/**
	 * Display a listing of the Hotel Resource.
	 *
	 * @return Response
	 */
	public function getHotels($data='')
	{
		//$data 				= Input::all();
		//For prefernces

		$preferences = session()->get('travel_preferences');
		$starRatingStr = '';
		$starRating = '';
		if(isset($preferences[0]['accommodation'][0])){
			$starRating = $preferences[0]['accommodation'][0];
			if($starRating == 5){
				$starRating = 2;
			}elseif($starRating == 2){
				$starRating = 3;
			}elseif($starRating == 3){
				$starRating = 4;
			}elseif($starRating == 9){
				$starRating = 5;
			}
			$starRatingStr = '&minStarRating='.$starRating.'&maxStarRating='.$starRating.'';
			if($starRating == 0){
				$starRatingStr = '';
			}
		}
        $numberOfResults 	= '50';
        $method 			= 'list';
        $rateType= 'MerchantPackage';
        $customerSessionId=$data['customerSessionId'];
        $num_of_rooms = session()->get( 'search_input' )['rooms'];
        $hotel_request_data = session()->get( 'search_input' );

        $children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.session()->get( 'search_input' )['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.session()->get( 'search_input' )['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}
      
		$nights = 0;
        $nights = session()->get('search')['itinerary'][$data['leg']]['city']['default_nights'];
        $cityId = session()->get('search')['itinerary'][$data['leg']]['city']['id'];
        $numberOfAdults     =$data['numberOfAdults'];
        $arrivalDate 		= date('m/d/Y', strtotime($data['arrivalDate'])); 
		$departureDate		= date('m/d/Y', strtotime($data['departureDate']));

    	$query  = $this->query;
    	$query .= "&customerSessionId={$customerSessionId}&rateType={$rateType}&city={$data['city']}&countryCode={$data['countryCode']}";
    	$cityData['cityId'] = $cityId;
    	$hotelIdList = http( 'post', 'Expedia-Map-Hotels', $cityData );
		if(!empty($hotelIdList)){
			$hotelIdList = implode(',',$hotelIdList);
			$query .= "&hotelIdList={$hotelIdList}";
		}
    	$query .= $starRatingStr;
    	$query .= "&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$expedia_request}&numberOfResults={$numberOfResults}";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		
		curl_close($ch);
		
		if ($headers['http_code'] != '200') {
		 	return json_encode(array(
				'status' => 200,
				'data' => []));	
		} else {
			$status = $headers['http_code'];
			$temp_result = json_decode($data,true);
			$data = json_decode($data);
			

			if($data->HotelListResponse && array_key_exists('EanWsError', $data->HotelListResponse)){
		 	 	return json_encode(array(
					'status' => 200,
					'data' => []));	
		 	} else {

		 		if(!isset($temp_result['HotelListResponse']['HotelList']['HotelSummary'][0])){
        	 		$HotelSummary['0'] = $temp_result['HotelListResponse']['HotelList']['HotelSummary'];
        	 		$hotelLists = $HotelSummary;
        	 		$hotelLists = json_decode(json_encode($hotelLists));
        	 	}else{
        	 		$hotelLists= $data->HotelListResponse->HotelList->HotelSummary;
        	 	}
				
				$hotels  = array();

				if($hotelLists){
				
					 	foreach ($hotelLists as $key => $hotelList ) { 
					 		if(isset($hotelList->thumbNailUrl)){
						 		$hotel['hotelId'] =  $hotelList->hotelId;
						 		$hotel['code'] =  $hotelList->hotelId;
								$hotel['name'] = $hotelList->name;
								$hotel['address'] = $hotelList->address1;
								$hotel['city'] = $hotelList->city;
								$hotel['stateProvinceCode'] = '';//$hotelList->stateProvinceCode;
								if(array_key_exists('postalCode', $hotelList)){
									$hotel['postalCode'] = $hotelList->postalCode;
								}else{
									$hotel['postalCode'] = '';
								}
								$hotel['hotelRating'] = $hotelList->hotelRating;
								$hotel['propertyCategory'] = $hotelList->propertyCategory;
								$hotel['rateCurrencyCode'] = $hotelList->rateCurrencyCode;
								$hotel['description'] = html_entity_decode($hotelList->shortDescription);
								$hotel['image'] = $this->changeImage("http://media.expedia.com".$hotelList->thumbNailUrl);
			
								$hotel['highRate'] = $hotelList->highRate;
								$hotel['lowRate'] = $hotelList->lowRate;
								$hotel['latitude'] = $hotelList->latitude;
								$hotel['longitude'] = $hotelList->longitude;
								$hotel['countryCode'] = $hotelList->countryCode;
								$hotel['hotelRating'] = $hotelList->hotelRating;
								$hotel['hotelRatingDisplay'] = $hotelList->hotelRatingDisplay;

								$hotel['rooms'] = [];

								$groups = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo;
								$i=0;
								if(count(array($groups->ChargeableRateInfo)) > 0){

									$hotel['rooms'][$i]['roomTypeCode'] = $hotelList->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
		                            $hotel['rooms'][$i]['rateCode'] = $hotelList->RoomRateDetailsList->RoomRateDetails->rateCode;
		                            $hotel['rooms'][$i]['maxRoomOccupancy'] = $hotelList->RoomRateDetailsList->RoomRateDetails->maxRoomOccupancy;
		                            $hotel['rooms'][$i]['quotedRoomOccupancy'] = $hotelList->RoomRateDetailsList->RoomRateDetails->quotedRoomOccupancy;
		                            $hotel['rooms'][$i]['roomDescription']  = $hotelList->RoomRateDetailsList->RoomRateDetails->roomDescription;
		                            $hotel['rooms'][$i]['expediaPropertyId'] = $hotelList->RoomRateDetailsList->RoomRateDetails->expediaPropertyId;
									$hotel['rooms'][$i]['currentAllotment'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->currentAllotment;
	                                $hotel['rooms'][$i]['nonRefundable'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->nonRefundable;
		                            $hotel['rooms'][$i]['rateType'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->rateType;
	                                $temp_array = json_decode(json_encode($hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup),true);
	                                if(isset($temp_array['Room'][0])){
	                                	$nights = count($temp_array['Room'][0]['ChargeableNightlyRates']);
	                                	$hotel['rooms'][$i]['rateKey'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup->Room[0]->rateKey;
	                                }else{
	                                	$nights = count($temp_array['Room']['ChargeableNightlyRates']);
	                                	$hotel['rooms'][$i]['rateKey'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup->Room->rateKey;
	                                }
	                              	
									foreach ($groups->ChargeableRateInfo as $key1 => $val1){
										if($key1 == '@total'){
											$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
											$total = (($val1 * $eroamPercentage) / 100) + $val1;
											$total = $total / $numberOfAdults; 
											$total = $total / $nights; 
											$hotel['rooms'][$i]['rate'] = round($total,2);
											$hotel['rooms'][$i]['exptotal'] = $val1;
											break;
										}
									}

									$hotels[] = $hotel;	
								}
							
					 		}
					 	}
					
				}

				return json_encode(array(
					'status' => $status,
					'data' => $hotels));
			}
		}
	}

	public function getHotelDetails($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,$type=1,$rateCode=''){
        
        $apiExperience = 'PARTNER_WEBSITE';
    	$query  = $this->query."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    	if($type == 1){
    		$query  .= "&includeDetails={$includeDetails}&options={$options}";
    		$return_data = $this->callExpediaAvaibility($query);
    	}else{
    		$query  .= "&rateCode={$rateCode}&roomTypeCode={$roomTypeCode}&includeDetails={$includeDetails}&options={$options}";
    		$return_data = $this->callExpediaAvaibility($query);
    		$data = $return_data['data'];
    		$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;
    		if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){
				$query  = $this->query."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    			$query  .= "&includeDetails={$includeDetails}&options={$options}";
    			$return_data = $this->callExpediaAvaibility($query);
    		}else{
    			$hotelDetail = $data->HotelRoomAvailabilityResponse;
		 		return json_encode(array(
					'status' => 200,
					'data' => $hotelDetail));		
    		}
    	}
    	$query  .= "&includeDetails={$includeDetails}&options={$options}";
    	$return_data = $this->callExpediaAvaibility($query);
    	$headers = $return_data['headers'];
    	$data = $return_data['data'];
		if ($headers['http_code'] != '200') {
		 	return json_encode(array(
				'status' => 200,
				'data' => []));	
		}else{
			$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;
			if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){		 	 	
				return json_encode(
					array(
						'status' => 200,
						'data' => []
					)
				);	
		 	} else {
		 		$hotelDetail = $data->HotelRoomAvailabilityResponse;
		 		return json_encode(
		 			array(
						'status' => 200,
						'data' => $hotelDetail
					)
		 		);
			}
		}
		
	}

	public function callExpediaAvaibility($query){
		$method = 'avail';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);
		$return['headers'] = $headers;
		$return['data'] = $data;
		curl_close($ch);
		return $return;
	}

	public function hotelInfo($hotelID){
		$method 			= 'info';
    	$query  = $this->query."&hotelId={$hotelID}&countryCode=AUD";
    	// initiate curl
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
		//curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		// close curl
		curl_close($ch);

		if ($headers['http_code'] != '200') {
		 	return json_encode(array(
				'status' => 200,
				'data' => []));	
		} else {
				$data = json_decode($data);
				return json_encode(array(
					'status' => 200,
					'data' => $data));
		}

	}

	function changeImage($uri){
		$hotelImage = '';
		$hotelImage1 = $uri;
		$hotelImage2 = str_replace("_t.", "_y.", $uri);
		$hotelImage3 = str_replace("_t.", "_s.", $uri);

		if ($this->existsImage($hotelImage2) == 200) {
			 $hotelImage = $hotelImage2;
		} elseif($this->existsImage($hotelImage3) == 200) {
			 $hotelImage = $hotelImage3;
		} else {
			 $hotelImage = $hotelImage1;
		}
		return $hotelImage;
	}

	function existsImage($uri)
	{
	    $ch = curl_init($uri);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);

	    return $code == 200;
	}

	//check price variation for expedia
	public function expediaPriceCheck(){
		
        $data = session()->get('search');
        
        
        $priceCheck = array();
        $num_of_rooms = session()->get( 'search_input' )['rooms'];
       	$hotel_request_data = session()->get( 'search_input' );
       	$children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.session()->get( 'search_input' )['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.session()->get( 'search_input' )['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}
		$session_data = session()->get('search');
        foreach ($session_data['itinerary'] as $key => $value) {
        	if($value['hotel']){
    			$value['hotel'] = json_decode(json_encode($value['hotel']) , true );
    			$nights = $value['city']['default_nights'];
				
    			$code = $value['hotel']['hotelId'];
    			$arrivalDate  = date('m-d-Y',strtotime($value['hotel']['checkin']));
				$departureDate = date('m-d-Y',strtotime($value['hotel']['checkout']));
    			$room = $expedia_request;
    			if(isset($value['hotel']['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'])){
    				$roomTypeCode = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'];
    			}else{
    				$roomCode = '@roomCode';
    				$roomTypeCode = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
    			}
    			$rateCode = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
    			$includeDetails = 'true';
    			$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
    			if(isset($value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'])){
    				$rateKey = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'];
    			}else{
    				$rateKey = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'];
    			}
    			$rateKey = $rateKey;
    			$latitude = $value['hotel']['latitude'];
    			$longitude = $value['hotel']['longitude'];
    			$customerSessionId = $value['hotel']['customerSessionId'];
    			$response = $this->getHotelDetails($code,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId);
    			$response = json_decode($response)->data;
    			$selected_hotel = json_decode(json_encode($value['hotel']),true);
    			$total = '@total';
    			$t_priceCheck = array();
    			
    			if(isset($response->HotelRoomResponse->rateCode)){
			    	$temp_array[0] = $response->HotelRoomResponse;
				    $response->HotelRoomResponse = $temp_array;
    			}
    			$lowest_price = array();
    			if (!empty($response)) {
				    foreach($response->HotelRoomResponse as $key2 => $RoomResponse){
				    	$total = '@total';
				    	$lowest_price[$key2] = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$total;
				    }
	    			$min_array = array_keys($lowest_price, min($lowest_price));
				    
				    $singleRate = '@nightlyRateTotal';
				    foreach ($response->HotelRoomResponse as $key1 => $value1) {
				      
				      	$current_hotel = json_decode(json_encode($value1),true);
				      
				      	$singleRate = '@nightlyRateTotal';
	                  	$singleRate = $current_hotel['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
	                  
	                  	$taxes = 0;
					  	if(isset($current_hotel['RateInfos']['RateInfo']['taxRate'])){
							$taxes = $current_hotel['RateInfos']['RateInfo']['taxRate'];
					  	}

	                  	$currentRate = getExpediaHotelPriceWithEroamMarkup($singleRate,$nights,$taxes,$this->eroamPercentage);

	                  	$singleRate = '@nightlyRateTotal';
	                  	$singleRate = $selected_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
	                  	$taxes = 0;
					  	if(isset($selected_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])){
							$taxes = $selected_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
					  	}
	                  	$selectedRate = getExpediaHotelPriceWithEroamMarkup($singleRate,$nights,$taxes,$this->eroamPercentage);
	                  
				      	if($currentRate != $selectedRate && $key1 == $min_array[0]){
				      		$t_priceCheck['oldRate'] = $selectedRate;
				      		$t_priceCheck['newRate'] = $currentRate;
				      		$t_priceCheck['hotelName'] = $selected_hotel['name'];
				      		$t_priceCheck['hotelId'] = $selected_hotel['hotelId'];
				      		$t_priceCheck['rateCode'] = $rateCode;
				      		$t_priceCheck['city'] = $value['city']['name'];
				      		$priceCheck[$key] = $t_priceCheck;
				      	}
				    }
    			}
        	}
        }
        return $priceCheck;
	}
	public function expediaBooking($hotelId,$arrivalDate,$departureDate,$rateKey,$roomTypeCode,$rateCode,$chargeableRate,$passengers_info,$bedtypes,$customerSessionId){

		//Test Booking Credentials Static data provided by Expedia
		 $headers1 = [
             'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
             'Origin' => url('')
         ];

        $response = http( 'get', 'getCardInformation', [], $headers1);
	    
	    $creditCardNumber = $this->safe_b64decode($response[0]['cardNumber']);
        $creditCardIdentifier = $this->safe_b64decode($response[0]['cvvNumber']);
        $creditCardExpirationMonth = $this->safe_b64decode($response[0]['expiryMonth']);
        $creditCardExpirationYear = $this->safe_b64decode($response[0]['expiryYear']);
        $email = 'test@travelnow.com';
	    $firstName = 'Test Booking';
	    $lastName = 'Test Booking';
	    $homePhone = '2145370159';
	    $workPhone = '2145370159';
	    $creditCardType = 'CA';
	    $address1 = 'travelnow';
	    $city = 'Atlantic City';
	    $stateProvinceCode = 'NJ';
	    $countryCode = 'AU';
	    $postalCode = '08401';

        $num_of_rooms = session()->get( 'search_input' )['rooms'];
        $hotel_request_data = session()->get( 'search_input' );

        $children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.session()->get( 'search_input' )['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
			$expedia_request .='&room1FirstName='.$passengers_info['passenger_first_name'][0].'&room1LastName='.$passengers_info['passenger_last_name'][0].'&room1BedTypeId='.$bedtypes.'&room1SmokingPreference=NS';
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.session()->get( 'search_input' )['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
				$expedia_request .='&room'.$n.'FirstName='.$passengers_info['passenger_first_name'][0].'&room'.$n.'LastName='.$passengers_info['passenger_last_name'][0].'&room'.$n.'BedTypeId='.$bedtypes.'&room'.$n.'SmokingPreference=NS';
			}
		}
		$rateType = $this->rateType;
		

    	
	    $query = "&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}&supplierType=E&rateKey={$rateKey}&roomTypeCode={$roomTypeCode}&rateCode={$rateCode}{$expedia_request}&chargeableRate={$chargeableRate}&email={$email}&firstName={$firstName}&lastName={$lastName}&homePhone={$homePhone}&workPhone={$workPhone}&creditCardType={$creditCardType}&creditCardNumber={$creditCardNumber}&creditCardIdentifier={$creditCardIdentifier}&creditCardExpirationMonth={$creditCardExpirationMonth}&creditCardExpirationYear={$creditCardExpirationYear}&address1={$address1}&city={$city}&stateProvinceCode={$stateProvinceCode}&countryCode={$countryCode}&postalCode={$postalCode}&rateType={$rateType}";

		
		$host = 'https://book.api.ean.com/';
		$ver 		= 'v3/';
		$method 	= 'res';
		$path 		= "ean-services/rs/hotel/{$ver}{$method}";
		
		$query = $this->query.'&customerSessionId={$customerSessionId}'.$query;	
		//Calling curl for booking
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $host . $path . $this->query);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$query);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		curl_close($ch);	
		$data = json_decode($data);
		$return_data = array();
		if($data->HotelRoomReservationResponse && array_key_exists('EanWsError', $data->HotelRoomReservationResponse)){
		 	 	$return_data['itineraryId'] = '';
				$return_data['numberOfRoomsBooked'] = '';
				$return_data['reservationStatusCode'] = 'NC';
		} else {
			$return_data['itineraryId'] = $data->HotelRoomReservationResponse->itineraryId;
			$return_data['numberOfRoomsBooked'] = $data->HotelRoomReservationResponse->numberOfRoomsBooked;
			$return_data['reservationStatusCode'] = $data->HotelRoomReservationResponse->reservationStatusCode;
		}
		return $return_data;
		
	}

	public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
}
