@extends('layouts.profileLayout')
@section('content')  
<script src="{{ url( 'js/theme/jquery.min.js' ) }}"></script>  
    <div class="profile_page">
        @include('pages.search_banner') 
    </div>
<section>
    @include('user.profile_sidebar')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    @include('user.profile_header')
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    @if(session()->has('profile_step2_success'))
                        <p class="success-box m-t-30">
                            {{ session()->get('profile_step2_success') }}
                        </p>
                    @endif
                    @if(session()->has('profile_step2_error'))
                        <p class="danger-box m-t-30">
                            {{ session()->get('profile_step2_error') }}
                        </p>
                    @endif
                    <div class="m-t-30 profile-prefrence">
                        <h2 class="profile-title">Physical Address</h2>
                        <form class="form-horizontal m-t-20" method="post" action="{{url('profile_step2')}}" id="profile_step2_form">

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Address Line One (1) *</label>
                                    <input type="text" name="phy_address_1" value="{{ $user->customer->phy_address_1 }}" class="form-control phy_address_1" placeholder="Street Address, P.O Box, Company Name" />
                                    @if ($errors->has('phy_address_1')) 
                                        <span class="error active">{{$errors->first('phy_address_1')}}</span>
                                    @endif
                                </div>
                                <label for="phy_address_1" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Address Line Two (2) </label>
                                    <input type="text" name="phy_address_2" value="{{ $user->customer->phy_address_2 }}" class="form-control phy_address_2" placeholder="Apartment, Suite, Unit, Building, Floor, etc." />                                   
                                </div>
                                <label for="phy_address_2" generated="true" class="error"></label>
                            </div>
                            
                            <div class="form-group">
                                <div class="panel-form-group">
                                    <label class="label-control">Country *</label>
                                    <select id="phy_country" name="phy_country" class="form-control phy_country">
                                        <option value="">Select Country</option>
                                        @foreach ($all_countries as $country)
                                            <option value="{{ $country['id'] }}" {{ $user->customer->phy_country == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                        @endforeach                                       
                                    </select>
                                    @if ($errors->has('phy_country')) 
                                        <span class="error active">{{$errors->first('phy_country')}}</span>
                                    @endif
                                </div>
                                <label for="phy_country" generated="true" class="error"></label>
                            </div>
                            
                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">State *</label>
                                    <input type="text" name="phy_state" value="{{ $user->customer->phy_state }}" class="form-control phy_state" placeholder="State / Territory / Province / Region" />
                                    @if ($errors->has('phy_state')) 
                                        <span class="error active">{{$errors->first('phy_state')}}</span>
                                    @endif
                                </div>
                                <label for="phy_state" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">City *</label>
                                    <input type="text" name="phy_city" value="{{ $user->customer->phy_city }}" class="form-control phy_city" placeholder="City" />
                                    @if ($errors->has('phy_city')) 
                                        <span class="error active">{{$errors->first('phy_city')}}</span>
                                    @endif
                                </div>
                                <label for="phy_city" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">ZIP *</label>
                                    <input type="text" name="phy_zip" value="{{ $user->customer->phy_zip }}" class="form-control phy_zip" placeholder="ZIP / Postal Code" />
                                    @if ($errors->has('phy_zip')) 
                                        <span class="error active">{{$errors->first('phy_zip')}}</span>
                                    @endif
                                </div>  
                                <label for="phy_zip" generated="true" class="error"></label>   
                            </div>

                        <h2 class="profile-title">Billing Address</h2>   
                            <br>
                            <div class="form-group blackcheckbox">
                                <p><label class="radio-checkbox label_check m-r-10" for="bill_checkbox">
                                    <input type="checkbox" id="bill_checkbox" value="9" name="bill_checkbox"> Same as Physical Address
                                </label></p>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Address Line One (1) *</label>
                                    <input type="text" name="bill_address_1" value="{{ $user->customer->bill_address_1 }}" class="form-control bill_address_1" placeholder="Street Address, P.O Box, Company Name" />
                                    @if ($errors->has('bill_address_1')) 
                                        <span class="error active">{{$errors->first('bill_address_1')}}</span>
                                    @endif
                                </div>
                                <label for="bill_address_1" generated="true" class="error"></label>
                            </div>
                            
                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Address Line Two (2) </label>
                                    <input type="text" name="bill_address_2" value="{{ $user->customer->bill_address_2 }}" class="form-control bill_address_2" placeholder="Apartment, Suite, Unit, Building, Floor, etc." />                                    
                                </div>
                                <label for="bill_address_2" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group">
                                    <label class="label-control">Country *</label>
                                    <select id="bill_country" name="bill_country" class="form-control bill_country">
                                        <option value="">Select Country</option>
                                        @foreach ($all_countries as $country)
                                            <option value="{{ $country['id'] }}" {{ $user->customer->bill_country == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                        @endforeach                                       
                                    </select>
                                    @if ($errors->has('bill_country')) 
                                        <span class="error active">{{$errors->first('bill_country')}}</span>
                                    @endif
                                </div>
                                <label for="bill_country" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">State *</label>
                                    <input type="text" name="bill_state" value="{{ $user->customer->bill_state }}" class="form-control bill_state" placeholder="State / Territory / Province / Region" />
                                    @if ($errors->has('bill_state')) 
                                        <span class="error active">{{$errors->first('bill_state')}}</span>
                                    @endif
                                </div>
                                <label for="bill_state" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">City *</label>
                                    <input type="text" name="bill_city" value="{{ $user->customer->bill_city }}" class="form-control bill_city" placeholder="City" />
                                    @if ($errors->has('bill_city')) 
                                        <span class="error active">{{$errors->first('bill_city')}}</span>
                                    @endif
                                </div>
                                <label for="bill_city" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">ZIP *</label>
                                    <input type="text" name="bill_zip" value="{{ $user->customer->bill_zip }}" class="form-control bill_zip" placeholder="ZIP / Postal Code" />
                                    @if ($errors->has('bill_zip')) 
                                        <span class="error active">{{$errors->first('bill_zip')}}</span>
                                    @endif
                                </div>  
                                <label for="bill_zip" generated="true" class="error"></label>     
                            </div>

                            <div class="m-t-20">
                                <button type="submit" name="" class="btn btn-black btn-block">UPDATE PROFILE</button>
                            </div>
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_leftmenu.js?v='.$version.'') }}"></script>
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_step2.js?v='.$version.'') }}"></script>
@stop