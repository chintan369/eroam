@extends('itinenary.booking-layout')

@section('booking-content')
@php
    $total = $totalCost * $travellers;
    $gst = $total*0.025;
    $finalTotal = number_format($total + $gst, 2, '.', ',');
    $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
    $travellers   = session()->get( 'search' )['travellers'];
    $total_childs = session()->get( 'search' )['child_total'];
    $rooms = session()->get( 'search' )['rooms'];
@endphp
<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
   
    <div class="container-fluid">
        @include('itinenary.partials.payment-filter-option')

        <div class="itinerary_page pt-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class=" m-1">
                            <div class="tripDetails mb-3">
                                <div class="card panel border-0 rounded-0 p-3">
                                    <div class="row">
                                        <div class="col-sm-7 col-xl-8">
                                            <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                            <div class="media">
                                                <i class="ic-calendar mr-2"></i>
                                                <div class="media-body pb-3 mb-0">
                                                    {{$startDate}} - {{$endDate}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="row align-items-end">
                                                <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="list-inline-item">
                                                            <i class=" ic-local_hotel"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-local_activity"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-directions_bus"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                                    <h2 class="font-weight-bold mb-0">{{$totalDays}}</h2>
                                                    Nights
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" m-1">

                <div class="row">
                    <div class="col-12">
                        <form  action="{{ url('payment') }}" method="post" name="signup_form" id="signup_form_id">
                        {{ csrf_field() }}  
                        <div class="accordion" id="accordionExample">
                            <div class="card continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingOne">
                                    <div class="" data-toggle="collapse" data-target="#lead_guest" aria-expanded="true" aria-controls="collapseOne"> 
                                        Lead Guest Information
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>

                                <div id="lead_guest" class="collapse  show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Your contact details are collected in case we need to contact you about your booking. Additional personal information for each guest maybe required post the booking before vouchers can be issued. </p>
                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text" name="passenger_first_name[0]"  class="form-control passenger_first_name" value="{{ !empty($data['user'])?$data['user']->customer->first_name:'' }}" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family Name(As Shown or Passport)</label>
                                                    <input type="text" name="passenger_last_name[0]" class="passenger_last_name form-control" placeholder="Family Name" value="{{ !empty($data['user'])?$data['user']->customer->last_name:'' }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address</label>
                                                    <input type="text" name="passenger_email[0]" class="passenger_email form-control"  placeholder="Email Address" value="{{ !empty($data['user'])?$data['user']->customer->email:'' }}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth</label>
                                                    <input name="passenger_dob[0]" type="text" placeholder="DD MM YYYY" class="form-control passenger_dob datepicker1">
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control passenger_contact_no" name="passenger_contact_no[0]"  placeholder="Contact Number" value="{{ !empty($data['user'])?$data['user']->customer->contact_no:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(isset($travellers) && $travellers>1)
                            <div class="card continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingTwo">
                                    <div data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Additional Guest
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>
                                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <h5 class="mb-3">Additional Guest One</h5>
                                        @if(!empty($data['search_input']['num_of_adults']) && $data['search_input']['num_of_adults']['0'] > 1)
                                            @for($i=1; $i < $data['search_input']['num_of_adults']['0']; $i++ )
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Adults (18+)</label>
                                                    <div class="custom-select">
                                                      <select>
                                                         <option>Adult (18+)</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Guest Title</label>
                                                    <div class="custom-select passenger_title" name="passenger_title[{{$i}}]">
                                                        <select>
                                                            <option value="">Please Select</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Ms">Ms</option>
                                                            <option value="Mrs">Mrs</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text" name="passenger_first_name[{{$i}}]" class="passenger_first_name form-control" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family Name(As Shown or Passport)</label>
                                                    <input type="text" name="passenger_last_name[{{$i}}]" class="passenger_last_name form-control" placeholder="Family Name">
                                                </div>
                                            </div>
                                            
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth</label>
                                                    <input name="passenger_dob[{{$i}}]" type="text" placeholder="DD MM YYYY" class="form-control passenger_dob datepicker1">
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address</label>
                                                    <input type="email" name="passenger_email[{{$i}}]" class="form-control passenger_email" placeholder="kane@eroam.com" value="">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control passenger_contact_no"  name="passenger_contact_no[{{$i}}]" placeholder="Contact Number">
                                                </div>
                                            </div>
                                        </div>
                                        @endfor
                                    @endif
                                    @if(!empty($data['search_input']['num_of_children']) && $data['search_input']['num_of_adults']['0'] > 1)
                                        @for($i=1; $i < $data['search_input']['num_of_children']['0']; $i++ )
                                        <h5 class="mb-3">Additional Guest Two</h5>

                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Adults (18+)</label>
                                                    <div class="custom-select">
                                                      <select>
                                                        <option>Child (0 - 17)</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Child Age</label>
                                                    <div class="custom-select" name="child_age[$i]">
                                                        <select>
                                                            <option>2 Years</option>
                                                            <option>3 Years</option>
                                                            <option>4 Years</option>
                                                            <option>5 Years</option>
                                                            <option>6 Years</option>
                                                            <option>7 Years</option>
                                                            <option>8 Years</option>
                                                            <option>9 Years</option>
                                                            <option>10 Years</option>
                                                            <option>11 Years</option>
                                                            <option>12 Years</option>
                                                            <option>13 Years</option>
                                                            <option>14 Years</option>
                                                            <option>15 Years</option>
                                                            <option>16 Years</option>
                                                            <option>17 Years</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text"  name="child_first_name" class="form-control" placeholder="Kane">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family Name(As Shown or Passport)</label>
                                                    <input type="text" name="child_family_name" class="form-control" placeholder="Willems">

                                                </div>
                                            </div>
                                        </div>
                                            @endfor
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="card  continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingThree">
                                    <div class="" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Payment
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body pb-5">
                                        <h5 class="pb-3 ">Billing Contact Details</h5>

                                        <div class="row">

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text" name="billing_first_name" class="passenger_first_name form-control" value="{{ !empty($data['user'])?$data['user']->customer->first_name:'' }}" placeholder="First Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family Name(As Shown or Passport)</label>
                                                    <input type="text" name="billing_last_name" class="passenger_first_name form-control" value="{{ !empty($data['user'])?$data['user']->customer->last_name:'' }}" placeholder="Family Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth</label>
                                                    <input name="billing_passenger_dob" type="text" placeholder="DD MM YYYY" class="form-control passenger_dob datepicker1">
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address</label>
                                                    <input type="email" name="billing_email" class="passenger_email form-control" value="{{ !empty($data['user'])?$data['user']->customer->email:'' }}" placeholder="kane@eroam.com">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number</label>
                                                    <input type="text" name="billing_contact" class="passenger_contact_no form-control" value="{{ !empty($data['user'])?$data['user']->customer->contact_no:'' }}" placeholder="+61401070744">

                                                </div>
                                            </div>
                                        </div>


                                        <h5>Billing Address</h5>
                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Company Name</label>
                                                    <input type="text" name="passenger_company_name" class="form-control" placeholder="Company Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Street Address</label>
                                                    <input type="text" name="passenger_address_one" class="form-control" placeholder="Street Address">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Additional Address Information</label>
                                                    <input type="text" name="passenger_address_two" class="form-control" placeholder="Additional Address Infomation (Optional)">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Suburb / Town </label>
                                                    <input type="text" name="passenger_suburb" class="form-control passenger_suburb" placeholder="Suburb / Town">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>State Territory/Region </label>
                                                    <input type="text"  name="passenger_state" class="form-control passenger_state" placeholder="State / Territory / Region">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Postcode / Area Code</label>
                                                    <input type="text" name="passenger_zip" class="form-control passenger_zip" placeholder="Postcode / Area Code">
                                                </div>
                                            </div>


                                            <div class="col-12 col-sm-12  form-group">
                                                <div class="fildes_outer">
                                                    <label>Country</label>
                                                    <div class="custom-select">
                                                        <select name="passenger_country" class="passenger_country">
                                                            <option value="">Please Select</option>
                                                            @php
                                                            $j=0;
                                                            $allCountry = array();
                                                            foreach($countries as $country){
                                                                foreach ($country['countries'] as $country_data){
                                                                    $allCountry[$j]['name'] = $country_data['name'];
                                                                    $allCountry[$j]['id'] = $country_data['id'];
                                                                    $allCountry[$j]['region'] = $country['id'];
                                                                    $allCountry[$j]['regionName'] = $country['name'];
                                                                    $j++;
                                                                }
                                                            }
                                                            usort($allCountry, 'sort_by_name');
                                                            @endphp
                                                            @if( count($allCountry) > 0 )
                                                                @foreach($allCountry as $Country)
                                                                    <option value="{{ $Country['id'] }}">{{ $Country['name'] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="totalAmount" id="totalAmount" value="{{$finalTotal}}">
                                        <input type="hidden" name="currency" id="currency" value="{{$currency}}">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
                                                <h5 class="pb-2">Payment Method</h5>

                                                <div class="form-group black-checkbox mt-3">
                                                    <span class="custom_check">I agree to the terms and conditions. &nbsp; <input type="checkbox" id="checkbox-03" value="3" checked><span class="check_indicator">&nbsp;</span></span> <a href="#" class="blue">View Terms and Conditions.</a>
                                                </div>

                                                <div class="form-group black-checkbox mt-3">
                                                    <span class="custom_check">This package pricing is inclusive of flights &nbsp; <input type="checkbox" id="checkbox-04" value="3" checked><span class="check_indicator">&nbsp;</span></span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Credit Card Numbers</label>
                                                    <input type="text" name="card_number" id="card_number" class="form-control" placeholder="Credit Card Number">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 form-group">
                                                <div class="fildes_outer">
                                                    <label>Month*</label>
                                                    <div class="custom-select" >
                                                        <select name="month" id="month" data-stripe="exp_month">
                                                            <option value="">MM</option>
                                                            <?php $expiry_month = date('m');?>
                                                            <?php for($i = 1; $i <= 12; $i++) {
                                                            $s = sprintf('%02d', $i);?>
                                                            <option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 form-group">
                                                <div class="fildes_outer">
                                                    <label>Year*</label>
                                                    <div class="custom-select">
                                                        <select name="year" id="year" data-stripe="exp_year">
                                                            <option value="">YYYY</option>
                                                            <?php
                                                            $lastyear = date('Y')+21;
                                                            $curryear = date('Y');
                                                            for($k=$curryear;$k<$lastyear;$k++){ ?>
                                                            <option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Security code csv</label>
                                                    <input maxlength="3" name="cvv" id="cvv" type="text" placeholder="000" class="form-control">
                                                    <span class="arrow_down"><i class="fa fa-question-circle-o"></i> </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 form-group">
                                                <button type="submit" name=""  class="btn  btns_input_dark transform d-block w-100">Pay ${{$currency}} {{$finalTotal}} NOW  </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="card  continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="account">
                                    <div class="" data-toggle="collapse" data-target="#accounts" aria-expanded="false" aria-controls="collapseThree">
                                        Accounts
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>
                                <div id="accounts" class="collapse" aria-labelledby="account" data-parent="#accordionExample">
                                    <div class="card-body pb-5">
                                        <h5 class="pb-3 ">Billing Contact Details</h5>
                                        <div class="row">
                                            <div class="col-12">
                                                <table class="table mb-0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="border-0">

                                                                <div>
                                                                    <p> <a href="#" class="blue">Save Itinerary</a></p>

                                                                    <p> <a href="#" class="blue">View Saved Itineraries</a></p>

                                                                    <p> <a href="#" class="blue">Enter Promo Code</a> </p>
                                                                    <p> &nbsp; </p>

                                                                    <p class="pt-2">Contact Us +<a href="#" class="blue">61 (0)3 9999 6774 </a></p>

                                                                </div>
                                                            </td>
                                                            <td colspan="2" class="text-right border-0">
                                                                <p>Total Per Person</p>
                                                                <p>Sub Total Amount</p>
                                                                <p>Credit Card Fee 2.5%</p>
                                                                <p>Local Payment</p>
                                                                <p class="pt-2"><strong>Total Amount</strong></p>

                                                            </td>

                                                            <td class="text-right border-0">
                                                                <p>${{$currency}} {{number_format($totalCost, 2, '.', ',')}}</p>
                                                                <p>${{$currency}} {{number_format($total, 2, '.', ',')}}</p>
                                                                <p>${{$currency}} {{number_format($gst, 2, '.', ',')}}</p>
                                                                <p>${{$currency}} {{$finalTotal}}</p>

                                                                <p class="pt-2"><strong>${{$currency}} {{$finalTotal}}</strong></p>

                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush