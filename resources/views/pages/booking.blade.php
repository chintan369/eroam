@extends('layouts.search')

<?php
	$total = $totalCost * $travellers;
	$gst = $total*0.025;
	$finalTotal = number_format($total + $gst, 2, '.', ',');
	$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
?>
@section('custom-css')
<style type="text/css">
	#itr {
		padding: 2rem;
	}
	.itr-leg {
		background: #ffffff;
		margin-bottom: 2rem;
	}
	.itr-leg .heading {
		border: 1px solid #B2B2B2;
		border-bottom: 1px solid white;
		position: relative;
		bottom: -1px;
		display: inline-block;
		font-weight: 400;
		margin: 0;
		padding: 2rem;
	}
	.itr-leg .heading i:before {
		font-size: 25px;
		color: #2AA9DF;
	}
	.itr-leg .body {
		border: 1px solid #B2B2B2;
		padding: 2rem;
	}
	.itr-leg .body section {
		margin-bottom: 2rem;
	}
	.itr-leg .body .title {
		font-size: 17px;
		text-transform: uppercase;
	}
	.itr-leg .body .desc {
		margin-left: 8.8rem;
	}
	.itr-leg .body .desc p {
		line-height: 1em;
	}
	.itr-leg .body .desc p i.status-icon {
		margin-left: 1.5rem;
		margin-right: .2rem;
		color: #009688;
		font-weight: 700;
	}
	span.not-available,
	span.not-available i {
		color: #d32f2f !important;
	}
	.itr-leg .body .desc p span.price {
		font-weight: 700;
		font-size: 18px;
		color: #009688;
	}
	.itr-leg .body .title i:before {
		position: relative;
		top: 5px;
		border-radius: 100px;
		padding: 1.5rem;
		color: #ffffff;
		font-size: 25px;
		margin-right: 2.8rem;
	}
	.itr-leg .body .accomodation .title i:before {
		background: #00bcd4;
	}
	.itr-leg .body .activities .title i:before {
		background: #2196f3;
	}
	.itr-leg .body .transport .title i:before {
		background: #f44336;
	}
	.change-btn i {
		font-size: 1.6rem;
		margin-left: 3rem;
		color: #f8f8f8 !important;
		background: #2AA9DF;
		padding: 8px;
		display: inline-block;
		transition: all 100ms ease-in-out 0s;
	}
	.change-btn i:hover {
		text-decoration: none;
		background: #2792BF;
	}
	.old-price {
		text-decoration: line-through;
		color: #ABABAB;
		font-weight: 400;
		margin-left: 1rem;
	}
	#review-summary {
		border: 1px solid #B2B2B2;
	    padding: 5px;
	    margin-top: 11px;
	}
	#review-summary h3 {
		margin-top: 0;
	}
	#total {
		font-size: 16px;
	}
	.white-bg{min-height: initial;}
</style>
@endsection

@section('content')
	<?php
	$travellers   = session()->get( 'search' )['travellers'];   
    $total_childs = session()->get( 'search' )['child_total'];
    $rooms = session()->get( 'search' )['rooms'];
	?>
	<div class="tabs-container">
		<div class="tabs-container-inner">
			<h2 class="topTitle"><i class="icon icon-itinerary"></i><span>Book Itinerary</span></h2>
			<div class="row m-t-10">
				<div class="col-sm-3 date-control">
					<div class="panel-form-group form-group">
						<label class="label-control">Check-in Date:</label>
						<div class="input-group datepicker">
							<input  readonly type="text" value="{{$startDate}}" placeholder="16 May 2017" class="form-control disabled" data-toggle="tooltip" title="Not Available in Pilot">
							<span class="input-group-addon"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-3 date-control">
					<div class="panel-form-group form-group">
						<label class="label-control">Check-out Date:</label>
						<div class="input-group datepicker">
							<input readonly type="text" value="{{$endDate}}" placeholder="16 May 2018" class="form-control disabled" data-toggle="tooltip" title="Not Available in Pilot">
							<span class="input-group-addon"><i class="icon-calendar"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="panel-form-group form-group">
						<label class="label-control">Rooms</label>
						<input type="text" class="form-control disabled" data-toggle="tooltip" readonly title="Not Available in Pilot" value="{{$rooms}}">
						<!-- <select class="form-control disabled" readonly data-toggle="tooltip" title="Not Available in Pilot">
							<option>1</option>
							<option>2</option>
						</select> -->
					</div>
				</div>
				<div class="col-sm-2">
					<div class="panel-form-group form-group">
						<label class="label-control">Adults (18+)</label>
						<input type="text" value="{{$travellers}}" class="form-control disabled" readonly data-toggle="tooltip" title="Not Available in Pilot">
						<!-- <select class="form-control disabled" readonly data-toggle="tooltip" title="Not Available in Pilot">
							<?php for($k=1;$k<10;$k++){?>
							<option value="<?php echo $k;?>" <?php if($k == $travellers){?>selected<?php } ?>><?php echo $k;?></option>
								<?php } ?>

						</select> -->
					</div>
				</div>
				<div class="col-sm-2">
					<div class="panel-form-group form-group">
						<label class="label-control">Children (0-17)</label>
						<input type="text" class="form-control disabled" data-toggle="tooltip" readonly title="Not Available in Pilot" value="{{$total_childs}}">
						
					</div>
				</div>
			</div>
			<div class="m-t-20 row">
				<div class="col-sm-4">
					<a href="/proposed-itinerary" name="" class="btn btn-primary btn-block m-b-10">VIEW ITINERARY</a>
				</div>
				<div class="col-sm-4">
					<a href="#" class="btn btn-primary btn-block m-b-10" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">PRINT / SHARE ITINERARY</a>
				</div>
				<div class="col-sm-4">
					<a href="#" name="" class="btn btn-primary btn-block active m-b-10">BOOK ITINERARY</a>
				</div>
			</div>
		</div>
	</div>
	<div class="tabs-content-container">
		<div class="accomodation-wrapper">
			<div class="m-t-20 payment-steps">
				<ul>
					@foreach (session()->get('search')['itinerary'] as $key => $leg)
					<li>
						<a href="#">
							<span class="stepCircle">{{ $leg['city']['default_nights']}}</span>
							<p><strong>{{ str_limit($leg['city']['name'], 18) }}</strong><br/>{{ $leg['city']['country']['name'] }}</p>
						</a>
					</li>
					@endforeach

				</ul>
			</div>
			<div class="m-t-20">
				<div class="paymentMap">
					<div id="map" style="height: 400px;"></div>
				</div>
			</div>
			<div class="m-t-20 paymentTopbox">
				<div class="row">
					<div class="col-sm-7 m-t-5">
						<h4><strong>Multi-City Tailormade Auto</strong></h4>
						<p class="m-t-10">
							<i class="icon icon-calendar"></i>
							{{$startDate}} - {{$endDate}}
						</p>
					</div>
					<div class="col-sm-5">
						<div class="row">
							<div class="col-xs-9 price-right border-right">
								<p></p>
								<ul class="paymentIcons">
									<li><a href="#"><i class="icon icon-hotel"></i></a></li>
									<li><a href="#"><i class="icon icon-activity"></i></a></li>
									<li><a href="#"><i class="icon icon-bus"></i></a></li>
								</ul>
							</div>
							<div class="col-xs-3 text-center">
								<h3 class="transport-price"><strong>{{$totalDays}}</strong></h3>
								<span class="transport-nights">Days</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form class="horizontal-form" action="{{ url('payment') }}" method="post" name="signup_form" id="signup_form_id">
			{{ csrf_field() }}	
				<div class="panel-group paymentAccordian paymentAccordianNew" id="payaccordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingPicks1">
							<div class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#payaccordion" href="#collapsePicks1" aria-expanded="true" aria-controls="collapsePicks1" class="collapsed">Person Information<span class="collapseIcon"><i class="fa fa-minus-circle"></i></span></a>
							</div>
						</div>
						<div id="collapsePicks1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPicks1">
							<div class="panel-body">
								
								 	<?php 
                					for($i=0;$i<$travellers;$i++){
                						if($i == 0){
                					?>
                					<h4>Lead Person Information</h4>
                					<?php
                					}else{
                					?>	
                					<h4>PAX {{($i)}} Information</h4>
                					<?php 
                					}
                					?>
									<div class="panel-form-group form-group">
										<label class="label-control">Title</label>
										 <select class="form-control passenger_title" name="passenger_title[{{$i}}]">	
											<option value="">Please Select</option>
											<option value="Mr">Mr</option>
					                        <option value="Ms">Ms</option>
					                        <option value="Mrs">Mrs</option>
										</select>
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">First Name* (As Shown On Passport)</label>
										<input type="text" name="passenger_first_name[{{$i}}]" class="passenger_first_name form-control" placeholder="First Name">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Family Name* (As Shown On Passport)</label>
										<input type="text" name="passenger_last_name[{{$i}}]" class="passenger_last_name form-control" placeholder="Family Name">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Email Address* </label>
										<input type="text" name="passenger_email[{{$i}}]" class="form-control passenger_email" placeholder="Email Address">
									</div>
									<div class="date-control">
										<div class="panel-form-group form-group">
											<label class="label-control">Date of Birth*</label>
											<div class="input-group datepicker">
												<input name="passenger_dob[{{$i}}]" type="text" placeholder="DD MMM YYYY" class="form-control passenger_dob">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
											</div>
										</div>
									</div>
									<div class="panel-form-group form-group">
										<label class="label-control">Gender*</label>
										 <select class="form-control passenger_gender" name="passenger_gender[{{$i}}]">	
											<option value="">Please Select</option>
											<option value="Male">Male</option>
					                        <option value="Female">Female</option>
											<option value="Other">Other</option>
										</select>
									</div>
									
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Contact Number* </label>
										<input type="text" class="form-control passenger_contact_no"  name="passenger_contact_no[{{$i}}]" placeholder="Contact Number">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Address Line 1* </label>
										<input type="text" name="passenger_address_one[{{$i}}]" class="form-control passenger_address_one" placeholder="Address Line 1">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Address Line 2</label>
										<input type="text" name="passenger_address_two[{{$i}}]" class="form-control" placeholder="Address Line 2">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Suburb / Town* </label>
										<input type="text" name="passenger_suburb[{{$i}}]" class="form-control passenger_suburb" placeholder="Suburb / Town">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">State / Province* </label>
										<input type="text" name="passenger_state[{{$i}}]" class="form-control passenger_state" placeholder="State / Province">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Postcode / Zip* </label>
										<input type="text" name="passenger_zip[{{$i}}]" class="form-control passenger_zip" placeholder="Postcode / Zip">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Country*</label>
										<select class="form-control passenger_country" name="passenger_country[{{$i}}]">
											 <option value="">Please Select</option>
											<?php 
										    $j=0;
										    $allCountry = array();
										    foreach($countries as $country){
										      foreach ($country['countries'] as $country_data){
										          $allCountry[$j]['name'] = $country_data['name'];
										          $allCountry[$j]['id'] = $country_data['id'];
										          $allCountry[$j]['region'] = $country['id'];
										          $allCountry[$j]['regionName'] = $country['name'];
										          $j++;
										      } 
										    }
										    usort($allCountry, 'sort_by_name');
										  	?>	

											 @if( count($allCountry) > 0 )
												@foreach($allCountry as $Country)
													<option value="{{ $Country['id'] }}">{{ $Country['name'] }}</option>
												@endforeach
											  @endif
				                        </select>
									</div>
									<?php 
									}
									?>
								<p class="notes m-t-20">Your contact details are collected in case we need to contact you about your booking. If you do not complete your booking, then by clicking the ‘Continue’ button below you agree that we may contact you by email or by telephone (including by SMS) to follow up regarding your incomplete booking. For more information, please see our privacy policy.<a href="#"><strong>click here</strong></a>.</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingPicks2">
							<div class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#payaccordion" href="#collapsePicks2" aria-expanded="true" aria-controls="collapsePicks2" class="collapsed">Payment Confirmation <span class="collapseIcon"><i class="fa fa-minus-circle"></i></span></a>
							</div>
						</div>
						<div id="collapsePicks2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPicks1">
							<div class="panel-body">
								<div class="table-responsive paymentTable">
									<table class="table">
										<thead>
										<tr>
											<th>Item</th>
											<th class="text-right">Quantity</th>
											<th class="text-right">Cost</th>
										</tr>
										</thead>
										<tbody>
										
										<?php
											$activities = ''; 
											$transport = ''; 
											$hotel = ''; 
											$total_pax = session()->get( 'search' )['travellers'];
										?>
										@if (session()->get('search'))
										 @foreach (session()->get('search')['itinerary'] as $key => $leg)
										 	<?php 
										 			$nights = $leg

['city']['default_nights'];
													if(isset($leg['activities']) && !empty($leg['activities'])){
														
														$activities .="<tr><td>";
														foreach ($leg['activities'] as $key => $value) {
															$activity_price = '';
															if(isset($value['price']['0']['price'])){
								    							$activity_price = number_format($value['price']['0']['price'] * $travellers,2);
								    						}else{
								    							$activity_price = (isset($value['0']['price']) ? number_format($value['0']['price'] * $travellers,2) : '0.00');
								    						}
															$activities .="<span class='table-title'>Activity:</span> ".$value['name']."";
														}
														$activities .="</td><td class='text-right'>".$total_pax."</td><td class='text-right'>".$activity_price."</td></tr>";
													}
													if(isset($leg['transport']) && !empty($leg['transport'])){
														$transport_price = '';
														if(isset($leg['transport']['price']['0']['price']) && !empty($leg['transport']['price']['0']['price'])){
															$transport_price = number_format($leg['transport']['price']['0']['price'],2);
														}
														$transport .="<tr><td><span class='table-title'>Transport:</span> ".$leg['transport']['transport_name_text']."</td><td class='text-right'>".$total_pax."</td><td class='text-right'>".$transport_price."</td></tr>";
													}
													if(isset($leg['hotel']) && !empty($leg['hotel'])){
														$leg['hotel'] = json_decode(json_encode($leg['hotel']) , true );
														$singleRate 

= '@nightlyRateTotal';
													      
									                      $singleRate = $leg['hotel']

['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
									                      $singleRate = number_format

(($singleRate * $eroamPercentage) / 100 + $singleRate,2); 
									                      $subTotal = $singleRate;
$taxes = 0;
					  if(isset($leg['hotel']

['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])){
							$taxes =$leg['hotel']

['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
					  }
									                      $selectedRate = $subTotal + 

$taxes;
														$hotel 

.="<tr><td><span class='table-title'>Accommodation:</span> ".$leg['hotel']['name']." X ".$nights.($nights > 1 ? " Nights":" Night")."</td><td class='text-right'>".

$total_pax."</td><td class='text-right'>".$selectedRate."</td></tr>";
$total_pax."</td><td class='text-right'>".$selectedRate."</td></tr>";
$total_pax."</td><td class='text-right'>".$selectedRate."</td></tr>";
$total_pax."</td><td class='text-right'>".$selectedRate."</td></tr>";

													}
												?>
										 @endforeach
										@endif
										
										<?php
											echo $hotel;
											echo $activities; 
											echo $transport; 
											
										?>
										</tr>	
										<tr>
											<td class="text-right">
												<p>Total Per Person {{$currency}}</p>
												<p>Sub Total Amount {{$currency}}</p>
												<p>Credit Card Fee 2.5% {{$currency}}</p>
												<p><strong>Total Amount Payable {{$currency}}</strong></p>
											</td>
											<td class="text-right">
											</td>	
											<td class="text-right">
												<p>{{number_format($totalCost, 2, '.', ',')}}</p>
												<p>{{number_format($total, 2, '.', ',')}}</p>
												<p>{{number_format($gst, 2, '.', ',')}}</p>
												<p><strong>{{$finalTotal}}</strong></p>
											</td>
										</tr>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingPicks3">
							<div class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#payaccordion" href="#collapsePicks3" aria-expanded="true" aria-controls="collapsePicks3" class="collapsed">Credit Card Details<span class="collapseIcon"><i class="fa fa-minus-circle"></i></span></a>
							</div>
						</div>
						<input type="hidden" name="totalAmount" id="totalAmount" value="{{$finalTotal}}">
						<input type="hidden" name="currency" id="currency" value="{{$currency}}">
						<div id="collapsePicks3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPicks1">
							<div class="panel-body">
									<div class="panel-form-group input-control form-group">
										<label class="label-control">First Name*</label>
										<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Family Name*</label>
										<input type="text" name="last_name" id="last_name"class="form-control" placeholder="Family Name">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Credit Card Number* </label>
										<input type="text" name="card_number" id="card_number" class="form-control" placeholder="Credit Card Number">
									</div>
									<div class="panel-form-group form-group">
										<label class="label-control">Month*</label>
										 <select class="form-control" name="month" id="month" data-stripe="exp_month">
											<option value="">MM</option>
                                            <?php $expiry_month = date('m');?>
                                            <?php for($i = 1; $i <= 12; $i++) {
                                            $s = sprintf('%02d', $i);?>
											<option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                            <?php } ?>
										</select>
									</div>
									<div class="panel-form-group form-group">
										<label class="label-control">Year*</label>
										 <select class="form-control" name="year" id="year" data-stripe="exp_year">
											<option value="">YYYY</option>
                                            <?php
                                            $lastyear = date('Y')+21;
                                            $curryear = date('Y');
                                            for($k=$curryear;$k<$lastyear;$k++){ ?>
											<option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                            <?php } ?>
										</select>
									</div>
									<div class="date-control">
										<div class="panel-form-group form-group">
											<label class="label-control">CSV Number*</label>
											<div class="input-group datepicker">
												<input maxlength="3" name="cvv" id="cvv" type="text" placeholder="000" class="form-control">
												<span class="input-group-addon"><a href="#"><i class="fa fa-question-circle"></i></a></span>
											</div>
										</div>
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Country*</label>
										<input type="text" name="country" class="form-control" placeholder="Country">
									</div>
									<div class="panel-form-group input-control form-group">
										<label class="label-control">Postcode / Zip* </label>
										<input type="text" name="postalcode" class="form-control" placeholder="Postcode / Zip">
									</div>
									<div class="row m-t-30">
										<div class="col-sm-3 masterCard-icon">
											<img src="{{ url( 'images/visa.png' ) }}" alt="" class="img-responsive visa-icon" />
											<img src="{{ url( 'images/masterCard.png' ) }}" alt="" class="img-responsive" />
										</div>
										<div class="col-sm-9">
											<button type="submit" name="" class="btn btn-black btn-block btn-primary">PAY {{$currency}} {{$finalTotal}} NOW</button>
											<p class="notes m-t-20">Your credit card will be charged {{$currency}} {{$finalTotal}}. By clicking "Pay Now" below, you agree to these terms</p>
										</div>
									</div>
							</div>
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>

	<!---Start Price Check Modal -->
	@if(!empty($priceCheck))
	<div class="modal fade" id="priceModal" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-md" role="document">
	      <div class="modal-content">        
	        <div class="modal-body">
	          <h2>New Prices</h2>
	          <p class="text-center">Sorry selected room prices no longer exisit, Below are the new prices</p>	
	          <div class="roomType-inner m-t-20">
	          	
	          		@php $i=0; @endphp	 
	            	@foreach($priceCheck as $key =>$check)
	            		@php 
	            			if($i==0){
	            				$from_city = $check['city'];
	            				$leg = $key;
	            			}
	            			
	            			$i++; 
	            		@endphp
	            		<div class="m-t-20">
	              		<p>City : {{$check['city']}}</p>
	              		<p>Accomodation : {{$check['hotelName']}}</p>
	              		<p>New Rate : {{$check['newRate']}}</p>
	              		<p>Old Rate : {{$check['oldRate']}}</p>
	            		</div>
	            	@endforeach	
	            
	            
	            <div class="m-t-30 text-right">
	              <div class="modal-footer" style="background: none;">
			        <div class="row">
			          <div class="col-md-2"></div>
			          <div class="col-md-8 text-center">
			          	<a href="#" class="btn btn-primary ">Continue</a>
			          	<a href="{{url($from_city.'/hotels?leg='.$leg)}}" class="btn btn-primary ">No</a>
			         
			          </div>
			          <div class="col-md-2"></div>
			        </div>
			      </div>

	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
  	</div>
  	@endif
  <!---Close Modal -->

	<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
	<input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">

@endsection

@section('custom-js')

	<script type="text/javascript" src="{{ url('js/eroam-map.js') }}"></script>
<script src="{{ url( 'js/theme/bootstrap-datepicker.js' ) }}"></script>
<script type="text/javascript">
	
	var searchSession = JSON.parse($('#search-session').val());

	// REVIEW MODULE
	var review = (function() {

		function init() {

			// var transportObject = {
			// 	fare_source_code : 'ODAxMDE5JlRGJlRGJjRkYzRmMjExLTllYzgtNDlkZC04MjcxLTg0MjgwMDY0MzZmMyZURiY=',
			// 	price : '180.00',
			// 	currency : globalCurrency,
			// 	fare_type : 'Public'
			// };
			var leg = 0;

			// var formatTransport = eroam.formatTransport( searchSession.itinerary[ leg ].transport, leg );
			// validateMystifly( formatTransport, leg );

			//var formattedHotel = eroam.formatHotel( searchSession.itinerary[ leg ].hotel, leg );
			//validateAEHotel( formattedHotel, leg );

			buildItinerary();
		}


		function buildItinerary() {
			var itinerary = searchSession.itinerary;
			for (var i = 0; i < itinerary.length; i++) {
				var leg = itinerary[i];

				// HOTEL
				var hotel = eroam.formatHotel(leg.hotel, leg.city);
				buildHotel(hotel, i);

				// ACTIVITIES
				var activities = eroam.formatActivities(leg.activities);
				buildActivities(activities, i);

				// TRANSPORT
				if (i != itinerary.length - 1) {
					var nextLeg = itinerary[ i + 1 ];
					var transport = eroam.formatTransport(leg.transport, leg.city, nextLeg.city);
					buildTransport(transport, i);
				}
			}
		}


		function buildHotel(hotel, index) {
			$('.hotel-row').eq(index).html(
				'<div class="col-md-7">' +
					'<b><i class="fa fa-hotel"></i> Accomodation:</b>' +
					'<br>' +
					'<span>'+ hotel.name +' ('+hotel.room_name+')</span>' +
				'</div>' +
				'<div class="col-md-5 text-right">' +
					'<br>' +
					'<span class="bold-txt">$279.89</span>' +
					'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
					'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
				'</div>'
			);
		}


		function buildActivities(activities, index) {
			var activityList = '';
			if (activities.length > 0) {
				for (var i = 0; i < activities.length; i++) {
					var activity = activities[i];
					activityList += '<li>'+ moment(activity.date_selected).format('dddd, MMM D') +' - '+ activity.name +'</li>';
				}

				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b>' +
						'<ul>' +
							activityList +
						'</ul>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="not-available"><i class="fa fa-times-circle"></i> Unavailable</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			} else {
				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b><br>' +
						'<span>No activities included.</span>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			}
		}


		function buildTransport(transport, index) {
			if (transport.provider != 'own_arrangement') {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ transport.name +'</span><br>' +
						'<span><b>Depart:</b> '+ transport.departure +'</span><br>' +
						'<span><b>Arrive:</b> '+ transport.arrival +'</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<span class="old-price">$279.89</span>' +
						'<span class="bold-txt">$280.89</span>' +
						'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			} else {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ searchSession.itinerary[index].city.name +' to '+ searchSession.itinerary[index + 1].city.name +' (Own Arrangement)</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			}
		}


		function validateMystifly( t, leg )
		{
			var unvalidated = isNotUndefined( t.availability_checked_at ) ? isExpired( t.availability_checked_at ) : true;
			if( unvalidated )
			{
				MystiflyController.revalidate(
					t.fare_source_code,
					function( revalidateSuccessRS ) {

						var newPrice = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
						var newCurrency = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
						var convertedNewPrice = eroam.convertCurrency( newPrice, newCurrency );
						if( convertedNewPrice != t.price ) {
							searchSession.itinerary[ leg ].transport.price = convertedNewPrice;
							searchSession.itinerary[ leg ].transport.old_price = t.price;
						}
						searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();


						MystiflyController.flightRules(

							t.fare_source_code,

							function( flightRulesSuccessRS ) {
								var rules = FareRules.FareRule.RuleDetails.RuleDetail;

								if( t.fare_type != 'Public' ) {
								}else{
									if( Array.isArray( rules ) ) {
										rules.forEach(function( elem, key ) {
										});
									}else{
									}
								}

								searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();
								bookingSummary.update( searchSession );
							},

							function( flightRulesFailureRS ) {
							}
						);
					},

					function( revalidateFailureRS ) {
					}

				);
			}
		}

		
		function validateAEHotel( h, leg )
		{
			var unvalidated = isNotUndefined( h.availability_checked_at ) ? isExpired( h.availability_checked_at ) : true;
			
			if( unvalidated )
			{
				var rq = {
					FromDate: h.checkin,
					ToDate : h.checkout,
					Adults : searchSession.travellers,
					HotelId : h.hotel_id
				};
				AEController.checkHotelAvailability(
					rq,
					function( availabilitySuccessRS )
					{
						if( availabilitySuccessRS )
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq, 'availabilitySuccessRS', availabilitySuccessRS);

							availabilitySuccessRS[0].rooms.forEach(function( room ){
								if( room.room_id == h.room_id )
								{
									// AN ERROR OCCURED HERE. CANNOT ACCESS SEARCHSESSION VARIABLE HERE. CONTINUE HERE
									var roomPrice = eroam.convertCurrency( h.price, h.currency );
									if( room.price != roomPrice )
									{
										searchSession.itinerary[ leg ].hotel.price = room.price;
										searchSession.itinerary[ leg ].hotel.old_price = h.price;
									}
									searchSession.itinerary[ leg ].hotel.provider_id = room.provider_id;
									searchSession.itinerary[ leg ].hotel.availability_checked_at = moment().format();
									bookingSummary.update( JSON.stringify( searchSession ) );
								}
							});
						}					
						else
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'No data returned from validateAEHotel');
							// AE HOTEL NOT AVAILABLE
						}
					},
					function( availabilityFailureRS )
					{
						console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'availabilityFailureRS', availabilityFailureRS);
						// AE HOTEL NOT AVAILABLE
					}
				);
			}
		}


		function isExpired( availability_checked_at )
		{
			var difference = moment().diff( moment( availability_checked_at ), 'seconds' );
			return ( difference > 600 ) ? true : false ;
		}


		return {
			init: init,
		};


	})( MystiflyController, HBController );


	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();
		review.init();
	}); // END OF DOCUMENT READY




</script>
<script src="{{ url( 'js/theme/prettify.js' ) }}"></script>
<script src="{{ url( 'js/theme/jquery.slimscroll.js' ) }}"></script>
<script type="text/javascript">
    $("#checkin-date, #checkout-date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function calculateWidth(){
        var owidth = $('.payment-steps').parent().width() - 10;
        
        var elem = $('.payment-steps li').length;
        if (elem >6) {
            var elemWidth = owidth / 6;
        }else{
            var elemWidth = owidth / elem;
        }
        $(".payment-steps li").width(elemWidth);
    }
    $( window ).load( eMap.init );
    $(document).ready(function() {
        calculateWidth();
        calculateHeight();

    	$.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-z," "]+$/i.test(value);
		}, "Letters only please."); 
		$.validator.addMethod("numbersonly", function(value, element) {
		  return this.optional(element) || /^[0-9," "]+$/i.test(value);
		}, "Numbers only please."); 
		var todayDate = new Date().getDate();
    	$(".passenger_dob").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        $(".passenger_passport_expiry_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        $("#signup_form_id").validate({
        	ignore: [],
            rules: {
            	passenger_address_one: {
                    required: true
                },
                passenger_suburb: {
                    required: true
                },
                passenger_state: {
                    required: true
                },
                passenger_zip: {
                    required: true,
                    number:true
                },
            	passenger_title: {
                    required: true
                },
                passenger_gender: {
                    required: true
                },
                first_name: {
                    required: true,
                    lettersonly:true
                },
                last_name: {
                    required: true,
                    lettersonly:true
                },
                card_number: {
                    required: true,
                    number:true
                },
                year: {
                    required: true
                },
                cvv: {
                    required: true,
                    maxlength: 3,
                    number:true
                },
                month: {
                    required: true
                },
				country: {
				  required: true
				},
				 postalcode: {
				  required: true
				 }
            },

            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
               form.submit();

            }
        });
		
		$('.passenger_zip').each(function () {
		    $(this).rules('add', {
		        required: true,
                number:true
		    });
		});	
		$('.passenger_state').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_suburb').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_address_one').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		
		$('.passenger_gender').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_title').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
        $('.passenger_first_name').each(function () {
		    $(this).rules('add', {
		        required: true,
                lettersonly:true
		    });
		});
		$('.passenger_last_name').each(function () {
		    $(this).rules('add', {
		        required: true,
                lettersonly:true
		    });
		});
		$('.passenger_dob').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_contact_no').each(function () {
		    $(this).rules('add', {
		        required: true,
		        numbersonly:true,
		        maxlength: 15,
		    });
		});
		$('.passenger_email').each(function () {
		    $(this).rules('add', {
		        required: true,
		        email:true
		    });
		});
		$('.passenger_country').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_passport_expiry_date').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_passport_num').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
        
    });
    $(window).resize(function(){
        leftpanel();
        calculateHeight();
        leftStripHeight();
        calculateWidth()
    });
    $('.accomodation-wrapper').slimScroll({
        height: '100%',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });
    function calculateHeight(){
        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if(winHeight < oheight){
            $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else{
            $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
    }
    function map(){
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 11,

                center: new google.maps.LatLng(40.6700, -73.9400), 
                styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
            };
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map,
                title: 'eRoam!'
            });
        }
    }

</script>

@endsection
@section('custom-js')

<script type="text/javascript" src="{{ url('assets/js/eroam-map.js') }}"></script>
<script src="{{ url( 'assets/js/theme/bootstrap-datepicker.js' ) }}"></script>
<script type="text/javascript">
	
	var searchSession = JSON.parse($('#search-session').val());

	// REVIEW MODULE
	var review = (function() {

		function init() {
			var leg = 0;
			buildItinerary();
		}


		function buildItinerary() {
			var itinerary = searchSession.itinerary;
			for (var i = 0; i < itinerary.length; i++) {
				var leg = itinerary[i];

				// HOTEL
				var hotel = eroam.formatHotel(leg.hotel, leg.city);
				buildHotel(hotel, i);

				// ACTIVITIES
				var activities = eroam.formatActivities(leg.activities);
				buildActivities(activities, i);

				// TRANSPORT
				if (i != itinerary.length - 1) {
					var nextLeg = itinerary[ i + 1 ];
					var transport = eroam.formatTransport(leg.transport, leg.city, nextLeg.city);
					buildTransport(transport, i);
				}
			}
		}


		function buildHotel(hotel, index) {
			$('.hotel-row').eq(index).html(
				'<div class="col-md-7">' +
					'<b><i class="fa fa-hotel"></i> Accomodation:</b>' +
					'<br>' +
					'<span>'+ hotel.name +' ('+hotel.room_name+')</span>' +
				'</div>' +
				'<div class="col-md-5 text-right">' +
					'<br>' +
					'<span class="bold-txt">$279.89</span>' +
					'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
					'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
				'</div>'
			);
		}


		function buildActivities(activities, index) {
			var activityList = '';
			if (activities.length > 0) {
				for (var i = 0; i < activities.length; i++) {
					var activity = activities[i];
					activityList += '<li>'+ moment(activity.date_selected).format('dddd, MMM D') +' - '+ activity.name +'</li>';
				}

				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b>' +
						'<ul>' +
							activityList +
						'</ul>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="not-available"><i class="fa fa-times-circle"></i> Unavailable</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			} else {
				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b><br>' +
						'<span>No activities included.</span>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			}
		}


		function buildTransport(transport, index) {
			if (transport.provider != 'own_arrangement') {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ transport.name +'</span><br>' +
						'<span><b>Depart:</b> '+ transport.departure +'</span><br>' +
						'<span><b>Arrive:</b> '+ transport.arrival +'</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<span class="old-price">$279.89</span>' +
						'<span class="bold-txt">$280.89</span>' +
						'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			} else {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ searchSession.itinerary[index].city.name +' to '+ searchSession.itinerary[index + 1].city.name +' (Own Arrangement)</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			}
		}


		function validateMystifly( t, leg )
		{
			var unvalidated = isNotUndefined( t.availability_checked_at ) ? isExpired( t.availability_checked_at ) : true;			
			if( unvalidated )
			{
				MystiflyController.revalidate(
					t.fare_source_code,
					function( revalidateSuccessRS ) {

						var newPrice = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
						var newCurrency = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
						var convertedNewPrice = eroam.convertCurrency( newPrice, newCurrency );
						if( convertedNewPrice != t.price ) {
							// NEW PRICE DOES NOT MATCH THE OLD PRICE
							// NEW PRICE OF FLIGHT IS STORE IN VARIABLE "convertedNewPrice"
							// UPDATE THE PRICE UI WITH THE NEW PRICE & USE "globalCurrency" VARIABLE AS THE CURRENCY
							searchSession.itinerary[ leg ].transport.price = convertedNewPrice;
							searchSession.itinerary[ leg ].transport.old_price = t.price;
						}
						searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();

						MystiflyController.flightRules(

							t.fare_source_code,

							function( flightRulesSuccessRS ) {
								var rules = FareRules.FareRule.RuleDetails.RuleDetail;

								if( t.fare_type != 'Public' ) {
								}else{
									if( Array.isArray( rules ) ) {
										rules.forEach(function( elem, key ) {
											
										});
									}else{
									}
								}

								searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();
								bookingSummary.update( searchSession );
							},

							function( flightRulesFailureRS ) {
								// MYSTIFLY TRANSPORT NOT AVAILABLE
							}
						);
					},

					function( revalidateFailureRS ) {
						// MYSTIFLY TRANSPORT NOT AVAILABLE
					}

				);
			}
		}
		
		function validateAEHotel( h, leg )
		{
			var unvalidated = isNotUndefined( h.availability_checked_at ) ? isExpired( h.availability_checked_at ) : true;
			if( unvalidated )
			{
				var rq = {
					FromDate: h.checkin,
					ToDate : h.checkout,
					Adults : searchSession.travellers,
					HotelId : h.hotel_id
				};
				AEController.checkHotelAvailability(
					rq,
					function( availabilitySuccessRS )
					{
						if( availabilitySuccessRS )
						{

							availabilitySuccessRS[0].rooms.forEach(function( room ){
								if( room.room_id == h.room_id )
								{
									// AN ERROR OCCURED HERE. CANNOT ACCESS SEARCHSESSION VARIABLE HERE. CONTINUE HERE
									var roomPrice = eroam.convertCurrency( h.price, h.currency );
									if( room.price != roomPrice )
									{
										searchSession.itinerary[ leg ].hotel.price = room.price;
										searchSession.itinerary[ leg ].hotel.old_price = h.price;
									}
									searchSession.itinerary[ leg ].hotel.provider_id = room.provider_id;
									searchSession.itinerary[ leg ].hotel.availability_checked_at = moment().format();
									bookingSummary.update( JSON.stringify( searchSession ) );
								}
							});
						}					
						else
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'No data returned from validateAEHotel');
							// AE HOTEL NOT AVAILABLE
						}
					},
					function( availabilityFailureRS )
					{
						console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'availabilityFailureRS', availabilityFailureRS);
						// AE HOTEL NOT AVAILABLE
					}
				);
			}
		}


		function isExpired( availability_checked_at )
		{
			var difference = moment().diff( moment( availability_checked_at ), 'seconds' );
			return ( difference > 600 ) ? true : false ;
		}


		return {
			init: init,
		};


	})( MystiflyController, HBController );


	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();
		review.init();
	}); // END OF DOCUMENT READY




</script>
<script src="{{ url( 'assets/js/theme/prettify.js' ) }}"></script>
<script src="{{ url( 'assets/js/theme/jquery.slimscroll.js' ) }}"></script>
<script type="text/javascript">
    $("#checkin-date, #checkout-date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function calculateWidth(){
        var owidth = $('.payment-steps').parent().width() - 10;
        //alert(owidth);
        var elem = $('.payment-steps li').length;
        //alert(owidth);
        //alert(elem);
        if (elem >6) {
            var elemWidth = owidth / 6;
        }else{
            var elemWidth = owidth / elem;
        }
        $(".payment-steps li").width(elemWidth);
    }
    $( window ).load( eMap.init );
    $(document).ready(function() {
        calculateWidth();
        calculateHeight();

    	$.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-z," "]+$/i.test(value);
		}, "Letters only please."); 
		$.validator.addMethod("numbersonly", function(value, element) {
		  return this.optional(element) || /^[0-9," "]+$/i.test(value);
		}, "Numbers only please."); 
		var todayDate = new Date().getDate();
    	$(".passenger_dob").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        $(".passenger_passport_expiry_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        $("#signup_form_id").validate({
        	ignore: [],
            rules: {
            	passenger_address_one: {
                    required: true
                },
                passenger_suburb: {
                    required: true
                },
                passenger_state: {
                    required: true
                },
                passenger_zip: {
                    required: true,
                    number:true
                },
            	passenger_title: {
                    required: true
                },
                passenger_gender: {
                    required: true
                },
                first_name: {
                    required: true,
                    lettersonly:true
                },
                last_name: {
                    required: true,
                    lettersonly:true
                },
                card_number: {
                    required: true,
                    number:true
                },
                year: {
                    required: true
                },
                cvv: {
                    required: true,
                    maxlength: 3,
                    number:true
                },
                month: {
                    required: true
                },
				country: {
				  required: true
				},
				 postalcode: {
				  required: true
				 }
            },

            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
				form.submit();

            }
        });
		
		$('.passenger_zip').each(function () {
		    $(this).rules('add', {
		        required: true,
                number:true
		    });
		});	
		$('.passenger_state').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_suburb').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_address_one').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		
		$('.passenger_gender').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_title').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
        $('.passenger_first_name').each(function () {
		    $(this).rules('add', {
		        required: true,
                lettersonly:true
		    });
		});
		$('.passenger_last_name').each(function () {
		    $(this).rules('add', {
		        required: true,
                lettersonly:true
		    });
		});
		$('.passenger_dob').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_contact_no').each(function () {
		    $(this).rules('add', {
		        required: true,
		        numbersonly:true,
		        maxlength: 15,
		    });
		});
		$('.passenger_email').each(function () {
		    $(this).rules('add', {
		        required: true,
		        email:true
		    });
		});
		$('.passenger_country').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_passport_expiry_date').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_passport_num').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
        
    });
    $(window).resize(function(){
        leftpanel();
        calculateHeight();
        leftStripHeight();
        calculateWidth()
    });
    $('.accomodation-wrapper').slimScroll({
        height: '100%',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });
    function calculateHeight(){
        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if(winHeight < oheight){
            $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else{
            $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
    }
    function map(){
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
            };
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map,
                title: 'eRoam!'
            });
        }
    }

</script>

@endsection
