@extends('layouts.tour')

@section('custom-css')
  <style type="text/css">
    .tabs-wrapper {
        margin-left: 350px;
        padding-top: 48px;
    }

    #tours-loader{
      width: 100%;
      /*position: absolute;
      top: 10%;
      transform: translateY( -50% );*/
      text-align: center;
      margin: 10px 0px -20px 0px;
    }

    #more-loader {
      width: 100%;
      position: absolute;
      bottom: 0%;
      transform: translateY( -2% );
      text-align: center;
     display: none;
    }

    #tours-loader span, #more-loader span{ 
      padding: 8px 21px;
      font-size: 18px;
      font-family: "HelveticaNeue";
    }

    .modelRegionCountry hr{
        margin-top: 5px;
    }

    .modelCountryList label{
      font-size: 12px;
    }

    .modelCountryList label.radio-checkbox span{
        color: rgba(0,0,0, .54);
    }

    .search_box {
      border: 1px solid #F3F3F3;
      border-radius: 4px;
    }
  </style>
@stop

<?php //echo '<pre>'; print_r($tourCountries); echo '</pre>'; die;?>

@section('content')

        <div class="tabs-container">
          <div class="row">
            <div class="col-sm-6">
              <div class="m-t-10">
                <?php /*<div class="panel-icon">    
                  <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                      <!-- Generator: Sketch 43.2 (39069) - https://www.bohemiancoding.com/sketch -->
                      <desc>Created with Sketch.</desc>
                      <defs>
                          <polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
                      </defs>
                      <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-503.000000, -69.000000)">
                              <g id="placeholder" transform="translate(503.000000, 69.000000)">
                                  <g id="Group">
                                      <mask id="mask-2" fill="white">
                                          <use xlink:href="#path-1"></use>
                                      </mask>
                                      <g id="Clip-2"></g>
                                      <path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
                                  </g>
                              </g>
                          </g>
                      </g>
                  </svg>
                </div>*/ ?>

                <div class="panel-container"> 
                   <?php if(array_key_exists('countryNames', $tourCountries)){ 
                        $tCount = 0;
                        foreach ($tourCountries['countryNames'] as $country_name) 
                        {
                          $tCount = $tourCount[$country_name] + $tCount;
                        }
                    ?> 
                    <h4>
                      <!-- <span class="countryName">{{ $tourCountries['countryNames'][0] }}</span> --> Total <span class="tour-count"> : 0 Tours Found</span>
                    </h4>
                  <?php } else { ?>
                    <h4><span class="tour-count"></span></h4>
                  <?php } ?>
                  <p class="m-t-5">
                    <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 43.2 (39069) - https://www.bohemiancoding.com/sketch -->
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-535.000000, -95.000000)" fill="#212121">
                                <g id="Page-1" transform="translate(535.000000, 95.000000)">
                                    <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
                                    <path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
                                </g>
                            </g>
                        </g>
                    </svg> 
                    <span id="dept_dates"></span>
                  </p>
                </div>
              </div>
            </div>


            <div class="col-sm-6 m-t-10">
                <div class="input-group search-control search_box">
                 <input type="text" class="form-control" name="tourSearch" placeholder="Search" id="tourSearch" value="<?php if(!empty($_POST) && array_key_exists('tourSearch', $_POST) ) { echo $_POST['tourSearch']; } ?>">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="getTourSearch();"><i class="fa fa-search"></i></button>
                  </span>
                </div>
            </div>
          </div>

          <div class="m-t-10">
            <hr/>
          </div>

          <div class="row m-t-10">
            <div class="col-sm-10">
              <div class="custom-tabs custom-tabs-new" data-example-id="togglable-tabs"> 
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                  <li role="presentation" class="active" style="width: 248px"><a href="" class="disabled" id="top-picks-tab" role="tab" aria-controls="top-picks" aria-expanded="true" disabled  data-toggle="tooltip" title="Not Available in Pilot">TOP PICKS</a>
                  </li>                  

                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle disabled" id="myTabDrop1"  aria-controls="myTabDrop1-contents"  disabled data-toggle="tooltip" title="Not Available in Pilot"> TYPE <span class="caret" ></span></a> 
                    
                  </li>

                  <!-- <li role="presentation" class="dropdown" style="width: 248px"> <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents">TYPE <span class="fa fa-caret-down"></span></a> 
                      <ul class="dropdown-menu catDrp" aria-labelledby="myTabDrop1" id="category-tab">
                          <li><a id="searchCategory">All</a></li>
                      </ul>
                  </li>
 -->
                  <li role="presentation" class="dropdown" style="width: 248px"> <a href="#" class="dropdown-toggle" id="priceSortAsc" data-toggle="dropdown" aria-controls="myTabDrop3-contents"> PRICE <span class="fa fa-caret-down"></span></a>
                  </li>
                  
                  <!-- <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle disabled" id="myTabDrop1"  aria-controls="myTabDrop1-contents"  disabled data-toggle="tooltip" title="Not Available in Pilot"> RATING <span class="caret" ></span></a> 
                    
                  </li>

                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle disabled" id="myTabDrop2" aria-controls="myTabDrop2-contents"  disabled data-toggle="tooltip" title="Not Available in Pilot">STARS <span class="caret"></span></a> 
                    
                  </li> 

                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle disabled" id="myTabDrop4" aria-controls="myTabDrop4-contents"  disabled data-toggle="tooltip" title="Not Available in Pilot">DISTANCE FROM CITY <span class="caret"></span></a> 
                    
                  </li> -->
                </ul> 
              </div>
            </div>

            <?php /*<div class="col-sm-2 content-toogle-icon">
              <a href="#" class="list-icon">
                <svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                    <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-1325.000000, -501.000000)">
                            <g id="menu" transform="translate(1322.000000, 495.000000)">
                                <rect id="bounds" x="0" y="0" width="24" height="24"></rect>
                                <path d="M3,18 L21,18 L21,16 L3,16 L3,18 L3,18 Z M3,13 L21,13 L21,11 L3,11 L3,13 L3,13 Z M3,6 L3,8 L21,8 L21,6 L3,6 L3,6 Z" id="Shape" fill="#000000"></path>
                            </g>
                        </g>
                    </g>
                </svg>
              </a>
              <a href="04.3-Homepage-Activities.html" class="grid-icon">
                <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                    <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.397644928">
                        <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-1373.000000, -499.000000)">
                            <g id="apps" transform="translate(1369.000000, 495.000000)">
                                <path d="M4,8 L8,8 L8,4 L4,4 L4,8 L4,8 Z M10,20 L14,20 L14,16 L10,16 L10,20 L10,20 Z M4,20 L8,20 L8,16 L4,16 L4,20 L4,20 Z M4,14 L8,14 L8,10 L4,10 L4,14 L4,14 Z M10,14 L14,14 L14,10 L10,10 L10,14 L10,14 Z M16,4 L16,8 L20,8 L20,4 L16,4 L16,4 Z M10,8 L14,8 L14,4 L10,4 L10,8 L10,8 Z M16,14 L20,14 L20,10 L16,10 L16,14 L16,14 Z M16,20 L20,20 L20,16 L16,16 L16,20 L16,20 Z" id="Shape" fill="#000000" opacity="0.539999962"></path>
                                <rect id="bounds" x="0" y="0" width="24" height="24"></rect>
                            </g>
                        </g>
                    </g>
                </svg>
              </a>
            </div>*/ ?>

            <div class="col-sm-2 content-toogle-icon">
              <ul class="nav nav-tabs" id="myTabs" role="tablist">

                <li role="presentation" class="active">
                  <a href="#listview" id="listview-tab" role="tab" data-toggle="tab" aria-controls="listview" aria-expanded="true">
                    <svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                      <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
                      <desc>Created with Sketch.</desc>
                      <defs></defs>
                      <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.45">
                          <g id="04.03-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-1325.000000, -501.000000)">
                              <g id="menu" transform="translate(1322.000000, 495.000000)">
                                  <rect id="bounds" x="0" y="0" width="24" height="24"></rect>
                                  <path d="M3,18 L21,18 L21,16 L3,16 L3,18 L3,18 Z M3,13 L21,13 L21,11 L3,11 L3,13 L3,13 Z M3,6 L3,8 L21,8 L21,6 L3,6 L3,6 Z" id="Shape" fill="#000000"></path>
                              </g>
                          </g>
                      </g>
                    </svg>
                  </a>
                </li> 

                <li role="presentation" class="dropdown">
                  <a href="#gridview" role="tab" id="gridview-tab" data-toggle="tab" aria-controls="gridview">
                    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                      <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
                      <desc>Created with Sketch.</desc>
                      <defs></defs>
                      <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.397644928">
                          <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-1373.000000, -499.000000)">
                              <g id="apps" transform="translate(1369.000000, 495.000000)">
                                  <path d="M4,8 L8,8 L8,4 L4,4 L4,8 L4,8 Z M10,20 L14,20 L14,16 L10,16 L10,20 L10,20 Z M4,20 L8,20 L8,16 L4,16 L4,20 L4,20 Z M4,14 L8,14 L8,10 L4,10 L4,14 L4,14 Z M10,14 L14,14 L14,10 L10,10 L10,14 L10,14 Z M16,4 L16,8 L20,8 L20,4 L16,4 L16,4 Z M10,8 L14,8 L14,4 L10,4 L10,8 L10,8 Z M16,14 L20,14 L20,10 L16,10 L16,14 L16,14 Z M16,20 L20,20 L20,16 L16,16 L16,20 L16,20 Z" id="Shape" fill="#000000" opacity="0.539999962"></path>
                                  <rect id="bounds" x="0" y="0" width="24" height="24"></rect>
                              </g>
                          </g>
                      </g>
                    </svg>
                  </a>
                </li>
                
              </ul>         
            </div>
          </div>
        </div>

        <div class="tabs-content-container tour-tabs-container">
            <div class="tourListGridBox">
          <div class="tab-content" id="myTabContent"> 

            <div id="tours-loader">
              <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
            </div>

            <div class="tab-pane fade in active" role="tabpanel" id="listview" aria-labelledby="listview-tab">
                <div class="tabs-content-wrapper">
              <div class="list-wrapper new-list-wrapper" id="listTourList">
              </div>
                </div>
            </div>
                
            <div class="tab-pane fade" role="tabpanel" id="gridview" aria-labelledby="gridview-tab">
                <div class="tabs-content-wrapper">
              <div class="grid-wrapper grid-tours row" id="gridTourList">  
              </div>
                </div>
            </div> 

            <div id="more-loader">
              <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
            </div>

          </div>
            </div>
        </div>

@stop
@section( 'custom-js' )

  <script type="text/javascript">

      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
      });

        var countryName       = '{{ $countryName }}';
        var default_currency  = '{{ $default_currency }}';
        var start_page        = 0; //initial page
        var end_record_text   = 'No more tours found!'; //no more records to load
        var loading           = false; 
        var end_record        = false;
        var totalTourCount    = parseInt(0);
        var tourTitle         = $("#tourSearch").val();

        function removeLastComma(strng){
            strng = strng.trim();
            if(strng[strng.length-1] == ','){
                var strng=strng.substring(0,strng.length-1)
            }
            return strng;
        }

        $(document).on('click', '#priceSortAsc', function() { //alert(1);
            sortMeBy('data-price', 'div.new-list-wrapper', 'div.list-box', 'asc');
            sortMeBy('data-price', 'div.grid-tours', 'div.gridBox', 'asc');
            $('#priceSortAsc span').removeClass('fa-caret-down').addClass('fa-caret-up');
            $('#priceSortAsc').attr('id', 'priceSortDesc');
        });

        $(document).on('click', '#priceSortDesc', function() {
            $('#priceSortDesc span').removeClass('fa-caret-up').addClass('fa-caret-down');
            sortMeBy('data-price', 'div.grid-tours', 'div.gridBox', 'desc');
            sortMeBy('data-price', 'div.new-list-wrapper', 'div.list-box', 'desc');
            $('#priceSortDesc').attr('id', 'priceSortAsc');
        });

        function getTourSearch(){
          var search_val = $('#tourSearch').val();
          if(search_val){
            search_val = search_val.toLowerCase();
            var list = $("div.new-list-wrapper div.list-box");            
            $(list).fadeOut("fast");            
            var totalCount = 0;            
            $("div.new-list-wrapper").find("div.list-box[data-tourname*='" + search_val + "']").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
            var list = $("div.grid-tours div.gridBox");
            $(list).fadeOut("fast");
            var totalCount = 0;
            $("div.grid-tours").find("div.gridBox[data-tourname*='" + search_val + "']").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
            checkPropertyCountForSearch(totalCount);
          }else{
            var list = $("div.new-list-wrapper div.list-box");
            $(list).fadeOut("fast");
            var totalCount = 0;
            search_val = 'eRoam';
            $("div.new-list-wrapper").find("div.list-box[data-provider*='" + search_val + "']").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
             var list = $("div.grid-tours div.gridBox");
            $(list).fadeOut("fast");
            var totalCount = 0;
            $("div.grid-tours").find("div.gridBox[data-provider*='" + search_val + "']").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
            checkPropertyCountForSearch(totalCount);
            $('#loadMore').show();
          }
        }

        $('#tourSearch').keyup(function(){
          console.log('asdasd');
          if($(this).val() == ''){
             var list = $("div.new-list-wrapper div.list-box");
              $(list).fadeOut("fast");
              var totalCount = 0;
              search_val = 'eRoam';
              $("div.new-list-wrapper").find("div.list-box[data-provider*='" + search_val + "']").each(function (i) {
                  totalCount ++;
                  $(this).delay(200).slideDown("fast");
              });
              var list = $("div.grid-tours div.gridBox");
              $(list).fadeOut("fast");
              var totalCount = 0;
              $("div.grid-tours").find("div.gridBox[data-provider*='" + search_val + "']").each(function (i) {
                  totalCount ++;
                  $(this).delay(200).slideDown("fast");
              });
              checkPropertyCountForSearch(totalCount);
              $('#loadMore').show();
          }
        });

        function checkPropertyCountForSearch(totalPropertyCount)
        {
          console.log(totalPropertyCount);
            $('div.list-wrapper').find('.noHotelFound').closest('h4').remove();
            $('.tour-count').html( 'Total ' + totalPropertyCount + ( (totalPropertyCount == 1 || totalPropertyCount == 0) ? ' Tour' : ' Tours' )+ ' Found');
            if( totalPropertyCount == 0 )
            {
                var html = '<h4 class="text-center blue-txt bold-txt noHotelFound">No Tours Available.</h4>';
                $('div.list-wrapper').append( html );
            }
            //next();
        }
        $(document).on('click', '#searchCategory', function() {
            //$("#transport-tab").children().removeClass("active");
            var provider = $(this).text(); //alert(provider);
            filterListOperator(provider);
            $("#category-tab").children().removeClass("active");
            $(this).parent().addClass("active");
        });

        function filterListOperator(value) {
            var list = $("div.grid-tours div.gridBox");
            var list1 = $("div.new-list-wrapper div.list-box");
            $(list).fadeOut("fast");
            $(list1).fadeOut("fast");
            var totalCount = 0;
            if (value == "All") {
                $("div.grid-tours").find("div.gridBox").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");
                });
                $("div.new-list-wrapper").find("div.list-box").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");
                });
            } else { //alert(1);
                //Notice this *=" <- This means that if the data-category contains multiple options, it will find them
                //Ex: data-category="Cat1, Cat2"
                $("div.grid-tours").find("div.gridBox[data-all-category*='" + value + "']").each(function (i) {
                    totalCount ++;                    
                    $(this).delay(200).slideDown("fast");
                });

                $("div.new-list-wrapper").find("div.list-box[data-all-category*='" + value + "']").each(function (i) {
                    totalCount ++;                    
                    $(this).delay(200).slideDown("fast");
                });
            }
            checkTransportCountForSearch(totalCount);
        }

        function checkTransportCountForSearch(totalTransportCount)
        {
            //$('.trans-count').html( ': Top ' + totalTransportCount + ( (totalTransportCount == 1 || totalTransportCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
            if( totalTransportCount == 0 )
            {
                var html = '<h4 class="text-center">No Transport Found.</h4>';
                $('#listTourList').append( html );
            }
            //next();
        }


        function sortMeBy(arg, sel, elem, order) 
        {
            var $selector = $(sel),
                $element = $selector.children(elem);
               console.log($element);
            $element.sort(function(a, b) {
                
                var an = parseInt(a.getAttribute(arg)),
                    bn = parseInt(b.getAttribute(arg));
                if (order == 'asc') {
                    if (an > bn)
                        return 1;
                    if (an < bn)
                        return -1;
                } else if (order == 'desc') {
                    if (an < bn)
                        return 1;
                    if (an > bn)
                        return -1;
                }
                return 0;
            });

            $element.detach().appendTo($selector);
        }
        $(document).ready(function(){
          tourList(tourTitle);
         

      /*document.onload = function() {
          inactivityTime();
      };

      document.onmousemove = function() {
          inactivityTime();

      };
      document.onmousedown = function() {
          inactivityTime();
      };
      document.onkeypress = function() {
          inactivityTime();
      };
      document.ontouchstart = function() {
          inactivityTime();
      };

      var inactivityTime = function() {
        if( '{{ isset($page_will_expire) ? 1 : 0 }}' == 1 )
        {
          var t;
          var timer
          document.onload = resetTimer;
          window.onload = resetTimer;
          document.onmousemove = resetTimer;
          document.onkeypress = resetTimer;

          function showInactiveNotification() {
            if(! ($("#session-expire").data('bs.modal') || {isShown: false}).isShown ){
              var counter = 60;
              
              //$('.session-home-btn').html('Home('+counter+')');
              $('#session-expire').modal('show');
              timer = setInterval(function(){
                //$('.session-home-btn').html('Home('+counter+')');
                // if( counter == 0 ){
                //   //$('.session-home-btn').html('Home');
                //   $('.session-home-btn').prop('disabled', true);
                //   $('.session-continue-btn').prop('disabled', true);
                //   clearInterval(timer);
                //   window.location = '{{ url("") }}';
                // }
                counter--;
              }, 1000);
            }
          }

            function resetTimer() {
                clearTimeout(t);
                clearInterval(timer);
                t = setTimeout(showInactiveNotification, 600000);
            }

            $('.session-home-btn').click(function(){
                location.reload();
            });

            $('.session-continue-btn').click(function(){
                clearTimeout(t);
                clearInterval(timer);
                $('#session-expire').modal('hide');
            });
            $('.session-continue-btn').click(function(){
                window.location = '{{ url("/") }}';
                //clearTimeout(t);
                // clearInterval(timer);
                // $('#session-expire').modal('hide');
            });
        }
        };*/

        $(window).scroll(function() { //detact scroll
            if($(window).scrollTop() + $(window).height() >= $(document).height()){ //scrolled to bottom of the page
                  $("#more-loader").show();
                  tourList(tourTitle); //load content chunk 
                }
            });

            $('body').on('click', '#searchTour', function() {
                $("#tours-loader").show();
                loading       = false; 
                end_record    = false;
                start_page    = 0;
                totalTourCount= parseInt(0);
                $('#gridTourList').html('');
                $('#listTourList').html('');
                var tourTitle = $("#tourSearch").val();
                tourList(tourTitle);
            }); 

            $("#search-form").submit(function( event ){
                $('#searchTour').click();
            });
        })    

        function getDistinctCategory()
        {
            
            var items = {};
            $('div.list-box').each(function() {
                items[$(this).attr('data-category')] = true;
            });

            var html = '';
            var result = new Array();            
            for(var i in items)
            {
                if(i != 'N/A'){
                    result.push(i);
                    html += '<li><a id="searchCategory">'+i+'</a></li>';
                }

            }
            $('#category-tab').append( html );
        }
        //Ajax load function
        function tourList(tourTitle){

        var record_end_txt = '';//$('<div/>').text(end_record_text).addClass('end-record-info'); //end record text
        
        if(loading == false && end_record == false){
          loading = true; //set loading flag on

          var countryNames = [];
          $('.countryList:checkbox:checked').each(function(){
              var name = $(this).attr('data-name');
              countryNames.push(name);
              totalTourCount = totalTourCount + parseInt($("#count"+name).text());
          })


          var countryID = [];
          <?php if($search_type == 'city') { ?>        
            countryID.push('<?php echo $search_value; ?>');            
          <?php } else { ?>
          $('.countryList:checkbox:checked').each(function(){
              var id = $(this).val();
              countryID.push(id);
              totalTourCount = totalTourCount + parseInt($("#count"+id).text());

          });
          <?php }?>


          var remove_country_ids = '<?php echo $remove_country_id; ?>';
          var res_country_id = remove_country_ids.split("#");
          
          for(var i = 0; i < res_country_id.length ; i++)
          {
            country_id_rm = res_country_id[i];
            var n = $( ".cityList"+country_id_rm ).length;  
            var count_checked = 0;
            $(".cityList"+country_id_rm+":checkbox:checked").each(function(){
                count_checked = count_checked + 1;
            });

            if(count_checked == n)
            {
              $("#label-"+country_id_rm).removeClass('label_partial');
              
              $("#model-label-"+country_id_rm).removeClass('label_partial');      
            }
          }
          

          $('.countryList:checkbox:checked').each(function(){
              var id = $(this).val();
              var country_open = "morecountry"+ id;
              var country_model_open = "modelmorecountry"+ id;
              $("."+country_open).show();
              $("."+country_model_open).show();
          });

            var cityID = [];
            $('.cityList:checkbox:checked').each(function(){
                var id = $(this).val();
                cityID.push(id);
                totalTourCount = totalTourCount + parseInt($("#count"+id).text());

                var cid = $(this).attr('data-country-id');
                var country_open = "morecountry"+ cid;
                $("."+country_open).show();

                var mcountry_open = "modelmorecountry"+ cid;
                $("."+mcountry_open).show();
            });

          var searchDate = '<?php echo $search_date ?>';
          if(searchDate != '' && searchDate != '1970-01-01')
          {
            $("#dept_dates").html('<?php echo $search_date ?>');
          }
          else
          {
            $("#dept_dates").html('All Date Selected');
          }

          tour_data = {
            countryNames: countryNames.join('# '),
            tourTitle: tourTitle,
            provider: 'searchTour',
            tour_type: '<?php echo $tour_type; ?>',
            search_type: '<?php echo $search_type;?>',
            search_country_id : countryID.join('# '),
            search_city_id : cityID.join('# '),
            search_value: 'INDIAN EXPRESS',
            date: '<?php echo $dep_date; ?>',
            default_selected_city: '<?php echo session()->get( 'default_selected_city' );?>',
            offset: start_page,
            limit : 0
          };
          console.log(tour_data);
          // CACHING Tour List
          var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'searchTour', true);
          //console.log('tourResponse', tourApiCall);
          eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
          console.log('tourResponse', tourResponse);

            $('.countryName').text(countryNames.join(', ').substring(0,24));
            //totalTourCount += tourResponse.results.length;
            if(totalTourCount > 0){
                $('.tour-count').html( ': ' + totalTourCount + ( (totalTourCount==1) ? ' Tour' : ' Tours' )+ ' Found');
            }
            if( tourResponse.results.length > 0 ){

              $.each( tourResponse.results, function( key, value ) {

                appendTours( value ); 
                loading = false;  //set loading flag off
                $("#more-loader").hide();
                  start_page++; //page increment
              })
              getDistinctCategory();
              
              $('.tour-count').html(': ' + (start_page) + ( (start_page==1) ? ' Tour' : ' Tours' )+ ' Found');

              /*if(countryNames.length == 0 ) {  $('.tour-count').html(': ' + (start_page) + ( (start_page==1) ? ' Tour' : ' Tours' )+ ' Found'); }
              if(tourTitle.length > 0 ) {  $('.tour-count').html(': ' + (start_page) + ( (start_page==1) ? ' Tour' : ' Tours' )+ ' Found'); }*/

              //if(tourResponse.results.length < 100){
                  $("#listTourList").append(record_end_txt);
                  $("#gridTourList").append(record_end_txt);

                  $("#more-loader").hide();
                  end_record = true; //set end record flag on
                  return; //exit
              //}
            } else {
              if(tourTitle.length > 0 ) {  $('.tour-count').html(': ' + (start_page) + ( (start_page==1) ? ' Tour' : ' Tours' )+ ' Found'); }

              $("#listTourList").append('<h5><center>No Tours Found</center></h5>');
              $("#gridTourList").append('<h5><center>No Tours Found </center></h5>');


              $("#tours-loader").hide();
              $("#more-loader").hide();
              end_record = true; //set end record flag on
              return; //exit
            }

          })
        } else {
           $("#more-loader").hide();
          calculateHeight();
        }
      }

      
      function getStars(count, half = false){
        var stars = '';
        if( parseInt( count ) ){
          for( star = 1; star <= count; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
          }
          var emptyStars = 5 - parseInt( count );
          if(half){
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
          }
          for( empty = 1; empty <= emptyStars; empty ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }else{
          for( star = 1; star <= 5; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }
        var stars = '';
        return stars;
      }

      function checkImageUrl(id, url, url2){
        eroam.ajax('post', 'existsImage', {url : url}, function(response){
          if(response == 200){
            var image = url;// "{{ url( 'images/no-image.jpg' ) }}";
            //$("#tourImage_"+id).attr('src', image);
          } else if(response == 400){
            var image = url; // "{{ url( 'images/no-image.jpg' ) }}";
            //$("#tourImage_"+id).attr('src', image);
          } else {

            eroam.ajax('post', 'existsImage', {url : url2}, function(response){
              if(response == 200){
                $(".tourImage_"+id).attr('src', url2);
                //$(".backTourImage_"+id).css('background-image', 'url("' + url2 + '")');
              } else if(response == 400){
                $(".tourImage_"+id).attr('src', url2);
                //$(".backTourImage_"+id).css('background-image', 'url("' + url2 + '")');
              } else {
                  var image = "{{ url( 'images/no-image-2.jpg' ) }}";
                  $(".tourImage_"+id).attr('src', image);
                  //$(".backTourImage_"+id).css('background-image', 'url("' + image + '")');
              }
            }); 
          }
        });  
      }

      function appendTours(tour){

        $("#tours-loader").hide();
        var price = 0;
        var land_price = 0;
        if(!tour.flightPrice  || tour.flightPrice < 1){
          tour.flightPrice = 0;
        }
        if(tour.flightDepart != '' && tour.flightDepart != null){
            <?php 
              $default_selected_city = session()->get( 'default_selected_city' );
              $city_name = 'MEL';
              if($default_selected_city == 7){
                $city_name = 'SYD';
              }elseif($default_selected_city == 15){
                $city_name = 'BNE';
              }elseif($default_selected_city == 30){
                $city_name = 'MEL';
              }
              $city_name;
            ?>
            tour.flightDepart = '<?php echo $city_name;?>';
          }
        if(tour.price){
          price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
        }
        if(tour.price){
          price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
          land_price = parseFloat(tour.price).toFixed(2);
        }
        var star;
        var rating = tour.rating;

        if(rating % 1 === 0){
          stars = getStars( rating);
        }else {
          stars = getStars( rating, true );
        }
        if(tour.thumb){
        var image_url_type = tour.thumb.replace("thumbnail","small");
       }else{
        var image_url_type = 'images/no-image-2.jpg';
       }
       var imgurl = 'http://www.adventuretravel.com.au'+tour.folder_path+'245x169/'+tour.thumb;
       var imgurl2 = 'http://dev.cms.eroam.com/'+image_url_type;
        //var imgurl = 'https://cms.ijurni.com/'+image_url_type;
        //var imgurl2 = 'https://cms.ijurni.com/'+image_url_type;
        var tourlogo = 'http://dev.cms.eroam.com/'+tour.logo_path;

        
        //var tourlogo = "{{ url( 'images/tour-logo.png' ) }}";
         var total_duration = ''; 
          if(tour.durationType == 'd'){
            total_duration = 'Day';
            if(Math.ceil(tour.no_of_days) > 1){
              total_duration = 'Days';
            }
          }else if(tour.durationType == 'h'){
            total_duration = 'Hour';
            if(Math.ceil(tour.no_of_days) > 1){
              total_duration = 'Hours';
            }
          }
          if(!tour.discount || tour.discount == '.00'){
            tour.discount = 0;
          }
          if(!tour.saving_per_person || tour.saving_per_person == '.00'){
            tour.saving_per_person = '0.00';
          }
          if(!tour.retailcost){
            tour.retailcost = '0.00';
          } 
          if(tour.location){
            tour.location = removeLastComma(tour.location);
          } 

        var tripActivities = '';
        if(tour.tripActivities){
            tripActivities = tour.tripActivities.split(",");
            tripActivities = tripActivities[0];
        }
        // if(tour.accommodation){
        //     accommodation = tour.accommodation.split(',');
        //     var a_html = '<ul>';
        //     for(i=0;i<accommodation.length;i++){
        //         a_html +='<li>'+accommodation[i]+'</li>';
        //     }
        //     a_html +='</ul>';
        //     tour.accommodation = a_html;
        // }
        // if(tour.transport){
        //      transport = tour.transport.split(',');
        //     var t_html = '<ul>';
        //     for(i=0;i<transport.length;i++){
        //         t_html +='<li>'+transport[i]+'</li>';
        //     }
        //     t_html +='</ul>';
        //     tour.transport = t_html;
        // }
        var tour_name = tour.tour_title.toLowerCase();
        var gridHtml = '<div class="col-md-6 col-sm-6 col-lg-4 grid-box gridBox" data-price="'+price+'" data-category="'+tripActivities+'" data-tourName="'+tour_name+'" data-provider="eRoam" data-all-category="'+tour.tripActivities+'">'+
                      '<div class="grid-box" style="cursor: pointer;"  onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'">'+
                        '<div class="grid-img">'+
                          '<img src="'+imgurl+'" alt="" class="img-responsive tourImage_'+tour.tour_id+'" >';
                          if(tour.flightPrice != 0){
                            gridHtml +='<div class="flight-included"><span class="flight-icon"><i class="fa fa-plane"></i></span><span class="flight-text">Flight Included</span></div>';
                          }
                        gridHtml +='</div>'+
                        '<div class="grid-details">'+
                          '<h4>'+tour.tour_title+'</h4>'+
                          '<div class="m-t-20 border-box">'+
                            '<div class="row">'+
                              '<div class="col-xs-4">'+
                                '<div class="row border-right-gray">'+
                                  '<div class="col-sm-5 col-xs-6 way-text">TOTAL<br>'+total_duration.toUpperCase()+''+
                                  '</div>'+
                                  '<div class="col-sm-6 col-xs-6 price-text">'+tour.no_of_days+''+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-xs-8">'+
                                '<div class="row">'+
                                  '<div class="col-sm-5 col-xs-6 way-text">eRoam<br>Cost P.P'+
                                  '</div>'+
                                  '<div class="col-sm-6 col-xs-6 price-text">$'+price+'<sup>*</sup>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>';

                            if(tour.flightPrice > 0 && (tour.flightDepart != '' && tour.flightDepart != null )){
                              gridHtml +='<p class="flight-note">FLIGHTS INCLUSIVE DEPARTING '+tour.flightDepart+'</p>';
                            }

                          var countryList = tour.CountryList;
                          var country = countryList.split(",");
                          gridHtml +='</div>'+
                          '<div class="tour-box-field"><div class="row m-t-20">'+
                            '<div class="col-sm-6 col-xs-5">'+
                              '<p class="orange-text">Tour Code</p>'+
                            '</div>'+
                            '<div class="col-sm-6 col-xs-7">'+
                              '<p>'+tour.tour_code+'</p>'+
                            '</div>'+
                          '</div>'+
                          '<div class="row">'+
                            '<div class="col-sm-6 col-xs-5">'+
                              '<p class="orange-text">Countries</p>'+
                            '</div>'+
                            '<div class="col-sm-6 col-xs-7">'+
                              '<p>'+country[0]+'</p>'+
                            '</div>'+
                          '</div>'+
                          // '<div class="row">'+
                          //   '<div class="col-sm-6 col-xs-5">'+
                          //     '<p class="orange-text">Tour Theme</p>'+
                          //   '</div>'+
                          //   '<div class="col-sm-6 col-xs-7">'+
                          //     '<p>'+tour.ActivityList+'</p>'+
                          //   '</div>'+
                          // '</div>'+
                          '<div class="row">'+
                            '<div class="col-sm-6 col-xs-5">'+
                             '<p class="orange-text">Group Size</p>'+
                            '</div>'+
                            '<div class="col-sm-6 col-xs-7">'+
                              '<p>Min '+tour.groupsize_min+' - Max '+tour.groupsize_max+'</p>'+
                            '</div>'+
                          '</div>'+
                          '<div class="row">'+
                            '<div class="col-sm-6 col-xs-5">'+
                              '<p class="orange-text">Children</p>'+
                            '</div>'+
                            '<div class="col-sm-6 col-xs-7">'+
                              '<p>'+(tour.is_childAllowed == 1 ? "Yes":"No")+'</p>'+
                            '</div>'+
                          '</div>'+
                           (tour.children_age != '' && tour.children_age != null && tour.is_childAllowed == 1? "<div class='row'><div class='col-sm-6 col-xs-5'><p class='orange-text'>Children Age</p></div><div class='col-sm-6 col-xs-7'><p>"+tour.children_age+"</p></div></div>":"")+
                          '</div><div class="row tour-box-price">'+
                              '<div class="col-sm-6 red-text">'+
                                '<div class="row border-right-gray">';
                                if(tour.discount && tour.discount != '.00'){
                                    gridHtml +='<div class="col-sm-6 col-xs-6 way-text">eRoam<br>Discount'+
                                    '</div>'+
                                    '<div class="col-sm-6 col-xs-6 price-text padding-left-3">'+tour.discount+'%'+
                                    '</div>';
                                }    
                              gridHtml +='</div>'+
                              '</div>'+
                             '<div class="col-sm-6 yellow-text">'+
                                '<div class="row">'; 
                            if(tour.discount && tour.discount != '.00'){  
                                    gridHtml +='<div class="col-sm-5 col-xs-6 way-text">Savings From P.P'+
                                    '</div>'+
                                    '<div class="col-sm-6 col-xs-6 price-text padding-left-3">$'+tour.saving_per_person+'<sup>*</sup>'+
                                    '</div>';
                            }
                            gridHtml +='</div>'+
                              '</div>'+
                            '</div>'+
                            '<p class="note-text"><em>*From price, discount and savings per person is based on the total price per adult in a twin share room, subject to departure dates</em></p>'+
                          '<button type="submit" name="" class="btn btn-primary btn-block m-t-10" onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'">EXPLORE FURTHER</button>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
        $('#gridTourList').append(gridHtml);


        var itinerary = tour.itinerary;
        //itinerary = text.replace('old', 'new');
        itinerary = itinerary.split('<p>');
        var i =1;
        var itineraryleg ='';
        itinerary.forEach(function(item) {
          if(i <= 3){
            item = item.replace('</p>', '');
            if($.trim(item).length > 0){
              if(item.length > 180){
                itineraryleg  = itineraryleg + '<li>'+item.substr(0, 80)+'...</li>';
              } else {
                itineraryleg  = itineraryleg + '<li>'+item+'</li>';
              }
              i++;
            }
          }
        });

       
        // console.log(tour);
        // console.log("inn");
        var countryList = tour.CountryList;
        var country     = countryList.split(",");
        var tour_name   = tour.tour_title.toLowerCase();
        var listHtml    = '<div class="list-box" data-price="'+price+'" data-category="'+tripActivities+'" data-all-category="'+tour.tripActivities+'" data-tourName="'+tour_name+'" data-provider="eRoam" style="cursor: pointer;" onclick = "window.location.href = \'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'">'+
                  '<div class="row">'+
                    '<div class="col-sm-7">'+
                      '<h4>'+tour.tour_title+'</h4>'+
                      '<p>'+
                        '<svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">'+
                            '<desc>Created with Sketch.</desc>'+
                            '<defs></defs>'+
                            '<g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">'+
                                '<g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">'+
                                    '<g id="Group" transform="translate(501.000000, 542.000000)">'+
                                        '<g id="Group-16">'+
                                            '<path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>'+
                                        '</g>'+
                                    '</g>'+
                                '</g>'+
                            '</g>'+
                        '</svg>'+
                        'Tour Code: '+tour.tour_code+'   |   Countries: '+country[0]+
                      '</p>'+
                    '</div>'+
                    '<div class="col-sm-5">'+
                      '<div class="row">'+
                        '<div class="col-xs-9 text-right border-right  tour-top-details">';
                        var flight_text = '(Land Only)';
                        if(tour.flightPrice != 0){
                          flight_text = '(Inclusive Flights)';
                        }
                        listHtml +='<p>From <strong>$'+price+'</strong> P.P '+flight_text+'</p>';
                          if(tour.discount && tour.discount != '.00'){
                           listHtml +='<p class="red-text">eRoam Discount <strong>'+tour.discount+'%</strong></p>';
                          }
                          if(tour.saving_per_person && tour.saving_per_person != '.00'){
                            listHtml +='<p class="yellow-text">Savings From <strong>$'+tour.saving_per_person+'</strong> P.P</p>';
                          }
                          
                        listHtml +='</div>'+
                        '<div class="col-xs-3 text-center">'+
                          '<h3 class="transport-price"><strong>'+tour.no_of_days+'</strong></h3>'+total_duration+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                  '<hr/>'+
                  '<div class="row">'+
                      '<div class="col-sm-4 listbox-img">'+
                        '<div class="listbox-image"><img src="'+imgurl+'" alt="" class="img-responsive tourImage_'+tour.tour_id+'" />';
                        if(tour.flightPrice != 0){
                          listHtml +='<div class="flight-included"><span class="flight-icon"><i class="fa fa-plane"></i></span><span class="flight-text">Flight Included</span></div>';
                        }
                      listHtml +='</div></div>'+
                      '<div class="col-sm-8 listbox-details">'+
                        '<div class="listbox-detailsInner">'+
                          '<div>'+
                            '<div class="logo-img">'+
                              '<img src="'+tourlogo+'" alt="" class="img-responsive" />'+
                            '</div>'+
                            '<div class="logo-details">'+
                              '<div class="row">'+
                                '<div class="col-sm-6">'+
                                  '<div class="row">'+
                                    '<div class="col-sm-6"><strong>Departure City</strong></div>'+
                                    '<div class="col-sm-6 city_wrap">'+tour.departure+'</div>'+
                                  '</div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-6"><strong>Destination City</strong></div>'+
                                    '<div class="col-sm-6 city_wrap">'+tour.destination+'</div>'+
                                  '</div>'+
                                  // '<div class="row">'+
                                  //   '<div class="col-sm-6"><strong>Tour Theme</strong></div>'+
                                  //   '<div class="col-sm-6">'+tour.ActivityList+'</div>'+
                                  // '</div>'+
                                '</div>'+
                                '<div class="col-sm-6">'+
                                  '<div class="row">'+
                                    '<div class="col-sm-6"><strong>Group Size</strong></div>'+
                                    '<div class="col-sm-6">Min '+tour.groupsize_min+' - Max '+tour.groupsize_max+'</div>'+
                                  '</div>'+
                                   '<div class="row">'+
                                    '<div class="col-sm-6"><strong>Children</strong></div>'+
                                    '<div class="col-sm-6">'+(tour.is_childAllowed == 1 ? "Yes":"No")+'</div>'+
                                  '</div>'+
                                  (tour.children_age != '' && tour.children_age != null && tour.is_childAllowed == 1? "<div class='row'><div class='col-sm-6'><strong>Children Age</strong></div><div class='col-sm-6'>"+tour.children_age+"</div></div>":"")+
                                '</div>'+

                            
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div>'+
                            
                            '<div class="logo-details"><BR>'+
                              '<div class="row">'+
                                '<div class="col-sm-12">'+
                                  '<div class="row">'+
                                    '<div class="col-sm-3"><strong>Accommodation</strong></div>'+
                                    '<div class="col-sm-9">'+tour.accommodation+'</div>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="row">'+
                                '<div class="col-sm-12">'+
                                  '<div class="row">'+
                                    '<div class="col-sm-3"><strong>Transport</strong></div>'+
                                    '<div class="col-sm-9">'+tour.transport+'</div>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="m-t-20">'+
                            '<p>'+tour.long_description.substr(0, 390)+' '+(tour.long_description.length > 390 ? "..." : "")+'</p>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                '</div>';
        $('#listTourList').append(listHtml);


        imageUrl = checkImageUrl(tour.tour_id, imgurl, imgurl2);
      }

      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        //setEqualHeight_CommonClassMin(".grid-img");
        setEqualHeight_CommonClass(".grid-details h2");
        setEqualHeight_CommonClass(".grid-details");
      });
  </script>
@stop