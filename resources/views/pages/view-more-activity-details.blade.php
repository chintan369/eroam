<style type="text/css">
	.body-container{
		margin-top:40px;
		padding:8px 0px 8px 0px;
	}
	.border{
		border:1px solid red;
	}
	.small-image img{
	    width: 100%;
	    height: 170px;
	    background-size: contain !important;
	    background-position: center !important;
	    background-repeat: no-repeat !important;
	    background-color: #2B2B2B !important;
	}

	.main-body{
		margin-top: 30px;
		margin-left:5px;
	}
	.tab-button{
		color:black;
		width: 33%;
	    border-radius:0px;
	    border:1px solid;
	    background:transparent;
	    text-transform: uppercase;
	    margin-left:-4px;
	    padding:12px;
	}
	.tab-button.focus{
		background-color: #2AA9DF;
		color:white;
		border-radius:2px;
		border:1px solid #2AA9DF;
		outline:0;
		
	}
	.details{
		padding:15px 2px 5px 2px;
	}
	.default{
		background-color: #2AA9DF;
		color:white;
		border:2px solid #2AA9DF;
		outline:0;
	}
	.thumbnail-image{
	    width: 90%;
    	height: 100px;
	    background-size: contain !important;
	    background-position: center !important;
	    background-repeat: no-repeat !important;
	    background-color: transparent !important;

	}
	.thumbnail-image:hover{
		border:3px solid #2AA9DF;
	}
	.responsive{
		margin-top:30px;
		margin-bottom: 20px;
	
	}
	.images{
		margin-right:18px;
		
	}
	.thumb{
		height: 400px;
	}
	.selected{
		border: 3px solid #2AA9DF;
	}
	/* Style the Image Used to Trigger the Modal */
	#myImg {
	    border-radius: 5px;
	    cursor: pointer;
	    transition: 0.3s;
	    
	}
	#myImg:hover {opacity: 0.7;}
	
	.modal-content {
	    margin: auto;
	    display: block;
	    max-width: 600px;
	   border:0px;
	}
	/* Caption of Modal Image (Image Text) - Same Width as the Image */
	#caption {
	    margin: auto;
	    display: block;
	    width: 80%;
	    max-width: 700px;
	    text-align: center;
	    color: #ccc;
	    padding: 10px 0;
	    height: 150px;
	}
	/* Add Animation - Zoom in the Modal */
	.modal-content, #caption { 
	    -webkit-animation-name: zoom;
	    -webkit-animation-duration: 0.6s;
	    animation-name: zoom;
	    animation-duration: 0.6s;
	}
	@-webkit-keyframes zoom {
	    from {-webkit-transform:scale(0)} 
	    to {-webkit-transform:scale(1)}
	}
	@keyframes zoom {
	    from {transform:scale(0)} 
	    to {transform:scale(1)}
	}
	
	@media only screen and (max-width: 700px){
	    .modal-content {
	       
	        width: 100%;
  			max-width: 600px;
	    }
	}
	.map-show {
	    height: 350px;
	    width: 100%;
	    /*margin-left: -22px;*/
	}
	#pac-input{
		margin-bottom: 1px;
	}
	.carousel-fade .carousel-inner .item {
	  opacity: 0;
	  -webkit-transition-property: opacity;
	  -moz-transition-property: opacity;
	  -o-transition-property: opacity;
	  transition-property: opacity;
	}
	.carousel-fade .carousel-inner .active {
	  opacity: 1;
	}
	.carousel-fade .carousel-inner .active.left,
	.carousel-fade .carousel-inner .active.right {
	  left: 0;
	  opacity: 0;
	  z-index: 1;
	}
	.carousel-fade .carousel-inner .next.left,
	.carousel-fade .carousel-inner .prev.right {
	  opacity: 1;
	}
	.carousel-fade .carousel-control {
	  z-index: 2;
	}
	.close-btn:before {
		position: absolute;
		right: -40px;
		top: -35px;
		font-size: 3rem;
		color: #ececec;
		z-index: 999;
		cursor: pointer;
		transition: .21s ease-ine;
	}
	.close-btn:hover {
		color: #ffffff;
	}
	.flaticon-left-arrow{
		margin-left: -40px !important;
	}

	.hotel-container{
		display: block;
	}

	/*.activityDesc{
		height: 500px%;
    	overflow: auto;
	}*/

	.jcarousel {
	    position: relative;
	    overflow: hidden;
	    width: 100%;
	}
	.hotel-container .panel-icon svg {
	    width: 23px !important;
	}

	.m-l-30 { margin-left: 30px; margin-bottom: 20px; }
	.m-b-0 { margin-bottom: 0px; }

	.datetimepick { position:relative; }

	.act-btn { width: 90%; }

	
</style>

<div class="hotel-container">
  <div class="panel-inner">
    <a href="#" class="hotel-close act-btn-less " data-provider="{{ $provider }}" data-code="{{ $code }}">
	    <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	        <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
	        <desc>Created with Sketch.</desc>
	        <defs></defs>
	        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	            <g id="54---EROA007-V4.1-Generic-(Edit-Hotel)-02" transform="translate(-1240.000000, -185.000000)">
	                <g id="Group-31" transform="translate(491.000000, 168.000000)">
	                    <g id="close" transform="translate(756.000000, 24.000000) scale(1, -1) translate(-756.000000, -24.000000) translate(744.000000, 12.000000)">
	                        <polygon id="Shape" fill="#000000" opacity="0.539999962" points="19 17.6 17.6 19 12 13.4 6.4 19 5 17.6 10.6 12 5 6.4 6.4 5 12 10.6 17.6 5 19 6.4 13.4 12"></polygon>
	                        <polygon id="bounds" points="24 0 0 0 0 24 24 24"></polygon>
	                    </g>
	                </g>
	            </g>
	        </g>
	    </svg>
    </a>
    <div class="hotel-top-content">

	    <div class="row m-t-10">
			<div class="col-sm-7">
				<h4>{{ $data['activity_name'] }}</h4>
			</div>
			<div class="col-sm-4 text-right pull-right">
				<p>From <strong>{{ session()->get( 'search' )['currency'].' '.$data['price'] }}</strong> Per Person</p>
			</div>
		</div>

	   	<div class="panel-icon">
	      <svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	              <desc>Created with Sketch.</desc>
	              <defs></defs>
	              <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                  <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">
	                      <g id="Group" transform="translate(501.000000, 542.000000)">
	                          <g id="Group-16">
	                              <path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>
	                          </g>
	                      </g>
	                  </g>
	              </g>
	      </svg>
	   	</div>
	    <div class="panel-container">
	   		<div class="row">
	   			<div class="col-sm-7">
	   				<p>Activity Code: {{ $data['code'] }} |&nbsp;Location: @if($data['address']){{ $data['address'] }}, @endif {{ $data['city_name'] }} </p>
	   			</div>
	   			<div class="col-sm-4 text-right pull-right">
	   				<p>Duration : {{ $data['duration'] }} </p>
	   			</div>
	   		</div>
	    </div>

	    <div class="jcarousel-wrapper">
	        <div class="jcarousel">
	        	<ul>
		        	<?php 
				        $i=0;
				        foreach($data['images'] as $key => $image){
				        	$i++
		        	?>
	                	<li><img src="{{ $image }}" alt="Image {{$i}}"></li>
	                <?php } ?>
	            </ul>
	        </div>

	        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	        <a href="#" class="jcarousel-control-next">&rsaquo;</a>

	        <p class="jcarousel-pagination"></p>
	    </div>
	</div>

	<div class="custom-tabs" data-example-id="togglable-tabs"> 
      <ul class="nav nav-tabs" id="myTabs1" role="tablist"> 
        <li role="presentation" class="active"><a href="#overview" id="overview-tab" role="tab" data-toggle="tab" aria-controls="overview" aria-expanded="true">OVERVIEW</a>
        </li>
        <li role="presentation"><a href="#info" id="info-tab" role="tab" data-toggle="tab" aria-controls="info" aria-expanded="true">Important Info</a>
        </li>
      </ul> 
    </div>
  </div>


  	<div class="hotel-bottom-content">
		<div class="row activityDesc">
			<div class="col-md-9 col-sm-8">
				
			      	<div class="tab-content" id="myTabContent1"> 
			          	<div class="tab-pane fade in active" role="tabpanel" id="overview" aria-labelledby="overview-tab">
			                <div class="panel-icon">
			                  <svg width="19px" height="18px" viewBox="0 0 19 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			                      <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
			                      <desc>Created with Sketch.</desc>
			                      <defs></defs>
			                      <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			                          <g id="57---EROA007-V4.1-Generic-(Edit-Activities)-03" transform="translate(-512.000000, -552.000000)" fill="#212121">
			                              <g id="Group-32" transform="translate(491.000000, 172.000000)">
			                                  <path d="M24.8462119,395.528963 L36.4171713,395.528963 L36.4171713,390.57225 L24.8462119,390.57225 L24.8462119,395.528963 Z M36.7608074,389.872079 L24.4991958,389.872079 C24.311041,389.872079 24.1566864,390.031826 24.1566864,390.222164 L24.1566864,395.877915 C24.1566864,396.072785 24.311041,396.228001 24.4991958,396.228001 L36.7608074,396.228001 C36.9545956,396.228001 37.1089502,396.072785 37.1089502,395.877915 L37.1089502,390.222164 C37.1089502,390.031826 36.9545956,389.872079 36.7608074,389.872079 L36.7608074,389.872079 Z M39.3048411,394.957949 C38.4068806,395.09957 37.6982016,395.842793 37.6148276,396.762759 L23.8130503,396.762759 C23.7206629,395.789544 22.9376233,395.014597 21.9607955,394.939822 L21.9607955,391.053759 C22.9026964,390.983515 23.6632025,390.261818 23.7995302,389.33732 L37.6283477,389.33732 C37.7511553,390.199505 38.4249074,390.886081 39.3003344,391.023169 L39.3003344,394.957949 L39.3048411,394.957949 Z M22.730315,387.884862 C22.8756562,387.48266 22.893683,387.048735 22.7652419,386.628406 L35.3310559,380.803843 C35.8098931,381.539136 36.7258804,381.889222 37.5573671,381.641103 L39.1899202,385.212202 C38.8383974,385.442194 38.5702485,385.783215 38.4204007,386.189949 C38.252526,386.637469 38.252526,387.120112 38.4113873,387.561967 L36.25493,388.602027 C36.2369032,388.611091 36.2323965,388.63375 36.2154964,388.641681 L34.7891249,388.641681 L37.4120259,387.424879 C37.5844073,387.345571 37.6632746,387.141638 37.5799006,386.964896 L35.2296551,381.826909 C35.1496611,381.653565 34.9434795,381.569726 34.7710981,381.658097 L23.6327822,386.814212 C23.5539149,386.853865 23.4829344,386.925242 23.4525141,387.009081 C23.4254739,387.097452 23.4254739,387.194887 23.4660342,387.278726 L24.0902126,388.641681 L23.4615275,388.641681 C23.2677393,388.641681 23.1178914,388.801429 23.1178914,388.991766 C23.1178914,389.017825 23.1178914,389.054079 23.1269048,389.084669 C23.1133847,389.59337 22.8058023,390.022763 22.3607654,390.226696 L21.8109476,388.961176 C22.2379577,388.726653 22.5590603,388.350509 22.730315,387.884862 L22.730315,387.884862 Z M39.6079169,390.363785 C38.8958578,390.363785 38.3189999,389.792771 38.3009731,389.089201 C38.3054798,389.054079 38.3099865,389.017825 38.3099865,388.991766 C38.3099865,388.801429 38.1601386,388.641681 37.9618437,388.641681 L37.7770689,388.641681 L39.0400723,388.035546 C39.124573,387.995892 39.1944269,387.921116 39.2169604,387.828214 C39.2473806,387.739842 39.2428739,387.642408 39.1944269,387.557435 C39.1640066,387.500787 39.1335864,387.460001 39.1110529,387.443006 C38.961205,387.120112 38.9476849,386.760962 39.0716192,386.429004 C39.1944269,386.101578 39.4366622,385.840997 39.7273445,385.703908 C39.8242386,385.676717 39.9076126,385.611005 39.9515529,385.518102 C40,385.420667 40,385.314169 39.9560596,385.216734 L38.0508511,381.051962 C37.9798705,380.901278 37.8041091,380.821971 37.6497545,380.861624 C37.6193343,380.870688 37.5753939,380.893347 37.5359603,380.914874 C36.883615,381.198114 36.1186022,380.910342 35.84144,380.334797 C35.8369333,380.295143 35.8189065,380.229431 35.8008797,380.193176 C35.7175057,380.025498 35.5192108,379.950722 35.3479561,380.034561 L22.1804973,386.137833 C22.0126226,386.212608 21.9731889,386.468658 22.0396628,386.642001 C22.1895107,386.960364 22.2019041,387.31838 22.0790964,387.646939 C21.9607955,387.978898 21.7185602,388.244011 21.3794308,388.390163 C21.3354905,388.399227 21.2915501,388.407158 21.2476098,388.420753 C21.1552224,388.447944 21.0752284,388.518188 21.0312881,388.606559 C20.9918544,388.69493 20.9873477,388.801429 21.0312881,388.894332 L21.6520864,390.328663 C21.5642057,390.328663 21.4673115,390.341125 21.3884442,390.40797 C21.3174637,390.470283 21.2735233,390.567718 21.2735233,390.669685 L21.2735233,395.325029 C21.2735233,395.426996 21.3174637,395.519899 21.3884442,395.585611 C21.4673115,395.652456 21.5642057,395.678514 21.6701132,395.670583 C21.6825066,395.666051 21.8019342,395.634328 21.8154543,395.629796 C22.5410335,395.629796 23.1314115,396.223469 23.1314115,396.945166 C23.1223981,396.966693 23.1043713,397.050532 23.0998646,397.068659 C23.0874712,397.170626 23.1178914,397.268061 23.1843653,397.342836 C23.2497125,397.418745 23.3466066,397.46293 23.4435007,397.46293 L38.0069107,397.46293 C38.1995722,397.46293 38.3539268,397.303183 38.3539268,397.112845 C38.3539268,397.046 38.3359,396.980288 38.3009731,396.931571 C38.3144932,396.209873 38.8958578,395.629796 39.5470764,395.626397 C39.7667781,395.673982 40,395.510835 40,395.289907 L40,390.704806 C39.9909866,390.514469 39.8017051,390.363785 39.6079169,390.363785 L39.6079169,390.363785 Z" id="Fill-1"></path>
			                              </g>
			                          </g>
			                      </g>
			                  </svg>
			                </div>
			                <div class="panel-container">
			                  <p><strong>Description</strong></p>
			                  <div class="m-t-10">
			                    <p>{!! $data['activity_description'] !!}</p>

			                    <?php if($data['highlights']){ ?> 
			                  		<p class="m-b-0"><strong>Highlights</strong></p>
			                  		<ul class="m-l-30">
			                  			<?php foreach ($data['highlights'] as $key => $value) { ?>
			                  				<li><?php echo $value?></li>
			                  			<?php } ?>
			                  		</ul>
			                  	<?php } ?>

			                  	<?php if($data['description']){ ?> 
			                  		<p class="m-b-0"><strong>What You Can Expect</strong></p>
			                  		<p><?php echo $data['description']; ?></p>
			                  	<?php } ?>

			                  	

			                  	<?php if($data['itinerary']){ ?> 
			                  		<p class="m-b-0"><strong>Itinerary</strong></p>
			                  		<p><?php echo $data['itinerary']; ?></p>
			                  	<?php } ?>
			                  </div>
			                </div>
			          	</div>

			          	<div class="tab-pane fade" role="tabpanel" id="info" aria-labelledby="info-tab">
			                <div class="panel-container">
			                  <p><strong>Details</strong></p>

			                  	<?php if($data['inclusions']){ ?> 
			                  		<p class="m-b-0">Inclusions</p>
			                  		<ul class="m-l-30">
			                  			<?php foreach ($data['inclusions'] as $key => $value) { ?>
			                  				<li><?php echo $value?></li>
			                  			<?php } ?>
			                  		</ul>
			                  	<?php } ?>

			                  	<?php if($data['exclusions']){ ?> 
			                  		<p class="m-b-0">Exclusions</p>
			                  		<ul class="m-l-30">
			                  			<?php foreach ($data['exclusions'] as $key => $value) { ?>
			                  				<li><?php echo $value?></li>
			                  			<?php } ?>
			                  		</ul>
			                  	<?php } ?>

			                  	<?php if($data['additionalInfo']){ ?> 
			                  		<p class="m-b-0">Additional info</p>
			                  		<ul class="m-l-30">
			                  			<?php foreach ($data['additionalInfo'] as $key => $value) { ?>
			                  				<li><?php echo $value?></li>
			                  			<?php } ?>
			                  		</ul>
			                  	<?php } ?>

			                  	<?php if($data['voucherRequirements']){ ?> 
			                  		<p class="m-b-0">Voucher info</p>
			                  		<p><?php echo $data['voucherRequirements']; ?></p>
			                  	<?php } ?>

			                  	<?php if($provider == 'viator') { ?> 
				                  	<p class="m-b-0 m-t-10">Local operator information</p>
				                  	<p>Complete Operator information, including local telephone numbers at your destination, are included on your Confirmation Voucher. Our Product Managers select only the most experienced and reliable operators in each destination, removing the guesswork for you, and ensuring your peace of mind.</p>

				                  	<p class="m-b-0 m-t-10"><strong>Cancellation Policy</strong></p>
				                  	<p>If you cancel at least 7 day(s) in advance of the scheduled departure, there is no cancellation fee.<br>
				                  	If you cancel between 3 and 6 day(s) in advance of the scheduled departure, there is a 50 percent cancellation fee.<br>
				                  	If you cancel within 2 day(s) of the scheduled departure, there is a 100 percent cancellation fee.</p>

				                  	<p class="m-b-0 m-t-10"><strong>Schedule and Pricing</strong></p>
				                  	<p>Click the link below to check pricing & availability on your preferred travel date. Our pricing is constantly updated to ensure you always receive the lowest price possible - we 100% guarantee it. Your currency is set to USD. Click here to change your currency.</p>
			                  	<?php } ?>
								<?php if($data['departurePoint']){ ?> 
			                  		<p class="m-b-0"><strong>Departure Point</strong></p>
			                  		<p><?php echo $data['departurePoint']; ?></p>
			                  	<?php } ?>

			                  	<?php if($data['departureTime']){ ?> 
			                  		<p class="m-b-0"><strong>Departure Time</strong></p>
			                  		<p><?php echo $data['departureTime'].'<br>'.$data['departureTimeComments']; ?></p>
			                  	<?php } ?>

			                  	<?php if($data['duration']){ ?> 
			                  		<p class="m-b-0"><strong>Duration</strong></p>
			                  		<p><?php echo $data['duration']; ?></p>
			                  	<?php } ?>

			                  	<?php if($data['returnDetails']){ ?> 
			                  		<p class="m-b-0"><strong>Return details</strong></p>
			                  		<p><?php echo $data['returnDetails']; ?></p>
			                  	<?php } ?>

			                </div>
			          	</div>

			          	<?php /*<div class="map-show" id="googleMap" style="display:none;"></div> */ ?>
			      	
			    </div>
			</div>
			<?php 
		     	$btnName = ($data['select'] == 'true')?'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
		    	$btnClass = ($data['select'] == 'true')?'selected-activity' : '';
		     	$selectDiv = '<div class="col-md-3 col-sm-4">
			              <input type="text"  '.$data['attributes'].' class="datetimepick" id="view_datepick-'.$data['code'].'" value="">
			              <button class="btn btn-primary btn-block act-btn act-select-btn '.$btnClass.'" data-view= "view" data-provider="'.$provider.'" data-city-id="'.$data['cityid'].'" data-index="'.$code.'" data-activity-id="'.$code.'" data-is-selected="'.$data['select'].'" data-price="'.$data['price'].'"  data-price-id=""  data-name="'.$data['activity_name'].'" data-currency="'.session()->get( 'search' )['currency'].'" data-startdate="'.$data['startDate'].'" data-enddate="'.$data['endDate'].'" data-description="" data-images="" data-duration="'.$data['duration'].'" data-label="" data-button-id="'.$data['code'].'">'.$btnName.'</button>
						</div>';
			              /*<h4 class="m-t-40">'.session()->get( 'search' )['currency'].' '.$data['price'].' Total</h4>
			                <p>Per Person</p>*/
				echo $selectDiv;
			?>
		</div>
  	</div>
</div>
 
<!-- The Modal -->
<div id="myModal" class="modal">
	<div class="modal-content">
		<span class="close-btn flaticon-multiply"></span>
		<div id="myCarousel" class="carousel carousel-fade" data-ride="carousel" data-interval="false">
			<!-- Wrapper for slides -->
			<div class="carousel-inner responsive" role="listbox">
				@foreach ($data['images'] as $key => $image)
					<div class="item {{($key == 0) ? 'active':'' }}" data-path-top="{{ $image}}">
						<div class="thumb" style="background-image: url( {{ $image}} )"></div>
					</div>
				@endforeach
			</div>
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left flaticon-left-arrow" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel " role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right flaticon-right-arrow" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div id="caption"></div>
</div>

<script>
	$(document).ready(function(){
		$('body').on('click', '#location-tab', function(e){
			$("#googleMap").show();
			init_map();
		});

		$('body').on('click', '#qa-tab, #details-tab, #review-tab, #overview-tab', function(e){
			$("#googleMap").hide();
		});

		$('body').on('click', '.tab-button', function(e){
			e.preventDefault();
			$(".button-image").removeClass("default");
			var myClass = $(this).attr('class');
			myClass = myClass.split(' ');
			//get the last element of an array to check what would be displayed
			// jayson suizo added
			// this should output the ff::
			// button-detail
			// button-itinerary
			// button-image
			// button-location
			// jayson suizo added end
			var last = myClass[myClass.length-1];
			if(last == "button-detail"){
				$('.details').show();
				$('.rooms').hide();
				$('.images').hide();
				$('.location').hide();
			}
			
			if(last == "button-image"){
				$('.details').hide();
				$('.rooms').hide();
				$('.images').show();
				$('.location').hide();
			}
			if(last == "button-location"){
				$('.details').hide();
				$('.rooms').hide();
				$('.images').hide();
				$('.location').show();
				init_map();
			}

			$('.tab-button').removeClass('focus');
			$(this).addClass('focus');

		});

		$('body').on('click', '.thumbnail-image', function(e){
			 $('.thumbnail-image').removeClass("selected");			
			 $(this).addClass('selected');
			 $('.item').removeClass('active');
			 var path = $(this).data('path');
			 var caption = $(this).data('context');
			 $("div").find("[data-path-top='" + path + "']").addClass('active');
		     var modal = document.getElementById('myModal');

			// Get the image and insert it inside the modal - use its "alt" text as a caption
			var img = document.getElementById('myImg');
			var modalImg = document.getElementById("img01");
			var captionText = document.getElementById("caption");
		    modal.style.display = "block";
		    modalImg.src = path;
		    captionText.innerHTML = caption;
			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() { 
			    modal.style.display = "none";
			}
		   
		     // Get the modal
			var modalImg = document.getElementById("img01");
			var captionText = document.getElementById("caption");
			
		   //  modal.style.display = "block";
		    modalImg.src = path;
		    captionText.innerHTML = caption+ " images";
	
		});

		$('body').on('click', '.left', function(e){
			 var path = $('div.active').prevAll().data('path-top');
			 $('.thumbnail-image').removeClass('selected');
			 if (path === undefined) {
			    $('.thumbnail-image').last().addClass('selected');	
		     }else{
		     	$("div").find("[data-path='" + path + "']").addClass('selected');
		     } 
		});

		$('body').on('click', '.right', function(e){
			 var path = $('div.active').nextAll().data('path-top');
			 $('.thumbnail-image').removeClass('selected');
			 if (path === undefined) {
			    $('.thumbnail-image').first().addClass('selected');	
		     }else{
		     	$("div").find("[data-path='" + path + "']").addClass('selected');
		     } 
		});
		$('body').on('click','.close-btn',function(e){
			$('.modal').hide();
		});
				
	});

	function init_map() {

		var mapOptions = {
			zoom: 13,
			center: {lat: 33.8688, lng: 151.2093},
		};
		map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);	

		var geocoder = new google.maps.Geocoder();
		var address = document.getElementById("pac-input").value;

		geocoder.geocode({
			address : address, 
			region: 'no' 
		}, function(results, status) {
			if (status.toLowerCase() == 'ok') {

				 // Show the map to the current location selected
				if (results[0].geometry.viewport) {
					map.fitBounds(results[0].geometry.viewport);
				} else {
					map.setCenter(results[0].geometry.location);
				}

				var marker = new google.maps.Marker({
					position : results[0].geometry.location,
					map : map,
					draggable : true,
					zoom: 13,
				});
				var infowindow = new google.maps.InfoWindow({
					content: '<p>Activity Name: ' + "{{ $data['activity_name'] }}" + '</p>'
				});

				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map, marker);
				});
			}
		});
	}

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}
</script>