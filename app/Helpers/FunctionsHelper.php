<?php
namespace App\Helpers;

use App\Http\Controllers\Controller;
use Exception;

use Leafo\ScssPhp\Compiler;
use Illuminate\Support\Facades\Storage;

class FunctionsHelper{
    public static function compileTheme($var_str){
		$scss = new Compiler();
        $scss->addImportPath(realpath(app()->path() . '/../resources/assets/sass'));
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');
		$output = $scss->compile($var_str.'@import "app";');

        $file=fopen('css/app.css','w');
		fwrite($file,$output);
		fclose($file);		
    }
}