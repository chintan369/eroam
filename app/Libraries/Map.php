<?php

namespace App\Libraries;

use App\Libraries\EroamSession;

use Carbon\Carbon;
use Cache;
use Guzzle;

use App\Http\Controllers\CacheController;
use App\Http\Controllers\ExpediaApiController;
use DateTime;
USE Log;

//test added jayson

class Map {

	private $session;
	private $cache;
	private $city_id;
	public $default;

	public function __construct() {

		$this->session = new EroamSession;
		$this->cache = new CacheController;
		$this->expediaApi = new ExpediaApiController;

		if(request()->input( 'option' ) == 'manual'){
            $this->city_id = request()->input( 'to_city' );
        }else{
            $this->city_id = request()->input( 'city' );
        }

		if( request()->has( 'auto_populate' ) )
		{
			$auto_populate = request()->input( 'auto_populate' );
			session()->put('auto_populate', $auto_populate );
		}
		else
		{
			$auto_populate = session()->get('auto_populate');
		}
		$formatted_date = date('Y-m-d', strtotime(request()->input('start_date')));
		$filtered_interests = request()->has('interests') ? array_filter(request()->input('interests')) : [] ;
		$child = request()->input('child'); // Total Childrens
		if(!isset($child) && empty($child)){
			$child = array();
		}
		$preferences = session()->get('travel_preferences');
		request()->merge( ['child'=>$child,'start_date' => $formatted_date, 'interests' => $filtered_interests,'travel_preferences'=>$preferences] );
		$this->default = [
			'city_ids'         => [],
			'date_from'        => $formatted_date,
			'auto_populate'    => $auto_populate,
			'traveller_number' => request()->input( 'travellers' ),
			'child_number' 	   => request()->input( 'total_children' ),
			'search_input'     => request()->input(),			
			'rooms'     => request()->input('rooms'),
			'child'     => request()->input('child'),
			'pax_information'  => []
		];

	}

	public function handle() {

		$this->session->save_input( request()->input() );
		
		switch ( request()->input( 'option' ) ) {
			case 'manual':
				$this->handle_manual();
				break;
			case 'auto':
				$this->handle_auto();
				break;
		}
	}

	private function handle_manual() {

		$city = get_city_by_id( $this->city_id );
		if ( $city ) {
			$city['lat'] = $city['latlong']['lat'];
			$city['lng'] = $city['latlong']['lng'];
			// DATA TO BE STORED IN THE SESSION map_data
			$this->default['city_ids'] = [$this->city_id];

			// SET MAP AND DEFAULT DATA
			$auto_sort = session()->has('map_data')['auto_sort'] ? session()->get('map_data')['auto_sort'] : 'on';
			$this->session->set_map( ['type' => 'manual', 'cities' => [$city], 'auto_sort' => $auto_sort] );
			$this->search_default( $this->default);
  		} else {
			return app()->abort( 404, 'City not found.' );
		}
	}

	private function handle_auto() {
		$response = http('post', 'map/auto-routes', [
			'from_city_id' => request()->input('city'),
			'to_city_id'   => request()->input('to_city')
		]);

		$routes = [];
		$default = [];
		if ($response) {
			
			foreach ($response as $key => $value) {
				foreach ($value['route_legs'] as $route_leg) {
					$city = get_city_by_id($route_leg['city_id']);
					if ($city == null) {
						app()->abort(500, 'Auto mode error: City data not found.');
					}
					$city['lat'] = $city['latlong']['lat'];
					$city['lng'] = $city['latlong']['lng'];

					// PUSH TO ROUTES
					$routes[$key]['cities'][] = $city;
					if ($value['is_default'] == 1) {
						$routes[$key]['default'] = 'yes';
						$this->default['city_ids'][] = $route_leg['city_id'];
					} else {
						$routes[$key]['default'] = 'no';
					}
				}
			}
			
			// CHECK FOR ALTERNATIVE ROUTES
			if (count($routes) > 1) {
				foreach ($routes as $key => $value) {

					$default['city_ids'] = array_pluck($value['cities'], 'id');
					$default['date_from'] = $this->default['date_from'];
					$default['traveller_number'] = $this->default['traveller_number'];
					$default['child_number'] = $this->default['child_number'];
					$default['auto_populate'] = $this->default['auto_populate'];
					$default['search_input'] = $this->default['search_input'];
					$default['rooms'] = $this->default['rooms'];
					$default['child'] = $this->default['child'];
					$itinerary = $this->search_default_route($default);
					$last_leg = count($itinerary) - 1;
					$itinerary[$last_leg]['transport'] = NULL;

					$search_data = $this->session->set_search($itinerary, $default, false);
					$routes[$key]['duration'] = $search_data['total_number_of_days'];
					$routes[$key]['price_per_person'] = $search_data['currency'] . ' ' . $search_data['cost_per_person'];
				}
			}

			// IF NO DEFAULT ROUTE, SET THE FIRST ROUTE AS DEFAULT
			if (empty($this->default['city_ids'])) {
				foreach ($routes as $key => $route) {
					$routes[$key]['default'] = 'yes';
					$this->default['city_ids'] = array_column($route['cities'], 'id');
				}
			}

		} else { 
			// FROM CITY
			
			$routes[0]['cities'][0] = get_city_by_id(request()->input('city'));

			$routes[0]['cities'][0]['lat'] = $routes[0]['cities'][0]['latlong']['lat'];
			$routes[0]['cities'][0]['lng'] = $routes[0]['cities'][0]['latlong']['lng'];
			// TO CITY
			$routes[0]['cities'][1] = get_city_by_id(request()->input('to_city'));
			$routes[0]['cities'][1]['lat'] = $routes[0]['cities'][1]['latlong']['lat'];
			$routes[0]['cities'][1]['lng'] = $routes[0]['cities'][1]['latlong']['lng'];
			$routes[0]['default'] = 'yes';
			$this->default['child'] = request()->input('child');
			$this->default['city_ids'] = [request()->input('city'), request()->input('to_city')];
		}

		// SET MAP AND DEFAULT DATA
		$this->session->set_map(['type' => 'auto', 'routes' => $routes, 'auto_sort' => 'on']);
		$this->search_default( $this->default );
	}


	public function search_default( $data ) {
		$itinerary = http( 'post', 'map/city-defaults', $data );
		$last_leg = count($itinerary) - 1;
		$add_days = 0;
		$temp_date_from = $data['date_from'];
		foreach ( $itinerary as $key => $value ) {
			if($value['hotel']){
				$returnRoom = $this->getSelectedHotelRoom($value['hotel'],$data);
				if(empty($returnRoom)){
					$itinerary[$key]['hotel'] = '';
				}else{
					$itinerary[$key]['hotel']['RoomRateDetailsList']['RoomRateDetails'] = $returnRoom;
				}
			}
		
			if (empty($value['hotel'])) { //check if hotel is null
				$return_hotel = $this->set_default_hotel_api($data, $value['city']);
				$itinerary[$key]['hotel'] = $return_hotel;
			}
			if( $key != 0 ){
				$temp_date_from = date('Y-m-d', strtotime( $temp_date_from. ' +'.$itinerary[$key - 1]['city']['default_nights']. ' days' ) );
			}
			if(empty($value['activities'])) {
				$return_activity = $this->set_viator_default_activity($data, $value['city']);
				if( isset( $return_activity['extra_nights']) ){
					$itinerary[$key]['city']['days_to_add']= 1;
					$itinerary[$key]['city']['add_after_date'] = $temp_date_from;
					$itinerary[$key]['city']['default_nights'] = (int)$value['city']['default_nights'] + (int)$return_activity['extra_nights'];
				}
				$itinerary[$key]['activities'] = $return_activity ? [ $return_activity ] : null ;
			}
			
			// added by miguel to determine if own arrangement is set as the transport type preference by the user. If so, remove all transports 
			$trans_own_arrangement = FALSE;
			$search_preferences = session()->get('search_input');
			
			if( isset( $search_preferences['transport_types'] ) )
			{
				// check if the transport_type_id is equal to 25(own arrangement)
				if( $search_preferences['transport_types'][0] == 25 )
				{
					$trans_own_arrangement = TRUE;
				}
			}
			if($last_leg == $key){
				$trans_own_arrangement = TRUE;
			}
			// TRANSPORT HANDLER
			// IF ( search is in manual mode ) OR ( search is in auto mode AND its the first leg )
			if( $this->default['search_input']['option'] == 'auto' && $key == 0 )
			{
				if ( $key != count( $itinerary ) - 1 )
				{

					if( $trans_own_arrangement != TRUE )
					{
						$transport        = $value['transport'];
						$origin_city      = $value['city'];
						$destination_city = $itinerary[ $key + 1 ]['city'];
						$date_from        = add_str_time( '+'.$add_days.' days' , $this->default['date_from'], 'Y-m-d' );
						$add_days         += intval( $value['city']['default_nights'] );
						$date_to          = add_str_time( '+'.$add_days.' days', $this->default['date_from'], 'Y-m-d' );
						// dd( $origin_city );
						$options = [
							'date_from'        => $date_from,
							'date_to'          => $date_to,
							'origin_city'      => $origin_city,
							'destination_city' => $destination_city,
							'traveller_number' => $this->default['traveller_number'],
							'leg'              => $key
						];
						$itinerary[ $key ]['transport'] = $this->get_transport_api_data( $transport, $options );
					}
					else
					{
						$itinerary[ $key ]['transport'] = NULL;
						//$itinerary[ $key ]['transport'] = NULL;
					}
				}
			}

		}
		$this->session->set_search( $itinerary, $this->default );
	}

	function set_default_hotel_api($data, $city){

		//set query data for hotel beds
		$hb_data     = [];
		$hotel       = [];
		$aot_data    = [];
		$ae_data     = [];
		$return_data = [];
		
		// edited by miguel to get hb_destination_codes
		$codes       = $this->getHBDestinationCodes( $city['hb_destinations_mapped'] );
		
		if(!empty($codes)){
			foreach ($codes as $key => $code) {
				$hb_data['city_ids']  = $city['id'];
				$hb_data['check_in']  = Carbon::parse($data['date_from'])->format('Y-m-d');
				$hb_data['check_out'] = Carbon::parse($data['date_from'])->addDays($city['default_nights'])->format('Y-m-d');
				$hb_data['room']      = '1';
				$hb_data['adult']     = $data['traveller_number'];
				$hb_data['code']      = trim($code);
				$hb_data['child']     = '0';
				// check for cached hb data
				$return_hb_data = $this->cache->set($hb_data,'hb');
				$return_data = $return_hb_data;
			}
		}
		if(isset($return_data['hotels']) && $return_data['hotels']['total'] > 0)
		{
			// ADDED BY MIGUEL ON 2017-04-11 04:07 AM TO GET THE FIRST HB HOTEL WHICH HAS A LEGITIMATE FORMAT 
			$ctr         = 0; // variable to increment in loop
			$length      = $return_data['hotels']['total']; // the length of the array
			while( $ctr < $length )
			{
				$first_hotel = $return_data['hotels']['hotels'][ $ctr ]; //get the first hotel
				$first_room  = $return_data['hotels']['hotels'][ $ctr ]['rooms'][0]; //get the first room
				$first_rate  = $return_data['hotels']['hotels'][ $ctr ]['rooms'][0]['rates'][0]; //get the first rate
				if( isset( $first_rate['net'] ) )
				{
					$hotel['hotel_name']  = $first_hotel['name'];
					$hotel['name']        = $first_hotel['name'];
					$hotel['price']       = convert_currency($first_rate['net'],'AUD');
					$hotel['price_id']    = $first_rate['rateKey'];
					$hotel['currency']    = 'AUD';

					$hotel['room_type']   = $first_room['name'];
					$hotel['room_name']   = $first_room['name'];
					$hotel['rate_key']    = $first_rate['rateKey'];
					$hotel['room_id']     = $first_room['code'];
					$hotel['hotel_id']    = $first_hotel['code'];
					$hotel['id']          = $first_hotel['code'];
					$hotel['provider']    = 'hb';
					$hotel['location']    =	isset($first_hotel['address']) ? $first_hotel['address']:NULL ;
					$hotel['description'] = isset($first_hotel['description']) ? $first_hotel['description']:NULL;
					$hotel['images']      = isset($first_hotel['images']) ?$first_hotel['images']:NULL ;
					$hotel['cancellation_policy'] = '';

					$ctr = $length; // set counter to length to end loop
				}
				$ctr++;

			}

		}
		else
		{ 
			//if hb hotels data equals null
			// check if city is part of UAE country, if true, then call AE api
			if( $city['country']['id'] == 502 ) // 502 is the current id of UAE in the database
			{

				$ae_data['FromDate']  = Carbon::parse($data['date_from'])->format('Y-m-d');
				$ae_data['ToDate']    = Carbon::parse($data['date_from'])->addDays($city['default_nights'])->format('Y-m-d');
				$ae_data['Adults']    = $data['traveller_number'];
				// $ae_data['HotelId']   = 0;
				$ae_data['city_name'] = $city['name'];
				$return_ae_data = $this->cache->set($ae_data,'ae');

				$return_data =  $return_ae_data['data'];

				if($return_data && count($return_data) > 0){

					$first_hotel = $return_data[0]; //get the first hotel
					$first_room  = $return_data[0]['rooms'][0]; //get the first room
					
					$hotel['hotel_name']          = $first_hotel['name'];
					$hotel['name']                = $first_hotel['name'];
					$hotel['price']               = convert_currency($first_room['price'],'AUD');
					$hotel['price_id']            = $first_room['room_id'];
					$hotel['currency']            = 'AUD';
					$hotel['room_type']           = $first_room['name'];
					$hotel['room_name']           = $first_room['name'];
					$hotel['rate_key']            = $first_room['room_id'];
					$hotel['room_id']             = $first_room['room_id'];
					$hotel['hotel_id']            = $first_hotel['hotel_id'];
					$hotel['id']                  = $first_hotel['hotel_id'];
					$hotel['provider']            = 'ae';
					$hotel['location']            =	$first_hotel['city_name'];
					$hotel['description']         = $first_hotel['description'];
					$hotel['images'][0]           = $first_hotel['image'];
					$hotel['cancellation_policy'] = '';
				}

			}
			
		}

		return ( !empty( $hotel ) ? $hotel : NULL );

	}

	public function set_viator_default_activity($data,$city){
		$viator_data = [];
		$activity = [];
		
		$return_viator_data = [];
		$city_data_viator = http( 'post', 'service/location',['destinationName' => $city['name']]);
		if($city_data_viator)
		{
			$viator_data['destId'] = $city_data_viator['destinationId'];
			$viator_data['startDate'] = Carbon::parse($data['date_from'])->format('Y-m-d');
			$viator_data['endDate'] = Carbon::parse($data['date_from'])->addDays($city['default_nights'])->format('Y-m-d');
			$viator_data['currencyCode'] = 'AUD';
			$viator_data['sortOrder'] = 'TOP_SELLERS';
			$viator_data['topX'] = '1-15';
			$return_viator_data = $this->cache->set($viator_data,'viator');
			if($return_viator_data && count($return_viator_data['data']) > 0){
				
				$first_viator                    = $return_viator_data['data'][0];//get the first activity
				
				$activity['name']                =  $first_viator['title'];
				$activity['price'][0]['price']   = convert_currency($first_viator['price'],'AUD');
				$hotel['price_id']               =  $first_viator['code'];
				$activity['currency']            =  'AUD';
				$activity['id']                  =  $first_viator['code'];
				$activity['activity_id']         =  $first_viator['code'];
				//$activity['pickup']         	 =  $first_viator['pickup'];
				$activity['provider']            =  'viator';
				$activity['location']            =	'';
				$activity['date_selected']       = $viator_data['startDate'];
				$activity['description']         = $first_viator['shortDescription'];
				$activity['images'][]            = $first_viator['thumbnailURL'];
				$activity['cancellation_policy'] = '';
				
				$activity_duration = count_viator_act_duration( get_viator_duration( $first_viator['duration'] ) );
				$activity['duration'] = $activity_duration;
				
				
				if( $activity_duration > 1 ){
					$extra_night = (int)$activity_duration - ( (int)$city['default_nights'] - 1 );
					if( $extra_night > 0 ){
						$activity['extra_nights'] = $extra_night;
					}
				}
				$activity['duration1'] = $first_viator['duration'];
			}
		}

		return $activity;

	}

	public function get_transport_api_data( $transport, $options )
	{	
		// dd($options);
		$result             = $transport;
		$date_from          = $options['date_from'];
		$date_to            = $options['date_to'];
		$origin_city        = $options['origin_city'];
		$destination_city   = $options['destination_city'];
		$is_flight_filtered = FALSE; 
		if( isset( $this->default['search_input']['transport_types'] ) )
		{
			$transport_type_filters = $this->default['search_input']['transport_types'];
			foreach( $transport_type_filters as $key => $filter )
			{
				if( $filter == '1' ) // if "flight" is selected as one of the transport type filters
				{
					$is_flight_filtered = TRUE;
				}
			}
		}

		if( $transport == NULL || ($transport['transport_type']['id'] != 1 && $is_flight_filtered) )
		{
			// START MYSTIFLY CAll
			$mystifly_rq_data  = [];
			$mystifly_rs_data  = [];
			$origin_iatas      = explode(',', $origin_city['airport_codes'] );
			$destination_iatas = explode(',', $destination_city['airport_codes'] );
			// generate requests
			foreach( $origin_iatas as $origin_iata )
			{
				foreach( $destination_iatas as $destination_iata )
				{
					$rq_data = array(
						'DepartureDate'           => $date_to,
						'OriginLocationCode'      => $origin_iata,
						'DestinationLocationCode' => $destination_iata,
						'CabinPreference'         => 'Y',
						'Code'                    => array('ADT'),
						'Quantity'                => array($options['traveller_number']),
						'IsRefundable'            => "false",
						'IsResidentFare'          => "false",
						'NearByAirports'          => "false"						
					);
					// echo json_encode($rq_data);die;
					array_push( $mystifly_rq_data, $rq_data );
				}
			}

			// check cache if leg is not zero
			if( $options['leg'] != 0 )
			{
				if( count( $mystifly_rq_data ) > 0 )
				{	
					foreach ( $mystifly_rq_data as $key => $rq )
					{
						$leg_has_cache = $this->cache->check( $rq, 'mystifly' );
					}
				}				
			}

			// send requests
			if( $options['leg'] == 0 || $leg_has_cache )
			{
				if( count( $mystifly_rq_data ) > 0 )
				{	
					foreach ( $mystifly_rq_data as $key => $rq )
					{
						$rs_data = $this->cache->set( $rq, 'mystifly' );
						// dd( $rs_data );
						if( $rs_data['success'] ){
							array_push( $mystifly_rs_data, json_decode(json_encode($rs_data['data']), FALSE) );
						}
					}
				}
			}
				
			// get first response if no error
			if( count( $mystifly_rs_data ) > 0 )
			{	
				// dd( $mystifly_rs_data );
				$flights_count = count( $mystifly_rs_data[0]->PricedItineraries->PricedItinerary );
				$ctr           = 0;

				while( $ctr < $flights_count )
				{
					try
					{
						$flight               = $mystifly_rs_data[ $ctr ]->PricedItineraries->PricedItinerary[0];
						$f_segment            = $flight->OriginDestinationOptions->OriginDestinationOption->FlightSegments->FlightSegment;
						//$airline_name         = isset( $f_segment->OperatingAirline->Name ) ? $f_segment->OperatingAirline->Name : 'N/A';
						$airline_name         = isset( $flight->ValidatingAirlineName ) ? $flight->ValidatingAirlineName: 'N/A';
						if(!isset($f_segment->DepartureDateTime)){
							return $result;
						}
						$etd                  = $f_segment->DepartureDateTime;
						$eta                  = $f_segment->ArrivalDateTime;
						$arrival_time         = (new DateTime($eta))->format('H:i');
						$arrival_time2        = (new DateTime($eta))->format('A');
						$departure_time       = (new DateTime($etd))->format('H:i');
						$departure_time2      = (new DateTime($etd))->format('A');
						// $duration_hour        = ( strtotime($eta) - strtotime($etd) ) / 3600;
						// $duration_min         = ( strtotime($eta) - strtotime($etd) ) % 3600;
						// $duration             = $duration_hour.'.'.$duration_min;
						$duration = date('H:i', mktime(0, $f_segment->JourneyDuration)) .' Hours';

						$the_etd = new DateTime( $date_to );
						$formatted_etd = $the_etd->format('jS, F Y');
						// $the_eta = new DateTime( $date_to );
						// if( $f_segment->JourneyDuration > 720 ){
						// 	($arrival_time2 != $departure_time2) ? $the_eta->modify('+1 day') : $the_eta ;
						// }else{
						// 	($arrival_time2 != $departure_time2) ? $the_eta : $the_eta->modify('+1 day') ;
						// }
						// $formatted_eta = $the_eta->format('jS, F Y');

						$dt = Carbon::createFromFormat('jS, F Y H:i', $formatted_etd.' '.$departure_time);
						$dt->addMinutes( $f_segment->JourneyDuration ); 
						$formatted_eta = $dt->format('jS, F Y H:i'); 


						// $booking_summary_text = strtoupper($airline_name).', Flight # '.$f_segment->FlightNumber.' <br/><small>Depart: '.$f_segment->DepartureData.' '.$formatted_etd.' '.$departure_time.'</small><br/><small>Arrive: '.$f_segment->ArrivalData.' '.$formatted_eta.' '.$arrival_time.'</small>';
						$booking_summary_text = strtoupper($airline_name).', Flight # '.$f_segment->FlightNumber.' <br/><small>Depart: '.$f_segment->DepartureData.' '.$formatted_etd.' '.$departure_time.'</small>';
						// echo '<BR>'.$f_segment->DepartureData.'<BR>'.$formatted_etd.'<BR>'.$departure_time.'<BR>';
						// echo $booking_summary_text;exit;
						//<br/><small>Arrive: '.$f_segment->ArrivalData.' '.$formatted_eta.'</small>

						$cabin_class          = ( trim($f_segment->CabinClassText) != '' ) ? $f_segment->CabinClassText : 'Cabin Class Not Specified';
						
						$currency             = $flight->AirItineraryPricingInfo->ItinTotalFare->TotalFare->CurrencyCode;
						$price                = convert_currency( $flight->AirItineraryPricingInfo->ItinTotalFare->TotalFare->Amount, $currency );

						$result = array(
							//'airline_code'            => isset( $f_segment->OperatingAirline->Code ) ? $f_segment->OperatingAirline->Code : 'N/A',
							'airline_code'            => isset( $flight->ValidatingAirlineCode ) ? $flight->ValidatingAirlineCode: 'N/A',
							'arrival_data'            => $f_segment->ArrivalData,
							'arrival_location'        => $f_segment->ArrivalAirportLocationCode,
							'arrival_itinerary_text'  => $dt->format('H:i, jS, F Y'),
							'booking_summary_text'    => $booking_summary_text,
							'cabin_class'             => $cabin_class,
							'currency'                => ( session()->has('currency') ) ? session()->get('currency') : 'AUD',
							'departure_data'          => $f_segment->DepartureData,
							'departure_location'      => $f_segment->DepartureAirportLocationCode,
							'duration'                => $duration,
							'eta'                     => $eta,
							'etd'                     => $etd,
							'fare_source_code'        => $flight->AirItineraryPricingInfo->FareSourceCode,
							'fare_type'               => $flight->AirItineraryPricingInfo->FareType,
							'flight_number'           => $f_segment->FlightNumber,
							'from_city_id'            => $origin_city['id'],
							'id'                      => $flight->AirItineraryPricingInfo->FareSourceCode,
							'is_selected'             => TRUE,
							'operating_airline'       => $airline_name, 
							'passenger_type_code'     => "ADT",
							'passenger_type_quantity' => $this->default['traveller_number'],
							'price'                   => [
								0 => [
									'price' => $price,
									'currency' => [
										'code' => $currency
									]
								]
							],
							'provider'                => 'mystifly',
							'to_city_id'              => $destination_city['id'],
							'transport_id'            => $flight->AirItineraryPricingInfo->FareSourceCode,
							'transport_type'          => [
								'id'   => 1,
								'name' => 'Flight'
							],
							'transport_type_id'       => 1,
							'transport_type_name'     => 'Flight',
							'transporttype'           => [
								'id'   => 1,
								'name' => 'Flight'
							]
						);
						$ctr = $flights_count;

					}
					catch(Exception $e)
					{
						Log::error('Initial mystifly API call error:', $e->getMessage() );
						$ctr++;
					}				
				}
			}

			// END MYSTIFLY CALL

		}
		elseif ( $transport )
		{

		}
		else
		{

		}

		return $result;
	}


	public function getHBDestinationCodes( $hb_destinations_mapped )
	{
		$result = [];
		if( !empty( $hb_destinations_mapped ) )
		{
			foreach( $hb_destinations_mapped as $key => $value )
			{
				array_push( $result, $value['hb_destination']['destination_code'] );
			}
		}
		// Log::error('HB DESTINATION CODES: ');
		// Log::error( json_encode( $result ) );
		return $result;
	}


	public function generate_eta( $etd, $duration )
	{

		$dt = Carbon::create(2012, 1, 31, 0);
		$dt->addMinutes( $duration );
		return $dt->format('jS \\of F Y H:i A');
	}

	public function getSelectedHotelRoom($value,$data)
	{
		$value = json_decode(json_encode($value) , true );
		$num_of_rooms = $data['rooms'];
       	$hotel_request_data = $data['search_input'];
       	$children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.$data['search_input']['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.$data['search_input']['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}
		$date_array = $this->convertUrl($value['deepLink']);
		$code = $value['hotelId'];
		$arrivalDate  = $date_array['arrivalDate'];
		$departureDate = $date_array['departureDate'];
		$room = $expedia_request;

		if(isset($value['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'])){
			$roomTypeCode = $value['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'];
		}else{
			$roomCode = '@roomCode';
			$roomTypeCode = $value['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
		}
		$rateCode = $value['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		if(isset($value['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'])){
			$rateKey = $value['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'];
		}else{
			$rateKey = $value['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'];
		}
		$rateKey = $rateKey;
		$latitude = $value['latitude'];
		$longitude = $value['longitude'];
		$customerSessionId = $value['customerSessionId'];
		//$response = $this->expediaApi->getHotelDetails($code,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,1);
		$response = $this->expediaApi->getHotelDetails($code,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,1);
		$response = json_decode($response)->data;
		if(empty($response)){
			return $response;
		}
		if(isset($response->HotelRoomResponse->rateCode)){
	    	$temp_array[0] = $response->HotelRoomResponse;
		    $response->HotelRoomResponse = $temp_array;
	    }

	    $lowest_price = array();

	    foreach($response->HotelRoomResponse as $key => $RoomResponse){
	    	$total = '@total';
	    	$lowest_price[$key] = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$total;
	    }
	    $min_array = array_keys($lowest_price, min($lowest_price));

		return $response->HotelRoomResponse[$min_array[0]];
	}
	public function convertUrl($url){
	    $remove_http = str_replace('http://', '', $url);
	    $split_url = explode('?', $remove_http);
	    $get_page_name = explode('/', $split_url[0]);
	    $page_name = $get_page_name[1];

	    $split_parameters = explode('&', $split_url[1]);

	    for($i = 0; $i < count($split_parameters); $i++) {
	        $final_split = explode('=', $split_parameters[$i]);
	        $split_complete[$final_split[0]] = $final_split[1];
	    }
	    $return_array['arrivalDate'] = $split_complete['standardCheckin'];
	    $return_array['departureDate'] = $split_complete['standardCheckout'];
	    return $return_array;
	}

	public function search_default_route( $data ) {

		$itinerary = http( 'post', 'map/city-defaults', $data );

		$last_leg = count($itinerary) - 1;
		$add_days = 0;
		//$key = 0;
		//$value = $itinerary[$key];
		$temp_date_from = $data['date_from'];
		foreach ( $itinerary as $key => $value ) {
			if($value['hotel']){
				$returnRoom = $this->getSelectedHotelRoom($value['hotel'],$data);
				if(empty($returnRoom)){
					$itinerary[$key]['hotel'] = '';
				}else{
					$itinerary[$key]['hotel']['RoomRateDetailsList']['RoomRateDetails'] = $returnRoom;
				}
			}
		
			if (empty($value['hotel'])) { //check if hotel is null
				$return_hotel = $this->set_default_hotel_api($data, $value['city']);
				$itinerary[$key]['hotel'] = $return_hotel;
			}
			if( $key != 0 ){
				$temp_date_from = date('Y-m-d', strtotime( $temp_date_from. ' +'.$itinerary[$key - 1]['city']['default_nights']. ' days' ) );
			}
			if(empty($value['activities'])) {
				$return_activity = $this->set_viator_default_activity($data, $value['city']);
				if( isset( $return_activity['extra_nights']) ){
					$itinerary[$key]['city']['days_to_add']= 1;
					$itinerary[$key]['city']['add_after_date'] = $temp_date_from;
					$itinerary[$key]['city']['default_nights'] = (int)$value['city']['default_nights'] + (int)$return_activity['extra_nights'];
				}
				$itinerary[$key]['activities'] = $return_activity ? [ $return_activity ] : null ;
			}
			
			// added by miguel to determine if own arrangement is set as the transport type preference by the user. If so, remove all transports 
			$trans_own_arrangement = FALSE;
			$search_preferences = session()->get('search_input');

			if( isset( $search_preferences['transport_types'] ) )
			{
				// check if the transport_type_id is equal to 25(own arrangement)
				if( $search_preferences['transport_types'][0] == 25 )
				{
					$trans_own_arrangement = TRUE;
				}
			}
			if($last_leg == $key){
				$trans_own_arrangement = TRUE;
			}
			// TRANSPORT HANDLER
			// IF ( search is in manual mode ) OR ( search is in auto mode AND its the first leg )
			if( $this->default['search_input']['option'] == 'auto' && $key == 0 )
			{
				if ( $key != count( $itinerary ) - 1 )
				{

					if( $trans_own_arrangement != TRUE )
					{
						$transport        = $value['transport'];
						$origin_city      = $value['city'];
						$destination_city = $itinerary[ $key + 1 ]['city'];
						$date_from        = add_str_time( '+'.$add_days.' days' , $this->default['date_from'], 'Y-m-d' );
						$add_days         += intval( $value['city']['default_nights'] );
						$date_to          = add_str_time( '+'.$add_days.' days', $this->default['date_from'], 'Y-m-d' );
						// dd( $origin_city );
						$options = [
							'date_from'        => $date_from,
							'date_to'          => $date_to,
							'origin_city'      => $origin_city,
							'destination_city' => $destination_city,
							'traveller_number' => $this->default['traveller_number'],
							'leg'              => $key
						];
						$itinerary[ $key ]['transport'] = $this->get_transport_api_data( $transport, $options );
					}
					else
					{
						$itinerary[ $key ]['transport'] = NULL;
						//$itinerary[ $key ]['transport'] = NULL;
					}
				}
			}

		}
		return $itinerary;
	}
}