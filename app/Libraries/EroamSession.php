<?php

namespace App\Libraries;

use Log;
use Config;
use App\Libraries\Map;

class EroamSession {

	// public function __construct() {

	// 	$this->map = new EroamSession;
	// 	$this->cache = new CacheController;
	// 	$this->expediaApi = new ExpediaApiController;
	// }
	public function get_search() {
		return session()->all();
	}

	public function set_currency( $currency = 'AUD', $currency_id = 1 ) {
		session()->put( 'currency', $currency );
		session()->put( 'currency_id', $currency_id );
	}
	public function set_city( $default_selected_city = 30,$show = 1 ) {
		session()->put( 'default_selected_city', $default_selected_city );
		session()->put( 'showCityModal', $show );
	}
	public function set_all_currencies() {
		// CURRENCY LAYER
		if (!session()->has('currency_layer')) {
			$currency_layer = http('get', 'get_currencies');
			session()->put('currency_layer', $currency_layer);
		}

		// EROAM CURRENCIES
		if (!session()->has('all_currencies')) {
			$all_currencies = http('get', 'all-currencies');
			session()->put('all_currencies', $all_currencies);
		}
	}

	public function get_currency(){
		return session()->get( 'currency' );
	}

	public function set_map( $map_data ) {
		session()->put('map_data', $map_data);
	}

	public function set_search( $itinerary, $data, $save = true )
	{	
		$total_price          = 0;
		$total_number_of_days = 0;
		$travel_date          = $data['date_from'];
		$add_days             = 0;
		$transport_duration   = '';

		$selected_dates       = array();
		$days_to_add          = 0;
		$add_after_date       = '';
		$days_to_deduct       = 0;
		$deduct_after_date    = '';
		$info                 = array();
		$last_leg = count($itinerary) - 1;
		$next_leg_date_from = '';
		$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');

		foreach ( $itinerary as $key => $leg )
		{

			if(!empty($leg['hotel'])){
				$temp_hotel = json_decode(json_encode($leg['hotel']) , true );
				if(!isset($temp_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])){
					$map = new Map;
					$reqest_data['city_ids'] = $leg['city']['id'];
					$reqest_data['date_from'] = session()->get('search_input')['start_date'];
					$reqest_data['auto_populate'] = session()->get('search_input')['auto_populate'];
					$reqest_data['traveller_number'] = session()->get('search_input')['travellers'];
					$reqest_data['child_number'] = session()->get('search_input')['total_children'];
					$reqest_data['search_input'] = session()->get('search_input');
					$reqest_data['rooms'] = session()->get('search_input')['rooms'];
					$reqest_data['child'] = session()->get('search_input')['child'];
					$reqest_data['pax_information'] = [];
					$response = $map->getSelectedHotelRoom($leg['hotel'],$reqest_data);
					$leg['hotel']['RoomRateDetailsList']['RoomRateDetails'] = $response;
				}
			}
			
			$total_number_of_days += intval( $leg['city']['default_nights'] );

			$hotel_nights     = intval( $leg['city']['default_nights'] );
			if (isset( $leg['city']['days_to_add'] ) && isset( $leg['city']['add_after_date']) ) {
				$days_to_add    = $leg['city']['days_to_add'];
				$add_after_date = $leg['city']['add_after_date'];
			}
			if(isset( $leg['city']['days_to_deduct'] ) && isset( $leg['city']['deduct_after_date'])){
				$days_to_deduct    = $leg['city']['days_to_deduct'];
				$deduct_after_date = $leg['city']['deduct_after_date'];
			}
			if( !$days_to_deduct && $next_leg_date_from && isset( $leg['city']['date_from'] ) ){
				$days_to_deduct = date_difference( $next_leg_date_from, $leg['city']['date_from']) > 0 ? date_difference( $next_leg_date_from, $leg['city']['date_from']) : 0 ;
				$deduct_after_date = $data['date_from'];
			}
			if(isset($leg['hotel']['provider']) && $leg['hotel']['provider'] == 'expedia'){	
				//echo '<pre>';print_r($leg['hotel']['provider']);		
				$currencyCode = '@currencyCode';
				$singleRate = '@nightlyRateTotal';

				if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']->RateInfos)){
					$currencyCode = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']->RateInfos->RateInfo->ChargeableRateInfo->$currencyCode;
					$singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']->RateInfos->RateInfo->ChargeableRateInfo->$singleRate;
					$taxes = 0;
					if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']->RateInfos->RateInfo->taxRate)){
						$taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']->RateInfos->RateInfo->taxRate;
					}
				}else{
					$currencyCode = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$currencyCode];
					$singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
					$taxes = 0;
					if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])){
						$taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
					}
				}
                $singleRate = round(($singleRate * $eroamPercentage) / 100 + $singleRate,2); 
                $subTotal = $singleRate;
                $selectedRate = $subTotal + $taxes;
                $RoomRate = $selectedRate;      
        	 	$price_per_person = $RoomRate / $data['traveller_number'];
        	 	$hotel_currency = $currencyCode;
        	 	
        	 	$converted_price_per_person = convert_currency( $price_per_person, $hotel_currency); // convert and round up to nearest integer
				$converted_hotel_price      = $converted_price_per_person * $data['traveller_number'];
				$hotel_price                = $converted_hotel_price; 
	        
	        }elseif( isset( $leg['hotel']['price'] ) ){
				$temp_hotel_price           = ( is_array( $leg['hotel']['price'] ) ) ? array_first( $leg['hotel']['price'] )['price'] : $leg['hotel']['price'];
				$hotel_currency             = ( is_array( $leg['hotel']['currency'] ) ) ? $leg['hotel']['currency']['code'] : $leg['hotel']['currency'];

				if(!isset( $leg['hotel']['provider']) && strpos( $leg['hotel']['prices'][0]['room_type']['name'], 'Dorm') !== FALSE){
					$price_per_person           = $temp_hotel_price;
					
				}else if(isset( $leg['hotel']['provider']) && $leg['hotel']['provider'] == 'eroam' && strpos( $leg['hotel']['room_name'], 'Dorm') !== FALSE  ){

					$price_per_person           = $temp_hotel_price;
				}else{
					
					$price_per_person           = $temp_hotel_price / $data['traveller_number'];
				}

				$converted_price_per_person = convert_currency( $price_per_person, $hotel_currency); // convert and round up to nearest integer
				$converted_hotel_price      = $converted_price_per_person * $data['traveller_number'];
				$hotel_price                = $converted_hotel_price; 
			}else{
				$hotel_price = 0;
			}

			/* allow user to add accomodation*/

			if( $key == 0 && isset($leg['city']['accommodation']) ){
				session()->put('search_input.no_accommodation', 0);
			}
			// CHECK IF MULTI-COUNTRY ITINERARY
			if ( !empty( session()->get( 'search_input' )['no_accommodation'] ) && $key == 0 )
			{
				$hotel_price              = 0;
				$hotel_nights             = $leg['city']['default_nights'];
				$itinerary[$key]['hotel'] = null;
				$itinerary[$key]['activities'] = null;
				$leg['activities'] = null;
			}
			/*------------------------- CALCULATE DATES ------------------------*/

			// Dates from
			$leg_date = add_str_time( '+' . $add_days . ' days ' . $transport_duration, $travel_date, 'Y-m-d' );

			$itinerary[$key]['city']['date_from'] = $leg_date;
			if($next_leg_date_from){
				$leg_date = $itinerary[$key]['city']['date_from'] = $next_leg_date_from;
			}

			/*if($key > 0){
				$itinerary[$key]['city']['date_from'] = date( 'Y-m-d', strtotime( $itinerary[$key - 1]['city']['date_to'] .'+1 day' ) );
				$travel_date = date( 'Y-m-d', strtotime( $travel_date .'+1 day' ) );
			}*/
			// transport
			if ( $leg['transport'] && $leg['transport']['price'] && count($leg['transport']['transporttype']) )
			{

				$leg['transport']['own_arrangement'] = false;
				$transport_price = 0;
				
				if( !isset( $leg['transport']['provider'] ) )
				{
					$leg['transport']['provider'] = 'eroam';
				}
				if($days_to_add){

					$temp_etd = $leg['transport']['etd'];
					$temp_eta = $leg['transport']['eta'];
					switch($leg['transport']['provider']){
						case 'mystifly':
	
							if($days_to_add){
								$itinerary[$key]['transport']['eta'] = date(DATE_ISO8601, strtotime($temp_eta.' +'.$days_to_add.' days'));
								$itinerary[$key]['transport']['etd'] = date(DATE_ISO8601, strtotime($temp_etd.' +'.$days_to_add.' days'));
							}
							if($days_to_deduct){
								$itinerary[$key]['transport']['eta'] = date(DATE_ISO8601, strtotime($temp_eta.' -'.$days_to_deduct.' days'));
								$itinerary[$key]['transport']['etd'] = date(DATE_ISO8601, strtotime($temp_etd.' -'.$days_to_deduct.' days'));
							}

						break;
						default:
						break;
					}
					

				}
				$temp_transport_price      = ( $leg['transport']['price'] ) ? array_last( $leg['transport']['price'] )['price'] : 0;
				$transport_currency        = ( is_array( $leg['transport']['currency'] ) ) ? $leg['transport']['currency']['code'] : $leg['transport']['currency'] ;
				$converted_transport_price = convert_currency( $temp_transport_price, $transport_currency);
				$transport_price           = $converted_transport_price * $data['traveller_number'];
				// array_push($info, 'added transport['.$key.'] - '.$transport_price.' to $total_price - '.$total_price);
				if($key != $last_leg){
					$total_price += number_format( $transport_price, 2,'.','');
					// array_push($info, 'new price = '.$total_price);
				}
			
			}
			else
			{
				$leg['transport']['own_arrangement'] = true;
				$leg['transport'] = null;

				
			}


			// transport duration
			if ( $leg['transport']['own_arrangement'] == false )
			{
				$etd = $leg['transport']['etd'] ? $leg['transport']['etd'] : null;
				$eta = $leg['transport']['eta'] ? $leg['transport']['eta'] : null;

				switch( $leg['transport']['provider'] )
				{
					case 'eroam':
						$transport_duration = calculate_transport_duration( $etd, $eta );
						$itinerary[$key]['transport']['duration'] = $transport_duration;
						//$departure_date = get_departure_date($date_to, $etd, $transport_duration);
						break;

					case 'mystifly':
						/*$transport_duration = calculate_transport_duration( $etd, $eta );
						$itinerary[$key]['transport']['duration'] = $transport_duration;*/
						break;

					default:
						break;
				}

			}


			// UPDATED BY MIGUEL ON 2017-03-28 TO CHECK IF HOTEL OBJECT HAS CHECKIN AND CHECKOUT PROPERTIES
			

			// activities
			if ( $leg['activities'] /*&& !empty($leg['activities'][0])*/)
			{
				// CHECK IF MULTI-COUNTRY ITINERARY && AUTO POPULATE IS NOT CHECKED
				if ( session()->get( 'search_input' )['auto_populate'] == 0 || $hotel_nights < 2 )
				{
					
					$activity_price = 0;
					$itinerary[$key]['activities'] = null;
				}
				else
				{	
					$adjust_next_activity_date = false;
					$add_to_next = 0;
					$dates_to_adjust = [];
					foreach ( $leg['activities'] as $act_key => $activity )
					{
						/*
						| check if date selected is set.
						*/

						if( isset($leg['activities'][$act_key]['date_selected']) ){
							
							$activity_date_selected = strtotime( $leg['activities'][$act_key]['date_selected'] );
							$formatted_date_selected = date( 'Y-m-d', $activity_date_selected );

							if(!$days_to_add && strtotime($leg_date) >= strtotime($formatted_date_selected) ){
								$formatted_date_selected = date('Y-m-d', strtotime($leg_date.' +1 day'));
								$leg['activities'][$act_key]['date_selected'] = $formatted_date_selected;
							}

							if( $days_to_add && $add_after_date){

								if( strtotime($leg_date) > strtotime($add_after_date) ){
									$leg['activities'][$act_key]['date_selected'] = date( 'Y-m-d', strtotime( $formatted_date_selected.' +'.$days_to_add.' day' ) );
									
								}else{
									if( $activity_date_selected <= strtotime( $leg_date ) ){
										
										$leg['activities'][$act_key]['date_selected'] = date( 'Y-m-d', strtotime( $leg_date.' +1 day' ) );
										
									}
								}
							}
							if( $days_to_deduct && $deduct_after_date ){
								
								if( strtotime($leg_date) > strtotime($deduct_after_date) ){
									$leg['activities'][$act_key]['date_selected'] = date( 'Y-m-d', strtotime( $formatted_date_selected.'-'.$days_to_deduct.' day' ) );
								}
							}

							/*
							| Adjust next activity date if there's a multi day activity
							*/
							if( $adjust_next_activity_date ){
								if( in_array( $leg['activities'][$act_key]['date_selected'] , $dates_to_adjust) ){
									$leg['activities'][$act_key]['date_selected'] = date( 'Y-m-d', strtotime( $leg['activities'][$act_key]['date_selected'].' +'.$add_to_next.' day' ) );
									$dates_to_adjust[] = $leg['activities'][$act_key]['date_selected'];
								}
								
							}
							if( isset($leg['activities'][$act_key]['duration']) && ($leg['activities'][$act_key]['duration'] > 1) ){
								$adjust_next_activity_date = true;
								$add_to_next = (int)$leg['activities'][$act_key]['duration'] - 1;

								foreach( range(1, $add_to_next ) as $number ){
									$dates_to_adjust[] = date( 'Y-m-d', strtotime( $formatted_date_selected.' +'.$number.' day' ) );
								}
							}
							/*
							| End
							*/
						}else{
							
							$formatted_date_selected = date('Y-m-d', strtotime($leg_date.' +1 day'));
							$leg['activities'][$act_key]['date_selected'] = $formatted_date_selected;
						}
						$itinerary[$key]['activities'] = $leg['activities'];
						
						// add total price
						//dd($activity);

						$temp_activity_price      = isset( $activity['price'] ) ? array_last( $activity['price'] )['price'] : 0;
						$activity_currency        = ( is_array( $activity['currency'] ) ) ? $activity['currency']['code'] : $activity['currency'];
						$converted_activity_price = convert_currency( $temp_activity_price, $activity_currency);
						$activity_price           = $converted_activity_price * $data['traveller_number'];
						$total_price              += number_format( $activity_price, 2,'.','');
					}
				}
			}



			// add hotel nights
			$add_days += intval( $hotel_nights );
			$itinerary[$key]['city']['date_to'] =  date('Y-m-d', strtotime($itinerary[$key]['city']['date_from'].' +'.$itinerary[$key]['city']['default_nights']. ' days' ));
			$depart_time = isset($itinerary[$key]['transport']['etd']) ? date(' H:i', strtotime($itinerary[$key]['transport']['etd'])) : ' 00:00';
			$departure_new_date = $itinerary[$key]['city']['date_to'].$depart_time;
			$formatted_duration = $leg['transport']['provider'] == 'mystifly' ? get_hours_min($itinerary[$key]['transport']['duration']) : $itinerary[$key]['transport']['duration'];

			$next_leg_date_from = date('Y-m-d', strtotime($departure_new_date.' '.$formatted_duration) );

			// if( isset( $leg['transport']['provider']) ){
			if( count($itinerary) != $key + 1 ){
				switch ( $leg['transport']['provider']) {
					
					case 'eroam':
						$departure               = date( 'H:i ', strtotime($leg['transport']['etd']) );
						$date_to                 = $itinerary[$key]['city']['date_to'];
						$duration                = $itinerary[$key]['transport']['duration'];
						$exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($departure.' '.$duration));
						$departure_date          = get_departure_date( $date_to, $departure, $duration );
						$from_city               = $leg['city']['name'];
						$to_city                 = $itinerary[ $key + 1 ]['city']['name'];
						$departTimezone          = get_timezone_abbreviation($leg['city']['timezone']['name']);
						$arriveTimezone          = get_timezone_abbreviation($itinerary[ $key + 1 ]['city']['timezone']['name']);
						$itinerary[$key]['transport']['transport_name_text'] = $leg['transport']['operator']['name'] . ' (' . $leg['transport']['transporttype']['name'].')';
						$itinerary[$key]['transport']['departure_text'] 	 = $from_city.' ('.$leg['city']['airport_codes'].')<br/>'.date('j M Y', strtotime($date_to.' '.$leg['transport']['etd'])).' @ '.date('H:i', strtotime($date_to.' '.$leg['transport']['etd']));
						$itinerary[$key]['transport']['arrival_text'] 		 = $to_city.' ('.$itinerary[ $key + 1 ]['city']['airport_codes'].')<br/>'.date('j M Y', strtotime($next_leg_date_from)).' @ '.date('H:i', strtotime($exact_arrival_date_time));
						
						$itinerary[$key]['transport']['booking_summary_text'] =  $leg['transport']['operator']['name'] . ' (' . $leg['transport']['transporttype']['name'] . ' ) <br/><small>Depart: '.$itinerary[$key]['transport']['departure_text'].'</small><br/><small>Arrive: '.$itinerary[$key]['transport']['arrival_text'].' </small>';
						break;

					case 'mystifly':
						$last = count($itinerary) - 1;
						$itineraryIndex = $key == $last ? $key : $key + 1;
						$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
			
						$arriveTimezone = get_timezone_abbreviation($itinerary[$itineraryIndex]['city']['timezone']['name']);
						
						$from_city               = $leg['city']['name'];
						$to_city                 = $itinerary[ $key + 1 ]['city']['name'];
						$itinerary[$key]['transport']['transport_name_text'] = strtoupper( $leg['transport']['operating_airline']).', Flight #'.$leg['transport']['flight_number'];
						$itinerary[$key]['transport']['departure_text'] 	 = $from_city.' ('.$leg['city']['airport_codes'].')<br/>'.date('j M Y', strtotime($itinerary[$key]['city']['date_to'])).' @ '.date('H:i', strtotime($itinerary[$key]['transport']['etd']));
						$itinerary[$key]['transport']['arrival_text'] 		 = $to_city.' ('.$itinerary[ $key + 1 ]['city']['airport_codes'].')<br/>'.date( 'j F Y', strtotime($next_leg_date_from)).' @ '.date('H:i', strtotime($itinerary[$key]['transport']['eta']));
			
						$itinerary[$key]['transport']['booking_summary_text'] = ucwords(strtolower( $leg['transport']['operating_airline'] )).', Flight # '.$leg['transport']['flight_number'].'<br/><small>Depart: '.$leg['transport']['departure_data'].' '.date('jS, F Y', strtotime($itinerary[$key]['city']['date_to'])).date( ' H:i', strtotime( $itinerary[$key]['transport']['etd'] ) ).'</small><br/><small>Arrive: '.$leg['transport']['arrival_data'].' '.date( 'jS, F Y', strtotime( $next_leg_date_from ) ) .' '.date( 'H:i', strtotime( $itinerary[$key]['transport']['eta'] ) ).'</small>';
						break;

					case 'busbud':
                        $last = count($itinerary) - 1;
                        $itineraryIndex = $key == $last ? $key : $key + 1;
                        $departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);

                        $arriveTimezone = get_timezone_abbreviation($itinerary[$itineraryIndex]['city']['timezone']['name']);
                        $departure               = date( 'H:i ', strtotime($leg['transport']['etd']) );
                        $date_to                 = $itinerary[$key]['city']['date_to'];
                        $duration                = $itinerary[$key]['transport']['duration'];
                        $exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($departure.' '.$duration));
                        $departure_date          = get_departure_date( $date_to, $departure, $duration );

                        $from_city               = $leg['city']['name'];
                        $to_city                 = $itinerary[ $key + 1 ]['city']['name'];
                        $itinerary[$key]['transport']['transport_name_text'] = 'Coach Minivan (' . $leg['transport']['transporttype']['name'].')';
                        $itinerary[$key]['transport']['departure_text'] 	 = $from_city.' ('.$leg['city']['airport_codes'].')<br/>'.date('j M Y', strtotime($itinerary[$key]['city']['date_to'])).' @ '.date('H:i', strtotime($itinerary[$key]['transport']['etd']));
                        $itinerary[$key]['transport']['arrival_text'] 		 = $to_city.' ('.$itinerary[ $key + 1 ]['city']['airport_codes'].')<br/>'.date( 'j M Y', strtotime($itinerary[$key]['transport']['eta'])).' @ '.date('H:i', strtotime($itinerary[$key]['transport']['eta']));
                        break;	
					
					default:
						break;
				}
			}

			/*
			| Added by Junfel
			*/
		
			if( $itinerary[$key]['hotel']){
				
				$checkout = $itinerary[$key]['city']['date_to'];
				if(isset($leg['hotel']['rooms']) && !empty($leg['hotel']['rooms'])){
					$leg['hotel']['rooms'] = $leg['hotel']['rooms'];
				}
				
				$hotel_nights = date_difference(date('Y-m-d', strtotime($leg_date) ), $checkout) <= 0 ? 0 : date_difference(date('Y-m-d', strtotime($leg_date) ), $checkout) ;
				if($hotel_nights){
					$itinerary[$key]['hotel']['nights'] = $hotel_nights;
					$itinerary[$key]['hotel']['checkin'] = date('Y-m-d', strtotime($leg_date) );
					$itinerary[$key]['hotel']['checkout'] = $checkout;

					if($hotel_price){
						$hotel_price      = $converted_hotel_price;
					}
					$total_price += number_format( $hotel_price, 2,'.','');
				}else{
					$itinerary[$key]['hotel'] = null;
				}
			}

			/*
			| End Added by Junfel
			*/

			// clear transport if last index
			if ( count( $itinerary ) > 1 && count( $itinerary ) - 1 == $key || empty($leg['transport']) ) {
				$itinerary[$key]['transport'] = null;
			}

			if (isset( $itinerary[$key]['city']['days_to_add'] ) && isset( $itinerary[$key]['city']['add_after_date']) ) {
				unset($itinerary[$key]['city']['days_to_add']);
				unset($itinerary[$key]['city']['add_after_date']);
			}

			if (isset( $itinerary[$key]['city']['days_to_deduct'] ) && isset( $itinerary[$key]['city']['deduct_after_date']) ) {
				unset($itinerary[$key]['city']['days_to_deduct']);
				unset($itinerary[$key]['city']['deduct_after_date']);
			}

		}
		
		$cost_per_day    = number_format( ( $total_price / $total_number_of_days ), 2 );
		$cost_per_person = number_format( $total_price / $data['traveller_number'], 2 );
		$pax_information = isset( $data['pax_information'] ) ? $data['pax_information'] : [];
		// prepare search data and store to session
		$search_data = [
			'cost_per_day'         => $cost_per_day,
			'currency'             => ( session()->has('currency') ? session()->get('currency') : 'AUD' ),
			'cost_per_person'      => $cost_per_person,
			'itinerary'            => $itinerary,
			'total_number_of_days' => sing( $total_number_of_days, 'Nights'),
			'travel_date'          => $data['date_from'],
			'travellers'           => $data['traveller_number'],
			'child_total'          => @$data['child_number'],
			'rooms'           	   => @$data['rooms'],
			'pax_information'      => $pax_information,
			'auto_populate'        => ( session()->has('auto_populate') ? session()->get('auto_populate') : 1 ) // autopopulate activities
		];
		if ($save == true) {			
			session()->put( 'search', $search_data );
		} else {
			return $search_data;
		}

	}
	public function set_transport_filter( $type, $day, $fl_options ) {
		session()->put( 'transport_filter', $type );
		session()->put( 'day', $day );
		session()->put( 'flight_options', $fl_options );
	}
	public function set_travel_pref( $travel_pref ) {
		session()->put( 'travel_preferences', $travel_pref );
	}

	public function get_flight_options(){
		return session()->get( 'flight_options' );
	}

	public function save_search( $search ) {
		if ( $search ) {
			session()->put( 'search', json_decode( $search, true ) );
		}
	}

	public function save_input( $input ) {
		if ( $input ) {
			session()->put( 'search_input', $input );
		}
	}

	public function update_dates_available($dates_available, $id){
		if($id){
			session()->put('dates_available_'.$id, $dates_available);
		}
	
	}

	public function set_tourCountry_session( $tourCountry ) {
		session()->put( 'tourCountryData', $tourCountry );
	}

	public function set_tourHome_session( $fromTourHome ) {
		session()->put( 'fromTourHome', $fromTourHome );
	}

	
	public function initial_session(){
		session()->put('initial_authentication', true );
	}

}