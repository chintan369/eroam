<section class="banner-container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide" src="{{ url( 'images/banner1.jpg' ) }}" alt="eRoam">
            </div>
        </div>
        <div class="banner-caption">
            <div class="banner-inner">
                <form method="post" action="/tours" id="search-form" class="form-horizontal clearfix">
                    {{ csrf_field() }}

                    <input type="hidden" name="country" id="starting_country" value="{{ $from_country_id }}">
                    <input type="hidden" name="city" id="starting_city" value="{{ $from_city_id }}">

                    <input type="hidden" name="to_country" id="destination_country" value="">
                    <input type="hidden" name="to_city" id="destination_city" value="">

                    <input type="hidden" name="auto_populate" id="auto-populate" value="1">

                    <input type="hidden" name="countryId" id="countryId" value="">
                    <input type="hidden" name="countryRegion" id="countryRegion" value="">
                    <input type="hidden" name="countryRegionName" id="countryRegionName" value="">
                    <input type="hidden" id="search_option_id" name="option" value="packages">
                    <div id="changePreferences">
                        <?php 
                          /*
                          | Added by Rekha
                          | Accomodation multi select options
                          */
                          $accommodation_options = '';
                          $room_type_options = '';
                          $transport_type_options  = '';
                          $nationality = '';
                          $gender = '';
                          $age_group = '';
                          $interestoption ='';
                          
                          if ( count( $travellers['categories'] ) > 0 ) {
                            foreach( $travellers['categories'] as $category ){
                              if( empty( $category['name'] ) ){
                                continue;
                              }
                              if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
                                $accommodation_options .= '<input type="hidden" name="hotel_category_id[]"  value="'.$category['id'].'" >';
                              }
                            }
                          }

                          if( count( $travellers['room_types'] ) > 0 ){
                            foreach( $travellers['room_types'] as $room_type){
                              if( empty( $room_type['name'] ) ){
                                continue;
                              }
                              if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
                                $room_type_options .= '<input type="hidden" name="room_type_id[]" value="'.$room_type['id'].'" >';
                              }
                            }
                          }

                          if( count( $travellers['transport_types'] ) > 0 ){
                            foreach( $travellers['transport_types'] as $transport_type){
                              if( empty( $transport_type['name'] ) ){
                                  continue;
                              }
                              if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
                                $transport_type_options .= '<input type="hidden" name="transport_types[]" value="'.$transport_type['id'].'" >';
                              }
                            }
                          }
                      

                          if( count( $travellers['nationalities'] ) > 0 ){
                            foreach( $travellers['nationalities']['featured'] as $featured ){
                              if( empty( $featured['name'] ) ){
                                continue;
                              }

                              if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) {
                                $nationality .= '<input type="hidden" name="nationality" value="'.$featured['id'].'" > ';
                              }
                            }
                            
                            foreach( $travellers['nationalities']['not_featured'] as $not_featured ){
                              if( empty( $not_featured['name'] ) ){
                                continue;
                              }
                              if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) ){
                                $nationality .= '<input type="hidden" name="nationality" value="'.$not_featured['id'].'" >';
                              }
                            }
                          }

                          $gender_array = ['male', 'female', 'other'];
                          foreach($gender_array as $gen){
                            if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) {
                              $gender .= '<input type="hidden" name="gender" value="'.$gen.'">';
                            }
                          }
                        
                          if( count( $travellers['age_groups'] ) > 0 ){
                            foreach( $travellers['age_groups'] as $age){
                              if( empty( $age['name'] ) ){
                                continue;
                              }
                              if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) {
                                $age_group .= '<input type="hidden" name="age_group" value="'.$age['id'].'" >'; 
                              }
                            }
                          }

                          if( count($labels) > 0 ){
                            foreach ($labels as $i => $label){
                              if( empty( $age['name'] ) ){
                                continue;
                              }

                              $sequence = '';
                              if( in_array($label['id'], $interest_ids ) ){ 
                                $sequence = array_search($label['id'], $interest_ids); 
                                $sequence += 1;
                                $interestoption .='<input type="hidden" name="interests['.$sequence.']" value="'.$label['id'].'">';
                              }      
                            }
                          }
                          echo $accommodation_options.$room_type_options.$transport_type_options.$nationality.$gender.$age_group.$interestoption;
                        ?>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="text-center">
                            <h1>The Boutique Travel Experts</h1>
                            <p>Over 500,000 hotels, 1,000 airlines, plus 100,000’s events, tours &amp; restaurants.</p>
                        </div>
                        <div class="row m-t-30">
                            <div class="col-sm-3 packages-dropdown">
                                <div class="dropdown packages search_dropdown_menu">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><span class="packages-option" id="packages-option" data-value="packages">Tours / Packages</span><span class="fa fa-caret-down"></span>
                          </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="packages-option">
                                        <li><a href="javascript://" class="package-select" data-id="packages" value="packages">Tours / Packages</a></li>
                                        <li><a href="javascript://" class="package-select" data-id="itinerary" value="itinerary">Create Itinerary</a></li>

                                        <li><a href="javascript://" class="package-select" data-id="flights" value="flights">Flights</a></li>
                                        <li><a href="javascript://" class="package-select" data-id="carhire" value="carhire">Car Hire</a></li>
                                        <li><a href="javascript://" class="package-select" data-id="hotels" value="hotels">Hotels</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-7 search-control padding-0">
                                <div id="project_div">
                                    <input type="text" name="project" id="project" class="form-control ui-autocomplete-input" placeholder="Where do you want to go?" autocomplete="off">
                                    <label for="project" generated="true" class="error active" id="project_cls" style="display: none;">This field is required.</label>
                                </div>
                                <div id="end_location_div">
                                    <input type="text" name="end_location" id="end_location" class="search_from_col form-control ui-autocomplete-input" placeholder="Where do you want to go?" autocomplete="off" style="display:none;">
                                    <label for="end_location" generated="true" class="error active" id="end_location_cls" style="display: none;">This field is required.</label>
                                </div>
                            </div>
                            <div class="col-sm-2 search-btn1">
                                <input type="submit" name="" class="btn btn-primary btn-block" id="search_btn" value="Search">
                            </div>
                        </div>
                        <div class="search-mainWrapper">
                            <div class="search-wrapper" id="itinerary">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <label class="radio-checkbox label_radio r_on" for="tailor_auto"><input type="radio" name="option1" id="tailor_auto" value="auto" checked class="radio_tailor">Tailor-Made Auto</label>
                                        <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                                        <label class="radio-checkbox label_radio m-r-10" for="tailor_manual"><input type="radio" name="option1" id="tailor_manual" value="manual" class="radio_tailor">Manual Itinerary</label>
                                        <!--  <label class="radio-checkbox label_check" for="checkbox-02"><input type="checkbox" id="checkbox-02" value="2">Manual Itinerary</label> -->
                                        <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>
                                    </div>
                                    <div class="col-sm-4 prefrence-right">
                                        <a href="#" class="advance-text" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="icon-fingerprint"></i> <span>Personal Preferences</span></a>
                                    </div>
                                </div>
                                <div class="searchMode-container search-box1 mode-block">
                                    <div class="row m-t-10">
                                        <div class="col-sm-3" id="departure_to_cls">
                                            <div class="panel-form-group input-control" id="start_location_div">
                                                <span class="label-control">Departing From</span>
                                                <input type="text" name="start_location" id="start_location" class="form-control ui-autocomplete-input" placeholder="Location" autocomplete="off" />
                                            </div>
                                            <div id="div_start_location">
                                                <label for="start_location" id="start_loc" generated="true" class="error active" id="start_location_cls" style="display: none;">This field is required.</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 date-control" id="departure_date_cls">
                                            <div class="panel-form-group">
                                                <span class="label-control">Departure Date</span>
                                                <div class="input-group datepicker">
                                                    <input id="start_date" name="start_date" type="text" placeholder="Thu 22 Feb 2018" class="form-control start_date">
                                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                </div>
                                            </div>
                                            <label for="start_date" generated="true" class="error active" style="display: none;">This field is required.</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="panel-form-group">
                                                <label class="label-control">Rooms</label>
                                                <select class="form-control">
                                                    <option value="0">0</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="panel-form-group">
                                                <label class="label-control">Adults (18+)</label>
                                                <select class="form-control" name="travellers" id="travellers">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="8">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="panel-form-group">
                                                <label class="label-control">Children (0-17)</label>
                                                <select class="form-control">
                                                    <option value="0">0</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-wrapper" id="comming_soon">
                                <p>Coming Soon</p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php 
    $i=0;
    $allCountry = array();
    foreach($countries as $country){
      foreach ($country['countries'] as $country_data){
          $allCountry[$i]['name'] = $country_data['name'];
          $allCountry[$i]['id'] = $country_data['id'];
          $allCountry[$i]['region'] = $country['id'];
          $allCountry[$i]['regionName'] = $country['name'];
          $i++;
      } 
    }
    usort($allCountry, 'sort_by_name');

?>

@section('custom-css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
<link rel="stylesheet" href="{{  url( 'css/eroam_dev.css') }}">    
@stop
@section('content')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    var projects = [
        <?php for($i=0; $i<count($cities);$i++) { ?>{
            value: "<?php echo $cities[$i]['id'];?>",
            label: "<?php echo $cities[$i]['name'];?>",
            desc: "<?php echo $cities[$i]['country_name'];?>",
            cid: "<?php echo $cities[$i]['country_id'];?>"
        },
        <?php } ?>
    ];

    var countries = [
        <?php for($i=0;$i<count($allCountry);$i++) { ?>{
            value : "<?php echo $allCountry[$i]['id'];?>",
            label : "<?php echo $allCountry[$i]['name'];?>",
            rid   : "<?php echo $allCountry[$i]['region'];?>",
            rname : "<?php echo $allCountry[$i]['regionName'];?>"
        },
        <?php } ?>
    ];
</script>
<script type="text/javascript" src="{{  url( 'js/eroam_js/search_banner.js?v='.$version.'') }}"></script>