<?php
ini_set('max_execution_time', 600);
/**
|---------------------------------------------
| All App routes without middlewares
|---------------------------------------------
 */
Route::get('app/login', 'AuthnticationController@initial');

Route::post('validate-auth-credentials', 'AuthnticationController@validate_auth_credentials');

Route::get('/redirect/{service}', 'SocialAuthController@redirect');
Route::get('/callback/{service}', 'SocialAuthController@callback');
	
	/* Routes for store sass css into css file*/
	Route::get('/sass-to-css', 'PagesController@sass_to_css');
	
	Route::get('/', 'PagesController@home');
	Route::post('/home', 'PagesController@maphome');
	Route::post('view-order-pdf', 'PagesController@view_order_pdf');
	//pdf views
	Route::get('proposed-itinerary', 'PagesController@proposed_itinerary');
	Route::get('view-itinerary', 'PagesController@view_itinerary');
	Route::get('proposed-itinerary-pdf', 'PagesController@propose_itinerary_pdf');
	Route::get('view-order-pdf', 'PagesController@propose_itinerary_pdf');
	Route::get('{city_name}/hotels', 'PagesController@hotels');
	Route::get('{city_name}/activities', 'PagesController@activities');
	Route::get('activity/view-more', 'PagesController@view_more_activity_details');
	Route::get('eroam/activity/{id}', 'PagesController@eroam_activity_view_more');
	Route::get('viator/activity/{code}', 'PagesController@viator_activity_view_more');
	Route::get('{city_name}/transports', 'PagesController@transports');
	Route::get('eroam/hotel/{id}', 'PagesController@eroam_hotel_view_more');
	Route::get('hb/hotel/{code}', 'PagesController@hb_hotel_view_more');
	Route::get('aot/hotel/{code}', 'PagesController@aot_hotel_view_more');
	Route::get('map', 'MapController@map');
	Route::post('map/update', 'MapController@update');
	Route::post('map/add-cities-auto-map', 'MapController@add_cities_to_auto_map');
	Route::post('update-booking-summary', 'PagesController@update_booking_summary');
	Route::post('save-booking', 'PagesController@saveBooking');
	Route::get('map/auto', 'MapController@auto');
	Route::post('search', 'MapController@search');
	Route::post('map/switch-route', 'MapController@switch_route');

	Route::post('get-cities-by-country-id', 'PagesController@get_cities_by_country_id');
	Route::get('cache_locations', 'PagesController@cache_locations');
	Route::post('set-cache-api-data', 'CacheController@set_api_cache');
	Route::post('get-cache-api-data', 'CacheController@get_api_cache');
	Route::get('cache-apis', 'CacheController@cache_apis');
	/*
		for caching all cities data
	*/
	Route::get('cache_all_city', 'CacheController@cacheAllCity');
	Route::get('cache_temp_mystifly', 'CacheController@tempCacheMystifly');
	Route::get('check_product_cache', 'CacheController@checkProductCache');
	Route::get('cache_hb_location_codes', 'CacheController@cacheHbLocationCodes');
	Route::get('cache_run_hb_dest', 'CacheController@runCacheHbDestinationCodes');
	/*
		 | Routes for Dummy data
	*/

	Route::post('hotel_detail', 'PagesController@view_more_hotel_details');
	Route::get('transport-lists', 'PagesController@get_transports');
	Route::get('about-us', 'PagesController@about_us');
	Route::get('privacy-policy', 'PagesController@privacy_policy');
	Route::get('affilliates', 'PagesController@affilliates');
	Route::get('terms', 'PagesController@terms');
	Route::get('contact-us', 'PagesController@contact_us');
	Route::get('sitemap', 'PagesController@sitemap');
	Route::get('register', 'PagesController@register');
	Route::post('enquiry-form', 'PagesController@enquiryForm');
	/*
		 |End Routes for Dummy data
	*/
	Route::post('contact-us', 'PagesController@send_contact_form');
	Route::post('register', 'PagesController@send_registration');
	Route::post('signup', 'UserController@send_registration');
	Route::post('email/validate', 'UserController@email_validate');

	Route::get('session/search', 'SessionController@get_search_session');
	Route::post('session/currency', 'SessionController@set_currency');
	Route::get('session/currency', 'SessionController@get_currency');
	Route::post('session/currency-layer', 'SessionController@set_currency_layer');
	Route::post('session/transport-filter', 'SessionController@set_transport_filter');
	Route::post('session/travel-preferences', 'SessionController@set_travel_preferences');
	Route::post('session/flight-options', 'SessionController@set_flight_options');
	Route::post('session/city', 'SessionController@set_city');

	Route::get('send/itinerary', 'PagesController@email_proposed_itinerary');
	Route::get('latest-search', 'SessionController@get_search');

	Route::get('session/updatePreferences', 'PagesController@updatePreferences');

	Route::get('expedia/gethotels', 'ExpediaApiController@getHotels');

	/*
		| Route for updating session availables dates
	*/
	Route::post('session/dates-available', 'SessionController@update_dates_available');
	Route::resource('booking/summary', 'BookingSummaryController');

	// USER ROUTES
	Route::get('reset-password/{password_token}', 'UserController@reset_password_view');
	Route::post('reset-password', 'UserController@send_reset_password');
	Route::get('forgot-password', 'UserController@forgot_password_view');
	Route::post('forgot-password', 'UserController@send_forgot_password');
	Route::get('login', 'UserController@login_view');
	Route::post('login', 'UserController@login');
	Route::get('logout', 'UserController@logout');
	Route::get('confirmation/{id}', 'UserController@cofirm_register');
	Route::group(['middleware' => ['user_auth']], function () {
		Route::get('profile/step1', 'UserController@profile_step1');
		Route::get('profile/demostep1', 'UserController@profile_demostep1');
		Route::get('profile/step2', 'UserController@profile_step2');
		Route::get('profile/step3', 'UserController@profile_step3');
		Route::get('profile/step4', 'UserController@profile_step4');
		Route::post('profile_step1', 'UserController@profile_step1_store');
		Route::post('profile_step2', 'UserController@profile_step2_store');
		Route::get('profile_step3', 'UserController@profile_step3_store');
		Route::post('profile_step4', 'UserController@profile_step4_store');
		Route::get('manage/trips', 'UserController@manage_trips');
		Route::get('view/trips/{order_id}', 'UserController@trip_details');
		Route::get('profile', 'UserController@profile_view');
		Route::post('profile/update-travel-preferences', 'UserController@update_travel_preferences');
		Route::post('save/travel-preferences', 'UserController@save_travel_preferences');
	});
	Route::get('itinerary/{reference_no}', 'UserController@view_itinerary');

	Route::get('test', 'PagesController@test');
	Route::post('payment', 'PagesController@payment');
	Route::get('payment', 'PagesController@order');

	/* Added By Rekha Patel - Tour Country Checkbox Data Save In Session - 25th Oct 2017*/
	Route::post('tours', 'ToursController@search');
	Route::post('set_session_tours', 'ToursController@set_session_tour');
	Route::post('session/set_tourCountry', 'SessionController@set_tourCountry');
	Route::post('existsImage', 'ToursController@existsImage');
	Route::any('tourDetail/{id}/{url}', 'ToursController@tourDetail');
	Route::post('tourPayment', 'ToursController@payment');

	Route::post('session/set_tourHome', 'SessionController@set_tourHome');

/*============================================================
BOOKING PROCESS - NEW UI
============================================================*/
Route::get('book/pax-information', 'BookingController@pax_information_page');
Route::get('book/confirm', 'BookingController@confirm_page');
Route::get('book/checkout', 'BookingController@checkout_page');

Route::get('review-itinerary', 'PagesController@review_itinerary_view');
Route::get('payment-summary', 'PagesController@payment_summary_view');
Route::get('/remove-itinerary/{itineraryNumber}/{itineraryType}/{activityNumber?}', 'PagesController@remove_itinerary');
Route::get('/signin-guest-checkout/', 'PagesController@book_itinerary_signin_guest_checkout');
Route::get('order-success', 'PagesController@ordersuccess');
Route::get('tourOrderSuccess', 'ToursController@orderSuccess');

Route::get('tours/{city_name}', 'PagesController@tourDetails');
Route::get('searchHotels', 'ToursController@searchHotels');

//Date - 24-04-2018
Route::post('room-details', 'PagesController@roomDetails');


Route::get('language/{language}', 'LanguageController@index');

Route::any('signin-guest-checkout-tour', 'PagesController@book_tour_signin_guest_checkout');
Route::get('book/tour', 'PagesController@tourBookingForm');
Route::post('tour/payment', 'ToursController@tourPayment');
Route::get('store/viatorTours', 'PagesController@storeViatorTours');

function pr($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}