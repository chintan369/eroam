        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="list-unstyled m-0">
                            <li class="list-inline-item m-0 footer_logo"><a class="rounded" href="#"><img src="{{ url('images/footer_logo.png') }}" alt="" /> </a> </li>
                            <li class="list-inline-item m-0"><a class="rounded" href="https://www.facebook.com/eroam.official/" target="_blank"><i class=" ic-facebook"></i> </a> </li>
                            <li class="list-inline-item m-0"><a class="rounded" href="https://twitter.com/EroamOfficial" target="_blank"><i class=" ic-twitter"></i> </a> </li>
                            <li class="list-inline-item m-0"><a class="rounded" href="https://www.linkedin.com/company/eroam-pty-ltd?trk=prof-exp-company-name" target="_blank"><i class=" ic-linkedin"></i> </a> </li>
                            <li class="list-inline-item m-0"><a class="rounded" href="https://www.pinterest.com.au/EroamOfficial/" target="_blank"><i class="  ic-pintrest"></i> </a> </li>
                            <li class="list-inline-item m-0"><a class="rounded" href="https://plus.google.com/115818520227253101465?hl=en" target="_blank"><i class=" ic-google_plus"></i> </a> </li>
                            <li class="list-inline-item m-0"><a class="rounded" href="https://www.youtube.com/channel/UCdPYvmFa1Ivt4DYGv42OxSw" target="_blank"><i class=" ic-you_tube"></i> </a> </li>

                        </ul>
                        <div class="footernav">
                            <a href="/">Home</a>
                            <span>|</span>
                            <a href="{{ url('terms') }}">Terms &amp; Conditions</a>
                            <span>|</span>
                            <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
                        </div>
                        <div class="copyright pt-2 pb-4">
                            <p class="mb-0">Powered by eRoam © Copyright 2016-2018. All Rights Reserved. Patent pending AU2016902466</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
        @include('partials.alert')

        @include('partials.sessionexpire')

        @include('partials.login')

        @include('partials.signup')

        @include('partials.forgotpassword')

        @include('partials.cities')

    </body>
    <input type="hidden" id="currency-layer" value="{{ json_encode(session()->get('currency_layer')) }}">
    <input type="hidden" id="site-url" value="{{ url('/') }}">
    <input type="hidden" id="api-url" value="https://cms.eroam.com/eroam/api/v2/">
    <input type="hidden" id="cms-url" value="https://cms.eroam.com/">
    <input type="hidden" id="search-session" value="{{ json_encode( session()->get( 'search' ) ) }}">
    <input type="hidden" id="search-input" value="{{ json_encode( session()->get( 'search_input' ) ) }}">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="hidden" id="itinerary-page" value="{{ request()->segment(2) }}">
    <input type="hidden" id="itinerary-leg" value="{{ request()->input('leg') }}">
    <script src="{{ url('js/popper.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ url('js/slick.js') }}"></script>
    <script src="{{ url('js/custom.js') }}"></script>
    <script src="{{ url('js/theme/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
    <script src="{{ url('js/eroam.js' ) }}"></script>
    <script src="{{ url('js/common.js') }}"></script>
    @yield( 'custom-js' )
    @stack('scripts')
</html>
