@extends('layouts.common')
@section('content') 
  <div class="body_sec">
          <div class="loginpage" style="background-image:url(images/login-bg.jpg);">
   <div class="container">
      <div class="loginouter">
         <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
               <a class="nav-link pt-3 pb-3 active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="home" aria-selected="true">Sign In</a>
            </li>
            <li class="nav-item">
               <a class="nav-link pt-3 pb-3" id="signup-tab" data-toggle="tab" href="#signup" role="tab" aria-controls="profile" aria-selected="false">Sign Up</a>
            </li>
         </ul>
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="home-tab">
               <div class="login_icon">
                  <i class="fa fa-user-o" aria-hidden="true"></i>
               </div>
               <div class="login_details">
                  <h4>Existing Customer Account</h4>
                  <p class="mb-5">Please fill in the details below to search for a registered client.</p>
                  @if(session()->has('reset-success'))
                      <p class="success-box">
                          {{ session()->get('reset-success') }}
                      </p>
                  @endif
                  @if(session()->has('register-success'))
                      <p class="success-box">
                          {{ session()->get('register-success') }}
                      </p>
                  @endif
                  <p class="login-error-msg" id="login-error-msg">

                  </p>
                  <form id="login-form1">
                     <div class="form-group input_effects  mt-4 mb-2">
                        <input type="email" class="form-control input_effect" id="username" name="username">
                        <label for="username">Email address</label>
                        <span class="focus-border"></span>
                     </div>
                     <div class="form-group input_effects mt-4 mb-2">
                        <input type="password" class="form-control input_effect" id="password" name="password">
                        <label for="password">Password</label>
                        <span class="focus-border"></span>
                     </div>
                     <p class="mb-5 mt-4"><a href="{{ url('/forgot-password') }}" class="forgot">Forgot Password?</a> </p>
                     <div class="row">
                        <div class="col-sm-12 col-md-6">
                           <button type="button" class="btn  btns_input_white transform d-block w-100" id="1" onclick="window.location='/'">CANCEL</button>
                        </div>
                        <div class="col-sm-12 col-md-6">
                           <button type="submit" class="btn  btns_input_white transform d-block w-100" id="login-btn">SUBMIT</button>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="tab-pane fade" id="signup" role="tabpanel" aria-labelledby="profile-tab">
               <div class="login_icon">
                  <i class="fa fa-user-o" aria-hidden="true"></i>
               </div>
               <div class="login_details">
                  <h4>Create New Customer Account</h4>
                  <p class="mb-5">Please fill in the details below to registered a new client.</p>
                  <form action="register" method="post" id="signup_form">
                     <div class="form-group input_effects  mt-4 mb-2">
                        <input type="text" id="first_name" name="first_name" value="{{old('first_name')}}" class="form-control input_effect">
                        <label for="first_name">First Name</label>
                        <span class="focus-border"></span>
                     </div>
                     <div class="form-group input_effects  mt-4 mb-2">
                        <input type="text" class="form-control input_effect" id="last_name" name="last_name" value="{{ old('last_name') }}">
                        <label for="exampleInputEmail1">Last Name</label>
                        <span class="focus-border"></span>
                     </div>
                     <div class="form-group input_effects  mt-4 mb-2">
                        <input type="email" class="form-control input_effect" id="email" name="email" value="{{ old('email') }}">
                        <label for="email">Email Address</label>
                        <span class="focus-border"></span>
                     </div>
                     <div class="form-group input_effects mt-4 mb-2">
                        <input type="password" id="reg_password" class="form-control input_effect" name="password">
                        <label for="reg_password">Password</label>
                        <span class="focus-border"></span>
                     </div>
                     <div class="form-group input_effects mt-4 mb-2">
                        <input type="password" id="confirmpassword" class="form-control input_effect" name="confirmpassword">
                        <label for="confirmpassword">Confirm Password</label>
                        <span class="focus-border"></span>
                     </div>
                     <div class="form-group mt-4 mb-2">
                        <select class="form-control border-top-0 border-right-0 border-left-0 border-bottom rounded-0 pl-0" name="nationality" id="nationality">
                           <option value=""> Select Nationality </option>
													@if( count( $travelers['nationalities'] ) > 0 )
														@if( count( $travelers['nationalities']['featured'] ) > 0 )
															@foreach( $travelers['nationalities']['featured'] as $featured)
																<option value="{{ $featured['id'] }}" {{ isset($travel_pref['nationality']) && $travel_pref['nationality'] == $featured['name'] ? 'selected' : ''  }}>{{ $featured['name'] }}</option>
															@endforeach
														@endif
														<option value="" disabled> --------------------------- </option>
														@if( count( $travelers['nationalities']['not_featured'] ) > 0 )
															@foreach( $travelers['nationalities']['not_featured'] as $not_featured)
																<option value="{{ $not_featured['id'] }}" {{ isset($travel_pref['nationality']) && $travel_pref['nationality'] == $not_featured['name'] ? 'selected' : ''  }}>{{ $not_featured['name'] }}</option>
															@endforeach
														@endif
													@endif
                        </select>
                     </div>
                     <div class="form-group mt-4 mb-2">
                        <select class="form-control border-top-0 border-right-0 border-left-0 border-bottom rounded-0 pl-0" name="gender" id="gender">
                            <option value="">Gender</option>
	         									<option value="male">Male</option>
	         									<option value="feMale">Female</option>
                        </select>
                     </div>
                     <div class="form-group mt-4 mb-2">
                        <select class="form-control border-top-0 border-right-0 border-left-0 border-bottom rounded-0 pl-0" name="age_group" id="age_group">
                            <option value="">Age Group</option>
           									@if( count( $travelers['age_groups'] ) > 0 )
           										@foreach( $travelers['age_groups'] as $age_group)
           											@if( empty( $age_group['name'] ) )
           												@continue
           											@endif
           											<option value="{{ $age_group['id'] }}" {{ isset($travel_pref['age_group']) && $travel_pref['age_group'] == $age_group['name'] ? 'selected' : ''  }}>{{ $age_group['name'] }}</option>
           										@endforeach
           									@endif
                        </select>
                     </div>
                     <div class="form-group mt-4 mb-2">
                        <select class="form-control border-top-0 border-right-0 border-left-0 border-bottom rounded-0 pl-0" name="currency" id="currency">
                          <option value="" >Select Default Currency</option>
                          @php $currencies = session()->get('all_currencies'); @endphp
         									@foreach( $currencies as $currency )
         										<option value="{{ $currency['code'] }}" {{ (old('currency')) && old('currency') == $currency['code'] ? ' selected' : ''  }}> {{ '('. $currency['code'] .') '.$currency['name'] }} </option>
         									@endforeach;
                        </select>
                     </div>
                     @if (count($errors->register) > 0 || session()->has('register_error') )
                     	<div class="error-msg">
                     		<p> Something went wrong! </p>
                     		{!! session()->has('register_error') ? '<p>'. array_get( session()->get('register_error'), 'email.0'  ) .'</p>' : ''  !!}
                     	</div>
                     @endif
                     <div class="form-group mt-4 mb-4">
                        <span class="custom_check">By clicking ‘Create Account’, your client agrees to the Terms of Use and Privacy Policy. &nbsp; <input type="checkbox" name="policy" id="checkbox-04" value="1"><span class="check_indicator border">&nbsp;</span></span>
                     </div>
                     <div class="row">
                        <div class="col-sm-12 col-md-6">
                           <button type="button" class="btn  btns_input_white transform d-block w-100" data-dismiss="modal" value="Cancel" onclick="window.location='/'">CANCEL</button>
                        </div>
                        <div class="col-sm-12 col-md-6">
                           <button type="submit" class="btn  btns_input_white transform d-block w-100">SIGN UP</button>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>

        </div>
   
@endsection

@push('scripts')
	<script type="text/javascript">
        // JavaScript for label effects only
        $(window).load(function() {

            $(".input_effects input").focusout(function() {
                if ($(this).val() != "") {
                    $(this).addClass("has-content");
                } else {
                    $(this).removeClass("has-content");
                }
            });
            $(".input_effects input").each(function() {
                if ($(this).val() != "") {
                    $(this).addClass("has-content");
                } else {
                    $(this).removeClass("has-content");
                }
            });

        });

    </script>
@endpush