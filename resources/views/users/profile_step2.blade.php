@extends('layouts.common')
@section('content')
 @include('partials.banner')
 @include('partials.search')
   
   <div class="account-block">
         @include('users.partials.sidebar')
        
        <div class="account-right p-4 pl-5">
        @include('users.partials.complete_profile')

        @if($link != '')
        <div class="mt-4 row">
        <div class="col-xl-4 offset-xl-8 col-sm-6 offset-sm-6">
          <div class="row">
            <div class="col-sm-5 col-5">
              <a href="{{ $link }}" class="btn btns_input_dark btn-block">UPDATE</a>
            </div>
            <div class="col-sm-7 col-7">Update your personal travel preferences.</div>
          </div>
        </div>
        </div>
        @endif

        @if(session()->has('profile_step2_success'))
			
			<div class="alert alert-success text-center" role="alert">
				  {{ session()->get('profile_step2_success') }}
			</div>
        @endif
        @if(session()->has('profile_step2_error'))
			
			<div class="alert alert-danger" role="alert">
				  {{ session()->get('profile_step2_error') }}
			</div>
          
        @endif
          <hr class="mt-5">
          <h5 class="mt-4">Physical Address</h5>
                <form class="mt-3" method="post" action="{{url('profile_step2')}}" id="profile_step2_form">
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>Address Line One (1) *</label>
                    <input type="text" name="phy_address_1" value="{{ $user->customer->phy_address_1 }}" class="form-control phy_address_1" placeholder="Street Address, P.O Box, Company Name" />
                  </div>
                   @if ($errors->has('phy_address_1')) 
                     <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('phy_address_1')}}</label>
                  @endif

                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>Address Line Two (2)</label>
                    <input type="text" name="phy_address_2" value="{{ $user->customer->phy_address_2 }}" class="form-control phy_address_2" placeholder="Apartment, Suite, Unit, Building, Floor, etc." />
                  </div>
                </div> 
                <div class="form-group">
                  <div class="fildes_outer">
                      <label>Country *</label>
                      <div class="custom-select">
                        <select id="phy_country" name="phy_country" class="form-control phy_country">
                            <option value="">Select Country</option>
                            @foreach ($all_countries as $country)
                                <option value="{{ $country['id'] }}" {{ $user->customer->phy_country == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                            @endforeach                                       
                        </select>
                      </div>
                      @if ($errors->has('phy_country')) 
                         <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('phy_country')}}</label>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>State *</label>
                   <input type="text" name="phy_state" value="{{ $user->customer->phy_state }}" class="form-control phy_state" placeholder="State / Territory / Province / Region" />
                  </div>
                   @if ($errors->has('phy_state')) 
                     <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('phy_state')}}</label>
                  @endif
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>City *</label>
                     <input type="text" name="phy_city" value="{{ $user->customer->phy_city }}" class="form-control phy_city" placeholder="City" />
                  </div>
                  @if ($errors->has('phy_city')) 
                     <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('phy_city')}}</label>
                  @endif
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>ZIP *</label>             
                    <input type="text" name="phy_zip" value="{{ $user->customer->phy_zip }}" class="form-control phy_zip" placeholder="ZIP / Postal Code" />
                  </div>
                   @if ($errors->has('phy_zip')) 
                     <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('phy_zip')}}</label>
                  @endif
                </div>
                <h5 class="mt-5">Billing Address</h5>
                <div class="form-group black-checkbox mt-3">
                    <span class="custom_check radio-checkbox">Same as Physical Address &nbsp; <input type="checkbox" id="bill_checkbox" value="9" name="bill_checkbox"><span class="check_indicator">&nbsp;</span></span>
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>Address Line One (1) *</label>
                    <input type="text" name="bill_address_1" value="{{ $user->customer->bill_address_1 }}" class="form-control bill_address_1" placeholder="Street Address, P.O Box, Company Name" />

                  </div>
                   @if ($errors->has('bill_address_1')) 
                     <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('bill_address_1')}}</label>
                  @endif
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>Address Line Two (2)</label>
                    <input type="text" name="bill_address_2" value="{{ $user->customer->bill_address_2 }}" class="form-control bill_address_2" placeholder="Apartment, Suite, Unit, Building, Floor, etc." />
                  </div>
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                      <label>Country *</label>
                      <div class="custom-select">
                        <select id="bill_country" name="bill_country" class="form-control bill_country">
                            <option value="">Select Country</option>
                            @foreach ($all_countries as $country)
                                <option value="{{ $country['id'] }}" {{ $user->customer->bill_country == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                            @endforeach                                       
                        </select>
                      </div>
                       @if ($errors->has('bill_country')) 
                         <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('bill_country')}}</label>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>State *</label>
                   <input type="text" name="bill_state" value="{{ $user->customer->bill_state }}" class="form-control bill_state" placeholder="State / Territory / Province / Region" />
                  </div>
                  @if ($errors->has('bill_state')) 
                         <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('bill_state')}}</label>
                      @endif
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>City *</label>
                    <input type="text" name="bill_city" value="{{ $user->customer->bill_city }}" class="form-control bill_city" placeholder="City" />
                  </div>
                  @if ($errors->has('bill_city')) 
                         <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('bill_city')}}</label>
                      @endif
                </div>
                <div class="form-group">
                  <div class="fildes_outer">
                    <label>ZIP *</label>
                    <input type="text" name="bill_zip" value="{{ $user->customer->bill_zip }}" class="form-control bill_zip" placeholder="ZIP / Postal Code" />
                  </div>
                   @if ($errors->has('bill_zip')) 
                     <label for="bill_address_1" generated="true" class="text-danger mt-1" style="display: inline-block;">{{$errors->first('bill_zip')}}</label>
                  @endif
                </div>
                <button type="submit" class="btn btns_input_dark btn-block mt-5 pb-2 pt-2">UPDATE PROFILE</button>
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
        </div>
      </div>
<script type="text/javascript">
$("#bill_checkbox").click(function() {
    if ($(this).is(':checked')) 
    {
        $(".bill_address_1").val($(".phy_address_1").val());
        $(".bill_address_2").val($(".phy_address_2").val());
        $(".bill_country").val($(".phy_country").val());
        $(".bill_state").val($(".phy_state").val());
        $(".bill_city").val($(".phy_city").val());
        $(".bill_zip").val($(".phy_zip").val());
        $(".bill_country").val($(".phy_country").val());
    }
    else
    {
        $(".bill_address_1").val('');
        $(".bill_address_2").val('');
        $(".bill_country").val('');
        $(".bill_state").val('');
        $(".bill_city").val('');
        $(".bill_zip").val('');
        $(".bill_country").val('');
    }
});
</script>
@stop