<div id="loginmodal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer">
                <div class="modal-header pb-0">
                    <h4 class="text-center">@lang('home.sign_in_heading')</h4>
                </div>
                @if(session()->has('register_confirm'))
                <div class="alert alert-warning" role="alert">
                    {{ session()->get('register_confirm') }}
                </div>
                @elseif(session()->has('reset-success'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('reset-success') }}
                </div>
                @elseif(session()->has('reset-success-done'))
                <div class="alert alert-danger" role="alert">
                    {{ session()->get('reset-success-done') }}
                </div>
                @endif
                <p class="text-center">@lang('home.social_network')  </p>
                <div class="modalform pt-2">
                    <form class="signup_forms" id="login_form">
                    {{ csrf_field() }}
                    <div class="row social_btn_link">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <a href="{{ url('redirect/google') }}" class="social_gplus"><i class=" ic-google_plus_new"></i>Google</a>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                           <button type="button" class="social_facebook fb-login border-0" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" disabled><i class=" ic-facebook"></i>Facebook</button>
                        </div>
                    </div>
                    <div class="or_block"><span>@lang('home.sign_in_or_text')</span></div>
                    <div  class="signup_forms">
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>@lang('home.email_address')</label>
                                <input type="text"  name="username_login" id="username_login" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>@lang('home.password')</label>
                                <input type="password" name="password_login" id="password_login" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="custom_check">@lang('home.sign_in_remember_me') &nbsp; <input type="checkbox"><span class="check_indicator">&nbsp;</span></span>
                        </div>
                        <div class="form-group">
                            <input class="sign_btn" id="submitLoginForm" type="submit" value="@lang('home.sign_in_heading')">
                        </div>
                        <div class="bottom_text">
                            <div class="float-left">@lang('home.sign_in_member_text') <a  href="javascript:void();" id="sign_up_button">@lang('home.sign_up_heading')</a></div>
                            <div class="float-right"><a href="javascript:void()" id="forgot_password_button">@lang('home.sign_in_recovery_text')</a></div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---END Login Modal--->
