@extends('itinenary.booking-layout')

@section('booking-content')
<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
   
    <div class="container-fluid">
        @include('itinenary.partials.payment-filter-option')

        <div class="itinerary_page pt-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class=" m-1">
                            <div class="tripDetails mb-3">
                                <div class="card panel border-0 rounded-0 p-3">
                                    <div class="row">
                                        <div class="col-sm-7 col-xl-8">
                                            <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                            <div class="media">
                                                <i class="ic-calendar mr-2"></i>
                                                <div class="media-body pb-3 mb-0">
                                                    {{$startDate}} - {{$endDate}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="row align-items-end">
                                                <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="list-inline-item">
                                                            <i class=" ic-local_hotel"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-local_activity"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-directions_bus"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                                    <h2 class="font-weight-bold mb-0">{{$totalDays}}</h2>
                                                    Nights
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="save_itinerary  m-1">
                            <div class="itinerary_list p-3 pb-1">
                                <div class="returning_customers">
                                    <form class="" id="login_form2">
                                    {{ csrf_field() }}
                                    <div class="title pb-2 border-bottom">
                                        Returning Customers

                                    </div>
                                    <div class="row pt-3 pb-3">
                                        <div class="col-sm-6">
                                            Sign in with your social network
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <a href="#" class="text-secondary"><i class="fa fa-lock"></i>  Secure</a>s
                                        </div>
                                    </div>
                                    <div class="row pt-3 pb-3">
                                        <div class="col-sm-6">
                                            <a href="{{ url('redirect/google') }}" class="social_gplus"><i class=" ic-google_plus_new"></i>Google</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="social_facebook fb-login border-0" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" disabled><i class=" ic-facebook"></i>Facebook</button>
                                        </div>
                                    </div>

                                    <div class="login-or border-top mt-4">
                                        <span class="pl-2 pr-2 pt-1 pb-1">or</span>
                                    </div>


                                    <div class="row">
                                        <div class="col-12">
                                                <div class="form-group">
                                                    <p id="login-error-message2" class="text-danger"></p>
                                                    <div class="fildes_outer">
                                                        <label>Email Address</label>
                                                        <input type="text" name="username_login" id="username_login2" class="form-control" placeholder="kane@eroam.com">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="fildes_outer">
                                                        <label>Password</label>
                                                        <input type="password" name="password_login" id="password_login2" class="form-control" placeholder="....">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span class="custom_check">Keep me signed in. &nbsp; <input type="checkbox" id="checkbox-login2" value="4"><span class="check_indicator">&nbsp;</span></span>
                                                </div>
                                                <div class="form-group">
                                                    <input class="btn  btns_input_dark transform d-block w-100" id="submitLoginForm2" type="submit" value="CHECKOUT">
                                                </div>
                                                <div class="bottom_text">
                                                    <div class="float-right"><a href="" class="text-secondary" data-toggle="tooltip" title="Not Available in Pilot">Recover my password</a></div>
                                                </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12">
                        <div class="save_itinerary  m-1">
                            <div class="itinerary_list p-3 pb-1">
                                <div class="returning_customers">
                                    <div class="title pb-2 border-bottom">
                                        Guest Checkout
                                    </div>
                                    <p class="pt-3 pb-3">Proceed to checkout, and you can create an eRoam account at the end.
                                    </p>
                                </div>
                                <div class="p-2">
                                    <a href="{{ url('/review-itinerary/') }}" class="btn  btns_input_white  transform d-block w-100">CONTINUE AS GUEST</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush