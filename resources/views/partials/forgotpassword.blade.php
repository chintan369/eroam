<div id="forgotModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer">
                <div class="modal-header pb-0">
                    <h4 class="text-center">Recover Password</h4>
                </div>
                <p class="text-center">Please enter your email and we'll help you to reset your password.</p>
                 @if(session()->has('forgot_confirm'))
               <div class="alert alert-success" role="alert">
                    {{ session()->get('forgot_confirm') }}
                </div>
                @else
                <div class="alert alert-success forgot_message" style="display:none"></div>
                @endif
                <div class="modalform pt-2">
                    <form class="signup_forms" id="forgot_form">
                     @csrf
                    
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Email Address</label>
                                <input type="text"  name="user_email" id="user_email" class="form-control" placeholder="">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input type="button" data-dismiss="modal" class="sign_btn" value="Cancel" onclick="window.location='/'">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input class="sign_btn" id="submitForgotForm" type="submit" value="Continue">
                                </div>
                            </div>
                        </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---END Login Modal--->
