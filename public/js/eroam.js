/**
 * Eroam JS
 */

var listOfCities        = [],
	listOfTransports    = [],
	listOfApiHotels     = [],
	listOfApiTransports = [],
	listOfApiActivities = [];


var eroam = ( function( $ ) {

	var ewayKey = 'nnnhrXdfl4Av/Z9uYM63MQnf6eXwue+cFZIxFkhmbK7sir16ns6s3sDQ8NOsrnGm8+IHQkVxRIqT/TCVspyznMOEvaSq9ZyIPEyEndAaTNZSmTMuRm6RkqnsGtSXPXoLQKZCikDxyOvRglqyoLjTWSSxiluoCPiY5u0jPUzbomkWD/xHMLLQtfoiXKslVKr9vtO4mnmM0vxuprqE5dA+MS/Hq4FixXcRISKPeTRbL2X+cEsVarVhWc5tQ4uusW4VmneNBX9Q8rLfk9iGC8l4Hn9Tf0lD0mIGmKmQK0HujdbDXFz6YDSziUbBwxVNKQPtMB/qfmdiZzOCCsw7ZKrHYQ==';
	var apiUrl  = $( '#api-url' ).val();
	var siteUrl = $( '#site-url' ).val();
	var cmsUrl  = $( '#cms-url' ).val();

	function ajax( method, path, data, success, before, complete ) {
		$.ajax({
			method: method,
			url: siteUrl + '/'  + path,
			headers: {
			    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
			},
			data: data,
			beforeSend: before,
			complete: complete,
			success: success,
			error: function( xhr ) {
				console.log( xhr.statusText + xhr.responseText );
			}
		});
	}

	function ajaxDeferred(route, method, data, api_type, async, optional) {
	    var deferredObject = $.Deferred();
	    $.ajax({
	        url: siteUrl + '/'  + route,
	        method: method,
	        data: data,
	        async: (async) ? true : false,
			headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	        success: function(response){
	        	var result = apiResponseFilter( response, data, api_type, optional );
				 deferredObject.resolve( result );
	        },
	        error: function(xhr) { 
	            console.log(xhr.statusText + xhr.responseText);
	        },
	    });

	    return deferredObject.promise();

	}

	function api( method, path, data, success, before, complete ) {
		$.ajax({
			method: method,
			url: apiUrl + path,
			 headers: {
				'X-Authorization': '7b94b3635849f1da030358d5724c72c1f320ca5c',
			},
			crossDomain: true,
			data: data,
			beforeSend: before,
			complete: complete,
			success: success,
			error: function( xhr ) {
				console.log( xhr.statusText + xhr.responseText );
			},
		});
	}

	function apiWithError( method, path, data, success, before, complete, error ) {
		$.ajax({
			method: method,
			url: apiUrl + path,
			 headers: {
				'X-Authorization': '7b94b3635849f1da030358d5724c72c1f320ca5c',
			},
			crossDomain: true,
			data: data,
			beforeSend: before,
			complete: complete,
			success: success,
			error: error
		});
	}

	// created by miguel on 2017-01-11
	// function to make ajax call and return a deferred object's promise
	function apiDeferred(route, method, data, api_type, async, optional = null) {

	    var deferredObject = $.Deferred();
	    $.ajax({
	        url: apiUrl + route,
	        method: method,
	        data: data,
	        async: (async) ? true : false,
			headers: {
				'X-Authorization': '7b94b3635849f1da030358d5724c72c1f320ca5c',
			},
	        success: function(response){
	            var result = apiResponseFilter( response, data, api_type, optional );
	            deferredObject.resolve( result );
	        },
	        error: function(xhr) { 
	            console.log(xhr.statusText + xhr.responseText);
	        },
	    });

	    return deferredObject.promise();

	}


	// created by miguel on 2017-01-11
	// function to filter the response of apiDeferred()
	function apiResponseFilter(response, data, api_type, optional){
		
	    var result = false;

	    if(response){

	        switch(api_type)
	        {
	        	case 'rawResponse':
	        		result = response;
	        		break;

	            case 'hb':
	                var array = [];
	               
	                if(!response.errors){
	                    var hotels = response.data.hotels;

	                    if(hotels && hotels.total > 0){
	                        $.each(hotels.hotels,function(key,value) {

	                            var hotel_data         = {};
	                            hotel_data.code        = value.code;
	                            hotel_data.zoneCode    = value.zoneCode;
	                            hotel_data.name        = value.name;
	                            hotel_data.description = value.description;
	                            hotel_data.rooms       = value.rooms;
	                            hotel_data.currency    = value.currency;
	                            hotel_data.categoryName = value.categoryName;
	                            hotel_data.categoryCode = value.categoryCode;
	                            hotel_data.category    = parseInt(value.categoryName.toString().replace(/[^0-9.]/g, ''));
	                            hotel_data.lat         = value.latitude;
	                            hotel_data.lng         = value.longitude;
	                            hotel_data.iata        = data.code;
	                            hotel_data.type        = api_type;

	                            hotel_data.address			= value.address;
	                            if( value.images !== undefined && value.images.length > 0 && value.images !== undefined ){
	                            	var first_image = value.images[0];
	                            	 hotel_data.image       = 'http://photos.hotelbeds.com/giata/' + first_image.path;
	                            }else{
	                            	 hotel_data.image       = 'http://photos.hotelbeds.com/giata/' + value;
	                            }

	                            hotel_data.other       = {};
	                            if( value.description !== undefined ){
	                            	 array.push(hotel_data);
	                            }
	                           
	                        }); 
	                    }
	                    result = {array : (array.length > 0) ? array : false, temp : false};
	                }else{
	                    result = {array : false, temp : false};
	                    console.log(response.errors);
	                }
	                break;

	            case 'aot':
	                switch(response.index){

	                    case 'Supplier':
	                        var array = [];
	                        if(response.data){
	                            var hotels = response.data[response.index];
	                            if(hotels){
	                                if($.isArray(hotels)){
	                                    for (var i = 0; i < hotels.length; i++) {
	                                        var hotel_data = aot.clean('sir_response', hotels[i], {iatacode : data.iatacode, api_type : api_type});
	                                        array.push(hotel_data);
	                                    }
	                                }else{
	                                    var hotel_data = aot.clean('sir_response', hotels, {iatacode : data.iatacode, api_type : api_type});
	                                    array.push(hotel_data);
	                                }
	                            }
	                        }
	                        result = array;
	                        break;

	                    case 'OptRates':
	                        if(response.data){
	                            if(data.type == 'orr_multi'){
	                                var rates = response.data[response.index];
	                                if(rates){
	                                    if($.isArray(rates)){
	                                        var rates_temp = []; 
	                                        var max = 0;
	                                        for (var i = 0; i < rates.length; i++) {
	                                            var room_rates = aot.clean('orr_response', rates[i]);
	                                            rates_temp.push(room_rates);
	                                        }

	                                        result = rates_temp;
	                                         console.log( "result : ", result  );
	                                    }
	                                }
	                            }else{
	                                var rates = response.data[response.index];
	                                if(rates){
	                                    if($.isArray(rates)){
	                                        var rates_temp = []; 
	                                        var max = 0;
	                                        for (var i = 0; i < rates.length; i++) {
	                                            var room_rates = aot.clean('orr_response', rates[i]);
	                                            rates_temp.push(room_rates);
	                                        }
	                                        result = aot.clean('orr_filter', rates_temp, optional);
	                                    }
	                                }
	                            }
	                        }
	                        break;

	                    case 'OptAvail':
	                        if(response.data){
	                            if(data.type == 'oar_multi'){
	                                var avail = response.data[response.index];
	                                if(avail){
	                                    if($.isArray(avail)){
	                                        result = {array : avail, param : {city_id : data.city_id} };
	                                    }
	                                }
	                            }
	                        }
	                        break;

	                    case 'q_supplier':
	                        var array = [];
	                        var temp = false;
	                        if(response.data){
	                            var hotels = response.data.suppliers;
	                            if($.isArray(hotels)){
	                                for (var i = 0; i < hotels.length; i++) {
	                                    if(hotels[i].rooms.length && hotels[i].SupplierCode.indexOf('TEST') == -1){
	                                        var hotel_data = aot.clean('q_supp_response', hotels[i], {room_type : data.room_type, api_type : api_type});
	                                        array.push(hotel_data);
	                                    }
	                                }
	                            }
	                        }
	                        temp = aot.clean('q_supp_rooms_merge', array);
	                        result = {array : (array.length > 0) ? array : false, temp : temp};
	                        break;

	                    case 'q_tour':
	                        if(response.data){

	                            var tours = response.data.tours;

	                            if($.isArray(tours)){

	                                result = tours;

	                                var temp = tours.map(function(val){
	                                    return val.Opt
	                                });

	                                result = {array : (tours.length > 0) ? tours : false, temp : temp, param : data, cache : false};
	                            }
	                        }
	                        break;
	                }
	                break;

	            case 'expedia':
	                result = response;
	                
	            case 'grouptours':
	                result = response;
	            break;

	            case 'query_activities':
	                if(response.data){
	                    result = response;
	                }
	                break;

	            //added by aljun hotels -- new query PHP
	            case 'hotels':
	                if(!response.success){
	                    console.log(response.error);
	                }     
	                result = response.data;           
	                break;

	            case 'transport':
	                if(response.data){
	                	
	                    result = response;
	                }               
	                break;

	            // added by miguel
	            case 'cities':
	                if(response){
	                    result = response;
	                }               
	                break;
				case 'viator_activity':
					if( response.success == false )
	                {
	                    console.log( api_type + ' call failed. ', response.error);
	                }
	  
                	result = response.data;
					break;
	            default:
	                if( response.success == false )
	                {
	                    console.log( api_type + ' call failed. ', response.error);
	                }
	                
                	result = response.data;
	                break;

	        }
	    }
	    return result;
	}

	// created by miguel on 2017-01-11
	// function to handle a single deferred object's promise
	function apiPromiseHandler(fn, callback){
		$.when(fn).then(function(response) {
			callback(response);
		});
	}

	// created by miguel on 2017-01-11
	// function to handle an array of deferred object's promise
	function apiArrayOfPromisesHandler(fn, callback){
		$.when.apply($, fn).done(function () {
			var data = [];			
		    for (var i = 0; i < arguments.length; i++) {					  
		    	data.push(arguments[i]);
		    }
		    callback(data);
		});
	}

	function updateProposedItinerary( markers ) {
		var city_ids = [];
		for ( i = 0; i < markers.length; i++ ) {
			city_ids.push( markers[i].cityId );
		}
	}

	// created by miguel on 2017-01-12
	// function to convert the currency based on the default/selected global currency variable;
	function convertCurrency( price, currency )
	{	
		var priceConvertedToAUD = parseFloat(price) / parseFloat(listOfCurrencies[currency]);
		var priceConvertedToSelectedPrice = 0.00;
	
		$.each(listOfCurrencies, function(key, value){
			if( globalCurrency == key )
			{	
				priceConvertedToSelectedPrice = priceConvertedToAUD * parseFloat(value);
			}
		});

		return parseFloat(priceConvertedToSelectedPrice ).toFixed(2);

	}	

	// created by miguel on 2017-02-02
	// function to handle all confirm ;
	function confirm( 
		title = null, 
		content = null, 
		confirmFunction, 
		cancelFunction, 
		modalSize = 'modal-sm',
		confirmBtnTxt = 'OK',
		cancelBtnTxt = 'Cancel'
		)
	{	

		$('.eroam-confirm-modal-size').addClass( modalSize );
		$('.confirm-ok-button').html( confirmBtnTxt );
		$('.confirm-cancel-button').html( cancelBtnTxt );

		if(title != null) // check if title is set
		{
			$('.eroam-confirm-title').html( title );
			$('.eroam-confirm-content-close').hide();
			$('.eroam-confirm-header').show();
		}
		else
		{
			$('.eroam-confirm-header').hide();
			$('.eroam-confirm-content-close').show();
		}

		if( content != null )
		{
			$('.eroam-confirm-content').html( content ); 
		}
		else
		{
			$('.eroam-confirm-content').html( "Do you want to continue?" ); 
		}


		$('#eroam-confirm').modal('show'); // show modal

		// FUNCTION TO EXECUTE WHEN "OK" IS CLICKED
		/*
		| Update by junfel
		| Change to one from on
		*/
		$('.confirm-ok-button').one('click',function(e){
			e.preventDefault();
			if( confirmFunction ){
				confirmFunction();	
			}
			$('#eroam-confirm').modal('hide');
			return true;

		});

		// FUNCTION TO EXECUTE WHEN "CANCEL" IS CLICKED
		$('.eroam-confirm-cancel-btn').one('click',function(e){
			e.preventDefault();
			if( cancelFunction ){
				cancelFunction();	
			}
			$('#eroam-confirm').modal('hide');
		});

	}	


	function getEwayKey(){
		return ewayKey;
	}


	function formatHotel( hotel, city )
	{
		var h = hotel;
		var result = h;
		if( h != null )
		{
			// if the property "provider" does not exist then the provider is eroam
			if( _.has( h, 'provider' ) === false ){ 
				h.provider = 'eroam';
			}

			// variables to use
			var price, hotel_price_id, room_name, room_type_id, checkin, checkout;
			var name = h.name;

			switch( h.provider )
			{
				case 'eroam':
					if( typeof h.price === 'object' ) // if hotel.price is an object
					{
						_.forOwn(h.price, function( v, k ){
							price          = eroam.convertCurrency( parseFloat(v.price), v.season.currency.code ); 
							hotel_price_id = v.id;
							room_type_id   = v.room_type.id;
							room_name      = v.room_type.name;
							return;
						});
						checkin  = city.date_from;
						checkout = city.date_to;
					}
					else if( Array.isArray( h.price ) ) // if hotel.price is an array
					{
						h.price.forEach(function( v, k ){
							price          = eroam.convertCurrency( parseFloat(v.price), v.season.currency.code ); 
							hotel_price_id = v.id;
							room_type_id   = v.room_type.id;
							room_name      = v.room_type.name;
							return;
						});
						checkin  = city.date_from;
						checkout = city.date_to;
					}
					else // if hotel.price is a string
					{
						price          = eroam.convertCurrency( parseFloat(h.price), h.currency );
						hotel_price_id = h.price_id;
						room_type_id   = h.room_type_id;
						room_name      = h.room_name;
						checkin        = h.checkin;
						checkout       = h.checkout;
					}

					result = {
						id : h.id,
						name : h.name,
						hotel_id : h.id,
						price : price,
						currency : globalCurrency,
						hotel_price_id : hotel_price_id,
						room_type_id : room_type_id,
						room_name : room_name,
						checkin : checkin,
						checkout : checkout
					};
					break;

				case 'hb':
					result = h;
					result.price = eroam.convertCurrency( h.price, h.currency );
					result.currency = globalCurrency;
					break;

				case 'aot':
					result = h;
					result.price = eroam.convertCurrency( h.price, h.currency );
					result.currency = globalCurrency;
					break;

				case 'ae':
					result = h;
					result.price = eroam.convertCurrency( h.price, h.currency );
					result.currency = globalCurrency;
					break;

			}
			result.provider = h.provider;
		}
		else
		{
			result = { provider : 'own_arrangement' };
		}

		return result;
	}



	function formatActivities( activities )
	{
		var a = activities;
		var result = [];
		if( activities != null )
		{
			activities.forEach(function(a, aKey){

				var id, name, price, currency, date_selected;
				var activity = {};

				// if the property "provider" does not exist then the provider is eroam
				if( _.has( a, 'provider' ) === false ){ 
					a.provider = 'eroam';
				}	
				
				switch( a.provider )
				{
					case 'eroam':
						var price, activity_price_id, date_selected;
						if( typeof a.price === 'object' ) // if activity.price is an object
						{
							_.forOwn(a.price,  function( v, k ){
								price = eroam.convertCurrency( parseFloat(v.price).toFixed(2), v.currency.code ); 
								activity_price_id = v.id;
								return;
							});
						}
						else if( Array.isArray( a.price ) ) // if activity.price is an array
						{
							a.price.forEach(function( v, k ){
								price = eroam.convertCurrency( parseFloat(v.price).toFixed(2), v.currency.code ); 
								activity_price_id = v.id;
								return;
							});
						}

						activity = {
							id : a.id,
							activity_id : a.id,
							name : a.name,
							price : price,
							duration : a.duration,
							currency : globalCurrency,
							activity_price_id : activity_price_id,
							date_selected : a.date_selected
						};
						break;

					case 'aot':
						activity = a;
						break;

					case 'viator':
						activity = a;
						activity.price = eroam.convertCurrency( a.price[0].price, a.currency );
						activity.currency = globalCurrency;
						break;
				}
				activity.provider = a.provider;
				result.push(activity);
			});
		}	
		else
		{
			result.push({ provider : 'own_arrangement' });
		}

		return result;
	}



	function formatTransport( transport, city, toCity )
	{
		var t = transport;
		var result = t;
		var departTimeZone = moment().tz( city.timezone.name ).format('z Z');
		var arriveTimeZone = moment().tz( toCity.timezone.name ).format('z Z');
		if( t != null )
		{
			// if the property "provider" does not exist then the provider is eroam
			if( _.has( t, 'provider' ) === false ){ 
				t.provider = 'eroam';
			}

			switch( t.provider )
			{
				case 'eroam':
					var transportType = '';
					if( isNotUndefined(t.transport_type) ){
						transportType =  t.transport_type.name;
					}else if( isNotUndefined(t.transporttype) ){
						transportType = t.transporttype.name;
					}else{
						transportType = t.transport_type_name;
					}
					var operator       = ( typeof t.operator === 'object' ) ? t.operator.name : t.transport_operator ;
					// var departure      = transport.departure_location+' '+ moment(transport.city.date_to).format('dddd, MMM D') +' '+formatEtd(transport.etd) +' ('+ departTimeZone +')';
					// var arrival        = transport.arrival_location+' '+ moment(searchSession.itinerary[index + 1].city.date_from).format('dddd, MMM D') +' '+arrival_am_pm(transport.eta) +' ('+ arriveTimeZone +')';
					result             = t;
					result.name        = transportType + ' (' + operator + ')';
					result.price       = eroam.convertCurrency( t.price[0].price, t.currency.code );
					result.currency    = globalCurrency;
					result.city        = city;
					result.departure   = t.departure_text;
					result.arrival     = t.arrival_text;
					// result = {
					// 	id : t.id,
					// 	name : t.transporttype.name+' with '+t.operator.name,
					// 	price : eroam.convertCurrency( t.price[0].price, t.currency.code ),
					// 	currency : globalCurrency
					// };
					break;

				case 'mystifly':
					var eta = moment( t.eta ).format('MMM Do YYYY, h:mm A');
					var etd = moment( t.etd ).format('MMM Do YYYY, h:mm A');
					result = {
						name : 'Flight with '+t.operating_airline+', Flight # '+t.flight_number+' ('+t.cabin_class+')',
						fare_source_code : t.fare_source_code,
						id : t.id,
						fare_type : t.fare_type,
						arrival : t.arrival_data+' - '+eta+' ('+departTimeZone+')',
						departure : t.departure_data+' - '+etd+' ('+arriveTimeZone+')',
						num_of_flight_stops : 1,
						price : eroam.convertCurrency( t.price[0].price, t.currency ),
						currency : globalCurrency,
						validating_airline : t.operating_airline,
						required_fields_to_book : t.required_fields_to_book,
						JSON_flight_data : t,
						city: city,
					};
					break;
			}
			result.provider = t.provider;
		}
		else
		{
			result = { provider : 'own_arrangement' };
		}

		return result;
	}


	return {
		cmsUrl: cmsUrl,
		apiUrl: apiUrl,
		siteUrl: siteUrl,
		ajax: ajax,
		ajaxDeferred: ajaxDeferred,
		api: api,
		apiWithError: apiWithError,
		convertCurrency: convertCurrency,
		confirm: confirm,
		updateProposedItinerary: updateProposedItinerary,
		apiDeferred: apiDeferred,
		apiResponseFilter: apiResponseFilter,
		apiPromiseHandler: apiPromiseHandler,
		apiArrayOfPromisesHandler: apiArrayOfPromisesHandler,
		getEwayKey : getEwayKey,
		formatHotel : formatHotel,
		formatActivities : formatActivities,
		formatTransport : formatTransport
	};

})( jQuery );


function isUndefined(data){
    return data === undefined;
}

function isNotUndefined(data){
    return data !== undefined;
}

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function convertToSnakeCase( obj )
{
	var newObj = {};
	for (var key in obj)
	{
		if ( obj.hasOwnProperty( key ) )
		{
			var newKey = _.snakeCase( key );
			newObj[newKey] = obj[key];
		}
	}
	return newObj;
}

function splitString(stringToSplit, separator) {
	var arrayOfStrings = [];
	if( isNotUndefined( stringToSplit ) )
	{
		if( stringToSplit != null && stringToSplit != "" )
		{
			arrayOfStrings = stringToSplit.split( separator );
		}
	}
	return arrayOfStrings;
}

function validURL(str) {
	  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
	  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
	  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
	  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
	  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
	  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
	  return pattern.test(str);
}

function is_ae_city( $city_id )
{
	var result = false;
	var city = list_of_cities[ $city_id ];
	if( !( typeof city == "undefined" ) )
	{
		return city.ae_city_data;
	}
	return result;
}

function triggerChat()
{
	window.open("http://messenger.providesupport.com/messenger/10u9zj2w12fal0hhwl1jxaoifj.html", 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=700, height=400, top=100, left=100');
}


$( document ).ready( function() {
	$( '#currency-picker' ).click( function() {
		$( this ).find( '.currency-list' ).toggle();
	});
	$( '.currency-list u' ).click( function() {
		var value = $( this ).data( 'value' ),
			icon = $( this ).find( '.flag-icon' ).attr( 'class' ),
			text = $( this ).find( 'span' ).text();
		$( '#currency-picker .currency-value' ).html(
			'<i class="' + icon + '" ></i> <span>' + text + ' <i class="fa fa-caret-down"></i></span>'
		);
	});

	// Booking Summary Links
	$( 'body' ).on( 'click', '.itinerary-leg-link', function() {
		if (!$(this).attr('data-loading')) {
			window.location.href = $( this ).data( 'link' );
		}
	} );
	
});


function markUpPrice(price,percentage){

	// COMMENTED OUT BY MIGUEL BECAUSE THIS BLOCK OF CODE IS CAUSING TOTAL ISSUES
	 // var markUpPrice = price +(price*(percentage/100));
	 // return markUpPrice; 
	 return price;
}
