$(window).load(function(){
    $('.loader1').hide(); 
});
$(document).ready(function () {
      //console.log(season);
    //$('#checkbox-01').prop('indeterminate', true);

    if(season != 'empty')
    {
        displayFlightPrice();     
    }
    else
    {   
        $('#with_flights').hide();
        $('#without_flights').hide();
        $('.withFlights').hide();
        $('.withOutFlights').show();
        $('.innerWithFlights').hide();
        $('.innerWithFlightsText').hide();
        $('.innerWithOutFlights').show();
        $('.innerWithOutFlightsText').show();
        $('.return_flights_from').hide();          
    }

    var siteUrl = $( '#site-url' ).val();
    $("body").on("click", "#enquiry-modal", function () {
        $("#signupmodal").modal("show");
        $(".blue").addClass("after_modal_appended");
        //appending modal background inside the blue div
        $('.modal-backdrop').appendTo('.blue');
    });

    $("#destination").datepicker({
        format: "MM yyyy",
        autoclose: true,
        viewMode: "months", 
        minViewMode: "months"
    });

    $("#travel_date").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $("#start_date5").datepicker({
        format: "MM yyyy",
        autoclose: true,
        viewMode: "months", 
        minViewMode: "months",
        startDate: '+1d'
    });
    
    $("#myForm").validate({
        rules: {
            email: {
                required: true,
                email:true
            },
            first_name: {
                required: true
            },
            family_name: {
                required: true
            },
            phone: {
                number: true
            },
            postcode: {
                required: true
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            enquiry();
        }
    });

    function enquiry() {

        var data = $('#myForm').serializeArray();  
        $.ajax({
            url: siteUrl +'/enquiry-form',
            method: 'POST',
            data: data,
            beforeSend: function () {
                $('.btn_enquiry').attr('disabled',true);
                $('.notification').css({color: 'rgb(0, 0, 0)'}).html('Sending request...').show();
            },
            success: function (response) {
                if (response.trim() == 1) {
                    $("#success-box").show().html('<p class="success-box">Enquiry sent successfully.</p>');
                    $('.btn_enquiry').attr('disabled',false);
                    $('.notification').hide();                    
                }else{
                    $('#success-box').css({color: 'red'}).html('<p class="success-box">Enquiry not sent please try again.</p>').show();
                    $('.btn_enquiry').attr('disabled',false);
                    $('.notification').hide();
                }
            },
            error: function () {
            }
        });
    }

    function flightPriceCls(){
        $('.flightPriceCls').each(function(){
            if($('#with_flights').hasClass('active'))
            {
                $('.withFlights').show();
                $('.withOutFlights').hide();
                $('.innerWithFlights').show();
                $('.innerWithFlightsText').show();
                $('.innerWithOutFlights').hide();
                $('.innerWithOutFlightsText').hide();
                $('.return_flights_from').show();
            }
            else
            {
                $('.withFlights').hide();
                $('.withOutFlights').show();
                $('.innerWithFlights').hide();
                $('.innerWithFlightsText').hide();
                $('.innerWithOutFlights').show();
                $('.innerWithOutFlightsText').show();
                $('.return_flights_from').hide();
            }
        });
    }

    $('.update_date_search').click(function(){
        var select_date = $('#start_date5').val().replace(' ','-');

        if(!select_date){
            $('#red-text').show();
            return true; 
        }
        else
        {
            $('#red-text').hide(); 
        }
        $('.panel-default').hide();
        $('#no_records').show();

        console.log(season);

        if(season != 'empty'){

            if($('#with_flights').hasClass('active'))
            {
                $('.'+select_date+'').each(function(index, element)
                {
                    if($(this).hasClass('flightYesCls'))
                    {
                        $(this).closest('.flightYesCls').show();
                        $('#no_records').hide();
                    }
                });
            }
            else
            {
                $('.'+select_date+'').each(function(index, element){
                    $('.'+select_date+'').show();
                    $('#no_records').hide();
                });
            }
            flightPriceCls();
        
        }
        else
        {
        
            $('.'+select_date+'').each(function(index, element){
                $('.'+select_date+'').show();
                $('#no_records').hide();
            });

            flightPriceCls();
            
        }
          
    });
    
    function displayFlightPrice(){
        var global_flag = 0; 

        if($('.panel-default').length < 1){
            $('#no_records').show();
        }else{
            $('#no_records').hide();
        }

        if($('.flightYesCls').length > 1){
            $('#with_flights').addClass('active');
            $('#without_flights').removeClass('active');
            $('.withFlights').show();
            $('.withOutFlights').hide();
            $('.innerWithFlights').show();
            $('.innerWithFlightsText').show();            
            $('.innerWithOutFlights').hide();
            $(".innerWithOutFlightsText").hide();
            $('.return_flights_from').show();
        }else{
            $('#with_flights').removeClass('active');
            $('#without_flights').addClass('active');
            $('.withFlights').hide();
            $('.withOutFlights').show();
            $('.innerWithFlights').hide();
            $('.innerWithFlightsText').hide(); 
            $('.innerWithOutFlights').show();
            $(".innerWithOutFlightsText").show();
            $('.return_flights_from').hide();
        }

        $('.panel-default').hide();
        var n_flag = 0;
        $('.panel-default').each(function(i){
            if($(this).hasClass('flightYesCls')){
                n_flag = 1;
                $(this).show();
                $('#no_records').hide();
                $('#with_flights').addClass('active');
                $('#without_flights').removeClass('active'); 
            }
        });
        if(n_flag == 0){
            $('.panel-default').each(function(){
                if(!$(this).hasClass('flightYesCls')){
                    $(this).show();
                    $('#no_records').hide();
                }
            });
            $('#with_flights').removeClass('active');
            $('#without_flights').addClass('active');
        }
        flightPriceCls();
    }

    $('#with_flights').click(function(){
        $('#no_records').show();
        $('.return_flights_from').show();
        $(this).addClass('active');
        $('#without_flights').removeClass('active');
        var select_date = $('#start_date5').val();
        if(select_date)
        {
            select_date = $('#start_date5').val().replace(' ','-');
            $('.panel-default').hide();
            $('.'+select_date+'').each(function(){
                if($(this).hasClass('flightYesCls')){
                    $(this).closest('.flightYesCls').show();
                    $('#no_records').hide();
                    flightPriceCls(); 
                }
            });
        }else{
            $('.panel-default').hide();
            $('.panel-default').each(function(){
               if($(this).hasClass('flightYesCls')){
                  $(this).show();
                  $('#no_records').hide();
                  flightPriceCls(); 
               }
            });
        }
    });
    $('#without_flights').click(function(){
        $('.return_flights_from').hide();
        $(this).addClass('active');
        $('#with_flights').removeClass('active');
        var select_date = $('#start_date5').val();
        if(select_date){
            select_date = $('#start_date5').val().replace(' ','-');
            $('.panel-default').hide();
            $('#no_records').show();
            $('.'+select_date+'').each(function(){
                $('.'+select_date+'').show();
                $('#no_records').hide();
            });
            flightPriceCls();
        }else{
            $('.panel-default').hide();
            $('#no_records').show();
            $('.panel-default').each(function(){
                $(this).show();
                $('#no_records').hide();
            });
            flightPriceCls(); 
        } 
    });
});