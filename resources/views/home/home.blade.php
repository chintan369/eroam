@extends('layouts.common')
@section('content')
       <div class="blue modal_outer_man">
                @include('partials.banner')
                <!---Banner END-->

                @include('partials.search')
                <!---Select Departure City Modal-->
                
                <!---END Select Departure City Modal-->
                
                <!---Signup Modal-->
                <div id="signupmodal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg middbox">
                        <div class="modal-content modalbox">
                            <div class="modal_outer">
                                <div class="modal-header pb-0">
                                    <h4 class="text-center">Sign Up</h4>
                                   
                                </div>
                                <p class="text-center">With your social network </p>
                                <div class="modalform pt-2">
                                    <div class="row social_btn_link">
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                            <a href="" class="social_gplus"><i class=" ic-google_plus_new"></i>Google</a>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                            <a href="" class="social_facebook"><i class=" ic-facebook"></i>Facebook</a>
                                        </div>
                                    </div>
                                    <div class="or_block"><span>Or</span></div>
                                    <form action="" class="signup_forms">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>Email Address</label>
                                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="kane@eroam.com" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>Password</label>
                                                <input type="password" class="form-control" aria-describedby="emailHelp" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <span class="custom_check">Show Password &nbsp; <input type="checkbox"><span class="check_indicator">&nbsp;</span></span>
                                        </div>
                                        <div class="form-group">
                                            <input class="sign_btn" type="submit" value="Sign Up">
                                        </div>
                                        <div class="bottom_text">
                                            <div class="float-left"><a href="">Already registered? Sign in</a></div>
                                            <div class="float-right">by signing up you accept our <a href="">Terms of service</a></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!---END Signup Modal-->

            </div>
            <section class="tourlist">

                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2><span> Top tours </span></h2>
                        </div>
                    </div>
                    <div id="tours-loader">
                        <p class="text-center"><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</p>
                    </div>

                    <div class="row" id="tourList">
                    
                    </div>
                </div>
            </section>
        
        @include('home.partials.custom-js')
@endsection
