@extends( 'layouts.search' )

@section( 'custom-css' )
@push('style')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ url('css/search.css') }}">
@endpush
@stop

@section('content')
	<div id="map-loader">
		<div class="loader-block">
	      <img src="{{ url( 'images/launch.png' ) }}" alt="" />
	      <h3 id="itenary_message_id"></h3>
	      <p class="loader-msg"><i class="fa fa-circle-o-notch fa-spin"></i> <span>Your itinerary will be displayed shortly…</span></p>
    	</div>
	</div>

	

	<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
	<input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
	<div id="edit-map-overlay"></div>
	<a name="helpBoxDiv" style="position:absolute;top:0px;left:0px;"></a>
	@if ( session()->get( 'map_data' )['type'] == 'auto' )
		<a href="#" id="edit-map-btn" style="display:none;"><i class="fa fa-sliders"></i> Edit Map{{ ( $alternative_routes ) ? ' & Alternative Routes' : '' }}</a>
		<div id="edit-map-container" class="helpBox">
			<a href="#" class="close-edit-map-btn"><i class="fa fa-times flaticon-multiply"></i></a>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="blue-txt"><i class="fa fa-map"></i> Current Route</h4>
					<div id="current-route-container">
						@foreach ( $current_route as $city )
							<span>{{ is_object($city) ? $city->name : $city['name'] }}</span>
							@if ( last( $current_route ) != $city )
								<i class="fa fa-long-arrow-right"></i>
							@endif
						@endforeach
					</div>
					@if ( $alternative_routes )
						<h4 class="blue-txt margin-top"><i class="flaticon flaticon-map"></i> Alternative {{ sing( count( $alternative_routes ), 'Routes', false ) }}</h4>
						@foreach ( $alternative_routes as $key => $value )
							<div class="route-container" data-key="{{ $key }}">
								<p>{{ $value['duration'] }} {{ $value['price_per_person'] }} (Per Person)</p>
								@foreach ( $value['cities'] as $city )
									<span>{{ $city['name'] }}</span>
									@if ( last( $value['cities'] ) != $city )
										<i class="fa fa-long-arrow-right"></i>
									@endif
								@endforeach
							</div>
						@endforeach
					@endif
					<h4 class="blue-txt margin-top"><i class="fa fa-gear"></i> Custom Route</h4>
					<p>If you want to set your own route, you can do this by switching to manual mode.</p>
					<p><small>Note: Once you have switched to manual mode, you can't switch back to auto mode.</small></p>
					<a href="javascript://" class="switch-manual-btn btn btn-primary"><i class="fa fa-exchange"></i> Switch to Manual Mode</a>
				</div>
			</div>
		</div>
	@else
		<a href="#" id="edit-map-btn" style="display:none;"><i class="fa fa-question-circle"></i> Help</a>
		<span id="auto-sort" class="text-center"><i style="cursor: pointer;" class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="If enabled, selected locations will be sorted based on the shortest route when adding a new location."></i> Auto Sort <a href="#"><i class="{{ (!empty(session()->get( 'map_data' )['auto_sort']) && session()->get( 'map_data' )['auto_sort'] == 'on') ? 'fa fa-toggle-on fa-2x' : 'fa fa-toggle-off fa-2x' }}"></i></a></span>
		<div id="edit-map-container" class="helpBox" >
			<a href="#" class="close-edit-map-btn"><i class="fa fa-times flaticon-multiply"></i></a>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="blue-txt"><i class="fa fa-map-marker"></i> Adding destinations</h4>
					<p>To add a destination to your itinerary, simple click on the blue dots found on the map.</p>
					<img src="{{ url( 'images/map-dot.png' ) }}">
					<h4 class="blue-txt margin-top"><i class="fa fa-hotel"></i> Removing a destination / Adding nights</h4>
					<p>To remove a destination, click on the blue marker of the destination you want to remove and click "REMOVE DESTINATION" button. Click on the "+" and "-" buttons to adjust the number of nights you want for that destination.</p>
					<img src="{{ url( 'images/map-nights.png' ) }}">
					<h4 class="blue-txt"><i class="flaticon flaticon-map"></i> Round Trips</h4>
					<p>If you want to include a round-trip to your itinerary, simple click on the blue marker of the starting location and click the "Round-trip" button.</p>
					<img src="{{ url( 'images/map-roundtrip.png' ) }}">
				</div>
			</div>
		</div>
	@endif
	<input type="hidden" value="{{ json_encode( session()->get( 'search_input' ) ) }}" id="search-input">

@stop

@section( 'custom-js' )
<script type="text/javascript" src="{{ url('js/eroam-map.js') }}"></script>

<script>

$( window ).load(eMap.init);

$(document).ready( function() {
	var searchInput = JSON.parse( $( '#search-input' ).val() ),
		countryId = searchInput.country,
		toCountryId = searchInput.to_country,
		tomorrow = moment().add( 1, 'days' ).format( 'YYYY/MM/DD' ),
		nextWeek = moment().add( 1, 'weeks' ).format( 'YYYY/MM/DD' );

	$('#date-of-travel').datetimepicker( {
		timepicker: false,
		format: 'Y-m-d',
		minDate: tomorrow,
		startDate: nextWeek
	} );

	eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: countryId }, function( response ) {
		var city = $( '#starting-point' );
		city.html( '<option value="">Starting Point</option>' );
		$.each( response, function( k, v ) {
			var selected = v.id == searchInput.city ? 'selected' : '';
			city.append( '<option value="' + v.id + '" '+ selected +'>' + v.name + '</option>' );
		} );
	} );
	$( '#country option[value="' + countryId + '"]' ).attr( 'selected', true );

	// auto
	if ( searchInput.option == 'auto' ) {
		eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: toCountryId }, function( response ) {
			var city = $( '#to-city' );
			city.html( '<option value="">Destination</option>' );
			$.each( response, function( k, v ) {
				var selected = v.id == searchInput.to_city ? 'selected' : '';
				city.append( '<option value="' + v.id + '" '+ selected +'>' + v.name + '</option>' );
			} );
		} );
		$( '#to-country option[value="' + countryId + '"]' ).attr( 'selected', true );
	}
	// Map Search Header
	$( '.search-option-btn' ).click( function( event ) {
		event.preventDefault();
		$( this ).next( '.search-option' ).prop( 'checked', true );
		$( '.search-option-btn' ).removeClass( 'active' );
		$( this ).addClass( 'active' );
		if ( $( this ).data( 'type' ) == 'manual' ) {
			$( '#auto-container' ).addClass( 'hide' );
		} else {
			$( '#auto-container' ).removeClass( 'hide' );
		}
	} );

	$( '.select-country' ).change( function() {
		var city = $( this ).parent().next().find( '.select-city' );
		if ( $( this ).val().trim() != '' ) {
			eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: $( this ).val() }, function( response ) {
				city.html( '<option value="">Starting Point</option>' );
				$.each( response, function( k, v ) {
					city.append( '<option value="' + v.id + '">' + v.name + '</option>' );
				} );
			} );
		}
	} );

	$( '.update-search-btn' ).click( function( event ) {
		event.preventDefault();
		$( '#submit-btn' ).click();
	} );

	$( '#edit-map-btn' ).click( function( event ) {
		event.preventDefault();
		$( '#edit-map-container' ).show( 'slide', {direction: 'left'}, 300 );
		$( '#edit-map-overlay' ).fadeIn( 300 );
	} );

	$( '#edit-map-overlay, .close-edit-map-btn' ).click( function( event ) {
		event.preventDefault();
		$( '#edit-map-container' ).hide();/* 'slide', {direction: 'left'}, 200 );*/
		$( '#edit-map-overlay' ).fadeOut( 200 );
	} );


	$( '.route-container' ).click( function() {
		var key = $( this ).data( 'key' );
		eMap.switchRoute( key );
	} );

	$( '.switch-manual-btn' ).click( function( event ) {
		event.preventDefault();
		eMap.switchToManual();
	} );

	$('#auto-sort a').click(function(e) {
		e.preventDefault();
		$(this).find('i').toggleClass('fa-toggle-off fa-toggle-on');
		var autoSort = $(this).find('i').attr('class') == 'fa fa-2x fa-toggle-off' ? 'off' : 'on';

		eroam.ajax('post', 'map/update', { auto_sort: autoSort }, function() {
			window.location.reload();
		});
	});

	// TOOLTIP
	$('[data-toggle="tooltip"]').tooltip();
	$('body').on('click', '#cancel-reorder-btn', function(e) {
		e.preventDefault();
		$('#reorder-locations-container').hide();
		$('.itinerary-container, #reorder-locations-btn').show();
	});


	// START FUNCTION TO POSSIBLY REPLACE ALL UNCESSARY OWN ARRANGEMENT WITH API DATA
	// CREATED BY MIGUEL ON 2017-03-09
	var initialLoad = false; // THIS SHOULD BE A VARIABLE IN THE SEARCH_SESSION; SET AS FALSE AFTER INITIAL LOAD
	var searchSession = JSON.parse($('#search-session').val());
	if( initialLoad )
	{
		// searchSession.initialLoad = false;
		var travelDate    = searchSession.travel_date;
		var travellers    = searchSession.travellers;
		var itinerary     = searchSession.itinerary;

		var ownArragementHotels     = []; // VARIABLE TO STORE THE FOUND OWN ARRANGEMENT HOTELS
		var ownArragementActivities = []; // VARIABLE TO STORE THE FOUND OWN ARRANGEMENT ACTIVITIES
		var ownArragementTransports = []; // VARIABLE TO STORE THE FOUND OWN ARRANGEMENT TRANSPORTS
		var transportDuration  = '';
	
		itinerary.forEach(function( leg, legKey ){ // START LOOP THROUGH ITNERARY

			// JAYSON SHOULD WORK HERE
			// CHECK IF HOTEL IS OWN ARRANGEMENT
			if( leg.hotel == null )
			{
				// eroam.apiDeferred();
				// eroam.apiPromiseHandler();
			}
			// END HOTEL CHECK

			// CHECK IF ACTIVITY IS OWN ARRANGEMENT
			if( leg.activities == null )
			{
				var destinationReq = {
					destinationName: leg.city.name,
					country: leg.city.country.name
				};
				
				var destinationApiCall = eroam.apiDeferred('service/location', 'POST', destinationReq, 'destination', true);
				eroam.apiPromiseHandler( destinationApiCall, function( destinationResponse ){
					
					if( destinationResponse != null ){

						if( Object.keys(destinationResponse).length > 0){
							vRQ = {
								destId: destinationResponse.destinationId,
								startDate: formatDate(leg.city.date_from),
								endDate: formatDate(leg.city.date_to),
								currencyCode: 'AUD',
								sortOrder: 'TOP_SELLERS',
								topX: '1-15',
								provider:'viator'
							};
			
							var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
							eroam.apiPromiseHandler( viatorApiCall, function( viatorResponse ){
								
								if( viatorResponse != null )
								{
									if( viatorResponse.length > 0 )
									{
										var viatorAct = viatorResponse[0];
										
										data ={
											'date_from':formatDate(leg.city.date_from),
											'date_to':formatDate(leg.city.date_to),
											'id':viatorAct.code,
											'city_id':leg.city.id,
											'name':	viatorAct.title,
											'city':{
												'id':leg.city.id
											},
											'activity_price':[{
												'price':viatorAct.price,
												'currency':{'id':1,'code':'AUD'}
											}], 
											'price':[{
												'price':viatorAct.price,
												'currency':{'id':1,'code':'AUD'}
											}], 
											'currency_id':1,
											'currency': 'AUD',
											'provider': 'viator',
											'description': viatorAct.shortDescription
										};
					
										searchSession.itinerary[legKey].activities = [data];

										bookingSummary.update( JSON.stringify( searchSession ) );
					
									}
								}
							});
						}
					}
				});	
			}
			// END ACTIVITY CHECK

			// CHECK IF TRANSPORT IS OWN ARRANGEMENT IF LEG IS NOT THE LAST LEG
			if( legKey != (itinerary.length - 1) )
			{
				if( leg.transport == null || !leg.transport )
				{
					var mystiflyRQ;
					var mystiflyApiCalls            = [];
					var originIatas                 = leg.city.airport_codes;
					var destinationIatas            = itinerary[ legKey + 1 ].city.airport_codes;
					var arrayOfOriginIataCodes      = splitString( originIatas, "," );
					var arrayOfDestinationIataCodes = splitString( destinationIatas, "," );

					if( arrayOfOriginIataCodes.length > 0 && arrayOfDestinationIataCodes.length > 0 )
					{
						arrayOfOriginIataCodes.forEach(function( originIataCode ){
							arrayOfDestinationIataCodes.forEach(function( destinationIataCode ){
								mystiflyRQ = {
									DepartureDate : leg.city.date_to,
									OriginLocationCode : originIataCode,
									DestinationLocationCode : destinationIataCode,
									CabinPreference : 'Y',
									Code : ['ADT'],
									Quantity : [travellers],
									IsRefundable : false,
									IsResidentFare : false,
									NearByAirports : false,
									provider: 'mystifly'
								};

								try{
									console.log('mystiflyRQ Leg '+legKey, mystiflyRQ);
									mystiflyApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', mystiflyRQ, 'mystifly', true ) );
								}catch(e){
									console.log('An error has occured while sending the request to the mystifly API for leg'.legKey, e.message);
								}
							});
						});

						try{

							eroam.apiArrayOfPromisesHandler( mystiflyApiCalls, function( mystiflyArrayOfResponses ){
								mystiflyArrayOfResponses.forEach( function( mystiflyResponse ){
									console.log('mystiflyRS Leg '+legKey, mystiflyResponse);
									if( mystiflyResponse != null )
									{
										try{

											var transport;
											if( Array.isArray( mystiflyResponse.PricedItineraries.PricedItinerary ) ){
												if( mystiflyResponse.PricedItineraries.PricedItinerary.length > 0 ){
													transport = mystiflyResponse.PricedItineraries.PricedItinerary[0];
												}
											}else{
												transport = mystiflyResponse.PricedItineraries.PricedItinerary;
											}

											var fromCity           = leg.city.name;
											var toCity             = itinerary[ legKey + 1 ].city.name;
											var t                  = transport.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment;
											var fareSourceCode     = transport.AirItineraryPricingInfo.FareSourceCode;
											var airlineName        = ( isNotUndefined( t.OperatingAirline.Name ) ) ? t.OperatingAirline.Name : 'N/A';
											var airlineCode        = ( isNotUndefined( t.OperatingAirline.Code ) ) ? t.OperatingAirline.Code : 'N/A';
											var etd                = t.DepartureDateTime;
											var eta                = t.ArrivalDateTime;
											var transportClass     = (t.CabinClassText).trim() != '' ? t.CabinClassText : 'Cabin Class Not Specified';
											var departure          = moment( etd, moment.ISO_8601 );
											var arrival            = moment( eta, moment.ISO_8601 );
											var difference         = moment.duration( arrival.diff( departure ) );
											var duration           = difference.asHours();
											var currency           = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
											var price              = eroam.convertCurrency( transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount, currency );
											
											var transportName      = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+' / Depart - '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure.format('hh:mm A')+'. Arrive - '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+arrival.format('hh:mm A');
											
											var bookingSummaryText = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+'<br/><small>Depart: '+t.DepartureData+' '+departure.format('hh:mm A')+'</small><br/><small>Arrive: '+t.ArrivalData+' '+arrival.format('hh:mm A')+'</small>';

											var data = {
												airline_code: airlineCode,
												arrival_data: t.ArrivalData,
												arrival_location: t.ArrivalAirportLocationCode,
												booking_summary_text: bookingSummaryText,
												currency: globalCurrency,
												departure_data: t.DepartureData,
												departure_location: t.DepartureAirportLocationCode,
												duration: duration,
												eta: eta,
												etd: etd,
												fare_source_code: fareSourceCode,
												fare_type: transport.AirItineraryPricingInfo.FareType,
												flight_number: t.FlightNumber,
												from_city_id: leg.city.id,
												id: fareSourceCode,
												is_selected: true,
												operating_airline: airlineName,
												passenger_type_code: "ADT",
												passenger_type_quantity: travellers,
												price: { 
													0:{
														price:price,
														currency:{
															code:currency
														}
													}
												},
												provider: "mystifly",
												to_city_id: itinerary[ legKey + 1 ].city.id,
												transport_id: fareSourceCode,
												transport_type: {
													id: 1,
													name: "Flight"
												},
												transport_type_id: 1,
												transport_type_name: "Flight",
												transporttype: {
													id: 1,
													name: "Flight"
												}
											};

											searchSession.itinerary[ legKey ].transport = data;
											bookingSummary.update( JSON.stringify(searchSession) );
										}
										catch(e)
										{
											console.log('An error has occured while formatting the response of mystifly API call for leg'.legKey, e.message);
										}
									}
									else
									{
									}
								});

							});
						
						}
						catch(e)
						{
							console.log('An error has occured while processing the response of mystifly API call for leg'.legKey, e.message);
						}

					}
					else
					{
						//bookingSummary.hideMiniLoader( transportElement ); // hide mini loader
					}
				}
			}
			// END TRANSPORT CHECK
			
		}); // END LOOP THROUGH ITNERARY

	}

}); // end document ready


/*
| Update by junfel
| Added separator parameter
*/
function formatDate(date, separator='-') {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join( separator );
}

function calculateHeight(){
    var winHeight = $(window).height();

    var oheight = $('.page-sidebar').outerHeight();// -22
    var oheight1 = $('.page-sidebar').outerHeight();
    //alert(oheight);
    var elem = $('.page-content .tabs-container').outerHeight();
    var elemHeight = oheight - elem;
    $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);

    var elemHeight1 = oheight1 - elem;
    var winelemHeight = winHeight - elem;

    if(winHeight < oheight){
        $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);
        $(".page-content .tabs-content-wrapper1").outerHeight(elemHeight);
        $(".page-content .tabs-content-wrapper2").outerHeight(oheight1);
    } else{
        $(".page-content .tabs-content-wrapper").outerHeight(winelemHeight);
        $(".page-content .tabs-content-wrapper1").outerHeight(winelemHeight);
        $(".page-content .tabs-content-wrapper2").outerHeight(winelemHeight);
    }
    
	/*---- hotel content ------*/
    $(".page-content .hotel-container").outerHeight(elemHeight);
    var hotelTop = $('.page-content .hotel-top-content').outerHeight();
    var hotelTabs = $('.page-content .custom-tabs').outerHeight() + 120;
    var hotelHeight = (elemHeight - hotelTop) - hotelTabs;
    var winHotelHeight = (winelemHeight - hotelTop) - hotelTabs;
    if(winHeight < oheight1){
        $(".page-content .hotel-container").outerHeight(elemHeight1);
        $(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
    } else{
        $(".page-content .hotel-container").outerHeight(winelemHeight);
        $(".page-content .hotel-bottom-content").outerHeight(winHotelHeight);
    }
}

</script>
@stop