<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ExpediaApiController;
use App\Libraries\ApiCache;
use App\Libraries\EroamSession;
use App\Libraries\Filter;
use App\Libraries\Map;
use Cache;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Mail;
use PDF;
use Validator;
use \App\Helpers\FunctionsHelper;
use Session;

class PagesController extends Controller {

	private $session;
	private $cms_url;
	protected $expediaApi;
	private $headers = [];

	public function __construct() {
		parent::__construct();
		$this->session = new EroamSession;
		if(!session()->has('default_selected_city')) {
			$this->session->set_city();
		}
		$this->eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
		$this->map = new Map;
		$this->cms_url = Config::get('env.CMS_URL');
		$this->expediaApi = new ExpediaApiController;
	}

	public function sass_to_css(){
		// Set Custome Sass file code into css file //
		
		$sass_var = $this->getVariables();
		$temp_str = '';
		foreach($sass_var as $key=>$val){
			$temp_str .= '$'.$key.':'.$val;
		}
		
		FunctionsHelper::compileTheme($temp_str);
		die;
	}
	
	public function getVariables(){
		$var_array = [
			'border-radius' => '.125rem;',
			'enable-shadows' => 'false;',
			'card-border-radius' => '0;',
			'grid-gutter-width' => '20px;',
			'font-weight-bold' => 'HelveticaNeue-Bold,sans-serif;',
			'leftColumnwidth' => '350px;',
			'accountColumnwidth' => '350px;',
			
			'font-stack' => 'HelveticaNeue,sans-serif;',
			'primary-regular-font' => 'HelveticaNeue,sans-serif;',
			'secondary-bold-font' => 'HelveticaNeue-Bold,sans-serif;',
			'primary-extended-font' => 'HelveticaNeueMedium11,sans-serif;',
			'primary-lightExt-font' => 'HelveticaNeueLight08,sans-serif;',
			'primary-boldExt-font' => 'harabara_maisharabaramaisdemo,sans-serif;',
			'primary-color' => '#212121;',
			
			'box-shadow' => '0px 4px 4px 0px rgba(0, 0, 0, 0.24);',
			'header-padding' => '1px 0;',
			'footer-bg-color' => '#212121;',
			'header-bg-color' => '#071B22;',
			'search-filter-bg-color' => 'rgba(7, 27, 38, 0.8);',
			
			'primary' => '#071B22;',
			'secondary' => '#212121;',
			'danger' => '#f53d3d;',
			'light' => '#f4f4f4;',
			'dark' => '#212121;',
			'white' => '#ffffff;',
			'blue' => '#249DD0;',
			'blacklight' => 'rgba(7, 27, 38, 0.8);',
			'text-align' => 'center;',
			'text-transform' => 'uppercase;',
			'text_decoration_none' => 'none;',

			/*----btns----*/

			'btn-border-radius' => '0;',
			'btn-border-width' => '1px;',
			'btn-border-color' =>'transparent;',
			'btn-primary-background-color' => '#ffffff;',
			'btn-lightblue-background-color' => '249DD0;',
			'btn-border-style' => 'solid;',
			'btn-font-size' => '14px;',
			'btns-box-shadow' => '0 2px 5px 0 rgba(149, 149, 149, 0.50);',
			'btns-text-transform' => 'uppercase;',
			'btn-border' => 'none;',

			/*----Theme Color----*/

			'theme-color' => '#249DD0;',
			'theme-text-color' => '#212121;',
			'search-bg-color' => '#071B22;',

			/*----text-color----*/

			'footer-text-color' => '#fff;',


			/*----forms-color----*/

			'fildes-outer-border' => '1px;',
			'fildes-background-color' => '#ffffff;',
			'fildes-position' => 'relative;',
			'fildes-border-radius' => '0px;',
			'fildes-padding' => '7px 0px 0 0px;',
			'fildes-border-color' => '#F3F3F3;',
			'fildes-border-style' => 'solid;',
			'fildes-title-color' => '#fff;',
		];
		return $var_array;
	}
	
	public function cache_locations() {
		$apiCache = new ApiCache;
		$apiCache->get_all_cities();
		$apiCache->get_all_countries();
		$apiCache->get_all_hotel_categories();
		$apiCache->get_all_labels();
		$apiCache->get_traveller_options();

		return redirect('/');
	}

	public function home() {
		//\session()->forget('initial_authentication');
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}
			
		try
		{
			$ip_address = '137.59.252.196';

			$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip_address));
			
			$countryName = $query['country'];
			$tourCountries = http('post', 'getTourCountriesAvailables', [], $this->headers);
			$tourCities = http('post', 'getTourCitiesAvailable', [], $this->headers);

			$city = ucfirst($query['city']);
			$res = get_cities_by_city_name($city); 

			if ($res) {
				$key = array_keys($res);
				$key = $key[0];
				$from_city_id = $res[$key]['id'];
				$from_country_id = $res[$key]['country_id'];

			} else {
				$from_city_id = '';
				$from_country_id = '';

			}

			$cities = Cache::get('cities');
			$countries = Cache::get('countries');
			$labels = Cache::get('labels');
			$travellers = Cache::get('travellers');
			$countries1 = Cache::get('countriesBookingsPro');

			if (!session()->has('transport_types')) {

				session()->put('transport_types', $travellers['transport_types']);
			}
			$travel_pref = [];
			$interest_ids = [];
			if (session()->has('travel_preferences')) {
				$travel_pref = session()->get('travel_preferences');
				$travel_pref = reset($travel_pref);
				$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
			}

			usort($countries, 'sort_by_name');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->home();
		}

		session()->forget('tourCountryData');
		session()->forget('fromTourHome');
		return view(
			'home.home',
			[
				'cities' => $cities,
				'from_city_id' => $from_city_id,
				'from_country_id' => $from_country_id,
				'countries' => $countries,
				'countries1' => $countries1,
				'labels' => $labels,
				'travellers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
				'countryName' => $countryName,
				'default_currency' => 'AUD',
				'page_will_expire' => 1,
				'tourcountries' => $tourCountries,
				'tourCities' => $tourCities,
			]
		);

	}

	public function maphome() {
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		try
		{
			$starting_country = request()->input()['country'];
			$starting_city = request()->input()['city'];
			$destination_country = request()->input()['to_country'];
			$destination_city = request()->input()['to_city'];

			if ($destination_city != '') {
				$toName = get_city_by_id($destination_city);
				$mapName = $toName['name'] . ',' . $toName['country_name'];
				$toName = $toName['name'];

				$url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($mapName);
				$json_data = file_get_contents($url);
				$result = json_decode($json_data, TRUE);
				$latitude = $result['results'][0]['geometry']['location']['lat'];
				$longitude = $result['results'][0]['geometry']['location']['lng'];
			} else {
				$toName = '';
				$latitude = '-33.868093';
				$longitude = '151.206427';
			}
			if ($starting_city != '') {
				$startName = get_city_by_id($starting_city);
				$startName = $startName['name'];
			} else {
				$startName = '';
			}

			$countries = Cache::get('countries');
			$labels = Cache::get('labels');
			$travellers = Cache::get('travellers');

			if (!session()->has('transport_types')) {
				session()->put('transport_types', $travellers['transport_types']);
			}
			$travel_pref = [];
			$interest_ids = [];
			if (session()->has('travel_preferences')) {
				$travel_pref = session()->get('travel_preferences');
				$travel_pref = reset($travel_pref);
				$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
			}

			usort($countries, 'sort_by_name');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->home();
		}

		return view(
			'pages.welcome',
			[
				'countries' => $countries,
				'destination_country' => $destination_country,
				'destination_city' => $destination_city,
				'starting_country' => $starting_country,
				'starting_city' => $starting_city,
				'toName' => $toName,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'startName' => $startName,
				'labels' => $labels,
				'travellers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
			]
		);

	}

	public function update_booking_summary() {

		$search = request()->input('search');
		// update

		if ($search) {

			$search = json_decode($search, true);
			$pax_information = [];
			$travel_date = $search['travel_date'];
			$travellers = $search['travellers'];
			$child_total = $search['child_total'];
			$pax_information = $search['pax_information'];
			$rooms = $search['rooms'];

			$next_date = null;
			$start_from = null;
			foreach ($search['itinerary'] as $key => $value) {

				if ($key == 0) {
					$start_from = $travel_date;
					$next_date = $start_from;
				} else {
					$next_date = Carbon::parse($next_date)->addDays($value['city']['default_nights'])->format('Y-m-d');
					$start_from = isset($search['itinerary'][$key - 1]['city']['date_to']) ? $search['itinerary'][$key - 1]['city']['date_to'] : $next_date;

				}

				if (empty($value['hotel'])) {

					$return_hotel = $this->map->set_default_hotel_api(['date_from' => date('Y-m-d', strtotime($start_from)), 'traveller_number' => $travellers], $value['city']);

					$search['itinerary'][$key]['hotel'] = $return_hotel;
				}

				if (empty($value['activities'])) {
					$return_activity = $this->map->set_viator_default_activity(['date_from' => date('Y-m-d', strtotime($start_from)), 'traveller_number' => $travellers], $value['city']);
					$search['itinerary'][$key]['activities'] = $return_activity ? [$return_activity] : null;

				}

				//miguel here
				$last_key = count($search['itinerary']) - 1;
				if ($key != $last_key) {
					if (empty($value['transport'])) {
						$transport = $value['transport'];
						$origin_city = $value['city'];
						$destination_city = $search['itinerary'][$key + 1]['city'];
						$date_from = $start_from;
						$add_days = intval($value['city']['default_nights']);
						$date_to = add_str_time('+' . $add_days . ' days', $start_from, 'Y-m-d');

						$is_flight_filtered = FALSE;
						if (isset($this->default['search_input']['transport_types'])) {
							$transport_type_filters = $this->default['search_input']['transport_types'];
							foreach ($transport_type_filters as $key => $filter) {
								if ($filter == '1') {
									$is_flight_filtered = TRUE;
								}
							}
						}

						$options = [
							'date_from' => $date_from,
							'date_to' => $date_to,
							'origin_city' => $origin_city,
							'destination_city' => $destination_city,
							'traveller_number' => $travellers,
							'leg' => $key,
							'is_flight_filtered' => $is_flight_filtered,
						];
						$search['itinerary'][$key]['transport'] = $this->map->get_transport_api_data($transport, $options);
					}
				} else {

					$search['itinerary'][$key]['transport'] = null;

				}
			}
			$last = count($search['itinerary']) - 1;
			if (!empty($search['itinerary'][$last]['hotel'])) {
				if (!isset($search['itinerary'][$last]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])) {
					$reqest_data['city_ids'] = $search['itinerary'][$last]['city']['id'];
					$reqest_data['date_from'] = session()->get('search_input')['start_date'];
					$reqest_data['auto_populate'] = session()->get('search_input')['auto_populate'];
					$reqest_data['traveller_number'] = session()->get('search_input')['travellers'];
					$reqest_data['child_number'] = session()->get('search_input')['total_children'];
					$reqest_data['search_input'] = session()->get('search_input');
					$reqest_data['rooms'] = session()->get('search_input')['rooms'];
					$reqest_data['child'] = session()->get('search_input')['child'];
					$reqest_data['pax_information'] = [];
					$response = $this->map->getSelectedHotelRoom($search['itinerary'][$last]['hotel'], $reqest_data);
					$search['itinerary'][$last]['hotel']['RoomRateDetailsList']['RoomRateDetails'] = $response;
				}
			}
			$this->session->set_search($search['itinerary'], ['date_from' => date('Y-m-d', strtotime($travel_date)), 'traveller_number' => $travellers, 'pax_information' => $pax_information, 'child_number' => $child_total, 'rooms' => $rooms]);
		}

		return view('itinenary.partials.booking_summary')->render();
	}

	public function saveBooking() {

		$search = request()->input('search');
		// update
		if ($search) {

			$search = json_decode($search, true);
			$pax_information = isset($search['pax_information']) ? $search['pax_information'] : [];
			$travel_date = $search['travel_date'];
			$travellers = $search['travellers'];
			$pax_information = $search['pax_information'];
			$rooms = $search['rooms'];
			$child_total = $search['child_total'];

			$next_date = null;
			$start_from = null;

			foreach ($search['itinerary'] as $key => $value) {
				if ($key == 0) {
					$start_from = $travel_date;
					$next_date = $start_from;
				} else {
					$next_date = Carbon::parse($next_date)->addDays($value['city']['default_nights'])->format('Y-m-d');
					$start_from = isset($search['itinerary'][$key - 1]['city']['date_to']) ? $search['itinerary'][$key - 1]['city']['date_to'] : $next_date;
				}

				$trans_own_arrangement = FALSE;
				$search_preferences = session()->get('search_input');
				if (isset($search_preferences['transport_types'])) {
					if ($search_preferences['transport_types'][0] == 25) {
						$trans_own_arrangement = TRUE;
					}
				}

				$last_key = count($search['itinerary']) - 1;

				if (empty($value['transport'])) {
					if ($key != $last_key) {
						if ($trans_own_arrangement != TRUE) {
							$transport = $value['transport'];
							$origin_city = $value['city'];
							$destination_city = $search['itinerary'][$key + 1]['city'];
							$date_from = $start_from;
							$add_days = intval($value['city']['default_nights']);
							$date_to = add_str_time('+' . $add_days . ' days', $start_from, 'Y-m-d');

							$transport_type_preference = (session()->has('travel_preferences')) ? session()->get('travel_preferences') : [];

							$is_flight_filtered = FALSE;
							if (count($transport_type_preference)) {
								if (isset($transport_type_preference[0]['transport'])) {
									if (count($transport_type_preference[0]['transport']) > 0) {
										$transport_type_filters = $transport_type_preference[0]['transport'];
										foreach ($transport_type_filters as $k => $filter) {
											if ($filter == '1') {
												$is_flight_filtered = TRUE;
											}
										}
									}
								}
							}

							$options = [
								'date_from' => $date_from,
								'date_to' => $date_to,
								'origin_city' => $origin_city,
								'destination_city' => $destination_city,
								'traveller_number' => $travellers,
								'leg' => $key,
								'is_flight_filtered' => $is_flight_filtered,
							];

							$search['itinerary'][$key]['transport'] = $this->map->get_transport_api_data($transport, $options);
						} else {
							$search['itinerary'][$key]['transport'] = NULL;
						}

					}
				}

				if (empty($value['hotel'])) {

					$return_hotel = $this->map->set_default_hotel_api(['date_from' => date('Y-m-d', strtotime($start_from)), 'traveller_number' => $travellers], $value['city']);

					$search['itinerary'][$key]['hotel'] = $return_hotel;
				}

			}

			$this->session->set_search($search['itinerary'], ['date_from' => date('Y-m-d', strtotime($travel_date)), 'traveller_number' => $travellers, 'pax_information' => $pax_information, 'rooms' => $rooms, 'child_number' => $child_total]);
		}

		return view('itinenary.partials.booking_summary')->render();
	}

	public function hotels(Request $request) {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}
		$search_session = json_decode(json_encode(session()->get('search')), FALSE);

		$leg = $request->input('leg');

		if ($leg != NULL) {
			if ($leg >= count($search_session->itinerary)) {
				abort(404);
			}
		} else {
			abort(404);

		}

		$city_name = $request->city_name;
		$request->session()->put('current_city', $city_name); //set session for what city the viewed hotels belong
		$city_data = get_city_by_name($city_name);
		$city_iata_code = $search_session->itinerary[$leg]->city->airport_codes;
		$travellers = session()->get('search_input')['travellers'];
		$hotel_categories = Cache::get('hotel_categories');
		$travel_preferences = (session()->has('travel_preferences')) ? session()->get('travel_preferences') : [];
		$hotel = $search_session->itinerary[$leg];

		$temp_hotel = (array) $hotel;
		if (empty($temp_hotel['hotel'])) {
			$price_data = 0;
			$city_id = $hotel->city->id;
			$default_hotel_id = 0;
			$default_nights = $hotel->city->default_nights;
			$default_room_price = '0.0';
			$default_room_name = '';
			$default_room_id = '0';
			$default_price_id = 0;
			$date_from = $hotel->city->date_from;
			$date_to = $hotel->city->date_to;
			$default_currency = 'AUD';
			$default_provider = 'eroam';
			$checkin = $hotel->city->date_from;
			$checkout = $hotel->city->date_to;
		} else {

			$city_id = $hotel->city->id;
			if ($hotel->hotel->provider == 'expedia') {

				$default_hotel_id = $hotel->hotel->hotelId;
				$default_nights = $hotel->hotel->nights;
				$date_from = $hotel->hotel->checkin;
				$date_to = $hotel->hotel->checkout;
				$checkin = $hotel->hotel->checkin;
				$checkout = $hotel->hotel->checkout;

				$total = '@total';
				$currencyCode = '@currencyCode';
				$default_room_price = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$total;
				$rData = json_decode(json_encode($hotel->hotel->RoomRateDetailsList->RoomRateDetails), true);

				if (isset($rData['rateDescription'])) {
					$default_room_name = $rData['rateDescription'];
				} else {
					$default_room_name = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->roomDescription;
				}
				$roomCode = '@roomCode';
				if (isset($rData['RoomType'][$roomCode]) && !empty($rData['RoomType'][$roomCode])) {
					$default_room_id = $rData['RoomType'][$roomCode];
					$default_price_id = $rData['RoomType'][$roomCode];
				} else {
					$default_room_id = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
					$default_price_id = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
				}
				$default_currency = $hotel->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$currencyCode;
				$default_provider = 'expedia';

			} else {

				$default_hotel_id = $hotel->hotel->id;
				$default_nights = $hotel->hotel->nights;
				$date_from = $hotel->city->date_from;
				$date_to = $hotel->city->date_to;
				$checkin = $hotel->hotel->checkin;
				$checkout = $hotel->hotel->checkout;

				$default_room_price = !is_array($hotel->hotel->price) ? $hotel->hotel->price : $hotel->hotel->price[0]->price;
				$default_room_name = !is_array($hotel->hotel->price) ? $hotel->hotel->room_name : $hotel->hotel->price[0]->room_type->name;
				$default_room_id = !is_array($hotel->hotel->price) ? $hotel->hotel->room_id : $hotel->hotel->price[0]->room_type->id;
				$default_price_id = !is_array($hotel->hotel->price) ? $hotel->hotel->price_id : $hotel->hotel->price[0]->id;
				$default_currency = (isset($hotel->hotel->currency->code)) ? $hotel->hotel->currency->code : $hotel->hotel->currency;
				$default_provider = (isset($hotel->hotel->provider)) ? $hotel->hotel->provider : 'eroam';

				if ($hotel->hotel->price) {
					if (is_array($hotel->hotel->price)) {
						$price_data = reset($hotel->hotel->price);
						$default_room_price = $price_data->price;
						$default_price_id = $price_data->id;
					} elseif (is_object($hotel->hotel->price)) {
						$price_data = reset($hotel->hotel->price);
						$default_room_price = $price_data->price;
						$default_price_id = $price_data->id;
					} elseif (is_string($hotel->hotel->price)) {
						$default_room_price = $hotel->hotel->price;
						$default_price_id = $hotel->hotel->price_id;
					}

				}
			}

		}

		$suburb = [];
		$suburb = http('get', 'suburb-by-city/' . $city_id);

		$hotel_filter = !empty(session()->get('search_input')['hotel_category_id']) ? session()->get('search_input')['hotel_category_id'] : array();
		$room_type_id = !empty(session()->get('search_input')['room_type_id']) ? session()->get('search_input')['room_type_id'] : array();

		if (empty(session()->get('search_input')['hotel_category_id'])) {
			$hotel_category_id[0] = 1;
			$key = array_search($hotel_categories[0], array_column($hotel_categories, 'id'));
			$hotel_stars = $hotel_categories[$key]['hotel_stars'];
			$hotel_stars = array_map('trim', explode(',', $hotel_stars));
			$default_hotel_category_id = 1;

		} else {
			$key = array_search(session()->get('search_input')['hotel_category_id'][0], array_column($hotel_categories, 'id'));
			$hotel_stars = $hotel_categories[$key]['hotel_stars'];
			$hotel_stars = array_map('trim', explode(',', $hotel_stars));
			$default_hotel_category_id = session()->get('search_input')['hotel_category_id'][0];
		}
		if (empty($travel_preferences)) {
			$hotel_category_id = [];
		} else {
			$hotel_category_id = $travel_preferences[0]['accommodation'];
		}
		$data = [
			'city' => $city_data,
			'city_name' => $city_name,
			'city_id' => $city_id,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'city_ids' => [$city_id],
			'default_provider' => $default_provider,
			'default_hotel_id' => $default_hotel_id,
			'default_nights' => $default_nights,
			'default_room_price' => $default_room_price,
			'default_room_name' => $default_room_name,
			'default_room_id' => $default_room_id,
			'default_price_id' => $default_price_id,
			'default_currency' => $default_currency,
			'city_iata' => $city_iata_code,
			'travellers' => $travellers,
			'leg' => $leg,
			'hotel_filter' => $hotel_filter,
			'room_type_id' => $room_type_id,
			'checkin' => $checkin,
			'checkout' => $checkout,
			'hotel_stars' => $hotel_stars,
			'default_hotel_category_id' => $default_hotel_category_id,
			'suburb' => $suburb,
			'hotel_category_id' => $hotel_category_id,
		];

		return view('accomodation.accomodation', ['data' => $data, 'page_will_expire' => 1]);
	}

	public function transports(Request $request) {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}

		$session_input = session()->get('search_input');


		$transport_pref = isset($session_input['transport_types']) ? $session_input['transport_types'] : [];

		$search_session = json_decode(json_encode(session()->get('search')), FALSE);
		$leg = $request->input('leg');

		if ($leg != NULL) {
			if ($leg >= count($search_session->itinerary)) {
				echo '404 page not found.';die; // check if leg is valid
			}
		} else {
			echo '404 page not found.';die; // check if leg get parameter leg exists
		}
		$city = json_decode(json_encode(get_city_by_name($request->city_name)), FALSE);

		$country_name = $city->country_name;
		$city_name = $request->city_name;
		$selected_transport_id = 0;
		$selected_provider = 'eroam';
		$from_city_iata = NULL;
		$transport_type_id = 0;
		$city_id = 0;
		$to_city_id = 0;
		$num_of_cities = count($search_session->itinerary);
		$transport_departure = false;
		$transport_duration = '';
		$activity_dates = [];

		$itinerary = $search_session->itinerary[$leg];

		if ($leg != ($num_of_cities - 1)) {

			$days_to_add_for_departure_date = intval($itinerary->city->default_nights);
			$next_city_data = $search_session->itinerary[$leg + 1];
			$transport_departure = isset($itinerary->transport->etd) ? $itinerary->transport->etd : '';
			$transport_duration = isset($itinerary->transport->duration) ? $itinerary->transport->duration : '';
			$transport_provider = isset($itinerary->transport->provider) ? $itinerary->transport->provider : 'eroam';
			$city_id = $itinerary->city->id;
			$from_city_iata = ($itinerary->city->iata) ? $itinerary->city->iata->iata_code : NULL;
			$from_city_iatas = $itinerary->city->airport_codes;
			$to_city_id = $next_city_data->city->id; // we need to have this for querying
			$to_city_iata = ($next_city_data->city->iata) ? $next_city_data->city->iata->iata_code : NULL;
			$to_city_iatas = $next_city_data->city->airport_codes;
			$date_from = $itinerary->city->date_from;
			$date_to = $itinerary->city->date_to;
			$to_city_geohash = $next_city_data->city->geohash;
			$from_city_geohash = $itinerary->city->geohash;
			$index = $leg;

			if ($itinerary->transport) {
				$transport_type_id = $itinerary->transport->transport_type_id;
				$selected_transport_id = $itinerary->transport->id;
				if (isset($itinerary->transport->provider)) {
					$selected_provider = $itinerary->transport->provider;
				}
			}
			/*
				| Added by junfel
				| Get all activity dates
			*/
			if ($itinerary->activities) {
				foreach ($itinerary->activities as $activities) {
					$activity_dates[] = $activities->date_selected;
				}
			}

		}

		switch ($transport_provider) {
		case 'mystifly':

			if ($leg == 0) {
				$departure_date = date('Y/m/d', strtotime($date_to));

			} else {
				$transport_duration = get_hours_min($transport_duration);
				$transport_departure = date('H:i A', strtotime($transport_departure));
				$departure_date = get_departure_date($date_to, $transport_departure, $transport_duration);
			}
			break;
		case 'busbud':

			if ($leg == 0) {
				$departure_date = date('Y/m/d', strtotime($date_to));

			} else {
				$transport_duration = get_hours_min($transport_duration);
				$transport_departure = date('H:i A', strtotime($transport_departure));
				$departure_date = get_departure_date($date_to, $transport_departure, $transport_duration);
			}
			break;

		default:
			$departure_date = get_departure_date($date_to, $transport_departure, $transport_duration);

			break;
		}

		$flight_options = [
			'IsRefundable' => FALSE,
			'IsResidentFare' => FALSE,
			'NearByAirports' => FALSE,
		];

		if (session()->has('flight_options')) {
			$fl_opts = session()->get('flight_options');
			if (isset($fl_opts[$leg])) {
				$flight_options = $fl_opts[$leg];
			}
		}

		$data = [
			'country_name' => $country_name,
			'city_name' => $city_name,
			'city_id' => $city_id,
			'from_city_iata' => $from_city_iata,
			'from_city_iatas' => $from_city_iatas,
			'to_city_id' => $to_city_id,
			'to_city_iata' => $to_city_iata,
			'to_city_iatas' => $to_city_iatas,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'key' => $index,
			'flight_options' => $flight_options,
			'selected_transport_id' => $selected_transport_id,
			'selected_provider' => $selected_provider,
			'leg' => $leg,
			'departure_date' => $departure_date,
			'to_city_geohash' => $to_city_geohash,
			'from_city_geohash' => $from_city_geohash,
			'activity_dates' => $activity_dates,
		];

		$transport_type_ids = [$transport_type_id];
		session(['transport_type_ids' => $transport_type_ids]);

		if (session()->get('transport_filter')) {
			session()->forget('transport_type_ids');
			session(['transport_type_ids' => session()->get('transport_filter')]);
		}

		$day = 0;
		if (session()->has('day')) {
			$day = session()->get('day');
		}

		$transport_types = [];
		if (session()->has('transport_types')) {
			$transport_types = session()->get('transport_types');
		}

		$view_data = [
			'transport_type_options' => $transport_types,
			'day' => $day,
			'data' => $data,
			'transport_pref' => $transport_pref,
			'page_will_expire' => 1,
		];
		return view('transport.transport', $view_data);
	}

	public function proposed_itinerary() {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}

		$search = session()->get('search');
		$page_will_expire = 1;
		return view('pages.proposed-itinerary')->with(compact('search', 'page_will_expire'));
	}

	public function propose_itinerary_pdf() {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}
		$today = date("F j, Y");
		$year = date('Y');

		$search = session()->get('search');

		$pdf = PDF::loadView('pages.proposed-itinerary-pdf', compact('search', 'today', 'year'));
		return $pdf->stream();

	}

	public function activities(Request $request) {
		$filter = new Filter();
		//get the session city by name

		$city = json_decode(json_encode(get_city_by_name($request->city_name)), FALSE);
		
		if (session()->get('search_input') == null) {
			return redirect('/');
		}

		//added by jayson for viator api get location by name
		//parameters needed country,destinationName
		$data = [];
		$data['destinationName'] = $city->name;
		$data['country'] = $city->country_name;
		$city_data_viator = http('post', 'service/location', $data);
		$destinationId = ($city_data_viator) ? $city_data_viator['destinationId'] : 0;
		//pr($destinationId);die;
		//end

		$city_id = $city->id;

		//get the session data
		$search_session = json_decode(json_encode(session()->get('search')), FALSE);
		$leg = $request->input('leg');

		if ($leg != NULL) {
			if ($leg >= count($search_session->itinerary)) {
				echo '404 page not found.';die; // check if leg is valid
			}
		} else {
			echo '404 page not found.';die; // check if leg get parameter leg exists
		}
		$selected_dates = [];
		$selected_activities = [];
		$dates = [];
		$dates_available = [];
		$default_nights = 0;
		$date_from = '';
		$date_to = '';
		$own_arrangement = false;
		$transport_own_arrangement = false;
		$transport_departure = false;
		$transport_duration = '';
		$activity_pref = session()->get('search_input')['interests'];
		$available_to_book = false;

		//traverse to itineraries and get city date_from and to
		$value = $search_session->itinerary[$leg];

		$default_nights = $value->city->default_nights;
		$date_from = $value->city->date_from;
		$date_to = $value->city->date_to;

		if (!$value->activities) {
			$own_arrangement = true;
		} else {

			foreach ($value->activities as $activity_key => $activity) {

				$activity_code = isset($activity->provider) ? $activity->provider . '_' . $activity->id : 'eroam_' . $activity->id;
				$selected_activities[] = $activity_code;
				$selected_dates[$activity_code] = date('jS, F Y', strtotime($activity->date_selected));

				$activity_duration = (int) $activity->duration;
				if ($activity_duration > 1) {
					$activity_duration = $activity_duration - 1;
					foreach (range(1, $activity_duration) as $day) {
						$selected_dates[] = date('jS, F Y', strtotime($activity->date_selected . '+' . $day . ' ' . str_plural('day', $day)));
					}
				}
			}
		}
		if (!$value->transport) {
			$transport_own_arrangement = true;
		} else {
			$transport_departure = $value->transport->etd;
			$transport_duration = $value->transport->duration;
		}
		/*
			|
		*/

		$departure_date = $date_to;

		/*
			| Added by junfel
			| Setting available dates for booking an activity
		*/
		if ($default_nights > 1) {
			$time = convert_24hr($transport_departure);

			$time = get_hour($time);
			for ($counter = 1; $counter < $default_nights; $counter++) {
				$date_generated = date('Y/m/d', strtotime($date_from . '+ ' . $counter . ' ' . str_plural('day', $counter)));
				/*
					| Check if there are activities being booked on the dates below, then exclude it to available dates.
				*/
				if (in_array(date('jS, F Y', strtotime($date_generated)), $selected_dates)) {
					continue;
				}
				/*
					| checking if the next date is equals to departure date
					| if not, then add the date.
				*/
				if ($date_generated == $departure_date) {
					/*
						| If departure is evening or own arrangement, then add date
					*/
					if ($time >= 18 || $transport_own_arrangement) {
						$dates_available[] = $date_generated;
						$available_to_book = true;
					}
				} else {
					$dates_available[] = $date_generated;
				}
			}
			/*
				| Just making sure that there will be no duplicated dates
			*/
			$dates_available = array_unique($dates_available);
		}
		/*
			| if dates available is empty, then add 0000/00/00
			| empty array can't be saved in a session.
		*/
		$dates_available = count($dates_available) > 0 ? $dates_available : array('0000/00/00');
		/*
			| Add to session available dates,
			| These dates will be displayed in the datepicker for bookings.
		*/
		session(['dates_available_' . $city_id => $dates_available]);

		/*
			| End setting available dates.
		*/

		$index = 0;
		$city_id = $city->id;
		$selected_activities = count($selected_activities) > 0 ? json_encode($selected_activities) : 0;

		$page_will_expire = 1;

		return view('activity.activity', compact('city', 'date_from', 'date_to', 'index', 'city_id', 'selected_activities', 'destinationId', 'selected_dates', 'leg', 'activity_pref', 'available_to_book', 'departure_date', 'page_will_expire'));
	}

	public function view_more_hotel_details(Request $request) {
		$provider = request()->input('provider');
		$code = request()->input('code');
		$data_attr = json_encode(request()->input('dataAttr'));
		$star_rating = request()->input('star_rating');

		// init
		$data = [];
		$data['hotel_name'] = '';
		$data['city_name'] = '';
		$data['hotel_description'] = '';
		$data['primary_photo'] = '';
		$data['images'] = [];
		$data['address'] = '';
		$data['telephone'] = '';
		$data['google_map_location'] = '';
		$data['lat'] = '';
		$data['lng'] = '';
		$data['cancel_policy'] = '';

		switch ($provider) {
		case 'eroam':
			$response = http('get', 'eroam/hotel/' . $code);
			$data['provider'] = 'eroam';
			$data['hotelName'] = $response['name'];
			$data['searchCity'] = $response['city']['name'];
			$data['leg'] = request()->input('leg');
			$data['city_id'] = request()->input('city_id');
			$data['country_name'] = request()->input('countryName');
			$data['arrivalDate'] = request()->input('arrivalDate');
			$data['departureDate'] = request()->input('departureDate');
			$data['currencyCode'] = request()->input('currencyCode');
			$data['hotelSeason'] = request()->input('hotelSeason');
			$data['hotel_description'] = $response['description'];
			$data['hotelId'] = request()->input('hotelId');
			$data['address_1'] = request()->input('address_1');
			$data['hotelPrice'] = request()->input('hotelPrice');
			$data['hotelCity'] = '';
			$data['latitude'] = (request()->input('latitude') == 'undefined' ? '' : request()->input('latitude'));
			$data['longitude'] = (request()->input('longitude') == 'undefined' ? '' : request()->input('longitude'));
			if (isset($response['geo_location']) && !empty($response['geo_location'])) {
				$data['google_map_location'] = $response['geo_location'];
			} else {
				$data['google_map_location'] = $response['name'] . ', ' . $response['city']['name'] . ', ' . $response['city']['country']['name'];
			}
			if ($response['image']) {
				foreach ($response['image'] as $key => $value) {

					$data['HotelImages']['HotelImage'][$key]['url'] = 'https://cms.eroam.com/' . $value['small'];
				}
			}
			if ($response['price']) {
				foreach ($response['price'] as $key => $value) {
					$data['rooms'][$key]['hotel_price_id'] = $value['id'];
					$data['rooms'][$key]['with_breakfast'] = $value['with_breakfast'];
					$data['rooms'][$key]['minimum_pax'] = $value['minimum_pax'];
					$data['rooms'][$key]['price'] = $value['price'];
					$data['rooms'][$key]['hotel_room_type_id'] = $value['hotel_room_type_id'];
					$data['rooms'][$key]['room_type_id'] = $value['room_type']['id'];
					$data['rooms'][$key]['name'] = $value['room_type']['name'];
					$data['rooms'][$key]['description'] = $value['room_type']['description'];
					$data['rooms'][$key]['pax'] = $value['room_type']['pax'];
					$data['rooms'][$key]['sequence'] = $value['room_type']['sequence'];
					$data['rooms'][$key]['dorm_pax'] = $value['room_type']['dorm_pax'];
					$data['rooms'][$key]['female_only'] = $value['room_type']['female_only'];
					$data['rooms'][$key]['season'] = $value['season']['id'];
				}
			}
			$data = json_decode(json_encode($data));

			break;

		case 'hb':
			$response = http('post', 'hb/gethoteldetailsbycodes', ['hotel_code' => array($code)]);

			$data['hotel_name'] = $response[0][$code]['name'];
			$data['city_name'] = session()->get('current_city');
			$data['hotel_description'] = $response[0][$code]['description'];
			$data['address'] = $response[0][$code]['address'];
			$data['telephone'] = $response[0][$code]['phones'] ? json_decode($response[0][$code]['phones'])[0]->phoneNumber : '';
			$data['lat'] = $response[0][$code]['latitude'];
			$data['lng'] = $response[0][$code]['longitude'];

			$data['provider'] = 'hb';
			$data['star_rating'] = $star_rating;
			// IMAGES
			$hb_images = json_decode($response[0][$code]['images']);
			if ($hb_images) {
				$data['primary_photo'] = config('env.HB_IMAGE_PATH') . $hb_images[0]->path;
				foreach ($hb_images as $key => $value) {
					$data['images'][] = config('env.HB_IMAGE_PATH') . $value->path;
				}
			}
			break;
		case 'expedia':

			$arrivalDate = date('m-d-Y', strtotime(request()->input('arrivalDate')));
			$departureDate = date('m-d-Y', strtotime(request()->input('departureDate')));

			$num_of_rooms = session()->get('search_input')['rooms'];
			$children = session()->get('search_input')['child'];
			$room = '';
			if ($num_of_rooms == 1) {
				$room .= '&room1=' . session()->get('search_input')['num_of_adults'][0];
				if (isset($children[0]) && !empty($children[0])) {
					$c_array = implode(',', $children[0]);
					$c_array = str_replace('0', '1', $c_array);
					$room .= ',' . count($children[0]) . ',' . $c_array;
				}
			} else {
				for ($i = 0; $i < $num_of_rooms; $i++) {
					$n = $i + 1;
					$room .= '&room' . $n . '=' . session()->get('search_input')['num_of_adults'][$i];
					if (isset($children[$i]) && !empty($children[$i])) {
						$c_array = implode(',', $children[$i]);
						$c_array = str_replace('0', '1', $c_array);
						$room .= ',' . count($children[$i]) . ',' . $c_array;
					}
				}
			}
			$roomTypeCode = request()->input('roomTypeCode');
			$includeDetails = request()->input('includeDetails');
			$includeDetails = request()->input('includeDetails');
			$options = request()->input('options');
			$rateKey = request()->input('rateKey');
			$latitude = request()->input('latitude');
			$longitude = request()->input('longitude');
			$customerSessionId = request()->input('customerSessionId');
			$response = $this->expediaApi->getHotelDetails($code, $arrivalDate, $departureDate, $room, $roomTypeCode, $includeDetails, $options, $rateKey, $latitude, $longitude, $customerSessionId);

			$response = json_decode($response)->data;

			$data = $response;
			$data->leg = request()->input('leg');
			$data->searchCity = request()->input('searchCity');
			$data->provider = 'expedia';
			$city_data = get_city_by_name(request()->input('searchCity'));
			$data->country_name = $city_data['country_name'];
			$data->latitude = request()->input('latitude');
			$data->longitude = request()->input('longitude');
			$data->arrivalDate = request()->input('arrivalDate');
			$data->departureDate = request()->input('departureDate');
			$data->city_id = request()->input('city_id');
			$data->hotelPrice = request()->input('hotelPrice');
			$data->google_map_location = '';

			break;

		case 'aot':
			$response = http('get', 'aot/hotel/' . $code);
			$data['hotel_name'] = $response['Name'];
			$data['city_name'] = session()->get('current_city');
			$data['hotel_description'] = $response['Description'];
			$data['address'] = $response['Address1'] . ', ' . $response['Address2'];
			$data['lat'] = json_decode($response['MetaData'])->Latitude;
			$data['lng'] = json_decode($response['MetaData'])->Longitude;
			$data['cancel_policy'] = $response['CancellationPolicy'];

			$aot_images = '';
			if (count(json_decode($response['Images'])) > 0) {
				$aot_images = json_decode($response['Images'])->image;
			}
			$data['provider'] = 'aot';
			$data['star_rating'] = $star_rating;

			if ($aot_images) {
				$data['primary_photo'] = $aot_images[0]->ServerPath;
				foreach ($aot_images as $key => $value) {
					$data['images'][] = $value->ServerPath;
				}
			}
			break;
		case 'ae':

			$response = http('get', 'arabian/explorer/get-hotel-details/' . $code);
			$data['hotel_name'] = $response['Name'];
			$data['city_name'] = session()->get('current_city');

			$data['hotel_description'] = $response['ShortDescription'];
			$data['address'] = $response['Address'];
			$data['lat'] = $response['Latitude'];
			$data['lng'] = $response['Longitude'];
			$data['cancel_policy'] = $response['Policies'];
			$ae_images = json_decode($response['Medias']);
			$data['provider'] = 'ae';
			$data['star_rating'] = $star_rating;

			if ($ae_images->Media) {
				$data['primary_photo'] = config('env.AE_IMAGE_PATH') . $ae_images->Media[0]->Path;
				foreach ($ae_images->Media as $key => $value) {
					$image = config('env.AE_IMAGE_PATH') . $value->Path;
					if (!in_array($image, $data['images'])) {
						$data['images'][] = $image;
					}

				}

			}

			break;
		}
		$page_will_expire = 1;
		return view('accomodation.accomodation_detail', compact('provider', 'code', 'data', 'data_attr', 'page_will_expire'));
	}

	public function eroam_activity_view_more($id) {
		$activity = http('get', 'eroam/activity/' . $id);

		if (count($activity['image']) > 0) {
			foreach ($activity['image'] as $key => $value) {
				$activity['image'][$key]['small'] = $this->cms_url . $value['small'];
				$activity['image'][$key]['medium'] = $this->cms_url . $value['medium'];
				$activity['image'][$key]['large'] = $this->cms_url . $value['large'];
				$activity['image'][$key]['original'] = $this->cms_url . $value['original'];
				$activity['image'][$key]['thumbnail'] = $this->cms_url . $value['thumbnail'];

				if ($value['is_primary'] == 1) {
					$activity['primary'] = $activity['image'][$key]['small'];
				}
			}
		}
		$page_will_expire = 1;
		return view('pages.view-more-activity', compact('activity', 'page_will_expire'));

	}

	public function viator_activity_view_more($code) {

		$data['code'] = $code;
		$data['currencyCode'] = 'AUD';
		$activity = http('post', 'search/product', $data);
		$page_will_expire = 1;
		return view('pages.view-more-viator-activity', compact('activity', 'page_will_expire'));

	}

	public function view_more_activity_details() {
		$provider = request()->input('provider');
		$code = request()->input('code');
		$select = request()->input('select');
		$cityid = request()->input('cityid');
		$leg = request()->input('leg');
		$startDate = session()->get('search')['itinerary'][$leg]['city']['date_from'];
		$endDate = session()->get('search')['itinerary'][$leg]['city']['date_to'];

		$data = [];
		$data['activity_name'] = '';
		$data['city_name'] = '';
		$data['activity_description'] = '';
		$data['primary_photo'] = '';
		$data['images'] = [];
		$data['address'] = '';
		$data['telephone'] = '';
		$data['google_map_location'] = '';
		$data['price'] = '';
		$data['code'] = '';
		$data['duration'] = '';

		$data['highlights'] = '';
		$data['itinerary'] = '';
		$data['description'] = '';

		$data['inclusions'] = '';
		$data['exclusions'] = '';
		$data['additionalInfo'] = '';
		$data['voucherRequirements'] = '';
		$data['departurePoint'] = '';
		$data['departureTime'] = '';
		$data['departureTimeComments'] = '';
		$data['returnDetails'] = '';
		$data['provider'] = $provider;

		switch ($provider) {
		case 'eroam':
			$response = http('get', 'eroam/activity/' . $code);
			$data['activity_name'] = $response['name'];
			$data['city_name'] = $response['city']['name'];
			$data['activity_description'] = $response['description'];
			$data['address'] = $response['address_1'];
			$data['telephone'] = $response['reception_phone'];
			$data['google_map_location'] = $response['name'] . ', ' . $response['city']['name'] . ', ' . $response['city']['country']['name'];
			$data['photos']['default'] = [];

			$data['price'] = round($response['activity_price'][0]['price']);
			$data['code'] = $response['id'];
			$data['duration'] = $response['duration'] . ' Day';
			$duration = $response['duration'];

			// IMAGES
			if ($response['image']) {
				foreach ($response['image'] as $key => $value) {
					if ($value['is_primary'] == 1) {
						$data['primary_photo'] = $this->cms_url . $value['small'];
					}
					$data['images'][] = $this->cms_url . $value['small'];
				}
			}
			break;
		case 'viator':
			$response = http('post', 'search/product', ['code' => $code]);
			$data['activity_name'] = $response['shortTitle'];
			$data['city_name'] = $response['city'];
			$data['activity_description'] = $response['shortDescription'];
			$data['primary_photo'] = $response['thumbnailHiResURL'];
			$data['photos']['default'] = [];
			$data['address'] = $response['location'];
			$data['google_map_location'] = $response['location'];

			$data['price'] = $response['price'];
			$data['code'] = $response['code'];
			$data['duration'] = $response['duration'];
			$duration = $response['duration'];
			$data['images'][] = $response['thumbnailHiResURL'];

			$data['highlights'] = $response['highlights'];
			$data['description'] = $response['description'];
			$data['itinerary'] = $response['itinerary'];

			$data['inclusions'] = $response['inclusions'];
			$data['exclusions'] = $response['exclusions'];
			$data['additionalInfo'] = $response['additionalInfo'];
			$data['voucherRequirements'] = $response['voucherRequirements'];
			$data['departurePoint'] = $response['departurePoint'];
			$data['departureTime'] = $response['departureTime'];
			$data['departureTimeComments'] = $response['departureTimeComments'];
			$data['returnDetails'] = $response['returnDetails'];

			if ($response['productPhotos'][0]) {
				foreach ($response['productPhotos'] as $product_photo) {
					$data['photos']['default'][] = $product_photo['photoURL'];
					$data['images'][] = $product_photo['photoURL'];
				}
			}
			if ($response['userPhotos']) {
				foreach ($response['userPhotos'] as $user_photo) {
					$data['images'][] = $user_photo['photoURL'];
				}
			}
			break;
		}

		$data['select'] = $select;
		$data['cityid'] = $cityid;
		$data['startDate'] = $startDate;
		$data['endDate'] = $endDate;

		$data['attributes'] = '
		data-provider="' . $provider . '"
		data-city-id= "' . $cityid . '"
		data-index="' . $data['code'] . '"
		data-activity-id="' . $data['code'] . '"
		data-is-selected="' . $select . '"
		data-price="' . $data['price'] . '"
		data-name="' . $data['activity_name'] . '"
		data-currency="' . session()->get('search')['currency'] . '"
		data-description="' . substr($data['activity_description'], 0, 425) . '"
		data-duration="' . $duration . '"
		data-label=""
		data-startdate="' . $startDate . '"
		data-enddate="' . $endDate . '"';

		$page_will_expire = 1;
		return view('activity.view-more-activity-details')->with(compact('data', 'provider', 'code', 'page_will_expire'))->render();
	}

	public function get_cities_by_country_id() {
		$country_id = request()->input('country_id');

		if ($country_id) {
			return get_cities_by_country_id($country_id);
		}
	}

	public function about_us() {
		return view('pages.about-us');
	}

	public function email_proposed_itinerary() {
		if (session()->get('search_input') == null) {
			return redirect('/');
		}
		$today = date("F j, Y");
		$year = date('Y');
		$email = request()->input('emails');
		$name = request()->input('name');

		$search = session()->get('search');

		$pdf = PDF::loadView('pages.proposed-itinerary-pdf', compact('search', 'today', 'year'));
		$data = [];
		$data['name'] = $name;

		Mail::send('pages.email-itinerary', $data, function ($message) use ($email, $name, $pdf) {
			$message->to($email)->subject('eRoam-Itinerary');
			$message->from('res@eroam.com');
			$message->attachData($pdf->output(), "itinerary.pdf");
		});

		return "success";
	}

	public function privacy_policy() {
		return view('pages.privacy-policy');
	}
	
	public function affilliates() {
		return view('pages.affilliates');
	}
	
	public function terms() {
		return view('pages.terms');
	}
	public function sitemap() {
		return view('pages.sitemap');
	}

	public function contact_us() {
		return view('pages.contact-us');
	}
	
	public function send_contact_form(Request $request) {

		$name = $request->input('name');
		$email = $request->input('email');
		$message = $request->input('message');

		$validate = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required',
		]);

		if ($validate->fails()) {
			return redirect('contact-us')
				->withErrors($validate, 'contact')
				->withInput();
		} else {
			$mail_content = [
				'text' => $message,
				'email' => $email,
				'name' => $name,
			];
			Mail::send(['html' => 'mail.contact-us'], $mail_content, function ($message) {
				$message->to('info@eroam.com')->subject('Contact Us');
				$message->from('res@eroam.com');
			});

			return redirect('contact-us')
				->with('message', 'Message successfully sent.');
		}
	}
	
	public function enquiryForm(Request $request) {
		$first_name = $request->input('first_name');
		$family_name = $request->input('family_name');
		$email = $request->input('email');
		$phone = $request->input('phone');
		$travel_date = $request->input('travel_date');
		$comments = $request->input('comments');
		$product = $request->input('product');
		$postcode = $request->input('postcode');

		$validate = Validator::make($request->all(), [
			'first_name' => 'required',
			'email' => 'required|email',
			'family_name' => 'required',
			'postcode' => 'required',
		]);

		$mail_content = [
			'first_name' => $first_name,
			'family_name' => $family_name,
			'email' => $email,
			'phone' => $phone,
			'travel_date' => $travel_date,
			'comments' => $comments,
			'product' => $product,
			'postcode' => $postcode,
		];
		Mail::send(['html' => 'mail.enquiry'], $mail_content, function ($message) use ($product) {
			$message->to('res@eroam.com')->subject('Enquiry For ' . $product . '');
			$message->from('res@eroam.com');
		});
		if (count(Mail::failures()) > 0) {
			echo '2';
		} else {
			echo '1';
		}
		exit;
	}
	
	public function register() {
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		$ip_address = '137.59.252.196';

		$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip_address));
		$countryName = $query['country'];
		$cities = Cache::get('cities');
		$countries = Cache::get('countries');
		$labels = Cache::get('labels');
		$travellers = Cache::get('travellers');

		if (!session()->has('transport_types')) {
			session()->put('transport_types', $travellers['transport_types']);
		}
		$travel_pref = [];
		$interest_ids = [];
		if (session()->has('travel_preferences')) {
			$travel_pref = session()->get('travel_preferences');
			$travel_pref = reset($travel_pref);
			$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
		}

		return view(
			'user.login',
			[
				'travelers' => $travellers,
				'travel_pref' => $travel_pref,
				'interest_ids' => $interest_ids,
				'countryName' => $countryName,
				'default_currency' => 'AUD',
				'is_register' => '1',
			]
		);

	}

	public function send_registration(Request $request) {
		$first_name = $request->input('first_name');
		$last_name = $request->input('last_name');
		$email = $request->input('email');
		$password = $request->input('password');
		$currency = $request->input('currency');
		$nationality = $request->input('nationality');
		$gender = $request->input('gender');
		$age = $request->input('age_group');
		$data = [];
		$data['first_name'] = $first_name;
		$data['last_name'] = $last_name;
		$data['email'] = $email;
		$data['password'] = $password;
		$data['currency'] = $currency;
		$data['pref_nationality_id'] = $nationality;
		$data['pref_gender'] = $gender;
		$data['pref_age_group_id'] = $age;
		$data['url'] = url('');

		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url(''),
		];
		$response = http('post', 'user/create-customer', $data, $headers);
	
		if ($response['successful']) {
			return redirect('login')
				->with('register-success', $response['message']);
		} else {
			return redirect('register')
				->with('register_error', $response['message'])
				->withInput();
		}
	}
	
	public function remove_itinerary(Request $request) {
		$itineraryNumber = $request->itineraryNumber;
		$itineraryType = $request->itineraryType;
		$activityNumber = $request->activityNumber;
		$data = session()->get('search');
		if ($itineraryType == 'hotel' || $itineraryType == 'transport'):
			$data['itinerary'][$request->itineraryNumber][$request->itineraryType] = "";
		elseif ($itineraryType == 'activities'):
			unset($data['itinerary'][$request->itineraryNumber][$request->itineraryType][$activityNumber]);
		endif;
		session()->put('search', $data);
		self::recalculate_totle_price(session()->get('search'));
		return redirect('payment-summary/');
	}

	public function recalculate_totle_price($data) {
		if (isset($data['itinerary'])):
			$this->session->set_search($data['itinerary'], ['date_from' => date('Y-m-d', strtotime($data['travel_date'])), 'traveller_number' => $data['travellers'], 'pax_information' => $data['pax_information'], 'child_number' => $data['child_total'], 'rooms' => $data['rooms']]);
		endif;
	}

	public function book_itinerary_signin_guest_checkout() {
		if (session()->get('user_auth')['id']):
			return redirect('/review-itinerary/');
		else:
			$data = session()->get('search');
			$priceCheck = $this->expediaApi->expediaPriceCheck();
			$totalCost = str_replace(',', '', $data['cost_per_person']);
			$travellers = $data['travellers'];
			$currency = $data['currency'];
			$totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
			$countries = Cache::get('countries');
			$startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
			$endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));

			return view('itinenary.partials.guest-checkout')->with(compact('priceCheck', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays'));
		endif;
	}
	
	public function payment_summary_view() {

		$data = session()->get('search');

		$priceCheck = $this->expediaApi->expediaPriceCheck(); //Call to expdia PriceCheck function
		$totalCost = str_replace(',', '', $data['cost_per_person']);
		$travellers = $data['travellers'];
		$currency = $data['currency'];
		$totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
		$countries = Cache::get('countries');
		$startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
		$endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));
		if (session()->has('search')) {
			$itinerary = json_decode(json_encode(session()->get('search')['itinerary']), false);
			$leg_detail = session()->get('search');

			foreach ($leg_detail['itinerary'] as $key1 => $itinerary) {
				foreach ($itinerary as $key => $itinerary_data) {
					if (!empty($itinerary_data)) {
						if ($key == 'transport') {
							if (!empty($itinerary_data)) {

								if (isset($itinerary_data['provider'])) {
									if ($itinerary_data['provider'] == 'busbud') {
										$booking_data = array();
										$booking_data['travellers'] = $data['travellers'];
										$booking_data['departureId'] = $itinerary_data['id'];
										$booking_data['departuredate'] = $itinerary_data['departuredate'];
										$booking_data['destinationlocationcode'] = $itinerary_data['destinationlocationcode'];
										$booking_data['originlocationcode'] = $itinerary_data['originlocationcode'];
										$headers = [
											'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
											'Origin' => url(''),
										];

										$response = http('post', 'createBusBudCart', $booking_data, $headers);
										if ($response['result'] == 'Yes') {
											$leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'Yes';
											$leg_detail['itinerary'][$key1]['transport']['cartId'] = $response['cartId'];
											$leg_detail['itinerary'][$key1]['transport']['passangerArray'] = $response['passengers'];
											$leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
										} else {
											$leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'No';
											$leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
										}

									}

								}
							}

						}
					}
				}

			}
			session()->put('search', $leg_detail);
			return view('itinenary.partials.payment-summary')->with(compact('priceCheck', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays'));
		} else {
			return redirect('/');
		}
	}
	
	public function review_itinerary_view() {
		$data = session()->get('search');

		$search_input = session()->get('search_input');
		$data['search_input'] = $search_input;

		$UserController = new UserController;
		$user = $UserController->get_user_detail();
		$data['user'] = $user;

		$totalCost = str_replace(',', '', $data['cost_per_person']);
		$travellers = $data['travellers'];
		$currency = $data['currency'];
		$totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
		$countries = Cache::get('countries');
		$startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
		$endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));

		if (session()->has('search')) {
		    $itinerary = json_decode(json_encode(session()->get('search')['itinerary']), false);
		    $leg_detail = session()->get('search');

		    foreach ($leg_detail['itinerary'] as $key1 => $itinerary) {
		        foreach ($itinerary as $key => $itinerary_data) {
		            if (!empty($itinerary_data)) {
		                if ($key == 'transport') {
		                    if (!empty($itinerary_data)) {

		                        if (isset($itinerary_data['provider'])) {
		                            if ($itinerary_data['provider'] == 'busbud') {
		                                $booking_data = array();
		                                $booking_data['travellers'] = $data['travellers'];
		                                $booking_data['departureId'] = $itinerary_data['id'];
		                                $booking_data['departuredate'] = $itinerary_data['departuredate'];
		                                $booking_data['destinationlocationcode'] = $itinerary_data['destinationlocationcode'];
		                                $booking_data['originlocationcode'] = $itinerary_data['originlocationcode'];
		                                $headers = [
		                                    'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
		                                    'Origin' => url(''),
		                                ];

		                                $response = http('post', 'createBusBudCart', $booking_data, $headers);
		                                if ($response['result'] == 'Yes') {
		                                    $leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'Yes';
		                                    $leg_detail['itinerary'][$key1]['transport']['cartId'] = $response['cartId'];
		                                    $leg_detail['itinerary'][$key1]['transport']['passangerArray'] = $response['passengers'];
		                                    $leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
		                                } else {
		                                    $leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'No';
		                                    $leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
		                                }

		                            }

		                        }
		                    }

		                }
		            }
		        }

		    }

		    session()->put('search', $leg_detail);
		    return view('itinenary.partials.review-itinerary')->with(compact('data','priceCheck', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays'));
		} else {
		    return redirect('/');
		}

	}

	public function test() {
		return view('pages.test');
	}

	public function updatePreferences() {
		$accommodation_options = '';
		$room_type_options = '';
		$transport_type_options = '';
		$nationality = '';
		$gender = '';
		$age_group = '';
		$interestoption = '';

		$labels = Cache::get('labels');
		$travellers = Cache::get('travellers');

		$travel_pref = session()->get('travel_preferences');
		$travel_pref = reset($travel_pref);
		$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];

		if (count($travellers['categories']) > 0) {
			foreach ($travellers['categories'] as $category) {
				if (empty($category['name'])) {
					continue;
				}
				if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
					$accommodation_options .= '<input type="hidden" name="hotel_category_id[]"  value="' . $category['id'] . '" >';
				}
			}
		}

		if (count($travellers['room_types']) > 0) {
			foreach ($travellers['room_types'] as $room_type) {
				if (empty($room_type['name'])) {
					continue;
				}
				if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
					$room_type_options .= '<input type="hidden" name="room_type_id[]" value="' . $room_type['id'] . '" >';
				}
			}
		}

		if (count($travellers['transport_types']) > 0) {
			foreach ($travellers['transport_types'] as $transport_type) {
				if (empty($transport_type['name'])) {
					continue;
				}
				if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
					$transport_type_options .= '<input type="hidden" name="transport_types[]" value="' . $transport_type['id'] . '" >';
				}
			}
		}

		if (count($travellers['nationalities']) > 0) {
			foreach ($travellers['nationalities']['featured'] as $featured) {
				if (empty($featured['name'])) {
					continue;
				}
				if (isset($travel_pref['nationality']) && ($featured['name'] == $travel_pref['nationality'])) {
					$nationality .= '<input type="hidden" name="nationality" value="' . $featured['id'] . '" > ';
				}
			}

			foreach ($travellers['nationalities']['not_featured'] as $not_featured) {
				if (empty($not_featured['name'])) {
					continue;
				}
				if (isset($travel_pref['nationality']) && ($not_featured['name'] == $travel_pref['nationality'])) {
					$nationality .= '<input type="hidden" name="nationality" value="' . $not_featured['id'] . '" >';
				}
			}
		}

		$gender_array = ['male', 'female', 'other'];
		foreach ($gender_array as $gen) {
			if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender'])) {
				$gender .= '<input type="hidden" name="gender" value="' . $gen . '">';
			}
		}

		if (count($travellers['age_groups']) > 0) {
			foreach ($travellers['age_groups'] as $age) {
				if (empty($age['name'])) {
					continue;
				}
				if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group'])) {
					$age_group .= '<input type="hidden" name="age_group" value="' . $age['id'] . '" >';
				}
			}
		}

		if (count($labels) > 0) {
			foreach ($labels as $i => $label) {
				if (empty($age['name'])) {
					continue;
				}

				$sequence = '';
				if (in_array($label['id'], $interest_ids)) {
					$sequence = array_search($label['id'], $interest_ids);
					$sequence += 1;
					$interestoption .= '<input type="hidden" name="interests[' . $sequence . ']" value="' . $label['id'] . '">';
				}
			}
		}

		return $accommodation_options . $room_type_options . $transport_type_options . $nationality . $gender . $age_group . $interestoption;
	}

	public function convertCurrency($price, $user_currency, $std_currency, $currency) {
		$priceConvertedToAUD = floatval($price) / floatval($currency[$std_currency]);

		$priceConvertedToSelectedPrice = 0.00;
		if (array_key_exists($user_currency, $currency)) {
			$currency_amount = $currency[$user_currency];
			$priceConvertedToSelectedPrice = $priceConvertedToAUD * floatval($currency_amount);
		}
		return number_format($priceConvertedToSelectedPrice, 2);

	}
	
	public function payment(Request $request) {
		require base_path() . '/vendor/autoload.php';
		require_once app_path() . '\libraries\eway-rapid-php-master\include_eway.php';
        //generate random Invoice Number
        $digits_needed = 8;
        $random_number = ''; // set up a blank string
        $count = 0;
        while ($count < $digits_needed) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        $invoiceNumber = 'eRoam' . $random_number;
        //Payment configuration start here
        $apiKey = 'F9802Cyu/LaPEuVH6DayevjOq0xvO2Ppyf/qGM60sSJGOBiTLz2NZvK+D5ZpVV1eaFCxWY';
        $apiPassword = '9NAW7KKQ';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $name = request()->input()['billing_first_name'] . ' ' . request()->input()['billing_last_name'];
        $find = array(",", ".");
        $totalAmount = str_replace($find, '', request()->input()['totalAmount']);

        $response = (object) ['TransactionStatus' => true, 'TransactionID' => '1234567890'];

        if ($response->TransactionStatus) {
            $data = [];
            $leg_detail = [];

            if (session()->get('search')) {
                $leg_detail = session()->get('search');
            } else {
                $leg_detail = '';
            }
            if (!session()->get('user_auth')['id']) {
                $data['user_id'] = "";
            } else {
                $data['user_id'] = session()->get('user_auth')['user_id'];
            }
            $data['invoiceNumber'] = $invoiceNumber;
            $data['travel_date'] = $leg_detail['travel_date'];
            $data['travellers'] = $leg_detail['travellers'];
            $data['cost_per_day'] = $leg_detail['cost_per_day'];
            $data['from_date'] = $leg_detail['itinerary'][0]['city']['date_from'];
            $data['to_date'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['date_to'];
            $data['total_itenary_legs'] = count($leg_detail['itinerary']);
            $data['total_amount'] = $request->input()['totalAmount'];
            $data['total_days'] = $leg_detail['total_number_of_days'];
            $data['total_per_person'] = $leg_detail['cost_per_person'];
            $data['evay_transcation_id'] = $response->TransactionID;
            $data['card_number'] = substr($request->input()['card_number'], -4);
            $data['expiry_month'] = $request->input()['month'];
            $data['currency'] = $request->input()['currency'];
            $data['expiry_year'] = $request->input()['year'];
            $data['cvv'] = $request->input()['cvv'];
            $data['from_city_id'] = $leg_detail['itinerary'][0]['city']['id'];
            $data['to_city_id'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['id'];
            $data['from_city_name'] = $leg_detail['itinerary'][0]['city']['name'];
            $data['to_city_name'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['name'];
            $data['voucher'] = 'Not Sent';
            $data['status'] = 'Pending';

            $from_city_to_city = '';

            foreach (session()->get('search')['itinerary'] as $key => $leg) {
                $from_city_to_city = $from_city_to_city . "," . $leg['city']['name'];
            }

            $data['from_city_to_city'] = ltrim($from_city_to_city, ',');

            $i = 0;
            $priceCheck = $this->expediaApi->expediaPriceCheck(); //Call to expdia PriceCheck function
            foreach ($leg_detail['itinerary'] as $key => $itinerary) {
                $data['leg'][$i]['from_city_id'] = $itinerary['city']['id'];
                $data['leg'][$i]['to_city_id'] = $itinerary['city']['id'];
                $data['leg'][$i]['from_date'] = $itinerary['city']['date_from'];
                $data['leg'][$i]['to_date'] = $itinerary['city']['date_to'];
                $data['leg'][$i]['country_code'] = $itinerary['city']['country']['code'];
                $data['leg'][$i]['from_city_name'] = $itinerary['city']['name'];
                $data['leg'][$i]['to_city_name'] = $leg_detail['itinerary'][count($leg_detail['itinerary']) - 1]['city']['name'];

                foreach ($itinerary as $key => $itinerary_data) {
                    if (!empty($itinerary_data)) {
                        if ($key == 'hotel' && $itinerary_data['provider'] == 'expedia') {
                            $itinerary_data = json_decode(json_encode($itinerary_data), true);
                            $data['leg'][$i]['hotel'][0]['leg_id'] = '';
                            $data['leg'][$i]['hotel'][0]['leg_type'] = 'hotel';
                            $data['leg'][$i]['hotel'][0]['leg_type_name'] = 'Accommodation';
                            $data['leg'][$i]['hotel'][0]['nights'] = $itinerary_data['nights'];
                            $data['leg'][$i]['hotel'][0]['checkin'] = $itinerary_data['checkin'];
                            $data['leg'][$i]['hotel'][0]['checkout'] = $itinerary_data['checkout'];
                            $data['leg'][$i]['hotel'][0]['hotel_id'] = $itinerary_data['hotelId'];
                            $data['leg'][$i]['hotel'][0]['leg_name'] = $itinerary_data['name'];
                            $data['leg'][$i]['hotel'][0]['provider'] = $itinerary_data['provider'];

                            $roomTypeId = '@roomTypeId';
                            $roomCode = '@roomCode';
                            $currencyCode = '@currencyCode';
                            $singleRate = '@nightlyRateTotal';
                            $total = '@total';

                            $data['leg'][$i]['hotel'][0]['hotel_room_type_id'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomTypeId];
                            $data['leg'][$i]['hotel'][0]['hotel_room_type_name'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateDescription'];
                            $data['leg'][$i]['hotel'][0]['hotel_room_code'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
                            $data['leg'][$i]['hotel'][0]['hotel_rate_code'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateCode'];

                            $Rate = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
                            $taxes = 0;
                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])) {
                                $taxes = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                            }
                            $data['leg'][$i]['hotel'][0]['hotel_price_per_night'] = $Rate;
                            $data['leg'][$i]['hotel'][0]['hotel_tax'] = $taxes;
                            $data['leg'][$i]['hotel'][0]['hotel_expedia_subtotal'] = $Rate * $itinerary_data['nights'];
                            $data['leg'][$i]['hotel'][0]['hotel_expedia_total'] = $Rate * $itinerary_data['nights'] + $taxes;

                            $Rate = round(($Rate * $this->eroamPercentage) / 100 + $Rate, 2);
                            $subTotal = $itinerary_data['nights'] * $Rate;

                            $data['leg'][$i]['hotel'][0]['hotel_eroam_subtotal'] = $subTotal;
                            $data['leg'][$i]['hotel'][0]['price'] = $subTotal + $taxes;
                            $data['leg'][$i]['hotel'][0]['currency'] = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$currencyCode];
                            $data['leg'][$i]['hotel'][0]['booking_id'] = '';
                            $data['leg'][$i]['hotel'][0]['provider_booking_status'] = '';
                            $data['leg'][$i]['hotel'][0]['booking_error_code'] = '';
                            $passengers_info = $request->input();

                            $hotelId = $itinerary_data['hotelId'];
                            $arrivalDate = date('m-d-Y', strtotime($itinerary_data['checkin']));
                            $departureDate = date('m-d-Y', strtotime($itinerary_data['checkout']));

                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['0']['rateKey'])) {
                                $rateKey = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['0']['rateKey'];
                            } else {
                                $rateKey = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'];
                            }
                            $roomTypeCode = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
                            $rateCode = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
                            $chargeableRate = $data['leg'][$i]['hotel'][0]['hotel_expedia_total'];
                            $id = '@id';
                            if (isset($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'][0])) {
                                $bedtypes = '';
                                foreach ($itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'] as $BedType) {
                                    $bedtypes .= $BedType[$id] . ',';
                                }
                                $bedtypes = rtrim($bedtypes, ',');
                            } else {
                                $bedtypes = $itinerary_data['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'][$id];
                            }
                            $customerSessionId = $itinerary_data['customerSessionId'];
                            $return_data = $this->expediaApi->expediaBooking($hotelId, $arrivalDate, $departureDate, $rateKey, $roomTypeCode, $rateCode, $chargeableRate, $passengers_info, $bedtypes, $customerSessionId);
                            $data['leg'][$i]['hotel'][0]['booking_id'] = $return_data['itineraryId'];
                            $data['leg'][$i]['hotel'][0]['provider_booking_status'] = $return_data['reservationStatusCode'];
                            $data['leg'][$i]['hotel'][0]['hotel_total_room'] = $return_data['numberOfRoomsBooked'];
                            $data['leg'][$i]['hotel'][0]['booking_error_code'] = '';

                        }
                        if ($key == 'activities') {
                            $j = 0;
                            foreach ($itinerary_data as $key1 => $activities) {
                                $data['leg'][$i]['activities'][$j]['leg_type'] = 'activities';
                                $data['leg'][$i]['activities'][$j]['leg_id'] = '';
                                $data['leg'][$i]['activities'][$j]['leg_name'] = (isset($activities['name']) ? $activities['name'] : '');
                                $data['leg'][$i]['activities'][$j]['from_date'] = (isset($activities['date_selected']) ? $activities['date_selected'] : '');
                                $data['leg'][$i]['activities'][$j]['to_date'] = (isset($activities['date_selected']) ? $activities['date_selected'] : '');
                                $data['leg'][$i]['activities'][$j]['currency'] = (isset($activities['activity_price']['0']['currency']['code']) ? $activities['activity_price']['0']['currency']['code'] : '');
                                $currencyCode = (isset($activities['activity_price']['0']['currency']['code']) ? $activities['activity_price']['0']['currency']['code'] : '');
                                if (empty($data['leg'][$i]['activities'][$j]['currency'])) {
                                    if (isset($activities['currency']) && !empty($activities['currency'])) {
                                        $currencyCode = $activities['currency'];
                                        $data['leg'][$i]['activities'][$j]['currency'] = $currencyCode;
                                    }
                                }
                                $data['leg'][$i]['activities'][$j]['booking_id'] = '';
                                $data['leg'][$i]['activities'][$j]['provider_booking_status'] = '';
                                $data['leg'][$i]['activities'][$j]['booking_error_code'] = '';
                                $data['leg'][$i]['activities'][$j]['leg_type_name'] = '';
                                $data['leg'][$i]['activities'][$j]['voucher_key'] = '';
                                $data['leg'][$i]['activities'][$j]['voucher_url'] = '';
                                if (isset($activities['provider'])) {
                                    $data['leg'][$i]['activities'][$j]['provider'] = $activities['provider'];
                                } else {
                                    $data['leg'][$i]['activities'][$j]['provider'] = '';
                                }
                                if (isset($activities['activity_price']['0']['supplier']['id'])) {
                                    $data['leg'][$i]['activities'][$j]['supplier_id'] = $activities['activity_price']['0']['supplier']['id'];
                                } else {
                                    $data['leg'][$i]['activities'][$j]['supplier_id'] = '';
                                }
                                if (isset($activities['price']['0']['price'])) {
                                    $data['leg'][$i]['activities'][$j]['price'] = number_format($activities['price']['0']['price'], 2);
                                } else {
                                    $data['leg'][$i]['activities'][$j]['price'] = (isset($activities['activity_price']['0']['price']) ? number_format($activities['activity_price']['0']['price'], 2) : '');
                                }
                                $j++;
                            }
                        }
                        if ($key == 'transport') {
                            if (!empty($itinerary_data)) {
                                $data['leg'][$i]['transport'][0]['leg_type'] = 'transport';
                                $data['leg'][$i]['transport'][0]['leg_id'] = '';
                                $data['leg'][$i]['transport'][0]['leg_name'] = (isset($itinerary_data['transport_type']['name']) ? $itinerary_data['transport_type']['name'] : '');
                                $data['leg'][$i]['transport'][0]['from_city_id'] = (isset($itinerary_data['from_city']['id']) ? $itinerary_data['from_city']['id'] : '');
                                $data['leg'][$i]['transport'][0]['from_city_name'] = (isset($itinerary_data['from_city']['name']) ? $itinerary_data['from_city']['name'] : '');
                                $data['leg'][$i]['transport'][0]['to_city_id'] = (isset($itinerary_data['to_city']['id']) ? $itinerary_data['to_city']['id'] : '');
                                $data['leg'][$i]['transport'][0]['to_city_name'] = (isset($itinerary_data['to_city']['name']) ? $itinerary_data['to_city']['name'] : '');
                                $data['leg'][$i]['transport'][0]['price'] = (isset($itinerary_data['price']['0']['price']) ? number_format($itinerary_data['price']['0']['price'], 2) : '');
                                $data['leg'][$i]['transport'][0]['currency'] = (isset($itinerary_data['price']['0']['currency']['code']) ? $itinerary_data['price']['0']['currency']['code'] : '');
                                $data['leg'][$i]['transport'][0]['departure_text'] = (isset($itinerary_data['departure_text']) ? $itinerary_data['departure_text'] : '');
                                $data['leg'][$i]['transport'][0]['arrival_text'] = (isset($itinerary_data['arrival_text']) ? $itinerary_data['arrival_text'] : '');
                                $data['leg'][$i]['transport'][0]['booking_summary_text'] = (isset($itinerary_data['booking_summary_text']) ? $itinerary_data['booking_summary_text'] : '');
                                $data['leg'][$i]['transport'][0]['duration'] = (isset($itinerary_data['duration']) ? $itinerary_data['duration'] : '');
                                $data['leg'][$i]['transport'][0]['booking_id'] = '';
                                $data['leg'][$i]['transport'][0]['provider_booking_status'] = '';
                                $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                $data['leg'][$i]['transport'][0]['leg_type_name'] = '';

                                if (isset($itinerary_data['provider'])) {
                                    $data['leg'][$i]['transport'][0]['provider'] = $itinerary_data['provider'];
                                    //code for mystifly booking
                                    if ($itinerary_data['provider'] == 'mystifly') {
                                        $booking_data = array();
                                        $booking_data['FareSourceCode'] = $itinerary_data['fare_source_code'];
                                        $post_data = $request->input();
                                        for ($t = 0; $t < count($post_data['passenger_first_name']); $t++) {
                                            if ($t == 0) {
                                                $booking_data['passenger_contact_no'] = $post_data['passenger_contact_no'][0];
                                                $booking_data['passenger_email'] = $post_data['passenger_email'][0];
                                            }

                                            //$booking_data['passenger_title'][$t] = strtoupper($post_data['passenger_title'][$t]);
                                            $booking_data['passenger_first_name'][$t] = $post_data['passenger_first_name'][$t];
                                            $booking_data['passenger_last_name'][$t] = $post_data['passenger_last_name'][$t];
                                            //$booking_data['passenger_gender'][$t] = $post_data['passenger_gender'][$t][0];
                                            $booking_data['passenger_dob'][$t] = date('Y-m-d', strtotime($post_data['passenger_dob'][$t])) . 'T00:00:00';
                                            $booking_data['passenger_country'][$t] = $this->getCountryCode($post_data['passenger_country'][0]);
                                            $booking_data['passenger_type'][$t] = "ADT";
                                            $passport_date = '2010-10-10';
                                            $booking_data['passenger_passport_expiry_date'][$t] = date('Y-m-d', strtotime($passport_date)) . 'T00:00:00';
                                            $booking_data['passenger_passport_number'][$t] = 'AFK13246';
                                            $booking_data['passenger_meal_preference'][$t] = "Any";
                                            $booking_data['passenger_seat_preference'][$t] = "Any";
                                        }
                                        $headers = [
                                            'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                                            'Origin' => url(''),
                                        ];

                                        $response = http('post', 'testMystifly', $booking_data, $headers);
                                        if ($response['BookFlightResult']['Status'] == 'CONFIRMED' || $response['BookFlightResult']['Status'] == 'PENDING') {

                                            $data['leg'][$i]['transport'][0]['booking_id'] = $response['BookFlightResult']['UniqueID'];
                                            $data['leg'][$i]['transport'][0]['provider_booking_status'] = $response['BookFlightResult']['Status'];
                                            $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                            $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');

                                        } else {
                                            if (isset($response['BookFlightResult']['Errors']['Error'][0]['Message'])) {

                                                $data['leg'][$i]['transport'][0]['provider_booking_status'] = $response['BookFlightResult']['Errors']['Error'][0]['Message'];
                                                $data['leg'][$i]['transport'][0]['booking_error_code'] = $response['BookFlightResult']['Errors']['Error'][0]['Code'];
                                                $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');

                                            } elseif (isset($response['BookFlightResult']['Errors']['Error']['Message'])) {

                                                $data['leg'][$i]['transport'][0]['provider_booking_status'] = $response['BookFlightResult']['Errors']['Error']['Message'];
                                                $data['leg'][$i]['transport'][0]['booking_error_code'] = $response['BookFlightResult']['Errors']['Error']['Code'];
                                                $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                            }
                                        }
                                    }
                                    if ($itinerary_data['provider'] == 'busbud') {
                                        $response = http('post', 'bookBusBud', $booking_data, $headers);
                                        if ($response['result'] == 'Yes') {
                                            $data['leg'][$i]['transport'][0]['booking_id'] = $response['data']['purchase_id'];
                                            $data['leg'][$i]['transport'][0]['provider_booking_status'] = 'CONFIRMED';
                                            $data['leg'][$i]['transport'][0]['booking_error_code'] = '';
                                            $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                        } else {
                                            $data['leg'][$i]['transport'][0]['provider_booking_status'] = 'NOT CONFIRMED';
                                            $data['leg'][$i]['transport'][0]['booking_error_code'] = $response['data']['details'];
                                            $data['leg'][$i]['transport'][0]['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                        }

                                    }
                                } else {
                                    $data['leg'][$i]['transport'][0]['provider'] = '';
                                }

                                if (isset($itinerary_data['supplier']['id'])) {
                                    $data['leg'][$i]['transport'][0]['supplier_id'] = $itinerary_data['supplier']['id'];
                                } else {
                                    $data['leg'][$i]['transport'][0]['supplier_id'] = '';
                                }
                            } else {
                                $data['leg'][$i]['transport'] = [];
                            }

                        }

                    }
                }
                $i++;
            }

            $post_data = $request->input();
            $data['totalAmount'] = $post_data['totalAmount'];
            $data['passenger_info'] = [];
            for ($i = 0; $i < count($post_data['passenger_first_name']); $i++) {
                $data['passenger_info'][$i]['passenger_first_name'] = $post_data['passenger_first_name'][$i];
                $data['passenger_info'][$i]['passenger_last_name'] = $post_data['passenger_last_name'][$i];
                $data['passenger_info'][$i]['passenger_dob'] = $post_data['passenger_dob'][$i];
                $data['passenger_info'][$i]['passenger_contact_no'] = $post_data['passenger_contact_no'][$i];
                $data['passenger_info'][$i]['passenger_email'] = $post_data['passenger_email'][$i];
            }

            $data['billing_info']['passenger_first_name'] = $post_data['billing_first_name'];
            $data['billing_info']['passenger_last_name'] = $post_data['billing_last_name'];
            $data['billing_info']['passenger_dob'] = $post_data['billing_passenger_dob'];
            $data['billing_info']['passenger_email'] = $post_data['billing_email'];
            $data['billing_info']['passenger_zip'] = $post_data['passenger_zip'];
            $data['billing_info']['passenger_contact_no'] = $post_data['billing_contact'];
            $data['billing_info']['passenger_state'] = $post_data['passenger_state'];
            $data['billing_info']['passenger_suburb'] = $post_data['passenger_suburb'];
            $data['billing_info']['passenger_address_one'] = $post_data['passenger_address_one'];
            $data['billing_info']['passenger_address_two'] = $post_data['passenger_address_two'];
            $data['billing_info']['passenger_company_name'] = $post_data['passenger_company_name'];
            $data['billing_info']['passenger_country'] = $post_data['passenger_country'];


            $headers = [
                'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                'Origin' => url(''),
            ];
            $response = http('post', 'saveOrderDetail', $data, $headers);
            session()->put('orderData', $data);
            $customerEmail = $data['passenger_info'][0]['passenger_email'];
            
            // Mail::send(['html' => 'mail.book'], $data, function ($message) use ($customerEmail, $invoiceNumber) {
            //     $message->to($customerEmail)->subject('Booking summary for Invoice number - ' . $invoiceNumber);
            //     $message->from('res@eroam.com');
            // });
            session()->put('billing_data', $data);
            return redirect('/payment/');
            
        } else {
            return back()->with('error', 'Your Payment is declined. Please try once again.');
        }
        exit;

	}

	private function getCountryCode($country_id) {
		$countries = Cache::get('countries');
		$return = 'AU';
		if (isset($countries) && !empty($countries)) {
			foreach ($countries as $key => $value) {
				if (isset($value['countries']) && !empty($value['countries'])) {
					foreach ($value['countries'] as $key1 => $value1) {
						if ($value1['id'] == $country_id) {
							$return = $value1['code'];
						}
					}
				}
			}

		}
		return $return;
	}
	
	private function getCountryName($country_id) {
		$countries = Cache::get('countries');
		$return = 'AU';
		if (isset($countries) && !empty($countries)) {
			foreach ($countries as $key => $value) {
				if (isset($value['countries']) && !empty($value['countries'])) {
					foreach ($value['countries'] as $key1 => $value1) {
						if ($value1['id'] == $country_id) {
							$return = $value1['name'];
						}
					}
				}
			}

		}
		return $return;
	}

	public function order() {
        $data = session()->get('billing_data');
        return view(
            'itinenary.partials.success', ['return_response' => $data]
        );
    }
	
	public function ordersuccess() {
		return view('pages.success');
	}
	
	public function view_order_pdf(Request $request) {

		$post_data = $request->input();
		$return_response = json_decode($post_data['return_response'], true);
		$pdf = PDF::loadView('pages.order_pdf', compact('return_response'));
		return $pdf->stream();
	}
	
	public function tourDetails($city) {

		if ($city == 'bhutan') {
			return view('tours.bhutan');
		} elseif ($city == 2) {
			return view('tours.sydney');
		} elseif ($city == 3) {
			return view('tours.3');
		} elseif ($city == 4) {
			return view('tours.4');
		} elseif ($city == 5) {
			return view('tours.5');
		} elseif ($city == 6) {
			return view('tours.6');
		} elseif ($city == 7) {
			return view('tours.7');
		} elseif ($city == 8) {
			return view('tours.8');
		}

	}

	public function roomDetails() {
		$search = request()->input();

		$response = $this->expediaApi->hotelInfo(request()->input('hotel'));
		$response = json_decode($response, true);
		$thumbnailUrl = '';
		if (isset($response['data']['HotelInformationResponse']['HotelImages']['HotelImage'][0]['thumbnailUrl'])) {
			$thumbnailUrl = $response['data']['HotelInformationResponse']['HotelImages']['HotelImage'][0]['thumbnailUrl'];
		}

		$search_session = json_decode(json_encode(session()->get('search')), FALSE);
		$search_session = $search_session->itinerary[request()->input('leg')]->hotel;

		$response['data']['HotelInformationResponse']['HotelSummary']['provider'] = $search_session->provider;
		$response['data']['HotelInformationResponse']['HotelSummary']['customerSessionId'] = $search_session->customerSessionId;
		$response['data']['HotelInformationResponse']['HotelSummary']['cacheKey'] = $search_session->cacheKey;
		$response['data']['HotelInformationResponse']['HotelSummary']['cacheLocation'] = $search_session->cacheLocation;
		$response['data']['HotelInformationResponse']['HotelSummary']['nights'] = $search_session->nights;
		$response['data']['HotelInformationResponse']['HotelSummary']['checkin'] = $search_session->checkin;
		$response['data']['HotelInformationResponse']['HotelSummary']['checkout'] = $search_session->checkout;
		$response['data']['HotelInformationResponse']['HotelSummary']['thumbNailUrl'] = $thumbnailUrl;
		$response['data']['HotelInformationResponse']['HotelSummary']['RoomRateDetailsList']['RoomRateDetails'] = request()->input('room');
		$response = $response['data']['HotelInformationResponse']['HotelSummary'];
		echo json_encode($response);exit;
	}


	public function view_itinerary() {

        $data = json_decode(json_encode(session()->get('search')), true);
        //echo "<pre>";print_r($data);exit;
        $totalCost = str_replace(',', '', $data['cost_per_person']);
        $travellers = $data['travellers'];
        $currency = $data['currency'];
        $rooms = $data['rooms'];
        $totalDays = str_replace(' Nights', '', $data['total_number_of_days']);
        $countries = Cache::get('countries');
        $startDate = date('j M Y', strtotime(session()->get('search')['itinerary'][0]['city']['date_from']));
        $endDate = date('j M Y', strtotime(session()->get('search')['itinerary'][count(session()->get('search')['itinerary']) - 1]['city']['date_to']));
        $adults = $data['itinerary'][0]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['numberOfAdults'];
        $children = $data['itinerary'][0]['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['numberOfChildren'];
        //echo "<pre>";print_r(session()->get('search')); exit();
        if (session()->has('search')) {
            $itinerary = json_decode(json_encode(session()->get('search')['itinerary']), false);
            $leg_detail = session()->get('search');

            foreach ($leg_detail['itinerary'] as $key1 => $itinerary) {
                foreach ($itinerary as $key => $itinerary_data) {
                    if (!empty($itinerary_data)) {
                        if ($key == 'transport') {
                            if (!empty($itinerary_data)) {

                                if (isset($itinerary_data['provider'])) {
                                    if ($itinerary_data['provider'] == 'busbud') {

                                        $booking_data = array();
                                        $booking_data['travellers'] = $data['travellers'];
                                        $booking_data['departureId'] = $itinerary_data['id'];
                                        $booking_data['departuredate'] = $itinerary_data['departuredate'];
                                        $booking_data['destinationlocationcode'] = $itinerary_data['destinationlocationcode'];
                                        $booking_data['originlocationcode'] = $itinerary_data['originlocationcode'];
                                        $headers = [
                                            'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                                            'Origin' => url(''),
                                        ];

                                        $response = http('post', 'createBusBudCart', $booking_data, $headers);
                                        //$itinerary['transport'][0]['cartavailable'] = 'test';
                                        if ($response['result'] == 'Yes') {
                                            $leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'Yes';
                                            $leg_detail['itinerary'][$key1]['transport']['cartId'] = $response['cartId'];
                                            $leg_detail['itinerary'][$key1]['transport']['passangerArray'] = $response['passengers'];
                                            $leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                        } else {
                                            $leg_detail['itinerary'][$key1]['transport']['cartavailable'] = 'No';
                                            $leg_detail['itinerary'][$key1]['transport']['leg_type_name'] = (isset($itinerary_data['transport_name_text']) ? $itinerary_data['transport_name_text'] : '');
                                        }

                                    }

                                    //dd($itinerary_data);
                                }
                            }

                        }
                    }
                    // dd($itinerary);
                }

            }

            if (session()->get('search_input') == null) {
                return redirect('/');
            }

            //$search = session()->get('search');
            $page_will_expire = 1;
            $alternative_routes = array();
            return view('itinenary.partials.view-itinerary')->with(compact('data','page_will_expire','alternative_routes', 'itinerary', 'totalCost', 'travellers', 'currency', 'countries', 'startDate', 'endDate', 'totalDays','rooms', 'adults'));
        } else {
            return redirect('/');
        }
    }

	public function book_tour_signin_guest_checkout(Request $request)
	{
		$post_data 		= $request->input();
		$child_price 	= "";
		$infant_price 	= "";

		if(isset($post_data['childPrice']))
		{
			$child_price = $post_data['childPrice'];
		}
		if(isset($post_data['infantPrice']))
		{
			$infant_price= $post_data['infantPrice'];
		}

		
		$end 	= Carbon::parse($post_data['finishDate']);
		$now 	= Carbon::parse($post_data['start_date']);
		$days 	= $end->diffInDays($now);

		$payError = 0; 
		if(Session::has('error'))
		{
		  	$payError = 1;
		}
		$is_DateRadioIdValue = 0;

		if(Session::has('DateRadioIdValue'))
		{ 
		  	$is_DateRadioIdValue = Session::get('DateRadioIdValue');
		}

		$data 	= array(
				'start_date' 	=> $post_data['start_date'],
				'finishDate' 	=> $post_data['finishDate'],
				'singlePrice' 	=> $post_data['singlePrice'],
				'twinPrice' 	=> $post_data['price'],
				'tour_name' 	=> $post_data['tour_name'],
				'tour_id' 		=> $post_data['tour_id'],
				'start_date' 	=> $post_data['start_date'],
				'no_of_days' 	=> $post_data['no_of_days'],
				'durationType' 	=> $post_data['durationType'],
				'code' 			=> $post_data['code'],
				'provider'		=> $post_data['provider'],
				'from'			=> $post_data['from'],
				'childPrice'	=> $post_data['childPrice'],
				'infantPrice'	=> $post_data['infantPrice'],
				'days' 			=> $days,
				'payError' 		=> $payError,
				'is_DateRadioIdValue' => $is_DateRadioIdValue,
				'default_currency'  => 'AUD'
			);
		
		$first_name         = "";
		$last_name          = "";
		$email              = "";
		$contact_no         = "";
		$bill_country       = "";
		$bill_address_1     = "";
		$bill_address_2     = "";
		$user_id            = "";

		

		session()->put('tour_book_user',$data);

		if (session()->get('user_auth')['id']):			
			return redirect('book/tour/');
		else:			
			return view('pages.signin_guest_checkout_tour')->with(compact('data'));
		endif;
	}

	public function tourBookingForm()
	{

		$data = session()->get('tour_book_user');
		$user = [];
		$all_countries = get_all_countries();
		if(session()->get('user_auth')['id'])		
		{		
			$id = session()->get('user_auth')['id'];
			$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
			$user = json_decode(json_encode($user), false);			
		}

		$days               = $data['days'];
		
		$first_name         = "";
		$last_name          = "";
		$email              = "";
		$contact_no         = "";
		$bill_country       = "";
		$bill_address_1     = "";
		$bill_address_2     = "";
		$user_id            = "";

		if(session()->get('user_auth')['id'])
		{
			$first_name         = $user->customer->first_name;
			$last_name          = $user->customer->last_name;
			$email              = $user->customer->email;
			$contact_no         = $user->customer->contact_no;
			$bill_address_2     = $user->customer->bill_address_1;
			$bill_address_1     = $user->customer->bill_address_2;
			$bill_country       = $user->customer->bill_country;
			$user_id            = $user->id;
		}

		$data_arr 	= [			
				'first_name' 	=> $first_name,
				'last_name' 	=> $last_name,
				'email' 		=> $email,
				'contact_no' 	=> $contact_no,
				'bill_address_1'=> $bill_address_1,
				'bill_address_2'=> $bill_address_2,
				'bill_country' 	=> $bill_country,
				'user_id' 		=> $user_id,
			];		
		$payError = 0; 
		if(Session::has('error'))
		{
		  	$payError = 1;
		}
		$is_DateRadioIdValue = 0;

		if(session()->has('DateRadioIdValue'))
		{ 
		  	$is_DateRadioIdValue = session()->get('DateRadioIdValue');
		}

		return view('pages.tour_booking')->with(compact('data','user','all_countries','data_arr','payError','is_DateRadioIdValue','days'));
	}

	public function storeViatorTours()
	{
		
		$destinations 	 		= http('get', 'tourDestinations');
		$data 					= [];
		$data['topX'] 			= '1-5000';		
		$data['currencyCode'] 	= 'AUD';
		$data['catId'] 			= 0;
		$data['subCatId'] 		= 0;
		$data['dealsOnly'] 		= false;
		
		foreach ($destinations as $key => $dest)
		{
			
			$tours 				= [];
			$data['destId'] 	= $dest['destinationId'];		
			if($data['destId'] != 4391)
			{
				$alldata = [
					'data' => $data,
					'country_id' => $dest['country_id'],
					'city_id' => $dest['city_id'],
					'region_id' => $dest['region_id'],
					'country_name' => $dest['country_name']
				];	
				//echo "hi";exit;
				$activities 		= http('post', 'service/tours', $alldata);
			
				$tours 		 		= http_dev('post', 'service/tours', $alldata);
			}
		}		
	}

}
