$(document).ready(function() {
    customReady();
});

function customReady() {
    var totalLeg = $('#last').val();
    for (var i = 0; i <= totalLeg; i++) {
        $("#start_date_" + i).datepicker({
            format: 'd M yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: '+1d'
        }).on('changeDate', function(selected) {
            var selectDate = new Date(selected.date.valueOf());
            var leg = $(this).attr('data-leg');
            var city_id = $(this).attr('data-cityId');
            eroam.ajax('get', 'latest-search', {}, function(response) {
                if (response) {
                    search_session = JSON.parse(response);
                    var session = search_session.itinerary;

                    if (session.length > 0) {
                        if (session[leg].city.id == city_id) {
                            if (leg == 0) {
                                var from_date = selectDate.getFullYear() + '-' + ('0' + (selectDate.getMonth() + 1)).slice(-2) + '-' + ('0' + selectDate.getDate()).slice(-2);
                                search_session.travel_date = from_date;
                            } else {
                                var From_date = new Date($("#start_date_0").attr('data-fromdate'));
                                var To_date = new Date($("#start_date_" + leg).attr('data-fromdate'));
                                var diff_date = To_date - From_date;
                                var legDays = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
                                var from_date = deductDays(selectDate, legDays);
                                search_session.travel_date = from_date;
                            }

                            if (search_session.itinerary[leg].activities != null) {
                                var act = sortByDate(search_session.itinerary[leg].activities);
                                var From_date = new Date($("#start_date_" + leg).attr('data-fromdate'));

                                if (From_date > selectDate) {
                                    var diff_date1 = From_date - selectDate;
                                    var deduct = Math.floor(((diff_date1 % 31536000000) % 2628000000) / 86400000);
                                    var value = $("#start_date_" + leg).attr('data-nights')
                                    var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                    activityCancellation(activityDates, leg, act, value);
                                } else {
                                    bookingSummary.update(JSON.stringify(search_session));
                                }
                            } else {
                                bookingSummary.update(JSON.stringify(search_session));
                            }
                        }
                    }
                }
            });
        });
    };
    
    $('.booking-summary').slimScroll({
        height: '1000px',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    var page = $('#itinerary-page').val();

    if (page == '' || page == undefined) {
        $('#demo0').addClass('show');
        $('#demo0').prev('div').addClass('itinerary_left_active');
        $('#demo0').prev('div').find('.ic-up_down').addClass('ic-down_up').removeClass('ic-up_down');
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
}

$(function() {
    $("#departure_date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $("#return_date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $('.booking-summary').slimScroll({
        height: '1000px',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    $('body').on("change",".changeNights", function() {
        var leg = $(this).attr('data-leg');
        var value = $(this).val();
        var city_id = $(this).attr('data-cityId');
        var from_date = $(this).attr('data-fromDate');
        var to_date = $(this).attr('data-toDate');

        eroam.ajax('get', 'latest-search', {}, function(response) {
            if (response) {
                search_session = JSON.parse(response);
                var session = search_session.itinerary;
                if (session.length > 0) {
                    if (session[leg].city.id == city_id) {
                        var nights = search_session.itinerary[leg].city.default_nights;
                        if ((search_session.itinerary[leg].hotel != null) && typeof search_session.itinerary[leg].hotel.nights != 'undefined') {
                            nights = search_session.itinerary[leg].hotel.nights;
                            search_session.itinerary[leg].hotel.nights = value;
                            var days_to_add = parseInt(value) - parseInt(nights);
                            if (days_to_add > 0) {
                                search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) + days_to_add;
                                search_session.itinerary[leg].city.days_to_add = days_to_add;
                                search_session.itinerary[leg].city.add_after_date = from_date;
                                bookingSummary.update(JSON.stringify(search_session));
                            } else {
                                var deduct = Math.abs(days_to_add);
                                var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);
                                search_session.itinerary[leg].city.days_to_deduct = deduct;
                                search_session.itinerary[leg].city.deduct_after_date = from_date;
                                search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);
                                if (search_session.itinerary[leg].activities != null) {
                                    var act = sortByDate(search_session.itinerary[leg].activities);
                                    var departureDate = getDeparture(leg, from_date, to_date);
                                    departureDate = departureDate.split(' ');
                                    var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                    //alert(departureDate + '==//==' + activityDates);
                                    activityCancellation(activityDates, leg, act, value);
                                } else {
                                    bookingSummary.update(JSON.stringify(search_session));
                                }
                                return;
                            }
                        } else {
                            $('.no-hotel-summary').modal();
                        }
                    }

                }
            }
        });
    });  
})

function getDeparture(itineraryIndex, from_date, to_date) {
    var search_session = JSON.parse($('#search-session').val());
    var newDateTo = addDays(from_date, parseInt(search_session.itinerary[itineraryIndex].city.default_nights));
    var transport = search_session.itinerary[itineraryIndex].transport;

    var departureDate = to_date + ' 24';
    if (transport != null) {
        var eta = transport.eta;
        var travelDuration = transport.duration;
        if (transport.provider == 'mystifly') {

            travelDuration = formatTime(get_hours_min(travelDuration));
            eta = moment(eta, moment.ISO_8601);
            eta = eta.format('hh:mm A');

        } else {
            travelDuration = removePlus(travelDuration);
            travelDuration = formatTime(travelDuration);
        }

        var arrivalTime = arrival_am_pm(eta);
        departureDate = getDayTime(newDateTo + ' ' + arrivalTime, travelDuration);
    }
    return departureDate;
}

function formatDate(date, time = false) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (time) {
        day += ' ' + d.getHours();
    }
    return [year, month, day].join('-');
}

function activityCancellation(activityDates, itineraryIndex, act, value) {
    if (activityDates.length > 0) {
        eroam.confirm(null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e) {
            if (act.length > 0) {
                for (index = act.length - 1; index >= 0; index--) {
                    var actDuration = parseInt(act[index].duration);
                    var multiDayRemoved = false;
                    if (actDuration > 1) {
                        actDuration = actDuration - 1;
                        var activityDateSelected = act[index].date_selected;
                        for (var counter = 1; counter <= actDuration; counter++) {
                            var multiDayActivityDate = addDays(activityDateSelected, counter);
                            if (activityDates.indexOf(multiDayActivityDate) > -1 && !multiDayRemoved) {
                                act.splice(index, 1);
                                multiDayRemoved = true;
                            }
                        }
                    } else {
                        if (activityDates.indexOf(act[index].date_selected) > -1) {
                            act.splice(index, 1);
                        }
                    }
                }
            }

            search_session.itinerary[itineraryIndex].activities = act;
            bookingSummary.update(JSON.stringify(search_session))
        });
    } else {
        bookingSummary.update(JSON.stringify(search_session));
    }
}

$('body').on('click', '#reorder-locations-btn', function(e) {
    e.preventDefault();
    $('.itinerary-container, #reorder-locations-btn').hide();
    $('#reorder-locations-container').show();
    $('#reorder-locations').sortable({
        items: 'li:not(li:first-child)',
        placeholder: 'ui-state-highlight',
        opacity: 0.7,
        scrollSensitivity: 50,
        forcePlaceholderSize: true,
        over: function(e, ui) {
            ui.placeholder.height(ui.item.height() - 30);
        }
    });
});

$('body').on('click', '#cancel-reorder-btn', function(e) {
    e.preventDefault();
    $('#reorder-locations-container').hide();
    $('.itinerary-container, #reorder-locations-btn').show();
});

$('body').on('click', '.accommodation_outer', function(e){
    var url = $(this).children('.accommodation').data("link");
    if (url != undefined) {
        window.location.href = url;
    }
});

// Collapse click
$('body').on('click', '.collapse_click', function() {
    if ($(this).parent('.itinerary_left_box').hasClass('itinerary_left_active')) {
        $(this).parent('.itinerary_left_box').removeClass('itinerary_left_active');
        $(this).find('.ic-down_up').addClass('ic-up_down').removeClass('ic-down_up');
    }else{
        $(this).parent('.itinerary_left_box').addClass('itinerary_left_active');
        $(this).find('.ic-up_down').addClass('ic-down_up').removeClass('ic-up_down');
    }
});

$("#login_form2").validate({

    rules: {
        username_login: {
            required: true,
            email:true
        },
        password_login: {
            required: true,
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter(element);
    },
    submitHandler: function (form) {
        
        $('input.submitLoginForm').val('Loading');
        var postData = $("#login_form2").serialize();
            eroam.ajax('post', 'login', postData, function(response) {
                $('#login-btn2').html('Loading...');
                if (response.trim() == 'valid') {
                    $('#login-error-msg2').hide();
                    $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
                    window.location.href = '/review-itinerary/';
                }
                else if(response.trim() == 'confirm_first')
                {
                    $('#login-error-message2').text('Please Confirm your mail first.').show();
                    $('#login-btn2').html('Log In');
                }
                else {
                    $('#login-error-message2').text('Invalid Email Address or Password').show();
                    $('#login-btn2').html('Log In');
                }
            }, function() {
                $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
            });
        }
});

$(document).ready(function() {
    $("#signup_form_id").validate({
        ignore: [],
        rules: {
            passenger_address_one: {
                required: true
            },
            passenger_suburb: {
                required: true
            },
            passenger_state: {
                required: true
            },
            passenger_zip: {
                required: true,
                number:true
            },
            passenger_title: {
                required: true
            },
            passenger_gender: {
                required: true
            },
            first_name: {
                required: true,
                lettersonly:true
            },
            last_name: {
                required: true,
                lettersonly:true
            },
            card_number: {
                required: true,
                number:true
            },
            year: {
                required: true
            },
            cvv: {
                required: true,
                maxlength: 3,
                number:true
            },
            month: {
                required: true
            },
            country: {
                required: true
            },
            postalcode: {
                required: true
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {

            form.submit();
        }
    });

    $('.passenger_zip').each(function () {
        $(this).rules('add', {
            required: true,
            number:true
        });
    });
    $('.passenger_state').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_suburb').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_address_one').each(function () {
        $(this).rules('add', {
            required: true
        });
    });

    $('.passenger_gender').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_title').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_first_name').each(function () {
        $(this).rules('add', {
            required: true,
            lettersonly:true
        });
    });
    $('.passenger_last_name').each(function () {
        $(this).rules('add', {
            required: true,
            lettersonly:true
        });
    });
    $('.passenger_dob').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_contact_no').each(function () {
        $(this).rules('add', {
            required: true,
            numbersonly:true,
            maxlength: 15,
        });
    });
    $('.passenger_email').each(function () {
        $(this).rules('add', {
            required: true,
            email:true
        });
    });
    $('.passenger_country').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_passport_expiry_date').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_passport_num').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
});