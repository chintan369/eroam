@extends('layouts.profileLayout')
@section('content')  
<script src="{{ url( 'js/theme/jquery.min.js' ) }}"></script>  
<section>
<div class="profile_page">
  @include('pages.search_banner') 
</div>
<section>
    @include('user.profile_sidebar')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    @include('user.profile_header')
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    @if(session()->has('profile_step4_success'))
                        <p class="success-box m-t-30">
                            {{ session()->get('profile_step4_success') }}
                        </p>
                    @endif
                    @if(session()->has('profile_step4_error'))
                        <p class="danger-box m-t-30">
                            {{ session()->get('profile_step4_error') }}
                        </p>
                    @endif
                    <div class="m-t-30 profile-prefrence">
                        <h2 class="profile-title">Your Contact Preferences</h2>
                        <form class="form-horizontal m-t-20" method="post" action="{{url('profile_step4')}}" id="profile_step4_form">

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Email Address *</label>
                                    <input type="text" name="email" value="{{ $user->customer->email }}" class="form-control" placeholder="Email Address" />
                                </div>
                                @if ($errors->has('email')) 
                                        <span class="error active">{{$errors->first('email')}}</span>
                                @endif
                                <label for="email" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Contact Phone Number</label>
                                    <input type="text" name="contact_no" class="form-control" placeholder="Contact Phone Number" value="{{ $user->customer->contact_no}}"/>
                                    
                                </div>  
                                <label for="contact_no" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Mobile Number</label>
                                    <input type="text" name="mobile_no" class="form-control" placeholder="Mobile Number" value="{{ $user->customer->mobile_no}}"/>                                    
                                </div>   
                                <label for="mobile_no" generated="true" class="error"></label>    
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group">
                                    <label class="label-control">Preferred Contact Method</label>
                                    <select class="form-control" name="pref_contact_method">
                                        <option value="" >Select Preferred Contact Method</option>
                                        <option value="phone_call" {{ $user->customer->pref_contact_method == 'phone_call' ? 'selected' : '' }}>Phone Call</option>
                                        <option value="sms" {{ $user->customer->pref_contact_method == 'sms' ? 'selected' : '' }}>SMS</option>
                                        <option value="email" {{ $user->customer->pref_contact_method == 'email' ? 'selected' : '' }}>Email</option>
                                        <option value="mail" {{ $user->customer->pref_contact_method == 'mail' ? 'selected' : '' }}>Mail</option>
                                    </select>
                                </div>  
                                <label for="pref_contact_method" generated="true" class="error"></label> 
                            </div>  
                            <h2 class="profile-title">Physical Address</h2><br>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Address Line One (1)</label>
                                    <input type="text" name="phy_address_1" value="{{ $user->customer->phy_address_1 }}" class="form-control phy_address_1" placeholder="Street Address, P.O Box, Company Name" />
                                    
                                </div>
                                <label for="phy_address_1" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Address Line Two (2)</label>
                                    <input type="text" name="phy_address_2" value="{{ $user->customer->phy_address_2 }}" class="form-control phy_address_2" placeholder="Apartment, Suite, Unit, Building, Floor, etc." />
                                    
                                </div>
                                <label for="phy_address_2" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group">
                                    <label class="label-control">Country</label>
                                    <select id="phy_country" name="phy_country" class="form-control phy_country">
                                        <option value="">Select Country</option>
                                        @foreach ($all_countries as $country)
                                            <option value="{{ $country['id'] }}" {{ $user->customer->phy_country == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                        @endforeach                                       
                                    </select>                                   
                                </div>
                                <label for="phy_country" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">State</label>
                                    <input type="text" name="phy_state" value="{{ $user->customer->phy_state }}" class="form-control phy_state" placeholder="State" />                                    
                                </div>
                                <label for="phy_state" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">City</label>
                                    <input type="text" name="phy_city" value="{{ $user->customer->phy_city }}" class="form-control phy_city" placeholder="City" />                                    
                                </div>
                                <label for="phy_city" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Zip</label>
                                    <input type="text" name="phy_zip" value="{{ $user->customer->phy_zip }}" class="form-control phy_zip" placeholder="Zip Code" />                                    
                                </div> 
                                <label for="phy_zip" generated="true" class="error"></label>  
                            </div>

                            <div class="m-t-20">
                                <button type="submit" name="" class="btn btn-black btn-block">UPDATE PROFILE</button>
                            </div>
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <input type="hidden" name="step" value="1">
                            <input type="hidden" name="_token" id="csrf_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="registered_user_id" value="{{ $user->customer->email }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
<section>
@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_leftmenu.js?v='.$version.'') }}"></script>
<script type="text/javascript" src="{{  url( 'js/eroam_js/profile_step4.js?v='.$version.'') }}"></script>
@stop