@php 
    /*
     Accomodation multi select options
    */

    $travel_pref = [];
    $interest_ids = [];
    if( session()->has('travel_preferences') )
    {
        $travel_pref    = session()->get('travel_preferences');
        $travel_pref    = reset( $travel_pref );
        $interest_ids   = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
    }

    $cities     = Cache::get('cities');
    $countries  = Cache::get('countries');
    $labels     = Cache::get('labels');
    $travellers = Cache::get('travellers');
    $countries1 = Cache::get('countriesBookingsPro');
    
    /*
    | Accomodation multi select options
    */

    $accommodation_name     = ''; 
    $accommodation_options  = '';
    $room_type_name         = '';
    $room_type_options      = '';
    $transport_type_name    = '';
    $transport_type_options = '';
    $nationality            = '';
    $nationality_name       = '';
    $age_group              = '';
    $age_group_name         = '';
    $gender                 = '';
    $gender_name            = '';
    $headers = [
        'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
        'Origin' => url('')
    ];
    
    if(session()->has('tourDatas') )
    {
        $tourDatas      = session()->get('tourDatas');
        $tourcountries  = $tourDatas['tourcountries'];
        $tourCities     = $tourDatas['tourCities'];     /* add by dhara */   
    }
    else
    {
        $tourcountries  = http('post', 'getTourCountriesAvailables', [], $headers);   
        $tourCities     = http('post', 'getTourCitiesAvailable', [], $headers);

        $tourDatas['tourcountries']  = $tourcountries;
        $tourDatas['tourCities']     = $tourCities;
        session()->put('tourDatas',$tourDatas);
    }

    if ( count( $travellers['categories'] ) > 0 ) 
    {
        foreach( $travellers['categories'] as $category )
        {
            if( empty( $category['name'] ) ){
                continue;
            }
          
            if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) 
            {
                $accommodation_name .= '<span title="'.$category['name'].'" class="drop-selected">'.$category['name'].'</span>';                
            
                $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" selected="selected">'.$category['name'].'</option>';
            }
            else
            {
            
                $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'" >'.$category['name'].'</option>';
            }
        }
    }

    if( count( $travellers['room_types'] ) > 0 )
    {
        foreach( $travellers['room_types'] as $room_type)
        {
            if( empty( $room_type['name'] ) ){
                continue;
            }
            if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) 
            {
                $room_type_name .= '<span title="'.$room_type['name'].'" class="drop-selected">'.$room_type['name'].'</span>';
            
                $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" selected="selected"> '.$room_type['name'].'</option>';
            }
            else
            {
            
                $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'" > '.$room_type['name'].'</option>';
            }    
        }
    }

    if( count( $travellers['transport_types'] ) > 0 )
    {
        foreach( $travellers['transport_types'] as $transport_type)
        {
            if( empty( $transport_type['name'] ) ){
                continue;
            }
            if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) 
            {
                $transport_type_name .= '<span title="'.$transport_type['name'].'" class="drop-selected">'.$transport_type['name'].'</span>';
            
                $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" selected="selected">'.$transport_type['name'].'</option>';
            }
            else
            {
            
                $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'">'.$transport_type['name'].'</option>';
            }
        }
    }

    if( count( $travellers['nationalities'] ) > 0 )
    {
        foreach( $travellers['nationalities']['featured'] as $featured )
        {
            if( empty( $featured['name'] ) )
            {
                continue;
            }
            if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) 
            {
                $nationality_name .= '<span title="'.$featured['name'].'" class="drop-selected">'.$featured['name'].'</span>';
            
                $nationality .= '<option value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" selected="selected">'.$featured['name'] .'</option>';
            }
            else
            {
                $nationality .= '<option value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'" >'.$featured['name'] .'</option>';
            }
        }

        foreach( $travellers['nationalities']['not_featured'] as $not_featured )
        {
            if( empty( $not_featured['name'] ) )
            {
                continue;
            }
            if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) )
            {
                $nationality_name .= '<span title="'.$not_featured['name'].'" class="drop-selected">'.$not_featured['name'].'</span>';
                $nationality .= '<option value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" selected="selected"> '.$not_featured['name'] .'</option>';
            }
            else
            {
                $nationality .= '<option value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .'</option>';
            }
        }
    }

    $gender_array = ['male', 'female', 'other'];
    foreach($gender_array as $gen)
    {
        if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) 
        {
            $gender_name .= '<span title="'.$gen.'" class="drop-selected">'.ucfirst($gen).'</span>';
            $gender .= '<option value="'.$gen.'" data-checked="1" data-name="'.$gen.'" selected="selected"> '.ucfirst($gen) .'</option>';
        }
        else
        {
            $gender .= '<option value="'.$gen.'" data-checked="0" data-name="'.$gen.'" > '.ucfirst($gen) .'</option>';
        }
    }
    
    if( count( $travellers['age_groups'] ) > 0 )
    {
        foreach( $travellers['age_groups'] as $age)
        {
            if( empty( $age['name'] ) )
            {
                continue;
            }
            if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) 
            {
                $age_group_name .= '<span title="'.$age['name'].'" class="drop-selected">'.$age['name'].'</span>';
                $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" selected="selected" data-checked="1">'.$age['name'].'</option>';
            }
            else
            {
                $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" data-checked="0">'.$age['name'].'</option>';
            }
        }
    }

    $date = new DateTime('+1 day');

    $accommo_select = '';
    if(!empty($travel_pref['accommodation'][0]))
    {
        $accommo_select = $travel_pref['accommodation'][0];    
    }
    $transport_select = '';
    if(!empty($travel_pref['transport'][0]))
    {
        $transport_select = $travel_pref['transport'][0];    
    }
    
  @endphp


<section class="searchtour_plan">
    <div class="container">
        <p>@lang('home.search_para')</p>
        <ul class="nav nav-tabs regular slider" id="myTab" role="tablist">
            <li class="nav-item tabtour">
                <a class="nav-link " id="home-tab" data-toggle="tab" href="#tourpackages" role="tab" aria-controls="tourpackages" aria-selected="true"><i class="ic-local_activity"></i> @lang('home.search_tours_package_link')</a>
            </li>
            <li class="nav-item tabtour">
                <a class="nav-link " id="profile-tab" data-toggle="tab" href="#createitinerary" role="tab" aria-controls="createitinerary" aria-selected="false"><i class="ic-place"></i> @lang('home.search_create_itinerary')</a>
            </li>
            <li class="nav-item tabtour">
                <a class="nav-link disabled" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')" ><i class="ic-local_hotel"></i> @lang('home.search_accommodation')</a>
            </li>
            <li class="nav-item tabtour">
                <a class="nav-link disabled" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')"><i class="ic-flight"></i> @lang('home.search_flights')</a>
            </li>
            <li class="nav-item tabtour">
                <a class="nav-link disabled" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')"><i class="ic-direction_car"></i> @lang('home.search_car_hire')</a>
            </li>
        </ul>


    </div>


</section>

<!---------Search FIlter TAB END-------->
<section class="toursearch_filter">
    <div class="container">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade" id="tourpackages" role="tabpanel" aria-labelledby="home-tab">
                <form method="post" action="{{ url('tours') }}" id="search-form" >
                    {{ csrf_field() }}
                    <input type="hidden" name="searchValType" id="searchValType" value="">
                    <input type="hidden" name="searchVal" id="searchVal" value="">
                    <input type="hidden" name="countryId" id="countryId" value="">
                    <input type="hidden" name="countryRegion" id="countryRegion" value="">
                    <input type="hidden" name="countryRegionName" id="countryRegionName" value="">
                    <input type="hidden" id="search_option_id" name="option" value="packages">
                   
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.tour_package_checkbox1') &nbsp;
                                <input type="checkbox" name="tour_type[]" id="checkbox-03" checked value="single" class="tour_checkbox"><span class="check_indicator">&nbsp;</span></span>
                                <small class="question" id="auto-popup" data-toggle="modal" data-target=".bd-auto-modal-lg"><i class="ic-faq"></i> </small>

                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.tour_package_checkbox2')<strong> </strong> 
                                <input type="checkbox" name="tour_type[]" id="checkbox-04" checked value="multi" class="tour_checkbox"><span class="check_indicator">&nbsp;</span></span>
                                 <small class="question" id="manual-popup" data-toggle="modal" data-target=".bd-manual-modal-lg"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-8 col-lg-9 col-xl-9 ">
                            <div class="form-group">
                                <div class="lable_title"><i class="ic-place"></i> @lang('home.location')</div>
                                <div class="fildes_outer">
                                    <label>@lang('home.location_destination')</label>
                                    <input type="text" name="project" id="project" class="form-control ui-autocomplete-input" placeholder="@lang('home.location_placeholder')" >
                                </div>
                            </div>
                             <label for="project" generated="true" class="errormessage"></label>
                        </div>

                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 ">
                            <div class="form-group">
                                <div class="lable_title"><i class="ic-date_range_white"></i>@lang('home.travel_date')</div>
                                <div class="fildes_outer">
                                    <label>@lang('home.map_label6')</label>
                                    <input id="start_date34" type="text" placeholder="<?php echo date('D, d M Y',strtotime('+1 day')) ?>" class="form-control departure_date datepicker"  name="departure_date">
                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="preference">
                        <a href="#" id="profile_popup" data-toggle="modal" data-target="#preferencesModal" data-target=".bd-example-modal-lg">
                            <i class="ic-fingerprint"></i>@lang('home.personal_preferences')<small class="question"><i class="ic-faq"></i> </small>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="p-20">
                       <input type="submit" class="search_itinery border-0 btns_default btns_light_blue btns_capital box_shadow_dark p-b-10 p-10" value="Search">
                        <div class="clearfix"></div>
                    </div>
                </form>


            </div>

            <!-----Tour packages END------>
            <div class="tab-pane fade" id="createitinerary" role="tabpanel" aria-labelledby="profile-tab">
                <form method="post" action="/search" id="search-form2">
                    {{ csrf_field() }}
                    <input type="hidden" name="country" id="starting_country" value="@isset($from_country_id) {{ $from_country_id }} @endisset">
                    <input type="hidden" name="city" id="starting_city" value="@isset($from_city_id) {{ $from_city_id }} @endisset">
                    <input type="hidden" name="to_country" id="destination_country" value="">
                    <input type="hidden" name="to_city" id="destination_city" value="">
                    <input type="hidden" name="auto_populate" id="auto-populate" value="1">
                    <input type="hidden" name="countryId" id="countryId2" value="">
                    <input type="hidden" name="countryRegion" id="countryRegion2" value="">
                    <input type="hidden" name="countryRegionName" id="countryRegionName2" value="">
                    <input type="hidden" id="search_option_id2" name="option" value="packages">
                    <input type="hidden" name="searchValType2" id="searchValType2" value="">
                    <input type="hidden" name="searchVal2" id="searchVal2" value="">
                    
                    <div class="row">
                        <div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2  fullwidth">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox1') <input type="checkbox" value="auto" name="option" class="radio_tailor" id="auto"><span class="check_indicator">&nbsp;</span></span> <small class="question" id="auto-popup" data-toggle="modal"  data-target=".bd-auto-modal-lg"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2 fullwidth ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox2')<strong> </strong> <input type="checkbox" name="option" value="manual" class="radio_tailor" id="manual"><span class="check_indicator">&nbsp;</span>
                                </span>
                                <small class="question" id="manual-popup" data-toggle="modal" data-target=".bd-manual-modal-lg"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-2 fullwidth ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox3')<strong> </strong> <input type="checkbox" disabled checked><span class="check_indicator">&nbsp;</span></span>
                                <small class="question" data-toggle="tooltip" data-placement="bottom" data-original-title="@lang('home.pilot_text')"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-12 col-sm-6 col-md-8 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <div class="lable_title "><i class="ic-place"></i> @lang('home.location')</div>
                            </div>
                        </div>
                    </div>
					
					
                    <div class="row" style="margin-top: -20px;">
                        <div class="col-12 col-sm-6 col-md-8 col-lg-6 col-xl-6" id="departing">
                            <div class="form-group">
                                <div class="fildes_outer">
                                    <label>@lang('home.location_departing')</label>
                                    <input type="text" class="form-control ui-autocomplete-input location_input" id="start_location"  placeholder="@lang('home.location_placeholder')" name="start_location">
                                </div>
                            </div>
                             <label for="start_location" generated="true" class="errormessage" id="start_loc"></label>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-6 col-xl-6 ">
                            <div class="form-group">
                                <div class="fildes_outer">
                                    <label>@lang('home.location_destination')</label> 
                                    <input type="text" class="search_from_col form-control ui-autocomplete-input valid location_input" id="end_location" placeholder="@lang('home.location_placeholder')"  name="end_location" >
                                </div>
                            </div>
                             <label for="end_location" generated="true" class="errormessage"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                            <div class="form-group">
                                <div class="lable_title "><i class="ic-date_range_white"></i> @lang('home.travel_date_label')</div>
                                <div class="fildes_outer">
                                    <label>@lang('home.map_label8')</label>
                                    <input type="text" class="form-control datepicker valid" name="start_date" id="start_date" placeholder="<?php echo date('d-m-Y',strtotime('+1 day')) ?>" value="<?php echo date('d-m-Y',strtotime('+1 day')) ?>" autocomplete="off">
                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 ">
                            <div class="subcat">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="lable_title bold_font"><i class="ic-people_outline_black"></i>@lang('home.create_itenerary_travel_detail')</div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>@lang('home.travel_detail1')</label>
                                                <div class="custom-select">
                                                    <select class="disabled" name="rooms" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
                                                    	 @for($i = 1; $i <= 1; $i++) 
                                                            <option value="{{$i}}" >{{$i}} Room</option>
                                                        @endfor
						                          	</select>
						                          	<input type="hidden" class="room_select" name="rooms" value="1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>@lang('home.travel_detail2')</label>
                                                <div class="custom-select">
                                                    <select name="num_of_adults[]" id="number_of_adults">
						                            	@for($i = 1; $i <= 14 ; $i++)
						                                  	<option value="{{$i}}">{{$i}}</option>
						                              	@endfor
						                          </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>@lang('home.travel_detail3') (0 - 17)</label>
                                                <div class="custom-select">
                                                    <select class="disabled" name="num_of_children[]" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
						                             <option value="0">@lang('home.map_label5')</option>
						                          </select>
						                          <input type="hidden" class="child_select" id="number_of_child" name="num_of_children[]" value="1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="total_adults">
                                        <input type="hidden" name="travellers" id="total_room_adults" value="1" >
                                        <input type="hidden" name="total_children" id="total_room_child" value="0" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12  ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox4') <strong> </strong> <input value="search-box4" type="checkbox" data-toggle="tooltip" data-placement="top" title="@lang('home.pilot_text')"><span class="check_indicator">&nbsp;</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="preference">
                    	<a href="#" id="profile_popup" data-toggle="modal" data-target="#preferencesModal" data-target=".bd-example-modal-lg"> <i class="ic-fingerprint"></i> @lang('home.personal_preferences') </a>
                         <small class="question" data-toggle="tooltip" data-placement="bottom" data-original-title="@lang('home.pilot_text')"><i class="ic-faq"></i> </small>
                    </div>
                    <div class="clearfix"></div>
                    <div class="p-20">
                        <input type="submit" class="search_itinery border-0 btns_default btns_light_blue btns_capital box_shadow_dark p-b-10 p-10" value="Search">
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>

            <!----- Create Itinerary END------>
            <div class="tab-pane fade " id="accommodation" role="tabpanel" aria-labelledby="contact-tab">
                <form>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <div class="lable_title "><i class="ic-place"></i> @lang('home.location')</div>
                                <div class="fildes_outer">
                                    <label>@lang('home.location_departing')</label>
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="@lang('home.location_placeholder')">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="lable_title "><i class="ic-date_range_white"></i> @lang('home.travel_date_label')</div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.map_label8')</label>
                                            <input type="text" class="form-control"  placeholder="17 Aug 2018">
                                            <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.map_label8')</label>
                                            <input type="text" class="form-control" placeholder="17 Aug 2018">
                                            <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="lable_title bold_font"><i class="ic-people_outline_black"></i>@lang('home.create_itenerary_travel_detail')</div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.travel_detail1')</label>
                                            <div class="custom-select">
                                                <select>
						                          <option value="volvo">1 Room</option>
						                          <option value="saab">2 Room</option>
						                          <option value="mercedes">3 Room</option>
						                          <option value="audi">4 Room</option>
						                          <option value="audi">5 Room</option>
						                       </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.travel_detail2')</label>
                                            <div class="custom-select">
                                                <select>
						                          <option value="volvo">8 Adults</option>
						                          <option value="saab">7 Adults</option>
						                          <option value="mercedes">6 Adults</option>
						                          <option value="audi">5 Adults</option>
						                          <option value="audi">4 Adults</option>
						                          <option value="audi">3 Adults</option>
						                          <option value="audi">2 Adults</option>
						                          <option value="audi">1 Adults</option>
						                       </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.travel_detail3')</label>
                                            <div class="custom-select">
                                                <select>
						                          <option value="volvo">8 Children</option>
						                          <option value="saab">7 Children</option>
						                          <option value="mercedes">6 Children</option>
						                          <option value="audi">5 Children</option>
						                          <option value="audi">4 Children</option>
						                          <option value="audi">3 Children</option>
						                          <option value="audi">2 Children</option>
						                          <option value="audi">1 Children</option>
						                       </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12  ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox4') <strong> </strong> <input type="checkbox"><span class="check_indicator">&nbsp;</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="preference">
                        <i class="ic-fingerprint"></i> @lang('home.personal_preferences')<small class="question"><i class="ic-faq"></i> </small>
                    </div>
                    <div class="clearfix"></div>
                    <div class="p-20">
                        <a href="#" class="btns_default btns_light_blue btns_capital box_shadow_dark p-b-10 p-10">@lang('home.search_text') </a>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade " id="flights" role="tabpanel" aria-labelledby="contact-tab">
                <form>
                    <div class="row">
                        <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-2 ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox1')<input type="checkbox"><span class="check_indicator">&nbsp;</span></span> <small class="question"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-2 ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox2')<strong> </strong> <input type="checkbox"><span class="check_indicator">&nbsp;</span>
                                </span>
                                <small class="question"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-2 ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.create_itenerary_checkbox3')<strong> </strong> <input type="checkbox"><span class="check_indicator">&nbsp;</span></span>
                                <small class="question"><i class="ic-faq"></i> </small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-8 col-lg-6 col-xl-6 ">
                            <div class="form-group">
                                <div class="lable_title "><i class="ic-place"></i>@lang('home.location')</div>
                                <div class="fildes_outer">
                                    <label>@lang('home.location_departing')</label>
                                    <input type="text" class="form-control" placeholder="@lang('home.location_placeholder')">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-6 col-xl-6 ">
                            <div class="form-group">
                                <div class="lable_title"> &nbsp; </div>
                                <div class="fildes_outer">
                                    <label>@lang('home.location_destination')</label>
                                    <input type="text" class="form-control" placeholder="@lang('home.location_placeholder')">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5">
                                    <div class="lable_title "><i class="ic-date_range_white"></i> @lang('home.travel_date_label')</div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                            <div class="form-group">

                                                <div class="fildes_outer">
                                                    <label>@lang('home.departure_date')</label>
                                                    <input type="text" class="form-control" placeholder="17 Aug 2018">
                                                    <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.map_label6')</label>
                                                    <input type="text" class="form-control" placeholder="17 Aug 2018">
                                                    <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7">
                                    <div class="subcat">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="lable_title"><i class="ic-people_outline_black"></i> @lang('home.traveller_details') <small class="question"><i class="ic-faq"></i> </small></div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                                                        <div class="form-group">
                                                            <div class="fildes_outer">
                                                                <label>@lang('home.travel_detail2')</label>
                                                                <div class="custom-select">
                                                                    <select>
							                                         <option value="volvo">4 Years</option>
							                                         <option value="saab">7 Years</option>
							                                         <option value="mercedes">6 Years</option>
							                                         <option value="audi">5 Years</option>
							                                         <option value="audi">4 Years</option>
							                                         <option value="audi">3 Years</option>
							                                         <option value="audi">2 Years</option>
							                                         <option value="audi">1 Years</option>
							                                      </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                                                        <div class="form-group">
                                                            <div class="fildes_outer">
                                                                <label>@lang('home.travel_detail3')(0-17)</label>
                                                                <div class="custom-select">
                                                                    <select>
							                                         <option value="volvo">6 Years</option>
							                                         <option value="saab">7 Years</option>
							                                         <option value="mercedes">6 Years</option>
							                                         <option value="audi">5 Years</option>
							                                         <option value="audi">4 Years</option>
							                                         <option value="audi">3 Years</option>
							                                         <option value="audi">2 Years</option>
							                                         <option value="audi">1 Years</option>
							                                      </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">

                            <div class="row">
                                <div class="col-12">
                                    <div class="lable_title"><i class="ic-child_care_white"></i> @lang('home.age_required') <small class="question"><i class="ic-faq"></i> </small></div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.travel_detail2')</label>
                                                    <div class="custom-select">
                                                        <select>
				                                         <option value="volvo">4 Years</option>
				                                         <option value="saab">7 Years</option>
				                                         <option value="mercedes">6 Years</option>
				                                         <option value="audi">5 Years</option>
				                                         <option value="audi">4 Years</option>
				                                         <option value="audi">3 Years</option>
				                                         <option value="audi">2 Years</option>
				                                         <option value="audi">1 Years</option>
				                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.travel_detail3') (0-17)</label>
                                                    <div class="custom-select">
                                                        <select>
				                                         <option value="volvo">6 Years</option>
				                                         <option value="saab">7 Years</option>
				                                         <option value="mercedes">6 Years</option>
				                                         <option value="audi">5 Years</option>
				                                         <option value="audi">4 Years</option>
				                                         <option value="audi">3 Years</option>
				                                         <option value="audi">2 Years</option>
				                                         <option value="audi">1 Years</option>
				                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label> @lang('home.travel_detail3') (0-17)</label>
                                                    <div class="custom-select">
                                                        <select>
				                                         <option value="volvo">6 Years</option>
				                                         <option value="saab">7 Years</option>
				                                         <option value="mercedes">6 Years</option>
				                                         <option value="audi">5 Years</option>
				                                         <option value="audi">4 Years</option>
				                                         <option value="audi">3 Years</option>
				                                         <option value="audi">2 Years</option>
				                                         <option value="audi">1 Years</option>
				                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label>@lang('home.travel_detail3') (0-17)</label>
                                                    <div class="custom-select">
                                                        <select>
				                                         <option value="volvo">6 Years</option>
				                                         <option value="saab">7 Years</option>
				                                         <option value="mercedes">6 Years</option>
				                                         <option value="audi">5 Years</option>
				                                         <option value="audi">4 Years</option>
				                                         <option value="audi">3 Years</option>
				                                         <option value="audi">2 Years</option>
				                                         <option value="audi">1 Years</option>
				                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label> @lang('home.travel_detail3') (0-17)</label>
                                                    <div class="custom-select">
                                                        <select>
				                                         <option value="volvo">6 Years</option>
				                                         <option value="saab">7 Years</option>
				                                         <option value="mercedes">6 Years</option>
				                                         <option value="audi">5 Years</option>
				                                         <option value="audi">4 Years</option>
				                                         <option value="audi">3 Years</option>
				                                         <option value="audi">2 Years</option>
				                                         <option value="audi">1 Years</option>
				                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2 ">
                                            <div class="form-group">
                                                <div class="fildes_outer">
                                                    <label> @lang('home.travel_detail3') (0-17)</label>
                                                    <div class="custom-select">
                                                        <select>
				                                         <option value="volvo">6 Years</option>
				                                         <option value="saab">7 Years</option>
				                                         <option value="mercedes">6 Years</option>
				                                         <option value="audi">5 Years</option>
				                                         <option value="audi">4 Years</option>
				                                         <option value="audi">3 Years</option>
				                                         <option value="audi">2 Years</option>
				                                         <option value="audi">1 Years</option>
				                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="preference">
                        <i class="ic-fingerprint"></i>@lang('home.personal_preferences') <small class="question"><i class="ic-faq"></i> </small>
                    </div>
                    <div class="clearfix"></div>
                    <div class="p-20">
                        <a href="#" class="btns_default btns_light_blue btns_capital box_shadow_dark p-b-10 p-10">@lang('home.search_text')</a>
                        <div class="clearfix"></div>
                    </div>


                </form>


            </div>
            <div class="tab-pane fade " id="carhire" role="tabpanel" aria-labelledby="contact-tab">
                <form>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 ">
                            <div class="form-group">
                                <span class="custom_check">@lang('home.search_note4') &nbsp; <input type="checkbox"><span class="check_indicator">&nbsp;</span></span>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-8 col-lg-9 col-xl-9 ">
                            <div class="form-group">
                                <div class="lable_title"><i class="ic-place"></i>@lang('home.location') </div>
                                <div class="fildes_outer">
                                    <label>@lang('home.location_destination')</label>
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="@lang('home.select_departure_location')">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 ">
                            <div class="form-group">
                                <div class="lable_title"><i class="ic-event_note"></i> @lang('home.travel_date')</div>
                                <div class="fildes_outer">
                                    <label>@lang('home.location_destination')</label>
                                    <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="@lang('home.select_departure_location')">
                                    <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="lable_title "><i class="ic-date_range_white"></i> @lang('home.travel_date')</div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.create_itenerary_checkin')</label>
                                            <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="17 Aug 2018">
                                            <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                    <div class="form-group">
                                        <div class="fildes_outer">
                                            <label>@lang('home.create_itenerary_checkin') </label>
                                            <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="17 Aug 2018">
                                            <span class="arrow_down"><i class="ic-expand_more"></i> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 ">
                            <div class="subcat">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="lable_title bold_font"><i class="ic-people_outline_black"></i> @lang('home.create_itenerary_travel_detail') </div>
                                    </div>

                                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>@lang('home.travel_detail1')</label>
                                                <div class="custom-select">
                                                    <select>
							                          <option value="volvo">1 Room</option>
							                          <option value="saab">2 Room</option>
							                          <option value="mercedes">3 Room</option>
							                          <option value="audi">4 Room</option>
							                          <option value="audi">5 Room</option>
							                       	</select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                        <div class="form-group">
                                            <div class="fildes_outer">
                                                <label>@lang('home.travel_detail2')</label>
                                                <div class="custom-select">
                                                    <select>
							                          <option value="volvo">8 Adults</option>
							                          <option value="saab">7 Adults</option>
							                          <option value="mercedes">6 Adults</option>
							                          <option value="audi">5 Adults</option>
							                          <option value="audi">4 Adults</option>
							                          <option value="audi">3 Adults</option>
							                          <option value="audi">2 Adults</option>
							                          <option value="audi">1 Adults</option>
							                       </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="preference">
                        <i class="ic-fingerprint"></i> @lang('home.personal_preferences') <small class="question"><i class="ic-faq"></i> </small>
                    </div>
                    <div class="clearfix"></div>
                    <div class="p-20">
                        <a href="#" class="btns_default btns_light_blue btns_capital box_shadow_dark p-b-10 p-10">@lang('home.search_text')</a>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

@include('home.partials.preference-modal',['age_group' => $age_group])
@include('home.partials.auto-modal')
@include('home.partials.manual-modal')

@push('scripts') 
	<script type="text/javascript" src="{{ url('js/home/custom.js') }}"></script>
@endpush

<script type="text/javascript">
        var projects = [
            <?php for($i=0; $i<count($cities);$i++) { ?>{
                value: "<?php echo $cities[$i]['id'];?>",
                label: "<?php echo $cities[$i]['name'];?>",
                desc: "<?php echo $cities[$i]['country_name'];?>",
                cid: "<?php echo $cities[$i]['country_id'];?>"
            },
            <?php } ?>
        ];

        var countries = [
                  <?php for($i=0;$i<count($tourcountries);$i++) { ?>{
                      value : "<?php echo $tourcountries[$i]['id'];?>",
                      label : "<?php echo $tourcountries[$i]['name'];?>",
                      cid : 0,
                      cname : "<?php echo $tourcountries[$i]['name'];?>",
                      rid   : "<?php echo $tourcountries[$i]['region_id'];?>",
                      rname : "<?php echo $tourcountries[$i]['rname'];?>",
                      type : "country"
                  },

                <?php } ?>

                <?php for($i=0;$i<count($tourCities);$i++) { ?>{
                    value : "<?php echo $tourCities[$i]['city_id'];?>",
                    label : "<?php echo $tourCities[$i]['name'];?>",
                    cid   : "<?php echo $tourCities[$i]['country_id'];?>",
                    cname : "<?php echo $tourCities[$i]['cname'];?>",
                    rid   : "<?php echo $tourCities[$i]['region_id'];?>",
                    rname : "<?php echo $tourCities[$i]['rname'];?>",
                    type  : "city"
                },

                <?php } ?>
            ];
</script>