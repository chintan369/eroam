<?php
    $legkey = explode('?', $_SERVER['HTTP_REFERER']);

    if (count($legkey) > 1) {
        $legkey = explode('&', $legkey[1]);
        $legkey = explode('=', $legkey[0]);
        $legkey = $legkey[1];
    }else{
        $legkey = 0;
    }

    $preUrl = url()->previous();
    $isMap = 0;

    if (substr($preUrl, -3) == 'map') {
        $isMap = 1;
    }

    $preUrl = str_replace(url('') . '/', '', $preUrl);
    $countcity = count(session()->get('search') ['itinerary']) - 1;
    $last = count(session()->get('search') ['itinerary']) - 1;
    $temp_data = session()->get('search');

    if (isset($temp_data['travel_date']) && !empty($temp_data['travel_date'])) {
        $departure_date = session()->get('search') ['travel_date'];
    }else {
        $departure_date = date('d-m-Y');
    }

    if (isset($temp_data['itinerary'][$last]['hotel']['checkout'])) {
        $return_date = session()->get('search') ['itinerary'][$last]['hotel']['checkout'];
    }elseif (isset($temp_data['itinerary'][$last]['activities'])) {
        $count_last = count(session()->get('search') ['itinerary'][$last]['activities']) - 1;

        if ($count_last == '-1') {
            $return_date = date('d-m-Y');
        }else{
            $return_date = session()->get('search') ['itinerary'][$last]['activities'][$count_last]['date_selected'];
        }
    }else {
        $return_date = date('d-m-Y');
    }

    $datetime1 = new DateTime($departure_date);
    $datetime2 = new DateTime($return_date);
    $interval = $datetime1->diff($datetime2);

    $day = explode(" ", session()->get('search') ['total_number_of_days']);
    $days = $day[0];
?>

<script type="text/javascript">
    function calculateWidth() {
        var owidth = $('.custom-tabs .nav-tabs').parent().width() - 10;
        $(".tabs-container .custom-tabs .nav-tabs>li").css('padding', 0);
        var elem = $('.tabs-container .custom-tabs .nav-tabs>li').length;
        var elemWidth = (owidth / elem);
        //alert(owidth+'//'+elem+'=='+elemWidth);
        $(".tabs-container .custom-tabs .nav-tabs>li").width(elemWidth);
    }

    function leftStripHeight() {
        var stripHeight = $('.left-strip').outerHeight();
        var lefticons = $('.leftStrip-icons').outerHeight();
        var arrowHeight = stripHeight - lefticons;
        $(".arrow-btn-new").outerHeight(arrowHeight);
    }

    function leftpanel() {
        var w = $(document).width();
        if (w > 991) {
            $('.arrow-btn-new').click(function() {
                if ($('.arrow-btn-new').hasClass('open')) {
                    $('.page-sidebar').css('display', 'none');
                    $(this).removeClass('open');
                    $('.page-content').css('margin-left', '0px');
                } else {
                    $('.page-sidebar').css('display', 'block');
                    $(this).addClass('open');
                    $('.page-content').css('margin-left', '354px');
                }
            });
        } else {
            $('.arrow-btn-new').click(function() {
                if ($('.arrow-btn-new').hasClass('open')) {
                    $('.page-sidebar').css('display', 'none');
                    $(this).removeClass('open');
                    $('.page-content').css('margin-left', '0px');
                    $('.left-strip').css('left', '0px');

                } else {
                    $('.page-sidebar').css('display', 'block');
                    $(this).addClass('open');
                    $('.page-content').css('margin-left', '0px');
                    $('.left-strip').css('left', '300px');
                }
            });
            $('.pageSidebar-inner').slimScroll({
                height: '100%',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

        }
    }

    $(document).ready(function() {
        leftpanel();
        leftStripHeight();
        calculateWidth();

    });

    $(window).resize(function() {
        leftpanel();
        leftStripHeight();
        calculateWidth();
    });

    $('[data-toggle="tooltip"]').tooltip();
</script>


<div class="accomodationSidebar">
    <h2><i class="icon icon-manage-trip"></i><span>Booking Summary 1</span></h2>
    <div class="panel">
        <div class="panel-inner">
            <div class="row">
                <div class="col-sm-10">
                    <p><strong>Pricing &amp; Travellers</strong></p>
                    <h4><strong>{{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_day'] }} Per Day</strong></h4>
                </div>
                <div class="col-sm-2 daysBlock">
                    <strong>{{$days}}</strong><br/><span>NIGHTS</span>
                </div>
            </div>
            <p>Total Cost {{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_person'] }} (Per Person) Inc. Taxes</p>
            <div class="row">
                <div class="col-sm-6">
                    <?php
                        $travellers = session()->get('search')['travellers'];
                        $total_childs = session()->get('search')['child_total'];
                    ?>
                    <div class="panel-form-group form-group">
                        <label class="label-control">Adults (18+)</label>
                        <input type="text" value="{{$travellers}}" class="form-control disabled" readonly data-toggle="tooltip" title="Not Available in Pilot">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel-form-group form-group">
                        <label class="label-control">Children (0-17)</label>
                        <input type="text" class="form-control disabled" data-toggle="tooltip" readonly title="Not Available in Pilot" value="{{$total_childs}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel">
    <div class="panel-inner">
        <p><strong>Duration &amp; Dates</strong></p>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-form-group form-group">
                    <label class="label-control">Total Trip Duration</label>
                    <input type="text" class="form-control disabled" data-toggle="tooltip" readonly title="Not Available in Pilot" value="{{session()->get('search')['total_number_of_days']}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel-form-group form-group">
                    <label class="label-control">Departure Date</label>
                    <div class="input-group datepicker">
                        <input id="departure_dates" class="disabled" readonly type="text" placeholder="31 Jan 2018" class="form-control" value="{{date('d M Y',strtotime($departure_date))}}" data-toggle="tooltip" title="Not Available in Pilot">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel-form-group form-group">
                    <label class="label-control">Return Date</label>
                    <div class="input-group datepicker">
                        <input id="return_dates" type="text" readonly class="disabled" placeholder="11 Feb 2017" class="form-control" value="{{date('d M Y',strtotime($return_date))}}" data-toggle="tooltip" title="Not Available in Pilot">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-t-10">
    <?php
        switch (session()->get('_previous')['url']) {
        case url('map'):
    ?>
    <a href="{{ url( 'proposed-itinerary' ) }}" name="" class="btn btn-secondary btn-block">VIEW DETAILED ITINERARY</a>
    <a href="{{ url( 'payment-summary' ) }}" name="" class="btn btn-secondary btn-block active">BOOK ITINERARY</a>
    <?php
        break;
        case url('proposed-itinerary'):
    ?>
    <a href="{{ url( 'proposed-itinerary' ) }}" name="" class="btn btn-secondary btn-block active">VIEW DETAILED ITINERARY</a>
    <a href="{{ url( 'payment-summary' ) }}" name="" class="btn btn-secondary btn-block">BOOK ITINERARY</a>
    <?php
        break;
        default:
    ?>
    <a href="{{ url( 'proposed-itinerary' ) }}" name="" class="btn btn-secondary btn-block">VIEW DETAILED ITINERARY</a>
    <a href="{{ url( 'payment-summary' ) }}" name="" class="btn btn-secondary btn-block">BOOK ITINERARY</a>
    <?php
        break;
        }
    ?>
</div>
<div class="m-t-30">
    <p>Click on a segment under Itinerary Summary to edit the accommodation, tours and transport arrangements. To add or remove locations, select the location on the map to the right.</p>
    @if (session()->get('map_data')['type'] == 'manual' && count(session()->get('map_data')['cities']) > 2 && session()->get( '_previous' )['url'] == url( 'map' ))
    <a href="#" id="reorder-locations-btn" class="btn btn-secondary btn-block active"><i class="fa fa-reorder m-r-10"></i> Reorder Locations</a>
    @endif
</div>
</div>
</div>
<hr/>

<div class="accomodationSidebar" id="panel-drag">
    <h2><i class="icon icon-itinerary"></i><span>Itinerary Summary</span></h2>
    <div class="itinerary-container panel-group sortable-list new-panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @if (session()->get('search'))
        @foreach (session()->get('search')['itinerary'] as $key => $leg)
        <?php
            $itineraryIndex = $key == $last ? $key : $key + 1;
            $departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
            $arriveTimezone = get_timezone_abbreviation(session()->get('search')['itinerary'][$itineraryIndex]['city']['timezone']['name']);
            ?>
        <div class="panel panel-default itinerary-leg-container" data-index="{{ $key }}">
            <div class="panel-heading {{ $key == $legkey ? 'panel-open' : '' }}" role="tab" id="heading-{{ $key }}">
                <div class="panel-title">
                    <div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $key }}" aria-expanded="true" aria-controls="collapse-{{ $key }}" class="collapse-title {{ $key != $legkey ? 'collapsed' : '' }}">
                        <i class="icon icon-explore"></i>
                        <div class="city-name"><strong>{{ str_limit($leg['city']['name'], 18) }}, {{ $leg['city']['country']['code'] }}</strong></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 date-control">
                            <div class="panel-form-group form-group">
                                <label class="label-control">Check-in Date:</label>
                                <div class="input-group datepicker">
                                    <input id="start_date_{{ $key }}" type="text" data-leg="{{$key}}" data-cityId="{{ $leg['city']['id'] }}" data-fromDate="{{ $leg['city']['date_from'] }}" data-toDate=" {{ $leg['city']['date_to'] }}" data-nights="{{ $leg['city']['default_nights'] }}" placeholder="{{ date( 'j M Y', strtotime( $leg['city']['date_from'] ) ) }}" class="form-control start_date" value="{{ date( 'j M Y', strtotime( $leg['city']['date_from'] ) ) }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 stay-control">
                            <div class="panel-form-group form-group">
                                <label class="label-control">Length of Stay:</label>
                                <select class="form-control changeNights" id="changeNights" data-leg="{{$key}}" data-cityId="{{ $leg['city']['id'] }}" data-fromDate="{{ $leg['city']['date_from'] }}" data-toDate=" {{ $leg['city']['date_to'] }}">
                                @for($i = 1; $i < 11; $i++)
                                <option value="{{$i}}" {{ ($leg['city']['default_nights'] == $i) ? 'selected' : '' }} > {{ $i }} {{ ($i == 1) ? 'Night': 'Nights' }}</option>
                                @endfor
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="night-data-{{ $key }}" value="{{ $leg['city']['default_nights'] }}">
                    </div>
                </div>
            </div>
            <div id="collapse-{{ $key }}" class="panel-collapse collapse {{ $key == $legkey ? 'in' : '' }}" role="tabpanel" aria-labelledby="heading-{{ $key }}">
                <div class="panel-body">
                    <div class="panel-inner itinerary-leg-link"  data-type="hotels" data-link="{{ url( $leg['city']['name'].'/hotels?leg='.$key ) }}">
                        <div class="panel-icon">
                            <i class="icon icon-hotel"></i>
                        </div>
                        <div class="panel-container">
                            @if( $leg['hotel'] )
                            <p><strong>Accommodation Summary</strong> <br/> {{ $leg['hotel']['name'] }}</p>
                            <div class="row m-t-20">
                                <div class="col-md-4 col-sm-5"><strong>Check-in: </strong></div>
                                <div class="col-md-8 col-sm-7">{{ date('j M Y', strtotime( $leg['city']['date_from'] ) ) }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-5"><strong>Check-out:</strong></div>
                                <div class="col-md-8 col-sm-7">{{ date('j M Y', strtotime( $leg['city']['date_to'] ) ) }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-5"><strong>Duration:</strong></div>
                                <div class="col-md-8 col-sm-7">{{ $leg['hotel']['nights'] }} Nights</div>
                            </div>
                            @else
                            <p><strong>Accommodation Summary</strong> <br/> Own Arrangement</p>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-inner itinerary-leg-link" data-type="activities" data-link="{{ url( $leg['city']['name'].'/activities?leg='.$key ) }}">
                        <div class="panel-icon">
                            <i class="icon icon-activity"></i>
                        </div>
                        <div class="panel-container">
                            <p class="itinerary-activities-name" id="itinerary-activity-{{$key}}"><strong>Activity Summary</strong>
                                @if ( $leg['activities'] )
                                @foreach ( $leg['activities'] as $activity  )
                                <br>{{ $activity['name'] }}
                            <div class="row m-t-20">
                                <div class="col-md-4 col-sm-5">
                                    <strong>Date: </strong>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    {{ date('j M Y', strtotime($activity['date_selected'])) }}
                                </div>
                            </div>
                            <?php
                                if (!isset($activity['duration1'])) {
                                    $activity['duration1'] = $activity['duration'];
                                }
                                if (isset($activity['duration1']) && !empty($activity['duration1'])) { ?>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5">
                                            <strong>Duration:</strong>
                                        </div>
                                        <div class="col-md-8 col-sm-7">
                                            <?php
                                                $activity['duration1'] = str_replace("hour", "Hour", $activity['duration1']);
                                                $activity['duration1'] = str_replace("day", "Day", $activity['duration1']);
                                                $activity['duration1'] = str_replace("minutes", "Minutes", $activity['duration1']);
                                            ?>
                                            {{$activity['duration1']}}
                                        </div>
                                    </div>
                            <?php } ?>
                            @endforeach
                            @else
                            <p>Own Arrangement</p>
                            @endif
                            </p>
                        </div>
                    </div>
                    <hr/>
                    @if ( $leg['transport'] )
                    <?php
                        //echo '<pre>';print_r($leg['transport']);exit();
                        $duration = $leg['transport']['duration'] ? $leg['transport']['duration'] : '';
                        if (!empty($duration)) {
                            if (isset($leg['transport']['provider']) && $leg['transport']['provider'] == 'busbud') {
                                $a = (int) $duration;
                                $hours = floor($a / 60);
                                $minutes = $a % 60;
                                $duration = ceil($hours) . ' Hour(s) and ' . ceil($minutes) . ' Minute(s)';
                            } else {
                                $duration = str_replace("+", "", $duration);
                                $duration = str_replace("hour", "Hour", $duration);
                                $duration = str_replace("minute", "Minute", $duration);
                            }
                        
                        }
                        if (isset($leg['transport']['departure_text']) && !empty($leg['transport']['departure_text'])) {
                            //$leg['transport']['departure_text']
                        }
                        ?>
                    <div class="panel-inner itinerary-leg-link" data-type="transports" data-link="{{ url( $leg['city']['name'].'/transports?leg='.$key ) }}">
                        <div class="panel-icon">
                            <i class="icon icon-bus"></i>
                        </div>
                        <div class="panel-container">
                            <p><strong>Transport Summary</strong> <br/> {{ $leg['transport']['transport_name_text'] }}</p>
                            <div class="row m-t-20">
                                <div class="col-md-4 col-sm-5"><strong>Depart:</strong></div>
                                <div class="col-md-8 col-sm-7">{!! $leg['transport']['departure_text'] !!}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-5"><strong>Arrive:</strong></div>
                                <div class="col-md-8 col-sm-7">{!! $leg['transport']['arrival_text'] !!}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-5"><strong>Duration:</strong></div>
                                <div class="col-md-8 col-sm-7">{!! $duration !!}</div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    @elseif ( last( session()->get('search')['itinerary'] ) != $leg )
                    <div class="panel-inner itinerary-leg-link" data-type="transports" data-link="{{ url( $leg['city']['name'].'/transports?leg='.$key ) }}">
                        <div class="panel-icon">
                            <i class="fa fa-car"></i>
                        </div>
                        <div class="panel-container">
                            <p><strong>Transport Summary</strong> <br/> {{ str_limit( $leg['city']['name'] . ' to ' . session()->get('search')['itinerary'][$key + 1]['city']['name'] , 50 ) }} <br>(Self-Drive / Own Arrangement)</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
    <div id="reorder-locations-container" style="display:none;">
        <span class="reorder-btn">Drag <i style="margin: 0 .5rem" class="fa fa-arrows"></i> locations to reorder.</span>
        <div class="row m-t-10">
            <div class="col-sm-6">
                <a href="javascript://" class="btn btn-primary btn-block" id="save-order-btn">Save</a>
            </div>
            <div class="col-sm-6">
                <a href="javascript://" class="btn btn-primary btn-block" id="cancel-reorder-btn">Discard</a>
            </div>
        </div>
        <br>
        <ul id="reorder-locations">
            @if (session()->get('search'))
            @foreach (session()->get('search')['itinerary'] as $key => $leg)
            <li class="ui-state-default panel-heading" data-city="{{ $leg['city']['id'] }}">
                <div class="panel-icon"><i class="icon icon-location" style="font-size:1.4em;"></i></div>
                <div class="panel-container"><span class="bold-txt">{{ $leg['city']['name'] }}, {{ $leg['city']['country']['code'] }}</span></div>
            </li>
            @endforeach
            @endif
        </ul>
    </div>
</div>


<div class="modal fade no-hotel-summary1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header " style="display: none">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Sorry, you can't add additional hotel nights because there's no hotel selected.</p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 pull-right">
                        <button type="button" class="btn confirm-cancel-button eroam-confirm-cancel-btn" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{ url( 'js/theme/jquery.slimscroll.js' ) }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ url( 'js/theme/bootstrap-datepicker.js' ) }}"></script>
<script type="text/javascript">
    var isMap = "{{ $isMap }}";

    $(document).ready(function() {

        var totalLeg = "{{ $last }}";

        for (var i = 0; i <= totalLeg; i++) {
            $("#start_date_" + i).datepicker({
                format: 'd M yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: '+1d'
            }).on('changeDate', function(selected) {
                var selectDate = new Date(selected.date.valueOf());
                var leg = $(this).attr('data-leg');
                var city_id = $(this).attr('data-cityId');

                eroam.ajax('get', 'latest-search', {}, function(response) {
                    if (response) {
                        search_session = JSON.parse(response);
                        var session = search_session.itinerary;

                        if (session.length > 0) {
                            if (session[leg].city.id == city_id) {
                                if (leg == 0) {
                                    var from_date = selectDate.getFullYear() + '-' + ('0' + (selectDate.getMonth() + 1)).slice(-2) + '-' + ('0' + selectDate.getDate()).slice(-2);
                                    search_session.travel_date = from_date;


                                } else {
                                    var From_date = new Date($("#start_date_0").attr('data-fromdate'));
                                    var To_date = new Date($("#start_date_" + leg).attr('data-fromdate'));
                                    var diff_date = To_date - From_date;
                                    var legDays = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
                                    var from_date = deductDays(selectDate, legDays);

                                    search_session.travel_date = from_date;
                                }

                                if (search_session.itinerary[leg].activities != null) {
                                    var act = sortByDate(search_session.itinerary[leg].activities);
                                    var From_date = new Date($("#start_date_" + leg).attr('data-fromdate'));

                                    if (From_date > selectDate) {
                                        var diff_date1 = From_date - selectDate;
                                        var deduct = Math.floor(((diff_date1 % 31536000000) % 2628000000) / 86400000);
                                        var value = $("#start_date_" + leg).attr('data-nights')
                                        var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                        activityCancellation(activityDates, leg, act, value);
                                    } else {
                                        bookingSummary.update(JSON.stringify(search_session));
                                    }
                                } else {
                                    bookingSummary.update(JSON.stringify(search_session));
                                }
                            }
                        }
                    }
                });
            });
        };

        $('.no-hotel-cancel-btn').one('click', function(e) {
            e.preventDefault();
            $('#eroam-confirm').modal('hide');
        });
    });

    $(function() {
        $("#departure_date").datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $("#return_date").datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('.helpBox').find('.row').slimScroll({
            height: '970px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });

        $('.calendarList12').slimScroll({
            height: '680px', //535px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });

        $('#hotel-list, #activity-list, .iternary-box, .payment-page').slimScroll({
            height: '99%',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
        $('.transportWrapper').slimScroll({
            height: '100%',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
        if (isMap == 1) {
            $('.pageSidebar-inner').slimScroll({
                height: '1000px',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });
        }

        $('#edit-map-btn').click(function(event) {
            $('#edit-map-container').show();
            $('#edit-map-overlay').fadeIn(300);
        });

        $('body').on('click', '#reorder-locations-btn', function(e) {
            e.preventDefault();
            $('.itinerary-container, #reorder-locations-btn').hide();
            $('#reorder-locations-container').show();
            $('#reorder-locations').sortable({
                items: 'li:not(li:first-child)',
                placeholder: 'ui-state-highlight',
                opacity: 0.7,
                scrollSensitivity: 50,
                forcePlaceholderSize: true,
                over: function(e, ui) {
                    ui.placeholder.height(ui.item.height() - 30);
                }
            });
        });

        $('.changeNights').change(function() {

            var leg = $(this).attr('data-leg');
            var value = $(this).val();
            var city_id = $(this).attr('data-cityId');
            var from_date = $(this).attr('data-fromDate');
            var to_date = $(this).attr('data-toDate');

            eroam.ajax('get', 'latest-search', {}, function(response) {
                if (response) {
                    search_session = JSON.parse(response);
                    var session = search_session.itinerary;

                    if (session.length > 0) {
                        if (session[leg].city.id == city_id) {

                            var nights = search_session.itinerary[leg].city.default_nights;

                            if ((search_session.itinerary[leg].hotel != null) && typeof search_session.itinerary[leg].hotel.nights != 'undefined') {

                                nights = search_session.itinerary[leg].hotel.nights;
                                search_session.itinerary[leg].hotel.nights = value;

                                var days_to_add = parseInt(value) - parseInt(nights);

                                if (days_to_add > 0) {
                                    search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) + days_to_add;
                                    search_session.itinerary[leg].city.days_to_add = days_to_add;
                                    search_session.itinerary[leg].city.add_after_date = from_date;
                                    bookingSummary.update(JSON.stringify(search_session));
                                } else {

                                    var deduct = Math.abs(days_to_add);
                                    var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

                                    search_session.itinerary[leg].city.days_to_deduct = deduct;
                                    search_session.itinerary[leg].city.deduct_after_date = from_date;
                                    search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

                                    if (search_session.itinerary[leg].activities != null) {

                                        var act = sortByDate(search_session.itinerary[leg].activities);
                                        var departureDate = getDeparture(leg, from_date, to_date);
                                        departureDate = departureDate.split(' ');


                                        var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                        alert(departureDate + '==//==' + activityDates);
                                        activityCancellation(activityDates, leg, act, value);
                                    } else {
                                        bookingSummary.update(JSON.stringify(search_session));
                                    }
                                    return;
                                }
                            } else {
                                $('.no-hotel-summary').modal();
                            }
                        }

                    }
                }
            });
        });
    })

    function activityCancellation(activityDates, itineraryIndex, act, value) {

        if (activityDates.length > 0) {

            eroam.confirm(null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e) {

                if (act.length > 0) {
                    for (index = act.length - 1; index >= 0; index--) {
                        var actDuration = parseInt(act[index].duration);
                        var multiDayRemoved = false;
                        if (actDuration > 1) {
                            actDuration = actDuration - 1;
                            var activityDateSelected = act[index].date_selected;
                            for (var counter = 1; counter <= actDuration; counter++) {
                                var multiDayActivityDate = addDays(activityDateSelected, counter);

                                if (activityDates.indexOf(multiDayActivityDate) > -1 && !multiDayRemoved) {
                                    act.splice(index, 1);
                                    multiDayRemoved = true;
                                }
                            }
                        } else {
                            if (activityDates.indexOf(act[index].date_selected) > -1) {
                                act.splice(index, 1);
                            }
                        }

                    }
                }

                search_session.itinerary[itineraryIndex].activities = act;
                bookingSummary.update(JSON.stringify(search_session))
            });

        } else {
            bookingSummary.update(JSON.stringify(search_session));
        }
    }

    function getActivityToBeCancelledByDates(dateTo, numberOfDays) {
        var dateArray = [];
        if (numberOfDays) {
            dateArray.push(dateTo);
            for (var count = 1; count <= numberOfDays; count++) {
                dateArray.push(deductDays(dateTo, count))
            }
        }
        return dateArray;
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + parseInt(days));
        return formatDate(result);
    }

    function deductDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() - parseInt(days));
        return formatDate(result);
    }

    function formatTime(time) {
        return time.replace(/hr\(s\)|min\(s\)|hours|minutes /gi, function(x) {
            return x == 'hr(s)' || 'hours' ? ':' : '';
        });
    }

    function get_hours_min(hour_min) {
        var hours_mins = hour_min.toString().split(".");
        var hours = parseInt(hours_mins[0]);
        var mins = parseFloat('0.' + hours_mins[1]) * 60;
        return hours + ' hr(s) ' + Math.ceil(mins) + ' min(s)';
    }

    function removePlus(time) {
        return time.replace(/\+/g, '');
    }

    function padd_zero(number) {
        if (parseInt(number) == 0) {
            number = 12;
        }
        return (number < 10) ? ("0" + number) : number;
    }

    function arrival_am_pm(eta) {
        var result;
        var arrival_split = eta.split("+");
        var arrival_split_am_pm = arrival_split[0].split(" ");
        var arrival_split_hour_min = arrival_split[0].split(":");
        var arrival_hour = parseInt(arrival_split_hour_min[0]);
        var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


        if (typeof arrival_split_am_pm[1] !== 'undefined') {

            result = padd_zero(arrival_hour) + ':' + arrival_min + ' ' + arrival_split_am_pm[1];
        } else {
            if (arrival_hour < 12) {
                result = padd_zero(arrival_hour) + ':' + arrival_min + ' AM';
            } else {
                hour = arrival_hour - 12;
                result = padd_zero(hour) + ':' + arrival_min + ' PM';
            }
        }

        return result;
    }

    function getDayTime(arrivalDate, duration) {
        var hours_mins = duration.split(':');
        var result = new Date(arrivalDate);
        result.setHours(result.getHours() - parseInt(hours_mins[0]));
        result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));
        return formatDate(result, true);
    }

    function getDeparture(itineraryIndex, from_date, to_date) {
        var search_session = JSON.parse($('#search-session').val());
        var newDateTo = addDays(from_date, parseInt(search_session.itinerary[itineraryIndex].city.default_nights));
        var transport = search_session.itinerary[itineraryIndex].transport;

        var departureDate = to_date + ' 24';
        if (transport != null) {
            var eta = transport.eta;
            var travelDuration = transport.duration;
            if (transport.provider == 'mystifly') {

                travelDuration = formatTime(get_hours_min(travelDuration));
                eta = moment(eta, moment.ISO_8601);
                eta = eta.format('hh:mm A');

            } else {
                travelDuration = removePlus(travelDuration);
                travelDuration = formatTime(travelDuration);
            }

            var arrivalTime = arrival_am_pm(eta);
            departureDate = getDayTime(newDateTo + ' ' + arrivalTime, travelDuration);
        }
        return departureDate;
    }

    function leftpanel() {
        var w = $(document).width();
        if (w > 991) {
            $('.arrow-btn-new').click(function() {
                if ($('.arrow-btn-new').hasClass('open')) {
                    $('.page-sidebar').css('display', 'none');
                    $(this).removeClass('open');
                    $('.page-content').css('margin-left', '0px');
                } else {
                    $('.page-sidebar').css('display', 'block');
                    $(this).addClass('open');
                    $('.page-content').css('margin-left', '354px');
                }
            });
        } else {
            $('.arrow-btn-new').click(function() {
                if ($('.arrow-btn-new').hasClass('open')) {
                    $('.page-sidebar').css('display', 'none');
                    $(this).removeClass('open');
                    $('.page-content').css('margin-left', '0px');
                    $('.left-strip').css('left', '0px');
                } else {
                    $('.page-sidebar').css('display', 'block');
                    $(this).addClass('open');
                    $('.page-content').css('margin-left', '0px');
                    $('.left-strip').css('left', '300px');
                }
            });
        }
    }

    function calculateHeight() {
        var winHeight = $(window).height();

        var oheight = $('.page-sidebar').outerHeight(); // -22
        var oheight1 = $('.page-sidebar').outerHeight();

        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);

        var elemHeight1 = oheight1 - elem;
        var winelemHeight = winHeight - elem;

        if (winHeight < oheight) {
            $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);
            $(".page-content .tabs-content-wrapper1").outerHeight(elemHeight);
            $(".page-content .tabs-content-wrapper2").outerHeight(oheight1);
        } else {
            $(".page-content .tabs-content-wrapper").outerHeight(winelemHeight);
            $(".page-content .tabs-content-wrapper1").outerHeight(winelemHeight);
            $(".page-content .tabs-content-wrapper2").outerHeight(winelemHeight);
        }
        
        $(".page-content .hotel-container").outerHeight(elemHeight);
        var hotelTop = $('.page-content .hotel-top-content').outerHeight();
        var hotelTabs = $('.page-content .custom-tabs').outerHeight() + 120;
        var hotelHeight = (elemHeight - hotelTop) - hotelTabs;
        var winHotelHeight = (winelemHeight - hotelTop) - hotelTabs;

        if (winHeight < oheight1) {
            $(".page-content .hotel-container").outerHeight(elemHeight1);
            $(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
        } else {
            $(".page-content .hotel-container").outerHeight(winelemHeight);
            $(".page-content .hotel-bottom-content").outerHeight(winHotelHeight);
        }
    }

    $(document).ready(function() {
        leftpanel();
        calculateHeight();
        leftStripHeight();
        $('.panel-heading .collapse-title').click(function() {
            $('.panel-heading').removeClass('panel-open');

            //If the panel was open and would be closed by this click, do not active it
            if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
                $(this).parents('.panel-heading').addClass('panel-open');
        });

    });

    $(window).resize(function() {
        leftpanel();
        calculateHeight();
        leftStripHeight();
    });

    function displayPrice(display) {
        if (display == 'perperson') {
            $("#perpersontotal").removeClass("active");
            $("#perperson").addClass("active");
            $('#perpersonCost').show();
            $('#totalCost').hide();

        } else {
            $("#perpersontotal").addClass("active");
            $("#perperson").removeClass("active");
            $('#perpersonCost').hide();
            $('#totalCost').show();
        }
    }

    function formatDate(date, time = false) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (time) {
            day += ' ' + d.getHours();
        }
        return [year, month, day].join('-');
    }
</script>