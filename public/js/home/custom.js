$("#project").autocomplete({
        minLength: 2,
        source: countries,
        focus: function(event, ui) {
            $("#project").val(ui.item.label);
            return false;
        },
        select: function(event, ui) {

            var displayVal = ui.item.label;
            $("#project").val(displayVal);

            $('#searchValType').val(ui.item.type);
            $('#searchVal').val(ui.item.value);
            $('#countryId').val(ui.item.cid);
            $('#countryName').val(ui.item.cname);
            $('#countryRegion').val(ui.item.rid);
            $('#countryRegionName').val(ui.item.rname);

            $('#countryId').val(ui.item.value);
            $('#countryRegion').val(ui.item.rid);
            $('#countryRegionName').val(ui.item.rname);

            $('#starting_country').val('');
            $('#starting_city').val('');

            $('#destination_country').val('');
            $('#destination_city').val('');
            return false;
        },
        response: function(event, ui) {
            if (ui.content.length == 0) {
                project_search_cnt = ui.content.length;
            } else {
                project_search_cnt = 1;
            }
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
            .append("<div>" + item.label + "</div>")
            .appendTo(ul);
    };

$("#start_location").autocomplete({
        minLength: 2,
        source: projects,
        focus: function(event, ui) {
            $("#start_location").val(ui.item.label);
            return false;
        },
        select: function(event, ui) {
            var displayVal = ui.item.label + ', ' + ui.item.desc;
            $("#start_location").val(displayVal);
            var option = $(".radio_tailor:checked").val();
            if (option == "packages" || option == "manual") {
                $('#starting_country').val(ui.item.cid);
                $('#starting_city').val(ui.item.value);
            } else {
               $('#starting_country').val(ui.item.cid);
                $('#starting_city').val(ui.item.value);
            }

            return false;
        },
        response: function(event, ui) {
            if (ui.content.length == 0) {
                start_location_search_cnt = ui.content.length;
            } else {
                start_location_search_cnt = 1;
            }
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
            .append("<div>" + item.label + ", " + item.desc + "</div>")
            .appendTo(ul);
    };

$(document).ready(function() {
    $("#start_location").val('');
    $("#end_location").val('');

});


$("#end_location").autocomplete({
    minLength: 2,
    source: projects,
    focus: function(event, ui) {
        $("#end_location").val(ui.item.label);
        return false;
    },
    select: function(event, ui) {
        var displayVal = ui.item.label + ', ' + ui.item.desc;
        $("#end_location").val(displayVal);
        $("#destination_city").val(ui.item.value);
        $("#destination_country").val(ui.item.cid);
        return false;
    },
    response: function(event, ui) {
        if (ui.content.length == 0) {
            end_location_search_cnt = ui.content.length;
        } else {
            end_location_search_cnt = 1;
        }
    }
}).autocomplete("instance")._renderItem = function(ul, item) {
    return $("<li>")
        .append("<div>" + item.label + ", " + item.desc + "</div>")
        .appendTo(ul);
};

var sform = $("#search-form");
sform.validate({
    ignore: [],
    errorPlacement: function (label, element) {
			if(element.hasClass('tour_checkbox')){
				label.insertAfter(element.closest('span'));
			}
			else{
				label.insertAfter(element);
			}
        },
    rules: {
        project:{
            required: true
        },
        'tour_type[]':{
            required:true,
            maxlength: 2
        }
    }
});

$("#search-form2").validate({
    ignore: [],
    rules: {
        end_location:{
            required: true
        },
        start_location: {
            required: function(element) {
                if($("input[class='radio_tailor']:checked").val() != undefined) {
                    if($("input[class='radio_tailor']:checked").val() == 'auto') {
                        return true;
                    }else{
                        return false;
                    }
                }else{
                  return true;
                }
            }
        },
        start_date: {
            required: true
        },
        room_select :{
            required: true
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
    }
});

$(document).on('change','.radio_tailor', function(){
	$('.location_input').nextAll().remove();
	//$("form#search-form2")[0].reset();
});

 $(document).on('change', '.num_of_adult', function(){
     var total_adult = 0;
     $('.num_of_adult').map(function(){              
         total_adult = total_adult + parseFloat(this.value);
     }).get();

     if(total_adult <= 14)
     {
         $('#number_of_adults').val(total_adult);
     }
     else
     {
         var lastValue = $('#number_of_adults option:last-child').val();            
         if(lastValue != 14)
         {
             $("#number_of_adults option[value='"+lastValue+"']").remove();
         }
         $('#number_of_adults').append($("<option selected></option>").attr("value",total_adult).text(total_adult));
     }

     var selected_val = $(this).val();
     $("#total_room_adults").val(total_adult);
 });

 $(document).on('change','#number_of_adults', function(){
     var selected_val = $(this).val();
     $("#total_room_adults").val(selected_val);
 });

 $(document).on('change','#number_of_child', function(){
     var selected_val = $(this).val();
     $("#total_room_child").val(selected_val);
 });
 

 $(document).on('change', '.num_of_child ', function(){
     var total_adult = 0;
     $('.num_of_child').map(function(){              
         total_adult = total_adult + parseFloat(this.value);
     }).get();
     $("#total_room_child").val(total_adult);
     if(total_adult <= 6)
     {
         $('#number_of_child').val(total_adult);
     }
     else
     {
         var lastValue = $('#number_of_child option:last-child').val();            
         if(lastValue != 6)
         {
             $("#number_of_child option[value='"+lastValue+"']").remove();
         }
         $('#number_of_child').append($("<option selected></option>").attr("value",total_adult).text(total_adult));
     }
 });

 $(document).on('change', '.room_select', function(){
     var total_room = $(this).val();
     var html = '';
     var i;
     if(total_room > 1)
     {
         $(".children_selectbox").html('');
         $(".children-block").hide();
         $('#number_of_child').val(0);

        /* $(".single_room").hide();
         $(".children-block").hide();*/
         $(".itinery_person_info").html('');
         $("#number_of_child").attr("disabled", true);
         $("#number_of_adults").attr("disabled", true);
         $("#number_of_adults").attr('name', 'total_adults');
         $("#number_of_child").attr('name', 'total_childs');           

         $('#number_of_adults').val(total_room);
         $("#total_room_adults").val(total_room);
         for(i = 1 ; i <= total_room ; i++)
         {
             html += '<div class="m-t-20"><div class="row"><div class="col-sm-4"><p><span class="icon-hotel m-r-10"></span><strong>Room '+i+'</strong></p><div class="row"><div class="col-sm-6"><div class="panel-form-group form-group"><label class="label-control">Number of Adults (18+)</label><select class="form-control num_of_adult" name="num_of_adults[]">';
             for(k=1 ; k<= 14; k++)
             {
                 html += '<option value="'+k+'">'+k+'</option>';
             }
             html += '</select></div></div><div class="col-sm-6"><div class="panel-form-group form-group"><label class="label-control">Number of Children (0-17)</label><select class="form-control children_append num_of_child" data-id="'+i+'" name="num_of_children[]"><option value="0">No Children</option>';
             for(j = 1 ; j <= 6; j++)
             {
                
                 html += '<option value="'+j+'">'+j+'</option>';
                 
             }
             html += '</select></div></div></div></div><div class="col-sm-8 child_age_'+i+'" style="display:none"><div class="left-spacing"><div><span class="smile-icon"><i class="icon-child"></i></span><label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a></div><div class="row child_append_'+i+'"></div></div></div></div></div>';
         }
         $(".itinery_person_info").append(html);
     }
     else
     {
         $("#number_of_child").attr("disabled", false);
         $("#number_of_adults").attr("disabled", false);
         
         $("#number_of_adults").attr('name', 'num_of_adults[]');
         $("#number_of_child").attr('name', 'num_of_children[]');

         $('#number_of_adults').val(1);
         $('#number_of_child').val(0);

         var lastValue = $('#number_of_child option:last-child').val();            
         if(lastValue != 6)
         {
             $("#number_of_child option[value='"+lastValue+"']").remove();
         }

         var lastValue = $('#number_of_adults option:last-child').val();            
         if(lastValue != 14)
         {
             $("#number_of_adults option[value='"+lastValue+"']").remove();
         }
         $(".itinery_person_info").html('');           
         var totalAdult = $("#number_of_adults").val();
         var totalChild = $("#number_of_child").val();
         $("#total_adults").html('<input type="hidden" name="travellers" id="total_room_adults" value="'+totalAdult+'" ><input type="hidden" name="total_children" id="total_room_child" value="'+totalChild+'" >');
         
     }
 });
 
 

 $(document).on('change', '.children_append', function(){
     var div_name = $(this).attr("data-id");
     var total_child = $(this).val();
     child_html = '';
     if(total_child == 0)
     {
         $(".child_age_"+div_name).hide();
     }
     else
     {
         $(".child_age_"+div_name).show();
     }

     $(".child_append_"+div_name).html('');
     
     for(i = 1 ; i <= total_child ; i++)
     {
         var index_val = div_name-1;
         var id = '['+index_val+']['+i+']';
         var template = jQuery.validator.format($.trim($("#addChild").html()));
         $(template(id)).appendTo(".child_append_"+div_name);
         $('.netChild').each(function () {
             $(this).rules("add", {
                 required: true
             });
         });
     }
 });

 $(document).on('change', '.child_select', function(e){
     
     var total_child = $(this).val();
     child_html = '';
     
     if(total_child == 0)
     {
         $(".children-block").hide();
     }
     else
     {
         $(".children-block").show();
         $(".children_selectbox").html('');
         var j = 1;
         for(i = 1 ; i <= total_child ; i++)
         {
             var id = '[0]['+i+']';
             var template = jQuery.validator.format($.trim($("#addChild").html()));
             $(template(id)).appendTo(".children_selectbox");
             $('.netChild').each(function () {
                 $(this).rules("add", {
                     required: true
                 });
             });
             e.preventDefault();                
         }
     }
 });