<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>iJurni</title>
    <style type="text/css">
        #booking-details tr td{padding: 8px; line-height: 1.42857143; vertical-align: top;}
        #booking-details{border-collapse: collapse;}
       /* #booking-details tr:nth-child(even)>td {background-color: #F1F1F1;}*/
    </style>
</head>
<body style="margin: 0; padding: 0; color: #505050; width:760px; margin: 0 auto; font-family: arial;">
  <table width="100%" style="margin: 0 auto; border: solid 1px #ddd;" cellspacing="0" cellpadding="0">
    <tr>
    	<td>
    		<table width="100%" id="header" style="vertical-align: middle; text-align: center; background-color: #212121; padding: 5px 0px 1px 0;">
			    <tr>
			    	<td>
                        <img src="{{ url( 'images/eRoam_LogoNew.png' ) }}" alt="" width="120px"/>
			    	</td>
			    </tr>
		  </table>
    	</td>
    </tr>
    <tr>
    	<td>
    		<table width="70%" style="margin: 10px auto 0 auto; font-size: 15px; border-collapse: collapse;">
			    <tr>
			    	<td style="border-top: none; padding: 0px;">
			    		<h3 style="margin-bottom: 30px;">Hello {{$name}},</h3>
			    		<p>We have received your funds and it is being processed by Adventure Travel. One of our friendly staff will contact you within the next 24 hours with your confirmation number.</p>
                        <p style="margin-bottom: 20px;">Please find your booking details below,</p>
                        <table style="width: 100%;border-collapse: collapse;" id="booking-details" border="0">
                              <tbody>
                                <tr>
                                  <td colspan="2" style="border-top: none;">
                                      <table width="100%" style="border-collapse: collapse;">
                                          <tr>
                                            <td style="padding: 0;padding: 8px; line-height: 1.42857143; vertical-align: top;"><strong>Transaction Number:</strong> {{$booking_id}}</td>
                                          </tr>
                                      </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2" style="border: none;">
                                    <table width="100%" style="border-collapse: collapse;">
                                      <tr>
                                        <th style="border: 1px solid #ddd; text-align: left; padding: 5px;">Details</th>
                                        <th style="border: 1px solid #ddd; text-align: center; padding: 5px;">Quantity</th>
                                        <th style="border: 1px solid #ddd; text-align: right; padding: 5px;">Amount</th>
                                      </tr>
                                     
                                    <tr>
                                      <td width="50%" style="border: 1px solid #ddd;">{{$tour_title}}</td>
                                      <td style="border: 1px solid #ddd; text-align: center">{{$total_pax}}</td>
                                      <td id="singaltotal" style="text-align: right; border: 1px solid #ddd;">AUD {{$total_amount}}</td>
                                    </tr>
                                   
                                    </table>
                                  </td>
                                </tr>
                               
                              </tbody>
                            </table>
			    	</td>
			    </tr>
                <tr>
                    <td>
                        <p style="margin-top: 30px;">If you have any query, please <a href="https://www.corporate.eroam.com/contact-us/" style="color: #F79E1B; text-decoration: none;">Contact Us</a>.</p>
                        <p style="margin-top: 50px;"><strong>Thank You</strong>, <br/>eRoam</p>
                    </td>
                </tr>
		  </table>
    	</td>
    </tr>
      <tr>
          <td>
              <table width="100%" id="footer" style="color: #fff; padding: 9px 10px 6px 10px; vertical-align: middle; text-align: center; background-color: #212121;">
                  <tr>
                      <td><img src="{{ url( 'images/footer-logoNew.png' ) }}" alt="" style="width: 100px;"/></td>
                      <td style="font-size: 14px; text-align: right; font-size: 13px;">
                          Powered by eRoam © Copyright 2016 - 2018. All Rights Reserved. Patent pending AU2016902466
                      </td>
                  </tr>
              </table>
          </td>
      </tr>
  </table>
</body>
</html>

