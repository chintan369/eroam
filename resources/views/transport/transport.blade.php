@extends('layouts.common')
@php 
    $preferences = session()->get('travel_preferences');
    $transport_options = 'all'; 
    
    if(isset($preferences[0]['accommodation'][0])){
        if($preferences[0]['accommodation'][0] == 1){
            $transport_options = 1; 
        }elseif($preferences[0]['accommodation'][0] == 2){
            $transport_options = 2;
        }
    }
@endphp
@section('content')
    <div class="body_sec">
        <div class="itinerary_block">
            <input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
            <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
            <div class="booking-summary">
                <span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.update_data_message') ...</span>
            </div>
            @include('transport.partials.transport')
            
        </div>
    </div>
    <input type="hidden" id="fl-opt-json" value="{{ json_encode($data['flight_options']) }}">

@endsection
@push('scripts') 
    <script src="{{url('js/api-aot.js')}}"></script>
    <script src="{{url('js/api-mystifly.js')}}"></script>
    <script src="{{url('js/api-hb.js')}}"></script>
    <script src="{{url('js/api-ae.js')}}"></script>
    
    <script type="text/javascript">
        var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
        var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}";
        var listOfCurrencies = JSON.parse($('#currency-layer').val());
        @if( session()->has('search_input') )
        @php $input = session()->get('search_input'); @endphp
        var travel_pref = ["{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}"];
        @else
        var travel_pref = [];
        @endif
    </script>
    <script src="{{url('js/itinerary/common.js')}}"></script>
    <script src="{{url('js/booking-summary.js')}}"></script>
    <script src="{{url('js/moment.js')}}"></script>
    <script src="{{url('js/moment-timezone.js')}}"></script>
    <script>
        var transport_count = 0;
        var selectedTransportId = "{{ $data['selected_transport_id'] }}";
        var selectedProvider = "{{ $data['selected_provider'] }}";

        var maxPrice = 0;
        var search_session = JSON.parse( $('#search-session').val() );

        var key = "{{ $data['key'] }}";
        var flightOptions = JSON.parse( $('#fl-opt-json').val() );
        var eroam_data = {
            city_ids: ['{{ $data['city_id'] }}'],
            date_from: formatDate("{{ $data['date_from'] }}"),
            date_to: formatDate("{{ $data['date_to'] }}")
        };
        var flightsExist        = false;
        var availableTransTypes = [];
        var totalTransportCount = 0;
        var activityDates = ["{!! join('", "', $data['activity_dates']) !!}"];

        $(document).ready(function(){
            buildTransports();

            var changeRangeField = function(){
            
                var id = $(this).attr('id');
                var min = parseInt( eroam.convertCurrency($('#price-from').val(), globalCurrency) ) ? parseInt( eroam.convertCurrency($('#price-from').val(), globalCurrency) ) :  parseInt( eroam.convertCurrency(0, 'AUD') );
                var max = parseInt( eroam.convertCurrency($('#price-to').val(), globalCurrency) ) ? parseInt( eroam.convertCurrency($('#price-to').val(), globalCurrency) ) :  parseInt( eroam.convertCurrency(maxPrice, 'AUD') );
                var value = parseInt( eroam.convertCurrency($(this).val(), globalCurrency) );

                var sliderMin = parseInt( eroam.convertCurrency(0, 'AUD') );
                var sliderMax = parseInt( maxPrice );

                if( !isNaN(value) ){

                    if (id == 'price-from') {
                        if(value <= max){
                            $( '#slider-range' ).slider({
                                range: true,
                                min: sliderMin,
                                max:  sliderMax,
                                values: [ value, max ]
                            });
                        }
                    }else{
                        if(value >= min){
                            $( '#slider-range' ).slider({
                                range:true,
                                min: sliderMin,
                                max: sliderMax,
                                values: [ min, value ]
                            });
                        }
                    }
                }
            }
            $('.range-field').bind('keyup', changeRangeField);

            var validateRange = function(){
                var from = parseInt($('#price-from').val());
                var to = parseInt($('#price-to').val());
                var value = $(this).val();
                var id = $(this).attr('id');
                if( !isNaN(value) ){
                    if (id == 'price-from') {
                        if(from > to){
                            $('#error_range').text('Invalid Price Range').fadeIn('slow');
                            $('#price-from').val(0);
                            $( '#slider-range' ).slider({
                                range: true,
                                min: 0,
                                max:  maxPrice,
                                values: [ 0, to ]
                            });
                            setTimeout(function(){ 
                                $('#error_range').fadeOut('slow');
                            }, 3000);
                        }
                    }else{
                        if(to < from){
                            $('#error_range').text('Invalid Price Range').fadeIn('slow');
                            $('#price-to').val(maxPrice);
                            $( '#slider-range' ).slider({
                                range: true,
                                min: 0,
                                max:  maxPrice,
                                values: [ from, maxPrice ]
                            });
                            setTimeout(function(){ 
                                $('#error_range').fadeOut('slow');
                             }, 3000);
                        }
                    }
                }else{
                    if (id == 'price-from') {
                        $('#error_range').text('Invalid Price').fadeIn('slow');
                        $('#price-from').val(0);
                        $( '#slider-range' ).slider({
                            range: true,
                            min: 0,
                            max:  maxPrice,
                            values: [ 0, to ]
                        });
                        setTimeout(function(){ 
                            $('#error_range').fadeOut('slow');
                        }, 3000);
                    }else{
                        $('#error_range').text('Invalid Price').fadeIn('slow');
                        $('#price-to').val(maxPrice);
                        $( '#slider-range' ).slider({
                            range: true,
                            min: 0,
                            max:  maxPrice,
                            values: [ from, maxPrice ]
                        });
                        setTimeout(function(){ 
                            $('#error_range').fadeOut('slow');
                         }, 3000);
                        
                    }
                }
            }
            $('.range-field').focusout(validateRange);

            var arrowSelect = function(event) {
                var numOfSuggestions = $('.suggestions').length-1;
                var selected = -1;
                var items = $('.suggestions');
                var current = $('.selected-suggestion').index();
        
                if(event.which == $.ui.keyCode.UP) {
                    if(current == -1){
                        $(items[numOfSuggestions]).addClass('selected-suggestion');
                        var value = $(items[numOfSuggestions]).text();
                        if(value != '' && value != null){
                            $('.search-field').val(value);
                        }
                    }else{
                        var next = current - 1;
                        next = next < 0 ? numOfSuggestions : next;
                        $(items[current]).removeClass('selected-suggestion');
                        $(items[next]).addClass('selected-suggestion');
                        var value = $(items[next]).text();
                        if(value != '' && value != null){
                            $('.search-field').val(value);
                        }
                    }
                }
                else if(event.which == $.ui.keyCode.DOWN){
                    if(current == -1){
                        $(items[0]).addClass('selected-suggestion');
                        var value = $(items[0]).text();
                        if(value != '' && value != null){
                            $('.search-field').val(value);
                        }
                    }else{
                        var next = current + 1;
                        next = next > numOfSuggestions ? 0 : next;
                        $(items[current]).removeClass('selected-suggestion');
                        $(items[next]).addClass('selected-suggestion');
                        var value = $(items[next]).text();
                        if(value != '' && value != null){
                            $('.search-field').val(value);
                        }
                    }
                }
                else{
                    $('#suggestion-container').find('ul').html('');
                    var priceFrom  = parseFloat($('#price-from').val());
                    var priceTo = parseFloat($('#price-to').val());
                    var pattern = new RegExp($('.search-field').val().toString(), 'gi');
                    $('.transport-list').filter(function() {

                        var list = '<li class="suggestions">'+$(this).attr('data-transport-operator')+'</li>'; 
                        var transPrice = parseFloat( Math.ceil( $(this).attr('data-price') ).toFixed(2) );

                        if( transPrice >= priceFrom && transPrice <= priceTo ){
                            if($(this).attr('data-transport-operator').match(pattern) != null){
                                if ( !$('ul li.suggestions:contains("'+$(this).attr('data-transport-operator')+'")').length ) {
                                    $('#suggestion-container').find('ul').append(list);
                                }
                            }
                        }else{
                            if( $(this).attr('data-transport-operator').match(pattern) != null){
                                if ( !$('ul li.suggestions:contains("'+$(this).attr('data-transport-operator')+'")').length ) {
                                    $('#suggestion-container').find('ul').append(list);
                                }
                            }
                        }
                    });
                }
            }

            /*
            | Added by junfel 
            | function for showing suggestion on keydown and selecting suggestion through arrow down and arrow up
            */
            $('.search-field').bind('keydown', arrowSelect);
            /*
            | Added by Junfel
            | function for clearing search and display all transports
            */
            $('#clear-search').click(function(){
                var min = parseInt(eroam.convertCurrency(0, 'AUD'));
            
                var max =  parseInt(maxPrice);
                $( '#slider-range' ).slider({
                    range: true,
                    min: min,
                    max: max,
                    values: [ min, max ]
                });
                $('#price-from').val(min);
                $('#price-to').val(max);

                $('.transport-list').show();
                $('.search-field').val('');
                $('#search-form').submit();
            });
            /*
            | Added by Junfel
            | function for selecting suggestion through mouse click
            */
            $('body').on('click', '.suggestions', function(){
                var value = $(this).text();
                $('.search-field').val(value);
                $('#search-form').submit();
            });
            /*
            | Added by Junfel 
            | Function for searching transports in the selected City
            */
            $('#search-form').submit(function(){
                /*
                | search pattern
                | check all possible matches and case insensitive
                */
                var priceFrom  = parseFloat($('#price-from').val());
                var priceTo = parseFloat($('#price-to').val());
        
                $('.transport-list').show();
                $('#suggestion-container').find('ul').html('');
                var pattern = new RegExp($('.search-field').val().toString(), 'gi');
                $('.transport-list').filter(function() {
                    //if (price_range) {
                    var transPrice = parseFloat($(this).attr('data-price'));
                    if( transPrice >= priceFrom && transPrice <= priceTo ){
                    
                        if($(this).attr('data-transport-operator').match(pattern) == null){
                            $(this).hide();
                        }else{
                            $(this).show();
                        }
                    }else{
                        $(this).hide();
                    }
                    
                });
                return false;
            });
        });


        // function to show the popover on the buttons clicked (filters)
        $('body').on('click', '.filter-buttons',  function(){
        
            var name = $(this).attr('data-filter-name');
            var isActive = $(this).hasClass('show');
            $('.t-popover').removeClass('show');
            $('.filter-buttons').removeClass('show');
            
            switch( name )
            {
                case 'type':
                    if(!isActive){
                        $('#drop-down').addClass('show');
                        $('#popover-transport-type-filter').addClass('show');
                    }else{
                        $('#drop-down').removeClass('show');
                        $('#popover-transport-type-filter').removeClass('show');
                    }
                    break;
                case 'day-parts':
                    if(!isActive){
                        $('#parts-of-day').addClass('show');
                        $('#popover-day-filter').addClass('show');
                    }else{
                        $('#parts-of-day').removeClass('show');
                        $('#popover-day-filter').removeClass('show');
                    }
                    break;
                case 'fl-options':
                    if(!isActive){
                        $('#flight-options').addClass('show');
                        $('#popover-flight-options').addClass('show');
                        return false;
                    }else{
                        $('#flight-options').removeClass('show');
                        $('#popover-flight-options').removeClass('show');
                    }
                    break;
                case 'popover-search':
                    if(!isActive){
                        $('#popover-search').addClass('show');
                        $('#popover-search-container.popover').addClass('show');
                        return false;
                    }else{

                        $('#popover-search').removeClass('show');
                        $('#popover-search-container').removeClass('show');
                    }
                    break;
            }
            
        });

        $('body').click(function(e){
            var target = e.target;
            if ( $(target).hasClass('filter-buttons') === false && $('.popover').has(e.target).length === 0 ) {
                $('.filter-buttons').removeClass('show');
                $('.popover.bottom').removeClass('show');
            }
        });

        // EVENT HANDLER FOR BOOKING TRANSPORTS
        $('body').on('click', '.transportButton', function(){
            $('.flight-rules-agree').data(); 
            $('.flight-rules-agree').prop( "disabled", true );
            $('.rules-content').html('<div class="text-center"><i class="fa fa-circle-o-notch fa-1x fa-spin" aria-hidden="true" style="color:#2AA9DF;"></i>  Loading.. </div>');

            if(! $(this).find('.select-transport').hasClass('btn-secondary') ) // check if transport is selected
            {
                var travelDuration =  get_hours_min1($(this).attr('data-duration'));
                var dataEta = $(this).attr('data-eta');
                travelDuration = formatTime(travelDuration);
                if($(this).attr('data-provider') == 'mystifly'){
                    dataEta = moment( dataEta, moment.ISO_8601 );
                    dataEta = dataEta.format('hh:mm A');
                }
                var arrivalTime = arrival_am_pm(dataEta);

                departureDate = getDayTime(eroam_data.date_to+' '+arrivalTime, travelDuration);

                var thisData = this;
                departureDate = departureDate.split(' ');
                var displayDate = moment(departureDate[0]).format('Do, MMMM YYYY');
                if( activityDates.indexOf(departureDate[0]) != -1 ){
                    
                    if( parseFloat(departureDate[1]) < 18 ){
                        eroam.confirm( 
                            'Confirm', 
                            'there\'s an activity booked on this departure date, would you like to proceed and cancel the activity?',
                            function(){
                                //$('#departing-date').html('<strong> Departing </strong> '+displayDate); 
                                cancelActivity(departureDate[0]);
                                saveTransport(thisData);
                            } 
                        );
                    }else{
                        //$('#departing-date').html('<strong> Departing </strong> '+displayDate); 
                        saveTransport(this);
                    }
                    
                }else{
                    //$('#departing-date').html('<strong> Departing </strong> '+displayDate); 
                    saveTransport(this);
                }
            }
            else
            {
                $(this).find('.select-transport').attr('data-is-selected', 'false');
            }
        });


        // when user selects a mystifly flight and agrees to the flight rules then this function is triggered.
        $('.flight-rules-agree').click(function(e){
            e.preventDefault();
            var data                         = $(this).data();
            var transport                    = JSON.parse( JSON.stringify( data ) );
            transport.transport_type         = {};
            transport.transport_type.id      = data.transportTypeId;
            transport.transport_type.name    = data.transportTypeName;
            transport.transporttype          = {};
            transport.transporttype.id       = data.transportTypeId;
            transport.transporttype.name     = data.transportTypeName;
            transport.price                  = [];
            transport.price[0]               = {};
            transport.price[0].price         = data.price;
            transport.price[0].currency      = {};
            transport.price[0].currency.code = globalCurrency;

            var theElement = $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]');
            $('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
            theElement.find('.select-transport').addClass('btn-secondary');

            search_session.itinerary[key].transport = transport;
            bookingSummary.update( JSON.stringify( search_session ) );

            $('.transport-list').removeClass('selected-transport');
            $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

            $('#flightRules').modal('hide');

        });


        // function to trigger when selecting day filter
        $('body').on('click', '.day', function(){
            $('.day').removeClass('btn-secondary');
            $(this).toggleClass('btn-secondary');
            var value = $(this).attr('data-value');
            $('.transport-list').filter(function() {

                var filter = $(this).attr('data-filter-trans');
                var am_pm = $(this).attr('data-am-pm');

                if( !$(this).hasClass('selected-transport') && filter == '0'){

                    if( value == am_pm){
                        $(this).show();
                        $(this).attr('data-filter-period', 0);
                    }else if( value == 'BOTH' ){
                        $(this).show();
                        $(this).attr('data-filter-period', 0);
                    }else{
                        $(this).hide();
                        $(this).attr('data-filter-period', 1);
                    }
                }
                
            });
        });


        // function to trigger when selecting transport types
        $('body').on('click', '.trans-type', function(){
            var transportTypeId = $(this).data('transport-type-id');
            $(this).toggleClass('btn-secondary');
            // if the transport type selected is flight
            if( transportTypeId == 1 )
            {
                $('#flight-options').toggle();
            }

            if( $(this).hasClass('btn-secondary') )
            {
                $(this).attr('data-trans-is-selected', 'true')
            }
            else
            {
                $(this).attr('data-trans-is-selected', 'false');
            }
            var transport_types = to_array($('.trans-type.btn-secondary'), 'data-transport-type-name');
            $('.transport-list').filter(function() {


                var filter = $(this).attr('data-filter-period');

                var transType = $(this).attr('data-transport-type-name');
                if( !$(this).hasClass('selected-transport') && filter == '0'){
                    if( transport_types.indexOf(transType) !== -1 ){
                        $(this).show();
                        $(this).attr('data-filter-trans', 0);
                    }else{
                        $(this).hide();
                        $(this).attr('data-filter-trans', 1);
                    }
                }
            });
        });


        // function to trigger when selecting flight options
        $('body').on('click', '.fl-opt', function(){

            var option     = $(this).data('option');
            var isSelected = $(this).data('is-selected');

            flightOptions[ option ] = !isSelected;

            $(this).data('is-selected', !isSelected);

            $(this).toggleClass('btn-secondary');

        });
        function buildTransports()
        {
            var transport_options = '{{ $transport_options }}';
            if(transport_options == 1){
                var tasks = [

                    hideTransType,
                    buildMystiflyTransport, 
                    buildBusBudTransport, 
                    showTransportType, 
                    hidePageLoader, 
                    rangeSlider,
                    checkTransportCount,
                    getDistinctProvider,
                    getDistinctStop,           
                ];
            }else if(transport_options == 2){
                var tasks = [

                    hideTransType,
                    buildEroamTransport,
                    showTransportType,
                    hidePageLoader,
                    rangeSlider,
                    checkTransportCount,
                    getDistinctProvider,
                    getDistinctStop,            
                ];
            }else {
                var tasks = [
                    hideTransType,
                    buildEroamTransport, // call eroam 
                    buildMystiflyTransport, // call mystifly next
                    buildBusBudTransport, // call busbud next
                    showTransportType, // show the available transport types
                    hidePageLoader, // hides the page loader when all API calls are done
                    rangeSlider,
                    checkTransportCount,
                    getDistinctProvider,
                    getDistinctStop,            
                ];
            }

            $.each(tasks, function(index, value) {
                $(document).queue('tasks', processTask(value));
            });
            // queue`
            $(document).queue('tasks');

            $(document).dequeue('tasks');

        }

        function processTask(fn){
            return function(next){
                doTask(fn, next);
            }
        }

        function doTask(fn, next){
            fn(next);
        }


        function buildEroamTransport( next )
        {
            var trans_eroam_data = {
                from_city_id : "{{ $data['city_id'] }}",
                to_city_id : "{{ $data['to_city_id'] }}",
                date_from: formatDate("{{ $data['date_from'] }}"),
                date_to: formatDate("{{ $data['date_to'] }}"),
                day: "{{ $day }}",
                transport_types: [ "{{ join(', ', $transport_pref) }}" ]
            };


            if (trans_eroam_data.transport_types == "") {
                delete trans_eroam_data.transport_types;
            }

            var eroamApiCall = eroam.apiDeferred('city/from-and-to-v2', 'POST', trans_eroam_data, 'eroam', true);

            var transport_type_ids = ["{!! implode('", "', session()->get('transport_type_ids')) !!}"];
            var day = "{{ $day == 1 ? 'AM' : 'PM' }}";
            if ( {{ $day }} == 0 ) {
                day = 0;
            }
                eroam.apiPromiseHandler( eroamApiCall, function( eroamResponse ){
                    if( eroamResponse === null ) eroamResponse = [];
                    if( eroamResponse.length > 0 )
                    {
                        transport_count = eroamResponse.length;
                        eroamResponse.forEach(function( eroamTrans, eroamTransIndex ){
                            if(day)
                            {
                                var split_am_pm = etdFormat(eroamTrans.etd);
                                var am_pm       = split_am_pm.split(' ');
                                if(day == am_pm[1])
                                {
                                    
                                }
                                else
                                {
                                    return;
                                }
                            }
                            eroamTrans.duration = calculateTransportDuration(eroamTrans.etd, eroamTrans.eta);
                            eroamTrans.provider = 'eroam';
                            try{
                                appendTransport( eroamTrans );
                                addTransportTypeId( eroamTrans.transport_type.id )
                                totalTransportCount++;
                            }catch( e ){
                                console.log('Error on eroam append', e.message );
                            }               
                
                        });
                        
                    }
                    next();
                });
        }

        function buildMystiflyTransport( next )
        {
            try{

                var mystiflyRQ;
                var mystiflyApiCalls            = [];
                var arrayOfOriginIataCodes      = splitString("{{ $data['from_city_iatas'] }}", ",");
                var arrayOfDestinationIataCodes = splitString("{{ $data['to_city_iatas'] }}", ",");

            
                if( arrayOfOriginIataCodes.length > 0 && arrayOfDestinationIataCodes.length > 0 )
                {
                    arrayOfOriginIataCodes.forEach(function( originIataCode ){
                        arrayOfDestinationIataCodes.forEach(function( destinationIataCode ){
                            mystiflyRQ = {
                                DepartureDate : "{{ date('Y-m-d', strtotime($data['departure_date'])) }}",
                                OriginLocationCode : originIataCode,
                                DestinationLocationCode : destinationIataCode,
                                CabinPreference : 'Y',
                                Code : ['ADT'],
                                Quantity : [search_session.travellers],
                                IsRefundable : flightOptions.IsRefundable,
                                IsResidentFare : flightOptions.IsResidentFare,
                                NearByAirports : flightOptions.NearByAirports,
                                provider: 'mystifly'
                            }
                            mystiflyApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', mystiflyRQ, 'mystifly', true ) );
                        });
                    });
                    eroam.apiArrayOfPromisesHandler( mystiflyApiCalls, function( mystiflyArrayOfResponses ){
                        mystiflyArrayOfResponses.forEach( function( mystiflyResponse ){
                            if( mystiflyResponse != null )
                            {
                                if( Array.isArray( mystiflyResponse.PricedItineraries.PricedItinerary ) )
                                {
                                    if( mystiflyResponse.PricedItineraries.PricedItinerary.length > 0 )
                                    {
                                        var k= 1;
                                        var transport_count_limit = Math.abs(20 - transport_count);
                                        mystiflyResponse.PricedItineraries.PricedItinerary.forEach(function( mystiflyTrans ){
                                            if(transport_count_limit >= k){
                                                mystiflyTrans.provider     = "mystifly";
                                                mystiflyTrans.fromCityName = "{{ get_city_by_id( $data['city_id'] )['name'] }}";
                                                mystiflyTrans.toCityName   = "{{ get_city_by_id( $data['to_city_id'] )['name'] }}";
                                                mystiflyTrans.fromCityId   = "{{ $data['city_id'] }}";
                                                mystiflyTrans.toCityId     = "{{ $data['to_city_id'] }}";
                                                mystiflyTrans.StopQuantity     = mystiflyTrans.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment.StopQuantity;
                                                k++;
                                                try{
                                                    appendTransport( mystiflyTrans );   
                                                    addTransportTypeId(1);
                                                    totalTransportCount++;
                                                }catch(e){
                                                    console.log('error occured in mystifly data', e.message);
                                                }
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    var mystiflyTrans = mystiflyResponse.PricedItineraries.PricedItinerary;

                                    if( mystiflyTrans != null )
                                    {
                                        totalTransportCount++;
                                        mystiflyTrans.provider     = 'mystifly';
                                        mystiflyTrans.fromCityName = "{{ get_city_by_id( $data['city_id'] )['name'] }}";
                                        mystiflyTrans.toCityName   = "{{ get_city_by_id( $data['to_city_id'] )['name'] }}";
                                        mystiflyTrans.fromCityId   = "{{ $data['city_id'] }}";
                                        mystiflyTrans.toCityId     = "{{ $data['to_city_id'] }}";
                                        mystiflyTrans.StopQuantity     = mystiflyTrans.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment.StopQuantity;
                                        
                                        try{
                                            appendTransport( mystiflyTrans );   
                                            addTransportTypeId(1);
                                        }catch(e){
                                            console.log('error occured in mystifly data', e.message);
                                        }
                                    }
                                }

                            }
                        });

                        next();
                    });
                }
                else
                {
                    next();
                }
            }
            catch(e)
            {
                console.log('An error occured during the mystifly call', e.message);
                next();
            }
        }

         function buildBusBudTransport( next )
        {
            try{

                var busbudRQ;
                var busbudApiCalls            = [];
                var arrayOfOriginBusbudCodes      = splitString("{{ $data['from_city_iatas'] }}", ",");
                var arrayOfDestinationBusbudCodes = splitString("{{ $data['to_city_iatas'] }}", ",");
                var arrayOfOriginBusbudCodes      = "{{ $data['from_city_geohash'] }}";
                var arrayOfDestinationBusbudCodes = "{{ $data['to_city_geohash'] }}";


                if( arrayOfOriginBusbudCodes != '' && arrayOfDestinationBusbudCodes != '' )
                {
                    busbudRQ = {
                                DepartureDate : "{{ date('Y-m-d', strtotime($data['departure_date'])) }}",
                                OriginLocationCode : arrayOfOriginBusbudCodes,
                                DestinationLocationCode : arrayOfDestinationBusbudCodes,
                                adult : search_session.travellers,
                                child : 0,
                                provider: 'busbud'
                            }
                    busbudApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', busbudRQ, 'busbud', true ) );


                    eroam.apiArrayOfPromisesHandler( busbudApiCalls, function( busbudArrayOfResponses ){

                        if( busbudArrayOfResponses != '' ){
                        busbudArrayOfResponses.forEach( function( busbudResponse ){


                               // var busbudTrans = busbudResponse;
                                busbudResponse.forEach(function( busbudTrans ){
                                busbudTrans.provider     = "busbud";
                                busbudTrans.fromCityName = "{{ get_city_by_id( $data['city_id'] )['name'] }}";
                                busbudTrans.toCityName   = "{{ get_city_by_id( $data['to_city_id'] )['name'] }}";
                                busbudTrans.fromCityId   = "{{ $data['city_id'] }}";
                                busbudTrans.toCityId     = "{{ $data['to_city_id'] }}";
                                busbudTrans.OriginLocationCode     = "{{ $data['from_city_geohash'] }}";
                                busbudTrans.DestinationLocationCode = "{{ $data['to_city_geohash'] }}";
                                busbudTrans.DepartureDate     = "{{ date('Y-m-d', strtotime($data['departure_date'])) }}";
                                            try{
                                                appendTransport( busbudTrans );
                                                addTransportTypeId(1);
                                                totalTransportCount++;
                                            }catch(e){
                                                console.log('error occured in busbud data', e.message);
                                            }
                                });

                        });
                }
                        next();
                    });
                }
                else
                {
                    next();
                }
            }
            catch(e)
            {
                console.log('An error occured during the busbud call', e.message);
                next();
            }
        }//appendTransport

        function addTransportTypeId( transTypeId )
        {
            if( isNotUndefined( transTypeId ) )
            {
                if( $.inArray( transTypeId, availableTransTypes ) == -1 )
                {
                    availableTransTypes.push( transTypeId );
                }
            }
        }

        function showTransportType( next )
        {
            availableTransTypes.forEach(function( transTypeId ){
                $(".li-trans-type[data-trans-type-id='"+transTypeId+"']").data("show-type","yes").show();
                $('.trans-type[data-transport-type-id="'+transTypeId+'"]').addClass('btn-secondary').data('trans-is-selected', 'true');
                if( transTypeId == 1 )
                {
                    $('#flight-options').show();
                }
            });
            next();
        }


        /*
        | Functions Created by Junfel
        */
        /*
        | get each value for selected transport type
        */
        function to_array( data, attribute ){
            var to_array = [];
            for ( var counter = 0; counter < data.length; counter++ ) {
                to_array.push( data[counter].getAttribute( attribute ) );
            }
            return to_array;
        }
        
        /*
        | funtion to display transport icon
        | return font-awesom icon class.
        */
        function transport_icon(type){
            var icon = '';
            switch(type){
                case 'Private boat':
                case 'Ferry':
                case 'Ferry or Boat':
                case 'Speed boat':
                case 'Slow boat':
                case 'Boat':
                case 'Jet Boat':
                case 'Cruise':
                case 'Ferry Boat':
                    icon = 'fa-ship';
                break;
                case 'Minivan':
                case 'Private Minibus':
                case 'Coach or Minivan':
                case 'Coach Minivan':
                case 'Coach':
                case 'Bus':
                    icon = 'fa-bus';
                break;
                case 'Flight':
                    icon = 'fa-plane';
                break;
                case 'Private Car (Landcruiser)':
                case 'Private Car or Minivan':
                case 'Private Car Minivan':
                case 'Private car':
                case 'Private Car (Deluxe)':
                    icon = 'fa-car';
                break;
                case 'Train':
                    icon = 'fa-train';
                break;
                case 'Taxi':
                    icon = 'fa-taxi';
                break;  
                case 'Coach + Ferry or Boat':
                case 'Train + Ferry or Boat':
                case 'Train + Minivan':
                case 'Train + Coach':
                case 'Minivan + Ferry or Boat':
                    icon = 'fa-plus';
                break;  
                case 'Transport Pass':
                    icon = 'fa-id-card';
                break;

                default:
                    icon = 'fa-car';
                break;
            }
            return icon;
        }

        /*
        | formatting price 
        | return price with currency code
        */

        function price_format(price, code){
            var new_price = Math.ceil(price).toFixed(2);
            var new_code = code.replace('D', '$');
            return new_code+' '+new_price;
        }
        /*
        | Temporary filter for the season
        | Return true or false
        */

        function compare_date(from, price_from, price_to){
            var result = false;
            var from = new Date(from);
            var price_from = new Date(price_from);
            var price_to = new Date(price_to);
            if( from >= price_from && from <= price_to ){
                result = true;
            }
            return result;
        }
        function get_arrival_am_pm(arrival){
            var arrival_split = arrival.split("+"); // since there's a possibility that there's a +, we split the + first
            var arrival_split_am_pm = arrival_split[0].split(" ");
            var arrival_hour = parseInt(arrival_split_am_pm[0]);
            if(arrival){

            }
        }
        /*
        | Function for calculating travel duration
        | Created by Aljun.
        */
        function calculateTransportDuration(departure,arrival){
            if(departure && arrival){
                departure = $.trim(departure);
                arrival = $.trim(arrival);

                var departure_split = departure.split(" ");
                var departure_split_time = departure_split[0].split(":");
                var departure_hour = parseInt(departure_split_time[0]);
                var departure_min = parseInt(departure_split_time[1]);
                var departure_hour_24 = departure_split[1] == "PM" ? departure_hour+12 : departure_hour;
                
                var arrival_split = arrival.split("+");
                var arrival_split_am_pm = arrival_split[0].split(" ");
                var arrival_split_hour_min = arrival_split_am_pm[0].split(":");
                var arrival_hour = parseInt(arrival_split_hour_min[0]);
                var arrival_min = parseInt(arrival_split_hour_min[1]);
                var formatted_min = arrival_min / 60;
                var plus_value = arrival_split[1] !== undefined ? arrival_split[1] : 0;
                var arrival_hour_24 = arrival_split[1] == "PM" ? arrival_hour+12 : arrival_hour;


                if(plus_value > 0 ){
                    var temp_hours = parseInt(24 - departure_hour_24); //3 
                    temp_hours     = parseInt(temp_hours) + parseInt(arrival_hour_24 );
                    temp_hours     += plus_value > 1 ? (plus_value - 1) * 24 : 0;
                    total_hours = temp_hours + formatted_min;

                }else{

                    var departure_time = departure_hour_24 + (departure_min / 60);

                    if(departure_time > 12){
                        arrival_hour_24 = plus_value == 0 ? arrival_hour+24 : arrival_hour;
                    }
                    if(arrival_hour > 12){
                        arrival_hour_24 =  arrival_hour;
                    }
                    var arrival_time = arrival_hour_24 + (arrival_min / 60);
                    var total_hours = arrival_time - departure_time;
                }
                return total_hours ;
            }else{
                return "";
            }
        }

        function get_hours_min1(hour_min,type,provider){
            //alert(type);
            var hours_mins = hour_min.toString().split(".");
            var hours = parseInt(hours_mins[0]);
            var mins = parseFloat('0.'+hours_mins[1]) * 60;

            if(type == 'Detail' && provider == 'busbud'){ //alert('hi');
                a = parseInt(hour_min);
                var hours = Math.trunc(a/60);
                var minutes = a % 60;
                return hours+' Hour(s) and '+Math.ceil(minutes)+' Minute(s)';
            }else if(type == 'Detail'){
                return hours+' Hour(s) and '+Math.ceil(mins)+' Minute(s)';
            } else {
                return hours+' hr(s) '+Math.ceil(mins)+' min(s)';
            }   
        }

        function arrival_am_pm(eta){

            var result;
            var arrival_split = eta.split("+"); 
            var arrival_split_am_pm = arrival_split[0].split(" ");
            var arrival_split_hour_min = arrival_split[0].split(":");
            var arrival_hour = parseInt(arrival_split_hour_min[0]);

            var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


            if(typeof arrival_split_am_pm[1] !== 'undefined'){

                if(arrival_split_am_pm[1] == 'PM'){
                    arrival_hour = arrival_hour + 12;
                }
                result = padd_zero(arrival_hour)+':'+arrival_min;
            }else{
                if( arrival_hour < 12 ){
                    result = padd_zero(arrival_hour)+':'+arrival_min;
                }else{
                    result = padd_zero(hour)+':'+arrival_min;
                }
            }
            
            return result;
        }

        function etdFormat(etd){
            var time = etd.replace(":00", "");
            return parseInt(time[0]) > 12 ? moment(etd, ['HH:mm A']).format('hh:mm') : moment(etd, ["hh:mm A"]).format('HH:mm') ;
        }
        function padd_zero(number) {
            if(parseInt(number) == 0){
                number = 12;
            }
            return (number < 10) ? ("0" + number) : number;
        }



        function appendTransport( transport )
        {
            var html, airline_code,attributes, transportName, transportTypeName, durationString, supplierName, fromCity, toCity, etd, eta, duration, currency, price = 0, priceString, convertedPrice = 0,StopQuantity = 0,departTimeZone,arriveTimeZone;
            var selected       = '';
            var selected_trans = '';
            var selected_trans2= '';
            var transportClass = '';
            var isSelected     = 'false';
            var showTrue = '';

            if(search_session.itinerary[parseInt(key)].city.timezone){
                departTimeZone = moment().tz(search_session.itinerary[parseInt(key)].city.timezone.name).format('z Z');
            }
            if(search_session.itinerary[parseInt(key)+1].city.timezone){
                arriveTimeZone = moment().tz(search_session.itinerary[parseInt(key)+1].city.timezone.name).format('z Z');
            }

            switch( transport.provider )
            {
                case 'eroam':
                    transportTypeName = transport.transport_type.name;
                    var operator      = transport.operator.name;
                    fromCity          = transport.from_city.name;
                    toCity            = transport.to_city.name;
                    etd               = transport.etd;
                    eta               = transport.eta;
                    var formattedEtd  = etdFormat(etd);
                    var etdAmPm       = formattedEtd.split(' ');
                    duration          = transport.duration.toFixed(2);
                    priceId           = 0;
                    currency          = 'AUD'
                    transportClass    = ( transport.transport_type_id == 1 ) ? 'Cabin Class Not Specified' : '';
                    transportName     = operator.toUpperCase()+' / Depart - '+fromCity+' '+formattedEtd+' ( '+ departTimeZone +' ) '+'. Arrive - '+toCity+' '+arrival_am_pm(eta)+ '( '+ arriveTimeZone+' )';
                    
                    var fliteFromTo   = fromCity+ ' to '+toCity;
                    var departTime    = formattedEtd;
                    var arriveTime    = arrival_am_pm(eta);
                    var fliteName     = operator.toUpperCase();
                    var fliteDeparture= fromCity;
                    var fliteArrival  = toCity;
                    var departTimeDetail= formattedEtd;
                    var arriveTimeDetail= arrival_am_pm(eta);
                    var FlightNumber  = transport.id;

                    if( isNotUndefined( transport.price[0] ) )
                    {
                        currency       = transport.price[0].currency.code;
                        price          = transport.price[0].price;
                        convertedPrice = eroam.convertCurrency( price, currency );
                        priceId        = transport.price[0].id;
                    }

                    if( selectedProvider == transport.provider )
                    {
                        if( selectedTransportId == transport.id )
                        {
                            isSelected     = 'true';
                            selected       = 'btn-secondary';
                            selected_trans = 'selected-transport';
                            selected_trans2= 'activetabe';

                            $("#search-icon").removeClass (function (index, className) {
                                return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                            });
                            $('#search-icon').addClass( transport_icon(transportTypeName) );
                        }
                    }
                    dataAttributes = [
                        'data-provider="eroam" ',
                        'data-is-selected="'+isSelected+'" ',
                        'data-id="'+transport.id+'" ',
                        'data-from-city-id="'+transport.from_city.id+'" ',
                        'data-to-city-id="'+transport.to_city.id+'" ',
                        'data-transport-id="'+transport.id+'" ',
                        'data-transport-type-id="'+transport.transport_type.id+'" ',
                        'data-transport-type-name="Bus" ',
                        'data-price="'+price+'" ',
                        'data-currency="'+currency+'" ',
                        'data-duration="'+transport.duration+'" ',
                        'data-price-id="'+priceId+'" ',
                        'data-etd="'+etd+'" ',
                        'data-eta="'+eta+'" ',
                        'data-transport-operator="'+transport.operator.name+'" ',
                        'data-am-pm="'+etdAmPm[1]+'" ',
                        'data-filter-period="0" ',
                        'data-filter-trans="0" '
                    ].join('');
                    break;

                case 'mystifly':
                    airline_code            = transport.ValidatingAirlineCode;
                    StopQuantity            = transport.StopQuantity;
                    transportTypeName       = 'Flight';
                    fromCity                = transport.fromCityName;
                    toCity                  = transport.toCityName;
                    var t                   = transport.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment;
                    var fareSourceCode      = transport.AirItineraryPricingInfo.FareSourceCode;
                    var airlineName         = ( isNotUndefined( transport.ValidatingAirlineName ) ) ? transport.ValidatingAirlineName : 'N/A';
                    var airlineCode         = ( isNotUndefined( transport.ValidatingAirlineCode ) ) ? transport.ValidatingAirlineCode : 'N/A';
                    etd                     = t.DepartureDateTime;
                    eta                     = t.ArrivalDateTime;
                    if(t.CabinClassCode.trim() == 'Y'){
                        transportClass = 'Economy';
                    }else if(t.CabinClassCode.trim() == 'C'){
                        transportClass = 'Business';
                    }else if(t.CabinClassCode.trim() == 'F'){
                        transportClass = 'First';
                    }else if(t.CabinClassCode.trim() == 'S'){
                        transportClass = 'Premium Economy';
                    }else{
                        transportClass = 'Cabin Class Not Specified';
                    }

                    var cachedDepartureTime = moment( etd, moment.ISO_8601 ).format('HH:mm');

                    var departure           = moment( eroam_data.date_to+"T"+cachedDepartureTime+":00", moment.ISO_8601 );
                    var departure2          = moment( etd, moment.ISO_8601 );

                    durationInMins          = parseInt(t.JourneyDuration);
                    duration                = ( parseFloat(durationInMins) / 60.0 );
                    
                    var arrival2             = moment( eta, moment.ISO_8601 );
                    var arrival             = moment( departure.add( durationInMins, 'minutes' ) );
                    
                    currency                = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
                    price                   = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
                    convertedPrice          = eroam.convertCurrency( price, currency );
                    
                    transportName           = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+' / Depart - '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure2.format('hh:mm A')+' ( '+ departTimeZone +' ). Arrive - '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+arrival2.format('hh:mm A')+' ( '+ arriveTimeZone +' )';
                    
                    if(t.DepartureData[0] == ','){
                        t.DepartureData = t.DepartureData.substring(1, t.DepartureData.length);
                    }
                    var fliteFromTo         = '<span class="textCap" >'+airlineName.toLowerCase()+'</span>,Flight #'+airlineCode+t.FlightNumber+'<br>'+(t.DepartureData != null ? t.DepartureData : fromCity)+ ' to '+(t.ArrivalData != null ? t.ArrivalData : toCity);
                    var departTime          = departure2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+departure2.format('Do MMM')+'</span>';
                    var arriveTime          = arrival2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+arrival2.format('Do MMM')+'</span>';
                    var fliteName           = '<span class="textCap" >'+airlineName.toLowerCase()+'</span>, Flight #'+airlineCode+t.FlightNumber;
                    var fliteDeparture      = (t.DepartureData != null ? t.DepartureData : fromCity);
                    var fliteArrival        =  (t.ArrivalData != null ? t.ArrivalData : toCity);
                    var departTimeDetail    = departure2.format('HH:mm dddd Do MMM YYYY');
                    var arriveTimeDetail    = arrival2.format('HH:mm dddd Do MMM YYYY');
                    var FlightNumber        = t.FlightNumber;

                    var arrivalItineraryText = arrival.format('HH:mm A, Do, MMMM YYYY');

                    var bookingSummaryText = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+'<br/><small>Depart: '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure.format('Do, MMMM YYYY')+' '+departure2.format('hh:mm A')+'</small><br/><small>Arrive: '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+moment(search_session.itinerary[parseInt(key)+1].city.date_from).format('Do, MMMM YYYY')+' '+arrival2.format('hh:mm A')+'</small>';


                    if( selectedProvider == transport.provider )
                    {
                        if( selectedTransportId.trim() == fareSourceCode.trim() )
                        {
                            isSelected     = 'true';
                            selected       = 'btn-secondary';
                            selected_trans = 'selected-transport';
                            selected_trans2= 'activetabe';
                            $("#search-icon").removeClass (function (index, className) {
                                return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                            });
                            $('#search-icon').addClass( transport_icon(transportTypeName) );
                        }
                    }


                    dataAttributes = [
                        'data-provider="mystifly" ',
                        'data-stop="'+StopQuantity+'" ',
                        'data-is-selected="'+isSelected+'" ',
                        'data-from-city-id="'+transport.fromCityId+'" ',
                        'data-to-city-id="'+transport.toCityId+'" ',
                        'data-id="'+fareSourceCode+'" ',
                        'data-transport-id="'+fareSourceCode+'" ',
                        'data-fare-source-code="'+fareSourceCode+'" ',
                        'data-fare-type="'+transport.AirItineraryPricingInfo.FareType+'" ',
                        'data-airline-code="'+airlineCode+'" ',
                        'data-operating-airline="'+airlineName+'" ',
                        'data-transport-operator="'+airlineName+'" ',
                        'data-flight-number="'+t.FlightNumber+'" ',
                        'data-etd="'+etd+'" ',
                        'data-eta="'+eta+'" ',
                        'data-durationMin="'+durationInMins+'" ',
                        'data-duration="'+duration+'" ',
                        'data-arrival-location="'+t.ArrivalAirportLocationCode+'" ',
                        'data-departure-location="'+t.DepartureAirportLocationCode+'" ',
                        'data-passenger-type-code="ADT" ',
                        'data-passenger-type-quantity="'+search_session.travellers+'" ',
                        'data-arrival-data="'+(t.ArrivalData != null ? t.ArrivalData : toCity)+'" ',
                        'data-departure-data="'+(t.DepartureData != null ? t.DepartureData : fromCity)+'" ',
                        'data-transport-type-id="1" ',
                        'data-transport-type-name="'+transportTypeName+'" ',
                        'data-price="'+price+'" ',
                        'data-currency="'+currency+'" ',
                        'data-cabin-class="'+transportClass+'" ',
                        'data-booking-summary-text="'+bookingSummaryText+'" ',
                        'data-arrival-itinerary-text="'+arrivalItineraryText+'" ',
                        'data-am-pm="'+ departure2.format('A') +'" ',
                        'data-filter-period="0" ',
                        'data-filter-trans="0" ',
                        'data-is-passport-mandatory="'+transport.IsPassportMandatory+'" '
                    ].join('');
                    break;

                    case 'busbud':

                    transportTypeName = 'Coach Minivan';
                    var operator      = transport.transportTypeName;
                    fromCity          = transport.fromCityName;
                    toCity            = transport.toCityName;
                    duration          = transport.duration;
                    priceId           = 0;
                    currency          = transport.currency;
                    transportClass    = transport.transportClass;
                    durationString    = transport.timeInHours;
                    var departTime1   = transport.departure_time;
                    var arriveTime1   = transport.arrival_time;
                    var fliteFromTo   = fromCity+ ' to '+toCity;

                    var fliteName     = operator.toUpperCase();
                    var fliteDeparture= fromCity;
                    var fliteArrival  = toCity;
                    var FlightNumber  = transport.transportId;

                    etd                     = transport.departure_time1;
                    eta                     = transport.arrival_time1;

                    var departure2    = moment( etd, moment.ISO_8601 );
                    var departTime    = departure2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+departure2.format('Do MMM')+'</span>';
                    var arriveTime2   = moment( eta, moment.ISO_8601 );
                    var arriveTime    = arriveTime2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+arriveTime2.format('Do MMM')+'</span>';

                    var departTimeDetail    = departure2.format('HH:mm dddd Do MMM YYYY');
                    var arriveTimeDetail    = arriveTime2.format('HH:mm dddd Do MMM YYYY');
                    currency                = transport.currency;
                    price                   = transport.price;
                    convertedPrice          = eroam.convertCurrency( price, currency );

                    if( selectedProvider == transport.provider )
                    {
                        if( selectedTransportId.trim() == transport.transportId.trim() )
                        {
                            isSelected     = 'true';
                            selected       = 'btn-secondary';
                            selected_trans = 'selected-transport';
                            selected_trans2= 'activetabe';
                            $("#search-icon").removeClass (function (index, className) {
                                return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                            });
                            $('#search-icon').addClass( transport_icon(transportTypeName) );
                        }
                    }
                    dataAttributes = [
                        'data-provider="busbud" ',
                        'data-is-selected="'+isSelected+'" ',
                        'data-id="'+transport.transportId+'" ',
                        'data-from-city-id="'+transport.fromCityId+'" ',
                        'data-to-city-id="'+transport.toCityId+'" ',
                        'data-transport-id="'+transport.transportId+'" ',
                        'data-transport-type-id="'+transport.transportId+'" ',
                        'data-transport-type-name="Bus" ',
                        'data-price="'+convertedPrice+'" ',
                        'data-etd="'+etd+'" ',
                        'data-eta="'+eta+'" ',
                        'data-currency="'+globalCurrency+'" ',
                        'data-durationMin="'+transport.duration+'" ',
                        'data-duration="'+transport.duration+'" ',
                        'data-etd="'+departTime1+'" ',
                        'data-eta="'+arriveTime1+'" ',
                        'data-transport-operator="'+transport.transportTypeName+'" ',
                        'data-filter-period="0" ',
                        'data-OriginLocationCode="'+transport.OriginLocationCode+'" ',
                        'data-DestinationLocationCode="'+transport.DestinationLocationCode+'" ',
                        'data-DepartureDate="'+transport.DepartureDate+'" ',
                        'data-transport-type-id="'+transport.transportId+'" ',
                        'data-filter-trans="0" '
                    ].join('');
                    break;

            }
            
            if( transport.provider == 'busbud'){
                durationString = durationString;
            }else{
                durationString = get_hours_min1( duration,'head',transport.provider);
            }
            
            maxPrice = parseInt( Math.ceil( convertedPrice ) ) > maxPrice ? parseInt( Math.ceil( convertedPrice ) ) : maxPrice;
            priceString = globalCurrency + ' ' + Math.ceil( convertedPrice ).toFixed(2);

            html = '<div class="card border-0 rounded-0 mb-3 transport-list '+selected_trans+'" '+dataAttributes+'">'+
                '<div class="card-header flight-header border-0 rounded-0 '+selected_trans2+'" '+dataAttributes+' id="heading'+FlightNumber+'" data-toggle="collapse" data-target="#collapse'+FlightNumber+'" aria-expanded="'+isSelected+'" aria-controls="collapse'+FlightNumber+'">'+
                    '<div class="row">'+
                        '<div class="col-12 col-sm-12 col-md-4 col-lg-2 col-xl-1 border-right">';
                        if(airline_code == '' || airline_code == undefined){
                            html+='<i class="fa '+transport_icon(transportTypeName)+' fa-3x pad10_0 "></i>';
                        }else{
                            html+= '<div class="flight_img">'+
                                '<img src="http://pics.avs.io/100/50/'+airline_code+'.png" alt="" />'+
                            '</div>';
                        }
                           
                        html+='</div>'+

                        '<div class="col-12 col-sm-12 col-md-8 col-lg-2 col-xl-3 border-right ">'+
                            '<p><i class="fa '+transport_icon(transportTypeName)+'  fa-1x"></i><strong>  '+transportTypeName+'</strong></p>'+
                            '<p>'+fliteFromTo+'</p>'+
                        '</div>'+
                        '<div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2 border-right  text-center">'+
                            '<p><strong>Depart</strong></p>'+
                            '<p class="mt-3">'+departTime+'</p>'+
                        '</div>'+

                        '<div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2 border-right  text-center">'+
                            '<p><strong>Arrive</strong></p>'+
                            '<p class="mt-3">'+arriveTime+'</p>'+
                        '</div>'+
                        '<div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2 border-right text-center">'+
                            '<p><strong>Duration</strong></p>'+
                            '<p class="mt-10">'+ durationString +'<br/>'+ transportClass +'</p>'+
                        '</div>'+
                        '<div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2  text-center">'+
                            '<p><strong>Price</strong></p>'+
                            '<p class="mt-10">'+ priceString +'</p>'+
                        '</div>'+
                    '</div>'+
                '</div>';
                
                if(isSelected == 'true') {
                    showTrue = 'show';
                } else {
                    showTrue = '';
                }
                html+='<div id="collapse'+FlightNumber+'" data-parent="#accordion" class="transport-collapse collapse '+showTrue+'" aria-labelledby="heading'+FlightNumber+'" data-parent="#accordion">'+
                    '<div class="card-body flight-body">'+
                        '<div class="flight_icons">'+
                            '<i class="fa fa-'+transport_icon(transportTypeName)+'  fa-1x"></i>'+
                        '</div>';
                        if(fliteDeparture[0] == ','){
                            fliteDeparture = fliteDeparture.substring(1, fliteDeparture.length);
                        }
                        if(fliteArrival[0] == ','){
                            fliteArrival = fliteArrival.substring(1, fliteArrival.length);
                        }
                        html+='<div class="flight_details">'+
                            '<h5><strong>'+ transportTypeName+'</strong></h5>'+
                            '<p><span class="textCap">'+fliteName+'</span></p>'+

                            '<div class="row">'+
                                '<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">'+
                                    '<div class="row">'+
                                        '<div class="col-12 col-sm-12 col-md-4 col-lg-3">'+
                                            '<p><strong>Departs:</strong> </p>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12 col-md-8 col-lg-9">'+
                                            '<p>'+fliteDeparture+' - '+departTimeDetail+'</p>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="row">'+
                                        '<div class="col-12 col-sm-12 col-md-4 col-lg-3">'+
                                            '<p><strong>Arrives:</strong> </p>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12 col-md-8 col-lg-9">'+
                                            '<p>'+fliteArrival+' - '+arriveTimeDetail+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row pt-2">'+
                                        '<div class="col-12 col-sm-12 col-md-4 col-lg-3">'+
                                            '<p><strong>Duration:</strong> </p>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12 col-md-8 col-lg-9">'+
                                            '<p>'+get_hours_min1( duration,"Detail",transport.provider)+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-12 col-sm-12 col-md-4 col-lg-3">'+
                                            '<p><strong>Class:</strong> </p>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12 col-md-8 col-lg-9">'+
                                            '<p>'+transportClass+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+

                                '<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">'+
                                    '<div class="row">'+
                                        '<div class="col-sm-5 text-right">'+globalCurrency+'</div>'+
                                        '<div class="col-sm-7">'+
                                            '<strong class="amount">'+Math.ceil( convertedPrice ).toFixed(2)+'</strong>'+
                                            '<p class="pt-2">from, per person</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="mt-4 transportButton" '+dataAttributes+'>'+
                                        '<button type="button" class="btn  btns_input_white transform d-block w-100 select-transport '+selected+'" '+dataAttributes+'>ADD TO ITINERARY</button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="clearfix"></div>'+
                    '</div>'+
                '</div>'+
            '</div>';

            if( selected_trans != '' )
            {
                $('#transportListSelected').prepend(html);
            }
            else
            {
                $('#transportList').append(html);
            }
            $('[data-toggle="tooltip"]').tooltip(); 
        }


        function formatDate(date, time = false)
        {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            if(time){
                day += ' '+ d.getHours();
            }
            return [year, month, day].join('-');
        }

        function hideTransType( next )
        {
            $(".li-trans-type").data('show-type', 'no').hide();
            next();
        }

        function hidePageLoader( next )
        {
            $( '#transport-loader' ).fadeOut(300);
            next();
        }

        function checkTransportCount( next )
        {
            $('#transportList').find('.noTransportFound').closest('h4').remove();
            $('.trans-count').html( ': Top ' + totalTransportCount + ( (totalTransportCount == 1 || totalTransportCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
            if( totalTransportCount == 0)
            {
                var html = '<h4 class="text-center noTransportFound">No Transport Found.</h4>';
                
                $('#transportList').append( html );
            }
            next();
        }
        /*
        | Added by Junfel
        | get departure date and time(24 hour format )
        */
        function getDayTime(arrivalDate, duration) {
            var hours_mins = duration.split(':');
            var result = new Date(arrivalDate);
            result.setHours(result.getHours() - parseInt(hours_mins[0]));
            result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));

            return formatDate(result, true);
        }
        /*
        | Added by Junfel
        | remove "hr(s)" and "min(s)"
        */
        function formatTime(time){
            return time.replace(/hr\(s\)|min\(s\)| /gi, function(x){
                return x == 'hr(s)' ? ':' : '';
            });
        }
        /*
        | Added by Junfel
        | for activity cancellation when transport conflicts with activity.
        */
        function cancelActivity(departureDate){
            $.each(search_session.itinerary, function (key, itinerary) {
                var temp  = itinerary.activities;
                if(itinerary.city.id == eroam_data.city_ids[0] && itinerary.activities.length > 0){
                    $.each(itinerary.activities, function (k, activity) {
                        if( typeof activity != 'undefined' ){
                            if( activity.date_selected == departureDate ){
                                temp.splice(k,1);
                            }
                        }
                    });
                }
            });
            bookingSummary.update( JSON.stringify( search_session ) );
        }

        function saveTransport(transportData){
            $( transportData ).find('.select-transport').attr('data-is-selected', 'true');
                var data = $( transportData ).find('.select-transport').data();
                var contentMsg = 'Are you sure you want to book this transport?';

                switch( data.provider )
                {
                    case 'eroam':

                        var transport                    = JSON.parse( JSON.stringify( data ) );
                        transport.transport_type_id      = data.transportTypeId;
                        transport.transporttype          = {};
                        transport.transporttype.id       = data.transportTypeId;
                        transport.transporttype.name     = data.transportTypeName;
                        transport.operator               = {};
                        transport.operator.name          = data.transportOperator;
                        transport.price                  = [];
                        transport.price[0]               = {};
                        transport.price[0].price         = data.price;
                        transport.price[0].id            = data.priceId;
                        transport.price[0].currency      = {};
                        transport.price[0].currency.code = globalCurrency;
                        transport.provider               = 'eroam';
                        
                        $('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
                        $( transportData ).find('.select-transport').toggleClass('btn-secondary');
                        /*
                        | Added by junfel
                        */

                        $("#search-icon").removeClass (function (index, className) {
                            return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                        });
                        $('#search-icon').addClass( transport_icon(transport.transporttype.name) );
                        /*
                        | End Junfel
                        */
                        search_session.itinerary[key].transport = convertToSnakeCase( transport );
                        bookingSummary.update( JSON.stringify( search_session ) );      

                        $('.transport-list').removeClass('selected-transport');
                        $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

                        $('.transport-list').find('.card-header').removeClass('activetabe');
                        $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').find('.card-header').addClass('activetabe');

                        break;

                    
                    case 'mystifly':

                        var transport                    = JSON.parse( JSON.stringify( data ) );
                        transport.transport_type         = {};
                        transport.transport_type.id      = data.transportTypeId;
                        transport.transport_type.name    = data.transportTypeName;
                        transport.transporttype          = {};
                        transport.transporttype.id       = data.transportTypeId;
                        transport.transporttype.name     = data.transportTypeName;
                        transport.price                  = [];
                        transport.price[0]               = {};
                        transport.price[0].price         = data.price;
                        transport.price[0].id            = data.id;
                        transport.price[0].currency      = {};
                        transport.price[0].currency.code = globalCurrency;

                        $('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
                        $( transportData ).find('.select-transport').toggleClass('btn-secondary');

                        /*
                        | Added by junfel
                        */
                        $("#search-icon").removeClass (function (index, className) {
                            return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                        });
                        $('#search-icon').addClass( transport_icon(transport.transporttype.name) );
                        /*
                        | End Junfel
                        */
                        
                        search_session.itinerary[key].transport = convertToSnakeCase( transport );
                        bookingSummary.update( JSON.stringify( search_session ) );      

                        $('.transport-list').removeClass('selected-transport');
                        $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

                        $('.transport-list').find('.card-header').removeClass('activetabe');
                        $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').find('.card-header').addClass('gray-bg');

                        break;

                    case 'busbud':

                        var transport                    = JSON.parse( JSON.stringify( data ) );
                        transport.transport_type         = {};
                        transport.transport_type.id      = data.transportTypeId;
                        transport.transport_type.name    = data.transportTypeName;
                        transport.transporttype          = {};
                        transport.transporttype.id       = data.transportTypeId;
                        transport.transporttype.name     = data.transportTypeName;
                        transport.price                  = [];
                        transport.price[0]               = {};
                        transport.price[0].price         = data.price;
                        transport.price[0].id            = data.id;
                        transport.price[0].currency      = {};
                        transport.price[0].currency.code = globalCurrency;

                        $('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
                        $( transportData ).find('.select-transport').toggleClass('btn-secondary');

                        /*
                         | Added by junfel
                         */
                        $("#search-icon").removeClass (function (index, className) {
                            return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                        });
                        $('#search-icon').addClass( transport_icon(transport.transporttype.name) );
                        /*
                         | End Junfel
                         */

                        search_session.itinerary[key].transport = convertToSnakeCase( transport );
                         bookingSummary.update( JSON.stringify( search_session ) );

                        $('.transport-list').removeClass('selected-transport');
                        $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

                        $('.transport-list').find('.card-header').removeClass('activetabe');
                        $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').find('.card-header').addClass('activetabe');

                        break;  
                        
                }
        }
        function rangeSlider(next){
            var min = parseInt( eroam.convertCurrency(0, 'AUD') );
            var max = parseInt( maxPrice );
            
            $( '#slider-range' ).slider({
                range: true,
                step: 5,
                min: min,
                max: max,
                values: [ min, max ],
                slide: function( event, ui ) {
                    $( '#price-from' ).val(ui.values[ 0 ]);
                    $( '#price-to' ).val(ui.values[ 1 ]);
                }
            });

            $( '#price-from' ).val(0);
            $( '#price-to' ).val(5000);

            $('.curr').text(globalCurrency);

            next();
        }

        function calculateHeight(){

            var winHeight = $(window).height();
            var oheight = $('.page-sidebar').outerHeight();
            var elem = $('.page-content .tabs-container').outerHeight();
            var elemHeight = oheight - elem;
            var winelemHeight = winHeight - elem;
            if(winHeight < oheight){ 
              $(".page-content .tabs-content-container").outerHeight(elemHeight);
            } else{
              $(".page-content .tabs-content-container").outerHeight(winelemHeight);
            }
          }
           

        /***************************** END FUNCTIONS ****************************/
        /**************************** start sorting function ********************/
        $(document).on('click', '#priceSortAsc', function() { //alert(1);
            sortMeBy('data-price', 'div.tabs-content-wrapper', 'div.transport-list', 'asc');
            $('#priceSortAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
            $('#priceSortAsc').attr('id', 'priceSortDesc');

        });
        $(document).on('click', '#priceSortDesc', function() {
            $('#priceSortDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
            sortMeBy('data-price', 'div.tabs-content-wrapper', 'div.transport-list', 'desc');
            $('#priceSortDesc').attr('id', 'priceSortAsc');
        });
        $(document).on('click', '#durationAsc', function() { //alert(1);
            sortMeBy('data-durationmin', 'div.tabs-content-wrapper', 'div.transport-list', 'asc');
            $('#durationAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
            $('#durationAsc').attr('id', 'durationDesc');
        });
        $(document).on('click', '#durationDesc', function() {
            $('#durationDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
            sortMeBy('data-durationmin', 'div.tabs-content-wrapper', 'div.transport-list', 'desc');
            $('#durationDesc').attr('id', 'durationAsc');
        });

        function sortMeBy(arg, sel, elem, order) {
            var $selector = $(sel),
                $element = $selector.children(elem);

            $element.sort(function(a, b) {
                var an = parseInt(a.getAttribute(arg)),
                    bn = parseInt(b.getAttribute(arg));

                if (order == 'asc') {
                    if (an > bn)
                        return 1;
                    if (an < bn)
                        return -1;
                } else if (order == 'desc') {
                    if (an < bn)
                        return 1;
                    if (an > bn)
                        return -1;
                }
                return 0;
            });

            $element.detach().appendTo($selector);
        }
        /**************************** end sorting function ********************/
        /**************************** start search function ********************/
        $(document).on('click', '#searchProvider', function() {
            $("#transport-tab").children().removeClass("active");
            var provider = $(this).text();
            filterListOperator(provider);
            $("#provider-tab").children().removeClass("active");
            $(this).parent().addClass("active");
        });
        function filterListOperator(value) {
            var list = $("div.tabs-content-wrapper div.transport-list");
            $(list).fadeOut("fast");
            var totalCount = 0;
            if (value == "All") {
                $("div.tabs-content-wrapper").find("div.transport-list").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");
                });
            } else { 
                $("div.tabs-content-wrapper").find("div.transport-list[data-transport-operator*='" + value + "']").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");

                });
            }
            checkTransportCountForSearch(totalCount);
        }
        $(document).on('click', '#transportProvider', function() {
            $("#provider-tab").children().removeClass("active");
            var provider = $(this).text(); //alert(provider);
            filterListTransport(provider);
            $("#transport-tab").children().removeClass("active");
            $(this).parent().addClass("active");
        });
        function filterListTransport(value) {
            var list = $("div.tabs-content-wrapper div.transport-list");
            $(list).fadeOut("fast");
            var totalCount = 0;
            if (value == "All") {
                $("div.tabs-content-wrapper").find("div.transport-list").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");
                });
            } else { 
                $("div.tabs-content-wrapper").find("div.transport-list[data-transport-type-name*='" + value + "']").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");

                });
            }
            checkTransportCountForSearch(totalCount);
        }
        function checkTransportCountForSearch(totalTransportCount)
        {
            $('#transportList').find('.noTransportFound').closest('h4').remove();
            $('.trans-count').html( ': Top ' + totalTransportCount + ( (totalTransportCount == 1 || totalTransportCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
            if( totalTransportCount == 00)
            {
                var html = '<h4 class="text-center noTransportFound">No Transport Found.</h4>';
                
                $('#transportList').append( html );
            }
            //next();
        }
        $(document).on('click', '#searchStop', function() {
            $("#stop-tab").children().removeClass("active");
              var stop = $(this).text();
              filterListStop(stop);
            $(this).parent().addClass("active");
        });
        
        
        function filterListStop(value) {
            var list = $("div.tabs-content-wrapper div.transport-list");
            $(list).fadeOut("fast");
            var totalCount = 0;
            if (value == "All") {
                $("div.tabs-content-wrapper").find("div.transport-list").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");
                });
            } else {
                $("div.tabs-content-wrapper").find("div.transport-list[data-stop*='" + value + "']").each(function (i) {
                    totalCount ++;
                    $(this).delay(200).slideDown("fast");

                });
            }
            checkStopCountForSearch(totalCount);
        }
        function checkStopCountForSearch(totalStopCount)
        {
            $('#transportList').find('.noTransportFound').closest('h4').remove();
            $('.trans-count').html( ': Top ' + totalStopCount + ( (totalStopCount == 1 || totalStopCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
            if( totalStopCount == 0)
            {
                var html = '<h4 class="text-center noTransportFound">No Transport Found.</h4>';
                $('#transportList').append( html );
            }
            //next();
        }
        /**************************** end search function ********************/

        function getDistinctProvider(next){
            var items = {};
            $('div.transport-list').each(function() {
                items[$(this).attr('data-transport-operator')] = true;
            });
            var html = '';
            var result = new Array();
            for(var i in items)
            {
                if(i != 'N/A'){
                    result.push(i);
                    html += '<li><a id="searchProvider" class="dropdown-item">'+i+'</a></li>';
                }

            }
            $('#provider-tab').append( html );
            next();
            
        }
        function getDistinctStop(){
            var items = {};
            $('div.transport-list').each(function() {
                items[$(this).attr('data-stop')] = true;
            });


            var html = '';
            var result = new Array();
            for(var i in items)
            {
                if(i != 'N/A'){
                    if(!isNaN(i)){
                         result.push(i);
                         html += '<li><a id="searchStop" class="dropdown-item">'+i+'</a></li>';
                    }
                }

            }
            $('#stop-tab').append( html );
        }
        
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).on('click','.flight-header',function() {
            $('.transport-collapse').each(function() {
                $(this).removeClass('show');
            });
            $('.flight-header').not(this).each(function() {
                $(this).attr('aria-expanded','false');
            });
            $(this).attr('aria-expanded','true');
        });
    </script>
    <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush