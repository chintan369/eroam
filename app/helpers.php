<?php
/**
 * Custom helper functions
 */

if ( !function_exists('http') ) {

	function http( $method, $path, $data = [], $headers = []) {

		$api = "https://cms.eroam.com/eroam/api/v2/";
		//$api = "http://cms.dev2.eroam.com/api/";
		try {		
			
			$client = new \GuzzleHttp\Client();
			$response = $client->request($method,$api.$path, [
			    'form_params' => $data
			]);
			
		} catch(Exception $e) {
			echo $e->getMessage();die;
			Log::error('An error occured in maps function name(): ' . $e->getMessage());
		}

		if (isset($response)) {
			$response = json_decode((string) $response->getBody(),true);
		}

		if( isset( $response['status'] ) ){
			if ( $response['status'] == 200 ) { return $response['data']; }
		}elseif( isset( $response['success'] ) ){
			return ( $response['success'] ? $response['data']: [] );
		}else{
			return $response;
		}

		return null;
	}
}

if ( !function_exists('http_gad') ) {
	// api call from G Adventure
	function http_gad( $method, $path, $data = [], $headers = []) {
		$ctr 				= 0;
		$limit 				= 20;
		$url 		 		= "https://rest.gadventures.com/";
		$X_application_key  = "live_41385f08d2ea2c2f004babe73079ead1a8f93f81";
		$headers 			= [
								'X-Application-Key' => $X_application_key,
								'Origin' => url('')
							];

		while ($ctr  < $limit) 
		{			
			try {			

				$client = new \GuzzleHttp\Client();
				$response = $client->request($method,$url.$path, [
				    'form_params'		=> $data,
					'headers' 	=> $headers
				]);
				
				$ctr = 20;			
				
			} catch(Exception $e) {
				$ctr++;
				Log::error('An error occured in maps function name(): ' . $e->getMessage());
				sleep(1);

			}			
		}

		if (isset($response)) {
			$response = json_decode((string) $response->getBody(),true);
		}


		if( isset( $response['status'] ) )
		{
			if ( $response['status'] == 200 ) { return $response['data']; }
		}
		else if( isset( $response['success'] ) )
		{
			return ( $response['success'] ? $response['data']: [] );
		}
		else
		{
			return $response;
		}

		return null;
	}
}

if ( !function_exists('http_dev') ) {

	function http_dev( $method, $path, $data = [], $headers = []) {
		try {		
			$client = new \GuzzleHttp\Client();
			$api_url = "http://dev.cms.eroam.com/";
			$response = $client->request($method,$api_url.$path, [
			    'form_params' => $data
			]);
			
		} catch(Exception $e) {
			echo $e->getMessage();
			Log::error('An error occured in maps function name(): ' . $e->getMessage());
			die;
		}

		if (isset($response)) {
			$response = json_decode((string) $response->getBody(),true);
		}

		if( isset( $response['status'] ) ){
			if ( $response['status'] == 200 ) { return $response['data']; }
		}elseif( isset( $response['success'] ) ){
			return ( $response['success'] ? $response['data']: [] );
		}else{
			return $response;
		}

		return null;
	}
}

if ( !function_exists('test_http') ) {
	function test_http( $method, $url, $data = [] ) {
		$response = json_decode( Guzzle::$method( $url, $data )->getBody(), true );
		//if ( $response['status'] == 200 ) {
			return $response;
		//}
		return null;
	}
}

if ( !function_exists('get_all_countries') ) {
    function get_all_countries() {
        $countries 	= Cache::get('countries');
       	$allCountry = array();
       	$j=0;
		foreach($countries as $country)
		{
			foreach ($country['countries'] as $country_data)
			{
				$allCountry[$j]['name'] 		= $country_data['name'];
				$allCountry[$j]['id'] 			= $country_data['id'];
				$allCountry[$j]['region'] 		= $country['id'];
				$allCountry[$j]['regionName'] 	= $country['name'];
				$j++;
			} 
		}
		usort($allCountry, 'sort_by_name');
        return $allCountry;
    }
}

if ( !function_exists('get_city_by_id') ) {
	function get_city_by_id( $id ) {
		$cities = Cache::get( 'cities' );
		$found = array_search( $id, array_column( $cities, 'id' ) );

		if ( $found !== false ) {
			return $cities[array_search( $id, array_column( $cities, 'id' ) )];
		}
	}
}

if ( !function_exists('get_city_by_name') ) {
	function get_city_by_name( $name ) {
		$cities = Cache::get( 'cities' );
		return $cities[array_search( $name, array_column( $cities, 'name' ) )];
	}
}

if ( !function_exists('get_cities_by_country_id') ) {
	function get_cities_by_country_id( $country_id ) {
		$cities = Cache::get( 'cities' );
		$result = array_where( $cities, function( $value, $key ) use ( $country_id ) {
			return $key['country_id'] == $country_id;
		} );
		return $result;
	}
}

if ( !function_exists('get_cities_by_city_name') ) {
    function get_cities_by_city_name( $name ) {
        $cities = Cache::get( 'cities' );
        $result = array_where( $cities, function( $value, $key ) use ( $name ) {
            return $key['name'] == $name;
        } );
        return $result;
    }
}

if ( !function_exists('get_countries_by_country_name') ) {
    function get_countries_by_country_name( $name ) {
        $countries = Cache::get( 'countries' );
        foreach($countries as $country){
        	$result = array_where($country['countries'], function( $value, $key ) use ( $name ) {
	        	if($key['name'] == $name){
	        		return $key['name'] == $name;
	        		//break;
	        	}
	        });	
	       return $result[1]['id'];
        }
        return 0;
    }
}

if ( !function_exists('get_country_by_country_name') ) {
    function get_country_by_country_name( $name ) {
        $countries = Cache::get( 'countries' );

        foreach($countries as $country){
        	$result = array_where($country['countries'], function( $value, $key ) use ( $name ) {
	        	if($key['name'] == $name){
	        		return $key['name'] == $name;
	        		//break;
	        	}
	        });

	       return $result;
        }
        return 0;
    }
}


if ( !function_exists('convert_date') ) {
	// created by miguel on 2017-01-09;
	// is used to convert date format from "2017-01-30" to "30th January 2017"
	function convert_date( $date, $date_format_name )
	{	
		$the_new_date_string = $date;

		switch( $date_format_name )
		{
			// e.g. 6th December 2016, 23rd October 2018
			case 'new_eroam_format':
				$the_new_date_string = date( 'D j M Y', strtotime($date));
				break;
				
			case 'eroam_format':
			
				$the_new_date_string = date( 'jS, M Y', strtotime($date));
				//$the_new_date_string = $day.' '.$month.' '.$year;
				break;
		}
		return $the_new_date_string;
	} 
}


if ( !function_exists('get_transport_icon') ) {
	function get_transport_icon($transport_type){
		switch ($transport_type) {
			case 'Private boat':
			case 'Ferry':
			case 'Ferry Boat':
			case 'Speed boat':
			case 'Slow boat':
			case 'Boat':
			case 'Jet Boat':
			case 'Cruise':
				$return = 'fa-ship';
				break;
			// case 'Minivan':
			case 'Private Minibus':
			case 'Coach Minivan':
			case 'Coach':
			case 'Bus':
				$return = 'fa-bus';
				break;
			case 'Flight':
				$return = 'fa-plane';
				break;
			case 'Private Car (Landcruiser)':
			case 'Private Car or Minivan':
			case 'Private car':
			case 'Private Car (Deluxe)':
				$return = 'fa-car';
				break;
			case 'Train':
				$return = 'fa-train';
				break;
			case 'Taxi':
				$return = 'fa-taxi';
				break;	
			case 'Coach + Ferry or Boat':
			case 'Train + Ferry or Boat':
			case 'Train + Minivan':
			case 'Train + Coach':
			case 'Minivan + Ferry or Boat':
				$return = 'fa-plus';
				break;
			case 'Transport Pass':
				$return = 'fa-id-card';
			break;
			default:
				$return = 'fa-car';
				break;
		}
		return $return;
	}
}


if ( !function_exists('sing') ) {
	function sing( $number, $string, $with_number = true ) {
		if ( $with_number ) {
			return $number > 1 ? $number . ' '. $string : $number . ' ' . str_singular( $string );
		} else {
			return $number > 1 ? $string : str_singular( $string );
		}

	}
}

if ( !function_exists( 'convert_24hr' ) ) {
	function convert_24hr( $time ) {
		return date( 'H:i', strtotime( $time ) );
	}
}

if ( !function_exists( 'add_str_time' ) ) {
	function add_str_time( $duration, $date, $format ) {

		return date( $format, strtotime( $duration, strtotime( $date ) ) );
	}
}

if ( !function_exists('calculate_transport_duration') ) {
	function calculate_transport_duration( $etd, $eta ) {

		$departure_time  = convert_24hr( $etd ); // 24 hour
		
		$eta_split_am_pm = explode(' ', $eta);
		$eta_split       = explode( '+', $eta_split_am_pm[0] );
		$plus_value      = isset( $eta_split[1] ) ? $eta_split[1] : 0; // value after + sign
		// arrival time in 24 hour format
		$arrival_time   = $plus_value == 0 ? date( 'H:i', strtotime( $eta_split[0] . "+12 hours" ) ) : $eta_split[0]; 
		if( date('H', strtotime( $eta_split[0] ) ) >= 12 ){
			$arrival_time =  $eta_split[0];
		}

		$time_interval = date( 'H:i', strtotime( $arrival_time ) - strtotime( $departure_time ) );

		$days = $plus_value > 1 ? '+' . ( $plus_value - 1 ) .' days ' : '';
		
		$duration = $days . '+' . date( 'H', strtotime( $time_interval ) ) . ' hours +' . date( 'i', strtotime( $time_interval ) ) . ' minutes';

		return $duration;
	}
}


if ( !function_exists('time_difference') ) {
	function time_difference( $etd, $eta, $additional_days = 0 )
	{
		$milliseconds = (strtotime($eta) - strtotime($etd)) / 3600;
		$time         = $milliseconds / 1000;
		$days         = floor($time / (24*60*60));
		$hours        = floor(($time - ($days*24*60*60)) / (60*60));
		$minutes      = floor(($time - ($days*24*60*60)-($hours*60*60)) / 60);
		return '+'.( $days+$additional_days ).' days '.$hours.' hours '.$minutes.' minutes';
	}
}



if ( !function_exists('create_iata_string') ) {
	function create_iata_string( $iatas )
	{
		$iata_str = '';
		if( count( $iatas ) == 1 )
		{
			$iata_str = $iatas[0]->iata_code;
		}
		if( count( $iatas ) > 1 )
		{
			$i = [];
			foreach( $iatas as $iata )
			{
				array_push( $i, $iata->iata_code );
			}
			$iata_str = implode(',', $i);
		}
		return $iata_str;
	}
}	
if ( !function_exists('get_hour')) {
	function get_hour($time){
		$hour =  date('H', strtotime($time));
		return (int)$hour;
	}
}

if( !function_exists('join_array_key_value') ){
	/*
	| Added by junfel
	| function for joining array keys and value;
	*/
	function join_array_key_value($array){
		return	implode(', ', array_map(function ($value, $key) {
		        	return $key.':"'.$value.'"';
		    	}, 
		    	$array, 
		    	array_keys($array)
			)
		);
	}
	
}
if (!function_exists('get_departure_date')) {
	/*
	| Added by Junfel
	| return exact departure date based on duration and date_to / next city's date_from
	| return format(Y/m/d)
	*/
	function get_departure_date($date_to, $transport_departure, $transport_duration, $hours = false){
		//$exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($transport_departure.' '.$transport_duration));
		$exact_arrival_date_time = date('Y-m-d H:i', strtotime($date_to.' '.$transport_departure.' '.$transport_duration));
		$time_to_deduct = str_replace('+', '-', $transport_duration);
		$number_of_hours = $hours ? ' H' : '';
		$departure_date = date('Y/m/d'.$number_of_hours, strtotime($exact_arrival_date_time.' '.$time_to_deduct));
		return $departure_date;
	}
}
/*
| Added by Junfel
*/
if (!function_exists('get_hours_min')) {
	function get_hours_min($hour_min){
		$hours_mins = explode('.', $hour_min);
		$hours = (int)$hours_mins[0];
		$mins = isset( $hours_mins[1] ) ? (number_format('0.'.$hours_mins[1], 2) * 60) : 0 ;

		return '+'.$hours.' hours +'.ceil($mins).' minutes';
	}
}

if ( !function_exists('sort_by_name') ) {
	/*
	| Added by Junfel
	| Sorting function for array of arrays
	*/
	function sort_by_name($itemA, $itemB){
	    return strcmp($itemA['name'], $itemB['name']);
	}
}

if ( !function_exists('convert_currency') ) {
	// added by miguel on 2017-01-30; description
	function convert_currency( $price, $currency ){

		$converted_price = 0;

		try{

			$currency_layer         = session()->get('currency_layer');
			
			$price_converted_to_aud = floatval( $price ) / floatval( $currency_layer[ $currency ] );
			
			$global_currency        = ( session()->has('currency') ) ? session()->get('currency') : 'AUD';
			
			$converted_price        = floatval( $price_converted_to_aud ) * floatval( $currency_layer[ $global_currency ] );

			// $currency_layer  = session()->get('currency_layer');
			
			// $price_in_aud    = floatval( $price ) * floatval( $currency_layer[ $currency ] );
			
			// $global_currency = ( session()->has('currency') ) ? session()->get('currency') : 'AUD';
			
			// $converted_price = ceil( $price_in_aud ) * floatval( $currency_layer[ $global_currency ] ); 

		}catch( Exception $e ){

			// echo $e->getMessage();

		}
		
		return number_format($converted_price, 2, '.', '');
		
	}
	if (!function_exists('to_one_dim_array_labels')) {

		function to_one_dim_array_labels($index, $array = []){
			if( is_array($array) ){
				$array = array_dot($array);
		     	$array = array_where($array, function ($key, $value) {
					return strpos($key, 'label_id');
				});
				$array = array_flatten($array);
				return $array;
			}
		}
	}

	if (!function_exists('filter_by_labels')) {
		function filter_by_labels($labels){
			$temp = [];
			foreach($labels as $label){
				if ( session()->has( 'search_input' ) && count(session()->get( 'search_input' )['interests']) > 0 ) {
					$interests = session()->get( 'search_input' )['interests'];
					if( in_array( (int)$label, $interests ) ){
						$temp[] = (int)$label;
					}
				}else{
					$temp[] = (int)$label;
				}
			}
			return $temp;
		}
	}

	if (!function_exists('get_pref_by_id')) {
		function get_pref_by_id($prefs, $ids, $index, $arr = true){
			$return = [];
			if( count($ids) > 0 ){
				foreach($prefs as $pref){
					if(in_array($pref['id'], $ids) ){
						$return[] = $pref[$index]; 
					}
				}
			}
			//dd($return);
			return $arr ? $return : implode(', ',$return) ;
		}
	}
	if( !function_exists('count_viator_act_duration') ){
		function count_viator_act_duration( $duration ){
			$result = 1;
			if ( $duration ) {
				$match = preg_match( '/hour|day/', $duration, $matches );
				$match = $match ? $matches[0] : '';
				switch ( $match ) {
					case 'hour':

						$hours = explode(' ', $duration );
						$hours = (int)$hours[0];
						$day = floor( $hours / 24 );
						$result = $day;

						break;
					case 'day':
						$days = explode(' ', $duration );
						$result = (int)$days[0];
						break;
					
					default:
						$result = 1;
						break;
				}
			}
			return $result;
		}
	}

	if ( !function_exists('get_viator_duration') ) {
		function get_viator_duration( $duration ){
			$match = preg_match('/[\d\.]+[\s|-]hour|[\d\.]+[\s|-]minute|[\d\.]+[\s|-]day/i', $duration, $matches);
			return $match ? ( preg_match('/minute/', $matches[0]) ? '1 day' : str_replace('-', ' ', $matches[0]) ): '1 day';
		}
	}

	if ( !function_exists('date_difference') ) {
		function date_difference( $first_date, $second_date ){
			$first_date = new DateTime($first_date);
			$second_date = new DateTime($second_date);

			$interval = $first_date->diff($second_date);
			//return $interval;
			return (int)$interval->format('%r%a');
		}
	}


}

if (!function_exists('api_is_active')) {
	function api_is_active($api) {
		$apis = json_decode(Cache::get('apis'));
		
		$found = [];
		foreach ($apis as $key => $value) {
			if (strtolower($value->name) == strtolower($api)) {
				$found = $value;
			}
		}
		return $found && $found->is_active == 'yes' ? true : false;
	}
}

if(!function_exists('get_timezone_abbreviation')){
	function get_timezone_abbreviation($timezone_id){
		$default = date_default_timezone_get();
		if($timezone_id){
	
			date_default_timezone_set ( $timezone_id );
			//$abbreviation = date('T O');
			//abbreviation = date('T').' (GMT+'.(date('O')/100).')';
			$abbreviation = date('T').' (GMT+'.(number_format((date('O')/100),2)).')';
			date_default_timezone_set ( $default );
			
			return $abbreviation;
		}
		return false;
	}
}

if ( !function_exists( 'get_hotel_category' )) {
	function get_hotel_category($code, $provider = ''){
		$stars = '';
		switch( $provider ){
			case 'hb':
				switch( $code ){
					case '5EST':
					case 'HS5':
					case 'APTH5':
					case '5LUX':
					case '5LL':
						$stars = get_stars( 5 );
					break;
					case '4EST':
					case 'HS4':
					case 'APTH4':
					case '4LUX':
					case '4LL':
						$stars = get_stars( 4 );
					break;
					case '3EST':
					case 'HS3':
					case 'APTH3':
					case '3LUX':
					case '3LL':
						$stars = get_stars( 3 );
					break;
					case '2EST':
					case 'HS2':
					case 'APTH2':
					case '2LUX':
					case '2LL':
						$stars = get_stars( 2 );
					break;
					case '1EST':
					case 'HS1':
					case 'APTH1':
					case '1LUX':
					case '1LL':
						$stars = get_stars( 1 );
					break;
					case 'H4_5':
						$stars = get_stars( 4, true );
					break;
					case 'H3_5':
						$stars = get_stars( 3, true );
					break;
					case 'H2_5':
						$stars = get_stars( 2, true );
					break;
					case 'H1_5':
						$stars = get_stars( 1, true );
					break;
					default: 
						$stars = get_stars(0);
					break;
				}
			break;
			case 'eroam':
				$stars = get_stars($code);
			break;
			case 'ae':
			break;
			case 'aot':
			break;
		}
		return $stars;
	}
}

if ( !function_exists( 'get_stars')) {
	function get_stars($count, $half = false){
		$stars = '';
		if( (int)$count ){
			for( $star = 1; $star <= $count; $star ++ ){
				$stars .= '<i class="fa fa-star"> </i> ';
			}
			$empty_stars = 5 - $count;
			if($half){
				$stars .= '<i class="fa fa-star-half-o"> </i> ';
				$empty_stars = $empty_stars - 1;
			}
			for( $empty = 1; $empty <= $empty_stars; $empty ++ ){
				$stars .= '<i class="fa fa-star-o"> </i> ';
			}
		}else{
			$stars .= '<i class="fa fa-star-o"> </i> <strong>Unranked</strong> ';

		}
		return $stars;
	}
}

if(!function_exists( 'countryTourCount')){
	function countryTourCount(){
		$key = 'tourCountryCount';
		Cache::forget($key);
		$result = Cache::get($key);
		
		if (!Cache::has($key)){
			$headers = [
				'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
				'Origin' => url('')
			];

			$countries = Cache::get( 'countries' );
			$allCountry = array();
	        foreach($countries as $country){
	      		foreach ($country['countries'] as $country_data){
					$allCountry[] = $country_data['name'];
				}
			}

			$allCountry = implode('# ', $allCountry);
			$result = http('post', 'tourCountryCount', ['countries' => $allCountry], $headers);

			$expiresAt = now()->addMinutes(241920);
	        // $expiresAt = date('Y-m-d H:i:s', strtotime('+24 week')); //Carbon::now()->addWeeks(24);
	        Cache::put($key, $result, $expiresAt);
		} 

		return $result;
	}
}

if(!function_exists( 'countryToursCount')){
	function countryToursCount($date = '', $tour_type = '' ){
		$key = 'tourCountryCounts';
		
		//Cache::forget($key);

		$result = Cache::get($key);

		if (!Cache::has($key)){

			$headers = [
				'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
				'Origin' => url('')
			];

			$countries = Cache::get( 'countries' );

			$allCountry = array();
	        foreach($countries as $country){
	      		foreach ($country['countries'] as $country_data){
					$allCountry[] = $country_data['name'];
				}
			}

			$allCountry = implode('# ', $allCountry);

			$result = http('post', 'tourCountryCounts', ['countries' => $allCountry,'date' => $date, 'tour_type' => $tour_type], $headers);

	        $expiresAt = now()->addMinutes(241920); //Carbon::now()->addWeeks(24);
	        Cache::put($key, $result, $expiresAt);
		} 

		return $result;
	}
}

if(!function_exists( 'cityTourCount')){
	function cityTourCount($country_id){
		$key = 'tourCityCount';		
		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];

		$cities = get_cities_by_country_id($country_id);
		$allCity = array();
		$i = 0;
		foreach($cities as $city){
      		$allCity[$i]['id'] = $city['id'];
      		$allCity[$i]['name'] = $city['name'];
      		$i++;
		}
		if(!empty($allCity))
		{
			$result = http('post', 'tourCityCount', ['cities' => $allCity], $headers);
			return $result;
		}
		return [];
	}
}

if(!function_exists( 'tourCounts_23_4')){
	function tourCounts_23_4(){
		$key = 'tourCityCount';		
		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];

		$country_tour_count = countryTourCount();
		
		$cities 	= Cache::get( 'cities' );
		$allCity 	= array();
		$i 			= 0;
		$tour_city 	= http('post', 'tourCityCountAll', [], $headers);

		foreach($cities as $city){

      		$search 	= searchForId($city['id'],$tour_city);
      		$country_id = $city['country_id'];
      		//if (empty($allCity[$city['country_id']])) {      		

      		if($search != 0)
      		{
      			if(!array_key_exists($city['country_id'],$allCity))
	      		{
	      			$allCity[$city['country_id']] = array();
	      			$allCity[$city['country_id']]['cities'] = array();
	      		}
      			$allCity[$city['country_id']]['country_count'] = $country_tour_count[$city['country_name']];
      			$allCity[$city['country_id']]['country_name'] = $city['country_name'];
      			$new_arr = array(
      					'city_id' 		=> $city['id'],
      					'city_name' 	=> $city['name'],
      					'city_count'	=> $search,
      					'country_name'	=> $city['country_name'],
      				);
      			array_push($allCity[$city['country_id']]['cities'],$new_arr);
      		}      		
		}

		if(!empty($allCity))
		{
			return $allCity;
		}
		return [];
	}

	function searchForId_23_4($id, $array) {
		$count = 0;
	   	foreach ($array as $key => $val) {
	       	if ($val['city_id'] === $id || $val['city_id1'] === $id) {	 
	       		$count =  $val['count_city'] + $count;
	       	}
	   	}
	   	return $count;
	}
}

if(!function_exists( 'tourCounts')){
	function tourCounts($date = '',$tour_type = ''){
		$key = 'tourCityCount';		
		$headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];


		$country_tour_count = countryTourCount();
		
		$cities 	= Cache::get( 'cities' );
		$allCity 	= array();
		$i 			= 0;
		$cData = [];
		
		$cData = array(
                'tour_type' => $tour_type
                );
        if(!empty($date) && $date != '1970-01-01')
        {
            $cData = array(
                'date' => $date,
                'tour_type' => $tour_type
                );
        }
        
        $tour_city 	= http('post', 'tourCityCountsAll', $cData, $headers);
		

		foreach($cities as $city){
			
      		$search 	= searchForId($city['id'],$tour_city);
      		$country_id = $city['country_id'];
      		//if (empty($allCity[$city['country_id']])) {      		
      		
      		if($search != 0)
      		{
      			if(!array_key_exists($city['country_id'],$allCity))
	      		{
	      			$allCity[$city['country_id']] = array();
	      			$allCity[$city['country_id']]['cities'] = array();
	      		}
      			$allCity[$city['country_id']]['country_count'] = $country_tour_count[$city['country_name']];
      			$allCity[$city['country_id']]['country_name']  = $city['country_name'];
      			$new_arr = array(
      					'city_id' 		=> $city['id'],
      					'city_name' 	=> $city['name'],
      					'city_count'	=> $search,
      					'country_name'	=> $city['country_name'],
      				);
      			array_push($allCity[$city['country_id']]['cities'],$new_arr);
      		}      		
		}

		if(!empty($allCity))
		{
			return $allCity;
		}
		return [];
	}

	function searchForId($id, $array) {
		$count = 0;
		
	   	foreach ($array as $key => $val) {
	       	//if ($val['city_id'] === $id || $val['city_id1'] === $id) {	 
	   		if ($val['city_id'] === $id ) {	 
	       		$count =  $val['count_city'] + $count;
	       	}
	   	}
	   	return $count;
	}
	if ( !function_exists('getExpediaHotelPriceWithEroamMarkup') ) {
		function getExpediaHotelPriceWithEroamMarkup($Rate,$nights,$taxes,$eroamPercentage){
		 $Rate = round(($Rate * $eroamPercentage) / 100 + $Rate,2); 
         $subTotal = $nights * $Rate;
         $Rate = $subTotal + $taxes;
         return $Rate;            
		}
	}
	if ( !function_exists('getExpediaHotelPriceWithoutEroamMarkup') ) {
		function getExpediaHotelPriceWithoutEroamMarkup($Rate,$nights,$taxes){
			 $Rate = round($Rate,2); 
	         $subTotal = $nights * $Rate;
	         $Rate = $subTotal + $taxes;
	         return $Rate; 
		}
	}
}