<div id="myModal" class="modal fade bd-manual-modal-lg" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer">
                <div class="modal-header">
                    <h4>Manual Itinerary</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <i class="ic-clear"></i>
                     </button>
                </div>
                <div class="modalform">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ url('images/auto-map.jpg') }}">
                        </div>
                        <div class="col-md-8">
                            <p>
                               Manual Itinerary allows the user to plot a multi-city itinerary by clicking on an interactive map. As you add each new location ‘trending’ data will recommended the best tours, transport and hotels to suit ythe travellers profile. Once this itinerary has been created, you have the ability to edit any segment using the interactive map, or booking summary panel.
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>