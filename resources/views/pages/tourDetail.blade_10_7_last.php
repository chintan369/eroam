@include( 'layouts.partials.header' )
<div class="body_sec">
    <div class="itinerary_block">
        <div class="container-fluid">
            <div class="row mt-4 tour-details">
                <div class="col-xl-10 offset-xl-1">
                    <div class="card panel p-3 mb-2">
                        <div class="row">
                            <div class="col-md-7 col-xl-8">
                                <h5 class="font-weight-bold">{{ $data['detail']['tour_title'] }}</h5>
                                <div class="media">
                                    <i class="ic-tour fa-lg mr-2"></i>
                                    <div class="media-body pb-3 mb-0">
                                        Tour Code: {{ $data['detail']['tour_code']}} | Countries: {{$allCountry}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-xl-4">
                                <div class="row">
                                  <div class="col-sm-8 col-xl-9 col-8 text-md-right border-right">
                                        @if($data['detail']['flightPrice'] > 0)
                                            From <strong>${{ $default_currency.' '.number_format($data['detail']['price'] + $data['detail']['flightPrice'] ,2)}}</strong> P.P (Inclusive Flights)<br>
                                        @else
                                            From <strong>${{ $default_currency.' '.number_format($data['detail']['price'],2)}}</strong> P.P (Land Only)<br>
                                        @endif

                                        @if($data['detail']['discount'] > 0)                
                                            <span>eRoam Discount {{$data['detail']['discount']}}%</span><br>
                                        @endif
                                        @if($data['detail']['saving_per_person'] > 0)                        
                                            <span>Savings From<strong> ${{$default_currency}} {{number_format($data['detail']['saving_per_person'],2)}}</strong> P.P</span>
                                        @endif
                                  </div>
                                  <div class="col-sm-4 col-xl-3 col-4 text-center">
                                    <h2 class="font-weight-bold mb-0">{{$data['detail']['no_of_days']}}</h2>
                                    {{@$total_duration}}
                                  </div>
                                </div>
                            </div>
                        </div>
                        <hr>
            <div class="accomodation-details mb-5 lh-condensed">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-xl-2 col-sm-4 col-5">
                                <span class="font-weight-bold">Countries:</span>
                            </div>
                            <div class="col-xl-10 col-sm-8 col-7">
                                <span>{{substr($allCountry, 0,-2)}}</span>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-2 col-sm-4 col-5">
                                <span class="font-weight-bold">Route:</span>
                            </div>
                            <div class="col-xl-10 col-sm-8 col-7">
                                <span>{{ $data['detail']['departure'].' - '.$data['detail']['destination'] }}</span>
                            </div>
                        </div>
                        @if(!empty($data['detail']['tripActivities']))
                            <div class="row mt-2">
                                <div class="col-xl-2 col-sm-4 col-5">
                                  <span class="font-weight-bold">Tour Theme:</span>
                                </div>
                                <div class="col-xl-10 col-sm-8 col-7">
                                  <span>{{ $tripActivities}}</span>
                                </div>
                            </div>                        
                        @endif
                        <div class="row mt-2">
                            <div class="col-xl-2 col-sm-4 col-5">
                              <span class="font-weight-bold">Accommodation:</span>
                            </div>
                            <div class="col-xl-10 col-sm-8 col-7">
                              <span>{{ $data['detail']['accommodation']}}</span>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-2 col-sm-4 col-5">
                              <span class="font-weight-bold">Transport:</span>
                            </div>
                            <div class="col-xl-10 col-sm-8 col-7">
                              <span>{{ $data['detail']['transport']}}</span>
                            </div>
                        </div>
                    </div>
                <div class="col-md-4">
                    <div class="row mt-2">
                        <div class="col-xl-3 col-sm-4 col-5">
                            <span class="font-weight-bold">Group Size:</span>
                        </div>
                        <div class="col-xl-9 col-sm-8 col-7">
                            <span>Min {{ $data['detail']['groupsize_min']}} @if($data['detail']['groupsize_max'] != 0) - Max {{ $data['detail']['groupsize_max']}} @endif</span>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-3 col-sm-4 col-5">
                            <span class="font-weight-bold">Children:</span>
                        </div>
                        <div class="col-xl-9 col-sm-8 col-7">
                            <span>{{ ($data['detail']['is_childAllowed'] == 1 ? "Yes":"No")}}</span>
                        </div>
                    </div>

                    @if($data['detail']['children_age'] != '' && $data['detail']['is_childAllowed'] == 1)
                    <div class="row mt-2">
                        <div class="col-xl-3 col-sm-4 col-5">
                            <span class="font-weight-bold">Children Age: </span>
                        </div>
                        <div class="col-xl-9 col-sm-8 col-7">
                            <span>{{ $data['detail']['children_age']}}</span>
                        </div>
                    </div>
                    @endif
                </div>
                </div>

                @if($data['images'])
                    <div class="jcarousel-wrapper">
                        <div class="jcarousel">  
                            <ul class="accomodationImg-list">
                                @php
                                    $i=0;
                                    foreach($data['images'] as $key => $image){
                                        $i++;
                                        $image['image_thumb'] = str_replace("thumbnail","small",$image['image_thumb']);
                                        if($image['image_thumb'] != '' && $image['image_small'] != ''){
                                           $imageUrl2 = "http://dev.cms.eroam.com/".$image['image_thumb'];
                                        }else{
                                           $imageUrl2 = "http://www.adventuretravel.com.au/".$data['detail']['folder_path']."245x169/".$image['image_thumb'];
                                        }
                                        echo '<li><img src="'.$imageUrl2.'" alt="Image '.$i.'"></li>';
                                    }
                                @endphp                              
                            </ul>
                        </div>
                        <a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true"><i class="fas fa-angle-left"></i></a>
                        <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"><i class="fas fa-angle-right"></i></a>
                        <p class="jcarousel-pagination"></p>                 
                    </div>
                @endif
            </div>
            <ul class="nav nav-pills nav-fill">
              <li class="nav-item mb-3 mb-md-0 pl-md-0 pl-sm-2">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">VIEW ITINERARY</a>
              </li>
              <li class="nav-item mb-3 mb-md-0">
                <a class="nav-link" data-toggle="modal" data-target="#enquiry-modal">MAKE AN ENQUIRY</a>
              </li>
              <li class="nav-item mb-3 mb-md-0 pr-md-0 pr-sm-2">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">DATES &amp; RATES</a>
              </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="mt-4">
                        <h4>Overview</h4>
                        <p>{!! $data['detail']['long_description'] !!}</p>
                        <h4 class="mt-4">Tour Itinerary</h4>
                        {!! $data['detail']['xml_itinerary'] !!}
                    </div>
                </div>
             
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    @if(!empty($viatorDates))                    
                        <form action="<?php echo url('tourDetail/'.$data["detail"]["tour_id"].'/'.$data["detail"]["tour_url"].''); ?>" method="post">
                       {{ csrf_field() }}
                    @endif 
                    <div class="row mt-5">
                        <div class="col-md-6 offset-md-6 col-sm-10 offset-sm-2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="fildes_outer">
                                        <label>Jump to a Departure Month</label>
                                        <div class="input-group ">
                                            <input id="start_date6" type="text" placeholder="Select Month" class="form-control" name="start_date5" value="{{@$start_date}}">
                                            <span class="input-group-addon px-2 py-2"><i class="ic-calendar"></i></span>
                                        </div>
                                        <label class="red-text" id="red-text" style="display:none;">Please select month</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">              
                                     @if(!empty($viatorDates))
                                        <button type="submit" name="" class="btn btn-white btn-block mt-2 mt-sm-0 update_date_search">EXPLORE FURTHER</button>
                                    </form>
                                    @else
                                        <button type="button" name="" class="btn btn-white btn-block mt-2 mt-sm-0 update_date_search">EXPLORE FURTHER</button>
                                    @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="px-2 d-none d-md-block">
                        <div class="row mt-4">
                            <div class="col-md-3 col-sm-6">
                              DEPARTING
                            </div>
                            <div class="col-md-3 col-sm-6">
                              FINISHING
                            </div>
                            <div class="col-md-5 col-sm-9">
                              TOTAL FROM $AUD
                            </div>
                            <div class="col-md-1 col-sm-3"></div>
                        </div>
                    </div>
                    <div class="accordion mt-2 dateRates-section" id="accordionExample">
                        @php                    
                            $i=0; $totalCost = 0; $singalCost = 0;
                            $startDate  = '';
                            $finishDate = '';
                        
                            if(!empty($getAllDates))
                            {

                                foreach ($getAllDates as $dates) {
                                $i++; 
                                $start_date = date('F-Y',strtotime($dates['start_date']));
                                $price = number_format($dates['lowest_pp2a_prices'][1]['amount'],2); 
                                $price_single = $dates['lowest_pp2a_prices'][1]['amount']; 
                                $price_total = $dates['lowest_pp2a_prices'][1]['amount'];
                                $flag_var = 'flightNoCls'; 

                                $date1Timestamp = strtotime($dates['start_date']);
                                $date2Timestamp = strtotime($dates['finish_date']);
                                //Calculate the difference.
                                $difference = $date2Timestamp - $date1Timestamp;                               
                                $no_of_days         = floor($difference / (60*60*24) );

                                $total_duration     = "Day";
                                if($no_of_days > 1)
                                {
                                    $total_duration     = "Days";
                                }
                                @endphp
                                <form action="{{url('signin-guest-checkout-tour')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="card panel border-0 mb-3 {{$flag_var}} <?php echo $start_date;?>">
                                        <div class="border-bottom accordion-title" id="headingOne">
                                            <div class="px-2 py-3 mb-0 row" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <div class="col-md-3 col-sm-6 col-6">
                                                    <span class="d-md-none d-block font-weight-bold">DEPARTING</span>
                                                    <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($dates['start_date']))}}</p>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-6">
                                                    <span class="d-md-none d-sm-block font-weight-bold">FINISHING</span>
                                                    <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($dates['finish_date']))}}</p>
                                                </div>
                                                <div class="col-md-5 col-sm-10 col-9">
                                                    <span class="d-md-none d-sm-block font-weight-bold">TOTAL FROM ${{ $default_currency}} </span>
                                                    <p class="mb-2 mb-md-0">${{ $default_currency}} {{number_format($dates['lowest_pp2a_prices'][1]['amount'],2)}} Per Person (Land Only)</p>
                                                    <p class="withFlights">${{ $default_currency}} {{$price}} Per Person (Inclusive Flights)</p>
                                                    <p class="<?php echo $start_date;?>-withOutFlights" style="display:none;">{{number_format($dates['lowest_pp2a_prices'][1]['amount'],2)}} (Land Only)</p>
                                                    <p class="<?php echo $start_date;?>-withFlights" style="display:none;">{{$price}} (Inclusive Flights)</p>
                                                </div>
                                                <div class="col-md-1 col-sm-2 col-3 text-right">
                                                    <a role="button" id="collapsedBtn1" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="px-2 py-3">
                                                <h5>{{ $data['detail']['tour_title'] }}</h5>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="media">
                                                        <i class="ic-tour fa-lg mr-2"></i>
                                                            <div class="media-body pb-3 mb-0">
                                                                <p class="mb-2"><span>Tour Code: </span> {{ $data['detail']['code']}}</p>
                                                                <p class="mb-2"><span>Countries:</span> {{$allCountry}}</p>
                                                                <p class="mb-2"><span>Duration:</span> {{$no_of_days}} {{$total_duration}}</p>
                                                                <p class="mb-2"><span>Start:</span> {{$data['detail']['departure']}}, {{$data['detail']['departure_country']}}</p>
                                                                <p class="mb-2"><span>Finish:</span> {{$data['detail']['destination']}}, {{$data['detail']['destination_country']}}</p>

                                                                @php
                                                                    $Flight = ''; 
                                                                      if(!empty($t_return)){
                                                                        if(array_key_exists($date['season_id'],$t_return)){
                                                                          $default_selected_city = session()->get( 'default_selected_city' );
                                                                          $city_name = 'MEL';
                                                                          if($default_selected_city == 7){
                                                                            $city_name = 'SYD';
                                                                          }elseif($default_selected_city == 15){
                                                                            $city_name = 'BNE';
                                                                          }elseif($default_selected_city == 30){
                                                                            $city_name = 'MEL';
                                                                          }
                                                                          $Flight = $city_name;  
                                                                        }
                                                                      }
                                                                      if(!empty($Flight)){
                                                                @endphp
                                                                    <p class="mb-2"><span>Flight:</span> Return Flights From ({{$Flight}})</p>
                                                                @php
                                                                    $Flight = '';
                                                                } 
                                                                @endphp
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 text-md-right">
                                                        ${{ $default_currency}} <h2 class="d-inline font-weight-bold">{{ $price}}</h2> (Land Only)
                                                        <p>Per Person in a Twin Share Room</p>
                                                        <p class="mb-0">Want your own single Room?</p>
                                                        <em>From an extra</em> ${{ $default_currency}} {{ $price}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <small class="d-block mt-4 mb-3 mb-md-0">All prices are inclusive of all relevant Government taxes & GST and are per person in Australian dollars.</small>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="col-sm-4">
                                                            <input type="hidden" name="singlePrice" id="singlePrice{{$i}}" value="{{$price}}">
                                                            <input type="hidden" class="dateRedio" name="price" id="radio-d{{$i}}" value="{{$price}}">
                                                            <input type="hidden" name="singlePrice" id="singlePrice{{$i}}two" value="{{$price_single}}">
                                                            <input type="hidden" class="dateRedio" name="date" id="radio-d{{$i}}two" value="{{$price_total}}">
                                                            <input type="hidden" class="dateRedio" name="index" value="{{$i}}">
                                                            <input type="hidden" name="tour_name" value="{{ $data['detail']['tour_title'] }}">
                                                            <input type="hidden" name="tour_id" value="{{ $data['detail']['tour_id'] }}">
                                                            <input type="hidden" name="start_date" value="{{$dates['start_date']}}">
                                                            <input type="hidden" name="no_of_days" value="{{$data['detail']['no_of_days']}}">
                                                            <input type="hidden" name="durationType" value="{{$data['detail']['durationType']}}">
                                                            <input type="hidden" id="startDate{{$i}}" value="{{ date('Y-m-d H:i:s', strtotime($dates['start_date'])) }}">
                                                            <input type="hidden" id="finishDate{{$i}}" value="{{ date('Y-m-d H:i:s', strtotime($dates['finish_date'])) }}" name="finishDate">
                                                            <input type="hidden" name="code" value="{{ $data['detail']['code'] }}">
                                                            <input type="hidden" name="provider" value="{{ $data['detail']['provider'] }}">
                                                            <input type="hidden" name="from" value="api">
                                                        <button type="submit" name="" class="btn btn-white btn-block bookButtons" id="bookButton_{{$i}}">BOOK TOUR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </form>
                                @php
                                }
                            }
                            else if(!empty($viatorDates))
                            {
                                foreach ($viatorDates as $dates) {
                                $i++; 
                                $start_date     = date('Y-m-d',strtotime($dates['bookingDate']));
                                $no_of_days     = $data['detail']['no_of_days'];
                                $finish_date    = date('Y-m-d', strtotime($dates['bookingDate']. ' + '.$no_of_days.' day'));
                                $price          = $dates['tourGrades'][0]['pricingMatrix'][0]['ageBandPrices'];
                                $price_adult    = number_format($price[0]['prices'][0]['price'],2); 
                                $price_child    = number_format($price[1]['prices'][0]['price'],2); 
                                $price_infant   = number_format($price[2]['prices'][0]['price'],2); 
                                
                                $price_single   = $price_adult;
                                $price_total    = $price_adult;
                                $flag_var       = 'flightNoCls'; 

                                

                                $date1Timestamp = strtotime($start_date);
                                $date2Timestamp = strtotime($finish_date);
                                //Calculate the difference.
                                $difference = $date2Timestamp - $date1Timestamp;                              
                                $no_of_days         = floor($difference / (60*60*24) );

                                $total_duration     = "Day";
                                if($no_of_days > 1)
                                {
                                    $total_duration     = "Days";
                                }
                                @endphp  
                                <form action="{{url('signin-guest-checkout-tour')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="card panel border-0 mb-3 {{$flag_var}} <?php echo $start_date;?>">
                                        <div class="border-bottom accordion-title" id="headingOne">
                                            <div class="px-2 py-3 mb-0 row" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <div class="col-md-3 col-sm-6 col-6">
                                                    <span class="d-md-none d-block font-weight-bold">DEPARTING</span>
                                                    <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($dates['start_date']))}}</p>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-6">
                                                    <span class="d-md-none d-sm-block font-weight-bold">FINISHING</span>
                                                    <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($dates['finish_date']))}}</p>
                                                </div>
                                                <div class="col-md-5 col-sm-10 col-9">
                                                    <span class="d-md-none d-sm-block font-weight-bold">TOTAL FROM ${{ $default_currency}} </span>
                                                    <p class="mb-2 mb-md-0">${{ $default_currency}} {{number_format($dates['lowest_pp2a_prices'][1]['amount'],2)}} Per Person (Land Only)</p>
                                                    <p class="withFlights">${{ $default_currency}} {{$price}} Per Person (Inclusive Flights)</p>
                                                    <p class="<?php echo $start_date;?>-withOutFlights" style="display:none;">{{number_format($dates['lowest_pp2a_prices'][1]['amount'],2)}} (Land Only)</p>
                                                    <p class="<?php echo $start_date;?>-withFlights" style="display:none;">{{$price}} (Inclusive Flights)</p>
                                                </div>
                                                <div class="col-md-1 col-sm-2 col-3 text-right">
                                                    <a role="button" id="collapsedBtn1" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="px-2 py-3">
                                                <h5>{{ $data['detail']['tour_title'] }}</h5>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="media">
                                                        <i class="ic-tour fa-lg mr-2"></i>
                                                            <div class="media-body pb-3 mb-0">
                                                                <p class="mb-2"><span>Tour Code: </span> {{ $data['detail']['code']}}</p>
                                                                <p class="mb-2"><span>Countries:</span> {{$allCountry}}</p>
                                                                <p class="mb-2"><span>Duration:</span> {{$no_of_days}} {{$total_duration}}</p>
                                                                <p class="mb-2"><span>Start:</span> {{$data['detail']['departure']}}, {{$data['detail']['departure_country']}}</p>
                                                                <p class="mb-2"><span>Finish:</span> {{$data['detail']['destination']}}, {{$data['detail']['destination_country']}}</p>

                                                                @php
                                                                    $Flight = ''; 
                                                                      if(!empty($t_return)){
                                                                        if(array_key_exists($date['season_id'],$t_return)){
                                                                          $default_selected_city = session()->get( 'default_selected_city' );
                                                                          $city_name = 'MEL';
                                                                          if($default_selected_city == 7){
                                                                            $city_name = 'SYD';
                                                                          }elseif($default_selected_city == 15){
                                                                            $city_name = 'BNE';
                                                                          }elseif($default_selected_city == 30){
                                                                            $city_name = 'MEL';
                                                                          }
                                                                          $Flight = $city_name;  
                                                                        }
                                                                      }
                                                                      if(!empty($Flight)){
                                                                @endphp
                                                                    <p class="mb-2"><span>Flight:</span> Return Flights From ({{$Flight}})</p>
                                                                @php
                                                                    $Flight = '';
                                                                } 
                                                                @endphp
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 text-md-right">
                                                        ${{ $default_currency}} <h2 class="d-inline font-weight-bold">{{ $price}}</h2> (Land Only)
                                                        <p>Per Person in a Twin Share Room</p>
                                                        <p class="mb-0">Want your own single Room?</p>
                                                        <em>From an extra</em> ${{ $default_currency}} {{ $price}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <small class="d-block mt-4 mb-3 mb-md-0">All prices are inclusive of all relevant Government taxes & GST and are per person in Australian dollars.</small>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="col-sm-4">
                                                            <input type="hidden" name="singlePrice" id="singlePrice{{$i}}" value="{{$price}}">
                                                            <input type="hidden" class="dateRedio" name="price" id="radio-d{{$i}}" value="{{$price}}">
                                                            <input type="hidden" name="singlePrice" id="singlePrice{{$i}}two" value="{{$price_single}}">
                                                            <input type="hidden" class="dateRedio" name="date" id="radio-d{{$i}}two" value="{{$price_total}}">
                                                            <input type="hidden" class="dateRedio" name="index" value="{{$i}}">
                                                            <input type="hidden" name="tour_name" value="{{ $data['detail']['tour_title'] }}">
                                                            <input type="hidden" name="tour_id" value="{{ $data['detail']['tour_id'] }}">
                                                            <input type="hidden" name="start_date" value="{{$dates['start_date']}}">
                                                            <input type="hidden" name="no_of_days" value="{{$data['detail']['no_of_days']}}">
                                                            <input type="hidden" name="durationType" value="{{$data['detail']['durationType']}}">
                                                            <input type="hidden" id="startDate{{$i}}" value="{{ date('Y-m-d H:i:s', strtotime($dates['start_date'])) }}">
                                                            <input type="hidden" id="finishDate{{$i}}" value="{{ date('Y-m-d H:i:s', strtotime($dates['finish_date'])) }}" name="finishDate">
                                                            <input type="hidden" name="code" value="{{ $data['detail']['code'] }}">
                                                            <input type="hidden" name="provider" value="{{ $data['detail']['provider'] }}">
                                                            <input type="hidden" name="from" value="api">
                                                        <button type="submit" name="" class="btn btn-white btn-block bookButtons" id="bookButton_{{$i}}">BOOK TOUR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </form>
                                @php
                                }
                            }
                            else if(!empty($data['dates'])){
                                foreach ($data['dates'] as $date) {
                              
                                $i++;
                                if($i == 1)
                                { 
                                    $totalCost  = $date['total']; 
                                    $singalCost = $date['singlePrice']; 
                                    $startDate  = date('Y-m-d H:i:s', strtotime($data['dates'][0]['startDate']));
                                    $finishDate = date('Y-m-d H:i:s', strtotime($data['dates'][0]['finishDate']));
                                }                             
                                  
                                $start_date = date('F-Y',strtotime($date['startDate']));
                                $price = number_format($date['total'],2); 
                                $price_single = $date['singlePrice']; 
                                $price_total = $date['total'];
                                $flag_var = 'flightNoCls'; 

                                if(!empty($season))
                                {

                                    if(array_key_exists($date['season_id'],$season))
                                    {
                                        $price = number_format($date['total'] + $season[$date['season_id']],2);
                                        $totalCost  = $date['total'] + $season[$date['season_id']]; 
                                        $singalCost = $date['singlePrice'] + $season[$date['season_id']];
                                        $price_single =$date['singlePrice'] + $season[$date['season_id']];
                                        $price_total = $date['total'] + $season[$date['season_id']];  
                                        $flag_var = 'flightYesCls';
                                    }
                                } 
                                @endphp
                                <form action="{{url('signin-guest-checkout-tour')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="card panel border-0 mb-3 {{$flag_var}} <?php echo $start_date;?>">
                                        <div class="border-bottom accordion-title" id="headingOne">
                                            <div class="px-2 py-3 mb-0 row" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <div class="col-md-3 col-sm-6 col-6">
                                                    <span class="d-md-none d-block font-weight-bold">DEPARTING</span>
                                                    <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($date['startDate']))}}</p>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-6">
                                                    <span class="d-md-none d-sm-block font-weight-bold">FINISHING</span>
                                                    <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($date['finishDate']))}}</p>
                                                </div>
                                                <div class="col-md-5 col-sm-10 col-9">
                                                    <span class="d-md-none d-sm-block font-weight-bold">TOTAL FROM ${{ $default_currency}} </span>
                                                    <p class="mb-2 mb-md-0">${{ $default_currency}} {{number_format($date['total'],2)}} Per Person (Land Only)</p>
                                                    <p class="withFlights">${{ $default_currency}} {{$price}} Per Person (Inclusive Flights)</p>
                                                    <p class="<?php echo $start_date;?>-withOutFlights" style="display:none;">{{number_format($date['total'],2)}} (Land Only)</p>
                                                    <p class="<?php echo $start_date;?>-withFlights" style="display:none;">{{$price}} (Inclusive Flights)</p>
                                                </div>
                                                <div class="col-md-1 col-sm-2 col-3 text-right">
                                                    <a role="button" id="collapsedBtn1" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="px-2 py-3">
                                                <h5>{{ $date['departureName'] }}</h5>
                                                <h5>{{ $data['detail']['tour_title'] }}</h5>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="media">
                                                        <i class="ic-tour fa-lg mr-2"></i>
                                                            <div class="media-body pb-3 mb-0">
                                                                <p class="mb-2"><span>Tour Code: </span> {{ $date['departureCode']}}</p>
                                                                <p class="mb-2"><span>Countries:</span> {{$allCountry}}</p>
                                                                <p class="mb-2"><span>Duration:</span> {{$data['detail']['no_of_days']}} {{@$total_duration}}</p>
                                                                <p class="mb-2"><span>Start:</span> {{$data['detail']['departure']}}, {{$data['detail']['departure_country']}}</p>
                                                                <p class="mb-2"><span>Finish:</span> {{$data['detail']['destination']}}, {{$data['detail']['destination_country']}}</p>

                                                                @php
                                                                    $Flight = ''; 
                                                                    if(!empty($t_return)){
                                                                        if(array_key_exists($date['season_id'],$t_return)){
                                                                        $default_selected_city = session()->get( 'default_selected_city' );
                                                                        $city_name = 'MEL';
                                                                        if($default_selected_city == 7){
                                                                            $city_name = 'SYD';
                                                                        }elseif(                        $default_selected_city == 15){
                                                                        $city_name = 'BNE';
                                                                        }elseif(    $default_selected_city == 30){
                                                                        $city_name = 'MEL';
                                                                        }
                                                                        $Flight = $city_name;  
                                                                        }
                                                                    }
                                                                    if(!empty($Flight)){
                                                                @endphp
                                                                    <p class="mb-2"><span>Flight:</span> Return Flights From ({{$Flight}})</p>
                                                                @php
                                                                    $Flight = '';
                                                                } 
                                                                @endphp
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 text-md-right">
                                                        ${{ $default_currency}} <h2 class="d-inline font-weight-bold">{{ number_format($date['total'],2)}}</h2> (Land Only)
                                                        <p>Per Person in a Twin Share Room</p>
                                                        <p class="mb-0">Want your own single Room?</p>
                                                        <em>From an extra</em> ${{ $default_currency}} {{ $price}}
                                                        <?php 
                                                        if($date['supplement'] > 0){
                                                          ?>
                                                            in a Twin Share Room
                                                            <p class="m-t-10"><strong>Want your own single Room?</strong><br > <i>From an extra</i> ${{ $default_currency.' '.number_format($date['supplement'],2)}} </p>
                                                          <?php
                                                        }
                                                      ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <small class="d-block mt-4 mb-3 mb-md-0">All prices are inclusive of all relevant Government taxes & GST and are per person in Australian dollars.</small>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="col-sm-4">
                                                            <input type="hidden"  name="singlePrice" id="singlePrice{{$i}}" value="{{$date['singlePrice']}}" >
                                                            <input type="hidden" class="dateRedio" name="price" id="radio-d{{$i}}" value="{{$date['total']}}" >
                                                            <input type="hidden"  name="singlePrice" id="singlePrice{{$i}}two" value="{{$price_single}}" >
                                                            <input type="hidden" class="dateRedio" name="date" id="radio-d{{$i}}two" value="{{$price_total}}" >
                                                            <input type="hidden" class="dateRedio" name="index"  value="{{$i}}" >
                                                            <input type="hidden" name="tour_name" value="{{ $data['detail']['tour_title'] }}"> 
                                                            <input type="hidden" name="tour_id" value="{{ $data['detail']['tour_id'] }}">
                                                            <input type="hidden" name="start_date" value="{{$date['startDate']}}"> 
                                                            <input type="hidden" name="no_of_days" value="{{$data['detail']['no_of_days']}}">
                                                            <input type="hidden" name="durationType" value="{{$data['detail']['durationType']}}">
                                                            <input type="hidden" id="startDate{{$i}}" value="{{ date('Y-m-d H:i:s', strtotime($date['startDate'])) }}">
                                                            <input type="hidden" id="finishDate{{$i}}" value="{{ date('Y-m-d H:i:s', strtotime($date['finishDate'])) }}" name="finishDate" >
                                                            <input type="hidden" name="code" value="{{ $data['detail']['code'] }}">
                                                            <input type="hidden" name="provider" value="{{ $data['detail']['provider'] }}">
                                                            <input type="hidden" name="from" value="">
                                                            <button type="submit" name="" class="btn btn-white btn-block bookButtons" id="bookButton_{{$i}}">BOOK TOUR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </form>  
                            @php
                               } } else {  echo '<div><center>No records found.</center></div>'; } 
                            @endphp
                            <div id="no_records" style="display:none;"><center>No departure date found.</center></div>                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div> 
  </div>
</div>
@include( 'layouts.partials.footer' )