<div class="itinerary_filter pt-5">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="itinerary_titles pl-5">
                <i class=" ic-place location "></i>
                <h5>{{$data['city_name']}}, {{$data['city']['country_name']}}</h5>
                <p class="hotel-count">138 Properties Available</p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="form-group">
                <div class="fildes_outer">
                    <label>@lang('home.accommodation_placeholder_search') </label>
                    <input type="text" class="form-control" id="search_val" placeholder="@lang('home.search_text')" data-toggle="tooltip">
                	<span class="input-group-btn">
	                    <button class="btn btn-default" type="button" onclick="getHotelSearch();"></button>
	                </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="checkinout">
                <div class="row">
                    <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <div class="fildes_outer">
                            <label>@lang('home.create_itenerary_checkin'):</label>
                            <input type="text" class="form-control disabled" id="checkin-date" placeholder="{{date('d M Y',strtotime($data['checkin']))}}" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
                            <span class="arrow_down"><i class="ic-calendar"></i></span>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <div class="fildes_outer">
                            <label>@lang('home.map_label_13') :</label>
                            <input type="text" class="form-control disabled" id="checkout-date" placeholder="{{date('d M Y',strtotime($data['checkout']))}}" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
                            <span class="arrow_down"><i class="ic-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
            <div class="row">
                <div class="form-group col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <div class="fildes_outer">
                        <label>@lang('home.map_label_14') </label>
                        <div class="custom-select">
                            <select class="disabled" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
                                <option value="{{$rooms}}">{{$rooms}} @lang('home.room_text_label') </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <div class="fildes_outer">
                        <label>@lang('home.adults')</label>
                        <div class="custom-select">
                            <select class="disabled" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
                                <option value="{{$travellers}}">{{$travellers}} @lang('home.adult_text_label')</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <div class="fildes_outer">
                        <label>@lang('home.childrens')</label>
                        <div class="custom-select">
                            <select class="disabled" data-toggle="tooltip" data-placement="bottom" title="@lang('home.pilot_text')">
                                <option value="volvo">@lang('home.map_label5')</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="filter_poperties border-bottom pl-3 pb-1 mb-5">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-12 col-xl-9 ">
                <span>@lang('home.filter_property_text') </span>
                <div class="transport_filter">
                    <ul class="nav nav-pills nav-fill">
                        <li class="nav-item dropdown">
                           <a class="nav-link" id="RatingAsc" href="#">@lang('home.map_filter_text2')<i class="fa fa-caret-down ml-2" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-controls="myTabDrop1-contents">@lang('home.map_filter_text3') </a>
                            <ul class="dropdown-menu border-0 rounded-0" id="property-tab">
                                <li><a href="#" class="dropdown-item" id="searchProperty">@lang('home.all_text')</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" id="priceSortAsc"> @lang('home.map_filter_text5')<i class="fa fa-caret-down ml-2" aria-hidden="true"></i> </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-12 col-xl-3">
                <div class="float-right filtericons">
                    <a href="" id="listView" class="active"><i class="ic-menu"></i></a>
                    <a href="" id="gridView"><i class="ic-apps"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
.disabled{
	cursor: no-drop;
    color: #777 !important;
    background-color: #fff !important;
    border-color: #fff!important;
}
</style>