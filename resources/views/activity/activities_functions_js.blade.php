<script>
    /*
	| Set all required variable & Moved some variables
	*/
    var eroamResponse_length = 0;
    var index, cityId, dateFrom, selectedActivities, destinationId, totalActivityCount = 0;

    var search_session = JSON.parse($('#search-session').val());
    var dates = ['{!! session()->has('dates_available_'.$city_id) ? join("', '", session()->get('dates_available_'.$city_id) ) : '0000/00/00' !!}'];
    var cityName = "{{ $city->name.', '.$city->country_name }}";
    var cityId = {{$city_id}};
    var dateFrom = formatDate("{{$date_from}}");
    var dateTo = formatDate("{{$date_to}}");
    var postData;
    var destinationId = {{$destinationId}};
    var selected_dates = {{!!join_array_key_value($selected_dates) !!}};
    var maxPrice = 0;
    //var travellers = parseFloat(search_session.travellers);
    var travel_pref = [{{join(', ', $activity_pref)}}];
    var available_to_book = {{$available_to_book ? 1 : 0 }};
    var departureDate = "{{ $departure_date }}";
    var cityData = JSON.parse($('.the-city-id').val());
    var leg = {{$leg}};

    $(document).ready(function() {
        selectedActivities = $('#selected-activities').val() != 0 ? JSON.parse($('#selected-activities').val()) : 0;

        buildActivities();

        $('.sort-asc-desc').click(function() {
            $('.sort-asc-desc').find('i').removeClass('white black-background');
            $(this).find('i').toggleClass('white black-background');
            var asc_desc = $(this).find('i').attr('data-sort');
            var asc = asc_desc == 'asc' ? true : false;
            activitySort(asc);
        });

        /**************************** View more info btn function ********************/
        $('body').on('click', '.view-more-btn', showActivityDetails);
        $('body').on('click', '.act-btn-less', function() {
            var provider = $(this).data('provider');
            var code = $(this).data('code');
            $('#view-more-details').html('').hide();
            $('#activities-container').show();
            var activity = $('#activities-container').find('.view-more-btn[data-provider="' + provider + '"][data-code="' + code + '"]').offset().top;
            $('.content-body').scrollTop(activity - 250);
        });
        /**************************** END view more info btn function ********************/
        $('.activity-list').slimScroll({
            height: '700px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });


    }); // END OF DOCUMENT READY METHOD
    $('input.datetimepick').css({
        'display': 'none'
    });

    function hide_input() {
        //$('input.datetimepick').css({'display':'none'});
    }



    function showActivityDetails(event) {
        event.preventDefault();
        var provider = $(this).data('provider');
        var code = $(this).data('code');
        var select = $(this).data('select');
        var cityid = $(this).data('cityid');

        eroam.ajax('get', 'activity/view-more', {
            provider: provider,
            code: code,
            select: select,
            cityid: cityid,
            leg: leg
        }, function(response) {
            $('#view-more-details').html(response).show();

            $(function() {
                calculateHeight();
                $('.activityDesc').slimScroll({
                    height: '95%',
                    color: '#212121',
                    opacity: '0.7',
                    size: '5px',
                    allowPageScroll: true
                });


            });
        }, function() {
            $('#activities-container').hide();
            $('#view-more-loader').show();
        }, function() {
            $('#view-more-loader').hide();
        });
    }

    /**************************** Start Booking and cancel btn function ********************/

    $('body').on('click', '.act-select-btn', function(e) {
        e.preventDefault();
        var data = $(this).data();
        var dataId = $(this).attr('data-button-id');
        var book = $(this).text();
        var viewData = $(this).attr('data-view');

        if (dates[0] == '0000/00/00' && book == 'BOOK ACTIVITY') {
            eroam.confirm(null, 'Would you like to add another day?', function(e) {
                addNumOfDay(data);
            }, function(e) {
                data = null;
            });
        } else {
            showDatefield(dataId, data.isSelected, data, viewData);
        }
    });


    /*
    | function to display date field
    */
    function showDatefield(id, selected, data, view) {
        if (view == 'view') {
            var input = $('#view_datepick-' + id);
        } else {
            var input = $('#datepick-' + id);
        }

        if (!selected) {
            $('#date-field-container-' + id).addClass('show');
            $(input).click();

        } else {
            $('#date-field-container-' + id).removeClass('show');

            $(input).nextAll('#act-date').text('');
            deselectThisActivity(data, input);

            if (view == 'view') {
                setTimeout(function() {
                    parent.location.reload();
                }, 2000);
            } else {
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }


        }

    }

    $('body').on('click', '.datetimepick', function() {
        dates.sort();
        var startDate = (dates.length > 0) ? dates[0] : 0;
        if (startDate == '0000/00/00' || startDate == 0) {
            startDate = dateFrom;
        }

        //$(this).datepicker();

        $(this).datetimepicker({
            //$(this).datepicker({
            onGenerate: function(ct) {
                $(this).css({
                    'background': '#000',
                    'padding': 0,
                });
                $(this).find('.xdsoft_label, .xdsoft_option ').css({
                    'background': '#000',
                    'color': '#fff'
                });
                $(this).find('.xdsoft_calendar').css({
                    'background': '#fff'
                });
                $(this).find('.xdsoft_date').toggleClass('xdsoft_disabled');
                $(this).find('.xdsoft_datepicker').css({
                    'margin-left': 0,
                    'width': '275px'
                });
                $(this).find('th').css({
                    'background': '#000',
                    'color': '#fff',
                    'border-color': '#000'
                });
                $(this).find('.xdsoft_date > div').css({
                    'padding': '7px'
                });
                var datePickerdates = $(this).find('.xdsoft_date');
                datePickerdates.each(function() {
                    if (!$(this).hasClass('xdsoft_disabled')) {
                        //$(this).toggleClass('xdsoft_current');
                        $(this).css({
                            'opacity': '1'
                        }).find('div').css({
                            'font-weight': 'bold',
                            'color': '#fff',
                            'background': '#000',
                            'opacity': '1'
                        });

                    } else {

                        //$(this).css('background', '#999')
                    }
                });
            },
            onSelectDate: function(ct, i) {
                updateOnDateSelected(ct.dateFormat('Y-m-d'), i);
            },
            onClose: function(ct, i) {
                $(i.context).blur()
            },

            closeOnDateSelect: true,
            timepicker: false,
            format: 'Y-m-d',
            disabledDates: dates,
            startDate: startDate.replace(/-/g, '/'),
            showOtherMonths: true,
            selectOtherMonths: false,
            //minDate: -20, maxDate: "+1M +10D" 
        });

        if ($(this).hasClass('focus-triggered')) {
            $(this).removeClass('focus-triggered');
            $(this).blur();
        } else {
            $(this).addClass('focus-triggered');
            $(this).focus();
        }
    });

    /*
    | Function to add number of days if the user want's to stay longer in the selected city
    */
    function addNumOfDay(data) {

        eroam.ajax('get', 'latest-search', {}, function(response) {
            if (response) {
                search_session = JSON.parse(response);

                var session = search_session.itinerary;
                if (session.length > 0) {

                    if (session[leg].city.id == cityId) {
                        var default_nights = search_session.itinerary[leg].city.default_nights;

                        search_session.itinerary[leg].city.days_to_add = 1;
                        search_session.itinerary[leg].city.add_after_date = dateFrom;

                        var act;
                        var selected_date = dateFrom;

                        if (search_session.itinerary[leg].activities != null) {
                            if (search_session.itinerary[leg].activities.length > 0) {
                                act = search_session.itinerary[leg].activities;
                                act = sortByDate(act);
                                var selected_date = act[act.length - 1].date_selected;
                                if (parseInt(act[act.length - 1].duration) > 1) {
                                    var addDuration = parseInt(act[act.length - 1].duration) - 1;
                                    selected_date = addDays(selected_date, addDuration);
                                }

                            }
                        }

                        var formattedDate = moment(addDays(selected_date, 1)).format('Do, MMMM YYYY');
                        $('#datepick-' + data.activityId).nextAll('#act-date').text(formattedDate);
                        updateOnDateSelected(addDays(selected_date, 1), $('#datepick-' + data.activityId), true);

                    }
                }
            }
        });


    }

    /*
    | this function will update the selected activities when date is selected
    */
    function updateOnDateSelected(date, i, onAdd = false) {
        if (date) {
            $(i.context).removeData('mousewheelPageHeight');
            $(i.context).removeData('mousewheelLineHeight');

            var data = onAdd ? i.data() : $(i.context).data();

            data.date_selected = date;
            data.extraNights = 0;

            var dateToStart = formatDate(data.date_selected, '/');
            var startCounting = false;
            var countNumOfDates = 0;

            for (count = 0; dates.length > count; count++) {
                if ((dateToStart == dates[count]) || startCounting) {
                    startCounting = true;
                    countNumOfDates += 1;
                } else {
                    continue;
                }
            }

            if (data.isSelected) {
                deselectThisActivity(data, $(i.context));
            } else {

                if (parseInt(data.duration) > 1 && !onAdd) {
                    if (countNumOfDates >= parseInt(data.duration)) {
                        selectThisActivity(data);
                    } else {

                        var extraNights = countNumOfDates - parseInt(data.duration);
                        extraNights = Math.abs(extraNights);
                        data.extraNights = extraNights;
                        eroam.confirm(null, 'You need ' + extraNights + ' more night(s) added to your stay for this city if you select this activity on this date. Would you like to continue?', function(e) {

                            selectThisActivity(data);
                        }, function(e) {
                            data = null;
                        });
                    }

                } else {
                    var add = false;
                    if (countNumOfDates <= 0) {
                        add = true;
                    }
                    selectThisActivity(data, add);
                }

                /*
                | Display date selected
                */
                var formattedDate = moment($(i.context).val()).format('Do, MMMM YYYY');
                //eeeeeeeeeeeeeee
                $(i.context).nextAll('#act-date').text(formattedDate);
            }
        }
    }

    /*
    | Function for adding days to a given date
    */
    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + parseInt(days));

        return formatDate(result, '-');
    }

    function sortByDate(activities) {
        var sorted = activities.sort(function(itemA, itemB) {
            var A = itemA.date_selected;
            var B = itemB.date_selected;
            return A.localeCompare(B);
        });
        return sorted;
    }

    function selectThisActivity(data, add = false) {
        if (data) {
            var map_data = [];
            var selected_date = data.date_selected;

            switch (data.provider) {

                case 'eroam':
                    var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    activityDiv.toggleClass('selected-activity');
                    activityDiv.toggleClass('listactive');

                    activityDiv.data('is-selected', true);
                    activityBtn.data('is-selected', true);
                    activityBtn.html('CANCEL ACTIVITY');
                    var dataAttr = activityDiv.data();

                    /*
                    | remove the date selected from the dates array
                    */

                    var selectedDates = [];

                    if (parseInt(data.extraNights)) {

                        var aLength = parseInt(data.extraNights) ? parseInt(data.extraNights) + 1 : parseInt(data.duration);
                        var range = _.range(1, aLength);

                        $.each(range, function(key, num) {
                            var sDate = addDays(selected_date, num).replace(/-/g, '/');
                            selectedDates.push(sDate);

                            var dateIndex = dates.indexOf(selected_date.replace(/-/g, '/'));

                            if (dateIndex > -1 && (typeof dates[dateIndex + parseInt(num)] != 'undefined')) {
                                selectedDates.push(dates[dateIndex + parseInt(num)]);
                            }
                        });
                    }

                    selectedDates.push(selected_date.replace(/-/g, '/'));

                    for (index = dates.length - 1; index >= 0; index--) {
                        var eachDate = dates[index];
                        if (typeof eachDate != 'undefined') {

                            if (selectedDates.indexOf(eachDate) > -1) {
                                dates.splice(index, 1);
                            }

                        }
                    }

                    /*
                    | This will update the session for dates available
                    */
                    if (dates.length > 0) {
                        postData = {
                            dates_available: dates,
                            id: cityId
                        };
                    } else {
                        dates.push('0000/00/00');
                        postData = {
                            dates_available: ['0000/00/00'],
                            id: cityId,
                            _token: $('meta[name="csrf-token"]').attr('content')
                        };
                    }

                    var eRoamActdata = {
                        "date_from": dateFrom,
                        "date_to": dateTo,
                        'id': dataAttr.activityId,
                        'city_id': dataAttr.cityId,
                        'name': dataAttr.name,
                        'city': {
                            'id': dataAttr.cityId
                        },
                        'activity_price': [{
                            'price': dataAttr.price,
                            'currency': {
                                'id': globalCurrency_id,
                                'code': dataAttr.currency
                            }
                        }],
                        'price': [{
                            'price': dataAttr.price,
                            'currency': {
                                'id': globalCurrency_id,
                                'code': dataAttr.currency
                            }
                        }],
                        'date_selected': selected_date,
                        'currency_id': globalCurrency_id,
                        'currency': dataAttr.currency,
                        'provider': dataAttr.provider,
                        'description': dataAttr.description,
                        'duration': dataAttr.duration
                    };

                    var extraNights = parseInt(data.extraNights) ? parseInt(data.extraNights) : 0;

                    extraNights = add ? parseInt(data.duration) : extraNights;
                    eRoamActdata.extra_nights = extraNights;

                    updateSession(search_session, eRoamActdata, true, extraNights);

                    break;

                case 'aot':

                    break;

                case 'ae':

                    break;
                case 'viator':
                    var activity_data = data;

                    var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                    activityDiv.toggleClass('selected-activity');
                    activityDiv.toggleClass('listactive');

                    activityDiv.data('is-selected', true);
                    activityBtn.data('is-selected', true);
                    activityBtn.html('CANCEL ACTIVITY');

                    data = {
                        "date_from": dateFrom,
                        "date_to": dateTo,
                        'id': activity_data.activityId,
                        'city_id': activity_data.cityId,
                        'name': activity_data.name,
                        'city': {
                            'id': activity_data.cityId
                        },
                        'activity_price': [{
                            'price': activity_data.price,
                            'currency': {
                                'id': 1,
                                'code': activity_data.currency
                            }
                        }],
                        'price': [{
                            'price': activity_data.price,
                            'currency': {
                                'id': 1,
                                'code': activity_data.currency
                            }
                        }],
                        'date_selected': selected_date,
                        'currency_id': 1,
                        'currency': activity_data.currency,
                        'provider': activity_data.provider,
                        'description': activity_data.description,
                        'images': [activity_data.images],
                        'duration': activity_data.duration,
                        'cancellation_policy': '',
                    };


                    var selectedDates = [];

                    var aLength = parseInt(data.extraNights) ? parseInt(data.extraNights) + 1 : parseInt(data.duration);
                    var range = _.range(1, aLength);

                    $.each(range, function(key, num) {

                        var sDate = addDays(selected_date, num).replace(/-/g, '/');
                        selectedDates.push(sDate);

                        var dateIndex = dates.indexOf(selected_date.replace(/-/g, '/'));

                        if (dateIndex > -1 && (typeof dates[dateIndex + parseInt(num)] != 'undefined')) {
                            selectedDates.push(dates[dateIndex + parseInt(num)]);
                        }
                    });

                    selectedDates.push(selected_date.replace(/-/g, '/'));
                    for (index = dates.length - 1; index >= 0; index--) {
                        var eachDate = dates[index];
                        if (typeof eachDate != 'undefined') {

                            if (selectedDates.indexOf(eachDate) > -1) {

                                dates.splice(index, 1);
                            }

                        }
                    }

                    /*
                    | This will update the session for dates available
                    */
                    if (dates.length > 0) {
                        postData = {
                            dates_available: dates,
                            id: cityId
                        };
                    } else {
                        dates.push('0000/00/00');
                        postData = {
                            dates_available: ['0000/00/00'],
                            id: cityId,
                            _token: $('meta[name="csrf-token"]').attr('content')
                        };
                    }

                    var extraNights = parseInt(activity_data.extraNights) ? parseInt(activity_data.extraNights) : 0;
                    extraNights = add ? parseInt(data.duration) : extraNights;
                    data.extra_nights = extraNights;
                    updateSessionViator(search_session, data, true, extraNights);

                    break;
            }
        }

    }


    function deselectThisActivity(data, input) {
        var map_data = [];
        switch (data.provider) {

            case 'eroam':
                var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                activityDiv.toggleClass('selected-activity');
                activityDiv.toggleClass('listactive');
                activityDiv.data('is-selected', false);
                activityBtn.data('is-selected', false);
                input.data('is-selected', false);
                activityBtn.html('BOOK ACTIVITY');
                map_data.id = data.activityId;
                map_data.city_id = data.cityId;

                updateSession(search_session, map_data, false)
                break;

            case 'aot':

                break;

            case 'ae':

                break;
            case 'viator':
                var activityDiv = $('.activity[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                var activityBtn = $('.act-select-btn[data-provider="' + data.provider + '"][data-activity-id="' + data.activityId + '"]');
                activityDiv.toggleClass('selected-activity');
                activityDiv.toggleClass('listactive');
                activityDiv.data('is-selected', false);
                activityBtn.data('is-selected', false);
                input.data('is-selected', false);
                activityBtn.html('BOOK ACTIVITY');
                map_data.id = data.activityId;
                map_data.city_id = data.cityId;
                updateSessionViator(search_session, map_data, false)

                break;

        }
    }

    function updateSessionViator(search_session, new_activity, isSelected, extraNights = 0) {
        var id = parseInt(new_activity.activityId);

        switch (isSelected) {
            case true:
                $.each(search_session.itinerary, function(key, itinerary) {

                    if (itinerary.city.id == new_activity.city_id /*&& itinerary.activities.length > 0*/ ) {

                        if (itinerary.activities == null) {
                            itinerary.activities = [];
                        }
                        if (extraNights) {

                            itinerary.city.default_nights = parseInt(itinerary.city.default_nights) + parseInt(extraNights);
                        }
                        itinerary.activities.push(new_activity);

                        if (itinerary.city.default_nights == 0 || itinerary.city.default_nights == null || itinerary.city.default_nights == '') {
                            itinerary.city.default_nights = itinerary.activities.length;
                        }
                        var act = itinerary.activities;
                        act = sortByDate(act);
                        itinerary.activities = act;
                    }
                });

                break;
            case false:

                $.each(search_session.itinerary, function(key, itinerary) {

                    var temp = itinerary.activities;

                    if (itinerary.city.id == new_activity.city_id && itinerary.activities.length > 0) {
                        $.each(itinerary.activities, function(k, activity) {
                            if (typeof activity != 'undefined') {

                                if (activity.id == new_activity.id) {

                                    if (activity.extra_nights) {

                                        search_session.itinerary[key].city.days_to_deduct = activity.extra_nights;
                                        search_session.itinerary[key].city.deduct_after_date = dateFrom;
                                        search_session.itinerary[key].city.default_nights = parseInt(search_session.itinerary[key].city.default_nights) - parseInt(activity.extra_nights);
                                    }

                                    var aLength = parseInt(activity.duration) - parseInt(activity.extra_nights);
                                    if (aLength) {

                                        if (dates[0] == '0000/00/00') {
                                            dates.splice(0, 1);
                                        }

                                        var range = _.range(1, aLength);
                                        var dateSelected = activity.date_selected.replace(/-/g, '/');
                                        $.each(range, function(key, num) {
                                            dates.push(addDays(dateSelected, num).replace(/-/g, '/'));
                                        });
                                        dates.push(dateSelected);
                                    }

                                    postData = {
                                        dates_available: dates,
                                        id: cityId,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    };
                                    temp.splice(k, 1);
                                }
                            }
                        });
                    }
                });

                break;
        }
        search_session._token = $('meta[name="csrf-token"]').attr('content');

        bookingSummary.update(JSON.stringify(search_session));

    }

    function updateSession(search_session, new_activity, isSelected, extraNights = 0) {

        var id = parseInt(new_activity.id);

        switch (isSelected) {
            case true:
                $.each(search_session.itinerary, function(key, itinerary) {
                    if (itinerary.city.id == new_activity.city_id /*&& itinerary.activities.length > 0*/ ) {
                        if (itinerary.activities == null) {
                            itinerary.activities = [];
                        }

                        if (extraNights) {
                            itinerary.city.default_nights = parseInt(itinerary.city.default_nights) + parseInt(extraNights);
                        }
                        itinerary.activities.push(new_activity);
                        if (itinerary.city.default_nights == 0 || itinerary.city.default_nights == null || itinerary.city.default_nights == '') {
                            itinerary.city.default_nights = itinerary.activities.length;
                        }
                        var act = itinerary.activities;
                        act = sortByDate(act);
                        itinerary.activities = act;
                    }
                });

                break;
            case false:

                $.each(search_session.itinerary, function(key, itinerary) {

                    var temp = itinerary.activities;
                    if (itinerary.city.id == new_activity.city_id && itinerary.activities.length > 0) {
                        $.each(itinerary.activities, function(k, activity) {
                            if (typeof activity != 'undefined') {
                                if (activity.id == new_activity.id) {
                                    if (activity.extra_nights) {

                                        search_session.itinerary[key].city.days_to_deduct = activity.extra_nights;
                                        search_session.itinerary[key].city.deduct_after_date = dateFrom;
                                        search_session.itinerary[key].city.default_nights = parseInt(search_session.itinerary[key].city.default_nights) - parseInt(activity.extra_nights);
                                    }


                                    var aLength = parseInt(activity.duration) - parseInt(activity.extra_nights);
                                    if (aLength) {

                                        if (dates[0] == '0000/00/00') {
                                            dates.splice(0, 1);
                                        }

                                        //var aLength = parseInt(activity.duration) ;
                                        var range = _.range(1, aLength);
                                        var dateSelected = activity.date_selected.replace(/-/g, '/');
                                        $.each(range, function(key, num) {
                                            dates.push(addDays(dateSelected, num).replace(/-/g, '/'));
                                        });
                                        dates.push(dateSelected);
                                    }



                                    postData = {
                                        dates_available: dates,
                                        id: cityId,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    };

                                    temp.splice(k, 1);
                                }
                            }
                        });
                    }
                });

                break;
        }
        search_session._token = $('meta[name="csrf-token"]').attr('content');
        bookingSummary.update(JSON.stringify(search_session));
    }

    /**************************** End Booking and cancel btn function ********************/

    // function to queue the calls to APIs e.g. eroam, viator
    function buildActivities() {
        var tasks = [
            buildEroamActivity, // call eroam 
            buildViatorActivity, // call viator next
            checkActivityCount, // show the available transport types
            hidePageLoader, // hides the page loader when all API calls are done
            @if(count($activity_pref) > 0)
            autoSortActivityByPref,
            hideAutoSortLoader
            @endif
        ];

        $.each(tasks, function(index, value) {
            $(document).queue('tasks', processTask(value));
        });
        // queue
        $(document).queue('tasks');

        $(document).dequeue('tasks');

    }

    function processTask(fn) {
        return function(next) {
            doTask(fn, next);
        }
    }

    function doTask(fn, next) {
        fn(next);
    }

    function buildEroamActivity(next) {

        eroamRQ = {
            city_ids: [cityId],
            date_from: dateFrom,
            date_to: dateTo,
            interests: travel_pref
        };
        var eroamApiCall = eroam.apiDeferred('city/activity', 'POST', eroamRQ, 'activity', true);
        eroam.apiPromiseHandler(eroamApiCall, function(eroamResponse) {

            if (eroamResponse.length > 0) {
                eroamResponse_length = eroamResponse.length;
                eroamResponse.forEach(function(eroamAct, eroamActIndex) {
                    var duration = parseInt(eroamAct.duration);
                    if (duration > 1) {
                        return;
                    }
                    eroamAct.provider = 'eroam';
                    eroamAct.index = eroamActIndex;
                    if (selectedActivities.length > 0) {
                        if (selectedActivities.indexOf('eroam_' + eroamAct.id) > -1) {
                            eroamAct.selected = true;
                            eroamAct.activity_date = selected_dates['eroam_' + eroamAct.id];
                        }
                    }
                    try {
                        appendActivities(eroamAct);
                        totalActivityCount++;
                    } catch (err) {
                        console.log(err.message);
                    }
                });
            }

            next();
        });
    }


    function buildViatorActivity(next) {
        var total_viator_activity = Math.abs(20 - eroamResponse_length);

        vRQ = {
            destId: destinationId,
            startDate: dateFrom,
            endDate: dateTo,
            currencyCode: 'AUD',
            topX: '1-' + total_viator_activity,
            provider: 'viator'
        };

        var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
        eroam.apiPromiseHandler(viatorApiCall, function(viatorResponse) {
            calculateHeight();
            if (viatorResponse != null) {
                if (viatorResponse.length > 0) {
                    viatorResponse.forEach(function(viatorAct, viatorActIndex) {

                        viatorAct.provider = 'viator';

                        var duration = countViatorActDuration(getViatorDuration(viatorAct.duration));
                        viatorAct.duration = viatorAct.duration;

                        if (selectedActivities.length > 0) {
                            if (selectedActivities.indexOf('viator_' + viatorAct.code) > -1) {
                                viatorAct.selected = true;
                                viatorAct.activity_date = selected_dates['viator_' + viatorAct.code];
                            }
                        }

                        try {
                            appendActivities(viatorAct);
                            totalActivityCount++;
                        } catch (err) {
                            console.log(err.message);
                        }

                    });
                }
            }
            next();
        });
    }

    function countViatorActDuration(duration) {

        var result = 1;
        if (duration) {

            var match = duration.match(/hour|day/);
            match = match ? match[0] : '';
            switch (match) {

                case 'hour':
                    var hour = duration.split(' ');
                    hour = parseInt(hour[0]);
                    day = moment.duration(hour, 'hours').days();
                    result = parseInt(day) == 0 ? 1 : day;
                    break;

                case 'day':
                    var day = duration.split(' ');
                    result = parseInt(day[0]);
                    break;

                default:
                    result = 1;
                    break;

            }
        }
        return result;
    }

    function getViatorDuration(duration) {
        var match = duration.match(/[\d\.]+[\s|-]hour|[\d\.]+[\s|-]minute|[\d\.]+[\s|-]day/i);
        return match ? (match[0].match(/minute/) ? '1 day' : match[0].replace('-', ' ')) : '1 day';
    }

    // function to build the html for activity-list based on api call results 
    function appendActivities(act) {
        var id;
        var name;
        var description;
        var price;
        var currency;
        var imageSrc;
        var dataAttributes;
        var viewMoreBtn; // variable to store "View more" button DOM for
        var index;
        var activityDate;
        var convertedPrice;
        var code;
        var duration;

        switch (act.provider) {
            case 'eroam':
                name = act.name;
                selected = (act.selected) ? true : false;
                btn_value = (selected) ? 'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
                class_selected = (selected) ? ' selected-activity listactive' : '';
                description = act.description.substr(0, 225) + '...';
                price = parseFloat(act.activity_price[0].price);
                currency = act.activity_price[0].currency.code;

                convertedPrice = eroam.convertCurrency(price, currency);
                imageSrc = "{{config('env.DEV_CMS_URL')}}" + ((act.image[0].medium) ? act.image[0].medium : 'uploads/activities/' + act.image[0].large);
                index = act.index;
                activityDate = (selected) ? act.activity_date : '';
                code = act.id;
                duration = act.duration;

                var label = [];
                if (act.pivot.length > 0) {
                    if (selected) {
                        label.push(parseInt(travel_pref[0]));
                    } else {
                        $.each(act.pivot, function(index, value) {
                            var labelID = parseInt(value.label_id);
                            label.push(labelID);
                        });
                    }

                }

                var sortedLabels = sortCatPriority(label);
                dataAttributes = [
                    'data-provider="' + act.provider + '" ',
                    'data-city-id="' + cityId + '" ',
                    'data-index="' + index + '" ',
                    'data-activity-id="' + act.id + '" ',
                    'data-is-selected="' + selected + '" ',
                    'data-price="' + convertedPrice + '" ',
                    'data-currency="' + globalCurrency + '" ',
                    'data-price-id="' + act.activity_price[0].id + '" ',
                    'data-name="' + act.name.replace(/"/g, '&quot;').toLowerCase() + '"',
                    'data-description="' + act.description.replace(/"/g, '&quot;') + '"',
                    'data-duration="' + act.duration + '"',
                    'data-search="1"'
                ];

                if (sortedLabels.length > 0) {
                    var dataLabel = 'data-label="' + sortedLabels[0] + '"';
                    dataAttributes.push(dataLabel);
                }
                dataAttributes = dataAttributes.join('');

                viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '" data-provider="' + act.provider + '" data-code="' + act.id + '" class="view-more-btn"><button class="btn act-btn btn-primary btn-block bold-txt act-view-more">View more</button></a>';
                id = act.id;
                break;

            case 'viator':

                name = act.title;
                selected = (act.selected) ? true : false;
                btn_value = (selected) ? 'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
                class_selected = (selected) ? ' selected-activity listactive' : '';
                description = stripTags(act.shortDescription.substr(0, 225) + '...');
                price = parseFloat(markUpPrice(act.price, 30));
                currency = act.currencyCode;
                convertedPrice = eroam.convertCurrency(price, currency);
                imageSrc = act.thumbnailHiResURL;
                index = act.code;
                id = act.code;
                activityDate = (selected) ? act.activity_date : '';
                code = act.code;
                duration = act.duration;

                //console.log('as', act);
                var sortedLabels = [];

                @if(session() -> has('search_input') && count(session() -> get('search_input')['interests']) > 0)
					if (selected) {
						sortedLabels.push(parseInt(travel_pref[0]));
					} else {
						sortedLabels = sortCatPriority(act.labels);
					}
                @endif
                dataAttributes = [
                    'data-provider="' + act.provider + '" ',
                    'data-city-id="' + cityId + '" ',
                    'data-index="' + index + '" ',
                    'data-activity-id="' + act.code + '" ',
                    'data-is-selected="' + selected + '" ',
                    'data-price="' + convertedPrice + '" ',
                    'data-name="' + act.title.replace(/"/g, '&quot;').toLowerCase() + '"',
                    'data-currency="' + globalCurrency + '"',
                    'data-startDate="' + dateFrom + '"',
                    'data-endDate="' + dateTo + '"',
                    'data-description="' + stripTags(act.shortDescription.replace(/"/g, '&quot;')) + '"',
                    'data-images="' + imageSrc + '"',
                    'data-rating="' + act.rating + '" ',
                    'data-duration="' + act.duration + '"',
                    'data-search="1"'

                ];
                if (sortedLabels.length > 0) {
                    var dataLabel = 'data-label="' + sortedLabels[0] + '"';
                    dataAttributes.push(dataLabel);
                }
                dataAttributes = dataAttributes.join('');

                viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + act.code + '" class="view-more-btn btn  btns_input_blue transform d-block w-100 act-view-more">View more</a>';
                id = act.code;

                break;

            case 'ae':

                name = act.ExcursionName;
                selected = (act.selected) ? true : false;
                btn_value = (selected) ? 'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
                class_selected = (selected) ? ' selected-activity listactive' : '';
                description = stripTags(act.Description.substr(0, 225) + '...');
                price = parseFloat(markUpPrice(parseFloat(act.Prices[0].Price), 30));
                currency = act.Prices[0].Currency;
                convertedPrice = eroam.convertCurrency(price, currency);
                imageSrc = act.Images[0];
                index = act.ExcursionId;
                id = act.ExcursionId;
                activityDate = (selected) ? act.activity_date : '';
                code = act.code;
                duration = act.duration;

                dataAttributes = [
                    'data-provider="' + act.provider + '" ',
                    'data-city-id="' + cityId + '" ',
                    'data-index="' + index + '" ',
                    'data-activity-id="' + id + '" ',
                    'data-is-selected="' + selected + '" ',
                    'data-price="' + convertedPrice + '" ',
                    'data-name="' + name.toLowerCase() + '"',
                    'data-currency="' + globalCurrency + '"',
                    'data-startDate="' + dateFrom + '"',
                    'data-endDate="' + dateTo + '"',
                    'data-search="1"',
                    'data-description="' + stripTags(act.Description.substr(0, 225) + '...') + '"'
                ].join('');

                viewMoreBtn = '<a href="#"  data-select ="' + selected + '" data-cityId ="' + cityId + '"  data-provider="' + act.provider + '" data-code="' + act.code + '" class="view-more-btn"><button class="btn act-btn btn-primary btn-block  bold-txt act-view-more">View more</button></a>';
                id = act.code;

                break;

            case 'own_arrangement':

                break;

            case 'aot':

                break;

        }
        // var convertedPricePerPerson = Math.ceil( convertedPrice / travellers );
        var priceString = globalCurrency + ' ' + Math.ceil(convertedPrice).toFixed(2);
        roundedUpConvertedPrice = Math.ceil(convertedPrice).toFixed(2);

        maxPrice = parseInt(roundedUpConvertedPrice) > maxPrice ? parseInt(roundedUpConvertedPrice) : maxPrice;

        html = [
            '<div class="activities_list activity pt-4 pb-4 pr-3 pl-3 mb-4' + class_selected + '" ' + dataAttributes + '>',
            '<div class="border-bottom mb-3">',
            '<div class="row">',
            '<div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">',
            '<h4>' + name + '</h4>',
            '<p><svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">',
            '<desc>Created with Sketch.</desc>',
            '<defs></defs>',
            '<g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">',
            '<g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">',
            '<g id="Group" transform="translate(501.000000, 542.000000)">',
            '<g id="Group-16">',
            '<path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>',
            '</g>',
            '</g>',
            '</g>',
            '</g>',
            '</svg>',
            'Tour Code: ' + code + '   |   ',
            '&nbsp;Location: ' + cityName +
            '</p></div>',
            '<div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">',
            '<div class="text-right">',
            '<p>From <strong>' + priceString + '</strong> Per Person</p>',
            '<p>Duration : ' + duration + '</p>',
            '</div>',
            '</div>',
            '</div>',
            '</div>',
            '<div class="row">',
            '<div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">',
            '<img src="' + imageSrc + '" alt="" class="img-responsive" style="height:200px"/>',
            '</div>',
            '<div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">',
            '<p class="m-t-10">' + description + '</p>',
            '<div class="row mt-5 pt-68">',
            '<div class="col-6">',
            '<input type="text" ' + dataAttributes + '  class="datetimepick" id="datepick-' + id + '" value="">',
            '<a href="#" class="btn  btns_input_blue transform d-block w-100 act-btn act-select-btn" ' + dataAttributes + ' style="margin-bottom:0px;" data-button-id="' + id + '">' + btn_value + '</a>',
            '</div>',
            '<div class="col-6">' + viewMoreBtn + '</div>',
            '</div>',
            '</div>',
            '</div>',
            '</div>'
        ].join('');

        html += '<h4 class="text-center blue-txt bold-txt no_activities" style="display:none;">No Activities Found.</h4>';

        if (class_selected) {
            $('.activity-list-selected').prepend(html);
        } else {
            $('.activity-list').append(html);
        }
        hide_input();
    }

    function checkActivityCount(next) {
        $('.act-count').html(': Top ' + totalActivityCount + ((totalActivityCount == 1) ? ' Activity' : ' Activities') + ' Found');
        if (totalActivityCount == 0) {
            var html = '<h4 class="text-center blue-txt bold-txt">No Activities Found.</h4>';
            $('.activity-list').append(html);
        }
        next();
    }

    function hidePageLoader(next) {
        $('#activity-loader').fadeOut(300);
        next();
    }

    function hideAutoSortLoader(next) {
        setTimeout(function() {
            $('#auto-sort-loader').fadeOut(300);
        }, 1000);

        next();
    }

    /*
    | Function to sort the activties by preference
    */

    function autoSortActivityByPref(next) {
        $('#auto-sort-loader').fadeIn('slow');
        var activityList = $('.activity-wrapper');

        var sortedActivities = activityList.sort(function(a, b) {
            var A = $(a).find('div.activity').attr('data-label');
            var B = $(b).find('div.activity').attr('data-label');

            var itemA = $.inArray(parseInt(A), travel_pref);
            var itemB = $.inArray(parseInt(B), travel_pref);

            itemA = itemA == -1 ? 100 : itemA;
            itemB = itemB == -1 ? 100 : itemB;

            return itemA - itemB;
        });
        $('#activity-list').html(sortedActivities);
        next();
    }


    /**************************** Start searching function ********************/

    $('#search_val').keyup(function() {
        if ($(this).val() == '') {
            var list = $("div.activity-list div.activity");
            $(list).fadeOut("fast");
            var totalCount = 0;
            search_val = '1';
            $("div.activity-list").find("div.activity[data-search*='" + search_val + "']").each(function(i) {
                totalCount++;
                $(this).delay(200).slideDown("fast");
            });

            checkActivityCountForSearch(totalCount);
        }
    });

    function getActivitySearch() {
        var search_val = $('#search_val').val();
        if (search_val) {
            search_val = search_val.toLowerCase();
            var list = $("div.activity-list div.activity");
            $(list).fadeOut("fast");
            var totalCount = 0;
            $("div.activity-list").find("div.activity[data-name*='" + search_val + "']").each(function(i) {
                totalCount++;
                $(this).delay(200).slideDown("fast");
            });
            checkActivityCountForSearch(totalCount);
        } else {
            var list = $("div.activity-list div.activity");
            $(list).fadeOut("fast");
            var totalCount = 0;
            search_val = '1';
            $("div.activity-list").find("div.activity[data-search*='" + search_val + "']").each(function(i) {
                totalCount++;
                $(this).delay(200).slideDown("fast");
            });
            checkActivityCountForSearch(totalCount);
        }
    }

    function checkActivityCountForSearch(totalActivityCount) {
        $('div.activity-list').find('.noHotelFound').closest('h4').remove();
        $('.act-count').html(': Top ' + totalActivityCount + ((totalActivityCount == 1) ? ' Activity' : ' Activities') + ' Found');
        if (totalActivityCount == 0) {
            var html = '<h4 class="text-center blue-txt bold-txt no_activities">No Activities Found.</h4>';
            $('.activity-list h4.no_activities').fadeIn();
        } else {
            $('.activity-list h4.no_activities').fadeOut();
        }
    }
    /**************************** end searching function ********************/


    /**************************** start sorting function ********************/
    $(document).on('click', '#priceSortAsc', function() { //alert(1);
        sortMeBy('data-price', 'div.activity-list', 'div.activity', 'asc');
        $('#priceSortAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
        $('#priceSortAsc').attr('id', 'priceSortDesc');

    });
    $(document).on('click', '#priceSortDesc', function() {
        $('#priceSortDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
        sortMeBy('data-price', 'div.activity-list', 'div.activity', 'desc');
        $('#priceSortDesc').attr('id', 'priceSortAsc');
    });
    $(document).on('click', '#ratingAsc', function() { //alert(1);
        sortMeBy('data-rating', 'div.activity-list', 'div.activity', 'asc');
        $('#ratingAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
        $('#ratingAsc').attr('id', 'ratingDesc');
    });
    $(document).on('click', '#ratingDesc', function() { //alert(2);
        $('#ratingDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
        sortMeBy('data-rating', 'div.activity-list', 'div.activity', 'desc');
        $('#ratingDesc').attr('id', 'ratingAsc');
    });

    function sortMeBy(arg, sel, elem, order) {
        var $selector = $(sel),
            $element = $selector.children(elem);

        $element.sort(function(a, b) {
            var an = parseInt(a.getAttribute(arg)),
                bn = parseInt(b.getAttribute(arg));

            if (order == 'asc') {
                if (an > bn)
                    return 1;
                if (an < bn)
                    return -1;
            } else if (order == 'desc') {
                if (an < bn)
                    return 1;
                if (an > bn)
                    return -1;
            }
            return 0;
        });

        $element.detach().appendTo($selector);
    }
    /**************************** end sorting function ********************/


    /*
    | Added separator parameter
    */
    function formatDate(date, separator = '-') {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator);
    }

    function calculateHeight() {
        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if (winHeight < oheight) {
            $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else {
            $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
    }

    function sortCatPriority(labelIds) {
        var labelId = labelIds.sort(function(a, b) {
            var itemA = $.inArray(a, travel_pref);
            var itemB = $.inArray(b, travel_pref);
            itemA = itemA == -1 ? 100 : itemA;
            itemB = itemB == -1 ? 100 : itemB;
            if (itemB != 100 && itemA != 100) {
                return (itemA < itemB) ? -1 : (itemA > itemB) ? 1 : 0;
            }
        });
        return labelId;
    }

    function stripTags(str) {
        return str.replace(/<(?:.|\n)*?>/gm, '');
    }

</script>
