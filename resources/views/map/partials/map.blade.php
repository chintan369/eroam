<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="map-main-container">
    	@if ( session()->get( 'map_data' )['type'] != 'auto' )
    		<span id="auto-sort" class="text-center map_auto_sort"><i style="cursor: pointer;" class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="If enabled, selected locations will be sorted based on the shortest route when adding a new location."></i> Auto Sort <a href="#"><i class="{{ (!empty(session()->get( 'map_data' )['auto_sort']) && session()->get( 'map_data' )['auto_sort'] == 'on') ? 'fa fa-toggle-on fa-2x' : 'fa fa-toggle-off fa-2x' }}"></i></a></span>
    	@endif
    	<div id="map" style="height: 1000px;"></div>
        <div class="mapWrapper map-loading">
        	<div id="map-loader">
				<div class="loader-block">
			      	<img src="{{ url( 'images/launch.png' ) }}" alt="" />
			      	<h3 id="itenary_message_id"></h3>
			      	<p class="loader-msg"><i class="fa fa-circle-o-notch fa-spin"></i> <span>Your itinerary will be displayed shortly…</span></p>
		    	</div>
			</div>
			<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
			<input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
			<div id="edit-map-overlay"></div>
        </div>
    </div>
</div>
