<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use App\Libraries\ApiCache;
use App\Libraries\EroamSession;
use \App\Helpers\FunctionsHelper;

class Controller extends BaseController
{
	use AuthorizesRequests,  DispatchesJobs, ValidatesRequests;

	public function __construct() {
		// SET CURRENCIES SESSIONS
		$eroam_session = new EroamSession();
		$eroam_session->set_all_currencies();
		
	}

	public function get_cache_instance(){
		return new ApiCache();
	}


}
