@extends( 'layouts.home' )
@section('content')

<div class="itinerary_block">
    <div class="container-fluid">
        <div class="row mt-4 tour-details">
            <div class="col-xl-10 offset-xl-1">
                <div class="loader1"><img src="{{ url( 'images/loader.gif' ) }}" alt="Loader" /></div>
                <div class="card panel p-3 mb-2">
                    <div class="row">
                        <div class="col-md-7 col-xl-8">
                            <h5 class="font-weight-bold">{{ $data['detail']['tour_title'] }}</h5>
                            <div class="media">
                                <i class="ic-tour fa-lg mr-2"></i>
                                <div class="media-body pb-3 mb-0">
                                    Tour Code: {{ $data['detail']['tour_code']}} | Countries: {{$allCountry}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-xl-4">
                            <div class="row">
                              <div class="col-sm-8 col-xl-9 col-8 text-md-right border-right">
                                    @if($data['detail']['flightPrice'] > 0)
                                        From <strong>${{ $default_currency.' '.number_format($data['detail']['price'] + $data['detail']['flightPrice'] ,2)}}</strong> P.P (Inclusive Flights)<br>
                                    @else
                                        From <strong>${{ $default_currency.' '.number_format($data['detail']['price'],2)}}</strong> P.P (Land Only)<br>
                                    @endif

                                    @if($data['detail']['discount'] > 0)                
                                        <span>eRoam Discount {{$data['detail']['discount']}}%</span><br>
                                    @endif

                                    @if($data['detail']['saving_per_person'] > 0)
                                        <span>Savings From<strong> ${{$default_currency}} {{number_format($data['detail']['saving_per_person'],2)}}</strong> P.P</span>
                                    @endif
                              </div>
                              <div class="col-sm-4 col-xl-3 col-4 text-center">
                                <h2 class="font-weight-bold mb-0">{{$data['detail']['no_of_days']}}</h2>
                                {{@$total_duration}}
                              </div>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="accomodation-details mb-5 lh-condensed">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-xl-2 col-sm-4 col-5">
                                        <span class="font-weight-bold">Countries:</span>
                                    </div>
                                    <div class="col-xl-10 col-sm-8 col-7">
                                        <span>{{substr($allCountry, 0,-2)}}</span>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-xl-2 col-sm-4 col-5">
                                        <span class="font-weight-bold">Route:</span>
                                    </div>
                                    <div class="col-xl-10 col-sm-8 col-7">
                                        <span>{{ $data['detail']['departure'].' - '.$data['detail']['destination'] }}</span>
                                    </div>
                                </div>
                                @if(!empty($data['detail']['tripActivities']))
                                    <div class="row mt-2">
                                        <div class="col-xl-2 col-sm-4 col-5">
                                          <span class="font-weight-bold">Tour Theme:</span>
                                        </div>
                                        <div class="col-xl-10 col-sm-8 col-7">
                                          <span>{{ $tripActivities}}</span>
                                        </div>
                                    </div>                        
                                @endif
                                <div class="row mt-2">
                                    <div class="col-xl-2 col-sm-4 col-5">
                                      <span class="font-weight-bold">Accommodation:</span>
                                    </div>
                                    <div class="col-xl-10 col-sm-8 col-7">
                                      <span>{{ $data['detail']['accommodation']}}</span>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-xl-2 col-sm-4 col-5">
                                      <span class="font-weight-bold">Transport:</span>
                                    </div>
                                    <div class="col-xl-10 col-sm-8 col-7">
                                      <span>{{ $data['detail']['transport']}}</span>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-4">
                            <div class="row mt-2">
                                <div class="col-xl-3 col-sm-4 col-5">
                                    <span class="font-weight-bold">Group Size:</span>
                                </div>
                                <div class="col-xl-9 col-sm-8 col-7">
                                    <span>Min {{ $data['detail']['groupsize_min']}} @if($data['detail']['groupsize_max'] != 0) - Max {{ $data['detail']['groupsize_max']}} @endif</span>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xl-3 col-sm-4 col-5">
                                    <span class="font-weight-bold">Children:</span>
                                </div>
                                <div class="col-xl-9 col-sm-8 col-7">
                                    <span>{{ ($data['detail']['is_childAllowed'] == 1 ? "Yes":"No")}}</span>
                                </div>
                            </div>

                            @if($data['detail']['children_age'] != '' && $data['detail']['is_childAllowed'] == 1)
                            <div class="row mt-2">
                                <div class="col-xl-3 col-sm-4 col-5">
                                    <span class="font-weight-bold">Children Age: </span>
                                </div>
                                <div class="col-xl-9 col-sm-8 col-7">
                                    <span>{{ $data['detail']['children_age']}}</span>
                                </div>
                            </div>
                            @endif
                        </div>
                        </div>

                        @if($data['images'])
                            <div class="jcarousel-wrapper">
                                <div class="jcarousel">  
                                    <ul class="accomodationImg-list">
                                        @php
                                            $i=0;
                                            foreach($data['images'] as $key => $image){
                                                $i++;
                                                $image['image_thumb'] = str_replace("thumbnail","small",$image['image_thumb']);
                                                if($image['image_thumb'] != '' && $image['image_small'] != ''){
                                                   $imageUrl2 = "http://dev.cms.eroam.com/".$image['image_thumb'];
                                                }else{
                                                   $imageUrl2 = "http://www.adventuretravel.com.au/".$data['detail']['folder_path']."245x169/".$image['image_thumb'];
                                                }
                                                echo '<li><img src="'.$imageUrl2.'" alt="Image '.$i.'"></li>';
                                            }
                                        @endphp                              
                                    </ul>
                                </div>
                                <a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true"><i class="fas fa-angle-left"></i></a>
                                <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"><i class="fas fa-angle-right"></i></a>
                                <p class="jcarousel-pagination"></p>                 
                            </div>
                        @endif
                    </div>

                    <ul class="nav nav-pills nav-fill">
                      <li class="nav-item mb-3 mb-md-0 pl-md-0 pl-sm-2">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">VIEW ITINERARY</a>
                      </li>
                      <li class="nav-item mb-3 mb-md-0">
                        <a class="nav-link" data-toggle="modal" data-target="#enquiry-modal">MAKE AN ENQUIRY</a>
                      </li>
                      <li class="nav-item mb-3 mb-md-0 pr-md-0 pr-sm-2">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">DATES &amp; RATES</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="mt-4">
                                <h4>Overview</h4>
                                <p>{!! $data['detail']['long_description'] !!}</p>
                                <h4 class="mt-4">Tour Itinerary</h4>
                                {!! $data['detail']['xml_itinerary'] !!}
                            </div>
                        </div>
                        
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            
                            @if(!empty($viatorDates))                    
                                <form action="<?php echo url('tourDetail/'.$data["detail"]["tour_id"].'/'.$data["detail"]["tour_url"].''); ?>" method="post">
                               {{ csrf_field() }}
                            @endif 
                            <div class="row mt-5">
                                <div class="col-sm-3">
                                    <button type="submit" id="with_flights" name="" class="btn btn-block btn-dark text-left active">
                                    <i class="ic-with-flight fa-lg"></i>
                                    <span>With Flights</span>
                                    </button>
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" id="without_flights" name="" class="btn btn-block btn-light text-left">
                                    <i class="ic-without-flight fa-lg"></i>
                                    <span>Without Flights</span>
                                    </button>
                                </div>                            
                                <div class="col-sm-3">
                                    <div class="fildes_outer">
                                        <label>Jump to a Departure Month</label>
                                        <div class="input-group ">
                                            <input id="start_date5" type="text" autocomplete="off" placeholder="Select Month" class="form-control" name="start_date5" value="{{@$start_date}}">
                                            <span class="input-group-addon px-2 py-2"><i class="ic-calendar"></i></span>
                                        </div>                                            
                                    </div>
                                    <label class="red-text text-danger" id="red-text" style="display:none;">Please select month</label>
                                </div>

                                <div class="col-sm-3">              
                                    @if(!empty($viatorDates))
                                        <button type="submit" name="" class="btn btn-white btn-block mt-2 mt-sm-0">EXPLORE FURTHER</button>
                                    </form>
                                    @else
                                        <button type="button" name="" class="btn btn-white btn-block mt-2 mt-sm-0 update_date_search">EXPLORE FURTHER</button>
                                    @endif 
                                </div>
                                
                            </div>
                            <div class="px-2 d-none d-md-block">
                                <div class="row mt-4">
                                    <div class="col-md-3 col-sm-6">
                                      DEPARTING
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                      FINISHING
                                    </div>
                                    <div class="col-md-5 col-sm-9">
                                      TOTAL FROM $AUD
                                    </div>
                                    <div class="col-md-1 col-sm-3"></div>
                                </div>
                            </div>
                            <div class="accordion mt-2 dateRates-section" id="accordionExample">

                                <?php 

                                    $i=0; $totalCost = 0; $singalCost = 0;

                                    $startDate      = '';
                                    $finishDate     = '';
                                    $flight_var     = 'flightNoCls'; 
                                    $expanded       = 'false';
                                    $price_adult    = '';
                                    $price_child    = '';
                                    $price_infant   = '';
                                    if(!empty($getAllDates))
                                    {
                                        $data['dates']  = $getAllDates; 
                                        $from           = "api";
                                    }
                                    else if(!empty($viatorDates))
                                    {
                                        $data['dates']  = $viatorDates;
                                        $from           = "viator";
                                    }  
                                    else
                                    {
                                        $from           = "";
                                    }                              

                                    if(!empty($data['dates']))
                                    {
                                        foreach ($data['dates'] as $dates) 
                                        {
                                            $i++;
                                            $flag_var       = 'collapsed';
                                            $collapsed      = '';
                                            if($i == 1) { 
                                                $flag_var   = ''; 
                                                $expanded   = 'true';
                                                $collapsed  = "show";
                                            }
                                            if(!empty($getAllDates))
                                            {
                                                $start_date     = date('F-Y',strtotime($dates['start_date']));
                                                $price          = number_format($dates['lowest_pp2a_prices'][1]['amount'],2); 
                                                $price_single   = $dates['lowest_pp2a_prices'][1]['amount']; 
                                                $price_total    = $dates['lowest_pp2a_prices'][1]['amount'];
                                                $date1Timestamp = strtotime($dates['start_date']);
                                                $date2Timestamp = strtotime($dates['finish_date']);
                                                //Calculate the difference.
                                                $difference     = $date2Timestamp - $date1Timestamp;
                                                $start_dates    = date('F-Y',strtotime($dates['start_date']));
                                                $singlePrice    = $price;
                                                $finish_date    = $dates['finish_date'];
                                                $price_inp      = $dates['lowest_pp2a_prices'][1]['amount'];
                                                $supplement     = '';
                                            }
                                            else if(!empty($viatorDates))
                                            {
                                                $start_date     = date('Y-m-d',strtotime($dates['bookingDate']));
                                                $no_of_days     = $data['detail']['no_of_days'];
                                                $finish_date    = date('Y-m-d', strtotime($dates['bookingDate']. ' + '.$no_of_days.' day'));
                                                $price          = $dates['tourGrades'][0]['pricingMatrix'][0]['ageBandPrices'];
                                                $price_adult    = number_format($price[0]['prices'][0]['price'],2); 
                                                $price_child    = number_format($price[1]['prices'][0]['price'],2); 
                                                $price_infant   = number_format($price[2]['prices'][0]['price'],2);                                     
                                                $price_single   = $price_adult;
                                                $price_total    = $price_adult;
                                                $date1Timestamp = strtotime($start_date);
                                                $date2Timestamp = strtotime($finish_date);
                                                //Calculate the difference.
                                                $difference     = $date2Timestamp - $date1Timestamp;
                                                $start_dates    = date('F-Y',strtotime($dates['bookingDate']));
                                                $singlePrice    = $price_adult;
                                                $price          = $price_adult;
                                                $price_inp      = $price_adult;

                                                $supplement     = '';
                                            }
                                            else
                                            {
                                                $singlePrice    = $dates['singlePrice'];
                                                $price          = $dates['total'];
                                                if($i == 1){ 
                                                    $totalCost  = $dates['total']; 
                                                    $singalCost = $dates['singlePrice']; 
                                                    $startDate  = date('Y-m-d H:i:s', strtotime($data['dates'][0]['startDate']));
                                                    $finishDate = date('Y-m-d H:i:s', strtotime($data['dates'][0]['finishDate']));
                                                    
                                                }                                         
                                                      
                                                $start_dates     = date('F-Y',strtotime($dates['startDate']));
                                                $start_date     = $dates['startDate'];
                                                $finish_date    = $dates['finishDate'];
                                                $price_inp      = $dates['total'];
                                                $price          = number_format($dates['total'],2); 
                                                $price_single   = $dates['singlePrice']; 
                                                $price_total    = $dates['total'];
                                               
                                                $price_adult    = number_format($dates['total'],2);
                                                $supplement     = number_format($dates['supplement'],2);
                                                if(!empty($season)){
                                                    if(array_key_exists($dates['season_id'],$season)){
                                                        $price = number_format($dates['total'] + $season[$dates['season_id']],2);
                                                        $totalCost  = $dates['total'] + $season[$dates['season_id']]; 
                                                        $singalCost = $dates['singlePrice'] + $season[$dates['season_id']];
                                                        $price_single =$dates['singlePrice'] + $season[$dates['season_id']];
                                                        $price_total = $dates['total'] + $season[$dates['season_id']];  
                                                        $flight_var     = 'flightYesCls'; 
                                                    }
                                                }
                                            }

                                            if(isset($difference))
                                            {
                                                $no_of_days         = floor($difference / (60*60*24) );
                                                $total_duration     = "Day";
                                                if($no_of_days > 1)
                                                {
                                                    $total_duration = "Days";
                                                }
                                            }

                                            ?>  
                                           
                                            <div class="panel-default card panel border-0 mb-3 {{$flight_var}} {{$start_dates}}">
                                                 <form action="{{url('signin-guest-checkout-tour')}}" method="post">
                                                {{ csrf_field() }}
                                                <div class="border-bottom accordion-title" id="heading{{$i}}">
                                                  <div class="px-2 py-3 mb-0 row {{$flag_var}}" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="{{$expanded}}" aria-controls="collapse{{$i}}">
                                                    <div class="col-md-3 col-sm-6 col-6">
                                                     <span class="d-md-none d-block font-weight-bold">DEPARTING</span>
                                                     <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($start_date))}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-6">
                                                        <span class="d-md-none d-sm-block font-weight-bold">FINISHING</span>
                                                        <p class="mb-2 mb-md-0">{{ date('l d F Y',strtotime($finish_date))}}</p>
                                                    </div>

                                                    <div class="col-md-5 col-sm-10 col-9">
                                                        @if($from == "viator")
                                                            <span class="d-md-none d-sm-block font-weight-bold">TOTAL FROM ${{ $default_currency}} </span>
                                                            <p class="mb-2 mb-md-0 withOutFlights flightPriceCls">${{ $default_currency}} {{$price_adult}} Per Person (Land Only)</p>
                                                                                          
                                                            <?php /*${{ $default_currency}} <span class="adult-price innerWithOutFlights"><strong>{{ $price_child}} </strong><small style="font-size: 14px;">(Child Price)</small></span><br>
                                                            
                                                            ${{ $default_currency}} <span class="adult-price innerWithOutFlights"><strong>{{ $price_infant}} </strong><small style="font-size: 14px;">(Infant Price)</small></span>

                                                            <span class="transport-price innerWithFlights"><strong>{{$price_adult}} </strong><small style="font-size: 14px;">(Inclusive Flights)</small></span>
                                                            Per Person */?>
                                                        @else

                                                            <span class="d-md-none d-sm-block font-weight-bold">TOTAL FROM ${{ $default_currency}} </span>
                                                            <p class="mb-2 mb-md-0 withOutFlights flightPriceCls">${{ $default_currency}} {{$price_adult}} Per Person (Land Only)</p>
                                                            <span class="withFlights"><p class="mb-2 mb-md-0">${{ $default_currency}}  {{ $price }} Per Person (Inclusive Flights)</p></span>

                                                            <span class="<?php echo $start_date;?>-withOutFlights" style="display:none;"><p class="mb-2 mb-md-0">{{number_format($price_total,2)}} (Land Only)</p></span>

                                                            <span class="<?php echo $start_date;?>-withFlights" style="display:none;">{{$price}} (Inclusive Flights)</span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-1 col-sm-2 col-3 text-right">
                                                      <a role="button" id="collapsedBtn{{$i}}" class="collapsedBtn fa-lg"><i class="fa fa-minus-circle"></i></a>
                                                    </div>
                                                  </div>
                                                </div>


                                                <div id="collapse{{$i}}" class="collapse {{$collapsed}}" aria-labelledby="heading{{$i}}" data-parent="#accordionExample">
                                                  <div class="px-2 py-3">
                                                    <?php /*<h5>{{ $dates['departureName'] }}</h5> */ ?>
                                                    <h5>{{ $data['detail']['tour_title'] }}</h5>
                                                    <div class="row">
                                                      <div class="col-md-4">
                                                        <div class="media tour-facility mt-2">
                                                            <img src="{{url('images/tour.svg')}}" alt="" class="mr-3">
                                                            <div class="media-body pb-3 mb-0">
                                                                <p class="mb-2"><span>Tour Code: </span> {{ $data['detail']['code']}}</p>
                                                                <p class="mb-2"><span>Countries:</span> {{$allCountry}}</p>
                                                                <p class="mb-2"><span>Duration:</span> {{$data['detail']['no_of_days']}} {{$total_duration}}</p>
                                                                <p class="mb-2"><span>Start:</span> {{$data['detail']['departure']}}, {{$data['detail']['departure_country']}}</p>
                                                                <p class="mb-2"><span>Finish:</span> {{$data['detail']['destination']}}, {{$data['detail']['destination_country']}}</p>

                                                                <?php 
                                                                $Flight = ''; 
                                                                if(!empty($t_return)){

                                                                    if(array_key_exists($dates['season_id'],$t_return)){

                                                                        $default_selected_city = session()->get( 'default_selected_city' );

                                                                            $city_name = 'MEL';
                                                                        if($default_selected_city == 7){
                                                                            $city_name = 'SYD';
                                                                        }elseif($default_selected_city == 15){
                                                                            $city_name = 'BNE';
                                                                        }elseif($default_selected_city == 30){
                                                                            $city_name = 'MEL';
                                                                        }
                                                                        $Flight = $city_name;  

                                                                    }
                                                                }
                                                                if(!empty($Flight)){

                                                                ?>
                                                                    <p class="mb-2 return_flights_from"><span>Flight:</span> Return Flights From ({{$Flight}})</p>              
                                                                <?php
                                                                    $Flight = '';
                                                                } 

                                                                ?>                                  
                                                            </div>
                                                        </div>
                                                      </div>

                                                      <div class="col-md-8 text-md-right">
                                                        
                                                        @if(!empty($viatorDates))
                                                            <div>${{ $default_currency}} <span class="adult-price innerWithOutFlights"><strong>{{ $price_adult}} </strong><small style="font-size: 14px;">(Adult Price)</small></span><br>
                                                                    
                                                                    ${{ $default_currency}} <span class="adult-price innerWithOutFlights"><strong>{{ $price_child}} </strong><small style="font-size: 14px;">(Child Price)</small></span><br>
                                                                    
                                                                    ${{ $default_currency}} <span class="adult-price innerWithOutFlights"><strong>{{ $price_infant}} </strong><small style="font-size: 14px;">(Infant Price)</small></span>

                                                                    <span class="transport-price innerWithFlights"><strong>{{$price_adult}} </strong><small style="font-size: 14px;">(Inclusive Flights)</small></span></div>
                                                                    Per Person
                                                        @else
                                                        ${{ $default_currency}} <span class="font-weight-bold transport-price innerWithOutFlights">{{ number_format($price_total,2)}} </span><span class="innerWithOutFlightsText" >(Land Only)</span>

                                                        <span class="font-weight-bold transport-price innerWithFlights">{{$price}} </span><span class="innerWithFlightsText">(Inclusive Flights)</span>

                                                        <p>Per Person in a Twin Share Room</p>
                                                        <p class="mb-0"><strong>Want your own single Room?</strong></p>
                                                        <em>From an extra</em> ${{ $default_currency.' '.$supplement}}
                                                        @endif
                                                      </div>
                                                    </div>

                                                    <div class="row">
                                                      <div class="col-md-8">
                                                        <small class="d-block mt-4 mb-3 mb-md-0">All prices are inclusive of all relevant Government taxes & GST and are per person in Australian dollars.</small>
                                                      </div>
                                                      <div class="col-md-4">
                                                        <input type="hidden"  name="singlePrice" id="singlePrice{{$i}}" value="{{$singlePrice}}" >
                                                        
                                                        <input type="hidden" class="dateRedio" name="price" id="radio-d{{$i}}" value="{{$price_inp}}" >

                                                        <input type="hidden"  name="singlePrice" id="singlePrice{{$i}}two" value="{{$price_single}}" >

                                                        <input type="hidden" name="childPrice" value="{{$price_child}}">

                                                        <input type="hidden" name="infantPrice" value="{{$price_infant}}">

                                                        <input type="hidden" class="dateRedio" name="date" id="radio-d{{$i}}two" value="{{$price_total}}" >

                                                        <input type="hidden" class="dateRedio" name="index"  value="{{$i}}" >

                                                        <input type="hidden" name="tour_name" value="{{ $data['detail']['tour_title'] }}"> 
                                                        <input type="hidden" name="tour_id" value="{{ $data['detail']['tour_id'] }}">
                                                        <input type="hidden" name="start_date" value="{{$start_date}}"> 
                                                        <input type="hidden" name="no_of_days" value="{{$data['detail']['no_of_days']}}">
                                                        <input type="hidden" name="durationType" value="{{$data['detail']['durationType']}}">
                                                        <input type="hidden" id="startDate{{$i}}" value="{{ $start_date }}">
                                                        <input type="hidden" id="finishDate{{$i}}" value="{{ $finish_date }}" name="finishDate" >
                                                        <input type="hidden" name="code" value="{{ $data['detail']['code'] }}">
                                                        <input type="hidden" name="provider" value="{{ $data['detail']['provider'] }}">
                                                        <input type="hidden" name="from" value="{{$from}}">
                                                        <button type="submit" name="" class="btn btn-white btn-block">BOOK TOUR</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                </form>
                                            </div>
                                            
                                        @php
                                        } // end foreach ($data['dates'] as $dates) 
                                    }
                                    else
                                    {
                                         echo "<div><center>No records found.</center></div>";
                                    }   
                                    @endphp
                                    <div id="no_records" style="display:none;"><center>No departure date found.</center></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>

    <div class="modal fade bd-example-modal-lg" id="enquiry-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Make Enquiry</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="myForm" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div id="alert alert-success text-center" style="dispaly:none;"></div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Product</label>
                                <input type="text" class="form-control" id="product" name="product" value="{{ $data['detail']['tour_title'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Tour Code</label>
                                <input type="text" class="form-control"id="product1" name="product1" value="{{ $data['detail']['tour_code'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Family Name</label>
                                <input type="text" class="form-control" id="family_name" name="family_name" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Email Address</label>
                                <input type="text" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" id="phone" name="phone" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Postcode</label>
                                <input type="text" class="form-control" id="postcode" name="postcode">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Travel Date</label>
                                <input type="text" class="form-control"  id="travel_date" name="travel_date">
                            </div>
                        </div>                  
                        <div class="form-group">
                            <div class="fildes_outer">
                              <label>Comments</label>
                              <textarea class="form-control" name="comments" rows="20" style="height: 150px;"></textarea>                    
                            </div>
                        </div>
                        <div class="notification text-center mb-2"></div>
                        <div>
                            <button type="submit" name="myForm" class="btn btn-primary btn-block btn_enquiry">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@php
    $seasons = "empty";
    if(!empty($season))
    {
        $seasons = "";
    }
@endphp
@stop
@section( 'custom-js' )
<script type="text/javascript">var season = "<?php echo $seasons; ?>";</script>
<script type="text/javascript" src="{{ url('js/tour.js') }}"></script>
@stop
