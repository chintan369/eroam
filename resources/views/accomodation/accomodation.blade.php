@extends('layouts.common')
@section('content')
@php
    $travellers = session()->get('search')['travellers'];
    $sessionItinerary = session()->get('search')['itinerary'];

    if(isset($sessionItinerary[$data['leg']]['hotel']['customerSessionId'])){
        $customerSessionId = session()->get('search')['itinerary'][$data['leg']]['hotel']['customerSessionId'];
    }else{
        $customerSessionId = '';
    }

    $child = session()->get('search')['child_total'];
    $rooms = session()->get('search')['rooms'];
@endphp
<div class="body_sec">
    <div class="itinerary_block">
        <input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
        <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
        <div class="booking-summary">
            <span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.update_data_message') </span>
        </div>
        @include('accomodation.partials.accomodation')
    </div>
</div>
<input type="hidden" class="the-city-id" value="{{ json_encode( get_city_by_id( $data['city_id'] ) ) }}">
@endsection
@push('scripts') 
    <script src="{{url('js/api-aot.js')}}"></script>
    <script src="{{url('js/api-mystifly.js')}}"></script>
    <script src="{{url('js/api-hb.js')}}"></script>
    <script src="{{url('js/api-ae.js')}}"></script>
    <script src="{{url('js/lodash.min.js')}}"></script>
    <script type="text/javascript">
        var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
        var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}";
        var listOfCurrencies = JSON.parse($('#currency-layer').val());
        @if( session()->has('search_input') )
        <?php $input = session()->get('search_input');?>
        var travel_pref = [{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}];
        @else
        var travel_pref = [];
        @endif
    </script>
    <script src="{{url('js/itinerary/common.js')}}"></script>
    <script src="{{url('js/booking-summary.js')}}"></script>
    <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
    <script charset="utf-8">
        var default_provider = '{{ $data['default_provider'] }}';
        var default_hotel = '{{ $data['default_hotel_id'] }}';
        var default_room_price_raw = '{{$data['default_room_price']}}';
        var default_room_price = '{{$data['default_room_price'], 2  / $data["travellers"]}}';
        var default_room_name = '{{$data['default_room_name'] }}';
        var default_price_id = '{{ $data['default_price_id']}}';
        var default_room_id = '{{ $data['default_room_id']}}';
        var default_currency = '{{ $data['default_currency']}}';
        var from_date = "{{ $data['date_from'] }}";
        var to_date = "{{ $data['date_to'] }}";
        var city_id = '{{ $data['city_id'] }}'; 
        var search_session = JSON.parse( $('#search-session').val() );
        var search_input = JSON.parse( $('#search-input').val() );
        var totalHotels = 0;
        var maxPrice = 0;
        var hotel_filter = ["{{ join( ', ', $data['hotel_filter'] ) }}"];
        var cityData = JSON.parse($('.the-city-id').val());
        var room_type_id = ["{{ join( ', ', $data['room_type_id'] ) }}"];
        var hotel_stars = ["{{ join( ', ', $data['hotel_stars'] ) }}"];
        var default_hotel_category_id    = '{{ $data['default_hotel_category_id'] }}';
        var number_of_travellers = "{{ $data["travellers"] }}";
        var leg = '{{$data['leg']}}';

        var hotels_appended = [];
        var hotels_from_price = {
            eroam: [],
            hb: [],
            aot: [],
            ae: [],
        }
        $(document).ready(function() {
            $('#listview-tab').click(function() {
                $('.hotel-list-selected').show();
                $('.hotel-grid-selected').hide();
            });
            $('#gridview-tab').click(function() {
                $('.hotel-list-selected').hide();
                $('.hotel-grid-selected').show();
            });
            for (var i = 1; i < 6; i++) {
                $(".checbox-hover" + i).mouseover(function() {
                    $(".checkbox-tip" + i).css("display", "block");
                });
                $(".checbox-hover" + i).mouseout(function() {
                    $(".checkbox-tip" + i).css("display", "none");
                });
            }


            $(document).on('click', '#searchProperty', function() {
                $("#property-tab").children().removeClass("active");
                var property = $(this).text(); //alert(provider);
                filterListProperty(property);
                $(this).parent().addClass("active");
            });

            /**************************** start sorting function ********************/
            // accomodation-grid-list hotel-grid-list
            $(document).on('click', '#priceSortAsc', function() { //alert(1);
                sortMeBy('data-from-price', 'div.hotel-list1', 'div.hotel-list-sort', 'asc');
                sortMeBy('data-from-price', 'div.hotel-list2', 'div.hotel-grid-sort', 'asc');
                $('#priceSortAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
                $('#priceSortAsc').attr('id', 'priceSortDesc');

            });
            $(document).on('click', '#priceSortDesc', function() {
                $('#priceSortDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
                sortMeBy('data-from-price', 'div.hotel-list1', 'div.hotel-list-sort', 'desc');
                sortMeBy('data-from-price', 'div.hotel-list2', 'div.hotel-grid-sort', 'desc');
                $('#priceSortDesc').attr('id', 'priceSortAsc');
            });

            $(document).on('click', '#RatingAsc', function() { //alert(1);
                sortMeBy('data-star-rating', 'div.hotel-list1', 'div.hotel-list-sort', 'asc');
                sortMeBy('data-star-rating', 'div.hotel-list2', 'div.hotel-grid-sort', 'asc');
                $('#RatingAsc i').removeClass('fa-caret-down').addClass('fa-caret-up');
                $('#RatingAsc').attr('id', 'RatingDesc');
            });

            $(document).on('click', '#RatingDesc', function() {
                $('#RatingDesc i').removeClass('fa-caret-up').addClass('fa-caret-down');
                sortMeBy('data-star-rating', 'div.hotel-list1', 'div.hotel-list-sort', 'desc');
                sortMeBy('data-star-rating', 'div.hotel-list2', 'div.hotel-grid-sort', 'desc');
                $('#RatingDesc').attr('id', 'RatingAsc');
            });

            function sortMeBy(arg, sel, elem, order) {
                var $selector = $(sel),
                    $element = $selector.children(elem);

                $element.sort(function(a, b) {
                    var an = parseFloat(a.getAttribute(arg)),
                        bn = parseFloat(b.getAttribute(arg));

                    if (order == 'asc') {
                        if (an > bn)
                            return 1;
                        if (an < bn)
                            return -1;
                    } else if (order == 'desc') {
                        if (an < bn)
                            return 1;
                        if (an > bn)
                            return -1;
                    }
                    return 0;
                });

                $element.detach().appendTo($selector);
            }
            /**************************** end sorting function ********************/
            $('.accomodation-wrapper').slimScroll({
                height: '100%',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

            calculateHeight();
			if(!$("#checkin-date").hasClass('disabled')){
				$("#checkin-date").datepicker({
					format: 'dd M yyyy',
					autoclose: true,
					todayHighlight: true
				});
			}
			if(!$("#checkout-date").hasClass('disabled')){
				$("#checkout-date").datepicker({
					format: 'dd M yyyy',
					autoclose: true,
					todayHighlight: true
				});
			}
			
            var selected_attributes,
                is_selected,
                is_active;

            buildHotels();




            // UPDATED BY RGR
            $('body').on('click', '.room-type-button', function() {
                buildRoomTypes(false, $(this).attr('data-index'), $(this).attr('data-season'), $(this).attr('data-hotel'), $(this).attr('data-provider'), $(this).attr('data-hotel-name'), $(this).attr('data-hotel-location'), $(this).attr('data-rooms'));
            });
            $('body').on('click', '.select-room-btn', function() {
                $('.select-room-btn').prop('disabled', false);
                $(this).prop('disabled', true);
            });

            /*
            |Added by Junfel
            |Sorting the display by Hotel names
            */
            $('.sort-asc-desc').click(function() {
                $('.sort-asc-desc').find('i').removeClass('black-background white');
                $(this).find('i').toggleClass('black-background white');
                var asc_desc = $(this).find('i').attr('data-sort');
                var asc = asc_desc == 'asc' ? true : false;
                hotelSort(asc);
            });
            /*
            | Added by Junfel
            | Update booking summary when room is selected
            */
            $('body').on('click', '.selected-room', function() {
                // UPDATED BY RGR
                $('.selected-room').prop('disabled', false);
                $(this).prop('disabled', true);
                var data = $(this).data();

                if (data.provider == 'hb') {
                    data.images = [data.images];
                }

                default_provider = data.provider;
                default_hotel = data.hotelId;
                default_room_name = data.roomName;
                default_room_id = data.roomId;

                if (data.provider == 'eroam' && data.roomName.includes('Dorm')) {
                    var perPersonPrice = Math.ceil(eroam.convertCurrency(data.price, data.currency)).toFixed(2);
                } else {
                    var perPersonPrice = Math.ceil(eroam.convertCurrency(data.price, data.currency) / number_of_travellers).toFixed(2);
                }

                //$('#hotel-price-'+data.hotelId).html('<small> From Price Per Night <br />( Per Person ) </small> <br /> '+globalCurrency+' '+Math.ceil(perPersonPrice).toFixed(2));
                $('#hotel-price-' + data.hotelId).html('From Price <strong>' + globalCurrency + ' ' + Math.ceil(perPersonPrice).toFixed(2) + '</strong> (Per Night Per Person)');
                $('.hotel-list').removeClass('selected-hotel').attr('data-is-selected', 'false');
                $('.hotel-list1-' + data.id).addClass('selected-hotel').attr('data-is-selected', 'true').attr('data-price', data.price).attr('data-price-id', data.priceId);
                $('.room-types').modal('hide');

                var session = search_session.itinerary;

                if (session.length > 0) {

                    if (session[leg].city.id == city_id) {
                        search_session.itinerary[leg].hotel = convertToSnakeCase(data);
                        if (leg == 0) {
                            search_session.itinerary[0].city.accommodation = true;
                        }
                        bookingSummary.update(JSON.stringify(search_session));
                    }

                }

            });
            /*
            | Added by Junfel
            | popover toggler
            */
            $('.filter-buttons').click(function() {
                var id = $(this).attr('id');
                switch (id) {
                    case 'sort-ad':
                        $('#number_of_nights_list').removeClass('show');
                        $('#search').removeClass('show');
                        $('#sort').toggleClass('show');
                        break;
                    case 'search-toggler':
                        $('#number_of_nights_list').removeClass('show');
                        $('#sort').removeClass('show');
                        $('#search').toggleClass('show');
                        $('#search-field').focus();
                        return false;
                        break;
                    case 'number_of_nights':
                        $('#sort').removeClass('show');
                        $('#search').removeClass('show');
                        $('#number_of_nights_list').toggleClass('show');
                        break;
                }
            });
            $('body').click(function(e) {
                var target = e.target;
                if ($(target).hasClass('filter-buttons') === false && $('.popover').has(e.target).length === 0) {
                    $('.filter-buttons').removeClass('show');
                    $('.popover.bottom').removeClass('show');
                }
            });
            /*
            | added by Junfel
            | displaying suggestions
            | arrow up and down selection
            */
            arrowSelect = function(event) {
                var numOfSuggestions = $('.suggestions').length - 1;
                var selected = -1;
                var items = $('.suggestions');
                var current = $('.selected-suggestion').index();

                if (event.which == $.ui.keyCode.UP) {
                    if (current == -1) {
                        $(items[numOfSuggestions]).addClass('selected-suggestion');
                        var value = $(items[numOfSuggestions]).text();
                        if (value != '' && value != null) {
                            $('.search-field').val(value);
                        }
                    } else {
                        var next = current - 1;
                        next = next < 0 ? numOfSuggestions : next;
                        $(items[current]).removeClass('selected-suggestion');
                        $(items[next]).addClass('selected-suggestion');
                        var value = $(items[next]).text();
                        if (value != '' && value != null) {
                            $('.search-field').val(value);
                        }
                    }
                } else if (event.which == $.ui.keyCode.DOWN) {
                    if (current == -1) {
                        $(items[0]).addClass('selected-suggestion');
                        var value = $(items[0]).text();
                        if (value != '' && value != null) {
                            $('.search-field').val(value);
                        }
                    } else {
                        var next = current + 1;
                        next = next > numOfSuggestions ? 0 : next;
                        $(items[current]).removeClass('selected-suggestion');
                        $(items[next]).addClass('selected-suggestion');
                        var value = $(items[next]).text();
                        if (value != '' && value != null) {
                            $('.search-field').val(value);
                        }
                    }
                } else {
                    $('#suggestion-container').find('ul').html('');
                    var pattern = new RegExp($('.search-field').val().toString(), 'gi');
                    var priceTo = parseFloat($('#price-to').val());
                    var priceFrom = parseFloat($('#price-from').val());

                    $('.hotel-list').filter(function() {
                        var hotPrice = parseFloat($(this).attr('data-from-price'));

                        if (hotPrice >= priceFrom && hotPrice <= priceTo) {
                            if ($(this).find('h4').text().match(pattern) != null) {
                                var list = '<li class="suggestions">' + $(this).find('h4').text() + '</li>';
                                $('#suggestion-container').find('ul').append(list);
                            }
                        } else {
                            if ($(this).find('h4').text().match(pattern) != null) {
                                var list = '<li class="suggestions">' + $(this).find('h4').text() + '</li>';
                                $('#suggestion-container').find('ul').append(list);
                            }
                        }
                    });
                }
            }
            /*
            | Added by junfel 
            | function for showing suggestion on keydown and selecting suggestion through arrow down and arrow up
            */
            $('.search-field').bind('keydown', arrowSelect);
            /*
            | Added by Junfel
            | function for clearing search and display all hotels
            */
            $('#clear-search').click(function() {
                var min = parseInt(eroam.convertCurrency(0, 'AUD'));
                //var max = parseInt(eroam.convertCurrency(50000, 'AUD'));
                var max = parseInt(maxPrice);
                $('#slider-range').slider({
                    range: true,
                    min: min,
                    max: max,
                    values: [min, max]
                });
                $('#price-from').val(min);
                $('#price-to').val(max);

                $('.hotel-list').show();
                $('.search-field').val('');
                $('#search-form').submit();
            });
            /*
            | Added by Junfel
            | function for selecting suggestion through mouse click
            */
            $('body').on('click', '.suggestions', function() {
                var value = $(this).text();
                $('.search-field').val(value);
                $('#search-form').submit();
            });
            /*
            | Added by Junfel 
            | Function for searching hotels in the selected City
            */
            $('#search-form').submit(function() {

                /*
                | search pattern
                | check all possible matches and case insensitive
                */
                $('#suggestion-container').find('ul').html('');
                var pattern = new RegExp($('.search-field').val().toString(), 'gi');
                var priceTo = parseFloat($('#price-to').val());
                var priceFrom = parseFloat($('#price-from').val());

                $('.hotel-list').filter(function() {

                    var hotPrice = parseFloat($(this).attr('data-from-price'));
                    if (hotPrice >= priceFrom && hotPrice <= priceTo) {
                        if ($(this).find('h4').text().match(pattern) == null) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    } else {
                        $(this).hide();
                    }
                });

                return false;

            });
            /*
            | Added by Junfel
            | function for updating the default nights
            */
            $('.toggle-check').click(function() {

                var checkbox = $(this).find('i');
                var value = checkbox.attr('data-value');

                eroam.ajax('get', 'latest-search', {}, function(response) {

                    if (response) {
                        search_session = JSON.parse(response);
                        var session = search_session.itinerary;

                        if (session.length > 0) {

                            if (session[leg].city.id == city_id) {

                                var nights = search_session.itinerary[leg].city.default_nights;
                                //var nights = 0;
                                //if( search_session.itinerary[leg].hotel != null && !isNaN(search_session.itinerary[leg].hotel.nights) ){
                                if ((search_session.itinerary[leg].hotel != null) && typeof search_session.itinerary[leg].hotel.nights != 'undefined') {

                                    nights = search_session.itinerary[leg].hotel.nights;
                                    search_session.itinerary[leg].hotel.nights = value;

                                    var days_to_add = parseInt(value) - parseInt(nights);

                                    if (days_to_add > 0) {

                                        search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) + days_to_add;
                                        search_session.itinerary[leg].city.days_to_add = days_to_add;
                                        search_session.itinerary[leg].city.add_after_date = from_date;
                                        bookingSummary.update(JSON.stringify(search_session));

                                        checked(checkbox, value);

                                    } else {

                                        var deduct = Math.abs(days_to_add);
                                        var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

                                        search_session.itinerary[leg].city.days_to_deduct = deduct;
                                        search_session.itinerary[leg].city.deduct_after_date = from_date;
                                        search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

                                        if (search_session.itinerary[leg].activities != null) {

                                            var act = sortByDate(search_session.itinerary[leg].activities);
                                            var departureDate = getDeparture(leg);
                                            departureDate = departureDate.split(' ');

                                            /*if( parseInt(departureDate[1]) < 18 ){
                                              deduct = parseInt(deduct) - 1;
                                            }*/

                                            var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                            activityCancellation(activityDates, leg, act, checkbox, value);
                                        } else {
                                            bookingSummary.update(JSON.stringify(search_session));
                                            checked(checkbox, value);
                                        }
                                        return;
                                    }
                                } else {
                                    $('.no-hotel').modal();
                                }
                            }

                        }
                    }
                });

            });
            //jayson added for filtering of hotels by suburbs
            $('body').on("click", '#suburbs-list li', function() {
                var selected_suburbs = [];
                var selected_zones = [];

                $('#suburbs-list li').each(function() {
                    if ($(this).hasClass("active")) {
                        var data = [];
                        selected_suburbs.push($(this).data("suburb-id"));
                        selected_zones.push($(this).data("hb-zone-id"));
                    }

                });

                $('.hotel-list').each(function() {
                    provider = $(this).data('provider');
                    div_id = $(this).attr('id');
                    is_selected = $(this).data('isSelected');

                    switch (provider) {
                        case "eroam":

                            hotel_suburb_id = $(this).data('hotelSuburbId');

                            if (selected_suburbs.length > 0 && $.inArray(hotel_suburb_id, selected_suburbs) == -1 && is_selected != true && selected_suburbs[0] !== 0) {

                                $('#' + div_id).hide();
                            } else {
                                $('#' + div_id).show();
                            }

                            break;
                        case "hb":

                            hotel_hb_zone = $(this).data('hotelZoneId');
                            if (selected_zones.length > 0 && $.inArray(hotel_hb_zone, selected_zones) == -1 && is_selected != true) {

                                $('#' + div_id).hide();
                            } else {

                                $('#' + div_id).show();
                            }


                            break;
                    }
                });

            });
            //jayson added end

            $('body').on("click", '.hotel-name', function() {
                var hotel_id = $(this).attr('data-hotel-id');
                $('.hotel-list[data-hotel-id="' + hotel_id + '"]').find(".hotel-desc-less").toggle();
                $('.hotel-list[data-hotel-id="' + hotel_id + '"]').find(".hotel-desc-more").toggle();
            });

            $('body').on('click', '.view-more-btn', showHotelDetails);
            $('body').on('click', '.act-btn-less', function() {
                var provider = $(this).data('provider');
                var code = $(this).data('code');
                $('#view-more-details').html('').hide();
                $('#hotels-container1').show();
                $('.accomodation-container').show();
                var hotel = $('#hotels-container1').find('.view-more-btn[data-provider="' + provider + '"][data-code="' + code + '"]').offset().top;
                //$('.content-body').scrollTop(hotel - 250);
            });

            $('#suburbs-list li').click(function() {
                var suburb = $(this).data('suburb');

                if (suburb == 'all') {
                    if ($(this).hasClass('active')) {
                        $('#suburbs-list li').removeClass('active');
                        $('#suburbs-list li i').attr('class', 'fa fa-circle-o');
                    } else {
                        $('#suburbs-list li').addClass('active');
                        $('#suburbs-list li i').attr('class', 'fa fa-check-circle-o');
                    }
                } else {
                    $('#suburbs-list li[data-suburb="all"]').removeClass('active').find('i').attr('class', 'fa fa-circle-o');
                    $('#suburbs-list li[data-suburb="all"]').removeClass('active');
                    $(this).find('i').toggleClass('fa-circle-o fa-check-circle-o');
                    $(this).toggleClass('active');
                }
            });

            // toggle more suburds
            $('#suburbs-list li').each(function(k, v) {
                if (k > 7) {
                    $(this).addClass('hidden-suburb')
                }
            });

            if ($('#suburbs-list li').length > 7) {
                $('#load-more-suburbs-btn').show();
            } else {
                $('#load-more-suburbs-btn').hide();
            }

            $('#load-more-suburbs-btn').click(function(e) {
                e.preventDefault();
                var hidden = $('#suburbs-list li.hidden-suburb');

                hidden.each(function(k, v) {
                    if (k < 7) {
                        $(this).removeClass('hidden-suburb');
                    }
                    // check if no more hidden suburbs
                    if (hidden.length <= 7) {
                        $('#load-more-suburbs-btn').hide();
                    }
                });
            });

        }); // end of document ready

        /* ADDED BY RGR */
        /* BUILD ROOM TYPES */
        function buildRoomTypes(viewMore = false, dataIndex, dataSeason, dataHotel, dataProvider, dataHotelName, dataHotelLocation, dataRooms) {
            var hotel_index = dataIndex;
            var season = dataSeason;
            var hotel_id = dataHotel;
            var html = '',
                table = '';
            var rooms;
            var hotel_provider = dataProvider;

            if (viewMore == true) {
                var container = $('#view-more-rooms');
            } else {
                var container = $('.table-container');
                $('.room-types').modal();
                container.html('<p class="text-center" style="padding: 20px"> <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>  </p>');
            }

            var table = '<table class="table table-striped ">';
            table += '<thead>';
            table += '<th> Room Type </th>';
            table += '<th> Price Per Person </th>';
            table += '<th> &nbsp; </th>';
            table += '</thead>';
            table += '<tbody class="room_prices">';
            table += '</tbody>';
            table += '</table>';

            switch (hotel_provider) {

                case "eroam":

                    var hotelName = dataHotelName;
                    rooms = jQuery.parseJSON(eroamRooms[hotel_id]);
                    if (rooms.length > 0) {
                        $('.selected-hotel-title').text(hotelName);
                        container.html(table);


                        for (counter = 0; counter < rooms.length; counter++) {
                            if (rooms[counter].hotel_season_id == season) {

                                rooms.hotel_name = hotelName;
                                rooms.price_id = rooms[counter].id;
                                rooms.each_room_price = price(parseFloat(rooms[counter].price).toFixed(2), rooms[counter].season['currency']['code']);

                                rooms.price = rooms[counter].price;
                                rooms.currency = rooms[counter].season.currency.code;
                                rooms.room_type = rooms[counter].room_type.name;
                                rooms.hotel_id = hotel_id;
                                rooms.room_type_id = rooms[counter].room_type.id;
                                rooms.room_id = rooms[counter].room_type.id;
                                rooms.season_id = rooms[counter].hotel_season_id;
                                rooms.provider = 'eroam';
                                appendRoomtypes(rooms);

                            } else {
                                continue;
                            }
                        }
                    } else {
                        container.html('<p class="text-center"> No Room Type Available. </p>');
                    }
                    break;

                case "hb":
                    rooms = jQuery.parseJSON(hbRooms[hotel_id]);

                    var hotel_name = dataHotelName;
                    var address = dataHotelLocation;
                    if (rooms.length > 0) {
                        $('.selected-hotel-title').text(hotel_name);
                        container.html(table);
                        $.each(rooms, function(key, value) {
                            if (typeof value.rates[0].net == 'undefined') {
                                return;
                            }
                            rooms.hotel_name = hotel_name;
                            rooms.price = parseFloat(value.rates[0].net).toFixed(2);
                            rooms.rate_key = value.rates[0].rateKey;
                            rooms.currency = 'AUD';
                            rooms.room_type = value.name;
                            rooms.room_id = value.code;
                            rooms.hotel_id = hotel_id;
                            rooms.provider = 'hb';
                            rooms.location = address;
                            rooms.images = rooms[0].images;
                            rooms.description = rooms[0].description;
                            rooms.cancellation_policy = '';
                            appendRoomtypes(rooms);
                        });
                    } else {
                        container.html('<p class="text-center"> No Room Type Available. </p>');
                    }
                    break;

                case "expedia":
                    rooms = jQuery.parseJSON(expediaRooms[hotel_id]);

                    var hotel_name = dataHotelName;
                    var address = dataHotelLocation;

                    if (rooms.length > 0) {
                        $('.selected-hotel-title').text(hotel_name);
                        container.html(table);
                        $.each(rooms, function(key, value) {
                            if (typeof value.rate == 'undefined') {
                                return;
                            }
                            rooms.hotel_name = hotel_name;
                            rooms.price = parseFloat(value.rate).toFixed(2);
                            rooms.rate_key = value.rateKey;
                            rooms.rate_code = value.rateCode;
                            rooms.currency = 'AUD';
                            rooms.room_type = value.roomDescription;
                            rooms.room_id = value.roomTypeCode;
                            rooms.hotel_id = hotel_id;
                            rooms.provider = 'expedia';
                            rooms.location = address;
                            rooms.images = rooms[0].images;
                            rooms.description = rooms[0].roomDescription;
                            rooms.cancellation_policy = '';
                            appendRoomtypes(rooms);
                        });
                    } else {
                        container.html('<p class="text-center"> No Room Type Available. </p>');
                    }
                    break;

                case 'ae':
                    rooms = jQuery.parseJSON(aeRooms[hotel_id]);
                    var hotel_name = dataHotelName;
                    var address = dataHotelLocation;

                    if (rooms.length > 0) {
                        $('.selected-hotel-title').text(hotel_name);
                        container.html(table);
                        $.each(rooms, function(key, value) {
                            if (typeof value.price == 'undefined') {
                                return;
                            }
                            rooms.hotel_name = hotel_name;
                            rooms.price = parseFloat(value.price).toFixed(2);
                            rooms.currency = 'AUD';
                            rooms.room_type = value.name;
                            rooms.room_id = value.room_id;
                            rooms.meal_id = value.meal_id;
                            rooms.hotel_id = hotel_id;
                            rooms.provider = 'ae';
                            rooms.location = address;
                            rooms.images = rooms[0].images;
                            rooms.description = rooms[0].description;
                            rooms.cancellation_policy = '';
                            rooms.group_identifier = value.group_identifier;
                            appendRoomtypes(rooms);
                        });
                    } else {
                        container.html('<p class="text-center"> No Room Type Available. </p>');
                    }

                    break;

                case "aot":

                    var room_opts = dataRooms;
                    var hotel_name = dataHotelName;
                    var address = dataHotelLocation;
                    room_opts = room_opts.split("|");

                    var aot_room_data = {
                        type: 'orr_multi',
                        opt: room_opts,
                        date_from: formatDate(from_date),
                        date_to: formatDate(to_date)
                    };

                    $('.selected-hotel-title').text(hotel_name);

                    var eroamApiCallAotRoomTypes = eroam.apiDeferred('api_aot', 'POST', aot_room_data, 'aot', true);

                    eroam.apiPromiseHandler(eroamApiCallAotRoomTypes, function(aotReponseRoomTypes) {


                        if (aotReponseRoomTypes.length > 0) {
                            container.html(table);

                            aotHotelsArray = aotHotelsArray.map(function(val) {
                                val.rooms = [];
                                return val;
                            });

                            var aotHotelsArrayMerged = aot.clean('supp_rooms_merge', aotHotelsArray, {
                                array: aotReponseRoomTypes,
                                room_type: 'double'
                            })

                            var hotel_code = hotel_id;
                            aotHotelsArray = aot.separateRoomTypesPrices(aotHotelsArray, aotHotelsArrayMerged, hotel_code);
                            var aotHotelSelected = aotHotels[hotel_id];
                            if (aotHotelSelected.rooms.length > 0) {
                                var rooms = {};
                                for (counter = 0; counter < aotHotelSelected.rooms.length; counter++) {
                                    var currentRoom = aotHotelSelected.rooms[counter];
                                    currentRoom.hotel_name = hotel_name;
                                    currentRoom.currency = 'AUD';
                                    currentRoom.provider = 'aot';
                                    currentRoom.price = parseFloat(currentRoom.price).toFixed(2);
                                    currentRoom.hotel_id = hotel_id;
                                    currentRoom.price_id = currentRoom.code;
                                    currentRoom.location = address;
                                    appendRoomtypes(currentRoom);
                                }
                            }
                        } else {
                            container.html('<p class="text-center"> No Room Type Available. </p>');
                        }
                    });

                    break;
            }
        }

        function removeAllAct() {

        }
        /* ADDED BY RGR */
        /* SHOW HOTEL DETAILS */
        function showHotelDetails(event) {
            event.preventDefault();

            var code = $(this).data('code');
            $('#hotel_detail_' + code).submit();
            var provider = $(this).data('provider');
            $('#hotel_detail_' + code).submit();
            // if($(this).data('provider') == 'expedia'){
            //  $('#hotel_detail_'+code).submit();
            // }

            var star_rating = $(this).data('star-rating');
            var arrivalDate = $(this).attr('data-from-date');
            var departureDate = $(this).attr('data-to-date');
            var travellers = $(this).attr('data-num-travellers');
            var latitude = $(this).attr('latitude');
            var longitude = $(this).attr('longitude');

            var room = "";
            if (travellers % 2 == 0) {
                j = 1;
                for (i = 1; i < travellers; i++) {
                    i = i + 1;
                    room += '&room' + j + '=2';
                    j++;
                }
            } else {
                travellers = Math.ceil(travellers / 2);
                for (i = 1; i <= travellers; i++) {
                    if (i == travellers) {
                        room += '&room' + i + '=1';
                    } else {
                        room += '&room' + i + '=2';
                    }
                }
            }

            var room = room;
            var roomTypeCode = $(this).attr('data-roomtype-code');
            var includeDetails = true;
            var options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
            var numberOfBedRooms = '1';
            var rateKey = $(this).attr('data-rate-key');

            var dataAttr = {
                index: $(this).attr('data-index'),
                season: $(this).attr('data-season'),
                hotel: $(this).attr('data-hotel'),
                provider: $(this).attr('data-provider'),
                hotelName: $(this).attr('data-hotel-name'),
                hotelLocation: $(this).attr('data-hotel-location'),
                rooms: $(this).attr('data-rooms')
            };

            eroam.ajax('get', 'hotels/view-more', {
                provider: provider,
                code: code,
                dataAttr: dataAttr,
                star_rating: star_rating,
                arrivalDate: arrivalDate,
                departureDate: departureDate,
                room: room,
                roomTypeCode: roomTypeCode,
                includeDetails: includeDetails,
                options: options,
                numberOfBedRooms: numberOfBedRooms,
                rateKey: rateKey
            }, function(response) {
                $('#view-more-details').html(response).show();

                $(function() {
                    calculateHeight();

                    $('.activityDesc').slimScroll({
                        height: '100%',
                        color: '#212121',
                        opacity: '0.7',
                        size: '5px',
                        allowPageScroll: true
                    });

                    $('.hotel-close').click(function(e) {
                        $('.accomodation-container').show();
                    });
                    var jcarousel = $('.jcarousel');

                    jcarousel
                        .on('jcarousel:reload jcarousel:create', function() {
                            var carousel = $(this),
                                width = carousel.innerWidth();

                            if (width >= 1280) {
                                width = width / 5;
                            } else if (width >= 992) {
                                width = width / 4;
                            } else if (width >= 768) {
                                width = width / 4
                            } else if (width >= 640) {
                                width = width / 4;
                            } else {
                                width = width / 3;
                            }

                            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                        })
                        .jcarousel({
                            wrap: 'circular'
                        });

                    $('.jcarousel-control-prev')
                        .jcarouselControl({
                            target: '-=1'
                        });

                    $('.jcarousel-control-next')
                        .jcarouselControl({
                            target: '+=1'
                        });

                    $('.jcarousel-pagination')
                        .on('jcarouselpagination:active', 'a', function() {
                            $(this).addClass('active');
                        })
                        .on('jcarouselpagination:inactive', 'a', function() {
                            $(this).removeClass('active');
                        })
                        .on('click', function(e) {
                            e.preventDefault();
                        })
                        .jcarouselPagination({
                            perPage: 1,
                            item: function(page) {
                                return '<a href="#' + page + '">' + page + '</a>';
                            }
                        });


                });
            }, function() {
                $('.accomodation-container').hide();
                $('#view-more-loader').show();
            }, function() {
                $('#view-more-loader').hide();
            });
        }

        /*
        | Added by Junfel
        | function for sorting hotels
        */
        // function hotelSort(asc = true){
        //   var hotelList = $('.hotel-list');
        //     var alphabeticallyOrderedHotels = hotelList.sort(function (itemA, itemB) {
        //       if(asc){
        //         return $(itemA).find('h4.hotel-name').text() > $(itemB).find('h4.hotel-name').text();
        //       }else{
        //         return $(itemB).find('h4.hotel-name').text() > $(itemA).find('h4.hotel-name').text();
        //       }

        //     });
        //     $('.hotel-list1').html(alphabeticallyOrderedHotels);
        // }

        function price(price, code) {
            var new_price = Math.ceil(price).toFixed(2);
            var new_code = code.replace('D', '$');
            return new_code + ' ' + new_price;
        }

        function date_between(from, price_from, price_to) {
            var result = false;
            var from = new Date(from);
            var price_from = new Date(price_from);
            var price_to = new Date(price_to);
            if (from >= price_from && from <= price_to) {
                result = true;
            }
            return result;
        }

        function str_limit(str) {
            var result = str;
            if (str.length > 500) {
                result = str.substring(0, 500) + '.....';
            }
            return result;
        }

        var ci = 0;

        function appendHotels(hotel) {
            var name, property_type, id, image, index, description, season_id, html, rooms, attributes, is_active, hidden, roomData, roomName, stars, star_rating, latitude, longitude;
            var location = '';
            var roomTypeCode = '';
            var rateKey = '';
            var hotel_rooms, hotelName, hotelSeason, hotelId, address_1, cityName, countryName, currencyCode, hotelPrice;

            switch (hotel.provider) {
                case 'eroam':
                    name = hotel.name;
                    id = hotel.id;
                    //image       = "{{config('env.DEV_CMS_URL')}}" + hotel.image;
                    image = "https://cms.eroam.com/" + hotel.image;
                    index = hotel.index;
                    description = hotel.description;
                    season_id = hotel.season_id;
                    attributes = hotel.attributes;
                    provider = "eroam";
                    is_active = hotel.is_active;
                    location = hotel.address_1;
                    category = hotel.hotel_category_id;
                    suburb = hotel.suburb;
                    rooms = '';
                    hotel_rooms = JSON.stringify(hotel.prices);
                    hotelName = hotel.name;
                    hotelId = hotel.id;
                    hotelSeason = hotel.season_id;
                    address_1 = hotel.address_1;
                    cityName = hotel.city.name;
                    countryName = hotel.city.country.name;
                    currencyCode = hotel.currency.code;
                    //url is needed for view button
                    url = window.location.origin + '/eroam/hotel/' + id;
                    //end
                    hidden = hotel.hidden;
                    if (window.eroamRooms === undefined) window.eroamRooms = {};
                    window.eroamRooms[id] = (hotel.prices) ? JSON.stringify(hotel.prices) : "";
                    roomData = getHotelFromPrice(hotel);
                    fromPriceString = roomData.fromPrice;
                    roomName = roomData.roomName;
                    stars = getHotelCategory(hotel.star_rating, 'eroam');
                    var star_rating = hotel.star_rating || "No Data";
                    if (star_rating != 'No Data') { //alert(hotel.star_rating);
                        star_rating = hotel.star_rating;
                    } else {
                        star_rating = 0.00;
                    }
                    hotelPrice = roomData.fromPrice
                    //fromOriginalPrice: Math.ceil( converted_price ).toFixed(2),
                    break;

                case 'aot':

                    provider = hotel.provider;
                    name = hotel.name;
                    id = hotel.code;
                    image = hotel.image;
                    category = Math.floor(hotel.StarRating);
                    index = hotel.index;
                    description = (hotel.description) ? hotel.description : "";
                    season_id = hotel.season_id;
                    provider = hotel.provider;
                    rooms = hotel.room_codes.join("|");
                    attributes = hotel.attributes;
                    is_active = hotel.is_active;
                    location = hotel.location;
                    url = window.location.origin + '/aot/hotel/' + hotel.code;
                    hidden = [];
                    //fromPriceString = getHotelFromPrice( hotel );
                    roomData = getHotelFromPrice(hotel);
                    fromPriceString = roomData.fromPrice;
                    roomName = roomData.roomName;
                    stars = '';
                    star_rating = '';

                    break;

                    //added by jayson for Hotel Beds API
                case 'hb':
                    name = hotel.name;
                    id = hotel.code;
                    image = hotel.image;
                    index = hotel.code;
                    provider = "hb";
                    category = hotel.category;
                    season_id = '';
                    rooms = '';

                    if (window.hbRooms === undefined) window.hbRooms = {};
                    hotel.rooms[0].images = [image];
                    hotel.rooms[0].description = hotel.description;
                    hotel.rooms[0].cancellation_policy = '';

                    window.hbRooms[id] = (hotel.rooms) ? JSON.stringify(hotel.rooms) : "";

                    description = (hotel.description) ? hotel.description : "";
                    attributes = hotel.attributes;
                    is_active = hotel.is_active;
                    location = hotel.address;
                    url = window.location.origin + '/hb/hotel/' + hotel.code;
                    hidden = [];
                    //fromPriceString    = getHotelFromPrice( hotel );
                    roomData = getHotelFromPrice(hotel);
                    fromPriceString = roomData.fromPrice;
                    roomName = roomData.roomName;

                    stars = getHotelCategory(hotel.categoryCode, 'hb');
                    star_rating = hotel.categoryCode;
                    break;

                case 'expedia':

                    name = hotel.name;
                    hotelName = hotel.name;
                    hotelSeason = '';
                    roomTypeCode = hotel.rooms[0].roomTypeCode;
                    rateKey = hotel.rooms[0].rateKey;
                    rateCode = hotel.rooms[0].rateCode;
                    hotelId = hotel.hotelId;
                    id = hotel.hotelId;
                    address_1 = hotel.address;
                    cityName = hotel.city;
                    countryName = hotel.countryCode;
                    currencyCode = hotel.rateCurrencyCode;
                    hotelPrice = hotel.rooms[0].rate;
                    image = hotel.image;
                    index = hotel.hotelId;
                    latitude = hotel.latitude;
                    longitude = hotel.longitude;
                    provider = "expedia";
                    category = hotel.propertyCategory;
                    season_id = '';
                    rooms = '';
                    property_type = getPropertyCategory(hotel.propertyCategory);
                    if (window.expediaRooms === undefined) window.expediaRooms = {};
                    hotel.rooms[0].images = [image];
                    //hotel.rooms[0].description = hotel.description;
                    hotel.rooms[0].cancellation_policy = '';

                    window.expediaRooms[id] = (hotel.rooms) ? JSON.stringify(hotel.rooms) : "";

                    description = (hotel.description) ? hotel.description : "";
                    attributes = hotel.attributes;
                    is_active = hotel.is_active;
                    location = hotel.address;
                    url = window.location.origin + '/expedia/hotel/' + hotel.hotelId;
                    hidden = [];

                    roomData = getHotelFromPrice(hotel);
                    fromPriceString = roomData.fromPrice;
                    roomName = roomData.roomName;

                    stars = getHotelCategory(hotel.hotelRating, 'expedia');
                    star_rating = hotel.hotelRating;
                    break;

                case 'ae':
                    name = hotel.name;
                    id = hotel.hotel_id;
                    image = (hotel.image) ? hotel.image : 'http://cms.testeroam.com/images/no-image.png';
                    index = hotel.hotel_id;
                    provider = "ae";
                    category = hotel.category;
                    season_id = '';
                    rooms = '';
                    if (window.aeRooms === undefined) window.aeRooms = {};
                    hotel.rooms[0].images = [image];
                    hotel.rooms[0].description = hotel.description;
                    hotel.rooms[0].cancellation_policy = '';

                    window.aeRooms[id] = (hotel.rooms) ? JSON.stringify(hotel.rooms) : "";

                    description = (hotel.description) ? hotel.description : "";
                    attributes = hotel.attributes;
                    is_active = hotel.is_active;
                    location = hotel.city_name;
                    url = '';
                    hidden = [];
                    //fromPriceString    = getHotelFromPrice( hotel );
                    roomData = getHotelFromPrice(hotel);
                    fromPriceString = roomData.fromPrice;
                    roomName = roomData.roomName;
                    stars = '';
                    star_rating = '';
                    break;
            }

            var fromPriceSplit = fromPriceString.split(' ');


            maxPrice = parseInt(fromPriceSplit[1]) > maxPrice ? parseInt(fromPriceSplit[1]) : maxPrice;
            attributes = attributes + ' ' + ' data-from-price="' + fromPriceSplit[1] + '" ';
            html = '<form action="{{ action("PagesController@view_more_hotel_details") }}" method="post" name="hotel_detail" id="hotel_detail_' + id + '">';
            html += '{{ csrf_field() }}';
            html += '<input type="hidden" name="provider" value="' + provider + '">';
            html += '<input type="hidden" name="code" value="' + id + '">';
            html += '<input type="hidden" name="star_rating" value="' + star_rating + '">';
            html += '<input type="hidden" name="arrivalDate" value="' + from_date + '">';
            html += '<input type="hidden" name="departureDate" value="' + to_date + '">';
            html += '<input type="hidden" name="travellers" value="' + number_of_travellers + '">';
            html += '<input type="hidden" name="latitude" value="' + latitude + '">';
            html += '<input type="hidden" name="longitude" value="' + longitude + '">';
            html += '<input type="hidden" name="city_id" value="' + city_id + '">';

            var room = "";
            if (number_of_travellers % 2 == 0) {
                j = 1;
                for (i = 1; i < number_of_travellers; i++) {
                    i = i + 1;
                    room += '&room' + j + '=2';
                    j++;
                }
            } else {
                number_of_travellers = Math.ceil(number_of_travellers / 2);
                for (i = 1; i <= number_of_travellers; i++) {
                    if (i == number_of_travellers) {
                        room += '&room' + i + '=1';
                    } else {
                        room += '&room' + i + '=2';
                    }
                }
            }
            html += '<input type="hidden" name="room" value="' + room + '">';
            html += '<input type="hidden" name="includeDetails" value="true">';
            html += '<input type="hidden" name="roomTypeCode" value="' + roomTypeCode + '">';
            html += '<input type="hidden" name="options" value="HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES">';
            html += '<input type="hidden" name="numberOfBedRooms" value="1">';
            html += '<input type="hidden" name="rateKey" value="' + rateKey + '">';
            html += '<input type="hidden" name="rateCode" value="' + rateCode + '">';
            var searchCity = (search_session.itinerary[leg].city);
            html += '<input type="hidden" name="searchCity" value="' + searchCity.name + '">';
            html += '<input type="hidden" name="leg" value="' + leg + '">';
            html += '<input type="hidden" name="hotelName" value="' + hotelName + '">';
            html += '<input type="hidden" name="hotelSeason" value="' + hotelSeason + '">';
            html += '<input type="hidden" name="hotelId" value="' + hotelId + '">';
            html += '<input type="hidden" name="address_1" value="' + address_1 + '">';
            html += '<input type="hidden" name="cityName" value="' + cityName + '">';
            html += '<input type="hidden" name="countryName" value="' + countryName + '">';
            html += '<input type="hidden" name="currencyCode" value="' + currencyCode + '">';
            html += '<input type="hidden" name="customerSessionId" value="<?php echo $customerSessionId;?>">';
            if (provider == 'eroam') {
                var eroamPriceSplit = hotel.priceString.split('AUD ');
                html += '<input type="hidden" name="hotelPrice" value="' + eroamPriceSplit[1] + '">';
            } else {
                html += '<input type="hidden" name="hotelPrice" value="' + hotelPrice + '">';
            }

            html += '</form>';

            new_html = html;
            
            new_html += '<a href="' + url + '" data-star-latitude="' + latitude + '" data-star-longitude="' + longitude + '" data-star-rating="' + star_rating + '" data-roomType-code = "' + roomTypeCode + '" data-rate-key = "' + rateKey + '" data-num-travellers = "' + number_of_travellers + '" data-from-date = "' + from_date + '" data-to-date ="' + to_date + '" data-hotel-name="' + name + '"  data-hotel-category="' + category + '" data-index="' + index + '" data-hotel="' + id + '" data-season="' + season_id + '" data-hotel-name="' + name + '" data-provider="' + provider + '" data-rooms="' + rooms + '" data-hotel-location="' + location + '" data-code="' + id + '" class="view-more-btn"><div class="accomodation-list hotel-list hotel-list-sort ' + is_active + '" data-id="accomodation' + id + '" id="hotel-list-' + id + '" ' + attributes + ' data-star-rating="' + star_rating + '" data-property-type="' + property_type + '">';
            
            new_html += hidden.join('');
			
			var default_nights = Number('{{$data["default_nights"]}}');
			default_nights = default_nights > 9 ? default_nights: "0" + default_nights;
			
            if (is_active) {
                new_html += '<div class="itinerary_hotel_list pl-3 pr-3 pb-3 pt-3 mb-3 listactive">';
            }else{
                new_html += '<div class="itinerary_hotel_list pl-3 pr-3 pb-3 pt-3 mb-3">';
            }
            new_html += '<div class="border-bottom hotlename pb-2 mb-3 ">';
            new_html += '<div class="row">';
            new_html += '<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">';
            new_html += '<div class="cat_name">' + name + '</div>';
            var new_location = '';
            if (location != '' && location != null) {
                new_location = location;
            }
            new_html += '<div class="pt-3"><i class="ic-place"></i> Location: ' + new_location + '</div>';
            new_html += '</div>';
            new_html += '<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">';
            new_html += '<div class="float-right">';
            new_html += '<div class="night_charge">';
            new_html += '<p class="text-right pr-2 pt-5 mb-0 ">From <strong>' + fromPriceString + '</strong> P.P (Per Night)</p>';
            new_html += '</div>';
            new_html += '<div class="dayes text-center border-left ">';
            new_html += '<span>'+default_nights+'</span> Nights';
            new_html += '</div>';
            new_html += '</div>';
            new_html += '</div>';
            new_html += '</div>';
            new_html += '</div>';
            new_html += '<div class="row">';
            new_html += '<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3 ">';
            new_html += '<img src="' + image + '" alt="" />';
            new_html += '</div>';
            new_html += '<div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-9 ">';
            new_html += '<div class="rating">';
            new_html += stars;
            new_html += '</div>';

            new_html += '<p class="pt-3">';
            new_html += (description) ? str_limit(description) : "";
            new_html += '</p>';
            new_html += '</div>';
            new_html += '</div>';
            new_html += '<div class="clearfix"></div>';
            new_html += '</div>';
            new_html += '</a></div>';

            html1 = '<form action="{{ action("PagesController@view_more_hotel_details") }}" method="post" name="hotel_detail" id="hotel_detail_' + id + '">';
            html1 += '{{ csrf_field() }}';
            html1 += '<input type="hidden" name="provider" value="' + provider + '">';
            html1 += '<input type="hidden" name="code" value="' + id + '">';
            html1 += '<input type="hidden" name="star_rating" value="' + star_rating + '">';
            html1 += '<input type="hidden" name="arrivalDate" value="' + from_date + '">';
            html1 += '<input type="hidden" name="departureDate" value="' + to_date + '">';
            html1 += '<input type="hidden" name="travellers" value="' + number_of_travellers + '">';
            html1 += '<input type="hidden" name="latitude" value="' + latitude + '">';
            html1 += '<input type="hidden" name="longitude" value="' + longitude + '">';
            html1 += '<input type="hidden" name="city_id" value="' + city_id + '">';

            var room = "";
            if (number_of_travellers % 2 == 0) {
                j = 1;
                for (i = 1; i < number_of_travellers; i++) {
                    i = i + 1;
                    room += '&room' + j + '=2';
                    j++;
                }
            } else {
                number_of_travellers = Math.ceil(number_of_travellers / 2);
                for (i = 1; i <= number_of_travellers; i++) {
                    if (i == number_of_travellers) {
                        room += '&room' + i + '=1';
                    } else {
                        room += '&room' + i + '=2';
                    }
                }
            }
            html1 += '<input type="hidden" name="room" value="' + room + '">';
            html1 += '<input type="hidden" name="includeDetails" value="true">';
            html1 += '<input type="hidden" name="roomTypeCode" value="' + roomTypeCode + '">';
            html1 += '<input type="hidden" name="options" value="HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES">';
            html1 += '<input type="hidden" name="numberOfBedRooms" value="1">';
            html1 += '<input type="hidden" name="rateKey" value="' + rateKey + '">';
            html1 += '<input type="hidden" name="rateCode" value="' + rateCode + '">';
            var searchCity = (search_session.itinerary[leg].city);
            html1 += '<input type="hidden" name="searchCity" value="' + searchCity.name + '">';
            html1 += '<input type="hidden" name="leg" value="' + leg + '">';
            html1 += '<input type="hidden" name="hotelName" value="' + hotelName + '">';
            html1 += '<input type="hidden" name="hotelSeason" value="' + hotelSeason + '">';
            html1 += '<input type="hidden" name="hotelId" value="' + hotelId + '">';
            html1 += '<input type="hidden" name="address_1" value="' + address_1 + '">';
            html1 += '<input type="hidden" name="cityName" value="' + cityName + '">';
            html1 += '<input type="hidden" name="countryName" value="' + countryName + '">';
            html1 += '<input type="hidden" name="currencyCode" value="' + currencyCode + '">';
            html1 += '<input type="hidden" name="customerSessionId" value="<?php echo $customerSessionId;?>">';
            if (provider == 'eroam') {
                var eroamPriceSplit = hotel.priceString.split('AUD ');
                html1 += '<input type="hidden" name="hotelPrice" value="' + eroamPriceSplit[1] + '">';
            } else {
                html1 += '<input type="hidden" name="hotelPrice" value="' + hotelPrice + '">';
            }

            html1 += '</form>';
            // html1 +='<a href="'+url+'" data-star-latitude="'+latitude+'" data-star-longitude="'+longitude+'" data-star-rating="'+star_rating+'" data-roomType-code = "'+roomTypeCode+'" data-rate-key = "'+rateKey+'" data-num-travellers = "'+number_of_travellers+'" data-from-date = "'+from_date+'" data-to-date ="'+to_date+'" data-hotel-name="'+name+'"  data-hotel-category="'+category+'" data-index="'+index+'" data-hotel="'+id+'" data-season="'+season_id+'" data-hotel-name="'+name+'" data-provider="'+provider+'" data-rooms="'+rooms+'" data-hotel-location="'+location+'" data-code="'+id+'" class="view-more-btn"><div class="accomodation-list hotel-list '+is_active+'" data-id="accomodation'+id+'" id="hotel-list-'+id+'" '+attributes+' >';
            html1 = '<a href="' + url + '" data-star-rating="' + star_rating + '" data-hotel-name="' + name + '"  data-hotel-category="' + category + '" data-index="' + index + '" data-hotel="' + id + '" data-season="' + season_id + '" data-hotel-name="' + name.toLowerCase() + '" data-provider="' + provider + '" data-rooms="' + rooms + '" data-hotel-location="' + location + '" data-code="' + id + '" class="view-more-btn"><div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 pb-4 accomodation-grid-list hotel-grid-list hotel-grid-sort ' + is_active + '" data-id="accomodation' + id + '" id="hotel-list-' + id + '" ' + attributes + ' data-star-rating="' + star_rating + '" data-property-type="' + property_type + '">';

            new_html1 = html1;

            new_html1 += '<div class="">';
            new_html1 += '<div class="itinerary_gridview_listing">';
            new_html1 += '<div class="text-center itinerary_listing"> <img src="' + image + '" alt="" /></div>';
            new_html1 += '<div class="gridview_dis text-center pt-2 pl-3 pr-3 pb-2 pb-4">';
            new_html1 += '<h6>' + name + '</h6>';
            new_html1 += '<h4>' + fromPriceString + ' Per Night</h4>';
            new_html1 += '<small class="d-block border-bottom pb-2 ">From Price Per Person / Per Night</small>';
            new_html1 += '<div class="rating pt-4">';
            new_html1 += stars;
            new_html1 += '</div>';
            if (location != '' && location != null) {
                new_location = location;
            }
            new_html1 += '<p class="mb-2 pt-3">' + new_location + '</p>';
            new_html1 += '<p class="mb-0"><strong>Duration:</strong> '+default_nights+' Nights</p>';
            new_html1 += '</div>';
            new_html1 += '</div>';
            new_html1 += '</div>';
            new_html1 += '</a>';

            clean_name = name.toLowerCase().replace(/\s+/g, '').replace(/[^a-z0-9]/ig, '');
            clean_data = {
                id: id,
                name: clean_name,
                price: fromPriceString,
            };
            var appendedKey = isHotelAppended(clean_name);
            if (appendedKey === false && fromPriceSplit[1] > 0) {

                if (is_active) {
                    $('.hotel-list-selected').prepend(new_html);
                    $('.hotel-grid-selected').prepend(new_html1);
                } else {
                    //this will filter hotel
                    if (hotel.provider == 'eroam' && hotel.category == default_hotel_category_id) { //filter if provider is eroam
                        if ($('.hotel-list1 > div:first-child').hasClass("selected-hotel")) {
                            $('.hotel-list1 > div:first-child').after(new_html);
                            $('.hotel-list2 > div:first-child').after(new_html1);
                        } else {
                            $('.hotel-list1').prepend(new_html);
                            $('.hotel-list2').prepend(new_html1);
                        }
                    } else if (hotel.provider != 'eroam' && jQuery.inArray(hotel.category, hotel_stars) != -1) {
                        if ($('.hotel-list1 > div:first-child').hasClass("selected-hotel")) {
                            $('.hotel-list1 > div:first-child').after(new_html);
                            $('.hotel-list2 > div:first-child').after(new_html1);
                        } else {
                            $('.hotel-list1').prepend(new_html);
                            $('.hotel-list2').prepend(new_html1);
                        }
                    } else {
                        $('.hotel-list1').append(new_html);
                        $('.hotel-list2').append(new_html1);
                    }
                }
                totalHotels += 1;
                hotels_appended.push(clean_data);

            } else {

                var appendedData = hotels_appended[parseInt(appendedKey)];
                var appendedId = appendedData.id;

                if (!$('.hotel-list1-' + appendedId).hasClass('selected-hotel')) {

                    var appendedPrice = appendedData.price.split(' ');
                    appendedPrice = parseInt(appendedPrice[1]);
                    var currentPrice = is_active ? hotel.priceString.split(' ') : fromPriceString.split(' ');
                    currentPrice = parseInt(currentPrice[1]);


                    if (currentPrice < appendedPrice && currentPrice > 0) {


                        //$('#hotel-price-'+appendedId).html('<small> From Price Per Night <br> ( Per Person ) </small> <br>'+fromPriceString);
                        $('#hotel-price-' + appendedId).html('From Price <strong>' + fromPriceString + '</strong> (Per Night Per Person)');
                        $('.hotel-list1-' + appendedId).attr('data-hotel-id', id)
                            .attr('data-index', index)
                            .attr('data-provider', provider);

                        if (is_active) {
                            $('.hotel-list1-' + appendedId).addClass('selectedHotel')
                                .attr('data-price'.hotel.priceString)
                                .attr('data-price-id', hotel.defaultPriceId)
                                .attr('data-selected', 'true');

                            //$('#hotel-price-'+appendedId).html('<small> From Price Per Night <br> ( Per Person ) </small> <br>'+hotel.priceString);
                            $('#hotel-price-' + appendedId).html('From Price <strong>' + hotel.priceString + '</strong> (Per Night Per Person)');
                        }
                        $('.hotel-list1-' + appendedId).find('button.room-type-button')
                            .attr('data-hotel-name', name)
                            .attr('data-hotel-category', category)
                            .attr('data-index', index)
                            .attr('data-hotel', id)
                            .attr('data-season', season_id)
                            .attr('data-provider', provider)
                            .attr('data-rooms', rooms)
                            .attr('data-hotel-location', location);

                        $('.hotel-list1-' + appendedId).find('a.view-more-btn')
                            .attr('data-provider', provider)
                            .attr('data-code', id);
                        $('.hotel-list1-' + appendedId).find('.hotel-name')
                            .attr('data-hotel-id', id);

                        $('.hotel-list1-' + appendedId).attr('id', 'hotel-list1-' + id);
                        hotels_appended[parseInt(appendedKey)].id = id;
                        hotels_appended[parseInt(appendedKey)].price = is_active ? hotel.priceString : fromPriceString;
                    }
                }
            }

            calculateHeight();
        }

        // edited by miguel on 2017-01-18; cleaned data, stored in session and passed to function create_itinerary() 
        function appendRoomtypes(rooms) {
            var price, roomName, html = '';
            switch (rooms.provider) {
                case 'eroam':
                    price = rooms.price;
                    convertedPrice = eroam.convertCurrency(price, rooms.currency);

                    roomName = ucfirst(rooms.room_type);
                    btnDataAttrHtml = [
                        'data-provider="eroam"',
                        'data-id="' + rooms.hotel_id + '"',
                        'data-hotel-id="' + rooms.hotel_id + '"',
                        'data-name="' + rooms.hotel_name + '"',
                        'data-price="' + price + '"',
                        'data-currency="' + rooms.currency + '"',
                        'data-price-id="' + rooms.price_id + '"',
                        'data-hotel-price-id="' + rooms.price_id + '"',
                        'data-room-id="' + rooms.room_type_id + '"',
                        'data-room-type-id="' + rooms.room_type_id + '"',
                        'data-room-name="' + rooms.room_type + '"',
                        'data-checkin="' + from_date + '"',
                        'data-checkout="' + to_date + '"'
                    ].join('');
                    break;

                case 'aot':
                    price = (rooms.price / 100);
                    convertedPrice = eroam.convertCurrency(price, 'AUD');
                    roomName = ucfirst(rooms.name);
                    btnDataAttrHtml = [
                        'data-provider="aot"',
                        'data-id="' + rooms.hotel_id + '"',
                        'data-hotel-id="' + rooms.hotel_id + '"',
                        'data-name="' + rooms.hotel_name + '"',
                        'data-price="' + price + '"',
                        'data-currency="' + 'AUD' + '"',
                        'data-price-id="' + rooms.price_id + '"',
                        'data-room-id="' + rooms.price_id + '"',
                        'data-room-name="' + rooms.name + '"',
                        'data-checkin="' + from_date + '"',
                        'data-checkout="' + to_date + '"',
                        'data-location="' + rooms.location + '"'
                    ].join('');
                    break;

                case 'hb':
                    price = rooms.price;
                    convertedPrice = eroam.convertCurrency(price, rooms.currency);
                    roomName = ucfirst(rooms.room_type.toLowerCase());

                    btnDataAttrHtml = [
                        'data-provider="hb"',
                        'data-id="' + rooms.hotel_id + '"',
                        'data-hotel-id="' + rooms.hotel_id + '"',
                        'data-name="' + rooms.hotel_name + '"',
                        'data-price="' + price + '"',
                        'data-currency="' + rooms.currency + '"',
                        'data-price-id="' + rooms.room_id + '"',
                        'data-room-id="' + rooms.room_id + '"',
                        'data-room-name="' + roomName + '"',
                        'data-room-type="' + rooms.room_type + '"',
                        'data-checkin="' + from_date + '"',
                        'data-checkout="' + to_date + '"',
                        'data-location="' + rooms.location + '"',
                        'data-description="' + rooms.description + '"',
                        'data-images="' + rooms.images + '"',
                        'data-cancellation_policy=""'
                    ].join('');
                    break;

                case 'expedia':
                    price = rooms.price;
                    convertedPrice = eroam.convertCurrency(price, rooms.currency);

                    roomName = ucfirst(rooms.room_type.toLowerCase());

                    btnDataAttrHtml = [
                        'data-provider="expedia"',
                        'data-id="' + rooms.hotel_id + '"',
                        'data-hotel-id="' + rooms.hotel_id + '"',
                        'data-name="' + rooms.hotel_name + '"',
                        'data-price="' + price + '"',
                        'data-currency="' + rooms.currency + '"',
                        'data-price-id="' + rooms.rate_code + '"',
                        'data-room-id="' + rooms.room_id + '"',
                        'data-room-name="' + roomName + '"',
                        'data-room-type="' + rooms.room_type + '"',
                        'data-checkin="' + from_date + '"',
                        'data-checkout="' + to_date + '"',
                        'data-location="' + rooms.location + '"',
                        'data-description="' + rooms.description + '"',
                        'data-images="' + rooms.images + '"',
                        'data-cancellation_policy=""'
                    ].join('');
                    break;
                case 'ae':
                    price = rooms.price;
                    convertedPrice = eroam.convertCurrency(price, rooms.currency);
                    roomName = ucfirst(rooms.room_type.toLowerCase());
                    btnDataAttrHtml = [
                        'data-provider="ae"',
                        'data-id="' + rooms.hotel_id + '"',
                        'data-hotel-id="' + rooms.hotel_id + '"',
                        'data-name="' + rooms.hotel_name + '"',
                        'data-price="' + price + '"',
                        'data-currency="' + rooms.currency + '"',
                        'data-price-id="' + rooms.room_id + '"',
                        'data-room-id="' + rooms.room_id + '"',
                        'data-room-name="' + roomName + '"',
                        'data-room-type="' + rooms.room_type + '"',
                        'data-checkin="' + from_date + '"',
                        'data-checkout="' + to_date + '"',
                        'data-location="' + rooms.location + '"',
                        'data-description="' + rooms.description + '"',
                        'data-images="' + rooms.images + '"',
                        'data-group-identifier="' + rooms.group_identifier + '"',
                        'data-meal-id="' + rooms.meal_id + '"',
                        'data-cancellation_policy=""'
                    ].join('');
                    break;

            }
            if (rooms.provider == 'eroam' && rooms.room_type.includes("Dorm")) {
                var convertedPricePerPerson = parseFloat(convertedPrice);
            } else {
                var convertedPricePerPerson = parseFloat(convertedPrice) / parseInt('{{$data["travellers"]}}');
            }

            var priceString = globalCurrency + ' ' + Math.ceil(parseFloat(convertedPricePerPerson)).toFixed(2);

            html += '<tr>';
            html += '<td>' + roomName + '</td>';
            html += '<td class="room-type-price" data-value="' + convertedPricePerPerson + '">' + priceString + '</td>';
            room_id = (rooms.room_id) ? rooms.room_id : rooms.room_type_id;

            if (rooms.hotel_id == default_hotel && rooms.provider == default_provider && room_id == default_room_id) {
                html += '<td><button class="btn btn-secondary btn-block bold-txt selected-room" ' + btnDataAttrHtml + ' disabled>Selected</button></td>';
            } else {
                html += '<td><button class="btn btn-primary btn-block bold-txt selected-room" ' + btnDataAttrHtml + '>Select</button></td>';
            }

            html += '</tr>';

            $('.room_prices').append(html);
        }


        /* ADDED BY CALOY - UTILITIES  added by jayson for HOTELBEDS*/
        function validate_hotels(array, data, type) {
            // edited by miguel on 2016-11-15 to accommodate AE api results for validation; 
            var found = false;
            var result = false;
            if (array.length > 0) {
                found = array.some(function(value, key) {
                    var hotel_name_in_array = value.name.toLowerCase().replace(/s+/g, '').replace(/[^a-z0-9]/ig, '');
                    var hotel_name_to_add = '';
                    var validate_latlng = false;
                    var validate_name = false;
                    switch (type) {
                        case 'hb':
                            hotel_name_to_add = data.name.toLowerCase().replace(/s+/g, '').replace(/[^a-z0-9]/ig, '');
                            validate_latlng = (data.lat.toString().indexOf(value.lat.toString()) !== -1 && data.lng.toString().indexOf(value.lng.toString()) !== -1) || (value.lat.toString().indexOf(data.lat.toString()) !== -1 && value.lng.toString().indexOf(data.lng.toString()) !== -1) ? true : false;
                            validate_name = (hotel_name_in_array.toString().indexOf(hotel_name_to_add.toString()) !== -1 || hotel_name_to_add.toString().indexOf(hotel_name_in_array.toString()) !== -1) ? true : false;
                            break;
                        case 'ae':
                            try {
                                hotel_name_to_add = data.name.toLowerCase().replace(/s+/g, '').replace(/[^a-z0-9]/ig, '');

                                // check if latitude and longitude already exists in hotel_array
                                if ((data.lat.toString().indexOf(value.lat.toString()) !== -1 && data.lng.toString().indexOf(value.lng.toString()) !== -1) ||
                                    (value.lat.toString().indexOf(data.lat.toString()) !== -1 && value.lng.toString().indexOf(data.lng.toString()) !== -1)
                                ) {
                                    validate_latlng = true;
                                }
                                // check if name already exists in hotel_array
                                if (hotel_name_in_array.toString().indexOf(hotel_name_to_add.toString()) !== -1 || hotel_name_to_add.toString().indexOf(hotel_name_in_array.toString()) !== -1) {
                                    validate_name = true;
                                }
                            } catch (err) {
                                // console.log('!--- Hotel validation error on '+type+' api with hotel_id:'+value.hotel_id+'; error_essage:'+err.message);
                            }
                            break;
                    }

                    if (validate_name === true || validate_latlng === true)
                        return true;
                    else
                        return false;
                });
            }

            if (found)
                result = true;
            return result;
        }

        function formatDate(date, time = false) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (time) {
                day += ' ' + d.getHours();
            }
            return [year, month, day].join('-');
        }

        /*  Number.prototype.between  = function (a, b) {
            var min = Math.min.apply(Math, [a,b]),
                max = Math.max.apply(Math, [a,b]);
            return this > min && this < max;
        };*/

        function buildHotels() {
            var tasks = [
                //buildEroamHotels,
                buildExpediaHotels,
                @if(api_is_active('hotelbeds'))
                //buildHotelBedsHotels,
                @endif
                @if(api_is_active('aot'))
                //buildAotHotels,
                @endif

                /* Added By Rekha Patel - 3rd Oct 2017 - Get Hotels list using Expedia API */
                @if(api_is_active('expedia'))
                //buildExpediaHotels,
                @endif

                checkHotelsCount,

                @if(api_is_active('Arabian Explorers'))
                //buildAEHotels,
                @endif
                buildSuburb,
                rangeSlider,
                getDistinctProperty
            ];

            $.each(tasks, function(index, value) {
                $(document).queue('tasks', processTask(value));
            });

            $(document).queue('tasks');
            $(document).dequeue('tasks');
        }

        function processTask(fn) {
            return function(next) {
                doTask(fn, next);
            }
        }

        function doTask(fn, next) {
            fn(next);
        }

        function buildSuburb(next) {
            $('#suburbs-list').show();
            next();
        }

        function buildEroamHotels(next) {
            eroam_data = {
                city_ids: [city_id],
                date_from: formatDate(from_date),
                date_to: formatDate(to_date),
                traveller_number: "{{$data['travellers']}}",
                hotel_category_id: hotel_filter,
                room_type_id: room_type_id,
            };
            var eroamApiCall = eroam.apiDeferred('city/hotel/v2', 'POST', eroam_data, 'eroam', true);
            eroam.apiPromiseHandler(eroamApiCall, function(eroamResponse) {

                if (eroamResponse.length > 0) {
                    /*
                    | Loop Eroam hotel response
                    */
                    var hidden = [];
                    eroamResponse.forEach(function(eroamHot, eroamHotIndex) {
                        var price_season_id;
                        eroamHot.prices.forEach(function(price, index) {
                            /*
                            | filter price by season
                            */

                            if (date_between(eroam_data.date_from, price.season.from, price.season.to)) {
                                price_season_id = price.season.id;
                                var convertedPrice = eroam.convertCurrency(price.price, price.season.currency.code)
                                //maxPrice = parseInt(convertedPrice) > maxPrice ? parseInt(convertedPrice) : maxPrice;
                                //hidden.push('<input type="hidden" class="hidden-room-price" value="'+convertedPrice+'">');
                            } else {
                                return;
                            }
                        });

                        eroamHot.provider = 'eroam';
                        eroamHot.season_id = price_season_id;
                        eroamHot.image = eroamHot.image.length > 0 ? eroamHot.image[0].small : 'uploads/hotels/no-image.png';
                        eroamHot.index = eroamHotIndex;
                        is_selected = 'false';
                        is_active = '';
                        selected_attributes = '';
                        if (eroamHot.provider == default_provider) {
                            if (eroamHot.id == default_hotel) {
                                is_selected = 'true';
                                is_active = ' selected-hotel';

                                if (default_room_name.includes('Dorm')) {
                                    var converted_price = eroam.convertCurrency(default_room_price_raw, eroamHot.currency.code);
                                    eroamHot.priceString = globalCurrency + ' ' + Math.ceil(converted_price).toFixed(2);
                                } else {
                                    var converted_price = eroam.convertCurrency(default_room_price_raw, eroamHot.currency.code) / number_of_travellers;
                                    eroamHot.priceString = globalCurrency + ' ' + Math.ceil(converted_price).toFixed(2);
                                }

                                selected_attributes = 'data-price="' + eroamHot.priceString + '" data-price-id="' + default_price_id + '"';
                                eroamHot.defaultPriceId = default_price_id;
                                eroamHot.roomName = default_room_name;
                            }
                        }

                        eroamHot.hidden = hidden;
                        eroamHot.is_active = is_active;
                        eroamHot.is_selected = is_selected;
                        eroamHot.attributes = ' data-provider="eroam" data-index="' + eroamHotIndex + '" data-city-id="' + eroamHot.city_id + '" data-hotel-id="' + eroamHot.id + '" data-hotel-suburb-id="' + eroamHot.suburb_id + '" data-is-selected="' + eroamHot.is_selected + '" ' + selected_attributes;
                        try {
                            appendHotels(eroamHot);
                        } catch (e) {
                            console.log("error eraom hotel append : ", eroamHot.id, e.message);
                        }
                    });
                }

                next();
            });
        }

        function buildHotelBedsHotels(next) {
            var destinationCodes = getHBDestinationCodes(search_session.itinerary[leg].city.hb_destinations_mapped);
            if (destinationCodes.length > 0) // check if hb_destination_code exists for this city
            {
                destinationCodes.forEach(function(code, codeKey) {
                    hb_data = {
                        city_ids: city_id,
                        check_in: from_date,
                        check_out: to_date,
                        room: '1',
                        adult: '{{$data["travellers"]}}', //number of traveller
                        code: code,
                        child: '0',
                        provider: 'hb'
                    };
                    // CACHING HOTEL BEDS
                    var hbApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', hb_data, 'hb', true);

                    eroam.apiPromiseHandler(hbApiCall, function(hbResponse) {
                        if (hbResponse.array.length > 0) {

                            $.each(hbResponse.array, function(key, value) {
                                var roomsAreAvailable = false; // added by miguel to filter out hotels with no rooms
                                if (value.rooms.length > 0) {
                                    $.each(value.rooms, function(roomIndex, roomValue) {
                                        if (typeof roomValue.rates[0].net != 'undefined') {
                                            roomPrice = parseInt(eroam.convertCurrency(roomValue.rates[0].net, 'AUD'));
                                            //maxPrice =  roomPrice > maxPrice ? roomPrice : maxPrice;
                                            roomsAreAvailable = true;
                                        }
                                    });
                                }
                                if (roomsAreAvailable) // added by miguel to filter out hotels with no rooms
                                {
                                    value.provider = 'hb';
                                    is_selected = 'false';
                                    is_active = '';
                                    selected_attributes = '';
                                    if (value.provider == default_provider) {
                                        if (value.code == default_hotel) {
                                            is_selected = 'true';
                                            is_active = 'selected-hotel';
                                            var converted_price = eroam.convertCurrency(default_room_price, default_currency);
                                            value.priceString = globalCurrency + ' ' + Math.ceil(converted_price).toFixed(2);
                                            selected_attributes = 'data-price="' + value.priceString + '" data-price-id="' + default_price_id + '"';
                                            value.defaultPriceId = default_price_id;
                                            value.roomName = default_room_name;
                                        }
                                    }

                                    value.is_active = is_active;
                                    value.is_selected = is_selected;
                                    value.attributes = ' data-provider="hb" data-index="' + key + '" data-city-id="' + city_id + '"  data-hotel-zone-id="' + value.zoneCode + '" data-is-selected="' + value.is_selected + '" ' + selected_attributes;


                                    try {
                                        appendHotels(value);
                                    } catch (e) {
                                        console.log("hb append error : ", e.message);
                                    }
                                }

                            });
                            next();
                        } else {
                            next();
                        }
                    });

                });
            } else {
                next();
            }

        }

        function buildExpediaHotels(next) {
            var location = (search_session.itinerary[leg].city);
            var cityName = location.name;
            expedia_data = {
                city: cityName.replace(/\s/g, ''),
                countryCode: location.country.code,
                arrivalDate: from_date,
                departureDate: to_date,
                numberOfAdults: '{{$data["travellers"]}}', //number of traveller
                provider: 'expedia',
                customerSessionId: '<?php echo $customerSessionId;?>',
                leg: leg
            };


            // CACHING HOTEL BEDS
            var expediaApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', expedia_data, 'expedia', true);
            eroam.apiPromiseHandler(expediaApiCall, function(expediaResponse) {

                if (expediaResponse.data.length > 0) {
                    $.each(expediaResponse.data, function(key, value) {

                        var roomsAreAvailable = false; // added by miguel to filter out hotels with no rooms
                        if (value.rooms.length > 0) {
                            $.each(value.rooms, function(roomIndex, roomValue) {
                                if (typeof roomValue.rate != 'undefined') {
                                    roomPrice = parseInt(eroam.convertCurrency(roomValue.rate, 'AUD'));
                                    //maxPrice =  roomPrice > maxPrice ? roomPrice : maxPrice;
                                    roomsAreAvailable = true;
                                }
                            });
                        }

                        if (roomsAreAvailable) // added by miguel to filter out hotels with no rooms
                        {

                            value.provider = 'expedia';
                            is_selected = 'false';
                            is_active = '';
                            selected_attributes = '';
                            if (value.provider == default_provider) {
                                if (value.code == default_hotel) {
                                    is_selected = 'true';
                                    is_active = 'selected-hotel';
                                    var converted_price = eroam.convertCurrency(default_room_price, default_currency);
                                    value.priceString = globalCurrency + ' ' + Math.ceil(converted_price).toFixed(2);
                                    selected_attributes = 'data-price="' + value.priceString + '" data-price-id="' + default_price_id + '"';
                                    value.defaultPriceId = default_price_id;
                                    value.roomName = default_room_name;
                                }
                            }

                            value.is_active = is_active;
                            value.is_selected = is_selected;
                            value.attributes = ' data-provider="expedia" data-index="' + key + '" data-city-id="' + city_id + '"  data-hotel-name="' + value.name.toLowerCase() + '" data-hotel-id="' + value.hotelId + '" data-is-selected="' + value.is_selected + '" ' + selected_attributes;

                            try {
                                appendHotels(value);
                            } catch (e) {
                                console.log("expedia append error : ", e.message);
                            }
                        }

                    });
                    next();
                } else {
                    next();
                }
            });
        }

        function buildAEHotels(next) {

            search_session.itinerary[leg].city.ae_city_mapped.forEach(function(mapped) {

                var ae_data = {
                    FromDate: from_date,
                    ToDate: to_date,
                    Adults: '{{$data["travellers"]}}',
                    RegionId: mapped.ae_city.RegionId,
                    provider: 'ae',
                };
                var aeApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', ae_data, 'ae', true);
                eroam.apiPromiseHandler(aeApiCall, function(aeResponse) {

                    if (aeResponse !== null && aeResponse.length > 0) {

                        $.each(aeResponse, function(key, value) {

                            if (value.rooms.length > 0) {
                                $.each(value.rooms, function(roomIndex, roomValue) {
                                    if (typeof roomValue.price != 'undefined') {
                                        roomPrice = parseInt(eroam.convertCurrency(roomValue.price, roomValue.currency));
                                        //maxPrice =  roomPrice > maxPrice ? roomPrice : maxPrice;
                                        roomsAreAvailable = true;
                                    }

                                });

                            }
                            if (roomsAreAvailable) // 
                            {
                                value.provider = 'ae';
                                is_selected = 'false';
                                is_active = '';
                                selected_attributes = '';

                                if (value.provider == default_provider) {
                                    if (value.hotel_id == default_hotel) {
                                        is_selected = 'true';
                                        is_active = 'selected-hotel';
                                        var converted_price = eroam.convertCurrency(default_room_price, default_currency);
                                        value.priceString = globalCurrency + ' ' + converted_price;
                                        selected_attributes = 'data-price="' + value.priceString + '" data-price-id="' + default_price_id + '"';
                                        value.defaultPriceId = default_price_id;
                                        value.roomName = default_room_name;
                                    }
                                }

                                value.is_active = is_active;
                                value.is_selected = is_selected;
                                value.attributes = ' data-provider="ae" data-index="' + key + '" data-city-id="' + city_id + '" data-hotel-id="' + value.hotel_id + '" data-is-selected="' + value.is_selected + '" ' + selected_attributes;
                                try {
                                    appendHotels(value);
                                } catch (e) {
                                    console.log("ae append error : ", e.message);
                                }
                            }

                        });
                        next();
                    } else {
                        next();
                    }
                });

            });

        }

        function buildAotHotels(next) {
            var traveller_number = "{{ $data['travellers'] }}";
            var aot_room_type = 'single';
            switch (traveller_number) {
                case '1':
                    aot_room_type = 'single';
                    break;
                case '2':
                    aot_room_type = 'double';
                    break;
                case '3':
                    aot_room_type = 'triple';
                    break;
            }
            aot_data = {
                type: 'q_supplier',
                city_id: city_id,
                room_type: aot_room_type,
                provider: 'aot'
            };
            //jayson  update this after demo
            if (search_session.itinerary[leg].hotel != null) {
                var aotApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', aot_data, 'aot', true);
                eroam.apiPromiseHandler(aotApiCall, function(aotReponse) {
                    if (aotReponse.array !== undefined && aotReponse.array.length > 0) {

                        /*
                        | Loop hotel response
                        */
                        if (window.aotHotelsArray === undefined) window.aotHotelsArray = [];
                        window.aotHotelsArray = aotReponse.array;
                        if (window.aotHotels === undefined) window.aotHotels = {};
                        aotReponse.array.forEach(function(aotHotel, aotHotelIndex) {

                            aotHotel.provider = 'aot';
                            aotHotel.season_id = 0; //since there's no need for seasons in AOT
                            aotHotel.index = aotHotelIndex;

                            var code = aotHotel.code;
                            if (window.aotHotels === undefined) window.aotHotels = {};
                            window.aotHotels[code] = aotHotel;
                            is_selected = 'false';
                            is_active = '';
                            selected_attributes = '';
                            if (aotHotel.provider == default_provider) {
                                if (aotHotel.code == default_hotel) {
                                    is_selected = 'true';
                                    is_active = ' selected-hotel';
                                    var converted_price = eroam.convertCurrency(default_room_price, 'AUD');
                                    aotHotel.priceString = globalCurrency + ' ' + converted_price;
                                    selected_attributes = 'data-price="' + aotHotel.priceString + '" data-price-id="' + default_price_id + '"';
                                    aotHotel.defaultPriceId = default_price_id;
                                    aotHotel.roomName = default_room_name;
                                }
                            }

                            aotHotel.is_active = is_active;
                            aotHotel.is_selected = is_selected;
                            aotHotel.attributes = ' data-provider="aot" data-index="' + aotHotelIndex + '" data-city-id="' + city_id + '" data-hotel-id="' + aotHotel.code + '" data-is-selected="' + aotHotel.is_selected + '" ' + selected_attributes;

                            try {
                                appendHotels(aotHotel);
                            } catch (e) {
                                console.log("aot append error : ", e.message);
                            }
                        });
                        next();
                    } else {
                        next();
                    }

                });
            } else {
                next();
            }


        }

        function checked(checkbox, value) {
            $('.toggle-check').find('i').removeClass('black-background');
            checkbox.toggleClass('black-background');
            var text = value > 1 ? value + ' Nights' : value + ' Night';
            $('#number_of_nights').html(text + '<span class="caret"></span>');
        }

        function checkHotelsCount(next) {
            $('#hotel-loader').fadeOut(300);
            totalHotels = parseInt(totalHotels);
            $('div.hotel-list1').find('.noHotelFound').closest('h4').remove();
            $('.hotel-count').html('Top ' + totalHotels + ((totalHotels == 1) ? ' Property' : ' Properties') + ' Found');
            if (totalHotels == 0) {
                $('#suburbs-list').hide();
                var html = '<h4 class="text-center blue-txt bold-txt noHotelFound">No Properties Available.</h4>';
                $('.hotel-list1').append(html);
            }
            next();
        }

        function isHotelAppended(name) {
            var keyContainer;
            found = hotels_appended.some(function(value, key) {

                if (value.name == name) {
                    keyContainer = key;
                    return key;
                } else {
                    return false;
                }
            });
            return found ? keyContainer : found;
        }


        function getHotelFromPrice(hotel) {
            var stringFromPrice = '';
            var lowest_price_room_type = '';
            var converted_price = 0.00;
            var lowest_price = 0.00;
            switch (hotel.provider) {
                case 'eroam':

                    rooms = jQuery.parseJSON(eroamRooms[hotel.id]);
                    if (rooms.length > 0) {
                        var lowest_price = 0;
                        rooms.map(function(value, key) {
                            var value_price = parseFloat(value.price);
                            // lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
                            // lowest_price_room_type = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value.room_type.name : lowest_price_room_type;
                            if (value.hotel_season_id == hotel.season_id) {
                                if (lowest_price == 0) {
                                    lowest_price = value_price;
                                    lowest_price_room_type = value.room_type.name;
                                } else {
                                    if (lowest_price > value_price) {
                                        lowest_price = value_price;
                                        lowest_price_room_type = value.room_type.name;
                                    }
                                }
                            }

                        });
                    }
                    if (lowest_price > 0) {

                        if (!lowest_price_room_type.includes('Dorm')) {
                            lowest_price = lowest_price / number_of_travellers;
                        }

                        converted_price = eroam.convertCurrency(lowest_price, hotel.currency.code);
                    }
                    break;
                case 'aot':
                    break;
                case 'hb':
                    if (hotel.rooms.length > 0) {
                        var lowest_price = 0;
                        hotel.rooms.map(function(value, key) {
                            if (typeof value.rates[0].net != 'undefined') {
                                var value_price = parseFloat(value.rates[0].net);
                                //lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
                                //lowest_price_room_type = 
                                if (lowest_price == 0) {
                                    lowest_price = value_price;
                                    lowest_price_room_type = value.name;
                                } else {
                                    if (lowest_price > value_price) {
                                        lowest_price = value_price;
                                        lowest_price_room_type = value.name;
                                    } else {
                                        lowest_price = lowest_price;
                                        lowest_price_room_type = lowest_price_room_type;
                                    }
                                }

                            }
                        })
                    }
                    if (lowest_price > 0) {
                        lowest_price = lowest_price / number_of_travellers;
                        converted_price = eroam.convertCurrency(lowest_price, 'AUD');
                    }
                    break;

                case 'expedia':
                    if (hotel.rooms.length > 0) {
                        var lowest_price = 0;
                        hotel.rooms.map(function(value, key) {

                            if (typeof value.rate != 'undefined') {
                                var value_price = parseFloat(value.rate);
                                //lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
                                //lowest_price_room_type =
                                if (lowest_price == 0) {
                                    lowest_price = value_price;
                                    lowest_price_room_type = value.roomDescription;
                                } else {
                                    if (lowest_price > value_price) {
                                        lowest_price = value_price;
                                        lowest_price_room_type = value.roomDescription;
                                    } else {
                                        lowest_price = lowest_price;
                                        lowest_price_room_type = lowest_price_room_type;
                                    }
                                }

                            }
                        })
                    }
                    if (lowest_price > 0) {
                        // lowest_price = ((lowest_price * 20 / 100) + lowest_price);
                        lowest_price = lowest_price;
                        converted_price = eroam.convertCurrency(lowest_price, 'AUD');
                    }
                    break;

                case 'ae':
                    if (hotel.rooms.length > 0) {
                        var lowest_price = 0;
                        hotel.rooms.map(function(value, key) {
                            if (typeof value.price != 'undefined') {
                                var value_price = parseFloat(value.price);
                                //lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
                                if (lowest_price == 0) {
                                    lowest_price = value_price;
                                    lowest_price_room_type = value.name;
                                } else {
                                    if (lowest_price > value_price) {
                                        lowest_price = value_price;
                                        lowest_price_room_type = value.name;
                                    } else {
                                        lowest_price = lowest_price;
                                        lowest_price_room_type = lowest_price_room_type;
                                    }
                                }
                            }
                        });
                    }
                    if (lowest_price > 0) {
                        lowest_price = lowest_price / number_of_travellers;
                        converted_price = eroam.convertCurrency(lowest_price, 'AUD');
                    }
                    break;
            }

            stringFromPrice = globalCurrency + ' ' + Math.ceil(converted_price).toFixed(2);

            var roomData = {
                fromPrice: stringFromPrice,
                fromOriginalPrice: lowest_price,
                roomName: lowest_price_room_type
            }
            //return stringFromPrice;
            return roomData;
        }

        function sortByDate(activities) {
            var sorted = activities.sort(function(itemA, itemB) {
                var A = itemA.date_selected;
                var B = itemB.date_selected;
                return A.localeCompare(B);
            });
            return sorted;
        }

        function numberRange(start, count) {
            return Array.apply(0, Array(count))
                .map(function(value, key) {
                    return key + start;
                });
        }

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + parseInt(days));
            return formatDate(result);
        }

        function deductDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() - parseInt(days));
            return formatDate(result);
        }

        function formatTime(time) {
            return time.replace(/hr\(s\)|min\(s\)|hours|minutes /gi, function(x) {
                return x == 'hr(s)' || 'hours' ? ':' : '';
            });
        }

        function removePlus(time) {
            return time.replace(/\+/g, '');
        }

        function get_hours_min(hour_min) {
            var hours_mins = hour_min.toString().split(".");
            var hours = parseInt(hours_mins[0]);
            var mins = parseFloat('0.' + hours_mins[1]) * 60;

            return hours + ' hr(s) ' + Math.ceil(mins) + ' min(s)';
        }

        function arrival_am_pm(eta) {

            var result;
            var arrival_split = eta.split("+");
            var arrival_split_am_pm = arrival_split[0].split(" ");
            var arrival_split_hour_min = arrival_split[0].split(":");
            var arrival_hour = parseInt(arrival_split_hour_min[0]);
            var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


            if (typeof arrival_split_am_pm[1] !== 'undefined') {

                result = padd_zero(arrival_hour) + ':' + arrival_min + ' ' + arrival_split_am_pm[1];
            } else {
                if (arrival_hour < 12) {
                    result = padd_zero(arrival_hour) + ':' + arrival_min + ' AM';
                } else {
                    hour = arrival_hour - 12;
                    result = padd_zero(hour) + ':' + arrival_min + ' PM';
                }
            }

            return result;
        }

        function padd_zero(number) {
            if (parseInt(number) == 0) {
                number = 12;
            }
            return (number < 10) ? ("0" + number) : number;
        }

        function getDayTime(arrivalDate, duration) {
            var hours_mins = duration.split(':');
            var result = new Date(arrivalDate);
            result.setHours(result.getHours() - parseInt(hours_mins[0]));
            result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));

            return formatDate(result, true);
        }

        function activityCancellation(activityDates, itineraryIndex, act, checkbox, value) {

            if (activityDates.length > 0) {

                eroam.confirm(null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e) {

                    if (act.length > 0) {
                        for (index = act.length - 1; index >= 0; index--) {
                            var actDuration = parseInt(act[index].duration);
                            var multiDayRemoved = false;
                            if (actDuration > 1) {
                                actDuration = actDuration - 1;
                                var activityDateSelected = act[index].date_selected;
                                for (var counter = 1; counter <= actDuration; counter++) {
                                    var multiDayActivityDate = addDays(activityDateSelected, counter);

                                    if (activityDates.indexOf(multiDayActivityDate) > -1 && !multiDayRemoved) {
                                        act.splice(index, 1);
                                        multiDayRemoved = true;
                                    }
                                }
                            } else {
                                if (activityDates.indexOf(act[index].date_selected) > -1) {
                                    act.splice(index, 1);
                                }
                            }

                        }
                    }

                    search_session.itinerary[itineraryIndex].activities = act;
                    bookingSummary.update(JSON.stringify(search_session));
                    checked(checkbox, value);

                });

            } else {
                bookingSummary.update(JSON.stringify(search_session));
                checked(checkbox, value);
            }

        }

        function getDeparture(itineraryIndex) {

            var newDateTo = addDays(from_date, parseInt(search_session.itinerary[itineraryIndex].city.default_nights));
            var transport = search_session.itinerary[itineraryIndex].transport;

            var departureDate = to_date + ' 24';
            if (transport != null) {
                var eta = transport.eta;
                var travelDuration = transport.duration;
                if (transport.provider == 'mystifly') {

                    travelDuration = formatTime(get_hours_min(travelDuration));
                    eta = moment(eta, moment.ISO_8601);
                    eta = eta.format('hh:mm A');

                } else {
                    travelDuration = removePlus(travelDuration);
                    travelDuration = formatTime(travelDuration);
                }

                var arrivalTime = arrival_am_pm(eta);
                departureDate = getDayTime(newDateTo + ' ' + arrivalTime, travelDuration);
            }

            return departureDate;
        }

        function getActivityToBeCancelledByDates(dateTo, numberOfDays) {
            var dateArray = [];
            if (numberOfDays) {
                dateArray.push(dateTo);
                for (var count = 1; count <= numberOfDays; count++) {
                    dateArray.push(deductDays(dateTo, count))
                }
            }
            return dateArray;
        }

        function getHBDestinationCodes(hb_destinations_mapped) {
            result = [];
            if (hb_destinations_mapped != null) {
                hb_destinations_mapped.forEach(function(value, key) {
                    result.push(value.hb_destination.destination_code.trim());
                });
            }
            return result;
        }

        /*
        | Added by Junfel
        | jquery Ui range slider
        */
        function rangeSlider(next) {
            var min = parseInt(eroam.convertCurrency(0, 'AUD'));
            var max = parseInt(maxPrice);

            $('#slider-range').slider({
                range: true,
                min: min,
                max: max,
                values: [min, max],
                slide: function(event, ui) {
                    $('#price-from').val(ui.values[0]);
                    $('#price-to').val(ui.values[1]);
                }
            });
            //$( '#price-from' ).val($( '#slider-range' ).slider( 'values', 0 ));
            //$( '#price-to' ).val($( '#slider-range' ).slider( 'values', 1 ));

            $('#price-from').val(0);
            $('#price-to').val(5000);

            $('.curr').text(globalCurrency);

            next();
        }

        function getHotelCategory(code, provider = '') {
            var stars;
            switch (provider) {
                case 'hb':
                    switch (code) {
                        case '5EST':
                        case 'HS5':
                        case 'APTH5':
                        case '5LUX':
                        case '5LL':
                            stars = getStars(5);
                            break;
                        case '4EST':
                        case 'HS4':
                        case 'APTH4':
                        case '4LUX':
                        case '4LL':
                            stars = getStars(4);
                            break;
                        case '3EST':
                        case 'HS3':
                        case 'APTH3':
                        case '3LUX':
                        case '3LL':
                            stars = getStars(3);
                            break;
                        case '2EST':
                        case 'HS2':
                        case 'APTH2':
                        case '2LUX':
                        case '2LL':
                            stars = getStars(2);
                            break;
                        case '1EST':
                        case 'HS1':
                        case 'APTH1':
                        case '1LUX':
                        case '1LL':
                            stars = getStars(1);
                            break;
                        case 'H4_5':
                            stars = getStars(4, true);
                            break;
                        case 'H3_5':
                            stars = getStars(3, true);
                            break;
                        case 'H2_5':
                            stars = getStars(2, true);
                            break;
                        case 'H1_5':
                            stars = getStars(1, true);
                            break;
                        default:
                            stars = getStars(0);
                            break;

                    }
                    break;
                case 'eroam':
                    stars = getStars(code);
                    break;
                case 'ae':
                    break;
                case 'aot':
                    break;
                case 'expedia':
                    if (code % 1 === 0) {
                        stars = getStars(code);
                    } else {
                        stars = getStars(code, true);
                    }
                    break;
            }
            return stars;
        }

        function getStars(count, half = false) {
            var stars = '';
            if (parseInt(count)) {
                for (star = 1; star <= count; star++) {
                    stars += '<span class="d-inline-block"><i class="fa fa-star"></i></span>';
                }
                var emptyStars = 5 - parseInt(count);
                if (half) {
                    stars += '<span class="d-inline-block"><i class="fa fa-star-half-o"></i></span>';
                    emptyStars = emptyStars - 1;
                }
                for (empty = 1; empty <= emptyStars; empty++) {
                    stars += '<span class="d-inline-block"><i class="fa fa-star-o"></i></span>';
                }
            } else {
                stars += '<span class="d-inline-block"><i class="fa-star-o"></i></span>';
            }
            return stars;
        }

        function getAERegionIds(AECitiesMapped) {
            var results = [];
            AECitiesMapped.forEach(function(AEMappedCity) {
                results.push(AEMappedCity.ae_city.RegionId);
            });
            return results;
        }

        function calculateHeight() {

            var winHeight = $(window).height();
            var oheight = $('.page-sidebar').outerHeight();
            var elem = $('.page-content .tabs-container').outerHeight();
            var elemHeight = oheight - elem;
            var winelemHeight = winHeight - elem;
            if (winHeight < oheight) {
                $(".page-content .tabs-content-container").outerHeight(elemHeight);
            } else {
                $(".page-content .tabs-content-container").outerHeight(winelemHeight);
            }
        }


        function getPropertyCategory(id) {
            var return_property = '';
            if (id == 1) {
                return_property = 'Hotel';
            } else if (id == 2) {
                return_property = 'Suite';
            } else if (id == 3) {
                return_property = 'Resort';
            } else if (id == 4) {
                return_property = 'Vacation rental/condo';
            } else if (id == 5) {
                return_property = 'Bed & Breakfast';
            } else if (id == 6) {
                return_property = 'All-inclusive';
            }
            return return_property;
        }

        function getDistinctProperty() {
            var items = {};
            $('div.accomodation-list').each(function() {
                //alert($(this).attr('data-property-type'));
                items[$(this).attr('data-property-type')] = true;
            });
            var html = '';
            var result = new Array();
            for (var i in items) {
                if (i != 'N/A') {
                    result.push(i);
                    html += '<li><a id="searchProperty" class="dropdown-item">' + i + '</a></li>';
                }
            }
            $('#property-tab').append(html);
        }

        function filterListProperty(value) {
            var list = $("div.hotel-list1 div.hotel-list-sort");
            $(list).fadeOut("fast");
            var totalCount = 0;
            if (value == "All") {
                $("div.hotel-list1").find("div.hotel-list-sort").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
            } else { //alert(1);
                //Notice this *=" <- This means that if the data-category contains multiple options, it will find them
                //Ex: data-category="Cat1, Cat2"
                $("div.hotel-list1").find("div.hotel-list-sort[data-property-type*='" + value + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                var list = $("div.hotel-list2 div.hotel-grid-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                $("div.hotel-list2").find("div.hotel-grid-sort[data-property-type*='" + value + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
            }
            checkPropertyCountForSearch(totalCount);
        }

        function getHotelSearch() {
            var search_val = $('#search_val').val();
            if (search_val) {
                search_val = search_val.toLowerCase();
                var list = $("div.hotel-list1 div.hotel-list-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                $("div.hotel-list1").find("div.hotel-list-sort[data-hotel-name*='" + search_val + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                var list = $("div.hotel-list2 div.hotel-grid-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                $("div.hotel-list2").find("div.hotel-grid-sort[data-hotel-name*='" + search_val + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                checkPropertyCountForSearch(totalCount);
            } else {
                var list = $("div.hotel-list1 div.hotel-list-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                search_val = 'expedia';
                $("div.hotel-list1").find("div.hotel-list-sort[data-provider*='" + search_val + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                var list = $("div.hotel-list2 div.hotel-grid-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                $("div.hotel-list2").find("div.hotel-grid-sort[data-provider*='" + search_val + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                checkPropertyCountForSearch(totalCount);
                $('#loadMore').show();
            }
        }

        $('#search_val').keyup(function() {
            if ($(this).val() == '') {
                var list = $("div.hotel-list1 div.hotel-list-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                search_val = 'expedia';
                $("div.hotel-list1").find("div.hotel-list-sort[data-provider*='" + search_val + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                var list = $("div.hotel-list2 div.hotel-grid-sort");
                $(list).fadeOut("fast");
                var totalCount = 0;
                $("div.hotel-list2").find("div.hotel-grid-sort[data-provider*='" + search_val + "']").each(function(i) {
                    totalCount++;
                    $(this).delay(200).slideDown("fast");
                });
                checkPropertyCountForSearch(totalCount);
                $('#loadMore').show();
            }
        });

        function checkPropertyCountForSearch(totalPropertyCount) {
            $('div.hotel-list1').find('.noHotelFound').closest('h4').remove();
            $('.hotel-count').html('Top ' + totalPropertyCount + ((totalPropertyCount == 1 || totalPropertyCount == 0) ? ' Property' : ' Properties') + ' Available');
            if (totalPropertyCount == 0) {
                var html = '<h4 class="text-center blue-txt bold-txt noHotelFound">No Properties Available.</h4>';
                $('div.hotel-list1').append(html);
            }
            //next();
        }
		
        $('#listView').click(function(e) {
            $(this).addClass('active');
            $('#gridView').removeClass('active');
            e.preventDefault();
            $('.hotel-list1').show();
            $('.hotel-list-selected').show();
            $('.hotel-list2').hide();
            $('.hotel-grid-selected').hide();
        });

        $('#gridView').click(function(e) {
            $(this).addClass('active');
            $('#listView').removeClass('active');
            e.preventDefault();
            $('.hotel-list2').show();
            $('.hotel-grid-selected').show();
            $('.hotel-list1').hide();
            $('.hotel-list-selected').hide();
        });

        $('.itinerary_page').slimScroll({
            height: '800px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
    </script>
@endpush