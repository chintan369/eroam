<!-- START ALERT -->
<div class="modal fade no-hotel-summary" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <p>Sorry, you can't add additional hotel nights because there's no hotel selected.</p>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6 pull-right">
            <button type="button" class="btn btn-primary confirm-cancel-button no-hotel-cancel-btn" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div><!-- /.modal -->
