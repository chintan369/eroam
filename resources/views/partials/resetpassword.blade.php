<div id="onForgotModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer">
                <div class="modal-header pb-0">
                    <h4 class="text-center">Reset Password</h4>
                </div>
                 @if(session()->has('success'))
                <div class="alert alert-success" role="alert">
                   {{ session()->get('success') }}
                </div>
                @endif
                <div class="modalform pt-2">
                    <form id="reset-form" action="/reset-password" method="post">
                         @csrf
                         <input type="hidden" name="code" value="{{ $response['code'] }}">
                         <input type="hidden" name="token" value="{{ $data['token'] }}">
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Password</label>
                                <input type="password" id="password" name="password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>Confirm Password</label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                            </div>
                        </div>
                        @if ( count( $errors->reset ) )
                        	<p id="error-msg">
                        		{{ $errors->reset->first() }}
                        	</p>
                        @endif
                        @if(session()->has('error'))
                        	<p id="error-msg">{{ session()->get('error') }}</p>
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="submit" name="" class="btn btn-black btn-block" value="Submit" id="login-btn">
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---END Signup Modal--->