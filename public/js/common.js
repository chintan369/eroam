$(document).ready(function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
});
$(".datepicker").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    startDate: '-0d'
});
$(".datepicker1").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
});

 $('.open-datepicker').click(function(){
       $(document).ready(function(){
           $(".datepicker").datepicker().focus();
           $(".datepicker1").datepicker().focus();
       });
   });

$.validator.addMethod("verifyEmail",
    function(value, element) {
        var result = false;
        var token = $('#token').val();
        $.ajax({
            type:"POST",
            async: false,
            url: "/email/validate", // script to validate in server side
            data: {"reg_email": value,'_token':token},
            success: function(data) {
                var final_result = '';
                if(data.success == 1)
                {
                    if(data.social == 1)
                    {
                        final_result = false;
                    }
                    else
                    {
                        final_result = true;    
                    } 
                }
                else
                {
                    final_result = false;
                }
                result = final_result;
            }
        });
        // return true if username is exist in database
        return result;
        alert("RESULT "+result);
    },
    
);

$.validator.addMethod("checkEmail",
    function(value, element) {
        var result = false;
        var token = $('#token').val();
        $.ajax({
            type:"POST",
            async: false,
            url: "/email/validate", // script to validate in server side
            data: {"reg_email": value,"_token":token},
            success: function(data) {
                var final_result = '';
                if(data.success == 1)
                {
                    if(data.social == 1)
                    {
                        final_result = true;
                    }
                    else
                    {
                        final_result = false;    
                    } 
                }
                else
                {
                    final_result = true;
                }
                result = final_result;
            }
        });
        // return true if username is exist in database
        return result;
        alert("RESULT "+result);
    },
    
);

$.validator.addMethod("pwcheck", function(value) {
    return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/.test(value) 
}, "Password must contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:");

$("#signup_form_model").validate({
        rules: {           
            reg_email: {
                required: true,
                email:true,
                verifyEmail : true
            },
            reg_pass: {
                required: true,
                minlength: 8,    
                pwcheck : true, 
            }
        },        
        messages: {
            reg_email: {
                verifyEmail: "Email already exists"
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            var registerForm = $("#signup_form_model");
            var formData = registerForm.serialize();
            $( '#email-error' ).html( "" );
            $( '#password-error' ).html( "" );

            $.ajax({
                url:'/signup',
                type:'POST',
                data:formData,
                beforeSend: function() {
                    $('input#submitSignupForm').val('Please Wait..');
                    $('input#submitSignupForm').prop('type', 'button');
                },
                success:function(data) { 
                    $('input#submitSignupForm').val('Submit');
                    $('input#submitSignupForm').prop('type', 'submit');
                    if(data.errors) {                    
                        if(data.errors.email){
                            $( '#email-error' ).html( data.errors.email );
                        }
                        if(data.errors.password){
                            $( '#password-error' ).html( data.errors.password );
                        }                    
                    }
                    if(data.success) {
                        $(".register_message").show();
                        $("#reg_email").val('');
                        $("#reg_pass").val('');
                        $(".register_message").html('Thank you for Registration. Please confirm your mail from your email Id.');                        
                    }
                },
            });
        }
    });
   
$("#login_form").validate({
        rules: {           
            username_login: {
                required: true,
                email:true
            },
            password_login: {
                required: true,
            }
        },
        errorPlacement: function (error, element) {  
        	element.after(error);
        },
        submitHandler: function (form) {
            //$("submitLoginForm").value('Loading..');
            $('input.submitLoginForm').val('Loading');
            var postData = $("#login_form").serialize();
                eroam.ajax('post', 'login', postData, function(response) {
                    $('#login-btn').html('Loading...');
                    if (response.trim() == 'valid') {
                        $('#login-error-msg').hide();
                        $('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');                        
                        window.location.href = '/profile/step1';
                    } 
                    else if(response.trim() == 'confirm_first')
                    {
                        $('#login-error-message').text('Please Confirm your mail first.').show();
                        $('#login-btn').html('Log In');
                    }
                    else {
                        $('#login-error-message').text('Invalid Email Address or Password').show();
                        $('#login-btn').html('Log In');
                    }
                }, function() {
                    $('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
                });
        }
    });

    $("#forgot_form").validate({
        rules: {           
            user_email: {
                required: true,
                email:true,
                checkEmail : true
            },
        },        
        messages: {
            user_email: {
                checkEmail: "We Could Not Find Your Email Address."
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            
            var forgotForm = $("#forgot_form");
            var formData = forgotForm.serialize();
       
            $.ajax({
                url:'/forgot-password',
                type:'POST',
                data:formData,
                beforeSend: function() {
                    $('input#submitForgotForm').val('Please Wait..');
                    $('input#submitForgotForm').prop('type', 'button');
                },
                success:function(data) { 
                    $('input#submitForgotForm').val('Continue');
                    $('input#submitForgotForm').prop('type', 'submit');
                    //$.unblockUI();
                    if(data.errors) 
                    {                    
                        if(data.errors.email){
                            $( '#email-error' ).html( data.errors.email );
                        }
                        if(data.errors.password){
                            $( '#password-error' ).html( data.errors.password );
                        }                    
                    }
                    if(data.success) {
                        $(".forgot_message").show();
                        $("#user_email").val('');
                        $(".forgot_message").html('Check your mail for recover password link.');
                    }
                },
            });
        }
    });

    $("#login_form2").validate({

        rules: {
            username_login: {
                required: true,
                email:true
            },
            password_login: {
                required: true,
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            
            $('input.submitLoginForm').val('Loading');
            var postData = $("#login_form2").serialize();
                eroam.ajax('post', 'login', postData, function(response) {
                    $('#login-btn2').html('Loading...');
                    if (response.trim() == 'valid') {
                        $('#login-error-msg2').hide();
                        $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
                        window.location.href = '/review-itinerary/';
                    }
                    else if(response.trim() == 'confirm_first')
                    {
                        $('#login-error-message2').text('Please Confirm your mail first.').show();
                        $('#login-btn2').html('Log In');
                    }
                    else {
                        $('#login-error-message2').text('Invalid Email Address or Password').show();
                        $('#login-btn2').html('Log In');
                    }
                }, function() {
                    $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
                });
            }
    });