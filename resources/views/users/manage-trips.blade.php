@extends('layouts.common')
@section('content')
    @include('partials.banner')
    @include('partials.search')
    <div class="account-block">
        @include('users.partials.sidebar')
        <div class="account-right p-4 pl-5">
        @include('users.partials.complete_profile')
            <hr class="mt-5">
            <h5 class="mt-4">Manage Trips</h5>
            <div class="mt-5 row">
            @if(count($all_trips) > 0)
                @foreach($all_trips as $trip)
                    <div class="col-xl-4 col-sm-6 mb-4">
                        <a href="{{ url('view/trips/'.$trip['order_id']) }}">
                            <div class="card panel border-0">
                                <div class="trip-container panel py-3">
                                    <div class="tripImage text-center mb-1">
                                        <img src="{{ url('images/trip-image.png')}}" alt="" class="img-fluid">
                                    </div>
                                    @php 
                                    $from_city_info = get_city_by_id($trip['from_city_id']);
                                    $to_city_info   = get_city_by_id($trip['to_city_id']);
                                    $from_country   = get_country_by_country_name($from_city_info['country_name']);
                                    $to_country     = get_country_by_country_name($to_city_info['country_name']);
                                    @endphp
                                    <div class="row text-center">
                                        <div class="col-sm-5">
                                        <h3><strong>{{$from_city_info['airport_codes']}}</strong></h3>
                                        <small>{{$trip['from_city_name']}},</small>
                                        </div>
                                        <div class="col-sm-2">
                                        <strong>-</strong>
                                        </div>
                                        <div class="col-sm-5">
                                        <h3><strong>{{$to_city_info['airport_codes']}}</strong></h3>
                                        <small>{{$trip['to_city_name']}},</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="tripBooking p-3 text-center">
                                    <h6 class="text-truncate"><strong>Multi-City Tailormade Auto</strong></h6>
                                    <p><small>Booking ID: {{$trip['invoice_no']}}</small></p>
                                    <ul class="list-unstyled mb-0">
                                        <li class="list-inline-item"><a href="#"><i class="ic-local_hotel"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ic-local_activity"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ic-flight"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="ic-directions_bus"></i></a></li>
                                    </ul>
                                </div>
                                <hr class="m-0">
                                <div class="traveller-details p-3">
                                    <div class="row">
                                        <div class="col-sm-4"><strong>Travellers:</strong></div>
                                        <div class="col-sm-8">
										
                                            @foreach($trip['passenger_information'] as $passenger_info)
                                                {{ $passenger_info['first_name']." ".$passenger_info['last_name'] }}<br>
                                            @endforeach
											
                                        </div>
                                    </div>
                                    <div class="row mt-1">
                                        <div class="col-sm-4"><strong>Itinerary:</strong></div>
                                        <div class="col-sm-8">
										<p class="itinerary_address">
                                            @php $itinery = str_replace(",",' - ',$trip['from_city_to_city']); @endphp
                                                {{ $itinery }}
												</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3 pt-3 px-3 pb-2 travellerPrice-box">
                                    <h5><strong>{{$trip['currency'] }}$ {{ $trip['cost_per_day'] }} Per Day</strong></h5>
                                    <p>Total Cost <strong>${{ $trip['currency'] }} {{ $trip['total_per_person'] }}</strong><br/>(Per Person) Inc. Taxes</p>
                                </div>
                                <div class="barcode-box p-3 position-relative text-center">
                                    <p><center>{{ date( 'j M Y', strtotime($trip['from_date']))}} - {{ date( 'j M Y',strtotime($trip['to_date']))}}</center></p>
                                </div>
                            </div>
                        </a>
                    </div>
                 @endforeach 
            @else
                <div class="col-lg-12 col-md-12 col-sm-12 m-t-10">
                    <h3>No Tours Found</h3>
                </div>
            @endif  
            </div>
        </div>
    </div>
@endsection