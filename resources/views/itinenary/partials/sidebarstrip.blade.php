<div class="overlay_div d-none" onclick="closeNav()"> </div>
<div class="edit-map routesidenav" id="myroutSidenav" style="display: none;">
    <div class="outerrout">
        <div class="pt-2 pb-1 text-right"><a href="#" onclick="closeNav('myroutSidenav')" class="closebtn"><i class=" ic-clear"></i> </a> </div>
        <div class="root_title pt-4">
            <i class="fa fa-map"></i> Current Route
        </div>
        <div class="current-route pt-2">
            @isset($current_route)
            @foreach ( $current_route as $city )
				<span>{{ is_object($city) ? $city->name : $city['name'] }}</span>
				@if ( last( $current_route ) != $city )
					<i class="fa fa-long-arrow-right"></i>
				@endif
			@endforeach
            @endisset
        </div>
		
        @isset($alternative_routes)
            @if(count($alternative_routes) > 0 )
                <div class="root_title pt-4 pb-1">
                    Alternative {{ sing( count( $alternative_routes ), 'Routes', false ) }}
                </div>
                @foreach ( $alternative_routes as $key => $value )
                    <div class="route-container" data-key="{{ $key }}">
                        <p>{{ $value['duration'] }} {{ $value['price_per_person'] }} (Per Person)</p>
                        @foreach ( $value['cities'] as $city )
                            <span>{{ $city['name'] }}</span>
                            @if ( last( $value['cities'] ) != $city )
                                <i class="fa fa-long-arrow-right"></i>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            @endif
        @endisset
        <div class="root_title pt-2">
             <i class="fa fa-gear"></i>  Custom Route
        </div>
        <p class="pt-2 ">If you want to set your own route, you can do this by switching to manual mode.</p>
        <small>Note: Once you have switched to manual mode, you can't switch back to auto mode.</small>
        <div class="text-center"> 
            <a href="#" class="btn btns_input_white transform d-block mt-5 w-100 switch-manual-btn"> <i class="fa fa-exchange"></i> &nbsp; Switch to Manual Mode</a>
        </div>
    </div>
</div>

<div class="edit-map routesidenav" id="myroutSidenavtwo" style="display: none;">
    <div class="outerrout">
        <div class="pt-2 pb-1 text-right"><a href="#" onclick="closeNav('myroutSidenavtwo')" class="closebtn"><i class=" ic-clear"></i> </a> </div>
        <div class="root_title pt-4">
            <i class="ic-place"></i> Adding destinations
        </div>
        <p>To add a destination to your itinerary, simple click on the blue dots found on the map.</p>
        <img src="{{url('images/map-dot.png')}}" alt="" />
        <div class="root_title pt-2">
            <i class="ic-local_hotel"></i> Removing a destination / Adding nights
        </div>
        <p>To remove a destination, click on the blue marker of the destination you want to remove and click "REMOVE DESTINATION" button. Click on the "+" and "-" buttons to adjust the number of nights you want for that destination.</p>
        <img src="{{url('images/map-nights.png')}}" alt="" />
        <div class="root_title pt-2">
            Round Trips
        </div>
        <p>If you want to include a round-trip to your itinerary, simple click on the blue marker of the starting location and click the "Round-trip" button.</p>
    </div>
</div>

<div class="right_form_click_block">
    <ul class="map_top_link list-unstyled">
    	@php
    	$currentPath= Route::getFacadeRoot()->current()->uri();
    	@endphp
        @if($currentPath == 'map')
    		@if (session()->get( 'map_data' )['type'] == 'auto')
                <li><a class="d-block text-center" href="" onclick="openNav('myroutSidenav', event)"><i class=" ic-edit_block"></i></a></li>
            @else
                <li><a class="d-block text-center" href="" onclick="openNav('myroutSidenavtwo', event)"><i class=" ic-circle_qustion"></i></a></li>
            @endif
        @elseif($currentPath != 'map')
            <li class="active"><a class="d-block text-center" href="{{url('map')}}"><i class=" ic-glob_map"></i></a></li>
        @endif
    </ul>
    <a href="javascript:void();" class="d-block click_map_hide text-center pt-3 pb-3"><i class="ic-navigate_before"></i></a>
</div>