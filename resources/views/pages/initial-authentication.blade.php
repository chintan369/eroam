@extends('layouts.common')
@section('content')
        <div class="body_sec">
            <div class="blue modal_outer_man">
                @include('partials.banner')
                @include('partials.search')
            </div>
            
        </div>
 <!-- Modal for Autentication-->
	<div id="onLoadLoginModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-lg middbox">
			<div class="modal-content modalbox">
				<div class="modal_outer" style="width: 51%;margin:10% 0 0 25%;">
					<div class="modal-header pb-0">
						<h4 class="text-center">Authentication</h4>
					</div>
					<div class="modalform pt-2">
						<form class="signup_forms" id="auth-form">
						{{ csrf_field() }}
						<div  class="signup_forms">
							<div class="form-group">
								<div class="fildes_outer">
									<label for="username">Username</label>
									<input type="text"  name="username" id="username" class="form-control" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<div class="fildes_outer">
									<label for="password">Password</label>
									<input type="password" name="password" id="password" class="form-control" placeholder="">
								</div>
							</div>
							<div class="notification text-center row"></div>
							<div class="input-field" style="position: relative;margin-top: 1.5rem;margin-bottom: 12px;">
								<span class="error-message"></span>
							</div>
							<div class="form-group">
								<input class="sign_btn" id="authenticate-btn" type="submit" value="Login">
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Modal Closed for Authentication-->
	<script>
		$(document).ready(function(){
			$('#onLoadLoginModal').modal('show');

			$("#auth-form").validate({
				rules: {
					username: {
						required: true
					},
					password: {
						required: true
					},
				},

				errorPlacement: function (label, element) {
					label.insertAfter(element);
				},
				submitHandler: function (form) {

					authentication();

				}
			});

			function authentication() {
				//e.preventDefault();

				var data = $('#auth-form').serializeArray();
				
				if (validateData(data)) {
					$.ajax({
						url: "{{url('/validate-auth-credentials')}}",
						method: 'POST',
						data: validateData(data),
						beforeSend: function () {
							$('.error-message').hide();
							$('.notification').css({'color': 'rgb(0, 0, 0)','background': 'none'}).html('<img src='+"{{url('/images/loader.gif')}}"+' style="    margin-left: 45%;height: 50%;width: 10%;">').show();
						},
						success: function (response) {
							if (response.trim() == 1) {
								window.location.href = "{{url('/')}}";
							}else {
								$('input[name="username"]').val('');
								$('input[name="password"]').val('');
								$('.notification').hide();
								$('.error-message').css({color: 'red'}).html('Invalid Username or Password').show();
							}
						},
						error: function () {
							$('input[name="username"]').val('');
							$('input[name="password"]').val('');
							$('.notification').hide();
							$('.error-message').css({color: 'red'}).html('An error has occured. Please try again.').show();
						}
					});
				}
				else {
					$('.notification').hide();
				}
			}
		});

		function validateData( data ) {
			var result = true;
			var values = {};
			try {
				data.forEach(function( d )
				{
					if( typeof d.value !== 'undefined' )
					{
						if( d.value.trim() == '' )
						{
							result = false;
						}
					}
					else
					{
						result = false;
					}
					values[d.name] = d.value;
				});

				if( result === true )
				{
					result = values;
				}
			} 
			catch( err )
			{
				console.log( 'An error has occured while validating the input fields', err );
				result = false;
			}
			return result;
		}
	</script>
        @include('home.partials.custom-js')

@endsection
