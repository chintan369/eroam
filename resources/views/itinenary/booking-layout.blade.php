@extends('layouts.common')
@section('content')
    <div class="body_sec">
        <div class="itinerary_block account-block">
            <input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
            <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
            <div class="booking-summary">
                <span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>
            </div>
            @yield('booking-content')
        </div>
    </div>
@endsection
             
