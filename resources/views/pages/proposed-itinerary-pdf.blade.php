<!DOCTYPE html>
<html>
<head>
	<title>Voucher</title>
	<style>


		html, body {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			letter-spacing: -1px;
			height: 100%;
			color: #212121;
		}
		div.container {
			width: 100%;
			font-family: Arial !important;
		}
		div.date{
			font-size: smaller;
			float: right;
			text-align: right;
			font-weight: bold;
		}
		.logo{
			width:150px;
			float: left;
		}
		.header{
			background-color: white;
			clear: left;
			font-size: smaller;
			padding: 1em;
		}
		footer {
			padding: 1em;
			background-color: white;
			clear: left;
			text-align: center;
			font-weight:bold;
			font-size: small;
			/*bottom:0px;*/
		    /*position: fixed;*/
		}

		 .text-center{
		 	text-align: center;
		 	font-weight: bold;
		 }
		 span p{
		 	margin-top: 0px;
		 }

		 .bold-txt-city{
		 	font-weight: bold;
		 	font-size: 16px !important;
		 	margin-bottom: 40px !important;
		 }


		.text-center{
			text-align: center;
			font-weight: bold;

		}

		span p{
			margin-top: 0px;
		}

		.bold-txt{
			font-weight: bold;

		}
		.pi-padding{
			padding-top: 4.5em;
		}
		.thumb{
			width: 100%;
			height: 200px;
			background-size: contain;
			background-position: center;
			background-repeat: no-repeat;
			background-color: #2B2B2B;
		}
		.col-md-8{
			width: 65%;
			margin-right: 30px;
		 }
		 .col-md-4{
		 	width: 33%;
		 	padding-top: 10px;
		 	float: right;
		 	position: inline;
		 }

		span{
			font-size: 14px;
		}
		 .display-inline{
		 	margin-top: 15px !important;
		 }


	</style>
</head>
<body>
<div class="header" >
	<div class="date">{{$today}}</div>
	<img src="{{public_path('images/logo/logo-black.png')}}" alt="" class="logo" >
</div>
<div class="pi-padding">
	<hr>
	<p class="text-center">PROPOSED ITINERARY</p>
	<p class="text-center " style="color:#212121">{{ $search['total_number_of_days'] }} {{$search['currency']}} {{ $search['cost_per_person'] }} (Per Person)</p>

	@foreach ( $search['itinerary'] as $key => $leg )
	<?php 

		$last = count($search['itinerary']) - 1;
		$itineraryIndex = $key == $last ? $key : $key + 1;
		$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
		$arriveTimezone = get_timezone_abbreviation($search['itinerary'][$itineraryIndex]['city']['timezone']['name']);
	?>
	<div class="row" style="margin-top:35px;margin-bottom: 50px;">
			<div class="col-md-4">
                <?php
                if(count($leg['city']['image']) != 0){
                    $img = $leg['city']['image'][0]['small'] ?  config( 'env.CMS_URL' ) . $leg['city']['image'][0]['small'] : asset('images/no-image.png');
                }else{
                    $img =  asset('images/no-image1.jpg');
                }
                ?>
			<img class="thumb" src="{{$img}}">
			<?php // exit; ?>
		</div>
		<div class="col-md-8 pi-data-box-padding">
			<span class="bold-txt-city">{{ $leg['city']['country']['name'] }}, {{ $leg['city']['name'] }}</span>
			<div class="display-inline">
				<?php
					$accommodation_string = '';
					if(empty($leg['hotel'])){
						$accommodation_string = 'Own Arrangement';
					}else{
						$leg['hotel'] = json_decode(json_encode($leg['hotel']) , true );
					}

					if(!empty($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'])){
						$accommodation_string = $leg['hotel']['name'].'('.$leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'].')';
					}
					if(empty($leg['hotel']['room_name']) && isset($leg['hotel'])){
						$room_name = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'];
						$accommodation_string = $leg['hotel']['name'].'('.$room_name.')';
					}
					
					$last = count($search['itinerary']) - 1;
					$itineraryIndex = $key == $last ? $key : $key + 1;
					$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
					$arriveTimezone = get_timezone_abbreviation($search['itinerary'][$itineraryIndex]['city']['timezone']['name']);


					$the_etd         = new DateTime( $leg['city']['date_to'] );
					$formatted_etd   = $the_etd->format('jS, F Y');
					$next_leg_date_from = isset($search['itinerary'][$key+1]) ? date('jS, F Y', strtotime($search['itinerary'][$key+1]['city']['date_from'] )) : '';

					$duration = $leg['transport']['duration'] ? $leg['transport']['duration'] : '' ;
					$departure = date( 'H:i ', strtotime($leg['transport']['etd']) );

					$date_to = $leg['city']['date_to'];
					$exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($departure.' '.$duration));
				?>
				<span style="padding-right: 25px;">Accommodation:</span><span> {{ $accommodation_string }}</span> <br/>
				<span style="padding-right: 9px">Telephone Number:</span><span> {{ isset($leg['hotel']['reception_phone']) ? $leg['hotel']['reception_phone']:''}} </span><br/>
				@if ($key != ( count($search['itinerary']) - 1 ) )
					@if ( !isset($leg['transport']['provider']) )
						<span style="padding-right: 31px;">Departure Date: </span><span>{{ date( 'h:i A',strtotime($leg['transport']['etd']) ). ', '.$formatted_etd.' ( '. $departTimezone .' ) - '.get_city_by_id( $leg['transport']['from_city_id'] )['name'] }}</span><br/>
						<span style="padding-right: 47px;" >Arrival Date: </span><span>{{ date('h:i A', strtotime($exact_arrival_date_time) ).', '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - ' .get_city_by_id( $leg['transport']['to_city_id'] )['name']  }}</span><br/>
					@else
						<?php
						switch( $leg['transport']['provider'] ){
							case 'eroam': ?>
								<span style="padding-right: 31px;">Departure Date: </span><span>{{ date( 'h:i A',strtotime($leg['transport']['etd']) ). ', '.$formatted_etd.' ( '. $departTimezone .' ) - '.get_city_by_id( $leg['transport']['from_city_id'] )['name'] }}</span><br/>
								<span style="padding-right: 47px;" >Arrival Date: </span><span>{{ date('h:i A', strtotime($exact_arrival_date_time) ).', '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - ' .get_city_by_id( $leg['transport']['to_city_id'] )['name']  }}</span><br/>
						<?php break; ?>
							<?php case 'mystifly':?>
								<?php
									$departure_time2 = (new DateTime($leg['transport']['etd']))->format('A');
									$arrival_time2   = (new DateTime($leg['transport']['eta']))->format('A');

									$depart = new DateTime(str_replace('T',' ',$leg['transport']['etd']));
									$arrive = new DateTime(str_replace('T',' ',$leg['transport']['eta']));
								?>
								<span style="padding-right: 31px;">Departure Date: </span><span>{{ $depart->format('H:i A').', '.$formatted_etd.' ( '.$departTimezone.' ) - '.$leg['transport']['departure_data'] }}</span><br/>
								<span style="padding-right: 47px;" >Arrival Date: </span><span>{{ $arrive->format('H:i A').' '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - '.$leg['transport']['arrival_data'] }}</span><br/>
							<?php break;?>
						<?php } ?>

					@endif
				@endif

			</div>
			<span class="bold-txt">Description: </span><br/><span>{!! $leg['city']['description'] !!}</span><br/><br/>
			<div class="activity">
				@if ( $leg['activities'] )
					@foreach ( $leg['activities'] as $activity )
						<div>
							<span class="bold-txt" style="padding-right: 21px">Activity: </span><span> {{ $activity['name'] }}</span><br>
							<span class="bold-txt" style="padding-right: 40px">Date:</span><span> {{ date( 'jS, F Y', strtotime( $activity['date_selected'] ) ) }}</span><br/>
							<span class="bold-txt">Desciption: <br /></span>
							{{-- <span>{!! isset( $activity['description'] ) ? $activity['description'] : '' !!}</span><br/><br/> --}}
							<span>{!! $activity['description'] !!}</span><br/><br/>
						</div>
					@endforeach
				@else
					<span >No Activities.</span><br/>
				@endif
			</div>
			@if ( $leg['transport'] )
				<?php
					$the_etd         = new DateTime( $leg['city']['date_to'] );
					$formatted_etd   = $the_etd->format('jS, F Y');
					$next_leg_date_from = isset($search['itinerary'][$key+1]) ? date('jS, F Y', strtotime($search['itinerary'][$key+1]['city']['date_from'] )) : '';

					$duration = $leg['transport']['duration'] ? $leg['transport']['duration'] : '' ;
					$departure = date( 'H:i ', strtotime($leg['transport']['etd']) );

					$date_to = $leg['city']['date_to'];
					$exact_arrival_date_time = $date_to.' '.date('H:i',strtotime($departure.' '.$duration));
				?>
				@if ( !isset($leg['transport']['provider']) )
					<div>
						<span class="bold-txt">Transport: {{ $leg['transport']['operator']['name'] . ' (' . $leg['transport']['transporttype']['name'] . ')' }}</span><br>
						<span class="bold-txt">Depart: {{ $leg['transport']['etd']. ', '.$formatted_etd.' ( '. $departTimezone .' ) - '.get_city_by_id( $leg['transport']['from_city_id'] )['name'] }}</span><br>
						<span class="bold-txt">Arrive: {{ date(' h:i A', strtotime($exact_arrival_date_time) ).', '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - ' .get_city_by_id( $leg['transport']['to_city_id'] )['name']  }}</span><br>
					</div>
				@else
					<?php
					switch( $leg['transport']['provider'] ):
						case 'eroam': ?>
							<div>
								<span class="bold-txt">Transport: {{ $leg['transport']['operator']['name'] . ' (' . $leg['transport']['transporttype']['name'] . ')' }}</span><br>
								<span class="bold-txt">Depart: {{ $leg['transport']['etd']. ', '.$formatted_etd.' ( '.$departTimezone .' ) - '.get_city_by_id( $leg['transport']['from_city_id'] )['name'] }}</span><br>
								<span class="bold-txt">Arrive: {{ date(' h:i A', strtotime($exact_arrival_date_time) ).', '.$next_leg_date_from.' ( '. $arriveTimezone .' ) - ' .get_city_by_id( $leg['transport']['to_city_id'] )['name']  }}</span><br>
							</div>
					<?php break; ?>
					<?php case 'mystifly': ?>
						<?php
							$departure_time2 = (new DateTime($leg['transport']['etd']))->format('A');
							$arrival_time2   = (new DateTime($leg['transport']['eta']))->format('A');

						?>
						<div>
							<span class="bold-txt">Transport: {{ $leg['transport']['operating_airline'] . ', Flight # ' . $leg['transport']['flight_number'] }}</span><br>
							<?php $depart = new DateTime(str_replace('T',' ',$leg['transport']['etd'])); ?>
							<span class="bold-txt">Depart: {{ $depart->format('H:i A').', '.$formatted_etd.' ( '.$departTimezone.' ) - '.$leg['transport']['departure_data'] }}</span><br>
							<?php $arrive = new DateTime(str_replace('T',' ',$leg['transport']['eta'])); ?>
							<span class="bold-txt">Arrive: {{ $arrive->format('H:i A').' '.$next_leg_date_from.' ( '.$arriveTimezone.' ) - '.$leg['transport']['arrival_data'] }}</span><br>
							{{-- {{ $leg['transport']['arrival_itinerary_text'].' - '.$leg['transport']['arrival_data'] }} --}}
						</div>
					<?php break; ?>
					<?php endswitch; ?>
				@endif
			@elseif ( $last !=  $key)

				<span class="bold-txt">Transport: </span><span style="padding-left:15px;">{{ $leg['city']['name'] . ' to ' . $search['itinerary'][$key + 1]['city']['name'] }} (Self-Drive / Own Arrangement)</span>
			@endif
		</div>
	</div>
	@endforeach
	<footer>Powered by eRoam © Copyright {{$year}}. All Rights Reserved.</footer>
</div>

</body>
</html>