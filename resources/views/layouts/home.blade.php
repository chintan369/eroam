@include( 'layouts.partials.header' )
<section class="content-wrapper">
	<div class="body_sec">
			@yield( 'content' )
	</div>
</section>
@include( 'layouts.partials.footer' )