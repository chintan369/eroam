@extends('itinenary.booking-layout')

@php 
    if(isset($return_response) && !empty($return_response)){
    $name = $return_response['passenger_info'][0]['passenger_first_name'].' '.$return_response['passenger_info'][0]['passenger_last_name'];
    $email = $return_response['passenger_info'][0]['passenger_email'];
    $contact = $return_response['passenger_info'][0]['passenger_contact_no'];
    //$gender = $return_response['passenger_info'][0]['passenger_gender'];
    $dob = date('d-m-Y',strtotime($return_response['passenger_info'][0]['passenger_dob']));
    //$country = $return_response['passenger_info'][0]['passenger_country_name'];
    
      $startDate = date('j M Y', strtotime($return_response['from_date']));
      $endDate = date('j M Y', strtotime($return_response['to_date']));
@endphp
@section('booking-content')
<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
   
    <div class="container-fluid">
        @include('itinenary.partials.payment-filter-option')

        <div class="itinerary_page pt-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class=" m-1">
                            <div class="tripDetails mb-3">
                                <div class="card panel border-0 rounded-0 p-3">
                                    <div class="row">
                                        <div class="col-sm-7 col-xl-8">
                                            <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                            <div class="media">
                                                <i class="ic-calendar mr-2"></i>
                                                <div class="media-body pb-3 mb-0">
                                                    {{$startDate}} - {{$endDate}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="row align-items-end">
                                                <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="list-inline-item">
                                                            <i class=" ic-local_hotel"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-local_activity"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-directions_bus"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                                    <h2 class="font-weight-bold mb-0">{{ explode(' ',$return_response['total_days'])[0] }}</h2>
                                                    Nights
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="save_itinerary  m-1">
                            <div class="itinerary_list p-3 pb-1">
                                <div class="m-t-30 text-center">
                                  <h4><strong>Booking Complete</strong></h4>
                                  <p class="m-t-10">Confirmation email to: {{$return_response['billing_info']['passenger_email']}}<br/>Your eRoam booking reference: {{$return_response['invoiceNumber']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @php } @endphp

                <div class="row mt-4">
                  <div class="col-sm-6">
                    <a href="#" class="btn  btns_input_dark transform d-block w-100">PRINT RECEIPT</a>
                  </div>
                  <div class="col-sm-6">
                    <a href="{{ url('/proposed-itinerary-pdf/') }}" target="_blank"  name="" class="btn  btns_input_dark transform d-block w-100">PRINT / SHARE ITINERARY</a>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush