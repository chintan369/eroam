 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ijurni</title>
    <style type="text/css">
        #booking-details tr td{padding: 8px; line-height: 1.42857143; vertical-align: top;}
        #booking-details{border-collapse: collapse;}
       /* #booking-details tr:nth-child(even)>td {background-color: #F1F1F1;}*/
    </style>
</head>
<body style="margin: 0; padding: 0; color: #505050; width:760px; margin: 0 auto; font-family: arial;">
  <table width="100%" style="margin: 0 auto; border: solid 1px #ddd;" cellspacing="0" cellpadding="0">
    <tr>
    	<td>
    		<table width="100%" id="header" style="vertical-align: middle; text-align: center; background-color: #212121; padding: 5px 0px 1px 0;">
			    <tr>
			    	<td>
			    		<img src="{{ url( 'images/mail-logo.png' ) }}" alt="" />
			    	</td>
			    </tr>
		  </table>
    	</td>
    </tr>
    <tr>
    	<td>
    		<table width="70%" style="margin: 10px auto 0 auto; font-size: 15px; border-collapse: collapse;">
			    <tr>
			    	<td style="border-top: none; padding: 0px;">
			    		<h3 style="margin-bottom: 30px;">Hi Admin,</h3>
			    		 <p>A new user has contacted us. Kindly find below details.</p>
                         <p> Product : <strong>{{ $product }}</strong></p>
						 <p> First Name : {{ $first_name }}</p>
						 <p> Family Name : {{ $family_name }}</p>
						 <p> Email : {{ $email }}</p>
                         <p> Phone : {{ $phone }}</p>
						 <p> Postcode : {{ $postcode }}</p>
						 <p> Travel Date : {{ (!empty($travel_date) ? date('d-m-Y',strtotime($travel_date)) : '')}}</p>
						 <p> Comments : {{ $comments }}</p>
			    	</td>
			    </tr>
                <tr>
                    <td>
                        <p style="margin-top: 50px;"><strong>Thank You</strong>, <br/>eRoam</p>
                    </td>
                </tr>
		  </table>
    	</td>
    </tr>
    <tr>
    	<td>
    		<table width="100%" id="footer" style="color: #fff; margin-top: 30px; padding: 9px 10px 6px 10px; vertical-align: middle; text-align: center; background-color: #212121;">
			    <tr>
                    <td><img src="{{ url( 'images/mail-footer-logo.png' ) }}" alt="" style="width: 100px;"/></td>
                    <td style="font-size: 14px; text-align: right; font-size: 13px;">
                        Powered by eRoam © Copyright 2016 - 2018. All Rights Reserved. Patent pending AU2016902466
                    </td>
			    </tr>
		  </table>
    	</td>
    </tr>
  </table>
</body>
</html>

