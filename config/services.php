<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1844229535877858',
        'client_secret' => '534a93be5efbe8e7d6fc2084f2e437ef',
        'redirect' => 'http://localhost:8000/callback',
    ],

    'google' => [ 
        'client_id' => '771079836587-9eopc6r3t24b7t12sbhf45geqf1qf8ut.apps.googleusercontent.com',
        'client_secret' => 'qtcy7oqfKqCdd8Ff7zz7EJ5j',
        'redirect' => 'http://dev.eroam.com/callback/google' 
    ],
    
    'setting' => [
        'version' => 10.0,
    ],

];
