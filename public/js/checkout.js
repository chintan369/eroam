$("#login_form2").validate({
    rules: {
        username_login: {
            required: true,
            email:true
        },
        password_login: {
            required: true,
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter(element);
    },
    submitHandler: function (form) {
        //$("submitLoginForm").value('Loading..');
        $('input.submitLoginForm').val('Loading');
        var username = $("#username_login2").val();
        var password = $("#password_login2").val();
    
        if (username != '' && password != '') {
            eroam.ajax('post', 'login', {username_login: username, password_login: password}, function(response) {
                $('#submitLoginForm2').html('Loading...');
                
                if (response.trim() == 'valid') {
                    $('#login-error-msg2').hide();
                    $('#submitLoginForm2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
                    window.location.href = '/book/tour/';
                }
                else if(response.trim() == 'confirm_first')
                {
                    $('#login-error-message2').text('Please Confirm your mail first.').show();
                    $('#submitLoginForm2').html('Log In');
                }
                else {
                    $('#login-error-message2').text('Invalid Email Address or Password').show();
                    $('#submitLoginForm2').html('Log In');
                }
            }, function() {
                $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
            });
        }
    }
});

 $('#NoOfPass').on('change', function () {

        var num = $(this).val();
        var return_val = num % 2;
        if(num == 1)
        {
            $(".additional_guests").hide();
        }

        if((no_of_days != 1 && durationType == 'd') || (durationType != 'h' && no_of_days != 1))
        {
            if(return_val == 0)
            {
                var t_html ='<option value="">Twin Room</option>';
                var new_num = num / 2;
                t_html +='<option value="'+new_num+'" selected>'+new_num+'</option>';
                var s_html ='<option value="" disabled>Single Room</option>'; 
                s_html +='<option value="0" selected>0</option>';
                for (var i = 2; i <= num; i++) {
                    s_html +='<option value="'+i+'">'+i+'</option>';
                    if(i!=num){
                        i = i+1; 
                    }
                }
            }
            else
            {
                if(num > 1)
                {
                    var new_num = num - 1;
                    var new_num = new_num / 2;
                    var t_html ='<option value="">Twin Room</option>'; 
                    t_html +='<option value="'+new_num+'" selected>'+new_num+'</option>';

                    for (var i = 1; i <= num; i++) 
                    {
                        s_html +='<option value="'+i+'">'+i+'</option>';
                        if(i!=num)
                        {
                            i = i+1; 
                        }
                    }
                }
                else
                {
                    var s_html ='<option value="" disabled>Single Room</option>'; 
                    s_html +='<option value="1" selected>1</option>';
                    var t_html ='<option value="">Twin Room</option>'; 
                    t_html +='<option value="0" selected>0</option>';
                }
            }
            $('#NoOfSingleRoom').html(s_html);
            $('#NoOfTwinRoom').html(t_html);
        }
        var singal = $('#NoOfSingleRoom option:selected').val();
        var twin = $('#NoOfTwinRoom option:selected').val();

        AddPersons(num);
        ChangePriceValues();
        if(singal == 0){
            $('.single_room_total').hide();
        }else{
            $('.single_room_total').show();
        }

        if(twin == 0){
            $('.twin_room_total').hide();
        }else{
            $('.twin_room_total').show();
        }
    });

    $('#NoOfSingleRoom').on('change', function () {
        var num = $('#NoOfPass option:selected').val();
        var new_num = $(this).val();
        var return_val = new_num % 2;
        var s_html ='<option value="" disabled>Single Room</option>';
        var t_html ='<option value="">Twin Room</option>';
        if(return_val == 0)
        {

            var s_html ='<option value="" disabled>Single Room</option>'; 
            s_html +='<option value="0" selected>0</option>';
            for (var i = 2; i <= num; i++) {
                if(i == new_num){
                    s_html +='<option value="'+i+'" selected>'+i+'</option>'; 
                }else{
                    s_html +='<option value="'+i+'">'+i+'</option>'; 
                }
                if(i!=num){
                    i = i+1; 
                }
            }
        }
        else
        {
            for (var i = 1; i <= num; i++) {
                if(i == new_num){
                    s_html +='<option value="'+i+'" selected>'+i+'</option>'; 
                }else{
                    s_html +='<option value="'+i+'">'+i+'</option>'; 
                }
                if(i!=num){
                    i = i+1; 
                }
            }
        }      
      
        new_num = num - new_num;
        if(new_num > 1){
            new_num = new_num / 2;
            t_html +='<option value="'+new_num+'" selected>'+new_num+'</option>';
        }else{
            var t_html ='<option value="">Twin Room</option>'; 
            t_html +='<option value="0" selected>0</option>';
        }
        
        $('#NoOfSingleRoom').html(s_html);
        $('#NoOfTwinRoom').html(t_html);

        var singal = $('#NoOfSingleRoom option:selected').val();
        var twin = $('#NoOfTwinRoom option:selected').val();
        
        AddPersons(num);
        ChangePriceValues();
        if(singal == 0){
            $('.single_room_total').hide();
        }else{
            $('.single_room_total').show();
        }

        if(twin == 0){
            $('.twin_room_total').hide();
        }else{
            $('.twin_room_total').show();
        }      
    });

    $('#NoOfTwinRoom').on('change', function () {

        var numOfPass = $('#NoOfPass option:selected').val();
        var singal = $('#NoOfSingleRoom option:selected').val();
        var twin = $(this).val();
        if(parseInt(twin) <= parseInt(numOfPass)){
            //if(parseInt(twin + singal)  <= parseInt(numOfPass)){
            var diff = parseInt(numOfPass) - parseInt(twin);
            $('#NoOfSingleRoom').val(diff);
            ChangePriceValues();
        } 
    });


    function AddPersons(num) {
        if (num == 1) {
            $('.otherPersons').html("");
            return;
        } 

        $('.otherPersons').html("");
        var data = ""; var key = ''; 
      
        for (var i = 1; i < num; i++) {

            key = i + 1;
            var k   = i - 1; 
            var html2 = '<div class="panel-body px-3 pt-4 pb-3">'+
          
            '<p><strong>Additional Guest '+key+'</strong></p>'+
                '<div class="row">'+
                    '<div class="col-sm-4">'+
                        '<div class="form-group">'+
                            '<label class="label-control">Guest Type</label>'+
                            '<select class="form-control guest_type" name="guest_type['+k+']" data-id='+key+'>'+
                                '<option value="">Select Guest Type</option>';
                                if(from == "viator")
                                {
                                html2 +=  '<option value="adult">Adult (Age 11+)</option>'+
                                  '<option value="viator_child">Child (Age 4 to 10)</option>'+
                                  '<option value="infant">Infant (Age 0 to 3)</option>';
                                }
                                else
                                {                                   
                                  html2 +='<option value="adult">Adult (18+)</option>'+
                                  '<option value="child">Child (0 - 17)</option>';
                                }
                            html2 +='</select>'+
                        '</div>'+
                    '</div>' +   
                    '<div class="col-sm-4 guest_type'+key+'">' +                   
                    '</div>'+
                '</div>'+ 
                '<div class="guest_info'+key+'"></div>'+
            '</div>';
            data += html2;
        }

        $(".additional_guests").show();
        $('.otherPersons').html(data);
   
        $(".passenger_dob").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
        $('.label_check, .label_radio').click(function(){
            setupLabel();
        });
    }

    function ChangePriceValues(){
        var amount            = $("#radio-d").val();
        var NoOfPass          = $('#NoOfPass option:selected').val();
        var NoOfSingleRoom    = $('#NoOfSingleRoom option:selected').val();
        var NoOfTwinRoom      = $('#NoOfTwinRoom option:selected').val();
        var id                = $("#DateRadioIdValue").val();
        var startDate         = $('#startDate').val();
        var finishDate        = $('#finishDate').val();
        var singlePrice       = $('#singlePrice').val();
        var singalTotal       = NoOfSingleRoom * singlePrice;
        var twinTotal         = amount * NoOfTwinRoom;
        twinTotal             = twinTotal * 2;
        var subtotal          = parseFloat(singalTotal) + parseFloat(twinTotal);
        var amount            = twinTotal;
        var singaltotal       = singalTotal;
        var ccfee             = 0;
        var finaltotal        = subtotal + ccfee;          
         
        //console.log(parseFloat(amount).toFixed(2)+'//'+NoOfPass+'//'+parseFloat(subtotal).toFixed(2)+'//'+parseFloat(ccfee).toFixed(2)+'//'+parseFloat(finaltotal).toFixed(2));

        if((no_of_days != 1 && durationType == 'd') || (durationType != 'h' && no_of_days != 1)){

            $('#total').text(parseFloat(amount).toFixed(2));
            $('#singaltotal').text(parseFloat(singaltotal).toFixed(2));
            $('#subtotal').text(parseFloat(subtotal).toFixed(2));
            $('#creditCardFee').val(parseFloat(ccfee).toFixed(2));
            $('#ccfee').text(parseFloat(ccfee).toFixed(2));
            $('#finaltotal, #chargeAmount').text(parseFloat(finaltotal).toFixed(2));

            $('#totalAmount').val(parseFloat(finaltotal).toFixed(2));
            $('#singleAmount').val(parseFloat(singaltotal).toFixed(2));
            $('#twinAmount').val(parseFloat(amount).toFixed(2));

            $('#pricePP1').text("$" +default_currency+" "+ parseFloat(amount).toFixed(2));
            $('#TotalPricePP').text("$"+ default_currency+" " + parseFloat(subtotal).toFixed(2));
            $('.pay_now_btn').text("PAY $"+default_currency+" " + parseFloat(subtotal).toFixed(2)+" NOW");

            $('#departure_date').val(startDate);
            $('#return_date').val(finishDate);
        }
        else
        { 
            if(from == 'viator')
            {
                var totalAmount =  $('#totalAmount').val();
                $(".guest_type").each(function(){
                    var value = $(this).val();
                    if(value == "adult")
                    {
                      totalAmount = parseFloat(totalAmount) + parseFloat(singlePrice);
                    }
                    else if(value == "viator_child")
                    {
                      totalAmount = parseFloat(totalAmount) + parseFloat(childPrice);
                    }
                    else if(value == "infant")
                    {
                      totalAmount = parseFloat(totalAmount) + parseFloat(infantPrice);
                    }
                });
                
                $("#TotalPricePP").text("$" +default_currency+ " "+ parseFloat(totalAmount).toFixed(2));
                $('.pay_now_btn').text("PAY $"+ default_currency+ " " + parseFloat(totalAmount).toFixed(2)+ ' NOW');
            }
            else
            {   
                console.log(NoOfPass);
                var finaltotal   = NoOfPass * singlePrice;
                $('#totalAmount').val(parseFloat(finaltotal).toFixed(2));
                $('#finaltotal, #chargeAmount').text(parseFloat(finaltotal).toFixed(2));
                $('#TotalPricePP').text("$"+default_currency+" "+ parseFloat(finaltotal).toFixed(2));
                $('.pay_now_btn').text("PAY $"+default_currency+" " + parseFloat(finaltotal).toFixed(2)+ ' NOW');
                
            }
        } 
    }

    $(document).on('change','.guest_type',function(){

        var id = $(this).attr('data-id');
        var value = $(this).val();
        if(value == 'adult')
        {
            var guest_type_html 
                =   '<div class="panel-form-group form-group">'+
                        '<label class="label-control">Guest Title</label>' +
                        '<select class="form-control guest_title" name="guest_title_'+id+'" class="guest_title">'+
                            '<option value="mr">Mr.</option>'+
                            '<option value="ms">Ms.</option>'+
                            '<option value="mrs">Mrs.</option>'+
                        '</select>'+
                    '</div>';
            $(".guest_type"+id).html(guest_type_html);

            var guest_info_html =
                    '<div class="panel-form-group input-control form-group m-t-20">'+
                        '<label class="label-control">First Name</label>'+
                        '<input type="text" name="guest_firstname_'+id+'" class="form-control guest_firstname" value="">'+
                    '</div>'+
                    '<div class="panel-form-group input-control form-group">'+
                        '<label class="label-control">Family Name (As Shown on Passport)</label>'+
                        '<input type="text" name="guest_familyname_'+id+'" class="form-control guest_familyname" value="">'+
                    '</div>'+
                    '<div class="panel-form-group input-control form-group">'+
                        '<label class="label-control">Email Address* </label>'+
                        '<input type="text" name="guest_email_'+id+'" class="form-control guest_email" value="">'+
                    '</div>'+
                    '<div class="panel-form-group input-control form-group">'+
                        '<label class="label-control">Contact Number</label>'+
                        '<input type="text" name="guest_contact_no_'+id+'" class="form-control guest_contact_no" value="">'+
                    '</div>';
            $(".guest_info"+id).html(guest_info_html);
        }
        else if(value == 'child' || value == 'infant' || value == 'viator_child')
        {
            if(value == "child")
            {
              var option_val = "";
              for(var i = 0; i<=17 ; i++)
              {
                  option_val += '<option value='+i+'>'+i+'</option>';
              }
              var guest_type_html = 
                      '<div class="panel-form-group form-group">'+
                          '<label class="label-control">Child Age</label>'+
                          '<select class="form-control guest_child_age" name="guest_cild_age_'+id+'">'+option_val+'</select>'+
                      '</div>';
            }
            else if(value == "viator_child")
            {
                var option_val = "";
                for(var i = 4; i<=10 ; i++)
                {
                    option_val += '<option value='+i+'>'+i+'</option>';
                }
                var guest_type_html = 
                      '<div class="panel-form-group form-group">'+
                          '<label class="label-control">Child Age</label>'+
                          '<select class="form-control guest_child_age" name="guest_cild_age_'+id+'">'+option_val+'</select>'+
                      '</div>';
            }
            else if(value == "infant")
            {
                var option_val = "";
                for(var i = 0; i<=3 ; i++)
                {
                    option_val += '<option value='+i+'>'+i+'</option>';
                }
                var guest_type_html = 
                      '<div class="panel-form-group form-group">'+
                          '<label class="label-control">Infant Age</label>'+
                          '<select class="form-control guest_child_age" name="guest_cild_age_'+id+'">'+option_val+'</select>'+
                      '</div>';
            }
            $(".guest_type"+id).html(guest_type_html);

            var guest_info_html = 
                    '<div class="panel-form-group input-control form-group m-t-10">'+
                        '<label class="label-control">First Name</label>'+
                        '<input type="text" name="guest_firstname_'+id+'" class="form-control guest_firstname" value="">'+
                    '</div>'+
                   ' <div class="panel-form-group input-control form-group">'+
                        '<label class="label-control">Family Name (As Shown on Passport)</label>'+
                        '<input type="text" name="guest_familyname_'+id+'" class="form-control guest_familyname" value="">'+
                    '</div>';
            $(".guest_info"+id).html(guest_info_html);
        }
        else
        {
            $(".guest_type"+id).html('');
            $(".guest_info"+id).html('');
        }
        ChangePriceValues();
    });

    $("#checkin-date").datepicker({
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        startDate: '+1d'
    });

    function getStartDate() {
        var f = $('#mySelect').val().split('-');
        return '01/04/' + f[0];
    }
    $('#tour_form_id').on('submit', function(event) {
      // adding rules for inputs with class 'comment'

        $('.guest_type').each(function () {            
            $(this).rules('add', {
                required: true,
            });
        });   

        $('.guest_familyname').each(function () {
            $(this).rules('add', {
                required: true,
                lettersonly:true
            });
        });   

        $('.guest_title').each(function () {
            $(this).rules('add', {
                required: true,
                lettersonly:true
            });
        });

        $('.guest_firstname').each(function () {
            $(this).rules('add', {
                required: true
            });
        }); 

        $('.guest_email').each(function () {
            $(this).rules('add', {
                required: true,
                email:true,
            });
        });       

        // event.preventDefault();

        // test if form is valid 
        if($('#tour_form_id').validate().form()) {
            console.log("validates");
        } else {
             // prevent default submit action         
            event.preventDefault();
            console.log("does not validate");
        }
    });

    $.validator.addMethod('ge', function(value, element, param) {
        //var total = parseInt($("#NoOfSingleRoom").val()) + parseInt($("#NoOfTwinRoom").val()) 
         var total = parseInt($("#NoOfSingleRoom").val());  
          return this.optional(element) || parseInt(total) <= parseInt($(param).val());
      }, 'Select less than or equal to value of No of Passenger.');

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters only please."); 

    $.validator.addMethod("numbersonly", function(value, element) {
        return this.optional(element) || /^[0-9," "]+$/i.test(value);
    }, "Numbers only please."); 

    $("#tour_form_id").validate({
        ignore: [],
        rules: {
            NoOfSingleRoom :{  
              required: true ,
              ge: '#NoOfPass'
            },
            NoOfTwinRoom :{  
              required: true ,
              ge: '#NoOfPass'
            },
            passenger_contact_no:{
                required: true,
                numbersonly:true,
                maxlength: 15,
            },
            passenger_email: {
                required: true,
                email:true
            },
            passenger_first_name: {
                required:true,
            },
            billing_firstname: {
                required:true,
            },
            billing_familyname: {
                required:true,
            },
            billing_email: {
                required:true,
            },
            card_number: {
                required: true,
                number:true,
            },
            expiration_date: {
                required: true
            },
            cvv: {
                required: true, 
                maxlength: 3,
                number:true
            },
            terms:{
                required: true
            },
            package_price:{
                required: true
            },
            passenger_first_name: {
                required: true
            },
            passenger_family_name: {
                required: true
            },
            billing_postcode: {
                required: true
            }
        },
        errorPlacement: function (label, element) {
          label.insertAfter(element);
        }
    });

