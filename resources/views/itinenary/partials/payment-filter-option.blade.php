<div class="itinerary_filter pt-5">
    <div class="row pl-1">
        <div class="col-12">
            <div class="pagetop_title pt-1 pb-2">
                <h2><i class="ic-explore"></i><span>Book Itinerary</span></h2>
            </div>
        </div>
    </div>
    <div class="p-3">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <a href="{{ url('view-itinerary') }}" class="btn  btns_input_blue transform d-block w-100">VIEW ITINERARY</a>

            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <a href="{{ url('proposed-itinerary-pdf') }}" class="btn  btns_input_blue transform d-block w-100">PRINT / SHARE ITINERARY</a>

            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <a href="#" class="btn  btns_input_blue transform d-block w-100">BOOK ITINERARY</a>

            </div>
        </div>
    </div>
</div>