<!---Signup Modal--->
<div id="signupmodal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer">
                <div class="modal-header pb-0">
                    <h4 class="text-center">@lang('home.sign_up_heading')</h4>
                </div>
                 @if(session()->has('register_confirm_fail'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('register_confirm_fail') }}
                </div>
                @else
                <div class="alert alert-success register_message" style="display:none" role="alert"></div>
                @endif
                <p class="text-center">@lang('home.social_network') </p>
                <div class="modalform pt-2">
                    <form id="signup_form_model">
                         @csrf
                    <div class="row social_btn_link">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <a href="{{ url('redirect/google') }}" class="social_gplus"><i class=" ic-google_plus_new"></i>Google</a>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <button type="button" class="social_facebook fb-login border-0" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" disabled><i class=" ic-facebook"></i>Facebook</button>
                        </div>
                    </div>
                    <div class="or_block"><span>@lang('home.sign_in_or_text')</span></div>
                    <div  class="signup_forms">
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>@lang('home.email_address')</label>
                                <input type="text" name="reg_email" class="form-control" id="reg_email" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fildes_outer">
                                <label>@lang('home.password')</label>
                                <input type="password" name="reg_pass" id="reg_pass" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="custom_check">@lang('home.sign_up_checkbox') &nbsp; <input type="checkbox" id="checkbox-reg" value="4"><span class="check_indicator">&nbsp;</span></span>
                        </div>
                        <div class="form-group">
                            <input class="sign_btn" type="submit" value="Sign Up"  id="submitSignupForm">
                        </div>
                        <div class="bottom_text">
                            <div class="float-left"> @lang('home.sign_up_text') <a href="javascript:void(0);" id="sign_in_button"> @lang('home.sign_in_heading')</a></div>
                            <div class="float-right">  @lang('home.sign_up_tnc_text') <a href="{{url('terms')}}" target="_blank">@lang('home.sign_up_tnc_link')</a></div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---END Signup Modal--->