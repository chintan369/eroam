function allowDropp(ev) {
    ev.preventDefault();
}

function dragp(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    var data = ev.target.id;
    if ($("#" + data).parent().parent().prop('className') == 'drop-list drop-lists') {
        var interest = data.replace('dragp_', 'interestp-');
        $(".interestMovep").css('opacity', '0.5');
        $("#" + interest + ".interestMovep").css('opacity', '');
    }
}

function dropp(ev) {
    ev.preventDefault();

    var data = ev.dataTransfer.getData("text");
    var name = data.replace('drag', 'name');
    var inputValue = data.replace('drag', 'inputValue');
    var interest = data.replace('dragp_', 'interestp-');
    var x = document.getElementById(name);
    var count = 0;
    var text = '';
    var data_value = document.getElementById(data).getAttribute("data-value");
    var data_name = document.getElementById(data).getAttribute("data-name");
    data_value = parseInt(data_value);
    var parentClass = $("#" + data).parent().parent().prop('className');
    var ClassName = $("#" + data).parent().prop('className');
    if (ClassName != "interest-button-active") {
        if ($("." + ev.target.getAttribute("class")).parent().prop('className') == 'drop-list drop-lists') {
            if (x.style.display === 'none') {
                x.style.display = 'block';
            } else {
                x.style.display = 'none';
            }

            var hidden_interest_field = $("#" + data).parent().find('.input-interest');
            if (hidden_interest_field.val()) {
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);
                hidden_interest_field.val('');
                hidden_interest_field.attr('name', 'interests[]');
            } else {
                activitySequence.push(data_value);
                var interestIndex = activitySequence.indexOf(data_value);
                hidden_interest_field.val(data_value);
                hidden_interest_field.attr('name', 'interests[' + (interestIndex + 1) + ']');
            }
            ev.target.appendChild(document.getElementById(data));
            ev.target.appendChild(document.getElementById(inputValue));
            var parentDiv = $("#" + data).parent();
            parentDiv.attr('id', interest);
            parentDiv.attr('data-name', data_name);
            $("#" + data).parent().removeClass('blankInterests').addClass('interest-button-active');
        }

    } else {
        
        if (ev.target.getAttribute("class") == 'blankInterests' && $("." + ev.target.getAttribute("class")).parent().prop('className') == 'drop-list drop-lists') {
            
            var interestIndex = activitySequence.indexOf(data_value);
            activitySequence.splice(interestIndex, 1);
            activitySequence.push(data_value);

            var hidden_interest_field = $("#" + data).parent().find('.input-interest');
            hidden_interest_field.attr('name', 'interests[' + ev.target.getAttribute("data-sequence") + ']');

            ev.target.appendChild(document.getElementById(data));
            ev.target.appendChild(document.getElementById(inputValue));
            $("#" + interest).removeClass('interest-button-active').addClass('blankInterest').removeAttr('data-name');
            $("#" + interest).removeAttr('id');
            $("#" + data).parent().attr('id', interest).attr('data-name', data_name);
            $("#" + data).parent().removeClass('blankInterest').addClass('interest-button-active');
        } else {
            
            if (parseInt(ev.target.getAttribute("data-value")) == data_value) {
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);

                var hidden_interest_field = $("#" + data).parent().find('.input-interest');
                hidden_interest_field.val('');
                hidden_interest_field.attr('name', 'interests[]');
                $(".interestMovep").css('opacity', '');
                
                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                $("#" + interest).removeClass('interest-button-active').addClass('blankInterests');
                $("#" + interest).removeAttr('id');
                x.style.display = 'none';
            }
        }
    }
}

$(document).ready(function(){

    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter letters only."); 
    $("#profile_step3_form").validate({
        rules: {
            age_group: {
                required: true,
            },
            transport_types: {
                required: true
            },
            gender: {
                required: true
            },
            hotel_category_id: {
                required: true,
            },
            nationality: {
                required: true,
            },     
            room_type: {
                required: true,
            }
        },        
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            var btn = $("#save_travel_preference").html('loading');

            if ($('.nationality_id').val()) {
                var nationality_dom = $(".nationality_id option:selected").text();
            } else {
                var nationality_dom = $(".nationality_id option:selected").val();
            }

            if ($('.age_group_id').val()) {
                var age_group_dom = $(".age_group_id option:selected").text();
            } else {
                var age_group_dom = $(".age_group_id option:selected").val();
            }
            var age_group_id = $('.age_group_id').val();
            var nationality_id = $('.nationality_id').val();
            var gender = $('.gender_sel').val();

            var nationality = (isNotUndefined(nationality_dom)) ? nationality_dom : [];
            var age_group = (isNotUndefined(age_group_dom)) ? age_group_dom : [];
            var interestLists = [];
            var accommodations = [];
            var accommodationIds = [];
            var interestListIds = [];
            var roomTypeIds = [];
            var roomTypes = [];
            var transportTypeIds = [];
            var transportTypes = [];

            $('.hotel_category option:selected').each(function() {
                if ($(this).val()) {
                    var id = parseInt($(this).val());
                    var name = $(this).attr('data-name');
                    accommodationIds.push(id);
                    accommodations.push(name);
                }
            });

            $('.transport_type_optionsp option:selected').each(function(){
                if($(this).val()){
                    var id = parseInt($(this).val());
                    var name = $(this).attr('data-name');
                    transportTypeIds.push(id);
                    transportTypes.push(name);
                }
            });

            $('.room_type_optionsp option:selected').each(function() {
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                roomTypeIds.push(id);
                roomTypes.push(name);
            });

            $('.interest-button-active').each(function() {
                interestLists.push($(this).attr('data-name'));
            });

            var data = {
                travel_preference: [{
                    accommodation: accommodationIds,
                    accommodation_name: accommodations,
                    room_name: roomTypes,
                    room: roomTypeIds,
                    transport_name: transportTypes,
                    transport: transportTypeIds,
                    age_group: age_group,
                    nationality: nationality,
                    gender: gender,
                    interestLists: interestLists.join(', '),
                    interestListIds: activitySequence
                    //interestListIds: interestListIds
                }]
            };


            var interestText = interestLists.length > 0 ? interestLists.join(', ') : 'All';
            var accommodation = accommodations.length > 0 ? accommodations.join(', ') : 'All';
            var transport = transportTypes.length > 0 ? transportTypes.join(', ') : 'All';
            var nationalityText = nationality != '' ? nationality : 'All';

            var ageGroupText = age_group != '' ? age_group : 'All';

            $('#_accommodation').html(' <strong> Accommodation: </strong> ' + accommodation);
            $('#_transport').html(' <strong> Transport: </strong> ' + transport);
            $('#_nationality').html(' <strong> Nationality: </strong> ' + nationalityText);
            $('#_age').html(' <strong> Age: </strong> ' + ageGroupText);
            $('#_interests').html(' <strong> Interests: </strong> ' + interestText);


            var post = {
                hotel_categories: accommodationIds,
                hotel_room_types: roomTypeIds,
                transport_types: transportTypeIds,
                age_group: age_group_id,
                nationality: nationality_id,
                gender: gender,
                interests: activitySequence,
                _token: $('meta[name="csrf-token"]').attr('content'),
                user_id: $("#user_id").val(),
            };
            eroam.ajax('post', 'save/travel-preferences', post, function(response) {
                //console.log(post);
            });
           

            eroam.ajax('post', 'session/travel-preferences', data, function(response) {
                setTimeout(function() {
                    btn.button('reset');

                    eroam.ajax('get', 'session/updatePreferences', '', function(responsedata) {
                        $("#changePreferences").html(responsedata);
                    });
                    //window.localtion.href="profile_step3";
                    $(location).attr('href', '/profile_step3');
                    //$('#preferencesModal').modal('hide');
                }, 3000);
            });
        }
    });
});

