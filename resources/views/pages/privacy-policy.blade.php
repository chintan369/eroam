@extends('layouts.static') @section('custom-css')
<style>
    .wrapper {
        background-image: url( {{ url( 'images/bg1.jpg' ) }} ) !important;
    }

    .top-margin {
        margin-top: 5px;
    }

    .container-padding {
        padding-left: 35px;
        padding-right: 35px;
    }

    .top-bottom-paddings {
        padding-top: 20px;
        padding-bottom: 20px;
    }

    .white-box {
        background: rgba(255, 255, 255, 0.9);
        border: solid thin #9c9b9b !important;
    }

    .wrapper {
        background-attachment: fixed;
    }

    #privacy-policy p {
        line-height: 1.3em;
        text-align: justify;
    }

    .padding-20 {
        padding-left: 20px;
        padding-right: 20px;
    }

</style>
@stop @section('content')




<div class="top-section-image">
    <img src="{{asset('images/bg-image.jpg')}}" alt="" class="img-responsive">
</div>

<section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <h2 class="border-bottom">Privacy Policy</h2>
                    <div class="mt-4">
                        <p>We feel it as our responsibility to keep the customers aware with our privacy statement. We consider that matter as the most important factor which should be discussed. eRoam Pty Ltd is committed to protect your personal information as well as having respect for them. Our privacy policy implied on (eroam.com) and the entire parent or sub domains relating to or controlled/owned by (eRoam Pty Ltd).</p>
                        <p class="mt-3"><strong>Sign up/online booking with us</strong></p>
                        <p class="mt-3">When you initiate a relation with us by sign up on our website, we do collect some information regarding your personal identification or ask you to connect through your social media websites accounts. In case of online booking, we collect information regarding your payment sources which are used only for verification and the payment receiving purposes. Furthermore there is no loss to your personal information’s with eRoam Pty Ltd dealings because we are the trusted technology partner.</p>
                        <p class="mt-4"><strong>Cookies</strong></p>
                        <p class="mt-3">Cookies are the small set of data packs which are sent by the server of visiting website to the user browser for reference purposes. This small packet of data is then stored in computer of the user for some future recalling. eRoam Pty Ltd also uses that cookies technology only to make personalize your experience with us. By using cookies we get information regarding your system IP address, browser type, location and some of the user previous searches. The information’s collected are utilized for the identification of the user so when he came back after some time to the website then we could provide search results according to his/her past visits. The use of these information’s also include development of new packages according to the needs of the customers in order to keep them more satisfied with our great services. In cookies information’s does not include (name of the user, e-mail address, some postal address or any other contact information). Moreover we do not rent, sell or share your information with anyone and used solely for the benefit of customers.</p>
                        <p class="mt-3"><strong>Third Party</strong></p>
                        <p class="mt-3">There are many of the third acting parties which also use the cookies technology like Google. eRoam Pty Ltd uses the Google services for advertisement purposes, so when a user visits a website where he/she interact with eRoam Pty Ltd advertisement then at that point these third parties use cookies to serve ads for user prior visits. Neither eRoam Pty Ltd nor any sub domain connect with eRoam Pty Ltd is liable for the third party cookies. Moreover the users can deactivate the third party cookies by going into Network Advertising Initiative opt-out page.</p>
                        <p class="mt-3"><strong>Linked websites</strong></p>
                        <p class="mt-3">You should be aware of the fact that the websites linked with the eRoam Pty Ltd (which are not in ownership or controlled by the eRoam Pty Ltd) are not included in this privacy policy. When you move to these links then it means you are leaving the eRoam Pty Ltd hosting page, so when you visit these links then kindly make a review of their privacy policy statement as it might be different to our policy.</p>
                        <p class="mt-3"><strong>Adults</strong></p>
                        <p class="mt-3">Only adults of 18 years or above are allowed to get registered to eRoam Pty Ltd. We do not collect information’s from the individuals below the age of 18, but we can in case of Guardian or family.</p>
                        <p class="mt-3"><strong>Signup for Newsletter</strong></p>
                        <p class="mt-3">When you sign up for our newsletter service, then it means that you are going to receive the updates on a fixed time period basis. Later on if you want to remove that service from our value pack then you can also unsubscribe to our newsletter service.</p>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>


@stop @section( 'custom-js' ) @stop
