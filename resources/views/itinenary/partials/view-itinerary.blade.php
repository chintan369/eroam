@extends('itinenary.booking-layout')
@section('booking-content')
<div class="account-right itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="container-fluid">
    	<div class="steps stepsouter p-4">
            <h5><i class="ic-explore"></i> View Detailed Itinerary</h5>
            <div class="text-right d-block d-lg-none"><a href="javascript://" class="navbtn menu-btn text-secondary"><i class="ic-menu"></i></a></div>
            <div class="row mt-4">
                <div class="form-group col-md-3 col-sm-6">
                    <div class="fildes_outer">
                        <label>Check-in Date:</label>
                        <input id="checkin-date" type="text" placeholder="{{$startDate}}" class="form-control disabled valid" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                        <span class="arrow_down"><i class="ic-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <div class="fildes_outer">
                        <label>Check-out Date:</label>
                        <input id="checkout-date" type="text" placeholder="{{ $endDate }}" class="form-control disabled valid" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                        <span class="arrow_down"><i class="ic-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Rooms</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="rooms" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                                <option value="{{$rooms}}">1</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Adults (18+)</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="num_of_adults" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" data-original-title="Not Available in Pilot">
                                <option value="{{ $adults }}">{{ $adults }}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Children (0-17)</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="num_of_children[]" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                                <option value="0">No Children</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 mb-4 row">
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="{{url('view-itinerary')}}" name="" class="btn btn-white btn-block active mb-3 mb-sm-0">VIEW ITINERARY</a>
                </div>
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="{{url('proposed-itinerary-pdf')}}" class="btn btn-white btn-block mb-3 mb-sm-0">PRINT / SHARE ITINERARY</a>
                </div>
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="{{url('payment-summary')}}" name="" class="btn btn-white btn-block mb-3 mb-sm-0">BOOK ITINERARY</a>
                </div>
            </div>
            <hr>
            <div class="profile-steps payment-steps mt-3">
                <ul class="nav nav-tabs nav-fill border-0">
                    @foreach ($data['itinerary'] as $leg)
                    <li class="nav-item">
                        <a href="javascript://" class="nav-link">
                            <span class="circle d-flex align-items-center justify-content-center">{{$leg['city']['default_nights']}}</span>
                            <span class="d-block"><strong>{{str_limit($leg['city']['name'], 18)}}</strong><br>{{$leg['city']['country']['name']}}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="locations_map pt-3 pt-5 pb-5 ">
                <div id="map1" style="height: 400px;"></div>
            </div>
            <section class="tours-wrapper">
            	@if (session()->get('search'))
            	@php $i=1; @endphp
                @foreach ($data['itinerary'] as $key => $leg)
                <div class="tripDetails mb-3">
                    <div class="card panel border-0 p-3">
                        <div class="row">
                            <div class="col-sm-7 col-xl-8">
                                <h5 class="font-weight-bold">{{$leg['city']['name']}}, {{$leg['city']['country']['name']}}</h5>
                                <div class="media">
                                    <i class="ic-calendar mr-2"></i>
                                    <div class="media-body pb-3 mb-0">
                                        {{date('d F Y', strtotime($leg['hotel']['checkin']))}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-xl-4">
                                <div class="row align-items-end">
                                    <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                        <ul class="list-unstyled mb-0">
                                            <li class="list-inline-item"><i class="ic-local_hotel fa-lg"></i></li>
                                            <li class="list-inline-item"><i class="ic-local_activity fa-lg"></i></li>
                                            <li class="list-inline-item"><i class="ic-flight fa-lg"></i></li>
                                        </ul>
                                    </div>
                                    <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                        <h2 class="font-weight-bold mb-0">{{$leg['hotel']['nights']}}</h2>
                                        Nights
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="clearfix">
                            <div class="accomodation-img">
                            	@if(!empty($leg['city']['image']))
                                <img src="https://cms.eroam.com/{{$leg['city']['image'][0]['small'] }}" alt="" class="img-fluid" />
                                @else
                                <img src="{{ url('assets/images/no-image1.jpg') }}" alt="" class="img-fluid" />
                                @endif
                            </div>
                            <div class="accomodation-details lh-condensed">
                                <p>{!! substr($leg['city']['description'], 0, 500) !!}</p>
                                <p class="mt-5"><a href="javascript://" class="text-dark trip-viewmore" data-id="trip{{$i}}"><i class="ic-expand_more mr-1"></i> Click to view detailed itinerary</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="tripDetails-wrapper" id="trip{{$i}}">
                        @if(isset($leg['hotel']) && !empty($leg['hotel']))
                        <div class="tripDetails-container mb-3 mt-3">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-local_hotel fa-lg"></i></span> <strong>ACCOMMODATION</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <a href="#" data-toggle="modal" data-target="#editTripModal"><i class="ic-create"></i></a>
                                        <a href="#"><i class="ic-block"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Check In</th>
                                                <th>Check out</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="tourTripName">
                                            	<td>{{date('d M Y', strtotime($leg['hotel']['checkin']))}}</td>
                                            	<td>{{$leg['hotel']['name']}}</td>
                                            	<td>{{$leg['hotel']['address1']}}</td>
                                            	<td>{{date('d M Y', strtotime($leg['hotel']['checkin']))}}</td>
                                            	<td>{{date('d M Y', strtotime($leg['hotel']['checkout']))}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="itinerary_hotel_list pl-3 pr-3 pb-3 pt-3 mb-3">
                            <div class="border-bottom hotlename pb-2   mb-3 ">
                                <div class="row">
                                    <div class="col-sm-7 col-xl-8">
                                    <h4>14 DAY SURF CAMP</h4>
                                    <div class="media">
                                        <i class="ic-place"></i>
                                        <div class="media-body pb-3 mb-0">
                                        Location: Country, City / Suburb
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-5 col-xl-4">
                                    <div class="row align-items-end">
                                        <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                            From <strong>$AUD 2799.00</strong> Per Person
                                        </div>
                                        <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                        <h2 class="font-weight-bold mb-0">14</h2>
                                        Nights
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3 ">
                                    <img src="images/accomodation-default-image.jpg" alt="" />
                                </div>
                                <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-9 ">
                                    <div class="rating">
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-half_star fa-lg"></i></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                            <ul class="pl-4 pt-4 ">
                                                <li>VIP Room Upgrades &amp; More</li>
                                                <li>Popular property highlights</li>
                                                <li>Free WiFi</li>
                                                <li>Restaurant</li>
                                            </ul>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                            <ul class="pl-4 pt-4 ">
                                                <li>Smoke-free property</li>
                                                <li>Babysitting or childcare (surcharge)</li>
                                                <li>24-hour front desk</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p class="pt-3">Along with a restaurant, this hotel has an outdoor pool and a health club. Free WiFi in public areas and free self parking are also provided. Additionally, a bar/lounge, a coffee shop/café, and a sauna are onsite.</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endif
                        @if(isset($leg['activities']) && !empty($leg['activities']))
                        <div class="tripDetails-container mb-3">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-local_activity"></i></span> <strong>TOURS / ACTIVITIES</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <a href="#" class="text-dark"><i class="ic-create"></i></a>
                                        <a href="#" class="text-dark"><i class="ic-block"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
				                            @foreach($leg['activities'] as $activity)
                                            <tr>
                                                <td>{{date('d M Y', strtotime($activity['date_selected']))}}</td>
				                                <td>{{$activity['name']}}</td>
				                                <td>{{ $activity['location'] != ''?$activity['location']:'-' }}</td>
				                                <td>{{$activity['duration1']}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if(isset($leg['transport']) && !empty($leg['transport']))
                        <div class="tripDetails-container">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-directions_bus"></i></span> <strong>TRANSPORT</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <a href="#" class="text-dark"><i class="ic-create"></i></a>
                                        <a href="#" class="text-dark"><i class="ic-block"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
				                              	<th>Name</th>
				                              	<th>Departure Time</th>
				                              	<th>Arrival / Time</th>
				                              	<th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
				                                $travelDate = explode('<br/>', $leg['transport']['departure_text']);
				                                $travelDate = explode('@', $travelDate[1]);
				                                $departure_time = explode('<br/>', $leg['transport']['departure_text']);
				                                $arrival_time = explode('<br/>', $leg['transport']['arrival_text']);
				                            @endphp
				                            <td><?php echo date('d M Y', strtotime($travelDate[0])); ?></td>
				                            <td>{{ $leg['transport']['transport_name_text'] }}</td>
				                            <td>{{$departure_time[1]}}</td>
				                            <td>{{$arrival_time[1]}}</td>
				                            <td>{{ str_replace('+', '', $leg['transport']['duration']) }}</td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            	@php $i++; @endphp
                @endforeach
                @endif
            </section>
        </div>
    </div>
</div>
<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
<input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
@endsection

@push('scripts')
   	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
   	<script src="{{url('js/itinerary/common.js')}}"></script>
   	<script src="{{url('js/booking-summary.js')}}"></script>
   	<script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
    <script src="{{url('js/markerlabel.js') }}"></script>
   	<script src="{{url('js/eroam-map_managetrip.js')}}"></script>
   	<script type="text/javascript">
   		$(document).ready(function() {
		    tripHideShow();
		});

		function tripHideShow() {
		    $('.trip-viewmore').click(function() {
		        var href_value = $(this).attr('data-id');
		        $('#' + href_value).toggle();
		        $('#' + href_value).toggleClass("open");
		        $(this).toggleClass("active");
		    });
		}

		function setupLabel() {
		    if ($('.label_radio input').length) {
		        $('.label_radio').each(function() {
		            $(this).removeClass('r_on');
		        });
		        $('.label_radio input:checked').each(function() {
		            $(this).parent('label').addClass('r_on');
		            var inputValue = $(this).attr("value");
		            //alert(inputValue);
		            $("." + inputValue).addClass('mode-block');
		            $("." + inputValue).siblings().removeClass('mode-block');
		        });
		    };

		    if ($('.label_check input').length) {
		        $('.label_check').each(function() {
		            $(this).removeClass('c_on');
		        });
		        $('.label_check input:checked').each(function() {
		            $(this).parent('label').addClass('c_on');
		        });
		    };

		};
		$(window).load(eMap.init);

		function map() {
		    // When the window has finished loading create our google map below
		    google.maps.event.addDomListener(window, 'load', init);

		    function init() {
		        // Basic options for a simple Google Map
		        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
		        var mapOptions = {
		            // How zoomed in you want the map to start at (always required)
		            zoom: 11,

		            // The latitude and longitude to center the map (always required)
		            center: new google.maps.LatLng(40.6700, -73.9400), // New York

		            // How you would like to style the map.
		            // This is where you would paste any style found on Snazzy Maps.
		            styles: [{
		                "featureType": "administrative",
		                "elementType": "labels.text.fill",
		                "stylers": [{
		                    "color": "#444444"
		                }]
		            }, {
		                "featureType": "landscape",
		                "elementType": "all",
		                "stylers": [{
		                    "color": "#f2f2f2"
		                }]
		            }, {
		                "featureType": "poi",
		                "elementType": "all",
		                "stylers": [{
		                    "visibility": "off"
		                }]
		            }, {
		                "featureType": "road",
		                "elementType": "all",
		                "stylers": [{
		                    "saturation": -100
		                }, {
		                    "lightness": 45
		                }]
		            }, {
		                "featureType": "road.highway",
		                "elementType": "all",
		                "stylers": [{
		                    "visibility": "simplified"
		                }]
		            }, {
		                "featureType": "road.arterial",
		                "elementType": "labels.icon",
		                "stylers": [{
		                    "visibility": "off"
		                }]
		            }, {
		                "featureType": "transit",
		                "elementType": "all",
		                "stylers": [{
		                    "visibility": "off"
		                }]
		            }, {
		                "featureType": "water",
		                "elementType": "all",
		                "stylers": [{
		                    "color": "#46bcec"
		                }, {
		                    "visibility": "on"
		                }]
		            }]
		        };

		        // Get the HTML DOM element that will contain your map
		        // We are using a div with id="map" seen below in the <body>
		        var mapElement = document.getElementById('map');

		        // Create the Google Map using our element and options defined above
		        var map = new google.maps.Map(mapElement, mapOptions);

		        // Let's also add a marker while we're at it
		        var marker = new google.maps.Marker({
		            position: new google.maps.LatLng(40.6700, -73.9400),
		            map: map,
		            title: 'eRoam!'
		        });
		    }
		}
		function tripHotelHideShow(){
          	$('.tourTripName').click(function(){
              	var href_value = $(this).attr('data-id');
              	$('#'+href_value).show();
              	$('#'+href_value).addClass("open");
              	$('#'+href_value).siblings('.tripHotel-wrapper').hide();
              	$('#'+href_value).siblings('.tripHotel-wrapper').removeClass('open');
              	$('.tourTripName').removeClass('active');
              	$(this).addClass("active");
          	});
      	}
   </script>
@endpush