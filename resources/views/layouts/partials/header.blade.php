<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>:: eRoam ::</title>
	<link rel="shortcut icon" href="{{ asset('images/icons/favicon.ico') }}" >
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<link rel="stylesheet" href="{{ url('css/app.css') }}" />
	<link href="{{ url( 'css/datepicker.css' ) }}" rel="stylesheet">
    <?php $currentPath= Route::getFacadeRoot()->current()->uri();?>
    @if($currentPath == 'map')
	<script>
		window.domLoadEventFired = true;
		if(document.readyState == "complete" || document.readyState == "interactive"){
		}else{
			function itinerary_messages(){
				if($('#map-loader:visible').length == 0){
					stop_itenary_message_interval();
					return false;
				}
				if($('#map-loader').find($('#itenary_message_id').length)){
					var itenary_message_id  = $('#itenary_message_id').text();
					if(itenary_message_id == ''){
						$('#itenary_message_id').text('Finding current trending cities...')
					}
					if(itenary_message_id == 'Finding current trending cities...'){
						$('#itenary_message_id').text('Plotting best route...')
					}
					if(itenary_message_id == 'Plotting best route...'){
						$('#itenary_message_id').text('Finding available trending hotels...')
					}
					if(itenary_message_id == 'Finding available trending hotels...'){
						$('#itenary_message_id').text('Finding available trending transport...')
					}
					if(itenary_message_id == 'Finding available trending transport...'){
						$('#itenary_message_id').text('Finding available trending activities...')
					}
					if(itenary_message_id == 'Finding available trending activities...'){
						$('#itenary_message_id').text('The best trending package with be displayed in seconds...')
					}
					if(itenary_message_id == 'The best trending package with be displayed in seconds...'){
						$('#itenary_message_id').text('Finding current trending cities...')
						
					}
				}
			}
			var itenary_message_interval = setInterval(itinerary_messages,5000);
				
			function stop_itenary_message_interval() {
		    	clearInterval(itenary_message_interval);
			}
		}
	</script>
	@endif
</head>

<body>
	<!-- <div class="bottom_chat">
	    <span class="chat_msg_circle">1</span>
	    <i class="ic-chat"></i>
	</div> -->
	<div class="loader" style="display:none;"><img src="{{ url( 'images/loader.gif' ) }}" alt="Loader" /></div>		
	<header class="fixed-top">
	    <nav class="navbar navbar-expand-md navigation">
	        <div class="container-fluid">
	            <a href="{{ url('/') }}" class="logo"><img src="{{ url('images/logo.svg') }}" alt="" /></a>
	            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	                <span class="navbar-toggler-icon"></span>
	                <span class="navbar-toggler-icon"></span>
	                <span class="navbar-toggler-icon"></span>
	            </button>
	            <div class="collapse navbar-collapse" id="navbarCollapse">
	                <ul class="nav justify-content-end ml-auto">
	                	@if (session()->has('user_auth'))
	                		<li  class="nav-item dropdown @if($currentPath == 'profile') active @endif">
		                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <i class="ic-expand_more"></i></a>
		                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		                      	<a href="{{ url('profile/step1') }}" class="dropdown-item">Profile</a>
		                      	<a href="{{ url('profile/step2') }}" class="dropdown-item">Addresses</a>
		                      	<a href="{{ url('profile/step3') }}" class="dropdown-item">Personal Preferences</a>
		                      	<a href="{{ url('profile/step4') }}" class="dropdown-item">Contact Preferences</a>
		                      	<a data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" class="dropdown-item">Manage Trips</a>		
		                      	<a href="{{ url('logout') }}" class="dropdown-item">Sign Out</a>
		                    </div>
		                </li> 
	                	@else	                    
	                    <li class="nav-item"><a class="nav-link active" href="#" id="login-modal" data-toggle="modal" data-target="#exampleModalCenter" data-target=".bd-example-modal-lg">Sign In</a></li>
	                    <li class="nav-item"><a class="nav-link" href="#" id="signup-modal" data-toggle="modal" data-target="#exampleModalCenter" data-target=".bd-example-modal-lg">Sign Up</a></li>
	                    @endif
	                    <li class="nav-item dropdown">
	                        <a class="nav-link dropdown-toggle" href="#" data-toggle="tooltip" data-placement="bottom" title="Not available in Pilot">Melbourne, AU <i class="ic-expand_more"></i>
	             			</a>
	                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	                            <a class="dropdown-item" href="#">Search City (Global)</a>
	                            <a class="dropdown-item" href="#">Melbourne, AU</a>
	                            <a class="dropdown-item" href="#">Sydney, AU</a>
	                            <a class="dropdown-item" href="#">Brisbane, AU</a>
	                            <a class="dropdown-item" href="#">Perth, AU</a>
	                            <a class="dropdown-item" href="#">Adelaide, AU</a>
	                            <a class="dropdown-item" href="#">Hobart, AU</a>
	                            <a class="dropdown-item" href="#">Canberra, AU</a>
	                        </div>
	                    </li>
						
	                    <li class="nav-item dropdown">
	                        <a class="nav-link dropdown-toggle" href="#"  data-toggle="tooltip" data-placement="bottom" title="Not available in Pilot">$AUD <i class="ic-expand_more"></i></a>
	                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	                            <a class="dropdown-item" href="#">Sydney, AU</a>
	                            <a class="dropdown-item" href="#">Brisbane, AU</a>
	                            <a class="dropdown-item" href="#">Perth, AU</a>
	                        </div>
	                    </li>
						<li>
							<div class="col-md-2 col-sm-2 col-xs-12">
							</div>	
						</li>
	                </ul>
	            </div>
	        </div>
	    </nav>
	</header>