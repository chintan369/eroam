@extends('layouts.partials.tour')
@section('content-tour')
<div class="itinerary_right tours-wrapper">
	<div class="right_form_click_block">
	    <a href="javascript:void();" class="d-block click_map_hide text-center pt-3 pb-3"><i class="ic-navigate_before"></i></a>
	</div>

	<div>
	    <div class="tabs-search-container pt-4 px-1 px-md-2">
	        <div class="container-fluid">
	            <div class="row">
	              	<div class="col-sm-6">
		                <div class="pb-3 mb-0 lh-125">
		                	@php
		                	if(array_key_exists('countryNames', $tourCountries)){ 
		                        $tCount = 0;
		                        foreach ($tourCountries['countryNames'] as $country_name) 
		                        {
		                          	$tCount = $tourCount[$country_name] + $tCount;
		                        }
		                		@endphp
		                  		<h5><strong class="d-block text-gray-dark">Total : <span class="tour-count">0 Tours Found</span></strong></h5>
		                 	@php 
		                 	} else 
		                 	{ @endphp
                    			
                    			<h5><strong class="d-block text-gray-dark"><span class="tour-count"></span></strong></h5>
                  			@php 
                  			} 
                  			@endphp
		                  	<span class="d-block"><i class="ic-calendar"></i><span id="dept_dates"></span></span>
		                </div>
	              	</div>
	              	<div class="col-sm-6">
	                <div class="input-group mb-3">
	                  <input type="text" class="form-control border-light border-right-0" placeholder="Search" id="tourSearch" value="<?php if(!empty($_POST) && array_key_exists('tourSearch', $_POST) ) { echo $_POST['tourSearch']; } ?>"  name="tourSearch">
	                  <div class="input-group-append">
	                    <button class="btn btn-outline-light border-left-0" type="button"><i class="fas fa-search" onclick="getTourSearch();"></i></button>
	                  </div>
	                </div>
	              </div>
	            </div>
	            <hr>
	            <div class="row">
	              <div class="col-sm-10 tour-tabs">
	                <ul class="nav nav-pills nav-justified">
	                  <li class="nav-item">
	                    <a class="nav-link" href="" class="disabled" id="top-picks-tab" role="tab" aria-controls="top-picks" aria-expanded="true" disabled  data-toggle="tooltip" title="Not Available in Pilot">TOP PICKS</a>
	                  </li>
	                  <li class="nav-item">
	                    <a class="nav-link" href="" class="dropdown-toggle disabled" id="myTabDrop1"  aria-controls="myTabDrop1-contents"  disabled data-toggle="tooltip" title="Not Available in Pilot">TYPE <span class="fa fa-caret-down"></span></a>
	                  </li>
	                  <li class="nav-item">
	                    <a class="nav-link" href="" class="dropdown-toggle" id="priceSortAsc" data-toggle="dropdown" aria-controls="myTabDrop3-contents">PRICE <span class="fa fa-caret-down"></span></a>
	                  </li>
	                </ul>
	              </div>
	              <div class="col-sm-2 text-sm-right exchange-tabs">
	                <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
	                  <li class="nav-item">
	                    <a class="nav-link active" id="listview-tab" data-toggle="pill" href="#listview" role="tab" aria-controls="listview" aria-selected="true"><i class="ic-menu"></i></a>
	                  </li>
	                  <li class="nav-item">
	                    <a class="nav-link" id="gridview-tab" data-toggle="pill" href="#gridview" role="tab" aria-controls="gridview" aria-selected="false"><i class="ic-apps"></i></a>
	                  </li>
	                </ul>
	              </div>
	            </div>
	        </div>
	    </div>
	    <div class="tabs-content-container my-3 px-1 px-md-2">
	        <div class="container-fluid tabs-content-scroll-wrapper pt-1">
	            <div class="tab-content">

	            	<div id="tours-loader">
                        <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
                    </div>

	            	<div class="tab-pane fade show active" role="tabpanel" id="listview" aria-labelledby="listview-tab">
	            		<div class="list-wrapper new-list-wrapper" id="listTourList">
                        </div>
	            	</div>

	            	<div class="tab-pane fade" id="gridview" aria-labelledby="gridview-tab" role="tabpanel">
	            		<div class="grid-wrapper grid-tours row" id="gridTourList">
                        </div>
	            	</div>

	            	<div id="more-loader">
                        <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
                    </div>	                
	            </div>
	        </div>
	    </div>
	</div>                   
</div>
<script type="text/javascript">
var countrName 			= '<?php echo $countryName; ?>';
var default_currency 	= '<?php echo $default_currency; ?>';
var search_type 		= '<?php echo $search_type;?>';
var search_value 		= '<?php echo $search_value;?>';
var remove_country_id 	= '<?php echo $remove_country_id;?>';
var city_name 			= '<?php echo @$city_name;?>';
var tour_type 			= '<?php echo $tour_type?>';
var dep_date 			= '<?php echo $dep_date?>';
var search_date			= '<?php echo $search_date; ?>';
var country_id_remove_push = '<?php echo @$country_id_remove_push;?>';
var default_selected_city = "<?php echo session()->get( 'default_selected_city' ); ?>";
var APP_URL 			=  {!! json_encode(url('/')) !!};
</script>
@stop
