@extends('layouts.common')
@section('content')
@include('partials.banner')
@include('partials.search')
<div class="account-block">
  @include('users.partials.sidebar')
  <div class="account-right p-4 pl-5">
    @include('users.partials.complete_profile')
    @if($link != '')
    <div class="mt-4 row">
      <div class="col-xl-4 offset-xl-8 col-sm-6 offset-sm-6">
        <div class="row">
          <div class="col-sm-5 col-5">
            <a href="{{ $link }}" class="btn btns_input_dark btn-block">UPDATE</a>
          </div>
          <div class="col-sm-7 col-7">Update your personal travel preferences.</div>
        </div>
      </div>
    </div>
    @endif
    @if(session()->has('profile_step3_success'))
    <p class="success-box m-t-30">
      {{ session()->get('profile_step3_success') }}
    </p>
    @endif
    @if(session()->has('profile_step3_error'))
    <p class="danger-box m-t-30">
      {{ session()->get('profile_step3_error') }}
    </p>
    @endif
    <hr class="mt-5">
    <h5 class="mt-4">Personal Preferences</h5>
    <form class="mt-3" method="post" id="profile_step3_form">
      <div class="form-group">
        <div class="fildes_outer">
          <label>Select Age Group</label>
          <div class="custom-select">
            <select id="age-group" name="age_group" class="form-control age_group_id">
                <option value="">Select Age Group</option>
                @foreach ($travel_preferences['age_groups'] as $age_group)
                    <option value="{{ $age_group['id'] }}" {{ $user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : '' }}>{{ $age_group['name'] }}</option>
                @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="fildes_outer">
          <label>Select Transport Type</label>
          <div class="custom-select">
            <select class="form-control transport_type_optionsp" name="transport_types[]" id="dropdown-transport">
              <option value="">Select Transport Type</option>                
                <option value="1" {{ $user->customer->pref_transport_types == 1 ? 'selected' : '' }}>Flight</option>
                <option value="25" {{ $user->customer->pref_transport_types == 25 ? 'selected' : '' }}>Overland</option>
                <?php /*{!! $transport_type_options !!}*/ ?>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="fildes_outer">
          <label>Select Gender</label>
          <div class="custom-select">
            <select class="form-control gender_sel" name="gender">
                <option value="">Select Gender</option>
                <option value="male" {{ $user->customer->pref_gender == 'male' ? 'selected' : '' }}>Male</option>
                <option value="female" {{ $user->customer->pref_gender == 'female' ? 'selected' : '' }}>Female</option>
                <option value="other" {{ $user->customer->pref_gender == 'other' ? 'selected' : '' }}>Other</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="fildes_outer">
          <label>Select Accommodation Type</label>
          <div class="custom-select">
            <select class="form-control hotel_category"  name="hotel_category_id[]">
                <option>Select Accommodation Type</option>
                <option value="1" {{ $user->customer->pref_hotel_categories && in_array(1, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 1 Star</option>
                <option value="5" {{ $user->customer->pref_hotel_categories && in_array(5, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 2 Star</option>
                <option value="2" {{ $user->customer->pref_hotel_categories && in_array(2, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 3 Star</option>
                <option value="3" {{ $user->customer->pref_hotel_categories && in_array(3, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 4 Star</option>
                <option value="9" {{ $user->customer->pref_hotel_categories && in_array(9, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : '' }}> 5 Star</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="fildes_outer">
          <label>Select Nationality</label>
          <div class="custom-select">
            <select id="nationality"  name="nationality" class="form-control nationality_id" >
                <option value="">Select Nationality</option>
                @foreach ($travel_preferences['nationalities']['featured'] as $nationality)
                    <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                @endforeach
                    <option disabled> ======================================== </option>
                @foreach ($travel_preferences['nationalities']['not_featured'] as $nationality)
                    <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="fildes_outer">
          <label>Select Room Type</label>
          <div class="custom-select">
            <select class="form-control room_type_optionsp" name="room_type">
                <option>Select Room Type</option>
                <option value="2" {{ $user->customer->pref_hotel_room_types && in_array(2, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Single</option>
                <option value="1" {{ $user->customer->pref_hotel_room_types && in_array(1, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Twin</option>
                <option value="8" {{ $user->customer->pref_hotel_room_types && in_array(8, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Triple</option>
                <option value="22" {{ $user->customer->pref_hotel_room_types && in_array(22, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : '' }}>Quad</option>
            </select>
          </div>
        </div>
      </div>
      <div class="mt-3">
        <h5>Activity Preferences</h5>
        <p>Please drag your preferred activities into the boxes below. Highest priority begins on the left.</p>
        <div class="dragdrop-list mt-4">
          <ul class="list-unstyled clearfix">
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
            <li ondrop="drop(event)" ondragover="allowDrop(event)"></li>
          </ul>
          <hr>
          <ul class="list-unstyled" id="drop-listss">

                  <?php 
                      $imgArr = array(
                        15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                        5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                        13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                        29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                        11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                        20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                         //'Expeditions','Astronomy','Food','Short-Breaks'
                        );

                      $ij = 0;
                      if( count($interest_ids) > 0 ){
                        foreach ($interest_ids as $j => $interest_id){
                          $key = array_search($interest_id, array_column($labels, 'id'));
                          $label = $labels[$key];
                          $ij++;
                  ?>
                            <li ondrop="dropp(event)" ondragover="allowDropp(event)" class="interest-button-active" id="interestp-{{ $label['id'] }}" data-sequence="{{ $ij }}" data-name="{{ $label['name'] }}" style="width:81px !important;height: 81px !important">
                              <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="dragp(event)" id="dragp_{{ $label['id'] }}" class="img-responsive" alt="{{ $j }} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                              <input type="hidden" id="inputValuep_{{ $label['id'] }}" class="input-interest" name="interests[{{$j + 1}}]" value="{{ $label['id'] }}">
                            </li>
                  <?php  } } ?>

                  <?php 
                    for($k = $ij; $k < 8; $k++){
                      echo '<li ondrop="dropp(event)" ondragover="allowDropp(event)" class="blankInterests" data-sequence="'.($k + 1).'"  style="width:81px !important;height: 81px !important"></li>';
                    }
                  ?>
                </ul>
        </div>
      </div>
      <div class="clearfix"></div>
       <button type="submit" name="" id="save_travel_preference" class="btn btns_input_dark btn-block pb-2 pt-2  mt-5">UPDATE PROFILE</button>
        <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
  </div>
</div>
<script src="{{ url('js/users/profile_step3.js' ) }}"></script>
<script type="text/javascript" src="{{ url('js/users/draggable-0.1.js' ) }}"></script>
<script type="text/javascript">
var activitySequence = [{{ join(', ', $interest_ids) }}];
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}
function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
</script>
@stop