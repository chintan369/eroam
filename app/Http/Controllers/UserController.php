<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\ApiCache;
use App\Http\Requests;
use Cache;
use Validator;
use Response;
use Redirect;

use Auth;
use \App\User;

class UserController extends Controller
{
	private $headers = [];

	public function __construct() {
		$this->headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];
		
	}

	public function login_view() {
		if (!session()->has('initial_authentication')) {
			return redirect('app/login');
		}

		
		if (session()->has('user_auth')) {
			return redirect('/');
		}
		$ip_address = '137.59.252.196';

        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
     
        $countryName = $query['country'];
        $cities = Cache::get('cities');
        $countries = Cache::get('countries');
        $labels    = Cache::get('labels');
        $travellers = Cache::get('travellers');
        $travel_pref = [];
        $interest_ids = [];
        if( session()->has('travel_preferences') )
        {
            $travel_pref = session()->get('travel_preferences');
            $travel_pref = reset( $travel_pref );
            $interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
        }
        return view(
             'users.login',
            [
            	'travelers'   => $travellers,
                'travel_pref'  => $travel_pref,
                'interest_ids' => $interest_ids,
                'countryName'  => $countryName,
				'default_currency'   => 'AUD'
            ]
        );
	}

	public function login() {
		$username = request()->input('username_login');
		$password = request()->input('password_login');

		$response = http('post', 'user/check',['username' => $username,'password'=>$password] , $this->headers);
		
		if ($response['d']['status'] == 'invalid') {			
			return 'invalid';
		} 
		else if($response['d']['status'] == 'confirm_first')
		{
			return 'confirm_first';
		}			
		else {
			$user_code = $response['d']['code'];
			session()->put('user_auth', $user_code);
			return 'valid';
		}
	}

	public function logout() {
		session()->forget('user_auth');
		session()->forget('travel_preferences');
		return redirect('/');
	}

	public function profile_view() {
		$id = session()->get('user_auth')['id'];

		$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user = json_decode(json_encode($user), false);

		// GET UPDATED PROFILE PIC
		$session = session()->get('user_auth');
		$session['image_url'] = isset( $user->customer->image_url ) ? $user->customer->image_url : null;
		$session['user_id'] = $user->id;
		session()->put('user_auth', $session);

		// Preferences
		$labels = http('get', 'labels', [], $this->headers);
		$travel_preferences = http('get', 'traveler-options', [], $this->headers);


		$travel_preferences['categories'] = [];
		$travel_preferences['categories'][1]['id'] = 5;
		$travel_preferences['categories'][1]['name'] = 'Camping';
		$travel_preferences['categories'][2]['id'] = 3;
		$travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
		$travel_preferences['categories'][3]['id'] =2;
		$travel_preferences['categories'][3]['name'] = '4 Star (Deluxe)';
		$travel_preferences['categories'][5]['id'] = 4;
		$travel_preferences['categories'][5]['name'] = '2 Star (Backpacker / Guesthouse)';
		$travel_preferences['categories'][9]['id'] =1;
		$travel_preferences['categories'][9]['name'] = '5 Star (Luxury)';
		
		$this->session_user_pref($user, $labels, $travel_preferences);

		$itineraries = http('post', 'user/get-itineraries', ['customer_id' => $user->customer->id], $this->headers);
		$itineraries = json_decode(json_encode($itineraries), false);
		return view('user.profile')->with(compact('user', 'labels', 'travel_preferences', 'itineraries'));
	}

	public function profile_step1()
	{
		$id = session()->get('user_auth')['id'];
		$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user = json_decode(json_encode($user), false);		
		
		// GET UPDATED PROFILE PIC
		$session = session()->get('user_auth');
		$session['image_url'] = isset( $user->customer->image_url ) ? $user->customer->image_url : null;
		$session['user_id'] = $user->id;
		session()->put('user_auth', $session);

		// Preferences
		$labels = http('get', 'labels', [], $this->headers);
		$travel_preferences = http('get', 'traveler-options', [], $this->headers);

		$travel_preferences['categories'] = [];
		$travel_preferences['categories'][1]['id'] = 5;
		$travel_preferences['categories'][1]['name'] = 'Camping';
		$travel_preferences['categories'][2]['id'] = 3;
		$travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
		$travel_preferences['categories'][3]['id'] =2;
		$travel_preferences['categories'][3]['name'] = '4 Star (Deluxe)';
		$travel_preferences['categories'][5]['id'] = 4;
		$travel_preferences['categories'][5]['name'] = '2 Star (Backpacker / Guesthouse)';
		$travel_preferences['categories'][9]['id'] =1;
		$travel_preferences['categories'][9]['name'] = '5 Star (Luxury)';
		
		$this->session_user_pref($user, $labels, $travel_preferences);
		
		
		$percent 			= self::getCompletedStep($user);
		$link 	 			= self::getRedirectLink($user);
		$banner_data    	= self::get_home_banner_data($user);
		$from_city_id 		= $banner_data['from_city_id'];
		$from_country_id 	= $banner_data['from_country_id'];	
		$travellers 		= $banner_data['travellers'];
		$countries1 		= $banner_data['countries1'];
		$interest_ids 		= $banner_data['interest_ids'];
		$cities 			= $banner_data['cities'];
		$countries 			= $banner_data['countries'];
		$labels 			= $banner_data['labels'];
		$version 			= config()->get('services.setting.version');
		
		return view('users.profile_step1')->with(compact('user', 'labels', 'travel_preferences', 'itineraries','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent','link'));
	}

	public function profile_demostep1()
	{
		$id = session()->get('user_auth')['id'];
		$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user = json_decode(json_encode($user), false);		
		
		// GET UPDATED PROFILE PIC
		$session = session()->get('user_auth');
		$session['image_url'] = isset( $user->customer->image_url ) ? $user->customer->image_url : null;
		$session['user_id'] = $user->id;
		session()->put('user_auth', $session);

		// Preferences
		$labels = http('get', 'labels', [], $this->headers);
		$travel_preferences = http('get', 'traveler-options', [], $this->headers);

		$travel_preferences['categories'] = [];
		$travel_preferences['categories'][1]['id'] = 5;
		$travel_preferences['categories'][1]['name'] = 'Camping';
		$travel_preferences['categories'][2]['id'] = 3;
		$travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
		$travel_preferences['categories'][3]['id'] =2;
		$travel_preferences['categories'][3]['name'] = '4 Star (Deluxe)';
		$travel_preferences['categories'][5]['id'] = 4;
		$travel_preferences['categories'][5]['name'] = '2 Star (Backpacker / Guesthouse)';
		$travel_preferences['categories'][9]['id'] =1;
		$travel_preferences['categories'][9]['name'] = '5 Star (Luxury)';
		
		$this->session_user_pref($user, $labels, $travel_preferences);
		
		
		$percent 			= self::getCompletedStep($user);
		$link 	 			= self::getRedirectLink($user);
		$banner_data    	= self::get_home_banner_data($user);
		$from_city_id 		= $banner_data['from_city_id'];
		$from_country_id 	= $banner_data['from_country_id'];	
		$travellers 		= $banner_data['travellers'];
		$countries1 		= $banner_data['countries1'];
		$interest_ids 		= $banner_data['interest_ids'];
		$cities 			= $banner_data['cities'];
		$countries 			= $banner_data['countries'];
		$labels 			= $banner_data['labels'];
		$version 			= config()->get('services.setting.version');
		
		return view('users.profile_step1')->with(compact('user', 'labels', 'travel_preferences', 'itineraries','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent','link'));
	}

	public function update_travel_preferences() {
		$input = request()->input();
		$response = http('post', 'user/update-preferences', $input, $this->headers);
		return redirect()->back()->with('success', 'Travel preferences successfully saved!');
	}

	public function view_itinerary($reference_no) {
		$response = http('post', 'user/get-itinerary', ['reference_no' => $reference_no], $this->headers);
		if ($response) {
			$search_session = json_decode($response['search_session'], true);
			$itinerary['title'] = get_city_by_id($response['from_city_id'])['name'].' to '.get_city_by_id($response['to_city_id'])['name'];
			$itinerary['total_amount'] = $response['total_amount'];
			$itinerary['total_per_person'] = $response['total_per_person'];
			$itinerary['total_nights'] = $response['total_days'];
			$itinerary['currency'] = $response['currency'];
			$itinerary['number_of_travellers'] = $response['num_of_travellers'];
			$itinerary['travel_date'] = $response['travel_date'];
			$itinerary['legs'] = [];

			foreach ($search_session['itinerary'] as $key => $value) {
				$itinerary['legs'][$key]['city'] = get_city_by_id($value['city']['id']);
				// HOTELS
				if ($value['hotel'] && isset($value['hotel']['provider'])) { // PROVIDED BY API
					$itinerary['legs'][$key]['hotel']['name'] = $value['hotel']['name'];
					$itinerary['legs'][$key]['hotel']['room_type'] = $value['hotel']['room_type'];
					$itinerary['legs'][$key]['hotel']['price'] = number_format($value['hotel']['price'], 2);
					$itinerary['legs'][$key]['hotel']['description'] = $value['hotel']['description'];
					$itinerary['legs'][$key]['hotel']['cancellation_policy'] = $value['hotel']['cancellation_policy'];
				} else if ($value['hotel'] && !isset($value['hotel']['provider'])) { // PROVIDED BY EROAM
					$itinerary['legs'][$key]['hotel']['name'] = $value['hotel']['name'];
					$itinerary['legs'][$key]['hotel']['room_type'] = array_last($value['hotel']['price'])['room_type']['name'];
					$itinerary['legs'][$key]['hotel']['price'] = number_format(array_last($value['hotel']['price'])['price'], 2);
					$itinerary['legs'][$key]['hotel']['description'] = $value['hotel']['description'];
					$itinerary['legs'][$key]['hotel']['cancellation_policy'] = $value['hotel']['cancellation_policy'];
				} else { // OWN ARRANGEMENT
					$itinerary['legs'][$key]['hotel'] = null;
				}

				// CTIVITIES
				if ($value['activities']) {
					foreach ($value['activities'] as $act_key => $activity) {
						$itinerary['legs'][$key]['activities'][$act_key]['name'] = $activity['name'];
						$itinerary['legs'][$key]['activities'][$act_key]['price'] = number_format(array_last($activity['price'])['price'], 2);
						$itinerary['legs'][$key]['activities'][$act_key]['description'] = $activity['description'];
					}
				} else {
					$itinerary['activities'] = null;
				}

				// Transport
				if ($value['transport']) {
					$itinerary['legs'][$key]['transport']['destination'] = get_city_by_id($value['transport']['to_city_id'])['name'];
					$itinerary['legs'][$key]['transport']['price'] = number_format(array_last($value['transport']['price'])['price'], 2);
					$itinerary['legs'][$key]['transport']['transport_type'] = $value['transport']['transport_type']['name'];
				} else {
					$itinerary['legs'][$key]['transport'] = null;
				}
			}

			// CONVERT TO OBJECT
			$itinerary = json_decode(json_encode($itinerary), false);
			return view('user.view-itinerary')->with(compact('itinerary'));
		} else {
			return abort(404);
		}
	}

	private function session_user_pref($user_data, $labels, $travel_prefs){
		$user = $user_data->customer;

		$user_preferences = [];

		$user_preferences['interestListIds'] = $user->interests ? explode(',', $user->interests) : [];
		$user_preferences['accommodation'] = $user->pref_hotel_categories ? explode(',',$user->pref_hotel_categories) : [];
		$user_preferences['room'] = $user->pref_hotel_room_types ? explode(',', $user->pref_hotel_room_types) : []; 
		$user_preferences['transport'] = $user->pref_transport_types ? explode(',', $user->pref_transport_types) : [];
		
		$labels = get_pref_by_id($labels, $user_preferences['interestListIds'], 'name', false);
		$accommodations = get_pref_by_id($travel_prefs['categories'], $user_preferences['accommodation'], 'name');
		$transport_name = get_pref_by_id($travel_prefs['transport_types'], $user_preferences['transport'], 'name');
		$room_name = get_pref_by_id($travel_prefs['room_types'], $user_preferences['room'], 'name');
		$fet_nationality = get_pref_by_id($travel_prefs['nationalities']['featured'], [$user->pref_nationality_id], 'name', false);
		$nationality = get_pref_by_id($travel_prefs['nationalities']['not_featured'], [$user->pref_nationality_id], 'name', false);
		$age_group = get_pref_by_id($travel_prefs['age_groups'], [$user->pref_age_group_id], 'name', false);
	
		$user_preferences['interestLists'] = $labels;
		$user_preferences['gender'] = $user->pref_gender;
		$user_preferences['nationality'] = $nationality ? $nationality : $fet_nationality;
		$user_preferences['accommodation_name'] = $accommodations;
		$user_preferences['transport_name'] = $transport_name;
		$user_preferences['room_name'] = $room_name;
		$user_preferences['age_group'] = $age_group;
	
		session()->put('travel_preferences', [$user_preferences]);
	}
	public function save_travel_preferences() {
		$input = request()->input();
		$response = http('post', 'user/update-preferences', $input, $this->headers);
	}

	public function forgot_password_view() {
		$ip_address = '137.59.252.196';

        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
        $countryName = $query['country'];
        return view(
             'user.forgot',
            [
                'countryName'  => $countryName,
				'default_currency'   => 'AUD'
            ]
        );
	}

	function reset_password_view($password_token){

		$data = [
			'token' => $password_token
		];
		$response = http( 'post', 'user/check-token', $data, $this->headers);
	
		if($response['response_status']){
			$ip_address = '137.59.252.196';

	        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
	        $countryName = $query['country'];
	        $default_currency = 'AUD';
	        
			return view('user.reset-password')->with(compact('response', 'data','countryName','default_currency'));
		}else{
			if($response['expire'])
			{
				return redirect('/')->with('reset-success-done', 'Your password link is expire now.');
			}
			else
			{
				return redirect('/')->with('reset-success-done', 'You already changed your password.');
			}
		}
	}

	function send_reset_password()
	{
		$id = request()->input('code');
		$password = request()->input('password');
		$token = request()->input('token');

		$validate = Validator::make(request()->all(), [
			'password' => 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6',
		]);

		if ($validate->fails()) 
		{
			return back()->withErrors($validate, 'reset');			
		}
		else
		{
			$data = [
				'id' => $id,
				'password' => $password,
				'token' => $token
			];

			$response = http( 'post', 'user/reset-password', $data, $this->headers);

			if($response['response_status'])
			{
				return redirect('/')->with('reset-success', 'Password Successfully Changed');
			}
			else
			{
				return back()->with('error','Oops, something went wrong!');
			}			
		}
	}

	public function send_registration( Request $request ){
		$email 		= $request->input('reg_email');
        $password 	= $request->input('reg_pass');

        $validator 	= Validator::make($request->all(), [
            'reg_email' => 'required|email|max:255',
            'reg_pass' => 'required|min:6',
        ]);

        $input 	= $request->all();

        if ($validator->passes()) {
			$data = [];

	        $data['email'] 		= $email;
	        $data['password'] 	= $password;        
	        $data['url'] 		= url('');
	        
	        $check_user = http( 'post', 'user/check-customer', $data, $this->headers);

	        if($check_user['successful'] == 1)
	        {
	        	$response_api = http( 'post', 'user/create_customer', $data, $this->headers);

		        if( $response_api['successful'] )
		        {        			        	
					return Response::json(['success' => '1']);    
		        }
		        else
		        {
		            return Response::json(['errors' => $response_api['message']]);
		        }	
	        }
	        else
	        {
	        	return Response::json(['errors' => $check_user['message']]);
	        }	        	
	    }
	    return Response::json(['errors' => $validator->errors()]);
    }

    public function send_forgot_password( Request $request ){

		$email = $request->input('user_email');

		$validator 	= Validator::make($request->all(), [
            'user_email' => 'required|email|max:255',
        ]);

        $input 		= $request->all();

        if ($validator->passes()) {

			$data = [ 
				'email' => $email,
				'url' 	=> url('reset-password').'/', 
				'logo' 	=> url( 'assets/img/logo.png' )
			];

			$check_user = http( 'post', 'user/check-customer', $data, $this->headers);
      	
	      	if($check_user['successful'] == 0)
	        {
				$response = http( 'post', 'user/request-password-reset', $data, $this->headers);
				if($response['response_status']){		
					return Response::json(['success' => '1']);    
				}else{
					return Response::json(['errors' => '1']);    
				}	
			}
		}
		return Response::json(['errors' => $validator->errors()]);
	}

    public function cofirm_register($id)
    {
    	$data['id'] = $id;

    	$confirm_register = http( 'post', 'user/confirm-registration', $data, $this->headers);

    	if($confirm_register['result'] == 1)
    	{
    		return redirect('/')
                    ->with('register_confirm', $confirm_register['message']);
    	}
    	else
    	{
    		return redirect('/')
                    ->with('register_confirm_fail', 'Something went wrong!');
    	}
    	
    }

    public function profile_step1_store(Request $request)
    {
    	$validate = Validator::make(request()->all(), [
			'first_name' 		=> 'required',
			'last_name' 		=> 'required',
			'currency' 			=> 'required',
			'contact_no'		=> 'required|numeric',
			'title' 			=> 'required',
			'pref_gender'		=> 'required',
			'pref_age_group_id' => 'required',
			'pref_nationality_id' => 'required',
		]);

		if ($validate->fails()) 
		{			
			return redirect('profile/step1')->withErrors($validate)->withInput();
		}
		else
		{

			$data = array(
				'first_name' 			=> $request['first_name'],
				'last_name'  			=> $request['last_name'],
				'currency' 	 			=> $request['currency'],
				'contact_no' 			=> $request['contact_no'],
				'title' 				=> $request['title'],
				'pref_gender' 			=> $request['pref_gender'],
				'pref_age_group_id' 	=> $request['pref_age_group_id'],
				'pref_nationality_id' 	=> $request['pref_nationality_id'],
				'old_password' 			=> $request['old_password'],
				'user_id' 				=> $request['user_id'],
				'new_password' 			=> $request['new_password'],
			);

	    	$response = http( 'post', 'user/update-profile/step1', $data, $this->headers);

	    	if($response['successful'] == 1)
	    	{
	    		return redirect('profile/step1')
	                    ->with('profile_step1_success', 'Your profile updated successfully!');
	    	}
	    	if($response['successful'] == false)
	    	{
	    		return redirect('profile/step1')
	                    ->with('profile_step1_error', 'Something went wrong!');
	    	}
	    }
    	
    }
    public function profile_step2()
    {
    	$id 				= session()->get('user_auth')['id'];
    	$user 				= http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user 				= json_decode(json_encode($user), false);	
    	$banner_data    	= self::get_home_banner_data($user);
		$from_city_id 		= $banner_data['from_city_id'];
		$from_country_id 	= $banner_data['from_country_id'];	
		$travellers 		= $banner_data['travellers'];
		$countries1 		= $banner_data['countries1'];
		$interest_ids 		= $banner_data['interest_ids'];
		$cities 			= $banner_data['cities'];
		$countries 			= $banner_data['countries'];
		$labels 			= $banner_data['labels'];
		$percent 			= self::getCompletedStep($user);
		$link 	 			= self::getRedirectLink($user);
		$version 			= config()->get('services.setting.version');

		$labels = http('get', 'labels', [], $this->headers);
		$travel_preferences = http('get', 'traveler-options', [], $this->headers);
		$travel_preferences['categories'] = [];
		$travel_preferences['categories'][0]['id'] =9;
		$travel_preferences['categories'][0]['name'] = '5 Star (Luxury)';
		$travel_preferences['categories'][1]['id'] =3;
		$travel_preferences['categories'][1]['name'] = '4 Star (Deluxe)';
		$travel_preferences['categories'][2]['id'] = 2;
		$travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
		$travel_preferences['categories'][3]['id'] = 5;
		$travel_preferences['categories'][3]['name'] = '2 Star (Backpacker / Guesthouse)';
		$travel_preferences['categories'][4]['id'] = 1;
		$travel_preferences['categories'][4]['name'] = 'Camping';
		$this->session_user_pref($user, $labels, $travel_preferences);

		$all_countries = get_all_countries();		

    	return view('users.profile_step2')->with(compact('user', 'labels','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent','id','travel_preferences','all_countries','link'));
    }

    public function profile_step2_store(Request $request)
    {
    	$validate = Validator::make(request()->all(), [
			'phy_address_1' 	=> 'required',
			'phy_state' 		=> 'required',
			'phy_city' 			=> 'required',
			'phy_zip'			=> 'required',
			'phy_country' 		=> 'required',
			'bill_address_1'	=> 'required',
			'bill_state' 		=> 'required',			
			'bill_city' 		=> 'required',
			'bill_zip' 			=> 'required',
			'bill_country' 		=> 'required',
		]);

		if ($validate->fails()) 
		{
			return redirect('profile/step2')->withErrors($validate)->withInput();
		}
		else
		{
	    	$data = array(
					'phy_address_1' 	=> $request['phy_address_1'],
					'phy_address_2'  	=> $request['phy_address_2'],
					'phy_state' 	 	=> $request['phy_state'],
					'phy_city' 			=> $request['phy_city'],
					'phy_zip' 			=> $request['phy_zip'],
					'phy_country' 		=> $request['phy_country'],
					'bill_address_1' 	=> $request['bill_address_1'],
					'bill_address_2' 	=> $request['bill_address_2'],
					'bill_state' 		=> $request['bill_state'],
					'bill_city' 		=> $request['bill_city'],
					'bill_zip'			=> $request['bill_zip'],
					'bill_country'      => $request['bill_country'],
					'user_id' 			=> $request['user_id'],
				);

	    	$response = http( 'post', 'user/update-profile/step2', $data, $this->headers);
	    	if($response['successful'] == 1)
	    	{
	    		return redirect('profile/step2')
	                    ->with('profile_step2_success', 'Your profile updated successfully!');
	    	}
	    	if($response['successful'] != 1)
	    	{
	    		return redirect('profile/step2')
	                    ->with('profile_step2_error', 'Something went wrong!');
	    	}
	    }
    }

    public function profile_step3()
    {
    	$id 				= session()->get('user_auth')['id'];    	
    	$user 				= http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user 				= json_decode(json_encode($user), false);	
    	$banner_data    	= self::get_home_banner_data($user);
		$from_city_id 		= $banner_data['from_city_id'];
		$from_country_id 	= $banner_data['from_country_id'];	
		$travellers 		= $banner_data['travellers'];
		$countries1 		= $banner_data['countries1'];
		$interest_ids 		= $banner_data['interest_ids'];
		$cities 			= $banner_data['cities'];
		$countries 			= $banner_data['countries'];
		$labels 			= $banner_data['labels'];
		$percent 			= self::getCompletedStep($user);
		$link 	 			= self::getRedirectLink($user);
		$version 			= config()->get('services.setting.version');
		$transport_type_options  = '';

		$labels = http('get', 'labels', [], $this->headers);
		$travel_preferences = http('get', 'traveler-options', [], $this->headers);
		$travel_preferences['categories'] 				= [];
		$travel_preferences['categories'][0]['id'] 		= 9;
		$travel_preferences['categories'][0]['name'] 	= '5 Star (Luxury)';
		$travel_preferences['categories'][1]['id'] 		= 3;
		$travel_preferences['categories'][1]['name'] 	= '4 Star (Deluxe)';
		$travel_preferences['categories'][2]['id'] 		= 2;
		$travel_preferences['categories'][2]['name'] 	= '3 Star (Standard)';
		$travel_preferences['categories'][3]['id'] 		= 5;
		$travel_preferences['categories'][3]['name'] 	= '2 Star (Backpacker / Guesthouse)';
		$travel_preferences['categories'][4]['id'] 		= 1;
		$travel_preferences['categories'][4]['name'] 	= 'Camping';
		$this->session_user_pref($user, $labels, $travel_preferences);

		$travel_pref = [];
		$interest_ids = [];
		if( session()->has('travel_preferences') )
		{
			// dd(session()->get('travel_preferences'));
			$travel_pref = session()->get('travel_preferences');
			$travel_pref = reset( $travel_pref );
			$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
		}

		if( count( $travellers['transport_types'] ) > 0 )
		{
	        foreach( $travellers['transport_types'] as $transport_type)
	        {
				if( empty( $transport_type['name'] ) )
				{
				  	continue;
				}
	          	if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) 
	          	{
	            	$transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" selected="selected">'.$transport_type['name'].'</option>';
	          	}
	          	else
	          	{
	            	$transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'">'.$transport_type['name'].'</option>';
	          	}
	        }
      	}

		return view('users.profile_step3')->with(compact('user', 'labels','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent','id','travel_preferences','transport_type_options','link'));
    }

    public function profile_step3_store()
    {
    	$data = array(
				'user_id' => session()->get('user_auth')['id'],
			);
    	$response = http( 'post', 'user/update-profile/step3', $data, $this->headers);
    	if($response['successful'] == 1)
    	{
    		return redirect('profile/step3')
                    ->with('profile_step3_success', 'Your profile updated successfully!');
        }
    	if($response['successful'] != 1)
    	{
    		return redirect('profile/step2')
                    ->with('profile_step3_error', 'Something went wrong!');
    	}
    }

    public function profile_step4()
    {
    	$id 				= session()->get('user_auth')['id'];
    	$user 				= http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user 				= json_decode(json_encode($user), false);	
    	$banner_data    	= self::get_home_banner_data($user);
		$from_city_id 		= $banner_data['from_city_id'];
		$from_country_id 	= $banner_data['from_country_id'];	
		$travellers 		= $banner_data['travellers'];
		$countries1 		= $banner_data['countries1'];
		$interest_ids 		= $banner_data['interest_ids'];
		$cities 			= $banner_data['cities'];
		$countries 			= $banner_data['countries'];
		$labels 			= $banner_data['labels'];
		$percent 			= self::getCompletedStep($user);
		$link 	 			= self::getRedirectLink($user);
		$version 			= config()->get('services.setting.version');
		$all_countries      = get_all_countries();
    	return view('users.profile_step4')->with(compact('user', 'labels','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent','id','all_countries','link'));
    }

    public function profile_step4_store(Request $request)
    {
    	$validate = Validator::make(request()->all(), [
    		'email' 			=> 'required|email',
			/*'phy_address_1' 	=> 'required',
			'phy_address_2' 	=> 'required',
			'phy_state' 		=> 'required',
			'phy_city' 			=> 'required',
			'phy_zip'			=> 'required',
			'phy_country' 		=> 'required',
			'contact_no'		=> 'required',
			'mobile_no' 		=> 'required',			
			'pref_contact_method'=> 'required',*/
		]);

		if ($validate->fails()) 
		{
	
			return redirect('profile/step4')->withErrors($validate)->withInput();
		}
		else
		{
	    	$data = array(
					'phy_address_1' 	=> $request['phy_address_1'],
					'phy_address_2'  	=> $request['phy_address_2'],
					'phy_state' 	 	=> $request['phy_state'],
					'phy_city' 			=> $request['phy_city'],
					'phy_zip' 			=> $request['phy_zip'],
					'phy_country' 		=> $request['phy_country'],
					'email' 			=> $request['email'],
					'contact_no' 		=> $request['contact_no'],
					'mobile_no' 		=> $request['mobile_no'],
					'user_id' 			=> $request['user_id'],
					'pref_contact_method'=> $request['pref_contact_method'],				
				);

	    	$response = http( 'post', 'user/update-profile/step4', $data, $this->headers);

	    	if($response['successful'] == 1)
	    	{
	    		return redirect('profile/step4')
	                    ->with('profile_step4_success', 'Your profile updated successfully!');
	    	}
	    	if($response['successful'] != 1)
	    	{
	    		return redirect('profile/step4')
	                    ->with('profile_step4_error', 'Something went wrong!');
	    	}
	    }
    }

    public function get_home_banner_data($user)
    {
    	$ip_address = '137.59.252.196';

        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
           
        /*Get City name by return array*/
        $countryName = $query['country'];
        
        $city = ucfirst($query['city']);
        $res = get_cities_by_city_name($city);//print_r($res);

       	if($res){
           $key = array_keys($res); $key = $key[0];
           $from_city_id = $res[$key]['id'];
           $from_country_id = $res[$key]['country_id'];

       	}else{
           $from_city_id = '';
           $from_country_id = '';

       	}

        $cities = Cache::get('cities');
		$countries = Cache::get('countries');
		$labels    = Cache::get('labels');
		$travellers = Cache::get('travellers');
        $countries1 = Cache::get('countriesBookingsPro');


		if(! session()->has('transport_types') )
		{			
			session()->put('transport_types', $travellers['transport_types']);	
		}

		$travel_pref 	= [];
		$interest_ids 	= [];
		if( session()->has('travel_preferences') )
		{
			$travel_pref = session()->get('travel_preferences');
			$travel_pref = reset( $travel_pref );
			$interest_ids = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
		}

		usort($countries, 'sort_by_name');

		$itineraries 	= http('post', 'user/get-itineraries', ['customer_id' => $user->customer->id], $this->headers);
		$itineraries 	= json_decode(json_encode($itineraries), false);

		
		$data = array(
			'from_city_id' 		=> $from_city_id,
			'from_country_id' 	=> $from_country_id,
			'itineraries' 		=> $itineraries,
			'travellers'		=> $travellers,
			'countries1'		=> $countries1,
			'interest_ids' 		=> $interest_ids,
			'cities' 			=> $cities,
			'countries'			=> $countries,
			'labels' 			=> $labels
			);

		return $data;
    }

    public function getCompletedStep($user)
    {
    	$completed_step = 0;
    	if($user->customer->step_1 == 1 )
		{
			$completed_step = $completed_step + 1;
		}

		if($user->customer->step_2 == 1 )
		{
			$completed_step = $completed_step + 1;
		}

		if($user->customer->step_3 == 1 )
		{
			$completed_step = $completed_step + 1;
		}

		if($user->customer->step_4 == 1 )
		{
			$completed_step = $completed_step + 1;
		}

		return $completed_step * 25;
    }

    public function getRedirectLink($user)
    {
    	$step1 	= $user->customer->step_1;
    	$step2 	= $user->customer->step_2;
    	$step3 	= $user->customer->step_3;
    	$step4 	= $user->customer->step_4;
    	
    	$linkArr = array(
    			"step1" => $step1,
    			"step2" => $step2,
    			"step3" => $step3,
    			"step4" => $step4,
    		);
    	$link = array_search('',$linkArr);

    	return $link;
    }

    public function manage_trips(){
    	$id = session()->get('user_auth')['id'];
		$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user = json_decode(json_encode($user), false);		
		
		// GET UPDATED PROFILE PIC
		$session = session()->get('user_auth');
		$session['image_url'] = isset( $user->customer->image_url ) ? $user->customer->image_url : null;
		$session['user_id'] = $user->id;
		session()->put('user_auth', $session);

		// Preferences
		$labels = http('get', 'labels', [], $this->headers);
		$travel_preferences = http('get', 'traveler-options', [], $this->headers);

		$travel_preferences['categories'] = [];
		$travel_preferences['categories'][1]['id'] = 5;
		$travel_preferences['categories'][1]['name'] = 'Camping';
		$travel_preferences['categories'][2]['id'] = 3;
		$travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
		$travel_preferences['categories'][3]['id'] =2;
		$travel_preferences['categories'][3]['name'] = '4 Star (Deluxe)';
		$travel_preferences['categories'][5]['id'] = 4;
		$travel_preferences['categories'][5]['name'] = '2 Star (Backpacker / Guesthouse)';
		$travel_preferences['categories'][9]['id'] =1;
		$travel_preferences['categories'][9]['name'] = '5 Star (Luxury)';
		
		$this->session_user_pref($user, $labels, $travel_preferences);		
		
		$percent 			= self::getCompletedStep($user);
		$link 	 			= self::getRedirectLink($user);
		$banner_data    	= self::get_home_banner_data($user);
		$from_city_id 		= $banner_data['from_city_id'];
		$from_country_id 	= $banner_data['from_country_id'];	
		$travellers 		= $banner_data['travellers'];
		$countries1 		= $banner_data['countries1'];
		$interest_ids 		= $banner_data['interest_ids'];
		$cities 			= $banner_data['cities'];
		$countries 			= $banner_data['countries'];
		$labels 			= $banner_data['labels'];
		$version 			= config()->get('services.setting.version');

		$trips = http('get', 'get_trips/'.$id, [], $this->headers);
		$all_trips = $trips['response'];
		
		return view('users.manage-trips')->with(compact('user', 'labels', 'travel_preferences', 'itineraries','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent','all_trips','link'));    	
    }

    public function trip_details($order_id){
    	$id = session()->get('user_auth')['id'];
		$user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
		$user = json_decode(json_encode($user), false);		
		
		// GET UPDATED PROFILE PIC
		$session = session()->get('user_auth');
		$session['image_url'] = isset( $user->customer->image_url ) ? $user->customer->image_url : null;
		$session['user_id'] = $user->id;
		session()->put('user_auth', $session);

    	$percent = self::getCompletedStep($user);
		$link = self::getRedirectLink($user);
		$banner_data = self::get_home_banner_data($user);
		$from_city_id = $banner_data['from_city_id'];
		$from_country_id = $banner_data['from_country_id'];	
		$travellers = $banner_data['travellers'];
		$countries1 = $banner_data['countries1'];
		$interest_ids = $banner_data['interest_ids'];
		$cities = $banner_data['cities'];
		$countries = $banner_data['countries'];
		$labels = $banner_data['labels'];
		$version = config()->get('services.setting.version');

    	$trip = http('get', 'get-itinerary-detail/'.$order_id, [], $this->headers);
    	
		$trip_details = $trip;

		foreach ($trip_details['itinerary_leg'] as $key => $leg) {
			$datetime1 = new \DateTime($leg['from_date']);
			$datetime2 = new \DateTime($leg['to_date']);
			$difference = $datetime1->diff($datetime2);
			$nights = $difference->d;
			$trip_details['itinerary_leg'][$key]['nights'] = $nights;
			$trip_details['city_detail'][$key]['default_nights'] = $nights;
		}

		foreach ($trip_details['itinerary_leg'] as $key => $leg) {
			$trip_details['itinerary_leg'][$key]['hasHotel'] = false;
			$trip_details['itinerary_leg'][$key]['hasActivities'] = false;
			$trip_details['itinerary_leg'][$key]['hasTransport'] = false;
			foreach ($leg['leg_detail'] as $legDetailKey => $legDetail) {
				if ($legDetail['leg_type'] == 'hotel') {
					$trip_details['itinerary_leg'][$key]['hasHotel'] = true;
				}
				if ($legDetail['leg_type'] == 'activities') {
					$trip_details['itinerary_leg'][$key]['hasActivities'] = true;
				}
				if ($legDetail['leg_type'] == 'transport') {
					$trip_details['itinerary_leg'][$key]['hasTransport'] = true;
				}
			}
		}

		$route = ['type'=>'auto','routes'=>[0=>[]],'auto_sort'=>'on'];
		$route['routes'][0]['default'] = 'yes';
		$route['routes'][0]['cities'] = $trip_details['city_detail'];

    	return view('users.manage-trip-details')->with(compact('user', 'itineraries','from_city_id','from_country_id','travellers','countries1','interest_ids','cities','countries','labels','version','percent', 'link','trip_details','route'));
    }

    public function email_validate( Request $request ){
		$email = $request->input('reg_email');
		
        $data['email'] = $email;     
        $data['url'] = url('');
        
        $check_user = http( 'post', 'user/check-customer', $data, $this->headers);
      	if($check_user['successful'] == 0)
        {
        	return Response::json(['success' => '0']);
        }
        else
        {
        	$social = 0;
        	if(isset($check_user['code']['id']))
        	{
        		$social = 1;
        	}
        	return Response::json(['success' => '1','social' => $social]);
        }	        	
	   
    }
    public function get_user_detail(){
        $user = array();
        if (session()->get('user_auth')['id']) {
            $id = session()->get('user_auth')['id'];
            $user = http('get', 'user/get-by-id/'.$id, [], $this->headers);
        }
        $user = json_decode(json_encode($user), false);
        return $user;
    }
}
