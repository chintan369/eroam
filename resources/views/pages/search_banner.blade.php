<section class="banner-container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide" src="<?php echo url( 'images/banner/tour-surf-lifestyle2.jpg'); ?>" alt="eRoam">
                <div class="banner-caption">
                    <div class="banner-inner">
                        <form class="form-horizontal clearfix">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <h1>@lang('home.banner_title1')</h1>
                                    <p><strong>90 Day Tour</strong></p>
                                    <p class="banner-price">$7950.00 </p>
                                    <a href="{{ url('tourDetail/11374/live_the_australian_surf_lifestyle_3_month_learn_to_surf_academy1') }}" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="first-slide" src="<?php echo url( 'images/banner/tour-surf-lifestyle.jpg'); ?>" alt="eRoam">
                <div class="banner-caption">
                    <div class="banner-inner">
                        <form class="form-horizontal clearfix">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <h1>LIVE THE AUSTRALIAN SURF LIFESTYLE</h1>
                                    <p><strong>30 Day Tour</strong></p>
                                    <p class="banner-price">$4000.00 </p>
                                    <a href="{{ url('tourDetail/11372/live_the_australian_surf_lifestyle_1_month_learn_to_surf_academy1') }}" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="item">
                <img class="first-slide" src="<?php echo url( 'images/banner/tour-surf-lifestyle3.jpg'); ?>" alt="eRoam">
                <div class="banner-caption">
                    <div class="banner-inner">
                        <form class="form-horizontal clearfix">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <h1>2 DAY GREAT BARRIER REEF SNORKEL</h1>
                                    <p><strong>2 Day Tour</strong></p>
                                    <p class="banner-price">$500.00 </p>
                                    <a href="{{ url('tourDetail/11378/2_day_great_barrier_reef_snorkel_daintree_rainforest_and_aboriginal_culture_tour') }}" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
    </div>
</section>
<section class="package-wrapper">
    <?php 
    /*
     Accomodation multi select options
    */

    $travel_pref = [];
    $interest_ids = [];
    if( session()->has('travel_preferences') )
    {
        // dd(session()->get('travel_preferences'));
        $travel_pref    = session()->get('travel_preferences');
        $travel_pref    = reset( $travel_pref );
        $interest_ids   = isset($travel_pref['interestListIds']) ? $travel_pref['interestListIds'] : [];
    }

    $cities     = Cache::get('cities');
    $countries  = Cache::get('countries');
    $labels     = Cache::get('labels');
    $travellers = Cache::get('travellers');
    $countries1 = Cache::get('countriesBookingsPro');
    
    /*
    | Accomodation multi select options
    */

    $accommodation_name     = ''; 
    $accommodation_options  = '';
    $room_type_name         = '';
    $room_type_options      = '';
    $transport_type_name    = '';
    $transport_type_options = '';
    $nationality            = '';
    $nationality_name       = '';
    $age_group              = '';
    $age_group_name         = '';
    $gender                 = '';
    $gender_name            = '';
    $headers = [
                'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
                'Origin' => url('')
            ];
    
    if(session()->has('tourDatas') )
    {
        $tourDatas      = session()->get('tourDatas');
        $tourcountries  = $tourDatas['tourcountries'];
        $tourCities     = $tourDatas['tourCities'];     /* add by dhara */   
    }
    else
    {
        $tourcountries  = http('post', 'getTourCountriesAvailables', [], $headers);   
        $tourCities     = http('post', 'getTourCitiesAvailable', [], $headers);

        $tourDatas['tourcountries']  = $tourcountries;
        $tourDatas['tourCities']     = $tourCities;
        session()->put('tourDatas',$tourDatas);
    }

   
    if ( count( $travellers['categories'] ) > 0 ) 
    {
        foreach( $travellers['categories'] as $category )
        {
            if( empty( $category['name'] ) ){
                continue;
            }
          
            if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) 
            {
                $accommodation_name .= '<span title="'.$category['name'].'" class="drop-selected">'.$category['name'].'</span>';                
            
                $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" selected="selected">'.$category['name'].'</option>';
            }
            else
            {
            
                $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'" >'.$category['name'].'</option>';
            }
        }
    }

    if( count( $travellers['room_types'] ) > 0 )
    {
        foreach( $travellers['room_types'] as $room_type)
        {
            if( empty( $room_type['name'] ) ){
                continue;
            }
            if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) 
            {
                $room_type_name .= '<span title="'.$room_type['name'].'" class="drop-selected">'.$room_type['name'].'</span>';
            
                $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" selected="selected"> '.$room_type['name'].'</option>';
            }
            else
            {
            
                $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'" > '.$room_type['name'].'</option>';
            }    
        }
    }

    if( count( $travellers['transport_types'] ) > 0 )
    {
        foreach( $travellers['transport_types'] as $transport_type)
        {
            if( empty( $transport_type['name'] ) ){
                continue;
            }
            if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) 
            {
                $transport_type_name .= '<span title="'.$transport_type['name'].'" class="drop-selected">'.$transport_type['name'].'</span>';
            
                $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" selected="selected">'.$transport_type['name'].'</option>';
            }
            else
            {
            
                $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'">'.$transport_type['name'].'</option>';
            }
        }
    }

    if( count( $travellers['nationalities'] ) > 0 )
    {
        foreach( $travellers['nationalities']['featured'] as $featured )
        {
            if( empty( $featured['name'] ) )
            {
                continue;
            }
            if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) 
            {
                $nationality_name .= '<span title="'.$featured['name'].'" class="drop-selected">'.$featured['name'].'</span>';
            
                $nationality .= '<option value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" selected="selected">'.$featured['name'] .'</option>';
            }
            else
            {
                $nationality .= '<option value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'" >'.$featured['name'] .'</option>';
            }
        }

        foreach( $travellers['nationalities']['not_featured'] as $not_featured )
        {
            if( empty( $not_featured['name'] ) )
            {
                continue;
            }
            if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) )
            {
                $nationality_name .= '<span title="'.$not_featured['name'].'" class="drop-selected">'.$not_featured['name'].'</span>';
                $nationality .= '<option value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" selected="selected"> '.$not_featured['name'] .'</option>';
            }
            else
            {
                $nationality .= '<option value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .'</option>';
            }
        }
    }

    $gender_array = ['male', 'female', 'other'];
    foreach($gender_array as $gen)
    {
        if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) 
        {
            $gender_name .= '<span title="'.$gen.'" class="drop-selected">'.ucfirst($gen).'</span>';
            $gender .= '<option value="'.$gen.'" data-checked="1" data-name="'.$gen.'" selected="selected"> '.ucfirst($gen) .'</option>';
        }
        else
        {
            $gender .= '<option value="'.$gen.'" data-checked="0" data-name="'.$gen.'" > '.ucfirst($gen) .'</option>';
        }
    }
    
    if( count( $travellers['age_groups'] ) > 0 )
    {
        foreach( $travellers['age_groups'] as $age)
        {
            if( empty( $age['name'] ) )
            {
                continue;
            }
            if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) 
            {
                $age_group_name .= '<span title="'.$age['name'].'" class="drop-selected">'.$age['name'].'</span>';
                $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" selected="selected" data-checked="1">'.$age['name'].'</option>';
            }
            else
            {
                $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" data-checked="0">'.$age['name'].'</option>';
            }
        }
    }

    $date = new DateTime('+1 day');

    $accommo_select = '';
    if(!empty($travel_pref['accommodation'][0]))
    {
        $accommo_select = $travel_pref['accommodation'][0];    
    }
    $transport_select = '';
    if(!empty($travel_pref['transport'][0]))
    {
        $transport_select = $travel_pref['transport'][0];    
    }
    
    ?>


    <div class="package-wrapperInner">
        <div class="container-fluid">
            <div class="clearfix">
                <div class="packageContainer-new">
                    <div class="package-container">
                        <p>Search from over 500,000 hotels, 1,000 airlines, plus 100,000’s events, tours &amp; restaurants. Select one of the options below.</p>
                        <ul class="nav nav-tabs" id="packageTabs" role="tablist">
                            <li class="active">
                                <a href="#tours" id="tours-tab" role="tab" data-toggle="tab" aria-controls="tours"   class="package-select" value="packages" aria-expanded="true"><span class="icon icon-activity"></span> Tours / Packages</a>
                            </li>
                            <li>
                                <a href="#itinerary" role="tab" id="itinerary-tab" data-toggle="tab" class="package-select" value="itinerary" aria-controls="itinerary"><span class="icon icon-location"></span> Create Itinerary</a>
                            </li>
                            <li>
                                <a class="package-select disabled" value="hotels" role="tab" id="hotel-tab"  aria-controls="hotel" data-toggle="tooltip" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot"><span class="icon icon-hotel"></span> Accommodation</a>
                            </li>
                            <li>
                                <a class="package-select disabled" value="flights" role="tab" id="flights-tab"  aria-controls="flights" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot"><span class="icon icon-plane"></span> Flights</a>
                            </li>
                            <li>
                                <a class="package-select disabled" value="carhire" role="tab" id="carHire-tab"  aria-controls="carHire" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot"><span class="icon icon-car"></span> Car Hire</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="packages-option" id="packages-option" data-value="packages">
    <div class="package-wrapperInner m-t-10">
        <div class="container-fluid">
            <div class="clearfix">
                <div class="packageContainer-new">
                    <div class="tab-content" id="packageTabContent">
                        <div class="tab-pane fade in active" role="tabpanel" id="tours" aria-labelledby="tours-tab">
                            <form method="post" action="{{ route('tours') }}" id="search-form" class="form-horizontal clearfix">
                                {{ csrf_field() }}
                                <input type="hidden" name="searchValType" id="searchValType" value="">
                                <input type="hidden" name="searchVal" id="searchVal" value="">
                                <input type="hidden" name="countryId" id="countryId" value="">
                                <input type="hidden" name="countryRegion" id="countryRegion" value="">
                                <input type="hidden" name="countryRegionName" id="countryRegionName" value="">
                                <input type="hidden" id="search_option_id" name="option" value="packages">
                                
                                <div>
                                    <label class="radio-checkbox label_radio r_on" for="checkbox-03">
                                        <input type="checkbox" name="tour_type[]" id="checkbox-03" value="single" checked >Single-Day Tour</label>
                                    <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                                    <label class="radio-checkbox label_radio" for="checkbox-04">
                                        <input type="checkbox" name="tour_type[]" id="checkbox-04" checked value="multi">Multi-Day Tour</label>
                                    <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>                                    
                                </div>
                                <div><label for="tour_type[]" generated="true" class="error"></label></div>
                                <div class="row m-t-20">
                                    <div class="col-md-9 col-sm-8">
                                        <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                                        <div class="panel-form-group input-control">
                                            <label class="label-control">Destination</label>
                                            <input type="text" name="project" id="project" class="form-control ui-autocomplete-input" placeholder="Region, Country, City or Suburb" />           
                                        </div>
                                        <label for="project" generated="true" class="error"></label>
                                    </div>
                                    <div class="col-md-3 col-sm-4 date-control">
                                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                                        <div class="panel-form-group">
                                            <label class="label-control">Departure Date</label>
                                            <div class="input-group datepicker">
                                                <input id="start_date34" type="text" placeholder="" class="form-control departure_date"  name="departure_date">
                                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="m-t-10"><a href="#"  data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                                <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" id="search_btn">                                
                           
                            </form>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="itinerary" aria-labelledby="itinerary-tab">
                            <form method="post" action="/search" id="search-form2" class="form-horizontal clearfix">
                                {{ csrf_field() }}
                                <input type="hidden" name="country" id="starting_country" value="{{ $from_country_id }}">
                                <input type="hidden" name="city" id="starting_city" value="{{ $from_city_id }}">
                                <input type="hidden" name="to_country" id="destination_country" value="">
                                <input type="hidden" name="to_city" id="destination_city" value="">
                                <input type="hidden" name="auto_populate" id="auto-populate" value="1">
                                <input type="hidden" name="countryId" id="countryId2" value="">
                                <input type="hidden" name="countryRegion" id="countryRegion2" value="">
                                <input type="hidden" name="countryRegionName" id="countryRegionName2" value="">
                                <input type="hidden" id="search_option_id2" name="option" value="packages">
                                <input type="hidden" name="searchValType2" id="searchValType2" value="">
                                <input type="hidden" name="searchVal2" id="searchVal2" value="">
                               
                                <div>
                                    <label class="radio-checkbox label_radio r_on" for="checkbox-05">
                                        <input type="radio" id="checkbox-05" name="option"  checked class="radio_tailor" value="auto">Tailor-Made Auto</label>
                                    <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                                    <label class="radio-checkbox label_radio" for="checkbox-06">
                                        <input type="radio" id="checkbox-06" name="option" value="manual" class="radio_tailor" >Manual Itinerary</label>
                                    <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>
                                    <label class="radio-checkbox label_radio" for="checkbox-07" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                        <input type="radio" id="checkbox-07" name="option2" value="search-box5" checked>Package Rates</label>
                                    <a href="#" class="help-icon" data-target="#packageRateModal" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot"><i class="fa fa-question-circle"></i></a>
                                </div>
                            <div class="m-t-20">
                           
                                <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                                <div class="searchMode-container search-box3 mode-block">
                                    <div class="row">
                                        <div class="col-sm-6" id="start_location_form2">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Departing</label>
                                                <input type="text" name="start_location" id="start_location" class="form-control ui-autocomplete-input" placeholder="Region, Country, City or Suburb" />
                                            </div>
                                            <label for="start_location" generated="true" class="error" id="start_loc"></label>
                                        </div>

                                        <div class="col-sm-6" id="end_location_form2">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Destination</label>
                                                <input type="text" name="end_location" id="end_location" class="search_from_col form-control ui-autocomplete-input valid" placeholder="Region, Country, City or Suburb" />
                                            </div>
                                            <label for="end_location" generated="true" class="error"></label>
                                        </div>
                                    </div>
                                </div>
                                <?php /*<div class="searchMode-container search-box4">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Destination</label>
                                                <input type="text" name="end_location" id="end_location" class="search_from_col form-control ui-autocomplete-input" placeholder="Region, Country, City or Suburb" />
                                            </div>
                                        </div>
                                    </div>
                                </div> */?>
                                <div class="row m-t-20">
                                    <div class="col-sm-3 date-control">
                                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                                        <div class="panel-form-group">
                                            <label class="label-control">Check-In Date</label>                                            
                                            <div class="input-group datepicker">
                                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control start_date valid" value="<?php echo $date->format('d-m-Y'); ?>" name="start_date" id="start_date">
                                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="left-spacing">
                                            <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Rooms</label>
                                                        
                                                       <!--  <select class="form-control room_select" name="rooms"> -->
                                                        <select class="form-control disabled" name="rooms" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                                           <!--  Before it was 8 -->
                                                            @for($i = 1; $i <= 1; $i++) 
                                                                <option value="{{$i}}" >{{$i}}</option>
                                                            @endfor
                                                        </select>
                                                        <input type="hidden" class="room_select" name="rooms" value="1">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 single_room">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Adults (18+)</label>
                                                        <select class="form-control" name="num_of_adults[]" id="number_of_adults">
                                                          @for($i = 1; $i <= 14 ; $i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 single_room">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Children (0-17)</label>
                                                        <?php /* <select class="form-control child_select" name="num_of_children[]" id="number_of_child"> */?>
                                                        <select class="form-control disabled" name="num_of_children[]" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                                                            
                                                            <option value="0">No Children</option>
                                                            <?php /*@for($i = 1; $i <= 6; $i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                            @endfor */?>
                                                        </select>
                                                        <input type="hidden" class="child_select" id="number_of_child" name="num_of_children[]" value="1">
                                                    </div>
                                                </div>
                                                <div id="total_adults">
                                                    <input type="hidden" name="travellers" id="total_room_adults" value="1" >
                                                    <input type="hidden" name="total_children" id="total_room_child" value="0" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>      
                                <div class="children-block " style="display:none">
                                <div><span class="smile-icon"><i class="icon-child"></i></span>
                                  <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                                </div>                                
                                <div class="children_selectbox row"></div>
                              </div>
                                <div class="itinery_person_info"></div>
                                <p class="m-t-20 position-relative">
                                    <label class="radio-checkbox label_check checbox-hover" for="checkbox-08">
                                        <input type="checkbox" id="checkbox-08" value="search-box4">I do not need accommodation in the departing location</label>
                                    <span class="checkbox-tooltip">
                                        Not Available in Pilot
                                    </span>

                                </p>
                                <p class="m-t-10"><a href="#" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                                <input type="submit" name="" class="btn btn-primary active m-t-20 search_itinery" value="SEARCH">
                            </form>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="hotel" aria-labelledby="hotel-tab">
                            <h3><center>Coming Soon.. </center></h3>
                            <?php /*<div>
                                <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                                <div class="panel-form-group input-control">
                                    <label class="label-control">Destination</label>
                                    <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" />
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-sm-4">
                                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                                        <div class="row">
                                            <div class="col-sm-6 date-control">
                                                <div class="panel-form-group">
                                                    <label class="label-control">Check-In Date</label>
                                                    <div class="input-group datepicker">
                                                        <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 date-control">
                                                <div class="panel-form-group">
                                                    <label class="label-control">Check-Out Date</label>
                                                    <div class="input-group datepicker">
                                                        <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="panel-form-group form-group">
                                                    <label class="label-control">Number of Rooms</label>
                                                    <select class="form-control">
                                                        <option>1 Room</option>
                                                        <option>2 Room</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="panel-form-group form-group">
                                                    <label class="label-control">Number of Adults (18+)</label>
                                                    <select class="form-control">
                                                        <option>1 Adult</option>
                                                        <option>2 Adult</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="panel-form-group form-group">
                                                    <label class="label-control">Number of Children (0-17)</label>
                                                    <select class="form-control">
                                                        <option>No Children</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-t-20">
                                    <p><span class="icon-hotel m-r-10"></span><strong>Room 1</strong></p>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group form-group">
                                                <label class="label-control">Number of Adults (18+)</label>
                                                <select class="form-control">
                                                    <option>1 Adult</option>
                                                    <option>2 Adult</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group form-group">
                                                <label class="label-control">Number of Children (0-17)</label>
                                                <select class="form-control">
                                                    <option>No Children</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-t-20">
                                    <p><span class="icon-hotel m-r-10"></span><strong>Room 2</strong></p>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group form-group">
                                                <label class="label-control">Number of Adults (18+)</label>
                                                <select class="form-control">
                                                    <option>1 Adult</option>
                                                    <option>2 Adult</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group form-group">
                                                <label class="label-control">Number of Children (0-17)</label>
                                                <select class="form-control">
                                                    <option>No Children</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-t-20">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <p><span class="icon-hotel m-r-10"></span><strong>Room 3</strong></p>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Adults (18+)</label>
                                                        <select class="form-control">
                                                            <option>1 Adult</option>
                                                            <option>2 Adult</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Children (0-17)</label>
                                                        <select class="form-control">
                                                            <option>No Children</option>
                                                            <option>1 Children</option>
                                                            <option selected>2 Children</option>
                                                            <option>3 Children</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="left-spacing">
                                                <div><span class="smile-icon"><i class="icon-child"></i></span>
                                                    <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="panel-form-group form-group">
                                                            <label class="label-control">Child 1 Age</label>
                                                            <select class="form-control">
                                                                <option>1 Years</option>
                                                                <option>2 Years</option>
                                                                <option>3 Years</option>
                                                                <option>4 Years</option>
                                                                <option>5 Years</option>
                                                                <option selected>6 Years</option>
                                                                <option>7 Years</option>
                                                                <option>8 Years</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="panel-form-group form-group">
                                                            <label class="label-control">Child 2 Age</label>
                                                            <select class="form-control">
                                                                <option>1 Years</option>
                                                                <option>2 Years</option>
                                                                <option>3 Years</option>
                                                                <option>4 Years</option>
                                                                <option>5 Years</option>
                                                                <option selected>6 Years</option>
                                                                <option>7 Years</option>
                                                                <option>8 Years</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="m-t-20"><a href="#" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                                <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" />
                            </div> */?>

                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="flights" aria-labelledby="flights-tab">
                            <h3><center>Coming Soon.. </center></h3>
                            <?php /*<div>
                                <label class="radio-checkbox label_radio r_on m-r-20" for="checkbox-09">
                                    <input type="radio" id="checkbox-09" name="option3" value="search-box9" checked>Return</label>
                                <label class="radio-checkbox label_radio m-r-20" for="checkbox-10">
                                    <input type="radio" id="checkbox-10" name="option3" value="search-box10">One Way</label>
                                <label class="radio-checkbox label_radio m-r-20" for="checkbox-11">
                                    <input type="radio" id="checkbox-11" name="option3" value="search-box11">Multi-City</label>
                            </div>
                            <div class="m-t-20">
                                <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                                <div class="searchMode-container search-box9 mode-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Departing</label>
                                                <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Destination</label>
                                                <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="searchMode-container search-box10">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Destination</label>
                                                <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="searchMode-container search-box11">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Departing</label>
                                                <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Destination</label>
                                                <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-sm-3 date-control">
                                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Dates</strong></p>
                                        <div class="panel-form-group">
                                            <label class="label-control">Check-In Date</label>
                                            <div class="input-group datepicker">
                                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="left-spacing">
                                            <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Rooms</label>
                                                        <select class="form-control">
                                                            <option>1 Room</option>
                                                            <option>2 Room</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Adults (18+)</label>
                                                        <select class="form-control">
                                                            <option>1 Adult</option>
                                                            <option>2 Adult</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="panel-form-group form-group">
                                                        <label class="label-control">Number of Children (0-17)</label>
                                                        <select class="form-control">
                                                            <option>No Children</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-t-20">
                                    <div><span class="smile-icon"><i class="icon-child"></i></span>
                                        <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group">
                                                <label class="label-control">Child 1 Age</label>
                                                <select class="form-control">
                                                    <option>1 Years</option>
                                                    <option>2 Years</option>
                                                    <option>3 Years</option>
                                                    <option>4 Years</option>
                                                    <option>5 Years</option>
                                                    <option selected>6 Years</option>
                                                    <option>7 Years</option>
                                                    <option>8 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group">
                                                <label class="label-control">Child 2 Age</label>
                                                <select class="form-control">
                                                    <option>1 Years</option>
                                                    <option>2 Years</option>
                                                    <option>3 Years</option>
                                                    <option>4 Years</option>
                                                    <option>5 Years</option>
                                                    <option>6 Years</option>
                                                    <option>7 Years</option>
                                                    <option selected>8 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group">
                                                <label class="label-control">Child 3 Age</label>
                                                <select class="form-control">
                                                    <option>1 Years</option>
                                                    <option>2 Years</option>
                                                    <option>3 Years</option>
                                                    <option>4 Years</option>
                                                    <option>5 Years</option>
                                                    <option>6 Years</option>
                                                    <option>7 Years</option>
                                                    <option selected>8 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group">
                                                <label class="label-control">Child 4 Age</label>
                                                <select class="form-control">
                                                    <option>1 Years</option>
                                                    <option>2 Years</option>
                                                    <option>3 Years</option>
                                                    <option>4 Years</option>
                                                    <option>5 Years</option>
                                                    <option>6 Years</option>
                                                    <option>7 Years</option>
                                                    <option selected>8 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group">
                                                <label class="label-control">Child 5 Age</label>
                                                <select class="form-control">
                                                    <option>1 Years</option>
                                                    <option>2 Years</option>
                                                    <option>3 Years</option>
                                                    <option>4 Years</option>
                                                    <option>5 Years</option>
                                                    <option>6 Years</option>
                                                    <option>7 Years</option>
                                                    <option selected>8 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <div class="panel-form-group">
                                                <label class="label-control">Child 6 Age</label>
                                                <select class="form-control">
                                                    <option>1 Years</option>
                                                    <option>2 Years</option>
                                                    <option>3 Years</option>
                                                    <option>4 Years</option>
                                                    <option>5 Years</option>
                                                    <option>6 Years</option>
                                                    <option>7 Years</option>
                                                    <option selected>8 Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-t-30 small-btn">
                                    <!-- searchMode-container search-box11 -->
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <p><span class="icon-location m-r-10"></span><strong>Additional Location</strong></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group input-control">
                                                        <label class="label-control">Departing</label>
                                                        <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group input-control">
                                                        <label class="label-control">Destination</label>
                                                        <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="row">
                                                <div class="col-sm-9 date-control">
                                                    <div class="panel-form-group">
                                                        <label class="label-control">Check-In Date</label>
                                                        <div class="input-group datepicker">
                                                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group input-control">
                                                        <label class="label-control">Departing</label>
                                                        <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group input-control">
                                                        <label class="label-control">Destination</label>
                                                        <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="row">
                                                <div class="col-sm-9 date-control">
                                                    <div class="panel-form-group">
                                                        <label class="label-control">Check-In Date</label>
                                                        <div class="input-group datepicker">
                                                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group input-control">
                                                        <label class="label-control">Departing</label>
                                                        <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="panel-form-group input-control">
                                                        <label class="label-control">Destination</label>
                                                        <input type="text" name="" class="form-control" placeholder="City or Airport" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="row">
                                                <div class="col-sm-9 date-control">
                                                    <div class="panel-form-group">
                                                        <label class="label-control">Check-In Date</label>
                                                        <div class="input-group datepicker">
                                                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p class="m-t-20"><a href="#" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                                <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" />
                            </div> */?>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="carHire" aria-labelledby="carHire-tab">
                            <h3><center>Coming Soon.. </center></h3>
                            <?php /*<div>
                                <label class="radio-checkbox label_check" for="checkbox-12">
                                    <input type="checkbox" id="checkbox-12" name="option4" value="search-box12">Drop off at a different location</label>
                            </div>
                            <div class="m-t-20">
                                <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                                <div class="without-drop">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Pick-Up Location</label>
                                                <input type="text" name="" class="form-control" placeholder="City, Suburb or Airport" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="with-drop">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Departing</label>
                                                <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="panel-form-group input-control">
                                                <label class="label-control">Destination</label>
                                                <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row m-t-20">
                                    <div class="col-sm-5">
                                        <p><span class="icon-manage-trip m-r-10"></span><strong>Pick-Up Date</strong></p>
                                        <div class="row">
                                            <div class="col-sm-6 date-control">
                                                <div class="panel-form-group">
                                                    <label class="label-control">Pick-Up Date</label>
                                                    <div class="input-group datepicker">
                                                        <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 date-control">
                                                <div class="panel-form-group">
                                                    <label class="label-control">Pick-Up Time</label>
                                                    <div class="input-group datepicker">
                                                        <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="left-spacing">
                                            <p><span class="icon-manage-trip m-r-10"></span><strong>Drop-Off Date</strong></p>
                                            <div class="row">
                                                <div class="col-sm-4 date-control">
                                                    <div class="panel-form-group">
                                                        <label class="label-control">Drop-Off Date</label>
                                                        <div class="input-group datepicker">
                                                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 date-control">
                                                    <div class="panel-form-group">
                                                        <label class="label-control">Drop-Off Time</label>
                                                        <div class="input-group datepicker">
                                                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>*/?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="preferencesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Personal Preferences</h4>
                <p>By entering your personal preferences for hotels, transport, activities, this assists eRoam to create a more personalised travel options for you.</p>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel-form-group">
                                <label class="label-control">Select Age Group</label>
                                <select class="form-control age_group" name="age_group" id="dropdown-age">
                                    <option value="">Select Age Group</option>
                                    {!! $age_group !!}
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="panel-form-group">
                                <label class="label-control">Select Transport Type</label>
                                <select class="form-control transport_type_options" name="transport_types[]" id="dropdown-transports">
                                    <option value="0">Select Transport Type</option>
                                    <option value="1" {{($transport_select == 1) ? 'selected' : '' }}>Flight</option>
                                    <option value="2" {{ ($transport_select == 2) ? 'selected' : '' }}>Overland</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel-form-group">
                                <label class="label-control">Select Gender</label>
                                <select class="form-control gender" name="gender" id="dropdown-gender">
                                    <option value="">Select Gender</option>
                                    {!! $gender !!}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel-form-group">
                                <label class="label-control">Select Accommodation Type</label>
                                <select class="form-control hotel_category" name="hotel_category_id[]" id="dropdown-accommodation">
                                    <option value="0">Select Accommodation Type</option>
                                    <option value="1" {{ ($accommo_select == 1) ? 'selected' : '' }}> 1 Star</option>
                                    <option value="5" {{ ($accommo_select == 5) ? 'selected' : '' }}> 2 Star</option>
                                    <option value="2" {{ ($accommo_select == 2) ? 'selected' : '' }}> 3 Star</option>
                                    <option value="3" {{ ($accommo_select == 3) ? 'selected' : '' }}> 4 Star</option>
                                    <option value="9" {{ ($accommo_select == 9) ? 'selected' : '' }}> 5 Star</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel-form-group">
                                <label class="label-control">Select Nationality</label>
                                <select class="form-control nationality" name="nationality" id="dropdown-nationality">
                                    <option value="">Select Nationality</option>
                                    {!! $nationality !!}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6" style="display:none;">
                            <div class="panel-form-group">
                                <label class="label-control">Select Room Type</label>
                                <select class="form-control room_type_options" name="room_type_id[]" id="dropdown-room">
                                    <option>Select Room Type</option>
                                    {!! $room_type_options !!}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="m-t-20" style="display:none;">
                        <h4 class="modal-title" id="myModalLabel">Holiday Preferences</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Price Range Per Day</label>
                                <div class="layout-slider">
                                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                                        <input id="Slider3" type="slider" name="price2" value="100;10000" />
                                     </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Number of Total Days</label>
                                <div class="layout-slider">
                                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                                        <input id="Slider4" type="slider" name="price3" value="1;100" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-t-20">
                        <h4 class="modal-title" id="myModalLabel">Activity Preferences</h4>
                        <p>Please drag your preferred activities into the boxes below.
                            
                        </p>

                        <div class="m-t-10">
                            <ul class="drop-list">
                            <?php 
                                $imgArr = array(
                                15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                                5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                                13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                                29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                                11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                                20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                                 //'Expeditions','Astronomy','Food','Short-Breaks'
                                );

                                //echo '<pre>'; print_r($labels); echo '</pre>';
                                $ij = 0;
                                if( count($interest_ids) > 0 ) {
                                foreach ($interest_ids as $j => $interest_id) {
                                    $key = array_search($interest_id, array_column($labels, 'id'));
                                    $label = $labels[$key];
                                    $ij++;
                                ?>
                                    <li ondrop="drop(event)" ondragover="allowDrop(event)" class="interest-button-active" id="interest-{{ $label['id'] }}" data-sequence="{{ $ij }}" data-name="{{ $label['name'] }}">
                                        <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $j }} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                                        <input type="hidden" id="minputValue_{{ $label['id'] }}" class="input-interest" name="interests[{{$j + 1}}]" value="{{ $label['id'] }}">
                                    </li>
                                    <?php  } } ?>

                                    <?php 
                                    for($k = $ij; $k < 8; $k++)
                                    {
                                        echo '<li ondrop="drop(event)" ondragover="allowDrop(event)" class="blankInterest" data-sequence="'.($k + 1).'"></li>';
                                    }
                                    ?>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <hr/>
                        <div class="m-t-10">

                            <ul class="drag-list interest-lists first">
                                @if( count($labels) > 0 ) @foreach ($labels as $i => $label) @if( empty($label['name']) ) @continue @endif
                                <li ondrop="drop(event)" ondragover="allowDrop(event)" data-value="{{$label['id']}}" class="{{ in_array($label['id'], $interest_ids ) ? ' interestMove' : ''  }}" id="interest-{{ $label['id'] }}">
                                    <?php
                                    $namedis = 'block';
                                    if(!in_array($label['id'], $interest_ids )){
                                    $namedis = 'none';
                                    ?>
                                        <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $i}} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                                        <input type="hidden" id="inputValue_{{ $label['id'] }}" class="input-interest" name="interests[]" value="">
                                        <?php } ?>
                                            <span style="display:{{ $namedis }};" id="name_{{ $label['id'] }}">{{ $label['name'] }}</span>
                                </li>
                                @endforeach @endif
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="m-t-30 text-right">
                            <a href="#" data-dismiss="modal" class="m-r-10">Close</a>
                            <a href="javascript://" id="save_travel_preferences">Save</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
    $i=0;
    $uid = session()->get('user_auth')['id'];
    //dd($uid,"asdasdasd");
    $allCountry = array();
    foreach($countries as $country){
      foreach ($country['countries'] as $country_data){
          $allCountry[$i]['name'] = $country_data['name'];
          $allCountry[$i]['id'] = $country_data['id'];
          $allCountry[$i]['region'] = $country['id'];
          $allCountry[$i]['regionName'] = $country['name'];
          $i++;
      } 
    }
    usort($allCountry, 'sort_by_name');
    
?>
<script type="text/html" id="addChild">
    <div class="col-md-2 col-sm-2" ><div class="panel-form-group form-group"><label class="label-control">Child 1 Age</label>
            <select class="form-control netChild" id="child{0}" name="child{0}"><option value="">Age</option><option value="0">Under 1</option>
            @for($l=1 ; $l<=17; $l++)      
                <option value="{{$l}}">{{$l}}</option>
            @endfor
            </select>
        </div>
        <label for="child{0}" generated="true" class="error"></label>
    </div>
</script>

<script type="text/javascript">

        $(".checbox-hover").mouseover(function(){
          $(".checkbox-tooltip").css("display", "block");
        });
        $(".checbox-hover").mouseout(function(){
          $(".checkbox-tooltip").css("display", "none");
        });

        var projects = [
            <?php for($i=0; $i<count($cities);$i++) { ?>{
                value: "<?php echo $cities[$i]['id'];?>",
                label: "<?php echo $cities[$i]['name'];?>",
                desc: "<?php echo $cities[$i]['country_name'];?>",
                cid: "<?php echo $cities[$i]['country_id'];?>"
            },
            <?php } ?>
        ];

        var countries = [
          <?php for($i=0;$i<count($tourcountries);$i++) { ?>{
              value : "<?php echo $tourcountries[$i]['id'];?>",
              label : "<?php echo $tourcountries[$i]['name'];?>",
              cid : 0,
              cname : "<?php echo $tourcountries[$i]['name'];?>",
              rid   : "<?php echo $tourcountries[$i]['region_id'];?>",
              rname : "<?php echo $tourcountries[$i]['rname'];?>",
              type : "country"
          },

        <?php } ?>

        <?php for($i=0;$i<count($tourCities);$i++) { ?>{
            value : "<?php echo $tourCities[$i]['city_id'];?>",
            label : "<?php echo $tourCities[$i]['name'];?>",
            cid   : "<?php echo $tourCities[$i]['country_id'];?>",
            cname : "<?php echo $tourCities[$i]['cname'];?>",
            rid   : "<?php echo $tourCities[$i]['region_id'];?>",
            rname : "<?php echo $tourCities[$i]['rname'];?>",
            type  : "city"
        },

        <?php } ?>
    ];

  $('body').on('click', '#save_travel_preferences', function() {
            var btn = $(this).button('loading');

            if($('.nationality_id').val()){
              var nationality_dom = $(".nationality option:selected").text();
            }else{
              var nationality_dom = $(".nationality option:selected").val();
            }
            
            if($('.age_group').val()){
              var age_group_dom = $(".age_group option:selected").text();
            }else{
              var age_group_dom = $(".age_group option:selected").val();
            }
            var age_group_id = $('.age_group').val();
            var nationality_id = $('.nationality').val();
            var gender = $('.gender').val();
           
            var nationality = ( isNotUndefined( nationality_dom ) ) ? nationality_dom : [] ;
            var age_group = ( isNotUndefined( age_group_dom ) ) ? age_group_dom : [] ;
            var interestLists = [];
            var accommodations = [];
            var accommodationIds = [];
            var interestListIds = [];
            var roomTypeIds = [];
            var roomTypes = [];
            var transportTypeIds = [];
            var transportTypes = [];
              
            $('.hotel_category option:selected').each(function(){
              if($(this).val()){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                accommodationIds.push(id);
                accommodations.push(name);
              }
            });

            $('.room_type_options option:selected').each(function(){
              var id = parseInt($(this).val());
              var name = $(this).attr('data-name');
              roomTypeIds.push(id);
              roomTypes.push(name);
            });
            
            $('.transport_type_options option:selected').each(function(){
              if($(this).val()){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                transportTypeIds.push(id);
                transportTypes.push(name);
              }
            });
            
            $('.interest-button-active').each(function(){
                interestLists.push($(this).attr('data-name'));
            });

            var  data = { 
              travel_preference: [{
                accommodation:accommodationIds, 
                accommodation_name: accommodations,
                room_name: roomTypes,
                room: roomTypeIds,
                transport_name: transportTypes,
                transport: transportTypeIds,
                age_group: age_group,
                nationality: nationality,
                gender: gender,
                interestLists: interestLists.join(', '),
                interestListIds: activitySequence
                //interestListIds: interestListIds
              }]
            };
            

            var interestText = interestLists.length > 0 ? interestLists.join(', ') : 'All';
            var accommodation = accommodations.length > 0 ? accommodations.join(', ') : 'All';
            var transport = transportTypes.length > 0 ? transportTypes.join(', ') : 'All';
            var nationalityText =  nationality !=''  ? nationality : 'All'; 
          
            var ageGroupText =  age_group !='' ? age_group : 'All'; 

            $('#_accommodation').html(' <strong> Accommodation: </strong> '+accommodation);
            $('#_transport').html(' <strong> Transport: </strong> '+transport);
            $('#_nationality').html(' <strong> Nationality: </strong> '+nationalityText);
            $('#_age').html(' <strong> Age: </strong> '+ageGroupText);
            $('#_interests').html(' <strong> Interests: </strong> '+ interestText);
            

            @if (session()->has('user_auth'))
              var post = {
                hotel_categories:accommodationIds, 
                hotel_room_types: roomTypeIds,
                transport_types: transportTypeIds,
                age_group: age_group_id,
                nationality: nationality_id,
                gender: gender,
                interests: activitySequence,
                _token: $('meta[name="csrf-token"]').attr('content'),
                user_id: "{{ session()->get('user_auth')['user_id'] }}",
              };
              eroam.ajax('post', 'save/travel-preferences', post, function(response){
                //console.log(post);
              });
            @endif;

            eroam.ajax('post', 'session/travel-preferences', data, function(response){
              setTimeout(function() {
                btn.button('reset');

                eroam.ajax('get', 'session/updatePreferences', '', function(responsedata){
                  $("#changePreferences").html(responsedata);
                });

                $('#preferencesModal').modal('hide');
              }, 3000);
            });
          });
    
    var activitySequence = [{{ join(', ', $interest_ids) }}];
    function allowDrop(ev) {
          ev.preventDefault();
      }

      function drag(ev) {
          ev.dataTransfer.setData("text", ev.target.id);
          var data = ev.target.id;

          if($("#"+data).parent().parent().prop('className') == 'drop-list'){
            var interest = data.replace('drag_', 'interest-');
            $(".interestMove").css('opacity','0.5');
            $("#"+interest+".interestMove").css('opacity','');
          }
      }

      function drop(ev) { 
          ev.preventDefault();
          var data = ev.dataTransfer.getData("text");
          var name = data.replace('drag', 'name');
          var inputValue = data.replace('drag', 'inputValue');
          console.log(inputValue);
          var interest = data.replace('drag_', 'interest-');
          var x = document.getElementById(name);
          var count = 0;
          var text = '';
          var data_value = document.getElementById(data).getAttribute("data-value");
          var data_name = document.getElementById(data).getAttribute("data-name");
          data_value = parseInt(data_value);
          var parentClass = $("#"+data).parent().parent().prop('className');
          var ClassName = $("#"+data).parent().prop('className');

          if(ClassName != "interest-button-active"){
            if($("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
                if (x.style.display === 'none') {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }
                
                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                if( hidden_interest_field.val() ){
                  var interestIndex = activitySequence.indexOf(data_value);
                  activitySequence.splice(interestIndex, 1);
                  hidden_interest_field.val('');
                  hidden_interest_field.attr('name', 'interests[]');
                } else {
                  activitySequence.push(data_value);
                  var interestIndex = activitySequence.indexOf(data_value);
                  hidden_interest_field.val(data_value);
                  hidden_interest_field.attr('name', 'interests['+(interestIndex + 1)+']');
                } 

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                var parentDiv = $("#"+data).parent();
                parentDiv.attr('id', interest); 
                parentDiv.attr('data-name', data_name); 

                $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
            }

          } else {
            if(ev.target.getAttribute("class") == 'blankInterest' && $("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
      
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);
                activitySequence.push(data_value);

                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                hidden_interest_field.attr('name', 'interests['+ev.target.getAttribute("data-sequence")+']');  

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                $("#"+interest).removeClass('interest-button-active').addClass('blankInterest').removeAttr('data-name');
                $("#"+interest).removeAttr('id');
                $("#"+data).parent().attr('id',interest).attr('data-name',data_name);
                $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
            } else {
              if(parseInt(ev.target.getAttribute("data-value")) == data_value){
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);

                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                hidden_interest_field.val('');
                hidden_interest_field.attr('name', 'interests[]');  
                $(".interestMove").css('opacity','');

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));

                $("#"+interest).removeClass('interest-button-active').addClass('blankInterest');
                $("#"+interest).removeAttr('id');
                x.style.display = 'none';
              } 
            }
          } 
      }

</script>
<?php $version = config()->get('services.setting.version'); ?>
<script type="text/javascript" src="{{  url( 'js/eroam_js/search_banner.js?v='.$version.'') }}"></script>