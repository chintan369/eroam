@extends( 'layouts.home' )
@section('content')
<div class="top-section-image">
			<img src="{{asset('images/bg-image.jpg')}}" alt="" class="img-responsive">
		</div>
<div class="itinerary_block form-row text-center">
    <div class="container-fluid">
        <div class="row mt-4 tour-details">
            <div class="col-xl-10 offset-xl-1">							
				<h1 class="text-center">Thank you for booking your trip with us !</h1>					
			  	<div class="col-sm-4 offset-sm-4 mt-5">
				  	<a href="/" name="" class="btn btn-white btn-block">Return to Homepage</a>
			  	</div>
			</div>
		</div>
	</div>
</div>

@stop
