<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eroamConfirm" id="eroam-confirm">
    <div class="modal-dialog eroam-confirm-modal-size" role="document" >
        <div class="modal-content">
            <div class="modal-header eroam-confirm-header">
                <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title eroam-confirm-title" id="gridSystemModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 eroam-confirm-content-close" style="display:none;">
                        <p>
                            <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 eroam-confirm-content">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary confirm-ok-button">OK</button>
                <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->