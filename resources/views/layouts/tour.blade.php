@include( 'layouts.partials.header' )

<?php 
    $i=$j=$k=0;
    $allCountryRegion = array();
    $regionList = '';
    $countryList = '';
    $RegionId = array();
    $allCountry = array();
    $countryDropdown = '';
    if(isset($country_id_remove))
    {
    	$country_id_remove  = $country_id_remove;
    }
    else
    {
    	$country_id_remove  = [];
    }
    
    $cityIds = isset($cityIds) ? $cityIds : [];	

    $availableTourRegions = array();
    if(!empty($tourRegions)){
    	foreach ($tourRegions as $key => $value) {
    		array_push($availableTourRegions, $value['region_id']);
    	}
    }
    $availableTourRegions = array_unique($availableTourRegions);
    $availableTourCounts = array();
    if(!empty($totalTourByRegion)){
    	foreach ($totalTourByRegion as $key => $value) {
    		$availableTourCounts[$value['id']] = $value['tourCount'];
    	}
    }
    
   	if($countryIds){
    	foreach ($countryIds as $key =>$countryId) { 

    		$k++;    		
    		$countryRegion 		= $tourCountries['countryRegions'][$key];
    		$countryName 		= $tourCountries['countryNames'][$key];
    		$countryRegionName  = $tourCountries['countryRegionNames'][$key];
    		$label_partial 		= '';
    		if($k <= 10){
				$check_checked = 'checked';
				if (in_array($countryId, $country_id_remove))
				{
					$label_partial = 'label_partial';
				}
				if(isset($country_tour_data[$countryId]['cities']))
	    		{
	    			$open_c = "open";	    			
	    		}
	    		else
	    		{
	    			$open_c = '';
	    		}
	    		if(isset($country_tour_data[$countryId]['cities']))
	    		{
	    		$countryList .= '<p class="regionCountry_'.$countryRegion.'">
						    		<label class="radio-checkbox label_check '.$label_partial.'" for="checkbox-'.$countryId.'" id="label-'.$countryId.'">
						    			<input class="countryList" data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" data-name="'.$countryName.'"  type="checkbox" id="checkbox-'.$countryId.'" name="region[]" value="'.$countryId.'" >'.$countryName.' 
						    		</label>
					    		[<span class="count'.$countryName.'" id="count'.$countryName.'">'.$tourCount[$countryName].'</span>]';
					    		
					    			$countryList .='<a href="javascript://" class="moreCountry morecity'.$countryId.' '.$open_c.'" data-id="'.$countryId.'"><i class="fa fa-plus"></i></a></p>';
					    		

	    		/*$cities 		= get_cities_by_country_id($countryId);
	    		$cityTourCount 	= cityTourCount($countryId);*/
	    		
		    		$cities 		= $country_tour_data[$countryId]['cities'];
		    		$countryList .= '<div class="moreCountry-block morecountry'.$countryId.'">';
		    		foreach ($cities as $city) {
		    			$check_checked = '';
	    				if (in_array($city['city_id'], $cityIds))
	    				{
	    					$check_checked = "checked";
	    				}

	    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$countryId.'" data-country-id="'.$countryId.'" data-name="'.$countryName.'" data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" id="checkbox-'.$city['city_id'].'" value="'.$city['city_id'].'" name="city[]">'.$city['city_name'].'('.$city['city_count'].')</label></p>';	
		    		}
		    		$countryList .= '</div>';
		    	}
					
			} else {
				$countryList .= '<p class="regionCountry_'.$countryRegion.'" style="display:none;">
		    		<label class="radio-checkbox label_check" for="checkbox-'.$countryId.'">
		    			<input class="countryList" checked data-regionName ="'.$countryRegionName.'" data-region="'.$countryRegion.'" data-name="'.$countryName.'"  type="checkbox" id="checkbox-'.$countryId.'" name="region[]" value="'.$countryId.'" >'.$countryName.' 
		    		</label>
	    		[<span class="count'.$countryName.'" id="count'.$countryName.'">'.$country_tour_data[$countryId]['country_count'].'</span>]<a href="javascript://" class="moreCountry morecity'.$countryId.'" data-id="'.$countryId.'"><i class="fa fa-plus"></i></a></p>';

				$cities 		= $country_tour_data[$countryId]['cities'];
				$countryList .= '<div class="moreCountry-block morecountry'.$countryId.'">';

	    		foreach ($cities as $city) {
	    			$check_checked = '';
    				if (in_array($city['city_id'], $cityIds))
    				{
    					$check_checked = "checked";
    				}
    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$countryId.'" data-country-id="'.$countryId.'" id="checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]"  data-name="'.$countryName.'"  data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" value="'.$city['city_id'].'">'.$city['name'].'('.$city['city_count'].')</label></p>';	
	    		}
	    		
	    		$countryList .= '</div>';
			}
    	}
    }
    $region_count = array();//print_r($countries);exit;
    foreach($countries as $country){
    	$i++;
    	$selectRegionId = 0;
    	$totalCount = 0;
    	$allCountry = array();
    	$total_country_region = 0;
      	foreach ($country['countries'] as $country_data){
      		if( count($country_data['city']) > 0  ){
      		// if( count($tourCount[$country_data['name']]) > 0  ){
		      	$j++;
		      	$allCountry[$j]['name'] = $country_data['name'];
		        $allCountry[$j]['id'] = $country_data['id'];
		        $allCountry[$j]['region'] = $country_data['region_id'];
		        $allCountry[$j]['regionName'] = $country['name'];

		        // $totalCount += $tourCount[$country_data['name']];

		        if($countryName == $country_data['name']){
	          		$selectRegionId = $country['id'];
		          	$RegionId[] = $country['id'];
		        }
		        if(isset($tourCountries['countryNames']) && !empty($tourCountries['countryNames'])){ 	
			        if(in_array($country_data['name'], $tourCountries['countryNames'])){
			        	$selectRegionId = $country['id'];
			        	$RegionId[] = $country['id'];
			        }
		    	}
      		} 
      	}

      	$selectRegion = '';
      	if($selectRegionId == $country['id']){
          	$selectRegion = 'checked';	
        }
        if(in_array($country_data['region_id'], $availableTourRegions)){
        	if(array_key_exists($country_data['region_id'], $availableTourCounts)){
        		$total_country_region = $availableTourCounts[$country_data['region_id']];
        	}else{
        		$total_country_region = 0;
        	}
        	
	      	$regionList .= '<p>
						      	<label class="radio-checkbox label_check" for="checkbox-'.$i.'">
							      	<input class="regionList" id="regionList" '.$selectRegion.' type="checkbox" id="checkbox-'.$i.'" value="'.$country['id'].'" name="country[]" >'.$country['name'].' 
						      	</label>
						      	[<span>'.$total_country_region.'</span>]</p>';
		}
    	usort($allCountry, 'sort_by_name');
    	$allCountryRegion[$country['id'].'_'.$country['name']] = $allCountry;
    }
	$RegionId = array_unique($RegionId);
	//dd($country_id_remove);

	foreach ($allCountryRegion as $name => $allcountry) {
		foreach ($allcountry as $country) { 
			if(!in_array($country['id'], $countryIds)){
				if(isset($country_tour_data[$country['id']]))
			    {
			    	$k++;
	        		//if($k <= 10){

	        			$label_partial = '';
	        			$check_checked = 'checked';
	        			$open = '';
						if (in_array($country['id'], $country_id_remove))
						{
							$label_partial = 'label_partial';
							$open 			= "open";
						}
	        			
	        			$cities 		= [];
	        			
	        			if(isset($country_tour_data[$country['id']]))
	        			{
	        				$cities 		= $country_tour_data[$country['id']]['cities'];
	        			}
	        			

			      		$countryList .= '<p style="display:none;" class="regionCountry_'.$country['region'].'">
			        			<label class="radio-checkbox label_check '.$label_partial.'" for="checkbox-'.$country['id'].'" id="label-'.$country['id'].'">
			        				<input class="countryList" data-regionName ="'.$country['regionName'].'" data-region="'.$country['region'].'" data-name="'.$country['name'].'" type="checkbox" id="checkbox-'.$country['id'].'" name="region[]" value="'.$country['id'].'" >'.$country['name'].'
			        			</label>
			        		[<span class="count'.$country['name'].'" id="count'.$country['name'].'">'.$tourCount[$country['name']].'</span>]';
			        		if(!empty($cities))
			        		{
			        			$countryList .=' <a href="javascript://" class="moreCountry morecity'.$country['id'].' '.$open.'" data-id="'.$country['id'].'"><i class="fa fa-plus"></i></a></p>';
			        		}

			        	$countryList 	.= '<div class="moreCountry-block test morecountry'.$country['id'].'">';
			        	
			        	if(!empty($cities))
			        	{
				        	foreach ($cities as $city) {
				    			$check_checked = '';
			    				if (in_array($city['city_id'], $cityIds))
			    				{
			    					$check_checked = "checked";
			    				}			    				

			    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$country['id'].'" data-country-id="'.$country['id'].'" id="checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]" data-name="'.$country['name'].'"  data-regionName ="'.$country['regionName'].'" '.$check_checked.' data-region="'.$country['region'].'" value="'.$city['city_id'].'">'.$city['city_name'].'('.$city['city_count'].')</label></p>';
				    		}
				    	}
			        	
			    		$countryList .='</div>';
		        }
  			}

		}
  	}
?>

	<section class="page-container">
      	<h1 class="hide"></h1>


		    <div class="page-sidebar-wrapper">
				<div class="page-sidebar top-margin">


		          <div class="location-inner m-t-20">
		            <div class="loc-icon">  
		              <svg width="25px" height="25px" viewBox="0 0 25 25" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
		                  <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
		                  <desc>Created with Sketch.</desc>
		                  <defs></defs>
		                  <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                      <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-21.000000, -59.000000)" stroke="#212121">
		                          <g id="Group-4" transform="translate(22.000000, 60.000000)">
		                              <circle id="Oval-2" stroke-width="2" cx="7.66666667" cy="7.66666667" r="7.66666667"></circle>
		                              <path d="M13.0333333,13.0333333 L22.3602359,22.3602359" id="Line" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
		                          </g>
		                      </g>
		                  </g>
		              </svg>
		            </div>

		            <div class="location-content">
		              <h4 class="loc-title">Search Region</h4>
		              <div class="m-t-20 black-box" id="regionBox">
		              	<?php echo $regionList; ?>
		              </div>
		            </div>
		          </div>
		          <hr/>

		         <form method="post" action="tours">
		         	{{ csrf_field() }}
		         <div class="location-inner m-t-20">
		            <div class="loc-icon"></div>
		            <div class="location-content">
		              <h4 class="loc-title">Search Country</h4>
		              <div class="m-t-20 black-box" id="countryBox">
		              	<?php echo $countryList; ?>
		              </div>
		              <p class="m-t-20"><a href="#" data-toggle="modal" data-target="#locationModal" id="openModel">View More Locations</a></p>
		            </div>
		            <input type="hidden" name="countryNames[]" id="country_names">
		            <input type="hidden" name="countryRegion[]" id="region_id">
		            <input type="hidden" name="countryRegionName[]" id="region_name">
		            <input type="hidden" name="removeCountry[]" id="remove_country" value="">
		            <input type="hidden" name="from_page"  value="1">
		            <input type="hidden" name="departure_date" value="{{@$dep_date}}"> 
		            <input type="hidden" name="tour_type" value="{{@$tour_type}}"> 
		            <center><input type="submit" class="btn btn-black btn-block" value="Search"></center>
		          </div>
		          </form>
		         
		        </div>
		      </div>



		    <div class="page-content-wrapper">
				<div class="page-content top-margin">
		      	<div class="left-strip">
		        	<a href="javascript://" class="arrow-btn-new open left_side_arrow_btn"><i class="fa fa-angle-left"></i></a>
		      	</div>
					<div class="tabs-main-container">
				@yield( 'content' )
					</div>
				</div>
			</div>

	</section>

	<!-- Modal -->
    <div class="modal fade" id="locationModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-container">
              <h4 class="modal-title" id="myModalLabel">Select a Country</h4>
            </div>
          </div>
          <div class="modal-body">
            <div class="modal-container">
              <form class="form-horizontal" method="post" action="tours" id="country-model">
				<?php
					$str = '';
					$j=0;
					foreach ($allCountryRegion as $name => $allcountry) {
						$idName = explode('_', $name); 

						$display = 'style="display:none;"';
						//if($idName[0] == $RegionId) { $display = '';}
						if(in_array($idName[0], $RegionId)) { $display = '';}

						$str .= '<div class="m-t-20 modelRegionCountry black-box" id="modelRegionCountry_'.$idName[0].'" '. $display.'>
									<label>'.$idName[1].'</label>
									<hr/>
									<div class="row">
										<div class="col-sm-6">';
						$count = round(count($allcountry)/2);
						$i=0;

						foreach ($allcountry as $country) {
							if(isset($country_tour_data[$country['id']]))
			        		{
								$modelSelect = '';
								$open = '';
								 if(isset($tourCountries['countryNames']) && !empty($tourCountries['countryNames'])){
							        if(!empty($countryIds) && in_array($country['id'], $countryIds)){
							        	$modelSelect = 'checked';
							        	$open = "open";        
							        } else if($countryName == $country['name']){
							          	$modelSelect = 'checked';
							          	$open = "open";
							        } else {
							        	$modelSelect = '';
							        	$open = '';
							        }
						    	}
						    	$label_partial = '';

						    	if (in_array($country['id'], $country_id_remove))
								{
									$label_partial = 'label_partial';
									$open = "open";
								}

			        			
			        			$cities 		= $country_tour_data[$country['id']]['cities'];			        			

						    	//$cityTourCount = cityTourCount($country['id']);
								$str .= '<p class="modelCountryList">
										<label class="radio-checkbox label_check '.$label_partial.'" for="checkbox-model-'.$country['id'].'" id="model-label-'.$country['id'].'">
											<input class="countryListModel" '.$modelSelect.' data-regionName ="'.$idName[1].'" data-region="'.$country['region'].'" data-name="'.$country['name'].'" type="checkbox" id="checkbox-model-'.$country['id'].'" name="region[]" value="'.$country['id'].'" >'.$country['name'].' 
										</label>
									[<span class="count'.$country['name'].'">'.$tourCount[$country['name']].'</span>]';
									
									if(!empty($cities))
			        				{
			        					
										$str .= '<a href="javascript://" class="modelmoreCountry modelmorecity'.$country['id'].' '.$open.'" data-id="'.$country['id'].'"><i class="fa fa-plus"></i></a></p>';
									}
								
					    		$str .= '<div class="moreCountry-block modelmorecountry'.$country['id'].'">';

					    		if(!empty($cities))
					    		{
						    		foreach ($cities as $city) 
						    		{
						    			if($city['city_count'] != 0)
						    			{
						    				$check_checked = '';
						    				if (in_array($city['city_id'], $cityIds))
						    				{
						    					$check_checked = "checked";
						    				}		
						    				$str .= '<p><label class="radio-checkbox label_check" for="model-checkbox-'.$city['city_id'].'"><input type="checkbox" class="modelcityList modelcityList'.$country['id'].'" data-country-id="'.$country['id'].'" id="model-checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]" data-name="'.$country['name'].'"  data-regionName ="'.$country['regionName'].'" '.$check_checked.' data-region="'.$country['region'].'" value="'.$city['city_id'].'">'.$city['city_name'].'('.$city['city_count'].')</label></p>';
						    					
						    			}
						    		}
						    	}
					    		$str .= '</div>';
								$i++;
								$j++;

								if($i%$count == 0) {$str .= '</div><div class="col-sm-6">';}
							}
						}
						$str .= '</div></div></div>';
	              	}
	              	echo $str;
              	?>

	            <div class="m-t-10">
	                <div class="m-t-30 text-right">
		                {{ csrf_field() }}
		                <input type="hidden" name="countryNames[]" id="model_country_names">
			            <input type="hidden" name="countryRegion[]" id="model_region_id">
			            <input type="hidden" name="countryRegionName[]" id="model_region_name">
			            <input type="hidden" name="removeCountry[]" id="model_remove_country">
			            <input type="hidden" name="tour_type" value="{{@$tour_type}}"> 
		                <a href="#" data-dismiss="modal" class="m-r-10">DECLINE</a>
		                <!-- <a href="javascript://" id="saveCountry">ACCEPT</a> -->
		                <input type="submit" value="ACCEPT" class="btnlink">
	                </div>
	            </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script type="text/javascript" src="{{ url( 'js/theme/jquery.slimscroll.js' ) }}"></script>

    <script type="text/javascript">
    	var countryName   		= '{{ $countryName }}';
      	var default_currency   	= '{{ $default_currency }}';

      	calculateHeight();
        function calculateHeightOld(){
            var winHeight = $(window).height();

            var oheight = $('.page-sidebar').outerHeight()-10;// -22
            var oheight1 = $('.page-sidebar').outerHeight();
            var elem = $('.page-content .tabs-container').outerHeight();
            var elemHeight = oheight - elem;
            $(".page-content .tour-tabs-container").outerHeight(elemHeight);

            var elemHeight1 = (oheight1 - elem)+20;
            var winelemHeight = winHeight - elem;

            if(winHeight < oheight){
                $(".page-content .tour-tabs-container").outerHeight(elemHeight1);

            } else{
                $(".page-content .tour-tabs-container").outerHeight(winelemHeight);

            }
            

            $(".page-content .hotel-container").outerHeight(elemHeight);
            var tourTop = $('.tour-top-content').outerHeight(); // .page-content
            var tourTabs = $('.tour-tabs').outerHeight();
            var tourHeight = (elemHeight - tourTop) - tourTabs;
            var winTourHeight = (winelemHeight - tourTop) - tourTabs;
            
            if(winHeight < oheight1){
                //$(".tabs-content-container-new").outerHeight(elemHeight);
                $(".tour-bottom-content").outerHeight(tourHeight);
            } else{
                //$(".tabs-content-container-new").outerHeight(winelemHeight);
                $(".tour-bottom-content").outerHeight(winTourHeight);
            }
            //$(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
        }
        function calculateHeight(){ //alert(1);
            var winHeight = $(window).height();
            var oheight = $('.page-sidebar').outerHeight();
            //alert(oheight);
            var elem = $('.page-content .tabs-container').outerHeight();
            //alert(elem);
            var elemHeight = oheight - elem;
            //alert(elemHeight);
            var winelemHeight = winHeight - elem;
            if(winHeight < oheight){
                $(".page-content .tour-tabs-container").outerHeight(elemHeight);

            } else{
                $(".page-content .tour-tabs-container").outerHeight(winelemHeight);

            }

			/*---- tour content ------*/
            var tourTop = $('.page-content .tour-top-content').outerHeight();
            //alert(tourTop);
            var tourTabs = $('.page-content .tour-tabs').outerHeight() + 15;
            //alert(tourTabs);
            var tourHeight = (elemHeight - tourTop) - tourTabs;
            //alert(tourHeight);
            var winTourHeight = (winelemHeight - tourTop) - tourTabs;
            if(winHeight < oheight){
                //$(".tabs-content-container-new").outerHeight(elemHeight);
                //$(".tour-bottom-content").outerHeight(tourHeight);
            } else{
                //$(".tabs-content-container-new").outerHeight(winelemHeight);
                //$(".tour-bottom-content").outerHeight(winTourHeight);
            }
        }

    	$(document).ready(function(){
    		//calculateWidth();
    		//countryTourCount();
          	leftpanel();
            calculateHeight();
            var removeC = [];
            var str = '<?php echo @$country_id_remove_push;?>';
            var res = str.split(",");
            res.forEach(function(element) {
			  removeC.push(element);
			});
            	     
            $("#remove_country").val(removeC);
            $('.moreCountry').click(function(){
            	var country_id = $(this).attr('data-id');
            	var morecountry_class = "morecountry"+country_id;
            	var morecity = "morecity"+country_id;
            	$('.'+morecountry_class).slideToggle();
	            $('.'+morecity).toggleClass('open');
	            calculateHeight();
	        });

            $(".modelmoreCountry").click(function(){
            	var country_id = $(this).attr('data-id');
            	var morecountry_class = "modelmorecountry"+country_id;
            	var morecity = "modelmorecity"+country_id;
            	$('.'+morecountry_class).slideToggle();
	            $('.'+morecity).toggleClass('open');
	            calculateHeight();
	        });


	    	$("#Slider1").slider({ 
	          from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "", skin: "round", limits: false 
	        });

	        $("#Slider2").slider({ 
	          from: 1, to: 100, step: 1 , smooth: true, round: 0, dimension: "&nbsp;Days", skin: "round", limits: false
	        });

	        $('#scroll-box').slimScroll({
	            height: '99%',
	            color: '#212121',
	            opacity: '0.7',
	            size: '5px',
	            allowPageScroll: true
	        });

	        $('.tourListGridBox, .gridTourList').slimScroll({
	          height: '100%',
	          color: '#212121',
	          opacity: '0.7',
	          size: '5px',
	          allowPageScroll: true
	        });

	        $('.tourDetailDiv,.bookScroll,.tourViewItineraryDiv').slimScroll({
                height: '600px',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

            $('.tourItineraryDiv, .tourmakeAnEnquiry').slimScroll({
                height: '94%',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

            $(".countryList").click(function(){
            	var val_id = $(this).val();
            	var country_checkall = "morecountry"+val_id;
            	$("#label-"+val_id).removeClass('label_partial');
            	if (this.checked) 
            	{
		            $("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = true;			            
			        });
			    }
			    else
			    {
			    	$("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = false;			            
			        });
			    }
		    });            

	    	var regionIds = [];
	    	var i = 0;
	    	var countryListCount = 0;
	    	$('.countryList:checkbox:checked').each(function(){
	    		if($(this).parent('p').css('display') != 'none'){
	    			countryListCount++;
	    		}
	    	});

	    	var cityListCount = 0;
	    	$('.cityList:checkbox:checked').each(function(){
	    		if($(this).parent('p').css('display') != 'none'){
	    			cityListCount++;
	    		}
	    	});	    	

	    	//console.log('countryListCount = '+countryListCount);

	      	$('#regionList:checkbox:checked').each(function(){
	          	var id = parseInt($(this).val());
	          	regionIds.push(id);	          

	          	$('.regionCountry_'+id).each(function(){
	            	i++; 
	            	if(i >= countryListCount && i <=10)
	            	{
	            		//console.log(i);
	              		$(this).show();
	            	}
	          });
	      	});

	        $('#regionList').click(function(){
	        	
	        	var regionId = parseInt($(this).val());
	            $("#countryBox").find('p').hide();
	            $(".modelRegionCountry").hide();

	            if (!$(this).is(':checked')) {
	                var id = parseInt($(this).val());

	                $('.regionCountry_'+id+' :input').each(function(){ 
	                    var conId = $(this).val();
	                    if ($(this).is(':checked')) {
	                      $("#checkbox-model-"+conId).attr('checked', false);
	                      $(this).attr('checked', false);
	                    }
	                });
	        	}

		        var i=0;
		        $('#regionList:checkbox:checked').each(function(){
	              var id = parseInt($(this).val());
	              regionIds.push(id);

	              $("#modelRegionCountry_"+id).show();
	              
	              $('.regionCountry_'+id).each(function(){
	                i++; //console.log('onclick = '+i);
	                if(i >= 1 && i <=10){
	                  $(this).show();
	                }
	              });

	            }); 

	            if(i == 0) { $(".modelRegionCountry").show(); }
	        });

	        $('.countryList').click(function(){

	            var id = parseInt($(this).val());
	            if ($(this).is(':checked')) {
	              $("#checkbox-model-"+id).attr('checked', true);   
	            } else{
	              $("#checkbox-model-"+id).attr('checked', false); 
	            }

	            var countryIds = [];
	            var countryNames = [];
	            var countryRegions = [];
	            var countryRegionNames = [];
	            var countryIdRemove = [];
	            var cityID = [];
	            var cityIDCount = [];

	            $('.countryList:checked').each(function(){
	              var id = parseInt($(this).val());
	              var name = $(this).attr('data-name');
	              var region = $(this).attr('data-region');
	              var regionName = $(this).attr('data-regionName');

	              countryIds.push(id);
	              countryNames.push(name);
	              countryRegions.push(region);
	              countryRegionNames.push(regionName);	
	            });	            

	            var  data = { 
	              tourCountryData: [{
	                countryIds:countryIds, 
	                countryNames: countryNames,
	                countryRegions: countryRegions,
	                countryRegionNames: countryRegionNames,
	                regionIds: regionIds,
	                search_type : 'country',
	                cityIds:cityID, 
	                countryIdRemove : countryIdRemove
	              }]
	            };
	            $("#country_names").val(countryNames);
	            $("#region_id").val(countryRegions);
	            $("#region_name").val(countryRegionNames);
	            $("#remove_country").val(countryIdRemove);

	        });

			$('.cityList').click(function(){

	            var id = parseInt($(this).val());
	            var country_id_chk = $(this).attr('data-country-id'); 

	            var cityListId = "cityList"+country_id_chk;
	            var numberOfChildCheckBoxes = $('.'+cityListId).length;
	            var countryIdRemove = [];

	            var rCountry = $("#remove_country").val();

	            countryIdRemove.push(rCountry);
              	if(jQuery.inArray( country_id_chk, rCountry ))
              	{
              		countryIdRemove.push(country_id_chk);
              	}
              	
              	$("#label-"+country_id_chk).addClass('label_partial');              	
              	$("#model-label-"+country_id_chk).addClass('label_partial');

				//$('.'+cityListId).change(function() {
				  	var checkedChildCheckBox = $('.'+cityListId+':checked').length;
				  	if (checkedChildCheckBox == numberOfChildCheckBoxes)
				  	{
				  		$("#checkbox-"+country_id_chk).prop('checked', true);
				    	$("#label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#label-"+country_id_chk).removeClass('label_partial');
				    	$("#label-"+country_id_chk).removeClass('c_on');

				    	$("#checkbox-model-"+country_id_chk).prop('checked', true);
				    	$("#model-label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#model-label-"+country_id_chk).removeClass('label_partial');
				    	$("#model-label-"+country_id_chk).removeClass('c_on');

				   	}
				   	else if(checkedChildCheckBox == 0)
				   	{
				   		$("#label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-"+country_id_chk).attr('checked', false); 

				   		$("#model-label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-model-"+country_id_chk).attr('checked', false); 

				   	}
				  	else
				  	{
				    	$("#checkbox-"+country_id_chk).prop('checked', false);
				    	$("#checkbox-model-"+country_id_chk).prop('checked', false);
				    }
				//});

	            if ($(this).is(':checked')) {
	              $("#checkbox-model-city-"+id).attr('checked', true);   
	            } else{
	              $("#checkbox-model-city-"+id).attr('checked', false); 
	            }

	            var cityIds = [];
	            var cityNames = [];
	            var cityRegions = [];
	            var cityRegionNames = [];
	            
	            $('.cityList:checked').each(function(){
	              	var id = parseInt($(this).val());
	              	var name = $(this).attr('data-name');
	              	var region = $(this).attr('data-region');
	              	var regionName = $(this).attr('data-regionName');

	              	cityIds.push(id);
	              	cityNames.push(name);
	              	cityRegions.push(region);
	              	cityRegionNames.push(regionName);	              	
	            });

	            var countryID = [];
	            $('.countryList:checkbox:checked').each(function(){
		              var id = $(this).val();
		              countryID.push(id);
		              totalTourCount = totalTourCount + parseInt($("#count"+id).text());
		        });

	            var  data = { 
	              	tourCityData: [{
		                cityIds:cityIds, 
		                countryIds:countryID, 
		                countryNames: cityNames,
		                countryRegions: cityRegions,
		                countryRegionNames: cityRegionNames,
		                regionIds: regionIds,
		                search_type : 'city',
		                countryIdRemove : countryIdRemove
	              	}]
	            };

	            $("#region_id").val(cityRegions);
	            $("#region_name").val(cityRegionNames);

	            $("#remove_country").val(countryIdRemove);

	        });

			$('.modelcityList').click(function(){

	            var id = parseInt($(this).val());
	            var country_id_chk = $(this).attr('data-country-id'); 

	            var cityListId = "modelcityList"+country_id_chk;
	            var numberOfChildCheckBoxes = $('.'+cityListId).length;
	            var countryIdRemove = [];
	            var rCountry = $("#remove_country").val();

              	if(jQuery.inArray( country_id_chk, rCountry ))
              	{
              		countryIdRemove.push(country_id_chk);
              	}
              	$("#label-"+country_id_chk).addClass('label_partial');
              	$("#model-label-"+country_id_chk).addClass('label_partial');

				//$('.'+cityListId).change(function() {
					
					var checkedChildCheckBox = $('.'+cityListId+':checked').length;

				  	if (checkedChildCheckBox == numberOfChildCheckBoxes)
				  	{
				  		$("#checkbox-model-"+country_id_chk).prop('checked', true);
				    	$("#model-label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#model-label-"+country_id_chk).removeClass('label_partial');
				    	$("#model-label-"+country_id_chk).removeClass('c_on');

				    	$("#checkbox-"+country_id_chk).prop('checked', true);
				    	$("#label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#label-"+country_id_chk).removeClass('label_partial');
				    	$("#label-"+country_id_chk).removeClass('c_on');
				   	}
				   	else if(checkedChildCheckBox == 0)
				   	{
				   		$("#model-label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-model-"+country_id_chk).attr('checked', false); 

				   		$("#label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-"+country_id_chk).attr('checked', false); 
				   	}
				  	else
				  	{
				    	$("#checkbox-model-"+country_id_chk).prop('checked', false);
				    	$("#checkbox-"+country_id_chk).prop('checked', false);
				    }
				//});

	            if ($(this).is(':checked')) {
	              $("#checkbox-model-city-"+id).attr('checked', true);   
	            } else{
	              $("#checkbox-model-city-"+id).attr('checked', false); 
	            }

	            var cityIds = [];
	            var cityNames = [];
	            var cityRegions = [];
	            var cityRegionNames = [];
	            
	            $('.cityList:checked').each(function(){
	              	var id = parseInt($(this).val());
	              	var name = $(this).attr('data-name');
	              	var region = $(this).attr('data-region');
	              	var regionName = $(this).attr('data-regionName');

	              	cityIds.push(id);
	              	cityNames.push(name);
	              	cityRegions.push(region);
	              	cityRegionNames.push(regionName);
	              	
	            });

	            var countryID = [];
	            $('.countryList:checkbox:checked').each(function(){
		              var id = $(this).val();
		              countryID.push(id);
		              totalTourCount = totalTourCount + parseInt($("#count"+id).text());
		        });

	            var  data = { 
	              	tourCityData: [{
		                cityIds:cityIds, 
		                countryIds:countryID, 
		                countryNames: cityNames,
		                countryRegions: cityRegions,
		                countryRegionNames: cityRegionNames,
		                regionIds: regionIds,
		                search_type : 'city',
		                countryIdRemove : countryIdRemove
	              	}]
	            };

	            $("#model_region_id").val(cityRegions);
	            $("#model_region_name").val(cityRegionNames);
	            $("#model_remove_country").val(countryIdRemove);

	        });

	        $('.countryListModel').click(function(){
	            var id = parseInt($(this).val());
	            if ($(this).is(':checked')) { 
	              $("#checkbox-model-"+id).attr('checked', true);
	            } else{ 
	              $("#checkbox-model-"+id).attr('checked', false);
	            }

	            var val_id = $(this).val();
            	var country_checkall = "modelmorecountry"+val_id;
            	$("#model-label-"+val_id).removeClass('label_partial');
            	if (this.checked) 
            	{
		            $("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = true;			            
			        });
			    }
			    else
			    {
			    	$("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = false;			            
			        });
			    }

			    var countryIds = [];
	            var countryNames = [];
	            var countryRegions = [];
	            var countryRegionNames = [];
	            var countryIdRemove = [];
	            var cityID = [];
	            var cityIDCount = [];

	            $('.countryListModel:checked').each(function(){
	              var id = parseInt($(this).val());
	              var name = $(this).attr('data-name');
	              var region = $(this).attr('data-region');
	              var regionName = $(this).attr('data-regionName');

	              countryIds.push(id);
	              countryNames.push(name);
	              countryRegions.push(region);
	              countryRegionNames.push(regionName);	
	            });	            

	            var  data = { 
	              tourCountryData: [{
	                countryIds:countryIds, 
	                countryNames: countryNames,
	                countryRegions: countryRegions,
	                countryRegionNames: countryRegionNames,
	                regionIds: regionIds,
	                search_type : 'country',
	                cityIds:cityID, 
	                countryIdRemove : countryIdRemove
	              }]
	            };
	            $("#model_country_names").val(countryNames);
	            $("#model_region_id").val(countryRegions);
	            $("#model_region_name").val(countryRegionNames);
	            $("#model_remove_country").val(countryIdRemove);
	        });

	        $('body').on('click', '#saveCountry', function() {

	            $(this).text('LOADING...');

	            $.ajax({
				    type : 'POST',
				    url : 'set_session_tours',
				    data : $('#country-model').serialize(),				    
        			dataType: 'json',
				    success: function(response) {
				    	console.log(response);
				    	if(response.msg == "success")
				    	{
                        	location.reload();
                        }
                    }
				});
	            
	        }); 



			var z=0;
	        $('#regionList:checkbox:checked').each(function(){
                z++; 
            });
            if(z == 0) { $(".modelRegionCountry").show(); }

    	});

		

	    function leftpanelOld(){
	        var w = $( document ).width();
	          if(w > 991){
	            $('.arrow-btn').click(function(){
	              if($('.arrow-btn').hasClass('open')){
	                $('.location-wrapper').css('display', 'none');
	                $(this).removeClass('open');
	                $('.tabs-wrapper').css('margin-left', '0px');
	              } else{
	                $('.location-wrapper').css('display', 'block');
	                $(this).addClass('open');
	                $('.tabs-wrapper').css('margin-left', '350px');
	              }
	           });
	          } else{
	            $('.arrow-btn').click(function(){
	              if($('.arrow-btn').hasClass('open')){
	                $('.location-wrapper').css('display', 'none');
	                $(this).removeClass('open');
	                $('.tabs-wrapper').css('margin-left', '0px');
	                $('.create-strip').removeClass('hideShow-panel'); 
	              } else{
	                $('.location-wrapper').css('display', 'block');
	                $(this).addClass('open');
	                $('.tabs-wrapper').css('margin-left', '0px');
	                $('.create-strip').addClass('hideShow-panel');
	              }
	          });
	        }
	    }

        function leftpanel(){
            var w = $( document ).width();
            if(w > 991){
                $('.arrow-btn-new').click(function(){
                    if($('.arrow-btn-new').hasClass('open')){
                        $('.page-sidebar').css('display', 'none');
                        $(this).removeClass('open');
                        $('.page-content').css('margin-left', '0px');
                    } else{
                        $('.page-sidebar').css('display', 'block');
                        $(this).addClass('open');
                        $('.page-content').css('margin-left', '354px');
                    }
                });
            } else{
                $('.arrow-btn-new').click(function(){
                    if($('.arrow-btn-new').hasClass('open')){
                         $('.page-sidebar').css('display', 'block');
                        $(this).removeClass('open');
                        $('.page-content').css('margin-left', '0px');
                        $('.left-strip').css('left', '300px');
                    } else{
                        $('.page-sidebar').css('display', 'none');
                        $(this).addClass('open');
                        $('.page-content').css('margin-left', '0px');
                        $('.left-strip').css('left', '0px');
                    }
                });
            }
        }

	    function calculateWidth(){
	        var owidth = $('.custom-tabs .nav-tabs').parent().width();
	        $(".tabs-container .custom-tabs .nav-tabs>li").css('padding',0);
	        var elem = $('.tabs-container .custom-tabs .nav-tabs>li').length;
	        var elemWidth = (owidth / elem );
	        //alert(owidth+'//'+elem+'=='+elemWidth);
	        $(".tabs-container .custom-tabs .nav-tabs>li").width(elemWidth);
	    }

	    $(window).resize(function(){
	        calculateWidth();
	        leftpanel();
            calculateHeight();
	    });
    </script>

@include( 'layouts.partials.footer' )