 $(window).on('load',function(){
        $('#onForgotModal').modal('show');
    });


$.validator.addMethod("pwcheck", function(value) {
        return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/.test(value) 
    }, "Password must contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:");


    $(document).ready(function() {
        $("#onForgotModal").click();
        $("#reset-form").validate({
            rules: {
                password: {
                  required: true,
                  minlength: 8,    
                  pwcheck : true, 
                },
                password_confirmation: {
                    equalTo : "#password"
                },
            },
            messages: {

                email: {
                    required: "Please enter Email Address."
                },
                password_confirmation: {
                    equalTo: 'Password and Confirm Password must be same.'
                },

            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });