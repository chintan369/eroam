
<!---Profile Popup Modal--->
<div id="preferencesModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg middbox">
        <div class="modal-content modalbox">
            <div class="modal_outer m-0">
                <div class="modal-header profile_modal">
                    <h4>Personal Preferences</h4>
                    <div class="clearfix"></div>
                    <p> By entering your personal preferences for hotels, transport, activities, this assists eRoam to create a more personalised travel options for you. </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ic-clear"></i></button>
                </div>
                <div class="modalform personal_profile_form">
                    <form action="" class="preferences_form">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">

                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label>Select Age Group</label>
                                        <div class="selectmenu">
                                            <select class="age_group" name="age_group" id="dropdown-age">
                                              <option value="">Select Age Group</option>
                                                {!! $age_group !!}
                                           </select>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="col-12 col-sm-6 col-md-6">

                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label>Select Transport Type</label>
                                        <div class="selectmenu">
                                            <select class="transport_type_options" name="transport_types[]" id="dropdown-transports">
                                              <option value="0">Select Transport Type</option>
                                              <option value="1" {{($transport_select == 1) ? 'selected' : '' }}>Flight</option>
                                              <option value="2" {{ ($transport_select == 2) ? 'selected' : '' }}>Overland</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label>Select Gender</label>
                                        <div class="selectmenu">
                                            <select class="gender" name="gender" id="dropdown-gender">
                                                <option value="">Select Gender</option>
                                                {!! $gender !!}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label>Select Accommodation Type</label>
                                        <div class="selectmenu">
                                            <select class="hotel_category" name="hotel_category_id[]" id="dropdown-accommodation">
                                              <option value="0">Select Accommodation Type</option>
                                              <option value="1" {{ ($accommo_select == 1) ? 'selected' : '' }}> 1 Star</option>
                                              <option value="5" {{ ($accommo_select == 5) ? 'selected' : '' }}> 2 Star</option>
                                              <option value="2" {{ ($accommo_select == 2) ? 'selected' : '' }}> 3 Star</option>
                                              <option value="3" {{ ($accommo_select == 3) ? 'selected' : '' }}> 4 Star</option>
                                              <option value="9" {{ ($accommo_select == 9) ? 'selected' : '' }}> 5 Star</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <div class="fildes_outer">
                                        <label>Select Nationality</label>
                                        <div class="selectmenu">
                                            <select class="nationality" name="nationality" id="dropdown-nationality">
                                              <option value="">Select Nationality</option>
                                                {!! $nationality !!}
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="activity_preferences_title text-left">
                        <h4>Activity Preferences</h4>
                        Please drag your preferred activities into the boxes below. Highest priority begins on the left.
                    </div>
                    <div class="row activity_preferences__sec">
                        <div class="col-12 pb-2">
                            <ul class="drop-list">
                        @php 
                            $imgArr = array(
                            15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                            5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                            13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                            29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                            11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                            20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                             //'Expeditions','Astronomy','Food','Short-Breaks'
                            );
                            $ij = 0;
                            if( count($interest_ids) > 0 ) {
                            foreach ($interest_ids as $j => $interest_id) {
                                $key = array_search($interest_id, array_column($labels, 'id'));
                                $label = $labels[$key];
                                $ij++;
                        @endphp
                    
                            <li ondrop="drop(event)" ondragover="allowDrop(event)" class="interest-button-active" id="interest-{{ $label['id'] }}" data-sequence="{{ $ij }}" data-name="{{ $label['name'] }}">
                                        <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $j }} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                                        <input type="hidden" id="minputValue_{{ $label['id'] }}" class="input-interest" name="interests[{{$j + 1}}]" value="{{ $label['id'] }}">
                            </li>
                            @php } } 
                            for($k = $ij; $k < 8; $k++)
                            {
                                echo '<li ondrop="drop(event)" ondragover="allowDrop(event)" class="blankInterest" data-sequence="'.($k + 1).'"></li>';
                            }
                            @endphp
                                </ul>
                            </div>
                        </div>


                        <div class="row activity_preferences__sec">
                            <div class="col-12">
                                <ul class="drag-list interest-lists first">
                                    @if( count($labels) > 0 ) @foreach ($labels as $i => $label) @if( empty($label['name']) ) @continue @endif
                                    <li ondrop="drop(event)" ondragover="allowDrop(event)" data-value="{{$label['id']}}" class="{{ in_array($label['id'], $interest_ids ) ? ' interestMove' : ''  }}" id="interest-{{ $label['id'] }}">
                                        <?php
                                        $namedis = 'block';
                                        if(!in_array($label['id'], $interest_ids )){
                                        $namedis = 'none';
                                        ?>
                                            <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $i}} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                                            <input type="hidden" id="inputValue_{{ $label['id'] }}" class="input-interest" name="interests[]" value="">
                                            <?php } ?>
                                                <span style="display:{{ $namedis }};" id="name_{{ $label['id'] }}">{{ $label['name'] }}</span>
                                    </li>
                                    @endforeach @endif
                                </ul>
                            </div>
                        </div>


                        <div class="modalbottom_btn">
                            <a href="#" data-dismiss="modal" onclick="$('form.preferences_form')[0].reset();" class="m-r-10"> DECLINE</a>
                            <a href="javascript://" id="save_travel_preferences">Save</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---END Profile Popup Modal--->


