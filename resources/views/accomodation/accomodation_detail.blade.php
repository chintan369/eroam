@extends('layouts.common')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('css/jcarousel.connected-carousels.css')}}">
<div class="body_sec">
    <div class="itinerary_block">
        <input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
        <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
        <div class="booking-summary">
            <span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>
        </div>
        @include('accomodation.partials.accomodation_details')
    </div>
</div>
@endsection
@push('scripts') 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
    <script src="{{url('js/itinerary/common.js')}}"></script>
    <script src="{{url('js/booking-summary.js')}}"></script>
    <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
    <script src="{{url('js/owl.carousel.js')}}"></script>
    <script src="{{url('js/jquery.jcarousel.min.js')}}"></script>
    <script src="{{url('js/jcarousel.responsive.js')}}"></script>
    <script type="text/javascript">
        var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
        var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}";
        var listOfCurrencies = JSON.parse($('#currency-layer').val());
        @if( session()->has('search_input') )
        <?php $input = session()->get('search_input');?>
        var travel_pref = [{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}];
        @else
        var travel_pref = [];
        @endif
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                margin: 10,
                loop: true,
                dots: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            });
            var map, marker, infowindow, infowindowContent;
            init_map();
        })

        function init_map() {
            map = new google.maps.Map(document.getElementById('map1'), {
                zoom: 11
            });
            // INFO WINDOW
            infowindow = new google.maps.InfoWindow();
            infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);

            // MARKER
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            var searchBox = $('#pac-input');
            var geocoder = new google.maps.Geocoder;

            if (searchBox.data('lat') != '' && searchBox.data('lng')) { // REVERSE GEOCODING
                var latlng = {
                    lat: searchBox.data('lat'),
                    lng: searchBox.data('lng')
                };
                geocoder.geocode({
                    'location': latlng
                }, function(results, status) {
                    if (status.toLowerCase() == 'ok') {
                        var service = new google.maps.places.PlacesService(map);
                        service.getDetails({
                            placeId: results[0].place_id
                        }, function(place, status) {
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                showPlaceOnMap(place);
                            }
                        });
                    }
                });

            } else {
                geocoder.geocode({
                    address: searchBox.val(),
                    region: 'no',
                }, function(results, status) {
                    if (status.toLowerCase() == 'ok') {
                        var service = new google.maps.places.PlacesService(map);
                        service.getDetails({
                            placeId: results[0].place_id
                        }, function(place, status) {
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                showPlaceOnMap(place);
                            }
                        });
                    }
                });
            }
        }

        function showPlaceOnMap(place) {
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        }

        var search_session = JSON.parse( $('#search-session').val() );
        var city_id = "{{$data->city_id}}";
        var leg = "{{$data->leg}}";

        $('body').on('click', '.selected-room', function() {
            $('.selected-room').prop('disabled', false);
            $(this).prop('disabled', true);
            var data = $(this).data();

            data.leg = leg;
            if (data.provider == 'expedia') {
                default_provider = data.provider;
            }

            if (data.provider == 'eroam' && data.roomName.includes('Dorm')) {
                var perPersonPrice = Math.ceil(eroam.convertCurrency(data.price, data.currency)).toFixed(2);
            } else {
                var perPersonPrice = Math.ceil(eroam.convertCurrency(data.price, data.currency)).toFixed(2);
            }

            $('#hotel-price-' + data.hotelId).html('From Price <strong>' + globalCurrency + ' ' + Math.ceil(perPersonPrice).toFixed(2) + '</strong> (Per Night Per Person)');
            $('.hotel-list').removeClass('selected-hotel').attr('data-is-selected', 'false');
            $('.hotel-list1-' + data.id).addClass('selected-hotel').attr('data-is-selected', 'true').attr('data-price', data.price).attr('data-price-id', data.priceId);
            $('.room-types').modal('hide');

            var session = search_session.itinerary;

            if (session.length > 0) {
                if (session[leg].city.id == city_id) {
                    eroam.ajax('post', 'room-details', data, function(response) {
                        data = response;
                        var data = JSON.parse(data);
                        search_session.itinerary[leg].hotel = data;
                        if (leg == 0) {
                            search_session.itinerary[0].city.accommodation = true;
                        }
                        bookingSummary.update(JSON.stringify(search_session));
                    });
                }
            }
        });

        var connector = function(itemNavigation, carouselStage) {
            return carouselStage.jcarousel('items').eq(itemNavigation.index());
        };

        $(function() {
            // Setup the carousels. Adjust the options for both carousels here.
            var carouselStage = $('.carousel-stage').jcarousel();
            var carouselNavigation = $('.carousel-navigation').jcarousel();
            console.log("ss");

            // We loop through the items of the navigation carousel and set it up
            // as a control for an item from the stage carousel.
            carouselNavigation.jcarousel('items').each(function() {
                var item = $(this);

                // This is where we actually connect to items.
                var target = connector(item, carouselStage);

                item
                    .on('jcarouselcontrol:active', function() {
                        carouselNavigation.jcarousel('scrollIntoView', this);
                        item.addClass('active');
                    })
                    .on('jcarouselcontrol:inactive', function() {
                        item.removeClass('active');
                    })
                    .jcarouselControl({
                        target: target,
                        carousel: carouselStage
                    });
            });

            // Setup controls for the stage carousel
            $('.prev-stage')
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .jcarouselControl({
                    target: '-=1'
                });

            $('.next-stage')
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .jcarouselControl({
                    target: '+=1'
                });

            // Setup controls for the navigation carousel
            $('.prev-navigation')
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .jcarouselControl({
                    target: '-=1'
                });

            $('.next-navigation')
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .jcarouselControl({
                    target: '+=1'
                });
        });
    </script>
@endpush