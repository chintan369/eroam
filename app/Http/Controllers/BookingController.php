<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BookingController extends Controller
{
	public function pax_information_page() {
		$pax = session()->get('search')['pax_information'];
		$featured_nationalities = json_decode(json_encode(http( 'GET', 'featured/nationalities', NULL)), false);
		$nationalities = json_decode(json_encode(http( 'GET', 'not-featured/nationalities', NULL)), false);
		$filled = session()->get('search')['travellers'] == count(session()->get('search')['pax_information']) ? true : false;
		return view('booking.pax-information')->with(compact('featured_nationalities', 'nationalities', 'pax', 'filled'));
	}
	public function confirm_page() {

	}
	public function checkout_page() {

	}
}
