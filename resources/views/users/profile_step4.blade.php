@extends('layouts.common')
@section('content')
 @include('partials.banner')
 @include('partials.search')
   
   <div class="account-block">
         @include('users.partials.sidebar')
        
        <div class="account-right p-4 pl-5">
        @include('users.partials.complete_profile')

        @if($link != '')
        <div class="mt-4 row">
        <div class="col-xl-4 offset-xl-8 col-sm-6 offset-sm-6">
          <div class="row">
            <div class="col-sm-5 col-5">
              <a href="{{ $link }}" class="btn btns_input_dark btn-block">UPDATE</a>
            </div>
            <div class="col-sm-7 col-7">Update your personal travel preferences.</div>
          </div>
        </div>
        </div>
        @endif

        @if(session()->has('profile_step1_success'))
            <p class="success-box m-t-30">
                {{ session()->get('profile_step1_success') }}
            </p>
        @endif
        @if(session()->has('profile_step1_error'))
            <p class="danger-box m-t-30">
                {{ session()->get('profile_step1_error') }}
            </p>
        @endif
          <hr class="mt-5">
          <h5 class="mt-4">Your Contact Preferences</h5>
          <form class="mt-3" method="post" action="{{url('profile_step4')}}" id="profile_step1_form">
            <div class="form-group">
              <div class="fildes_outer">
                <label>Email Address *</label>
                <input type="text" name="email" value="{{ $user->customer->email }}" class="form-control" placeholder="" />  
                @if ($errors->has('email')) 
                 <label for="email" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('email')}}</label>
                @endif

              </div>
            </div>
            <div class="form-group">    
              <div class="fildes_outer">  
                <label>Contact Phone Number</label>
                 <input type="text" name="contact_no" value="{{ $user->customer->contact_no }}" class="form-control" placeholder="" /> 
                 @if ($errors->has('contact_no')) 
                <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('contact_no')}}</label>
              @endif
              </div>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                <label>Mobile Number</label>
                 <input type="text" name="mobile_no" value="{{ $user->customer->mobile_no }}" class="form-control" placeholder="" />
              </div>
                <label for="mobile_no" generated="true" class="error"></label>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                  <label>Preferred Contact Method</label>
                  <div class="custom-select">
                    <select class="form-control" name="pref_contact_method">
                        <option value="" >Select Preferred Contact Method</option>
                        <option value="phone_call" {{ $user->customer->pref_contact_method == 'phone_call' ? 'selected' : '' }}>Phone Call</option>
                        <option value="sms" {{ $user->customer->pref_contact_method == 'sms' ? 'selected' : '' }}>SMS</option>
                        <option value="email" {{ $user->customer->pref_contact_method == 'email' ? 'selected' : '' }}>Email</option>
                        <option value="mail" {{ $user->customer->pref_contact_method == 'mail' ? 'selected' : '' }}>Mail</option>
                    </select>
                    </div>
                     @if ($errors->has('pref_gender')) 
                        <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_gender')}}</label>
                        @endif
              </div>
            </div>
             <h5 class="mt-4">Physical Address</h5>
             <div class="form-group">
              <div class="fildes_outer">
                <label>Address Line One (1)</label>
                 <input type="text" name="phy_address_1" class="form-control" placeholder="" value="{{ $user->customer->phy_address_1}}"/>
                  @if ($errors->has('phy_address_1')) 
                <label for="phy_address_1" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('phy_address_1')}}</label>
              @endif
              </div>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                <label>Address Line One (2)</label>
                 <input type="text" name="phy_address_2" class="form-control" placeholder="" value="{{ $user->customer->phy_address_2}}"/>
                  @if ($errors->has('phy_address_2')) 
                <label for="phy_address_2" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('phy_address_2')}}</label>
              @endif
              </div>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                  <label>Country</label>
                  <div class="custom-select">
                    <select id="phy_country" name="phy_country" class="form-control phy_country">
                        <option value="">Select Country</option>
                        @foreach ($all_countries as $country)
                            <option value="{{ $country['id'] }}" {{ $user->customer->phy_country == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                        @endforeach                                       
                    </select> 
                    </div>
                     @if ($errors->has('phy_country')) 
                        <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('phy_country')}}</label>
                        @endif
              </div>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                <label>State</label>
                 <input type="text" name="phy_state" class="form-control" placeholder="" value="{{ $user->customer->phy_state}}"/>
                  @if ($errors->has('phy_state')) 
                <label for="phy_state" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('phy_state')}}</label>
              @endif
              </div>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                <label>City</label>
                 <input type="text" name="phy_city" class="form-control" placeholder="" value="{{ $user->customer->phy_city}}"/>
                  @if ($errors->has('phy_city')) 
                <label for="phy_city" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('phy_city')}}</label>
              @endif
              </div>
            </div>
            <div class="form-group">
              <div class="fildes_outer">
                <label>Zip</label>
                 <input type="text" name="phy_zip" class="form-control" placeholder="" value="{{ $user->customer->phy_zip}}"/>
                  @if ($errors->has('phy_zip')) 
                <label for="phy_zip" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('phy_zip')}}</label>
              @endif
              </div>
            </div>
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="step" value="1">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
            <input type="hidden" id="registered_user_id" value="{{ $user->customer->email }}">
            <button type="submit" class="btn btns_input_dark btn-block mt-5 pb-2 pt-2">UPDATE PROFILE</button>
          </form>
        </div>
      </div>

@stop