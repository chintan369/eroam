<h5>Complete Your Account Setup</h5>
<div class="text-right d-block d-lg-none"><a href="javascript://" class="navbtn menu-btn text-secondary"><i class="ic-menu"></i></a></div>
<div class="profile-steps mt-5">
<ul class="nav nav-tabs nav-fill border-0">
  <li class="nav-item {{ ($user->customer->step_1 == 1) ? 'profile-fill':'' }}">
	<a href="{{url('profile/step1')}}" class="nav-link">
	  <span class="circle"></span>
	  <span class="d-block">Complete Profile<br>Details</span>
	</a>
  </li>
  <li class="nav-item {{ ($user->customer->step_2 == 1) ? 'profile-fill':'' }}">
	<a href="{{url('profile/step2')}}" class="nav-link">
	  <span class="circle"></span>
	  <span class="d-block">Add Address<br>Details</span>
	</a>
  </li>
  <li class="nav-item {{ ($user->customer->step_3 == 1) ? 'profile-fill':'' }}">
	<a href="{{url('profile/step3')}}" class="nav-link">
	  <span class="circle"></span>
	  <span class="d-block">Update Personal<br>Preferences</span>
	</a>
  </li>
  <li class="nav-item {{ ($user->customer->step_4 == 1) ? 'profile-fill':'' }}">
	<a href="{{url('profile/step4')}}" class="nav-link">
	  <span class="circle"></span>
	  <span class="d-block">Update Contact<br>Preferences</span>
	</a>
  </li>
  <li class="nav-item">
	<a href="#" class="nav-link">
	  <span class="circle"></span>
	  <span class="d-block">Book Your<br>First Trip!</span>
	</a>
  </li>
</ul>
</div>
<div class="row mt-5">
<div class="col-sm-9">
  <div class="progress">
	<div class="progress-bar" role="progressbar" style="width: {{$percent}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
</div>
<div class="col-sm-3">
  <strong>Profile {{$percent}}% Complete</strong>
</div>
</div>