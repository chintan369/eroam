<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;

use App\Http\Requests;
use Carbon\Carbon;
use App\Libraries\ApiCache;
use Cache;
use Guzzle;
use Log;
use DateTime;
use App;
use Config;
use File;
use App\Http\Controllers\ExpediaApiController;

class CacheController extends Controller
{

	protected $expediaApi;
	public function __construct() {
		$this->expediaApi = new ExpediaApiController;
	}

	public function set_api_cache(){
		return $this->set( Input::except('provider'), Input::get('provider') );
	}
	

	public function set( $data, $provider ){
		
		$result = [];
		$method = 'post'; // DEFAULT
		$url = '';

		switch ( $provider ) {

			case 'hb':
				$url = 'hb/hotels';
				// CREATED BY ALJUN TO USE HB DESTINATION CODES FROM CACHE
				
				$key = $data;
				$key = array_filter($key);
				$key = implode(';', $key);
				Cache::forget($key);

				// CREATED BY MIGUEL TO GENERATE A CLEAN KEY
				break;

			case 'aot':
				$url = 'api_aot';
				$key    = json_encode($data);
				break;

			case 'viator':
				$url = 'service/search/products';
				$key = $data;
				if( isset($key['startDate']) ){ unset($key['startDate']); }
				if( isset($key['endDate']) ){ unset($key['endDate']); }
				$key = array_filter($key);
				$key = implode(';',$key);
				Cache::forget($key);
				break;

			case 'mystifly':
				$url = 'mystifly/flight/search';
				$data = array(
					'DepartureDate'           => trim( $data['DepartureDate'] ),
					'OriginLocationCode'      => strtoupper( trim( $data['OriginLocationCode'] ) ),
					'DestinationLocationCode' => strtoupper( trim( $data['DestinationLocationCode'] ) ),
					'CabinPreference'         => strtoupper( trim( $data['CabinPreference'] ) ),
					'Code'                    => explode(',', strtoupper( trim( implode(',', $data['Code'])))),
					'Quantity'                => explode(',', strtoupper( trim( implode(',', $data['Quantity'])))),
					'IsRefundable'            => ( trim(strtoupper( $data['IsRefundable'])) == 'TRUE') ? TRUE: FALSE,
					'IsResidentFare'          => ( trim(strtoupper( $data['IsResidentFare'])) == 'TRUE') ? TRUE: FALSE,
					'NearByAirports'          => ( trim(strtoupper( $data['NearByAirports'])) == 'TRUE') ? TRUE: FALSE
				);
				$key = $data;
				$key = json_encode($key);
				//Cache::forget($key);
				break;

			case 'ae':
				$url = 'arabian/explorer/hotelAvailabilitySearch';
				$key = $data;
				$key = array_filter($key);
				$key = implode(';',$key);
				break;	
				
			/* Added By Rekha Patel - 3rd Oct 2017 - set cache of Hotels list using Expedia API */
            case 'expedia' :
                $url =  'expedia/hotels';
                $key = $data;
                $key = array_filter($key);
                $key = implode(';',$key);
                //Cache::forget($key);
                break;
             case 'getTours' :
            	$url =  'getTours';
                $key = $data;
                $key = array_filter($key);
                $key = implode(';',$key);
                Cache::forget($key);
                break;
                
            case 'searchTour' :
            	$url =  'searchTour';
                $key = $data;
                $key = array_filter($key);
                $key = implode(';',$key);
                Cache::forget($key);
                break;

            case 'tourCountryCount' :
            	$url =  'tourCountryCount';
                $key = $data;
                $key = array_filter($key);
                $key = implode(';',$key);
                //Cache::forget($key);
                break;

            /* Added By chintan Patel - 29th Nov 2017 - set cache of transport list using Busbud API */
            case 'busbud' :
                $url =  'busbud/transportList';
                $key = $data;
                $key = array_filter($key);
                $key = implode(';',$key);
                Cache::forget($key);
                break;    

		}
		//check if data is already cache
		
		$result = Cache::get($key);
		if($provider == 'expedia'){
        	if (!Cache::has($key))
            {
	            $request = TRUE;
	            while( $request )
	            {
	                try{
	                    $response = $this->expediaApi->getHotels($data);
	                    //echo '<pre>'; print_r($response);
	                    $request = FALSE;
	                }catch(Exception $e){
	                    Log::error('An error has occured during a guzzle call on CacheController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
	                }
	            }

	            $result = json_decode( $response , true );
	            $expiresAt = Carbon::now()->addWeeks(24);
	            Cache::put($key, $result, $expiresAt);
	        }
        } else {
			if (!Cache::has($key))
			{
				$request = TRUE;
				while( $request )
				{
					try{
						$api_url = "https://cms.eroam.com/eroam/api/v2/";;
						// $api_url = env('API_URL');
						// if($url == 'getTours'){
						// 	$api_url = 'http://192.168.1.77/cmseroam/public/eroam/api/v2/';
						// }
						$client = new \GuzzleHttp\Client();
						$response = $client->request($method,$api_url . $url, [
						    'form_params' => $data
						]);	

						$request = FALSE;
					}catch(Exception $e){
						Log::error('An error has occured during a guzzle call on CacheController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
					}

				}
				
				$result = json_decode( $response->getBody() , true );
				$expiresAt = Carbon::now()->addWeeks(24);
				Cache::put($key, $result, $expiresAt);
			}
		}
		if ($url == 'service/search/products') {
			if ( session()->has( 'search_input' ) && count(session()->get( 'search_input' )['interests']) > 0 ){
				$this->viatorActivityFilter($result);
			}
			$temp_result = array();
			if(isset($result['data']) && !empty($result['data'])){
				$temp_result = array();
				foreach ($result['data'] as $keys => $values) {
					$hours = stripos($values['duration'],'hours');
					$minutes = stripos($values['duration'],'minutes');
					$day = stripos($values['duration'],'1 day');
					if($hours > 0 || $minutes > 0 || $day > 0){
						$temp_result[] = $values;
					}
				}
				if(!empty($temp_result)){
					$result['data'] = $temp_result;
				}
			}
		}

		return $result;
	}

	public function check($data, $provider){

		switch ($provider)
		{
			case 'hb':
				$key = $data;
				$key = array_filter($key);
				$key = implode(';', $key);
				break;

			case 'aot':
				$url = 'api_aot';
				$key = json_encode($data);
				break;

			case 'mystifly':
				$data = array(
					'DepartureDate'           => trim( $data['DepartureDate'] ),
					'OriginLocationCode'      => strtoupper( trim( $data['OriginLocationCode'] ) ),
					'DestinationLocationCode' => strtoupper( trim( $data['DestinationLocationCode'] ) ),
					'CabinPreference'         => strtoupper( trim( $data['CabinPreference'] ) ),
					'Code'                    => explode(',', strtoupper( trim( implode(',', $data['Code'])))),
					'Quantity'                => explode(',', strtoupper( trim( implode(',', $data['Quantity'])))),
					'IsRefundable'            => ( trim(strtoupper( $data['IsRefundable'])) == 'TRUE') ? TRUE: FALSE,
					'IsResidentFare'          => ( trim(strtoupper( $data['IsResidentFare'])) == 'TRUE') ? TRUE: FALSE,
					'NearByAirports'          => ( trim(strtoupper( $data['NearByAirports'])) == 'TRUE') ? TRUE: FALSE
				);
				$key = $data;
				$key = json_encode($key);
				break;	

			case 'ae':
				$key = $data;
				$key = array_filter($key);
				$key = implode(';',$key);
				break;	

			case 'expedia' :
                $key = $data;
                $key = array_filter($key);
                $key = implode(';',$key);
                break;
		}
		
		return Cache::has($key);
	}

	private function viatorActivityFilter(&$result){
		$headers = [
        	'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];
		$data_container ;
		$data_first = [];
		$data_last = [];
		if( count( $result['data'] ) > 0 ){
			$counter = 0;
			foreach( $result['data'] as $data ){

				$notInFilter = false;
				$data['labels'] = [];

				if( count($data['catIds']) > 0 ){
					$temp = [];
					/*
					| Get all label by category ids
					*/
					$labels = http( 'post', 'viator/category', ['category_ids' => $data['catIds']], $headers );
					$labels = to_one_dim_array_labels('label_id', $labels);

					if( count($labels) > 0 ){
						$temp = filter_by_labels($labels);
						$data['labels'] = array_unique($temp);
						$data_first[] = $data;
					}else{
						$data_last[] = $data;
						
					}
				}else{
					$data_last[] = $data;
				}
				$data_container = array_merge($data_first, $data_last);
			}
			$result['data'] = $data_container;
		}
	}

	//from vs to
	public function cacheAllCity(  ){
		$apiCache = new ApiCache;
		$cities = Cache::has('cities') ? Cache::get('cities') : $apiCache->get_all_cities();
		if( !empty( $cities ) ){
			foreach( $cities as $key => $origin_city ){
				//caching hotels
				$date_from = date("Y-m-d", strtotime(" +6 months"));
				$date = new DateTime($date_from);
				$date->modify('+1 day');
				$date_to = $date->format('Y-m-d');
				$city_id = $origin_city["id"];
				$params = [
					'data' => $origin_city,
					'date_from' => $date_from,
					'date_to' =>  $date_to,
					'code' => NULL,
				];
				$this->cacheHb( $params );
				
			}
		}
	}

	private function cacheHb( $params ){
		$adult_limit = 3;
		if( !empty( $params ) ){
			$data = $params['data'];
			$date_from = $params['date_from'];
			$date_to = $params['date_to'];
			$code = trim($params['code']);
			for( $count = 1; $count <= $adult_limit; $count++ ){
				$hb_rq_data = [
					'city_ids' => $data['id'],
					'check_in' => $date_from,
					'check_out' => $date_to,
					'room' => 1,
					'adult' => $count,
					'code' => $code,
					'child' => 0,
				];
				$response = $this->set( $hb_rq_data, 'hb' );
				echo "HOTELBEDS cache done for - " . $data['name'] . "<br/>";
			}
		}
	}

	private function cacheAot( $params ){
		$room_types = ['double', 'single', 'triple'];
		if( !empty( $params ) ){
			$data = $params['data'];
			$date_from = $params['date_from'];
			$date_to = $params['date_to'];		
			foreach( $room_types as $key=> $room_type ){
				$aot_rq_data = [
					'type' => 'q_supplier',
					'city_id' => $data['id'],
					'room_type' => $room_type,
				];
				$response = $this->set( $aot_rq_data, 'aot' );
				echo "AOT cache done for - " . $data['name'] . "<br/>";
				sleep( 1 );
			}
		}		
	}

	private function cacheViator( $params ){
		if( !empty( $params ) ){
			$data = $params['data'];
			$date_from = $params['date_from'];
			$date_to = $params['date_to'];		
			$viator_rq_data = [
				'destId' => $data['id'],
				'startDate' => $date_from,
				'endDate' => $date_to,
				'currencyCode' => 'AUD',
				'sortOrder' => 'TOP_SELLERS',
				'topX' => '1-15'
			];
			$response = $this->set( $viator_rq_data, 'viator' );
			echo "VIATOR cache done for - " . $data['name'] . "<br/>";
			sleep( 1 );
		}	
	}

	public function tempCacheMystifly(  ){
		$airport_codes_au_intl = 'ADL,BNE,CNS,DRW,MEL,OOL,PER,SYD,NAN,AKL,CHC,GBB,DAC,PBH,PNH,REP,HKG,MFM,PEK,NAY,SHA,TBS,BOM,CCU,DEL,GOI,DPS,JKT,THR,KIX,ITM,NRT,HND,ALA,LPQ,PKZ,VTE,KUL,ULN,ULN,RGN,KTM,CEB,MNL,SIN,GMP,BKK,CNX,HKT,ASB,TAS,HAN,SGN,EZE,BZE,LPB,GIG,GRU,YMX,YUL,YVR,YYZ,SCL,BOG,SJO,SYQ,HAV,UIO,GUA,CUN,MEX,BLB,LIM,BFI,SEA,DCA,IAD,DFW,IAH,JFK,LGA,LAX,MIA,ORD,SFO,VIE,BRU,SJJ,SOF,ZAG,PRG,CPH,LHR,HEL,CDG,FRA,MUC,SXF,ATH,BUD,DUB,FCO,MXP,VCE,AMS,OSL,KRK,WAW,LIS,DME,SVO,VKO,MOW,LED,EDI,BEG,BTS,BCN,MAD,MAD,IST,SAW,KBP,CAI,NBO,AGA,RAK,CPT,DUR,JNB,DAR,AUH,DOH,DXB';
		$airport_codes_asia_dom = 'BBM,KMT,KOS,PNH,REP,AMI,BDO,BJW,DPS,JKT,JOG,LBJ,SUB,FUK,HIJ,KIX,ITM,NGS,NRT,HND,OKD,UKY,YOK,LPQ,PKZ,VTE,BKI,BWN,IPH,KBR,KCH,KUL,LGK,MKZ,KUL,MYY,PEN,SDK,TGG,TOD,ULN,HEH,LIW,MAQ,RGN,MDL,MGZ,NYU,MDL,RGN,SNW,RGN,TVY,CEB,CRK,CYZ,LAO,MNL,MPH,PPS,TAG,SIN,CJU,GMP,PUS,TAE,WJU,YNY,BKK,CEI,CNX,HHQ,HKT,KBV,URT,USM,CXR,DAD,HAN,HUE,SGN,CKG,CTU,HKG,KWL,MFM,NNG,PEK,NAY,SHA,SZX,HKG,URC,WUH,XIY,YIH,BHR,BOM,BWA,CCU,CGP,COK,CXB,DAC,DEL,GAU,GOI,IXB,IXL,JAI,JDH,JSR,CCU,KTM,KUU,LXA,MAA,PBH,PKR,RJH,UDR,VNS,ZYL,ALA,GBB,GYD,BUS,KUT,TBS,MHD,THR,ULN,IKT,ASB,KRW,BHK,SKD,TAS';
		$airport_codes_americas_dom = 'ABQ,EZE,IGR,MDZ,SLA,BZE,LPB,SRJ,UYU,BZC,CGR,FLN,GIG,GRU,IGU,MAO,APT,YBA,YKA,YMX, YUL,YOW,YPR,YQB,YQR,YVR,YWG,YYC,YYZ,CJC,SCL,BOG,SJO, SYQ,HAV,UIO,FRS,GUA,LCF,RTB,SDH, RUY,XPL,CPE,CUN,CUU,CZM, CUN,JAL,MEX,MID,MTY,OAX,PBC,PCM,PQM,PUH,PXM,SZT,VER,VSA,GCK,MGA,BLB,AQP,CUZ,LIM,PEM,TYL,BFI, SEA,BLI,DCA, IAD,DFW,DTW,GCN,IAH,JFK, LGA,JNU,LAS,LAX,MCO, SFB,MIA,MSY,OKC,ORD,PDX,SAN,SFO,STL,TUS';
		$airport_codes_europe_dom = 'SZG,VIE,BRU,OST,SJJ,BOJ,SOF,DBV,SPU,ZAG,PRG,CPH,LHR,HEL,BOD,CDG,LIL,MCM,MRS,FRA,MUC,SXF,ATH,CFU,JMK,JTR,SKG,BUD,DUB,JRS,APF,FCO,FLO,MXP,MXP,RMI,RRO,VCE,RIX,VNO,AMS,EIN,RTM,BGO,OSL,KRK,WAW,FAO,LIS,OPO,DME, SVO, VKO, MOW,LED,ABZ,EDI,BEG,BTS,AGP,BCN,BIO,EAS,GRX,IBZ,MAD,SVQ,VLC,ARN,SOO,ZHI,ADB,ASR,AYT,BJV,CKZ,DLM,ISE,IST, SAW,KBP,CWL,AUH,DOH,DXB,AGA,RAK,CAI';
		$airport_codes_africa = 'AAN,BBK,MUB,ABS,ASW,CAI,HBE,HRG,LXR,SSH,MBA,NBO,DWA,LLW,AGA,CMN,ESU,FEZ,RAK,RBA,TNG,MPM,SWP,WDH,CPT,DUR,GRJ,HZV,JNB,MZF,PHW,PZB,ULD,AEI,ARK, JRO,DAR,JRO,MRE,QSI,ZNZ,AUH,DOH,DXB,FJR,JNJ,KHS,MCT,MSH,RKT,SHJ,SLL,SUH,EBB,KGL,LUN,LVI,NLA,BUQ,VFA';
		$airport_codes_intl = 'ADL,BNE,CNS,DRW,MEL,OOL,PER,SYD,NAN,AKL,CHC,GBB,DAC,PBH,PNH,REP,HKG,MFM,PEK, NAY,SHA,TBS,BOM,CCU,DEL,GOI,DPS,JKT,THR,KIX,ITM,NRT,HND,ALA,LPQ,PKZ,VTE,KUL,ULN,ULN,RGN,KTM,CEB,MNL,SIN,GMP,BKK,CNX,HKT,ASB,TAS,HAN,SGN,EZE,BZE,LPB,GIG,GRU,YMX, YUL,YVR,YYZ,SCL,BOG,SJO, SYQ,HAV,UIO,GUA,CUN,MEX,BLB,LIM,BFI, SEA,DCA, IAD,DFW,IAH,JFK, LGA,LAX,MIA,ORD,SFO,VIE,BRU,SJJ,SOF,ZAG,PRG,CPH,LHR,HEL,CDG,FRA,MUC,SXF,ATH,BUD,DUB,FCO,MXP,VCE,AMS,OSL,KRK,WAW,LIS,DME, SVO, VKO, MOW,LED,EDI,BEG,BTS,BCN,MAD,MAD,IST, SAW,KBP,CAI,NBO,AGA,RAK,CPT,DUR,JNB,DAR,AUH,DOH,DXB';
		
		$au_intl = explode( ",", $airport_codes_au_intl );
		$asia_dom = explode( ",", $airport_codes_asia_dom );
		$americas_dom = explode( ",", $airport_codes_americas_dom );
		$europe_dom = explode( ",", $airport_codes_europe_dom );
		$africa = explode( ",", $airport_codes_africa );
		$intl = explode( ",", $airport_codes_intl );
		$date_from = date("Y-m-d", strtotime(" +6 months"));

		$airport_codes = [
			$au_intl,
			$asia_dom,
			$americas_dom,
			$europe_dom,
			$africa,
			// $intl,
		];
		// dd( $airport_codes );
		foreach( $airport_codes as $key => $codes ){
			$this->tempCacheRegionMystifly( $codes, $date_from );
		}
	}

	private function tempCacheRegionMystifly( $codes, $date_from ){
		$adult_limit = 1;
		$counter = 0;
		if( !empty( $codes ) ){
			foreach( $codes as $key => $origin_code ){
				$origin_code = trim( $origin_code  );
				foreach( $codes as $key => $destination_code ){
					$destination_code = trim( $destination_code );
					if( $origin_code != $destination_code  ){
						for( $count = 1; $count <= $adult_limit; $count++ ){
							//if( $origin_code == "HKG" &&  $destination_code == "SHA"){
							$mystifly_rq_data = [
								'DepartureDate' => $date_from,
								'OriginLocationCode' => $origin_code,
								'DestinationLocationCode' => $destination_code,
								'CabinPreference' => 'Y',
								'Code' => [ 'ADT' ],
								'Quantity' => [ $count ],
								'IsRefundable' => 'FALSE',
								'IsResidentFare' => 'FALSE',
								'NearByAirports' => 'FALSE',
							];
							$response = $this->set( $mystifly_rq_data, 'mystifly' );
							//}
							// $counter++;
							 echo "MYSTILFY cache done for {$count} TRAVELLER - " . $origin_code . " => " . $destination_code . "<br/>";
						}
					}
				}
			}
		}
		// echo "------------" . $counter . "<br/>";
	}

	public function checkProductCache(  ){
		$apiCache = new ApiCache;
		$cities = Cache::has('cities') ? Cache::get('cities') : $apiCache->get_all_cities();
		$provider =  Input::get('provider') ? Input::get('provider') : "hb";
		$clear =  Input::get('clear') ? Input::get('clear') : 0;
		$verbose = Input::get('verbose') ? Input::get('verbose') : 0;
		$clearEmpty = Input::get('clearempty') ? Input::get('clearempty') : 0;
		$adult_limit = 3;
		$counter = 0;
		if( !empty( $cities ) ){
			foreach( $cities as $key => $origin_city ){
				//caching hotels
				$date_from = date("Y-m-d", strtotime(" +6 months"));
				$date = new DateTime($date_from);
				$date->modify('+1 day');
				$date_to = $date->format('Y-m-d');
				$params = [
					'data' => $origin_city,
					'date_from' => $date_from,
					'date_to' =>  $date_to,
				];

				if( !empty( $params ) ){
					$data = $params['data'];
					$date_from = $params['date_from'];
					$date_to = $params['date_to'];
					for( $count = 1; $count <= $adult_limit; $count++ ){
						switch( $provider ){
							case 'hb' :
								$cacheKey = "hb_desc_codes_match";
								$hb_destination_codes = Cache::has( $cacheKey ) ? Cache::get( $cacheKey ) : [];
								if( empty( $hb_destination_codes ) ){
									die( "no hb destination codes, run cache function runCacheHbDestinationCodes : /cache_run_hb_dest" );
								}
								$hotel_city_id = $data['id'];
								$data['code'] = $data['airport_codes'];
								$data['code'] = !empty($hb_destination_codes[$hotel_city_id]) ? $hb_destination_codes[$hotel_city_id] : $data['code'];
								$data['code'] = empty( $data['code'] ) ? "XXX" : $data['code'];
								$rq_data = [
									'city_ids' => $data['id'],
									'check_in' => $date_from,
									'check_out' => $date_to,
									'room' => 1,
									'adult' => $count,
									'code' => $data['code'],
									'child' => 0,
								];

								break;
							case 'ae' :
								$rq_data = [
									'FromDate' => $date_from,
									'ToDate' => $date_to,
									'Adults' => $count,
									'city_name' => $origin_city["name"],
								];
								break;
						}
						$response = $this->checkCacheExists( $rq_data, $provider );
						if( $response ){
							if( $verbose ){
								echo "have cache for  " . $origin_city["name"] . " at num of adult : " . $count . "<br/>";
							}
							if( $clear == 1 ){
								$this->forgetThisCache( $rq_data, $provider );
							}

							if( $clearEmpty == 1 ){
									$this->forgetThisEmptyCache( $rq_data, $provider );
							}
						}else{
							if( $verbose ){
								echo "NO CACHING ANYMORE!!!! ---- " . $origin_city["name"] . "ONLY TOTAL OF : " . $counter . "<br/>";
							}

						}
					}
				}
			}
		}
	}

	private function checkCacheExists( $data, $provider ){
			switch ( $provider ) {

				case 'hb':
					$url = 'hb/hotels';
					
					$key = $data;
					unset($key['check_in']);
					unset($key['check_out']);
					$key=array_filter($key);
					$key = implode(';', $key);
					break;
				case 'aot':
					$url = 'api_aot';
					$key    = json_encode($data);
					break;

				case 'viator':
					$url = 'service/search/products';
					$key = $data;
					unset($key['startDate']);
					unset($key['endDate']);
					$key=array_filter($key);
					$key = implode(';',$key);
					break;
				case 'mystifly':
					$url = 'mystifly/flight/search';
					$data = array(
						'DepartureDate'           => trim( $data['DepartureDate'] ),
						'OriginLocationCode'      => strtoupper( trim( $data['OriginLocationCode'] ) ),
						'DestinationLocationCode' => strtoupper( trim( $data['DestinationLocationCode'] ) ),
						'CabinPreference'         => strtoupper( trim( $data['CabinPreference'] ) ),
						'Code'                    => explode(',', strtoupper( trim( implode(',', $data['Code'])))),
						'Quantity'                => explode(',', strtoupper( trim( implode(',', $data['Quantity'])))),
						'IsRefundable'            => ( trim(strtoupper( $data['IsRefundable'])) == 'TRUE') ? TRUE: FALSE,
						'IsResidentFare'          => ( trim(strtoupper( $data['IsResidentFare'])) == 'TRUE') ? TRUE: FALSE,
						'NearByAirports'          => ( trim(strtoupper( $data['NearByAirports'])) == 'TRUE') ? TRUE: FALSE		
					);
					$key = $data;
					unset($key['DepartureDate']);
					unset($key['IsRefundable']);
					unset($key['IsResidentFare']);
					unset($key['NearByAirports']);
					$key = json_encode($key);
					break;

				case 'ae':
					$url = 'arabian/explorer/hotelAvailabilitySearch';
					$key = $data;
					unset($key['FromDate']);
					unset($key['ToDate']);
					$key=array_filter($key);
					$key = implode(';',$key);
				break;
			}

			//check if data is already cache
			return Cache::has($key) ? Cache::get($key) : [];
	}

	public function forgetThisCache( $data, $provider ){
			switch ( $provider ) {

				case 'hb':
					$url = 'hb/hotels';
					
					$key = $data;
					unset($key['check_in']);
					unset($key['check_out']);
					$key=array_filter($key);
					$key = implode(';', $key);
					break;
				case 'aot':
					$url = 'api_aot';
					$key    = json_encode($data);
					break;

				case 'viator':
					$url = 'service/search/products';
					$key = $data;
					unset($key['startDate']);
					unset($key['endDate']);
					$key=array_filter($key);
					$key = implode(';',$key);
					break;
				case 'mystifly':
					$url = 'mystifly/flight/search';
					$data = array(
						'DepartureDate'           => trim( $data['DepartureDate'] ),
						'OriginLocationCode'      => strtoupper( trim( $data['OriginLocationCode'] ) ),
						'DestinationLocationCode' => strtoupper( trim( $data['DestinationLocationCode'] ) ),
						'CabinPreference'         => strtoupper( trim( $data['CabinPreference'] ) ),
						'Code'                    => explode(',', strtoupper( trim( implode(',', $data['Code'])))),
						'Quantity'                => explode(',', strtoupper( trim( implode(',', $data['Quantity'])))),
						'IsRefundable'            => ( trim(strtoupper( $data['IsRefundable'])) == 'TRUE') ? TRUE: FALSE,
						'IsResidentFare'          => ( trim(strtoupper( $data['IsResidentFare'])) == 'TRUE') ? TRUE: FALSE,
						'NearByAirports'          => ( trim(strtoupper( $data['NearByAirports'])) == 'TRUE') ? TRUE: FALSE		
					);
					$key = $data;
					unset($key['DepartureDate']);
					unset($key['IsRefundable']);
					unset($key['IsResidentFare']);
					unset($key['NearByAirports']);
					$key = json_encode($key);
					break;

				case 'ae':
					$url = 'arabian/explorer/hotelAvailabilitySearch';
					$key = $data;
					unset($key['FromDate']);
					unset($key['ToDate']);
					$key=array_filter($key);
					$key = implode(';',$key);
				break;
			}
			echo $key . "<br/>";
			Cache::forget($key);
	}

	public function forgetThisEmptyCache( $data, $provider ){
			switch ( $provider ) {

				case 'hb':					
					$key = $data;
					unset($key['check_in']);
					unset($key['check_out']);
					$key=array_filter($key);
					$key = implode(';', $key);

					$cacheContent = Cache::get($key);
					if( empty($cacheContent["data"]["hotels"]["hotels"])  ){
						echo $key . " ====== <br/>";
						Cache::forget($key);
					}


					break;
				case 'aot':
					break;

				case 'viator':
					$url = 'service/search/products';
					$key = $data;
					unset($key['startDate']);
					unset($key['endDate']);
					$key=array_filter($key);
					$key = implode(';',$key);
					break;
				case 'mystifly':
					$url = 'mystifly/flight/search';
					$data = array(
						'DepartureDate'           => trim( $data['DepartureDate'] ),
						'OriginLocationCode'      => strtoupper( trim( $data['OriginLocationCode'] ) ),
						'DestinationLocationCode' => strtoupper( trim( $data['DestinationLocationCode'] ) ),
						'CabinPreference'         => strtoupper( trim( $data['CabinPreference'] ) ),
						'Code'                    => explode(',', strtoupper( trim( implode(',', $data['Code'])))),
						'Quantity'                => explode(',', strtoupper( trim( implode(',', $data['Quantity'])))),
						'IsRefundable'            => ( trim(strtoupper( $data['IsRefundable'])) == 'TRUE') ? TRUE: FALSE,
						'IsResidentFare'          => ( trim(strtoupper( $data['IsResidentFare'])) == 'TRUE') ? TRUE: FALSE,
						'NearByAirports'          => ( trim(strtoupper( $data['NearByAirports'])) == 'TRUE') ? TRUE: FALSE		
					);
					$key = $data;
					unset($key['DepartureDate']);
					unset($key['IsRefundable']);
					unset($key['IsResidentFare']);
					unset($key['NearByAirports']);
					$key = json_encode($key);
					break;

				case 'ae':
					$url = 'arabian/explorer/hotelAvailabilitySearch';
					$key = $data;
					unset($key['FromDate']);
					unset($key['ToDate']);
					$key=array_filter($key);
					$key = implode(';',$key);
				break;
			}


	}
	
	public function cacheHbLocationCodes(  ){
		$url = "hb/getlocationcodes";
		$input = Input::all(  );
		$location_values = array( 'destinations', 'countries' );
		$location = empty( $input['location'] ) ? "destinations" : $input['location'];
		if( !in_array( $location, $location_values ) ){
			echo "enk!"; exit;
		} 
		$data = [
			'location' => $location,
		];
		$cache_key = "hotelbeds_locationcodes_" . $location;
		if( Cache::has($cache_key)  ){
			$result = Cache::get($cache_key);
			return $this->testCompare( $result );
			return $result;
		}else{
			// $api_url = Config::get('env.API_URL');
			$api_url = "https://cms.eroam.com/eroam/api/v2/";
			$api_call_url = $api_url . $url . '?' . http_build_query( $data ) ;
			$response = Guzzle::get( $api_call_url )->getBody();
			$result = json_decode( $response , true );
			$expiresAt = Carbon::now()->addWeeks(24);
			Cache::put($cache_key, $result, $expiresAt);
		}

	}

	public function testCompare( $destinations ){
		return $this->forManualCompare( $destinations );
		$apiCache = new ApiCache;
		$cities = Cache::has('cities') ? Cache::get('cities') : $apiCache->get_all_cities();
		$destination_names = array_column( $destinations, 'name' );
		$dest_names_contents = array_column($destination_names,"content");
		$dest_zone = array_column($destinations, "zones");
		foreach( $cities as $key => $city ){
			$city_name = $city["name"];
			foreach( $dest_names_contents as $dest_key => $dest_name ){
				$city_name_comp =  strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $city_name));
				$dest_name_comp = strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $dest_name));
				$str_a = (strlen( $city_name_comp ) >= strlen( $dest_name_comp ) ) ? $dest_name_comp : $city_name_comp;
				$str_b = (strlen( $city_name_comp ) >= strlen( $dest_name_comp ) ) ? $city_name_comp : $dest_name_comp;
				
				similar_text( $str_a, $str_b, $similarity  );
				if( $city_name == 'Melbourne' && ( $similarity  ) >= 50 ){
					echo $str_a . " : " . $str_b ." : " . $similarity . "% <br/>";
				}
				
				if( ( $similarity  ) >= 60 ){
				}
			}
			echo "============== <br/>"; 
		}
	}

	public function forManualCompare( $destinations ){
		$cities = Cache::has('cities') ? Cache::get('cities') : $apiCache->get_all_cities();
		$destination_names = array_column( $destinations, 'name' );
		$dest_names_contents = array_column($destination_names,"content");
		$dest_zone = array_column($destinations, "zones");
		$city_names = array_column( $cities, 'name' );
		foreach( $cities as $key =>  $city){
			echo $city["id"] . "," . $city["name"] . ",". $city["country_name"]."<br/>";
		}
		exit;
		$counter = 0;
		foreach( $destinations as $dkey => $destination ){
			if( !empty( $destination["name"] ) && !empty( $destination["code"] ) ){
				echo $destination["name"]["content"] . "," . $destination["code"] .",". $destination["countryCode"] ."<br/>";
			}

		}
		exit;
	}
 public function compareCityName($str_a, $str_b){
    $length = strlen($str_a);
    $length_b = strlen($str_b);
 
    $i = 0;
    $segmentcount = 0;
    $segmentsinfo = array();
    $segment = '';
    while ($i < $length)
    {
        $char = substr($str_a, $i, 1);
        if (strpos($str_b, $char) !== FALSE)
        {
            $segment = $segment.$char;
            if (strpos($str_b, $segment) !== FALSE)
            {
                $segmentpos_a = $i - strlen($segment) + 1;
                $segmentpos_b = strpos($str_b, $segment);
                $positiondiff = abs($segmentpos_a - $segmentpos_b);
                $posfactor = ($length - $positiondiff) / $length_b; // <-- ?
                $lengthfactor = strlen($segment)/$length;
                $segmentsinfo[$segmentcount] = array( 'segment' => $segment, 'score' => ($posfactor * $lengthfactor));
            }
            else
            {
                 $segment = '';
                 $i--;
                 $segmentcount++;
             }
         }
         else
         {
             $segment = '';
            $segmentcount++;
         }
         $i++;
     }
 
     // PHP 5.3 lambda in array_map
     $totalscore = array_sum(array_map(function($v) { return $v['score'];  }, $segmentsinfo));
     return $totalscore;
}

	public function runCacheHbDestinationCodes(  ){
		$hb_locations_path = base_path().'/public/hb_locations_run.csv';
		$hb_contents = File::get($hb_locations_path);
		if( empty( $hb_contents ) ){
			echo "empty file"; exit;
		}
		$this->forgetCacheHbDestinationCodes(  );
		$content_explode = explode( "\r\n", $hb_contents );
		$cacheKey = "hb_desc_codes_match";
		$matched_codes_array = [];
		foreach( $content_explode as $key => $content ){
			$row_explode = explode( ",", $content );
			if( !empty( $row_explode[3] ) ){ //if assigned to city id
				$matched_codes_array[$row_explode[3]] = $row_explode[1];
			}
		}
		Cache::forever($cacheKey, $matched_codes_array);
		return $matched_codes_array;
	}

	public function forgetCacheHbDestinationCodes(  ){
		$cacheKey = "hb_desc_codes_match";
		if( Cache::has( $cacheKey ) ){
			Cache::forget( $cacheKey );
		}
	}

	public function cache_apis() {
		$apis = file_get_contents(env('CMS_URL').'apis');
		$apiCache = new ApiCache;
		$apiCache->save('apis', $apis);
	}

}
