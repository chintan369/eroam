@extends( 'layouts.home' )

@if(session()->has('register_confirm'))
<p class="success-box m-t-30">
	{{ session()->get('register_confirm') }}
</p>
@endif

@section('content')
<div class="itinerary_block">
	<div class="container-fluid">
		<div class="row mt-4 tour-details">
			<div class="col-xl-10 offset-xl-1">
				<div class="card panel p-3 mb-3">
					<div class="row">
						<div class="col-sm-8 col-xl-9">
							<h5 class="font-weight-bold">{{$data['tour_name']}}</h5>
							<div class="media">
                          		<img src="{{ url('images/tour.svg')}}" alt="">
                          		<div class="media-body pb-3 mb-0">
                          			{{date('d F Y', strtotime($data['start_date']))}} - 
                  					{{date('d F Y', strtotime($data['finishDate']))}}
                  				</div>
                        	</div>
                      	</div>
                      	<div class="col-sm-4 col-xl-3 text-left text-sm-right">
                        	<h2 class="font-weight-bold mb-0">{{$data['days']}}</h2>
                        	Days
                        </div>
                    </div>
                </div>
                <div class="card panel p-3 mb-3">
                	<h5>Returning Customers</h4>
                	<hr class="mt-2">
                    <div class="row mt-3">
                      	<div class="col-sm-8 col-8">
                        	Sign in with your social network
                      	</div>
                      	<!-- <div class="col-sm-4 col-4 text-right">
                        	<a href="#" class="text-dark"><i class="fa fa-lock"></i> Secure</a>
                      	</div> -->
                    </div>
                    <div class="row mt-4">
                      	<div class="col-sm-6 col-6">
                      		<a href="{{ url('redirect/google') }}" class="google-login btn-block text-uppercase"><i class="fab fa-google-plus-g float-left"></i> <center>GOOGLE</center></a>
                      	</div>
                      	<div class="col-sm-6 col-6">
                        	<button type="button" data-toggle="tooltip" title="Not Available in Pilot" class="fb-login text-uppercase btn-block"><i class="fab fa-facebook-f float-left"></i> <span>FACEBOOK</span></button>
                      	</div>
                    </div>
                    <div class="login-or mt-5">
                      	<span>or</span>
                    </div>
                    <form class="form-horizontal" id="login_form2">
					{{ csrf_field() }}
					<p class="alert alert-danger" id="login-error-message2" style="display:none"></p>
	                    <div class="form-group mt-3">
	                      	<div class="fildes_outer">
	                        	<label>Email Address</label>
	                        	<input type="text"  name="username_login" id="username_login2"  class="form-control" placeholder="kane@eroam.com"> 
	                      	</div>
	                    </div>
	                    <div class="form-group">
	                      	<div class="fildes_outer">
	                        	<label>Password</label>
	                        	<input type="password" style="display:none">
	                        	<input type="password"  name="password_login" id="password_login2"  class="form-control" placeholder="...."> 
	                      	</div>
	                    </div>
	                    <div class="form-group black-checkbox">
	                      	<span class="custom_check">Keep me signed in. <input type="checkbox"><span class="check_indicator">&nbsp;</span></span>
	                    </div>
	                    <div class="form-group">
	                    	<input type="hidden" id="type" value="tour">
	                      	<input type="submit" name="" class="btn btn-white btn-block" value="CHECKOUT">
	                    </div>
	                    <div class="text-right">
	                      	<a href="javascript:void()" id="forgot_password_button" class="text-primary">Recover my password</a>
	                    </div>
	                </form>
                </div>
                <div class="card panel p-3 mb-3">
                    <h5>Guest Checkout</h5>
                    <hr class="mt-2">
                    <div class="mt-3">
                      	<p>Proceed to checkout, and you can create an eRoam account at the end.</p>
                      	<a href="{{ url('book/tour') }}" class="btn btn-white btn-block mt-4">CONTINUE AS GUEST</a>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{ url('js/checkout.js') }}"></script>
@stop
