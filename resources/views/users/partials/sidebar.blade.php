<?php $segment1	=	 Request::segment(1);
 	  $segment2	=	 Request::segment(2);
  ?>
<div class="account-left">
  <h5 class="mt-2">My Account</h5>
  <ul class="list-unstyled">
	<li class="{{($segment2=='step1') ? 'active':''}}"><a href="{{url('profile/step1')}}" class="d-flex align-items-center"><i class="ic-account_circle mr-3"></i>Profile</a></li>
	<li class="{{($segment2=='step2') ? 'active':''}}"><a href="{{url('profile/step2')}}" class="d-flex align-items-center"><i class="ic-person_pin_circle mr-3"></i>Addresses</a></li>
	<li class="{{($segment2=='step3') ? 'active':''}}"><a href="{{url('profile/step3')}}" class="d-flex align-items-center"><i class="ic-fingerprint mr-3"></i>Personal Preferences</a></li>
	<li class="{{($segment2=='step4') ? 'active':''}}"><a href="{{url('profile/step4')}}" class="d-flex align-items-center"><i class="ic-markunread mr-3"></i>Contact Preferences</a></li>
	<li class="{{($segment2=='trips') ? 'active':''}}"><a href="{{url('manage/trips')}}" class="d-flex align-items-center"><i class="ic-event_note mr-3"></i>Manage Trips</a></li>
	<li class="{{($segment2=='step3#') ? 'active':''}}"><a href="{{url('profile/step3#')}}" class="d-flex align-items-center"><i class="ic-favorite_border mr-3"></i>Saved Trips</a></li>
  </ul>
</div>