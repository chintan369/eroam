<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="container-fluid">
        <div class="outerpage_scroll accommodation_top pt-4 pb-3 ">
            <div class="pl-3">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                        <ul>
                            <li class="d-inline pr-2 pl-4"><i class="ic-place"></i> <strong> {{ $city->name }}, {{ $city->country_name }}<span class="act-count"></span></strong> </li>
                        </ul>
                        <p class="pt-3"><i class="ic-calendar"></i> {{ convert_date($date_from, 'new_eroam_format') }} - {{ convert_date($date_to, 'new_eroam_format') }}</p>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                            <div class="fildes_outer pt-0">
                                <input type="text" id="search_val" class="form-control pt-0" aria-describedby="emailHelp" placeholder="Search">
                                <span class="arrow_down" onclick="getActivitySearch();"><i class="fa fa-search"></i> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-4">
                        <div class="transport_filter">
                            <ul class="nav nav-pills nav-fill activities_nav">
                                <li class="nav-item">
                                    <a data-toggle="tooltip" title="@lang('home.pilot_text')" class="nav-link disabled" href="#">@lang('home.activity_label1')  <i class="fa fa-caret-down" aria-hidden="true"></i> </a>
                                </li>

                                <li role="presentation" class="nav-item"> <a href="#" class="nav-link" id="ratingAsc">@lang('home.activity_label2')<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                </li>

                                <li role="presentation" class="nav-item"> <a href="#" class="nav-link" id="priceSortAsc">@lang('home.activity_label3')<i class="fa fa-caret-down" aria-hidden="true"></i> </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#" data-toggle="tooltip" title="@lang('home.pilot_text')">@lang('home.activity_label4')<i class="fa fa-caret-up" aria-hidden="true"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="itinerary_page pt-2 tabs-content-container">
            <div class="tab-content activity-wrapper" id="myTabContent">
                <div class="in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab">
                    <div class="container-fluid activityScroll tabs-content-wrapper" id="activities-container">
                        <!-- Calendar Data Start -->
                        @php 
						$i=0; 
						$actArr = array(); 
						foreach ($selected_dates as $name => $date) { 
							$sdate = explode(' ', $date);
							$sdate = date('Y',strtotime($sdate[2])).'-'.date('m',strtotime($sdate[1])).'-'.str_replace(array('th','rd','nd','st',','), '', $sdate[0]); 
							$sdate2 = date('j M Y',strtotime($sdate)); 
							$sdate = date('Y-m-d',strtotime($sdate)); 
							//$actArr[$sdate] = session()->get('search')['itinerary'][$leg]['activities'][$i]['name']; 
							$i++; 
						} 
						@endphp

                        <div id="activity-loader" class="loader_container">
                            <div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.loading_text')</div>
                        </div>
                        <div id="auto-sort-loader" class="loader_container" style="display:none;">
                            <div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.sorting_text')</div>
                        </div>
                        <div class="itinerary_page activity-list-selected"></div>
                        <div class="itinerary_page activities activity-list" id="activity-list"></div>
                    </div>

                    <!-- Activity Details Start -->
                    <div id="view-more-details"></div>

                    <div id="view-more-loader" class="loader_container" style="display:none;">
                        <div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.loading_data_text')</div>
                    </div>
                    <!-- Activity Details End -->
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="selected-activities" value="{{$selected_activities}}" />
<input type="hidden" class="the-city-id" value="{{ json_encode( get_city_by_id( $city_id ) ) }}">