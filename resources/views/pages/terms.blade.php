@extends('layouts.static') @section('custom-css')
<style>
    .wrapper {
       background-image: url( {{ url( 'images/bg1.jpg' ) }} ) !important;
    }

    .top-margin {
        margin-top: 5px;
    }

    .container-padding {
        padding-left: 35px;
        padding-right: 35px;
    }

    .top-bottom-paddings {
        padding-top: 20px;
        padding-bottom: 20px;
    }

    .white-box {
        background: rgba(255, 255, 255, 0.9);
        border: solid thin #9c9b9b !important;
    }

    .wrapper {
        background-attachment: fixed;
    }

    #terms p {
        line-height: 1.3em;
        text-align: justify;
    }

    .padding-20 {
        padding-left: 20px;
        padding-right: 20px;
    }

</style>
@stop @section('content')




<div class="top-section-image">
    <img src="{{asset('images/bg-image.jpg')}}" alt="" class="img-responsive">
</div>

<section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="border-bottom">Terms &amp; Conditions</h2>
                    <div class="mt-4">
                        <p>eRoam Pty Ltd ACN 609 719 486 VIC32783 is travel technology company and, in the provision of holidays or travel products acts solely as an agent on behalf of the Operator Services. eRoam Pty Ltd is not liable to you for any act, default or neglect of any kind by any Operational Services.</p>
                        <p>Operator Services are all travel, accommodation and other associated services provided to you by a Third Party tour, travel, accommodation or other operator, booked on your behalf via the eRoam.com interface.</p>
                        <p><strong>Disclaimer:</strong> Due to conditions outside of our control such as natural disaster, weather, politics or operational problems (e.g. Flooding, road and park closures), eRoam Pty Ltd and its suppliers have the right to cancel a sector / sectors and or alter the itinerary. In these circumstances, it is not a guarantee that missed or cancelled sectors will be fully refunded. This will be at the suppliers discretion and eRoam Pty Ltd will charge an administration fee. In circumstances where the cancellation / amendment is due to external events outside our reasonable control, refunds will be less any unrecoverable costs and applicable administration fees. We are not responsible for any incidental expenses that may be incurred as a result of amending / cancelling the booking including but not limited to Travel Insurance excess, non-refundable flights or alternative travel arrangements i.e.: connecting travel services, additional accommodation, tours or upgrading. This will be at the clients expense. eRoam Pty Ltd will endeavour to organise new arrangements for travel during any of these circumstances. ( Amendment fees will apply as per our usual terms and conditions) However, it will be ultimately be the clients own responsibility. eRoam Pty Ltd cannot be held responsible for any forward travel arrangements that are affected by any of the above. Change or cancellation of forward travel booked through eRoam Pty Ltd will be treated under our normal terms and conditions.</p>
                        <p>For example you could create itineraries along the East Coast of Australia (Sydney to Cairns), New Zealand (Auckland to Christchurch), Thailand (Bangkok to Koh Samui), Indochina (Bangkok to Saigon). We also have a full range of tours and packages for travelers that are after more private touring with the additional creature comforts.</p>
                        <p>In the unlikely hood that eRoam Pty Ltd or its suppliers, are forced to cancel or alter a service due to non-availability, eRoam Pty Ltd or its suppliers, will offer an alternative service of similar inclusions. Any extra costs incurred from the alternative arrangements, such as additional meals etc will be at the clients expense.</p>
                        <p>Prices are subject to change.<br/> Refunds: Refunds are made in AUD only and eRoam Pty Ltd cannot be held responsible for loss due to bank fees or fluctuations in currency exchange rates. Refunds may take up to 30 days to process.</p>
                        <p><strong>Cancellation Fees:</strong> In the event of cancellation the following cancellation fees will be payable by the client:</p>
                        <ul class="simple-listing">
                            <li>Outside 55 days - Deposit only</li>
                            <li>42 to 55 days prior - 35% of tour cost plus full Greyhound Pass cost if applicable</li>
                            <li>28 to 41 days prior - 50% of tour cost plus full Greyhound Pass cost if applicable</li>
                            <li>14 to 27 days prior - 75% of tour cost plus full Greyhound Pass cost if applicable</li>
                            <li>Less than 14 days prior - 100% of tour cost, if the deposit is greater than the cancellation fee then the deposit will be forfeited. Amendment Fees: If a date change or itinerary change is done at a Clients request, a fee from $50 to $200 per passenger per change will be charged on top of any applicable supplier fees.</li>
                            <li>If these changes fall within the cancellation period, then any applicable cancellation fees will also apply</li>
                        </ul>
                        <p><strong>Travel Insurance:</strong> We strongly recommend clients to purchase travel insurance at the same time as booking travel arrangements to suitably cover medical, personal effects, travel delays and cancellation costs.</p>
                        <p>All cancellations or disputes should be sent by registered post to:</p>
                        <p>
                            <em>Operations</em><br/>
                            <em>eRoam Pty Ltd </em><br/>
                            <em>9/80 Balcombe Rd </em><br/>
                            <em>Mentone 3194 </em><br/>
                            <em>AUSTRALIA </em><br/> Our company name is eRoam Pty Ltd - Level 3, IBM Centre, 60 City Road, Southbank VIC 3006
                        </p>
                        <p><strong>Definitions:</strong></p>
                        <ul class="simple-listing">
                            <li><strong>Supplier -</strong> A supplier is the third party tour operator or service provider that operates the travel arrangements or services for the Client.</li>
                            <li><strong>Client -</strong> The client is the person that has made the booking with eRoam Pty Ltd and is responsible for adhering to the booking conditions and acts as the sole correspondent for the client and the client's party.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>


@stop @section( 'custom-js' ) @stop
