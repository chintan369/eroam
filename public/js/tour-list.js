var countryName = countrName;
var default_currency = default_currency;
var start_page = 0; //initial page
var end_record_text = 'No more tours found!'; //no more records to load
var loading = false;
var end_record = false;
var totalTourCount = parseInt(0);
var tourTitle = $("#tourSearch").val();

function removeLastComma(strng) {
    strng = strng.trim();
    if (strng[strng.length - 1] == ',') {
        var strng = strng.substring(0, strng.length - 1)
    }
    return strng;
}

$(document).on('click', '#priceSortAsc', function() { //alert(1);
    sortMeBy('data-price', 'div.new-list-wrapper', 'div.list-box', 'asc');
    sortMeBy('data-price', 'div.grid-tours', 'div.gridBox', 'asc');
    $('#priceSortAsc span').removeClass('fa-caret-down').addClass('fa-caret-up');
    $('#priceSortAsc').attr('id', 'priceSortDesc');
});

$(document).on('click', '#priceSortDesc', function() {
    $('#priceSortDesc span').removeClass('fa-caret-up').addClass('fa-caret-down');
    sortMeBy('data-price', 'div.grid-tours', 'div.gridBox', 'desc');
    sortMeBy('data-price', 'div.new-list-wrapper', 'div.list-box', 'desc');
    $('#priceSortDesc').attr('id', 'priceSortAsc');
});

function getTourSearch() {
    var search_val = $('#tourSearch').val();
    if (search_val) {
        search_val = search_val.toLowerCase();
        var list = $("div.new-list-wrapper div.list-box");
        $(list).fadeOut("fast");
        var totalCount = 0;

        $("div.new-list-wrapper").find('div.list-box[data-tourname*="' + search_val + '"]').each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        var list = $("div.grid-tours div.gridBox");
        $(list).fadeOut("fast");
        var totalCount = 0;
        $("div.grid-tours").find('div.gridBox[data-tourname*="' + search_val + '"]').each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        checkPropertyCountForSearch(totalCount);
    } else {
        var list = $("div.new-list-wrapper div.list-box");
        $(list).fadeOut("fast");
        var totalCount = 0;
        search_val = 'eRoam';
        $("div.new-list-wrapper").find('div.list-box[data-provider*="' + search_val + '"]').each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        var list = $("div.grid-tours div.gridBox");
        $(list).fadeOut("fast");
        var totalCount = 0;
        $("div.grid-tours").find('div.gridBox[data-provider*="' + search_val + '"]').each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        checkPropertyCountForSearch(totalCount);
        $('#loadMore').show();
    }
}

$('#tourSearch').keyup(function() {
    if ($(this).val() == '') {
        var list = $("div.new-list-wrapper div.list-box");
        $(list).fadeOut("fast");
        var totalCount = 0;
        search_val = 'eRoam';
        $("div.new-list-wrapper").find('div.list-box[data-provider*="' + search_val + '"]').each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        var list = $("div.grid-tours div.gridBox");
        $(list).fadeOut("fast");
        var totalCount = 0;
        $("div.grid-tours").find('div.gridBox[data-provider*="' + search_val + '"]').each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        checkPropertyCountForSearch(totalCount);
        $('#loadMore').show();
    }
});

function checkPropertyCountForSearch(totalPropertyCount) {

    $('div.list-wrapper').find('.noHotelFound').closest('h4').remove();
    $('.tour-count').html('Total ' + totalPropertyCount + ((totalPropertyCount == 1 || totalPropertyCount == 0) ? ' Tour' : ' Tours') + ' Found');
    if (totalPropertyCount == 0) {
        var html = '<h4 class="text-center blue-txt bold-txt noHotelFound">No Tours Available.</h4>';
        $('div.list-wrapper').append(html);
    }
    //next();
}
$(document).on('click', '#searchCategory', function() {
    //$("#transport-tab").children().removeClass("active");
    var provider = $(this).text(); //alert(provider);
    filterListOperator(provider);
    $("#category-tab").children().removeClass("active");
    $(this).parent().addClass("active");
});

function filterListOperator(value) {
    var list = $("div.grid-tours div.gridBox");
    var list1 = $("div.new-list-wrapper div.list-box");
    $(list).fadeOut("fast");
    $(list1).fadeOut("fast");
    var totalCount = 0;
    if (value == "All") {
        $("div.grid-tours").find("div.gridBox").each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
        $("div.new-list-wrapper").find("div.list-box").each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
    } else { //alert(1);
        //Notice this *=" <- This means that if the data-category contains multiple options, it will find them
        //Ex: data-category="Cat1, Cat2"
        $("div.grid-tours").find("div.gridBox[data-all-category*='" + value + "']").each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });

        $("div.new-list-wrapper").find("div.list-box[data-all-category*='" + value + "']").each(function(i) {
            totalCount++;
            $(this).delay(200).slideDown("fast");
        });
    }
    checkTransportCountForSearch(totalCount);
}

function checkTransportCountForSearch(totalTransportCount) {
    //$('.trans-count').html( ': Top ' + totalTransportCount + ( (totalTransportCount == 1 || totalTransportCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
    if (totalTransportCount == 0) {
        var html = '<h4 class="text-center">No Transport Found.</h4>';
        $('#listTourList').append(html);
    }
    //next();
}


function sortMeBy(arg, sel, elem, order) {
    var $selector = $(sel),
        $element = $selector.children(elem);
    //console.log($element);
    $element.sort(function(a, b) {

        var an = parseInt(a.getAttribute(arg)),
            bn = parseInt(b.getAttribute(arg));
        if (order == 'asc') {
            if (an > bn)
                return 1;
            if (an < bn)
                return -1;
        } else if (order == 'desc') {
            if (an < bn)
                return 1;
            if (an > bn)
                return -1;
        }
        return 0;
    });

    $element.detach().appendTo($selector);
}

$(document).ready(function() {
    tourList(tourTitle);
    $('[data-toggle="tooltip"]').tooltip();
    $('.partial_check').prop('indeterminate', true);
    toursContentHeight();
    /* $('.tabs-content-container').css('height', $('.itinerary_left').outerHeight() - $('.tabs-search-container').outerHeight()); */
    var removeC = [];
    var str = country_id_remove_push;
    var res = str.split(",");
    res.forEach(function(element) {
        removeC.push(element);
    });

    $("#remove_country").val(removeC);
    $('.moreCountry').click(function() {
        var country_id = $(this).attr('data-id');
        var morecountry_class = "morecountry" + country_id;
        var morecity = "morecity" + country_id;
        $('.' + morecountry_class).slideToggle();
        $('.' + morecity).toggleClass('open');
    });

    $(".modelmoreCountry").click(function() {
        var country_id = $(this).attr('data-id');
        var morecountry_class = "modelmorecountry" + country_id;
        var morecity = "modelmorecity" + country_id;
        $('.' + morecountry_class).slideToggle();
        $('.' + morecity).toggleClass('open');
    });


    $("#Slider1").slider({
        from: 100,
        to: 10000,
        step: 1,
        smooth: true,
        round: 0,
        dimension: "",
        skin: "round",
        limits: false
    });

    $("#Slider2").slider({
        from: 1,
        to: 100,
        step: 1,
        smooth: true,
        round: 0,
        dimension: "&nbsp;Days",
        skin: "round",
        limits: false
    });

    $('#scroll-box').slimScroll({
        height: '99%',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    $('.tourListGridBox, .gridTourList').slimScroll({
        height: '100%',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    $('.tourDetailDiv,.bookScroll,.tourViewItineraryDiv').slimScroll({
        height: '600px',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    $('.tourItineraryDiv, .tourmakeAnEnquiry').slimScroll({
        height: '94%',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    $(".countryList").click(function() {
        var val_id = $(this).val();
        var country_checkall = "morecountry" + val_id;
        $("#label-" + val_id).removeClass('label_partial');
        if (this.checked) {
            $("." + country_checkall).find('input[type=checkbox]').each(function() {
                this.checked = true;
            });
        } else {
            $("." + country_checkall).find('input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }
    });

    var regionIds = [];
    var i = 0;
    var countryListCount = 0;
    $('.countryList:checkbox:checked').each(function() {
        if ($(this).parent('p').css('display') != 'none') {
            countryListCount++;
        }
    });

    var cityListCount = 0;
    $('.cityList:checkbox:checked').each(function() {
        if ($(this).parent('p').css('display') != 'none') {
            cityListCount++;
        }
    });

    //console.log('countryListCount = '+countryListCount);

    $('.regionList:checkbox:checked').each(function() {
        var id = parseInt($(this).val());
        regionIds.push(id);

        $('.regionCountry_' + id).each(function() {
            i++;
            if (i >= countryListCount && i <= 10) {
                //console.log(i);
                $(this).show();
            }
        });
    });

    $('.regionList').click(function() {

        var regionId = parseInt($(this).val());
        $("#countryBox").find('p').hide();
        $(".modelRegionCountry").hide();

        if (!$(this).is(':checked')) {
            var id = parseInt($(this).val());

            $('.regionCountry_' + id + ' :input').each(function() {
                var conId = $(this).val();
                if ($(this).is(':checked')) {
                    $("#checkbox-model-" + conId).attr('checked', false);
                    $(this).attr('checked', false);
                }
            });
        }

        var i = 0;
        $('.regionList:checkbox:checked').each(function() {
            var id = parseInt($(this).val());
            regionIds.push(id);

            $("#modelRegionCountry_" + id).show();

            $('.regionCountry_' + id).each(function() {
                i++; //console.log('onclick = '+i);
                if (i >= 1 && i <= 10) {
                    $(this).show();
                }
            });

        });

        if (i == 0) {
            $(".modelRegionCountry").show();
        }
    });

    $('.countryList').click(function() {

        var id = parseInt($(this).val());
        if ($(this).is(':checked')) {
            $("#checkbox-model-" + id).attr('checked', true);
        } else {
            $("#checkbox-model-" + id).attr('checked', false);
        }

        var countryIds = [];
        var countryNames = [];
        var countryRegions = [];
        var countryRegionNames = [];
        var countryIdRemove = [];
        var cityID = [];
        var cityIDCount = [];

        $('.countryList:checked').each(function() {
            var id = parseInt($(this).val());
            var name = $(this).attr('data-name');
            var region = $(this).attr('data-region');
            var regionName = $(this).attr('data-regionName');

            countryIds.push(id);
            countryNames.push(name);
            countryRegions.push(region);
            countryRegionNames.push(regionName);
        });

        var data = {
            tourCountryData: [{
                countryIds: countryIds,
                countryNames: countryNames,
                countryRegions: countryRegions,
                countryRegionNames: countryRegionNames,
                regionIds: regionIds,
                search_type: 'country',
                cityIds: cityID,
                countryIdRemove: countryIdRemove
            }]
        };
        $("#country_names").val(countryNames);
        $("#region_id").val(countryRegions);
        $("#region_name").val(countryRegionNames);
        $("#remove_country").val(countryIdRemove);

        /*eroam.ajax('post', 'session/set_tourCountry', data, function(response){
            location.reload();
        });*/


    });

    $('.cityList').click(function() {

        var id = parseInt($(this).val());
        var country_id_chk = $(this).attr('data-country-id');

        var cityListId = "cityList" + country_id_chk;
        var numberOfChildCheckBoxes = $('.' + cityListId).length;
        var countryIdRemove = [];

        var rCountry = $("#remove_country").val();

        countryIdRemove.push(rCountry);
        if (jQuery.inArray(country_id_chk, rCountry)) {
            countryIdRemove.push(country_id_chk);
        }

        $("#label-" + country_id_chk).addClass('label_partial');
        $("#model-label-" + country_id_chk).addClass('label_partial');

        //$('.'+cityListId).change(function() {
        var checkedChildCheckBox = $('.' + cityListId + ':checked').length;
        if (checkedChildCheckBox == numberOfChildCheckBoxes) {
            $("#checkbox-" + country_id_chk).prop('checked', true);
            $("#label-" + country_id_chk).attr("class", "radio-checkbox label_check c_on");
            $("#label-" + country_id_chk).removeClass('label_partial');
            $("#label-" + country_id_chk).removeClass('c_on');

            $("#checkbox-model-" + country_id_chk).prop('checked', true);
            $("#model-label-" + country_id_chk).attr("class", "radio-checkbox label_check c_on");
            $("#model-label-" + country_id_chk).removeClass('label_partial');
            $("#model-label-" + country_id_chk).removeClass('c_on');

        } else if (checkedChildCheckBox == 0) {
            $("#label-" + country_id_chk).removeClass('label_partial');
            $("#checkbox-" + country_id_chk).attr('checked', false);

            $("#model-label-" + country_id_chk).removeClass('label_partial');
            $("#checkbox-model-" + country_id_chk).attr('checked', false);

        } else {
            $("#checkbox-" + country_id_chk).prop('checked', false);
            $("#checkbox-model-" + country_id_chk).prop('checked', false);
        }
        //});

        if ($(this).is(':checked')) {
            $("#checkbox-model-city-" + id).attr('checked', true);
        } else {
            $("#checkbox-model-city-" + id).attr('checked', false);
        }

        var cityIds = [];
        var cityNames = [];
        var cityRegions = [];
        var cityRegionNames = [];

        $('.cityList:checked').each(function() {
            var id = parseInt($(this).val());
            var name = $(this).attr('data-name');
            var region = $(this).attr('data-region');
            var regionName = $(this).attr('data-regionName');

            cityIds.push(id);
            cityNames.push(name);
            cityRegions.push(region);
            cityRegionNames.push(regionName);
        });

        var countryID = [];
        $('.countryList:checkbox:checked').each(function() {
            var id = $(this).val();
            countryID.push(id);
            totalTourCount = totalTourCount + parseInt($("#count" + id).text());
        });

        var data = {
            tourCityData: [{
                cityIds: cityIds,
                countryIds: countryID,
                countryNames: cityNames,
                countryRegions: cityRegions,
                countryRegionNames: cityRegionNames,
                regionIds: regionIds,
                search_type: 'city',
                countryIdRemove: countryIdRemove
            }]
        };

        $("#region_id").val(cityRegions);
        $("#region_name").val(cityRegionNames);

        //$(this).parent().find('.accordion-body').length == 1; 
        $("#remove_country").val(countryIdRemove);

        /*if (cityIds.length === 0) {
            window.location.href = "/";
        }
        else
        {
            eroam.ajax('post', 'session/set_tourCity', data, function(response){
                location.reload();
            });
        }*/

    });

    $('.modelcityList').click(function() {

        var id = parseInt($(this).val());
        var country_id_chk = $(this).attr('data-country-id');

        var cityListId = "modelcityList" + country_id_chk;
        var numberOfChildCheckBoxes = $('.' + cityListId).length;
        var countryIdRemove = [];
        var rCountry = $("#remove_country").val();

        if (jQuery.inArray(country_id_chk, rCountry)) {
            countryIdRemove.push(country_id_chk);
        }
        $("#label-" + country_id_chk).addClass('label_partial');
        $("#model-label-" + country_id_chk).addClass('label_partial');

        //$('.'+cityListId).change(function() {

        var checkedChildCheckBox = $('.' + cityListId + ':checked').length;

        if (checkedChildCheckBox == numberOfChildCheckBoxes) {
            $("#checkbox-model-" + country_id_chk).prop('checked', true);
            $("#model-label-" + country_id_chk).attr("class", "radio-checkbox label_check c_on");
            $("#model-label-" + country_id_chk).removeClass('label_partial');
            $("#model-label-" + country_id_chk).removeClass('c_on');

            $("#checkbox-" + country_id_chk).prop('checked', true);
            $("#label-" + country_id_chk).attr("class", "radio-checkbox label_check c_on");
            $("#label-" + country_id_chk).removeClass('label_partial');
            $("#label-" + country_id_chk).removeClass('c_on');
        } else if (checkedChildCheckBox == 0) {
            $("#model-label-" + country_id_chk).removeClass('label_partial');
            $("#checkbox-model-" + country_id_chk).attr('checked', false);

            $("#label-" + country_id_chk).removeClass('label_partial');
            $("#checkbox-" + country_id_chk).attr('checked', false);
        } else {
            $("#checkbox-model-" + country_id_chk).prop('checked', false);
            $("#checkbox-" + country_id_chk).prop('checked', false);
        }
        //});

        if ($(this).is(':checked')) {
            $("#checkbox-model-city-" + id).attr('checked', true);
        } else {
            $("#checkbox-model-city-" + id).attr('checked', false);
        }

        var cityIds = [];
        var cityNames = [];
        var cityRegions = [];
        var cityRegionNames = [];

        $('.cityList:checked').each(function() {
            var id = parseInt($(this).val());
            var name = $(this).attr('data-name');
            var region = $(this).attr('data-region');
            var regionName = $(this).attr('data-regionName');

            cityIds.push(id);
            cityNames.push(name);
            cityRegions.push(region);
            cityRegionNames.push(regionName);

        });

        var countryID = [];
        $('.countryList:checkbox:checked').each(function() {
            var id = $(this).val();
            countryID.push(id);
            totalTourCount = totalTourCount + parseInt($("#count" + id).text());
        });

        var data = {
            tourCityData: [{
                cityIds: cityIds,
                countryIds: countryID,
                countryNames: cityNames,
                countryRegions: cityRegions,
                countryRegionNames: cityRegionNames,
                regionIds: regionIds,
                search_type: 'city',
                countryIdRemove: countryIdRemove
            }]
        };

        $("#model_region_id").val(cityRegions);
        $("#model_region_name").val(cityRegionNames);
        $("#model_remove_country").val(countryIdRemove);

    });

    $('.countryListModel').click(function() {
        var id = parseInt($(this).val());
        if ($(this).is(':checked')) {
            $("#checkbox-model-" + id).attr('checked', true);
        } else {
            $("#checkbox-model-" + id).attr('checked', false);
        }

        var val_id = $(this).val();
        var country_checkall = "modelmorecountry" + val_id;
        $("#model-label-" + val_id).removeClass('label_partial');
        if (this.checked) {
            $("." + country_checkall).find('input[type=checkbox]').each(function() {
                this.checked = true;
            });
        } else {
            $("." + country_checkall).find('input[type=checkbox]').each(function() {
                this.checked = false;
            });
        }

        var countryIds = [];
        var countryNames = [];
        var countryRegions = [];
        var countryRegionNames = [];
        var countryIdRemove = [];
        var cityID = [];
        var cityIDCount = [];

        $('.countryListModel:checked').each(function() {
            var id = parseInt($(this).val());
            var name = $(this).attr('data-name');
            var region = $(this).attr('data-region');
            var regionName = $(this).attr('data-regionName');

            countryIds.push(id);
            countryNames.push(name);
            countryRegions.push(region);
            countryRegionNames.push(regionName);
        });

        var data = {
            tourCountryData: [{
                countryIds: countryIds,
                countryNames: countryNames,
                countryRegions: countryRegions,
                countryRegionNames: countryRegionNames,
                regionIds: regionIds,
                search_type: 'country',
                cityIds: cityID,
                countryIdRemove: countryIdRemove
            }]
        };
        $("#model_country_names").val(countryNames);
        $("#model_region_id").val(countryRegions);
        $("#model_region_name").val(countryRegionNames);
        $("#model_remove_country").val(countryIdRemove);
    });

    $('body').on('click', '#saveCountry', function() {

        $(this).text('LOADING...');

        $.ajax({
            type: 'POST',
            url: 'set_session_tours',
            data: $('#country-model').serialize(),
            dataType: 'json',
            success: function(response) {
                
                if (response.msg == "success") {
                    location.reload();
                }
            }
        });
    });

    var z = 0;
    $('.regionList:checkbox:checked').each(function() {
        z++;
    });
    if (z == 0) {
        $(".modelRegionCountry").show();
    }

    $(window).scroll(function() { //detact scroll
        if ($(window).scrollTop() + $(window).height() >= $(document).height()) { //scrolled to bottom of the page
            $("#more-loader").show();
            tourList(tourTitle); //load content chunk 
        }
    });

    $('body').on('click', '#searchTour', function() {
        $("#tours-loader").show();
        loading = false;
        end_record = false;
        start_page = 0;
        totalTourCount = parseInt(0);
        $('#gridTourList').html('');
        $('#listTourList').html('');
        var tourTitle = $("#tourSearch").val();
        tourList(tourTitle);
    });

    $("#search-form").submit(function(event) {
        $('#searchTour').click();
    });
});

function getDistinctCategory()
{    
    var items = {};
    $('div.list-box').each(function() {
        items[$(this).attr('data-category')] = true;
    });

    var html = '';
    var result = new Array();            
    for(var i in items)
    {
        if(i != 'N/A'){
            result.push(i);
            html += '<li><a id="searchCategory">'+i+'</a></li>';
        }

    }
    $('#category-tab').append( html );
}

function tourList(tourTitle) {

    var record_end_txt = ''; //$('<div/>').text(end_record_text).addClass('end-record-info'); //end record text

    if (loading == false && end_record == false) {
        loading = true; //set loading flag on

        var countryNames = [];
        $('.countryList:checkbox:checked').each(function() {
            var name = $(this).attr('data-name');
            countryNames.push(name);
            totalTourCount = totalTourCount + parseInt($("#count" + name).text());
        })


        var countryID = [];
        if(search_type == 'city') { 
            countryID.push(search_value);
        } 
        else 
        { 
            $('.countryList:checkbox:checked').each(function() {
                var id = $(this).val();
                countryID.push(id);
                totalTourCount = totalTourCount + parseInt($("#count" + id).text());
            });
        }


        var remove_country_ids = remove_country_id;
        var res_country_id = remove_country_ids.split("#");

        for (var i = 0; i < res_country_id.length; i++) {
            country_id_rm = res_country_id[i];
            var n = $(".cityList" + country_id_rm).length;
            var count_checked = 0;
            $(".cityList" + country_id_rm + ":checkbox:checked").each(function() {
                count_checked = count_checked + 1;
            });

            if (count_checked == n) {
                $("#label-" + country_id_rm).removeClass('label_partial');

                $("#model-label-" + country_id_rm).removeClass('label_partial');
            }
        }


        $('.countryList:checkbox:checked').each(function() {
            var id = $(this).val();
            var country_open = "morecountry" + id;
            var country_model_open = "modelmorecountry" + id;
            $("." + country_open).show();
            $("." + country_model_open).show();
        });

        var cityID = [];
        $('.cityList:checkbox:checked').each(function() {
            var id = $(this).val();
            cityID.push(id);
            totalTourCount = totalTourCount + parseInt($("#count" + id).text());

            var cid = $(this).attr('data-country-id');
            var country_open = "morecountry" + cid;
            $("." + country_open).show();

            var mcountry_open = "modelmorecountry" + cid;
            $("." + mcountry_open).show();
        });

        var searchDate = search_date;
        if (searchDate != '' && searchDate != '1970-01-01') {
            $("#dept_dates").html(search_date);
        } else {
            $("#dept_dates").html('All Date Selected');
        }

        tour_data = {
            countryNames: countryNames.join('# '),
            tourTitle: tourTitle,
            provider: 'searchTour',
            tour_type: tour_type,
            search_type: search_type,
            search_country_id: countryID.join('# '),
            search_city_id: cityID.join('# '),
            search_value: 'INDIAN EXPRESS',
            date: dep_date,
            default_selected_city: default_selected_city,
            offset: start_page,
            limit: 0
        };
        //console.log("tour_data = " +tour_data);
        // CACHING Tour List
        var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'searchTour', true);
        //console.log('tourResponse', tourApiCall);
        eroam.apiPromiseHandler(tourApiCall, function(tourResponse) {
            //console.log('tourResponse', tourResponse);

            $('.countryName').text(countryNames.join(', ').substring(0, 24));
            //totalTourCount += tourResponse.results.length;
            if (totalTourCount > 0) {
                $('.tour-count').html(': ' + totalTourCount + ((totalTourCount == 1) ? ' Tour' : ' Tours') + ' Found');
            }
            if (tourResponse.results.length > 0) {

                $.each(tourResponse.results, function(key, value) {

                    appendTours(value);
                    loading = false; //set loading flag off
                    $("#more-loader").hide();
                    start_page++; //page increment
                })
                getDistinctCategory();

                $('.tour-count').html(': ' + (start_page) + ((start_page == 1) ? ' Tour' : ' Tours') + ' Found');

                /*if(countryNames.length == 0 ) {  $('.tour-count').html(': ' + (start_page) + ( (start_page==1) ? ' Tour' : ' Tours' )+ ' Found'); }
                if(tourTitle.length > 0 ) {  $('.tour-count').html(': ' + (start_page) + ( (start_page==1) ? ' Tour' : ' Tours' )+ ' Found'); }*/

                //if(tourResponse.results.length < 100){
                $("#listTourList").append(record_end_txt);
                $("#gridTourList").append(record_end_txt);

                $("#more-loader").hide();
                end_record = true; //set end record flag on
                return; //exit
                //}
            } else {
                if (tourTitle.length > 0) {
                    $('.tour-count').html(': ' + (start_page) + ((start_page == 1) ? ' Tour' : ' Tours') + ' Found');
                }

                $("#listTourList").append('<h5><center>No Tours Found</center></h5>');
                $("#gridTourList").append('<h5><center>No Tours Found </center></h5>');


                $("#tours-loader").hide();
                $("#more-loader").hide();
                end_record = true; //set end record flag on
                return; //exit
            }

        })
    } else {
        $("#more-loader").hide();
        calculateHeight();
    }
}


function getStars(count, half = false) {
    var stars = '';
    if (parseInt(count)) {
        for (star = 1; star <= count; star++) {
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
        }
        var emptyStars = 5 - parseInt(count);
        if (half) {
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
        }
        for (empty = 1; empty <= emptyStars; empty++) {
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
        }
    } else {
        for (star = 1; star <= 5; star++) {
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
        }
    }
    var stars = '';
    return stars;
}

function checkImageUrl(id, url, url2) {
    eroam.ajax('post', 'existsImage', {
        url: url
    }, function(response) {
        if (response == 200) {
            var image = url; // "{{ url( 'assets/images/no-image.jpg' ) }}";
            //$("#tourImage_"+id).attr('src', image);
        } else if (response == 400) {
            var image = url; // "{{ url( 'assets/images/no-image.jpg' ) }}";
            //$("#tourImage_"+id).attr('src', image);
        } else {

            eroam.ajax('post', 'existsImage', {
                url: url2
            }, function(response) {
                if (response == 200) {
                    $(".tourImage_" + id).attr('src', url2);
                    //$(".backTourImage_"+id).css('background-image', 'url("' + url2 + '")');
                } else if (response == 400) {
                    $(".tourImage_" + id).attr('src', url2);
                    //$(".backTourImage_"+id).css('background-image', 'url("' + url2 + '")');
                } else {
                    var image = "{{ url( 'assets/images/no-image-2.jpg' ) }}";
                    $(".tourImage_" + id).attr('src', image);
                    //$(".backTourImage_"+id).css('background-image', 'url("' + image + '")');
                }
            });
        }
    });
}

function appendTours(tour) {

    $("#tours-loader").hide();
    var price = 0;
    var land_price = 0;
    if (!tour.flightPrice || tour.flightPrice < 1) {
        tour.flightPrice = 0;
    }
    if (tour.flightDepart != '' && tour.flightDepart != null) {      
        var city_name = 'MEL';
        if(default_selected_city == 7)
        {
            city_name = 'SYD';
        }
        else if(default_selected_city == 15)
        {
            city_name = 'BNE';
        }
        else if(default_selected_city == 30)
        {
            city_name = 'MEL';
        }
        tour.flightDepart = city_name;
    }
    if (tour.price) {
        price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
    }
    if (tour.price) {
        price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
        land_price = parseFloat(tour.price).toFixed(2);
    }
    var star;
    var rating = tour.rating;

    if (rating % 1 === 0) {
        stars = getStars(rating);
    } else {
        stars = getStars(rating, true);
    }
    if (tour.thumb) {
        var image_url_type = tour.thumb.replace("thumbnail", "small");
    } else {
        var image_url_type = 'assets/images/no-image-2.jpg';
    }
    var imgurl = 'http://www.adventuretravel.com.au' + tour.folder_path + '245x169/' + tour.thumb;
    var imgurl2 = 'http://dev.eroam.com/' + image_url_type;
    //var imgurl = 'https://cms.ijurni.com/'+image_url_type;
    //var imgurl2 = 'https://cms.ijurni.com/'+image_url_type;
    var tourlogo = 'http://dev.eroam.com/' + tour.logo_path;

    /*if(tour.provider != 194 && tour.provider != 206 && tour.provider != null)
    {
        var imgurl = 'http://dev.cms.eroam.com/'+image_url_type;
    }*/
    //console.log(tour.provider);
    //var tourlogo = "{{ url( 'assets/images/tour-logo.png' ) }}";
    var total_duration = '';
    if (tour.durationType == 'd') {
        total_duration = 'Day';
        if (Math.ceil(tour.no_of_days) > 1) {
            total_duration = 'Days';
        }
    } else if (tour.durationType == 'h') {
        total_duration = 'Hour';
        if (Math.ceil(tour.no_of_days) > 1) {
            total_duration = 'Hours';
        }
    }
    if (!tour.discount || tour.discount == '.00') {
        tour.discount = 0;
    }
    if (!tour.saving_per_person || tour.saving_per_person == '.00') {
        tour.saving_per_person = '0.00';
    }
    if (!tour.retailcost) {
        tour.retailcost = '0.00';
    }
    if (tour.location) {
        tour.location = removeLastComma(tour.location);
    }

    var tripActivities = '';
    if (tour.tripActivities) {
        tripActivities = tour.tripActivities.split(",");
        tripActivities = tripActivities[0];
    }
    
    var tour_name = tour.tour_title.toLowerCase();
    var gridHtml = '<div class="col-xl-4 col-sm-6 grid-box gridBox" data-price="' + price + '" data-category="' + tripActivities + '" data-tourName="' + tour_name + '" data-provider="eRoam" data-tour-provider="'+tour.provider+'" data-all-category="' + tour.tripActivities + '" style="cursor: pointer;"  onclick="window.location.href=\'/tourDetail/' + tour.tour_id + '/' + tour.tour_url + '\'">' +
        '<div class="card accomodation-grid mb-4 panel border-0">' +
        '<div class="accomodation-grid-img">' +
        '<img class="card-img-top  tourImage_' + tour.tour_id + '"  src="' + imgurl + '" data-src="'+APP_URL+'images/accomodation-default-image.jpg" alt="Hotel Name">';
    if (tour.flightPrice != 0) {
        gridHtml += 
            '<div class="flight-included">'+
                '<span class="flight-icon"><i class="ic-flight fa-lg"></i></span>' +
                '<span class="flight-text">Flight Included</span>' +
            '</div>';
    }
    gridHtml +=  '</div>' +
        '<div class="card-body mt-2">' +
        '<h6 class="font-weight-bold text-center text-truncate">' + tour.tour_title + '</h6>' +
        '<div class="mt-4">' +
        '<div class="row">' +
        '<div class="col-sm-5 col-5">' +
        '<div class="border-right pr-3 d-flex justify-content-between"> <span>TOTAL<br>' + total_duration.toUpperCase() + '</span>' +
        '<h5 class="font-weight-bold">' + tour.no_of_days + '</h5>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-7 col-7 d-flex justify-content-between">' +
        '<span>eRoam<br>Cost P.P</span>' +
        '<h5 class="font-weight-bold price-text">$' + price + '</h5>' +
        '</div>' +
        '</div>' +
        '</div>' ;

    if (tour.flightPrice > 0 && (tour.flightDepart != '' && tour.flightDepart != null)) {
        gridHtml += '<p class="flight-note">FLIGHTS INCLUSIVE DEPARTING ' + tour.flightDepart + '</p>';
    }

    var countryList = tour.CountryList;
    var country = countryList.split(",");
    gridHtml += '<hr class="border-secondary">' +
        '<div class="tours-facility">' +
        '<div class="row mb-2">' +        
        '<div class="col-sm-6 col-6 text-right">' +
        '<strong>Tour Codess</strong>' +
        '</div>' +
        '<div class="col-sm-6 col-6">' + tour.tour_code +
        '</div>' +
        '</div>' +
        '<div class="row mb-2">' +
        '<div class="col-sm-6 col-6 text-right">' +
        '<strong>Countries</strong>' +
        '</div>' +
        '<div class="col-sm-6 col-6">' +country[0] +
        '</div>' +
        '</div>' +
        // '<div class="row">'+
        //   '<div class="col-sm-6 col-xs-5">'+
        //     '<p class="orange-text">Tour Theme</p>'+
        //   '</div>'+
        //   '<div class="col-sm-6 col-xs-7">'+
        //     '<p>'+tour.ActivityList+'</p>'+
        //   '</div>'+
        // '</div>'+
        '<div class="row mb-2">' +
        '<div class="col-sm-6 col-6 text-right">' +
        '<strong>Group Size</strong>' +
        '</div>' +
        '<div class="col-sm-6 col-6">' +'Min ' + tour.groupsize_min + ' - Max ' + tour.groupsize_max +
        '</div>' +
        '</div>' +
        '<div class="row  mb-2">' +
        '<div class="col-sm-6 col-6 text-right">' +
        '<strong>Children</strong>' +
        '</div>' +
        '<div class="col-sm-6 col-6">' + (tour.is_childAllowed == 1 ? "Yes" : "No") +
        '</div>' +
        '</div>' +
        '</div>' +
        (tour.children_age != '' && tour.children_age != null && tour.is_childAllowed == 1 ? "<div class='row mb-2'><div class='col-sm-6 col-6 text-right'><strong>Children Age</strong></div><div class='col-sm-6 col-6'>" + tour.children_age + "</div></div>" : "") +
        '<div class="row mt-4 mb-3">' +
        '<div class="col-sm-6 col-6">';
    if (tour.discount && tour.discount != '.00') {
        gridHtml += '<div class="border-right pr-2 d-flex justify-content-between"><span>eRoam<br>Discount</span>' +
            '<h5 class="font-weight-bold">' + tour.discount + '%' + '</h5>';
            '</div></div>';
    }
  
    if (tour.discount && tour.discount != '.00') {
        gridHtml += '<div class="col-sm-6 col-6 d-flex justify-content-between"><span>Savings <br>From P.P</span>' +
            '<h5 class="font-weight-bold">$' + tour.saving_per_person + '</h5>' +
            '</div>' +
            '</div>';
    }
    gridHtml += '</div><small><em>*From price, discount and savings per person is based on the total price per adult in a twin share room, subject to departure dates</em></small>' +
        '<button type="submit" name="" class="btn btn-white btn-block mt-5" onclick="window.location.href=\'/tourDetail/' + tour.tour_id + '/' + tour.tour_url + '\'">EXPLORE FURTHER</button>' +
        '</div>' +
        '</div>' +
        '</div>'
        '</div>';
    $('#gridTourList').append(gridHtml);


    var itinerary = tour.itinerary;
    //itinerary = text.replace('old', 'new');
    itinerary = itinerary.split('<p>');
    var i = 1;
    var itineraryleg = '';
    itinerary.forEach(function(item) {
        if (i <= 3) {
            item = item.replace('</p>', '');
            if ($.trim(item).length > 0) {
                if (item.length > 180) {
                    itineraryleg = itineraryleg + '<li>' + item.substr(0, 80) + '...</li>';
                } else {
                    itineraryleg = itineraryleg + '<li>' + item + '</li>';
                }
                i++;
            }
        }
    });


    // console.log(tour);
    // console.log("inn");
    var countryList = tour.CountryList;
    var country = countryList.split(",");
    var tour_name = tour.tour_title.toLowerCase();
    var listHtml = '<div class="list-box card panel border-0 p-3 mb-3" data-price="' + price + '" data-category="' + tripActivities + '" data-all-category="' + tour.tripActivities + '" data-tourName="' + tour_name + '" data-provider="eRoam" style="cursor: pointer;" onclick = "window.location.href = \'/tourDetail/' + tour.tour_id + '/' + tour.tour_url + '\'">' +
        '<div class="row">' +
        '<div class="col-sm-7 col-xl-8">' +
        '<h5 class="font-weight-bold">' + tour.tour_title + '</h5>' +
        '<div class="media">' +
        '<img src="'+APP_URL+'/images/tour.svg">' +
        '<div class="media-body pb-3 mb-0">'+
        'Tour Code: ' + tour.tour_code + '   |   Countries: ' + country[0] +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-5 col-xl-4">' +
        '<div class="row">' +
        '<div class="col-xl-9 col-sm-8 text-left text-sm-right border-right tour-top-details">';

    var flight_text = '(Land Only)';
    if (tour.flightPrice != 0) {
        flight_text = '(Inclusive Flights)';
    }
    listHtml += 'From <strong>$' + price + '</strong> P.P ' + flight_text + '<br>';
    if (tour.discount && tour.discount != '.00') {
        listHtml += '<span class="red-text">eRoam Discount <strong>' + tour.discount + '%</strong></span>';
    }
    if (tour.saving_per_person && tour.saving_per_person != '.00') {
        listHtml += '<span class="yellow-text">Savings From <strong>$' + tour.saving_per_person + '</strong> P.P</span>';
    }

    listHtml += '</div>' +
        '<div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0">' +
        '<h2 class="transport-price font-weight-bold mb-0">' + tour.no_of_days + '</h2>' + total_duration +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +        
        '<hr/>' +
        '<div class="clearfix">' +
        '<div class="listbox-image accomodation-img"><img src="' + imgurl + '" alt="" class="img-fluid tourImage_' + tour.tour_id + '" />';
    if (tour.flightPrice != 0) {
        listHtml += '<div class="flight-included"><span class="flight-icon"><i class="fa fa-plane"></i></span><span class="flight-text">Flight Included</span></div>';
    }
    listHtml += '</div>' +
        '<div class="accomodation-details lh-condensed">' +
        '<div class="media">' +
        '<img src="' + tourlogo + '" alt="" class="img-fluid mr-2" />' +
        '<div class="media-body">' +
        '<div class="row">' +
        '<div class="col-xl-6 col-sm-12">'+
        '<div class="row mb-2 mb-sm-0">'+
        '<div class="col-lg-6 col-sm-4"><span class="font-weight-bold">Departure City</span></div>' +
        '<div class="col-lg-6 col-sm-8 city_wrap"><span>' + tour.departure + '</span></div>' +
        '</div>' +
        '<div class="row mb-2 mb-sm-0 mt-1">' +
        '<div class="col-lg-6 col-sm-4"><span class="font-weight-bold">Destination City</span></div>' +
        '<div class="col-lg-6 col-sm-8 city_wrap"><span>' + tour.destination + '</span></div>' +
        '</div>' +
        '</div>' +
        '<div class="col-xl-6 col-sm-12">' +
        '<div class="row">' +
        '<div class="col-lg-6 col-sm-4"><span class="font-weight-bold">Group Size</span></div>' +
        '<div class="col-lg-6 col-sm-8"><span>Min ' + tour.groupsize_min + ' - Max ' + tour.groupsize_max + '</span></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-lg-6 col-sm-4"><span class="font-weight-bold">Children</span></div>' +
        '<div class="col-lg-6 col-sm-8"><span>' + (tour.is_childAllowed == 1 ? "Yes" : "No") + '</span></div>' +
        '</div>' +
        (tour.children_age != '' && tour.children_age != null && tour.is_childAllowed == 1 ? "<div class='row'><div class='col-lg-6 col-sm-4'><span class='font-weight-bold'>Children Age</span></div><div class='col-lg-6 col-sm-8'><span>" + tour.children_age + "</span></div></div>" : "") +
        '</div>' +
        '</div>' +
        '<div class="row mt-2 mt-sm-3">' +
        '<div class="col-sm-12">' +
        '<div class="row">' +
        '<div class="col-xl-3 col-lg-6 col-sm-4"><span class="font-weight-bold">Accommodation</span></div>' +
        '<div class="col-xl-9 col-lg-6 col-sm-8"><span>' + tour.accommodation + '</span></div>' +       
        '</div>' +
        '<div class="row mt-2 mt-sm-1">' +
        '<div class="col-xl-3 col-lg-6 col-sm-4"><span class="font-weight-bold">Transport</span></div>' +
        '<div class="col-xl-9 col-lg-6 col-sm-8"><span>' + tour.transport + '</span></div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="mt-3">' +tour.long_description.substr(0, 390) + ' ' + (tour.long_description.length > 390 ? "..." : "")  +
        '</div>' +
        '</div>' +
        '</div>';
    $('#listTourList').append(listHtml);


    imageUrl = checkImageUrl(tour.tour_id, imgurl, imgurl2);
}
$(window).resize(function() {
    toursContentHeight();
});

function toursContentHeight() {
    $('a#pills-profile-tab').on('shown.bs.tab', function(e) { //a[data-toggle="pill"]
        var maxHeight = 0;
        $(".tours-facility").each(function() {
            maxHeight = Math.max(maxHeight, $(this).height());
        });
        $(".tours-facility").height(maxHeight);

        $(".accomodation-grid").each(function() {
            maxHeight = Math.max(maxHeight, $(this).height());
        });
        $(".accomodation-grid").height(maxHeight);

        $(".accomodation-grid-img").each(function() {
            maxHeight = Math.max(maxHeight, $(this).height());
        });

        $(".accomodation-grid-img").height(maxHeight);
    })
}
if ($(window).width() > 991) {
    $('.tabs-content-scroll-wrapper').slimScroll({
        height: '100%',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });
}