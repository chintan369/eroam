@extends('layouts.home')


@section('custom-css')
<style>
  .wrapper {
    background-image: url( {{ url( 'images/bg1.jpg' ) }} ) !important;
  }
  .top-margin{
    margin-top: 5px;
  }
  .container-padding{
    padding-left: 35px;
    padding-right: 35px;
  }
  .top-bottom-paddings{
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .white-box{
    background: rgba(255, 255, 255, 0.9);
    border: solid thin #9c9b9b !important;
  }
  .main-content{
    padding: 10px 0 265px 0;
  }
  .full-width{
    width: 100%;
  }
  .half-width{
    width: 50%;
  }
  .table-display{
    display: table;
  }
  .radius{
    border-radius: 2px !important;
  }
  .left-radius{
    border-radius: 2px 0 0 2px !important;
  }
  .right-radius{
    border-radius: 0 2px 2px 0 !important;
  }
  
  .default-border{
    border: 1px solid #2AA9DF !important;
  }
  .new-border{
    border: 2px solid #333 !important;
  }
  .white-button{
    transition: all 0.5s;
    background: #fff;
      color: #333;
      padding: 11px;
      font-weight: bold;
  }

  .default-button{
    background: #BEE5F6;
      border-color: #2AA9DF;
      color: #2AA9DF;
      padding: 11px;
      font-weight: bold;
  }
  .blue-button{
    transition: all 0.5s;
    background: #2AA9DF;
      border-color: #2AA9DF;
      color: #fff;
      padding: 11px;
      font-weight: bold;
  }
  .manual.default-button:hover,
  .auto.default-button:hover, 
  .manual.default-button.focus, 
  .auto.default-button.focus,
  .blue-button:hover,
  .blue-button.focus{
    background: #BEE5F6;
    color: #2AA9DF;
    border-color: #2AA9DF;

  }
  .auto.white-button:hover, .manual.white-button:hover{
    background: #BEE5F6;
    color: #2AA9DF;
  }
  .select-style{
    text-align-last: center;
    border: 1px solid #000; 
    padding: 11px;
    width: 100%;
    background: #fff;
  }
  .select-padding{
    padding: 12px 0 12px 0;
  }
  .col-reduced-padding{
    padding-right: 2px ;
    padding-left: 2px ;
  }
  .col-reduced-padding:last-child{
    padding-right:0 ;
  }
  .col-reduced-padding:first-child{
    padding-left:0 ;
  }

  .last{
    padding-right:0 ;
  }
  .first{
    padding-left:0 ;
  }
  #homepage form .multi-city,
  #homepage form #search{
    padding-top: 20px;
  }
  #homepage #homepage-desc{
    padding-top: 10px;
    line-height: 1.3em;
  }

  .interest-lists > li {
    /*display: inline-block;
    margin-bottom: 8px;
    margin-left: 5px;*/
  }
  #traveller-option-toggle-box{
    display: none;
  }
  .hide{
    display: none;
  }
  .wrapper{
    background-attachment: fixed;
  }
  #profile-preferences-toggler, #check-box{
    cursor: pointer;
  }
  #profile-preferences-toggler > i, #check-box > i{
    vertical-align: middle;
  }
  #profile-preferences-toggler > p, #check-box  > p{
    padding-left: 12px;
    display: inline-block;
  }
  #traveller-option-toggle-box{
    margin-top: 20px;
  }
  /*.interest-button-active, .interest-lists > li:hover{
    background:  rgba(39, 168, 223, 0.44);
    color: #2AA9DF;
  }*/
  #profile-preferences-data{
    line-height: 1.7em;
  }
  #profile-preferences-data > span {
    padding-right: 15px;
  }
  #traveller-option-toggle-box > div > select{
    margin-bottom: 7px;
  }
  .country-button{
    background: #fff;
      color: #333;
  }

  .btn.focus{
    outline: none !important;
  }
  .country-city-container   ul{
    list-style: none;
      padding-left: 20px;
          padding-top: 13px;
      /* padding-right: 20px; */
  }
  .country-city-container   ul > li{
    display: inline-block;
    margin-bottom: 10px;
    margin-right: 12px;
    color: #333;
    border-radius: 0px;
    font-weight: 500;
  }
  .country-city-container  ul > li:hover{
    background:  #333;
    color: #fff;
  }
  .modal-content{
    border-radius: 3px;
  }
  .modal-footer{
    background: #008ed4;
  }
  #all-countries-container{
    display: none;
  }
  .destination-container{
    display: none;
  }
  .col-md-2-4 {
      width: 20%;
      float: left;
  }

  button{
    overflow: hidden;
  }
  .icon-style{
    border: 2px solid #918F8F;
      border-radius: 4px;
      padding: 6px 0;
      text-align: center;
      width: 37px;
      font-size: 21px;
  }
  .check-box{
    color: #fff;
  }
  .black-background{
    background: #000;
  }
  /* #no-time-box {
    display: none;
  } */
  .btn{
    border-radius: 3px;
  }
  .country-back{
    float: left;
      margin: -3px 20px -2px 2px;
      background: none;
      border: none;
    color: #333;
    display: none;
  }
  .modal-header .close {
      margin-top: 0px;
  }
  .modal-header{
    border-bottom: none !important;
    padding: 15px 15px 0 15px;
  }

  .region-list{
    list-style: none;
    display: table;
    padding: 0;
    width: 100%;
  }
  .region-list > li{
    display: inline-block;
    border: solid thin;
    height: 100%;
    display: table-cell;
  }
  .country-city-container{
    padding-top: 0px;
  }
  .region-name{
    border-bottom: 1px solid #999;
    padding-bottom: 8px;
  }
  .country-city-title{
    padding-left: 13px;
  }
  .city-list{
    margin-top: 25px;
  }
  #profile-preferences-data > span{
    display: inline-block;
  }
  .btn-default,
  .btn-default:hover,
  .btn-default.active.focus, 
  .btn-default.active:focus, 
  .btn-default.active:hover, 
  .btn-default:active.focus, 
  .btn-default:active:focus, 
  .btn-default:active:hover, 
  .open>.dropdown-toggle.btn-default.focus, 
  .open>.dropdown-toggle.btn-default:focus, 
  .open>.dropdown-toggle.btn-default:hover{
    background: #fff !important;
    border: 1px solid #000;
  } 
  .dropdown-menu {
    min-height: 20px;
    max-height: 200px;
    overflow-y: scroll;
  }
  .dropdown-menu > li{
    padding-left: 20px;
    cursor: pointer;
  }
  .dropdown-menu > li > label{
    font-weight: 100;
    width: 100%;
    color: #000;
  }
  .dropdown-menu > li > label > i{
    margin-right: 10px;
    color: #2AA9DF;
  }
  .dropdown-menu > li input{
    visibility: hidden;
  }
  .dropdown-menu > li:hover > label{
    color: #2AA9DF;
  }
  .dropdown-menu label{
    display: inline-block;
    max-width: 100%;
    margin-bottom: 0; 
    padding-bottom: 5px;
  }
  span.drop-selected{
    display: inline-block;
      border: 1px solid #2AA9DF;
      padding: 0 5px 0 5px;
      margin-right: 2px;
      border-radius: 2px;
      background: #fff;
      color: #2AA9DF;
  }
  .switch-box{
    border: solid thin #ccc;
      width: 50px;
      height: 24px;
      display: inline-block;
      border-radius: 10px;
      vertical-align: middle;
      padding: 0 1px;
      text-align: left;
      background: #ccc;
      cursor: pointer;
  }
  .switch-box > .switcher{
    transition: all .4s !important;
    width: 20px;
    height: 20px;
    display: inline-block;
    text-align: right;
    cursor: pointer;
  }
  .switch-box .switch-button{
    background: #fff;
    display: inline-block;
    height: 20px;
    width: 20px;
    border-radius: 50%;
    vertical-align: middle;
    cursor: pointer;
  }
  .checked{
    transition: all .4s !important;
    width: 100% !important;
  }
  .on{
    background: #2AA9DF !important;
    border: solid thin #2AA9DF !important;
  }
  #no-accommodation{
    position: absolute;
    z-index: -9;
  }
  .icon-container{
    transition: all 0.4s;
    width: 40px;
      height: 40px;
     
      background: #fff;
      border-radius: 3px;
      vertical-align: middle;
      display: inline-block;
  }
  .icon-container > div {
    transition: all 0.4s;
      width: 15px;
      height: 15px;
      background-color: #fff;
      margin: 2px;
      float: left;
      border-radius: 3px;
      border: 2px solid #000;
  }
  #profile-preferences-toggler:hover .icon-container > div{
    background: #000;
  }
  .dark{
    background-color: #000 !important;
  }
  .thumbnail{
    padding: 0;
    margin-bottom: 0;
    border: none;
    /* border: 1px solid rgba(76, 72, 72, 0.65); */
  }
  .thumbnail > .caption{
    border-bottom: solid thin rgba(76, 72, 72, 0.65);
    border-left: solid thin rgba(76, 72, 72, 0.65);
    border-right: solid thin rgba(76, 72, 72, 0.65);
    border-radius: 0 0 2px 2px;
  }
  .thumbnail > img{
    border-radius: 2px 2px 0 0;
  }
  .caption > h5{
    font-weight: bold;
  }
  .popular{
    margin: 20px 0;
  }
  .slick-prev {
    left: -8px;
    color: red;
  }
  .slick-next {
    right: -8px;
  }
  .slick-prev:before, .slick-next:before {
    color: rgba(0, 0, 0, 0.41);
    font-size: 40px;
  }

  .slick-prev, .slick-next {
    z-index: 9999;
    height: 40px;
    width: 40px;
    top: 30%;
  }
  .slick-initialized .slick-slide{
    outline: none !important;
  }
  .slick-pad{
    padding: 0 2px;
  }
  .slider-slick{
    margin-bottom: 20px;
  }
</style>
@stop



@section('content')

<?php
  $options = '<div class="col-md-12">';
    foreach($countries as $country){
      $options .= '<h5 class="region-name">'. $country['name'].'</h5>';
      $options .= '<ul class="country-list" data-selector="">';
      usort($country['countries'], 'sort_by_name');
      foreach ($country['countries'] as $country_data){

        if( count($country_data['city']) > 0 ){
            $options .= '<li data-value="'.$country_data['id'].' " class="btn white-button new-border select-country">'. $country_data['name'] .'</li>'; 
        }else{
            continue;
        }
      }
      $options .= '</ul>';
    }
  $options .= '</div><div class="clearfix"> </div>';
?>
  
          <div class="location-wrapper">
            <form method="post" action="search" id="search-form">
              {{ csrf_field() }}

              <input type="hidden" name="country" id="starting_country" value="{{ $starting_country }}">
              <input type="hidden" name="city" id="starting_city" name ="starting_city" value="{{ $starting_city }}" required>
            
              <input type="hidden" name="to_country" id="destination_country" value="{{ $destination_country }}">
              <input type="hidden" name="to_city" id="destination_city" name="destination_city" value="{{ $destination_city }}">

              <input type="hidden" name="auto_populate" id="auto-populate" value="1">


              <div id="changePreferences">
                <?php 
                  /*
                  | Added by Rekha
                  | Accomodation multi select options
                  */
                  
                  $accommodation_options = '';
                  $room_type_options = '';
                  $transport_type_options  = '';
                  $nationality = '';
                  $gender = '';
                  $age_group = '';
                  $interestoption ='';
                  
                  if ( count( $travellers['categories'] ) > 0 ) {
                    foreach( $travellers['categories'] as $category ){
                      if( empty( $category['name'] ) ){
                        continue;
                      }
                      if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
                        $accommodation_options .= '<input type="hidden" name="hotel_category_id[]"  value="'.$category['id'].'" >';
                      }
                    }
                  }

                  if( count( $travellers['room_types'] ) > 0 ){
                    foreach( $travellers['room_types'] as $room_type){
                      if( empty( $room_type['name'] ) ){
                        continue;
                      }
                      if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
                        $room_type_options .= '<input type="hidden" name="room_type_id[]" value="'.$room_type['id'].'" >';
                      }
                    }
                  }

                  if( count( $travellers['transport_types'] ) > 0 ){
                    foreach( $travellers['transport_types'] as $transport_type){
                      if( empty( $transport_type['name'] ) ){
                          continue;
                      }
                      if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
                        $transport_type_options .= '<input type="hidden" name="transport_types[]" value="'.$transport_type['id'].'" >';
                      }
                    }
                  }
              

                  if( count( $travellers['nationalities'] ) > 0 ){
                    foreach( $travellers['nationalities']['featured'] as $featured ){
                      if( empty( $featured['name'] ) ){
                        continue;
                      }

                      if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) {
                        $nationality .= '<input type="hidden" name="nationality" value="'.$featured['id'].'" > ';
                      }
                    }
                    
                    foreach( $travellers['nationalities']['not_featured'] as $not_featured ){
                      if( empty( $not_featured['name'] ) ){
                        continue;
                      }
                      if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) ){
                        $nationality .= '<input type="hidden" name="nationality" value="'.$not_featured['id'].'" >';
                      }
                    }
                  }

                  $gender_array = ['male', 'female', 'other'];
                  foreach($gender_array as $gen){
                    if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) {
                      $gender .= '<input type="hidden" name="gender" value="'.$gen.'">';
                    }
                  }
                
                  if( count( $travellers['age_groups'] ) > 0 ){
                    foreach( $travellers['age_groups'] as $age){
                      if( empty( $age['name'] ) ){
                        continue;
                      }
                      if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) {
                        $age_group .= '<input type="hidden" name="age_group" value="'.$age['id'].'" >'; 
                      }
                    }
                  }

                  if( count($labels) > 0 ){
                    foreach ($labels as $i => $label){
                      if( empty( $age['name'] ) ){
                        continue;
                      }

                      $sequence = '';
                      if( in_array($label['id'], $interest_ids ) ){ 
                        $sequence = array_search($label['id'], $interest_ids); 
                        $sequence += 1;
                        $interestoption .='<input type="hidden" name="interests['.$sequence.']" value="'.$label['id'].'">';
                      }      
                    }
                  }
                  echo $accommodation_options.$room_type_options.$transport_type_options.$nationality.$gender.$age_group.$interestoption;
                

                ?>
              </div>


              <div id="scroll-box">
                <div class="location-inner m-t-20">
                  <div class="loc-icon">
                    <svg width="28px" height="27px" viewBox="0 0 28 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="45---EROA007-V4.1-Generic-(Create-Itinerary)-01" transform="translate(-18.000000, -59.000000)" fill="#212121">
                                <g id="(Generic)-Icon---Location-Preferences" transform="translate(18.000000, 59.000000)">
                                    <g id="(eRoam)-Icon---Location-Preferences">
                                        <path d="M7.53597816,15.6938325 C6.41269472,15.6938325 5.49904215,14.7574227 5.49904215,13.6064237 C5.49904215,12.4554248 6.41269472,11.519015 7.53597816,11.519015 C8.65926159,11.519015 9.57337087,12.4554248 9.57337087,13.6064237 C9.57337087,14.7574227 8.65926159,15.6938325 7.53597816,15.6938325 M7.53597816,9.92609431 C5.53146871,9.92609431 3.90054977,11.5770428 3.90054977,13.6064237 C3.90054977,15.6358046 5.53146871,17.2867531 7.53597816,17.2867531 C9.54071596,17.2867531 11.1718633,15.6358046 11.1718633,13.6064237 C11.1718633,11.5770428 9.54071596,9.92609431 7.53597816,9.92609431 M7.53506473,24.6908759 C6.91781832,24.0081956 5.94250961,22.8703952 4.97108295,21.4975251 C2.91405161,18.5904449 1.82684844,15.9841991 1.82684844,13.9605072 C1.82684844,8.2633127 6.19644154,7.82048075 7.5357498,7.82048075 C12.8329252,7.82048075 13.2446512,12.5200518 13.2446512,13.9605072 C13.2446512,17.9671578 9.15410916,22.8967921 7.53506473,24.6908759 M12.6621149,7.80637203 C11.3504377,6.64171947 9.52975487,6 7.5357498,6 C5.54174473,6 3.72106191,6.64171947 2.40938473,7.80637203 C0.833042887,9.20632173 0,11.3342362 0,13.9605072 C0,19.6809129 6.6061123,26.383923 6.8872186,26.6663251 C7.058714,26.8388156 7.29232224,26.9355287 7.5357498,26.9355287 C7.77917735,26.9355287 8.0127856,26.8388156 8.18428099,26.6663251 C8.4653873,26.383923 15.0714996,19.6809129 15.0714996,13.9605072 C15.0714996,11.3342362 14.2384567,9.20632173 12.6621149,7.80637203" id="location"></path>
                                        <path d="M19.5359782,9.69383246 C18.4126947,9.69383246 17.4990421,8.75742267 17.4990421,7.60642371 C17.4990421,6.45542476 18.4126947,5.51901497 19.5359782,5.51901497 C20.6592616,5.51901497 21.5733709,6.45542476 21.5733709,7.60642371 C21.5733709,8.75742267 20.6592616,9.69383246 19.5359782,9.69383246 M19.5359782,3.92609431 C17.5314687,3.92609431 15.9005498,5.57704279 15.9005498,7.60642371 C15.9005498,9.63580463 17.5314687,11.2867531 19.5359782,11.2867531 C21.540716,11.2867531 23.1718633,9.63580463 23.1718633,7.60642371 C23.1718633,5.57704279 21.540716,3.92609431 19.5359782,3.92609431 M19.5350647,18.6908759 C18.9178183,18.0081956 17.9425096,16.8703952 16.971083,15.4975251 C14.9140516,12.5904449 13.8268484,9.98419914 13.8268484,7.96050722 C13.8268484,2.2633127 18.1964415,1.82048075 19.5357498,1.82048075 C24.8329252,1.82048075 25.2446512,6.52005182 25.2446512,7.96050722 C25.2446512,11.9671578 21.1541092,16.8967921 19.5350647,18.6908759 M24.6621149,1.80637203 C23.3504377,0.641719466 21.5297549,0 19.5357498,0 C17.5417447,0 15.7210619,0.641719466 14.4093847,1.80637203 C12.8330429,3.20632173 12,5.33423617 12,7.96050722 C12,13.6809129 18.6061123,20.383923 18.8872186,20.6663251 C19.058714,20.8388156 19.2923222,20.9355287 19.5357498,20.9355287 C19.7791774,20.9355287 20.0127856,20.8388156 20.184281,20.6663251 C20.4653873,20.383923 27.0714996,13.6809129 27.0714996,7.96050722 C27.0714996,5.33423617 26.2384567,3.20632173 24.6621149,1.80637203" id="location"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                  </div>
                  <div class="location-content">
                    <h4 class="loc-title">Location Preferences</h4>
                    <button type="button" class="btn btn-primary btn-block m-t-20 active" id="auto_multicity">AUTO MULTI-CITY</button>
                    <button type="button" class="btn btn-primary btn-block m-t-10 " id="manual_multicity">Manual MULTI-CITY</button>
                    <input type="hidden" name="option" id="option1" value="auto">
                    <div class="m-t-10">
                      @if($starting_city == '')
                      <div>
                        <button type="button" class="btn btn-primary btn-block country-button" id="starting" data-value="starting">Select a starting Point</button>
                        <label id="starting_city-error" class="error" for="starting_city" style="display: inline-none;">This field is required.</label>
                      </div>
                      @endif
                        @if($destination_city == '')
                      <div class="input-field">
                        <button type="button" class="btn btn-primary btn-block m-t-20 country-button" id="destination" data-value="destination" style="display:none;">Select a destination</button>
                      </div>
                        @endif
                        @if($starting_city != '')
                        <div class="input-field">
                          <input id="fromcity" name="fromcity" type="text" placeholder="Starting Point" value="{{$startName}}" readonly>
                        </div>
                        @endif
                        @if($destination_city != '')
                        <div class="input-field">
                          <input id="tocity" name="tocity" type="text" placeholder="Destination Point" value="{{$toName}}" readonly>
                        </div>
                        @endif
                      <div class="input-field">
                        <input id="startdate" name="start_date" type="text" placeholder="Start Date" >
                      </div>
                      <div class="input-field">
                        <input id="travellers" type="number" name="travellers" min="1" max="10" placeholder="Number of Travellers">
                        
                      </div>
                    </div>
                  </div>
                </div>
                <hr/>
                <div class="location-inner">
                  <div class="loc-icon"></div>
                  <div class="location-content">
                    <h4 class="loc-title">Travel Preferences</h4>
                    <label>Price Range Per Day</label>
                    <!-- <div>
                      <b>€ 10</b> <input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> <b>€ 1000</b>
                    </div> -->

                    <div class="layout-slider">
                      <span style="display: inline-block; width: 100%; padding: 0 5px;">
                      <input id="Slider1" type="slider" name="price" value="100;10000" />
                      </span>
                    </div>

                    <label>Number of Total Days</label>
                    <div class="layout-slider">
                      <span style="display: inline-block; width: 100%; padding: 0 5px;">
                      <input id="Slider2" type="slider" name="price1" value="1;100" />
                      </span>
                    </div>

                    <div class="m-t-20 black-box">
                      <p><label class="radio-checkbox label_check" for="checkbox-01"><input type="checkbox" id="checkbox-01" name="no_accommodation" value="1">I don’t need accommodation within <span id="no-accomdation-to" style="color: #333;font-weight: bold;">{{$startName}}</span></label></p>
                      <!--<p><label class="radio-checkbox label_check" for="checkbox-02"><input type="checkbox" id="checkbox-02" value="1">I want to self drive between locations</label></p>
                      <p><label class="radio-checkbox label_check" for="checkbox-03"><input type="checkbox" id="checkbox-03" value="1">I already have a travel pass</label></p>-->
                    
                    <button type="button" name="" class="btn btn-primary btn-block"  data-toggle="modal" data-target="#preferencesModal" id="openModel">Update Travel Preferences</button>

                    </div>

                  </div>
                </div>
               <?php /*<!--  <hr/> -->
               <!--  <div class="location-inner">
                  <div class="loc-icon">
                    <svg width="20px" height="24px" viewBox="0 0 20 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                        <!-- <desc>Created with Sketch.</desc> -->
                       <!--  <defs></defs> -->
                       <!--  <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="45---EROA007-V4.1-Generic-(Create-Itinerary)-01" transform="translate(-22.000000, -780.000000)" fill="#212121">
                                <g id="(Generic)-Icon---PAX-Preferences" transform="translate(22.000000, 780.000000)">
                                    <g id="(eRoam)-Icon---PAX-Preferences">
                                        <path d="M13.4646063,13.9881818 C13.6438724,14.1654545 13.7459162,14.4109091 13.7459162,14.6645455 C13.7459162,14.9154545 13.6438724,15.1609091 13.4646063,15.3381818 C13.2853401,15.5154545 13.0343675,15.6163636 12.780637,15.6163636 C12.5269065,15.6163636 12.2786918,15.5154545 12.0994257,15.3381818 C11.9174016,15.1609091 11.8153578,14.9154545 11.8153578,14.6645455 C11.8153578,14.4109091 11.9174016,14.1654545 12.0994257,13.9881818 C12.4579579,13.6336364 13.106074,13.6336364 13.4646063,13.9881818 L13.4646063,13.9881818 Z M5.28841544,6.13636364 C5.28841544,5.60918182 5.72030894,5.18181818 6.25369466,5.18181818 L12.8184208,5.18181818 C13.3515307,5.18181818 13.7837,5.60918182 13.7837,6.13636364 C13.7837,6.66354545 13.3515307,7.09090909 12.8184208,7.09090909 L6.25369466,7.09090909 C5.72030894,7.09090909 5.28841544,6.66354545 5.28841544,6.13636364 L5.28841544,6.13636364 Z M5.28841544,10.2272727 C5.28841544,9.70009091 5.72030894,9.27272727 6.25369466,9.27272727 L12.8184208,9.27272727 C13.3515307,9.27272727 13.7837,9.70009091 13.7837,10.2272727 C13.7837,10.7544545 13.3515307,11.1818182 12.8184208,11.1818182 L6.25369466,11.1818182 C5.72030894,11.1818182 5.28841544,10.7544545 5.28841544,10.2272727 L5.28841544,10.2272727 Z M5.13507394,14.5909091 C5.13507394,14.0637273 5.56724324,13.6363636 6.10035316,13.6363636 L9.3605148,13.6363636 C9.89362472,13.6363636 10.325794,14.0637273 10.325794,14.5909091 C10.325794,15.1180909 9.89362472,15.5454545 9.3605148,15.5454545 L6.10035316,15.5454545 C5.56724324,15.5454545 5.13507394,15.1180909 5.13507394,14.5909091 L5.13507394,14.5909091 Z M2.52385574,21.8181818 L16.8651471,21.8181818 L16.8651471,2.18181818 L2.52385574,2.18181818 L2.52385574,21.8181818 Z M17.9683233,0 L1.42067948,0 C0.811450392,0 0.317503223,0.488454545 0.317503223,1.09090909 L0.317503223,22.9090909 C0.317503223,23.5115455 0.811450392,24 1.42067948,24 L17.9683233,24 C18.5775524,24 19.0714996,23.5115455 19.0714996,22.9090909 L19.0714996,1.09090909 C19.0714996,0.488454545 18.5775524,0 17.9683233,0 L17.9683233,0 Z" id="document"></path>
                                    </g>
                                </g>
                            </g>
                        </g> -->
                   <!--  </svg>
                  </div> --> 
                  <!-- <div class="location-content black-box">
                    <h4 class="loc-title">Traveller Details</h4>
                    <div class="m-t-20">
                      <div class="input-field">
                        <input id="field4" type="text">
                        <label for="field4">Text Field</label>
                      </div>
                      <div class="input-field">
                        <input id="field5" type="text">
                        <label for="field5">Text Field</label>
                      </div>
                      <div class="input-field">
                        <input id="field6" type="text">
                        <label for="field6">Text Field</label>
                      </div>
                    </div>
                    <button type="button" name="" class="btn btn-primary btn-block">Update Travel Preferences</button>
                    <div class="m-t-20">
                      <p><label class="radio-checkbox label_check" for="checkbox-04"><input type="checkbox" id="checkbox-04" value="1">Automatically Populate Suggested Activities</label></p>
                    </div>
                  </div> -->
                <!-- </div> --> */ ?>
                <hr/>
                <div class="location-inner">
                  <div class="loc-icon">
                    <svg width="32px" height="34px" viewBox="0 0 32 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="45---EROA007-V4.1-Generic-(Create-Itinerary)-01" transform="translate(-16.000000, -1117.000000)">
                                <g id="(Generic)-Icon---Create-Itinerary" transform="translate(16.000000, 1119.000000)">
                                    <g id="(eRoam)-Icon---Create-Itinerary">
                                        <path d="M23.5359782,18.6938325 C22.4126947,18.6938325 21.4990421,17.7574227 21.4990421,16.6064237 C21.4990421,15.4554248 22.4126947,14.519015 23.5359782,14.519015 C24.6592616,14.519015 25.5733709,15.4554248 25.5733709,16.6064237 C25.5733709,17.7574227 24.6592616,18.6938325 23.5359782,18.6938325 M23.5359782,12.9260943 C21.5314687,12.9260943 19.9005498,14.5770428 19.9005498,16.6064237 C19.9005498,18.6358046 21.5314687,20.2867531 23.5359782,20.2867531 C25.540716,20.2867531 27.1718633,18.6358046 27.1718633,16.6064237 C27.1718633,14.5770428 25.540716,12.9260943 23.5359782,12.9260943 M23.5350647,27.6908759 C22.9178183,27.0081956 21.9425096,25.8703952 20.971083,24.4975251 C18.9140516,21.5904449 17.8268484,18.9841991 17.8268484,16.9605072 C17.8268484,11.2633127 22.1964415,10.8204808 23.5357498,10.8204808 C28.8329252,10.8204808 29.2446512,15.5200518 29.2446512,16.9605072 C29.2446512,20.9671578 25.1541092,25.8967921 23.5350647,27.6908759 M28.6621149,10.806372 C27.3504377,9.64171947 25.5297549,9 23.5357498,9 C21.5417447,9 19.7210619,9.64171947 18.4093847,10.806372 C16.8330429,12.2063217 16,14.3342362 16,16.9605072 C16,22.6809129 22.6061123,29.383923 22.8872186,29.6663251 C23.058714,29.8388156 23.2923222,29.9355287 23.5357498,29.9355287 C23.7791774,29.9355287 24.0127856,29.8388156 24.184281,29.6663251 C24.4653873,29.383923 31.0714996,22.6809129 31.0714996,16.9605072 C31.0714996,14.3342362 30.2384567,12.2063217 28.6621149,10.806372" id="location" fill="#212121"></path>
                                        <path d="M1,7.28286561 L6.73944561,3.39002187 L6.73944561,24.2205745 L1,28.1134182 L1,7.28286561 Z" id="Rectangle-2" stroke="#212121" stroke-width="2"></path>
                                        <path d="M7.19155649,3.24152097 L12.9310021,6.57824417 L12.9310021,27.5120097 L7.19155649,24.1752865 L7.19155649,3.24152097 Z" id="Rectangle-2" stroke="#212121" stroke-width="2"></path>
                                        <path d="M20.1225586,10.2571635 L20.1225586,0.753530649 L12.383113,6.00289694 L12.383113,29.2500905 L19.6514848,24.3202353 C18.2302235,22.0806375 17,19.4290796 17,16.9605072 C17,14.3342362 17.8330429,12.2063217 19.4093847,10.806372 C19.6328846,10.607924 19.8711628,10.4246586 20.1225586,10.2571635 Z" id="Combined-Shape" stroke="#212121" stroke-width="2"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                  </div>
                  <div class="location-content black-box">
                    <h4 class="loc-title">Create Itinerary</h4>
                    <button type="submit" class="btn btn-primary btn-block m-t-20" id="submit-search-btn">CREATE ITINERARY</button>
                  </div>
                </div>

              </div>
            </form>
          </div>
          <?php /*<div class="create-strip">
            <p class="map-icon"><a href="#">
              <svg width="21px" height="24px" viewBox="0 0 21 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                  <desc>Created with Sketch.</desc>
                  <defs></defs>
                  <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="(Generic)-Icon---Edit-Itinerary" transform="translate(-16.000000, -17.000000)" fill="#FFFFFF">
                          <g id="Icon---Edit-Itinerary-(eRoam)" transform="translate(16.000000, 17.000000)">
                              <path d="M16.8519039,12.1743021 L16.8519039,22.3618573 C16.8519039,22.9188258 16.3440134,23.3704013 15.7713079,23.3704013 L1.03704024,23.3704013 C0.464334766,23.3704013 0,22.9188258 0,22.3618573 L0,3.15464079 C0,2.59767236 0.464334766,2.19097699 1.03704024,2.19097699 L11.8155179,2.19097699 C12.3882234,2.19097699 12.8525582,2.64255257 12.8525582,3.199521 C12.8525582,3.75648944 12.3882234,4.20806502 11.8155179,4.20806502 L2.07408048,4.20806502 L2.07408048,21.3533133 L14.7778234,21.3533133 L14.7778234,12.1743021 C14.7778234,11.6173336 15.2421582,11.1657581 15.8148636,11.1657581 C16.3875691,11.1657581 16.8519039,11.6173336 16.8519039,12.1743021 L16.8519039,12.1743021 Z M8.91854604,13.0787139 L7.67306072,13.5592851 L8.17187707,12.3185239 L18.3607974,2.25981011 L19.1295035,3.01747881 L8.91854604,13.0787139 Z M20.7758049,2.48622825 L18.9140584,0.651182407 C18.7678357,0.507212749 18.5684647,0.426024955 18.3602789,0.426024955 L18.3600196,0.426024955 C18.1518338,0.426024955 17.9524628,0.507464885 17.8062401,0.651434543 L6.95153997,11.3674669 C6.87765086,11.4405863 6.8195766,11.5273211 6.78120611,11.6228806 L5.5733135,14.6283418 C5.46053537,14.908717 5.52949855,15.227417 5.74883256,15.4399676 C5.89764783,15.5836851 6.09572252,15.6600823 6.2976861,15.6600823 C6.39413085,15.6600823 6.49135337,15.6426849 6.58442773,15.6068816 L9.64421495,14.4263809 C9.74428933,14.3875519 9.83528962,14.3290564 9.91099355,14.2544241 L20.7758049,3.5489815 C21.0747317,3.25423452 21.0747317,2.7807231 20.7758049,2.48622825 L20.7758049,2.48622825 Z" id="document-edit"></path>
                          </g>
                      </g>
                  </g>
              </svg>
              <span class="map-icon-title">Edit</span>
            </a></p>
            <p class="map-icon"><a href="#">
              <svg width="24px" height="25px" viewBox="0 0 24 25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                  <desc>Created with Sketch.</desc>
                  <defs></defs>
                  <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="-(Generic)-Icon---Assistance" transform="translate(-12.000000, -17.000000)">
                          <g id="Icon---Assistance-(eRoam)" transform="translate(12.000000, 17.000000)">
                              <ellipse id="Oval" fill="#FFFFFF" cx="12" cy="12.0855615" rx="12" ry="12.0855615"></ellipse>
                              <text id="?" font-family="HelveticaNeue-Bold, Helvetica Neue" font-size="18" font-weight="bold" fill="#212121">
                                  <tspan x="7" y="20">?</tspan>
                              </text>
                          </g>
                      </g>
                  </g>
              </svg>
              <span class="map-icon-title">ASSISTANCE</span>
              </a></p>
            <a href="#" class="arrow-btn left_side_arrow_btn"><i class="fa fa-angle-left"></i></a>
          </div> */ ?>

          <div id="all-countries-container">
            {!! $options !!}
          </div>
  <div class="modal fade" id="country-city-container" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
          <button type="button" class="country-back" aria-label="Close"><i class="fa fa-arrow-circle-left fa-2x"></i></button>
          <h4 class="modal-title country-city-title ">Select Countries</h4>
        </div>
        <div class="modal-body country-city-container">

          <div class="clearfix"></div>
        </div>
        <!--    <div class="modal-footer">
                     <img src="{{ asset('images/eroam-global.png') }}" alt="" class="img-responsive">
                  </div> -->

      </div>
    </div>
  </div>


<?php 
    /*
    | Added by Rekha
    | Accomodation multi select options
    */
    $accommodation_name = ''; 
    $accommodation_options = '';
    $room_type_name = '';
    $room_type_options = '';
    $transport_type_name = '';
    $transport_type_options  = '';
    $nationality = '';
    $nationality_name = '';
    $age_group = '';
    $age_group_name = '';
    $gender = '';
    $gender_name = '';

    
    if ( count( $travellers['categories'] ) > 0 ) {
      foreach( $travellers['categories'] as $category ){
        if( empty( $category['name'] ) ){
          continue;
        }
        
        if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
          $accommodation_name .= '<span title="'.$category['name'].'" class="drop-selected">'.$category['name'].'</span>';
          //$accommodation_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="hotel_category" name="hotel_category_id[]" id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" checked> '.$category['name'] .' </label></li>';
          $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" selected="selected">'.$category['name'].'</option>';
        }else{
          //$accommodation_options .= '<li><label ><input type="radio" class="hotel_category" name="hotel_category_id[]" id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'"> '.$category['name'] .' </label></li>';
          $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'" >'.$category['name'].'</option>';
        }
      }
    }

    if( count( $travellers['room_types'] ) > 0 ){
      foreach( $travellers['room_types'] as $room_type){
        if( empty( $room_type['name'] ) ){
          continue;
        }
        if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
          $room_type_name .= '<span title="'.$room_type['name'].'" class="drop-selected">'.$room_type['name'].'</span>';
          //$room_type_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="room_type_options" name="room_type_id[]" value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" checked> '.$room_type['name'] .' </label></li>';
          $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" selected="selected"> '.$room_type['name'].'</option>';
        }else{
          //$room_type_options .= '<li><label ><input type="radio" class="room_type_options" name="room_type_id[]" value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'"> '.$room_type['name'] .' </label></li>';
          $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'" > '.$room_type['name'].'</option>';
        }
  
      }
    }

    if( count( $travellers['transport_types'] ) > 0 ){
      foreach( $travellers['transport_types'] as $transport_type){
        if( empty( $transport_type['name'] ) ){
            continue;
        }
        if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
          $transport_type_name .= '<span title="'.$transport_type['name'].'" class="drop-selected">'.$transport_type['name'].'</span>';
          //$transport_type_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="transport_type_options" name="transport_types[]" value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" checked> '.$transport_type['name'] .' </label></li>';
          $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" selected="selected">'.$transport_type['name'].'</option>';
        }else{
          //$transport_type_options .= '<li><label ><input type="radio" class="transport_type_options" name="transport_types[]" value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'"> '.$transport_type['name'] .' </label></li>';
          $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'">'.$transport_type['name'].'</option>';
        }
      }
    }

    if( count( $travellers['nationalities'] ) > 0 ){
      foreach( $travellers['nationalities']['featured'] as $featured ){
        if( empty( $featured['name'] ) ){
          continue;
        }
        if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) {
          $nationality_name .= '<span title="'.$featured['name'].'" class="drop-selected">'.$featured['name'].'</span>';
          //$nationality .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="nationality" name="nationality" value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" checked> '.$featured['name'] .' </label></li>';
          $nationality .= '<option value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" selected="selected">'.$featured['name'] .'</option>';
        }else{
          //$nationality .= '<li><label ><input type="radio" class="nationality" name="nationality" value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'"> '.$featured['name'] .' </label></li>';
          $nationality .= '<option value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'" >'.$featured['name'] .'</option>';
        }
      }

      foreach( $travellers['nationalities']['not_featured'] as $not_featured ){
        if( empty( $not_featured['name'] ) ){
          continue;
        }
        if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) ){
          $nationality_name .= '<span title="'.$not_featured['name'].'" class="drop-selected">'.$not_featured['name'].'</span>';
          //$nationality .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="nationality" name="nationality" value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" checked> '.$not_featured['name'] .' </label></li>';
          $nationality .= '<option value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" selected="selected"> '.$not_featured['name'] .'</option>';
        }else{
          //$nationality .= '<li><label ><input type="radio" class="nationality" name="nationality" value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .' </label></li>';
          $nationality .= '<option value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .'</option>';
        }
      }
    }

    $gender_array = ['male', 'female', 'other'];
    foreach($gender_array as $gen){
      if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) {
        $gender_name .= '<span title="'.$gen.'" class="drop-selected">'.ucfirst($gen).'</span>';
        //$gender .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="gender" name="gender" value="'.$gen.'" data-checked="1" data-name="'.$gen.'" checked> '.ucfirst($gen) .' </label></li>';
        $gender .= '<option value="'.$gen.'" data-checked="1" data-name="'.$gen.'" selected="selected"> '.ucfirst($gen) .'</option>';
      }else{
        //$gender .= '<li><label ><input type="radio" class="gender" name="gender" value="'.$gen.'" data-checked="0" data-name="'.$gen.'"> '.ucfirst($gen) .' </label></li>';
        $gender .= '<option value="'.$gen.'" data-checked="0" data-name="'.$gen.'" > '.ucfirst($gen) .'</option>';
      }
    }
  
    if( count( $travellers['age_groups'] ) > 0 ){
      foreach( $travellers['age_groups'] as $age){
        if( empty( $age['name'] ) ){
          continue;
        }
        if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) {
          $age_group_name .= '<span title="'.$age['name'].'" class="drop-selected">'.$age['name'].'</span>';
          //$age_group .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="age_group" name="age_group" value="'.$age['id'].'" data-checked="1" data-name="'.$age['name'].'" checked> '.$age['name'] .' </label></li>';
          $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" selected="selected" data-checked="1">'.$age['name'].'</option>';
        }else{
          //$age_group .= '<li><label ><input type="radio" class="age_group" name="age_group" value="'.$age['id'].'" data-checked="0" data-name="'.$age['name'].'"> '.$age['name'] .' </label></li>';
          $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" data-checked="0">'.$age['name'].'</option>';
        }
      }
    }
?>



  <!-- Modal -->
    <div class="modal fade" id="preferencesModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-icon">
              <svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                  <desc>Created with Sketch.</desc>
                  <defs></defs>
                  <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
                          <g id="Group-3" transform="translate(291.000000, 144.000000)">
                              <g id="Group-2">
                                  <path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
                              </g>
                          </g>
                      </g>
                  </g>
              </svg>
            </div>
            <div class="modal-container">
              <h4 class="modal-title" id="myModalLabel">Personal Profile Preferences</h4>
              <p>Please fill in the details below to personalise your travel preferences.</p>
            </div>
          </div>
          <div class="modal-body">
            <div class="modal-container">
              <form class="form-horizontal">
               <div class="row">
                 <div class="col-sm-6">
                   <div class="select-box">
                    <select class="form-control age_group" name="age_group" id="dropdown-age">
                      <option value="">Select Age Group</option>
                      {!! $age_group !!}
                    </select>
                  </div>
                 </div>
                 <div class="col-sm-6">
                   <div class="select-box">
                    <select class="form-control transport_type_options" name="transport_types[]" id="dropdown-transport">
                      <option>Select Transport Type</option>
                      {!! $transport_type_options !!}
                    </select>
                  </div>
                 </div>
               </div>

               <div class="row">
                 <div class="col-sm-6">
                   <div class="select-box">
                    <select class="form-control gender" name="gender" id="dropdown-gender">
                      <option>Select Gender</option>
                      {!! $gender !!}
                    </select>
                  </div>
                 </div>
                 <div class="col-sm-6">
                   <div class="select-box">
                    <select class="form-control hotel_category" name="hotel_category_id[]" id="dropdown-accommodation">
                      <option>Select Accommodation Type</option>
                      {!! $accommodation_options !!}
                    </select>
                  </div>
                 </div>
               </div>

               <div class="row">
                 <div class="col-sm-6">
                   <div class="select-box">
                    <select class="form-control nationality" name="nationality" id="dropdown-nationality">
                      <option>Select Nationality</option>
                      {!! $nationality !!}
                    </select>
                  </div>
                 </div>
                 <div class="col-sm-6">
                   <div class="select-box">
                    <select class="form-control room_type_options" name="room_type_id[]" id="dropdown-room">
                      <option>Select Room Type</option>
                      {!! $room_type_options !!}
                    </select>
                  </div>
                 </div>
               </div>

                <div class="m-t-20">
                 <h4 class="modal-title" id="myModalLabel">Holiday Preferences</h4>
                 <div class="row">
                   <div class="col-sm-6">
                     <label>Price Range Per Day</label>
                     <div class="layout-slider">
                      <span style="display: inline-block; width: 100%; padding: 0 5px;">
                      <input id="Slider3" type="slider" name="price2" value="100;10000" />
                      </span>
                    </div>
                   </div>
                   <div class="col-sm-6">
                     <label>Number of Total Days</label>
                    <div class="layout-slider">
                      <span style="display: inline-block; width: 100%; padding: 0 5px;">
                      <input id="Slider4" type="slider" name="price3" value="1;100" />
                      </span>
                    </div>
                   </div>
                 </div>
                </div>
              
              <div class="m-t-10">
                <h4 class="modal-title" id="myModalLabel">Activity Preferences</h4>
                <p>Please drag your preferred activities into the boxes below. <?php /*Highest priority begins on the left.*/ ?></p> 

                <div class="m-t-10">
                  <ul class="drop-list">

                    <?php 
                        $imgArr = array(
                          15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                          5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                          13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                          29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                          11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                          20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                           //'Expeditions','Astronomy','Food','Short-Breaks'
                          );

                        //echo '<pre>'; print_r($labels); echo '</pre>';
                        $ij = 0;
                        if( count($interest_ids) > 0 ){
                          foreach ($interest_ids as $j => $interest_id){
                            $key = array_search($interest_id, array_column($labels, 'id'));
                            $label = $labels[$key];
                            $ij++;
                    ?>
                              <li ondrop="drop(event)" ondragover="allowDrop(event)" class="interest-button-active" id="interest-{{ $label['id'] }}" data-sequence="{{ $ij }}" data-name="{{ $label['name'] }}" >
                                <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $j }} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                                <input type="hidden" id="inputValue_{{ $label['id'] }}" class="input-interest" name="interests[{{$j + 1}}]" value="{{ $label['id'] }}">
                              </li>
                    <?php  } } ?>

                    <?php 
                      for($k = $ij; $k < 8; $k++){
                        echo '<li ondrop="drop(event)" ondragover="allowDrop(event)" class="blankInterest" data-sequence="'.($k + 1).'"></li>';
                      }
                    ?>
                  </ul>
                </div>
                <div class="m-t-10">

                  <ul class="drag-list interest-lists first">
                    @if( count($labels) > 0 )
                      @foreach ($labels as $i => $label)

                        @if( empty($label['name']) )
                          @continue
                        @endif
                        <li ondrop="drop(event)" ondragover="allowDrop(event)" data-value="{{$label['id']}}" class="{{ in_array($label['id'], $interest_ids ) ? ' interestMove' : ''  }}" id="interest-{{ $label['id'] }}">
                          <?php
                            $namedis = 'block';
                            if(!in_array($label['id'], $interest_ids )){
                              $namedis = 'none';
                          ?>
                            <img src="<?php echo url( 'images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $i}} - {{ $label['name'] }}" data-value="{{$label['id']}}"  data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                            <input type="hidden" id="inputValue_{{ $label['id'] }}" class="input-interest" name="interests[]" value="">
                          <?php } ?>
                            <span style="display:{{ $namedis }};" id="name_{{ $label['id'] }}">{{ $label['name'] }}</span>
                            
                        </li>
                      @endforeach
                    @endif
                  </ul>

                </div>

                <div class="m-t-30 text-right">
                  <a href="#" data-dismiss="modal" class="m-r-10">DECLINE</a>
                  <a href="javascript://" id="save_travel_preferences">ACCEPT</a>
                </div>
              </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


    @stop
    @section( 'custom-js' )




<script>

  var activitySequence = [{{ join(', ', $interest_ids) }}];
  $(document).ready(function(){

    $('#scroll-box').slimScroll({
      height: '950px',
      color: '#212121',
      opacity: '0.7',
      size: '5px',
      allowPageScroll: true
    });

    $('#auto_multicity').click(function(){
       $('#manual_multicity').removeClass('active');
       $(this).addClass('active');
       $('#destination').show();
       $("#option1").val('auto');
    });
    $('#manual_multicity').click(function(){
         $('#auto_multicity').removeClass('active');
         $(this).addClass('active');
         $('#destination').hide();
         $("#option1").val('manual');
    });
    
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate()+1);
    var nextWeek  = new Date();
    nextWeek.setDate(nextWeek.getDate()+7);
    $("#startdate").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      startDate: tomorrow,
      todayHighlight: true
    });
       
    $("#Slider1").slider({ 
      from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "", skin: "round", limits: false 
      //from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "&nbsp;("+globalCurrency+")", skin: "round", limits: false 
    });

    $("#Slider2").slider({ 
      from: 1, to: 100, step: 1 , smooth: true, round: 0, dimension: "&nbsp;Days", skin: "round", limits: false
    });

    $( "#preferencesModal").on('shown.bs.modal', function(){
        $("#Slider3").slider({ 
          from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "", skin: "round", limits: false 
        });

        $("#Slider4").slider({ 
          from: 1, to: 100, step: 1 , smooth: true, round: 0, dimension: "&nbsp;Days", skin: "round", limits: false
        });
    }); 


    $('.left_side_arrow_btn').click(function(){
      if($(this).find('i').hasClass('fa-angle-left')){
        $(this).find('i').removeClass('fa-angle-left');
        $(this).find('i').addClass('fa-angle-right');
        $('.location-wrapper').hide();
      }else{
        $(this).find('i').removeClass('fa-angle-right');
        $(this).find('i').addClass('fa-angle-left');
        $('.location-wrapper').show();
      }
    });

    $('body').on('click', '.country-button', function(){
        $('.country-city-title').text('Select a Country');
        $('.country-city-container').html('<p class="text-center" style="padding: 20px"> <i class="fa blue-txt fa-circle-o-notch fa-spin fa-2x fa-fw"></i>  </p>');
        var selector = $(this).attr('data-value');
        var content = $('#all-countries-container').html();
        
        $('.country-city-container').html(content);
        $('.country-list').attr('data-selector', selector);
        $('#country-city-container').modal();
    });

    $('body').on('click', '.select-country', function(){
        var country = $(this).text();
        $('.country-city-title').text('Select a City From '+country);
        
        var selection_type = $(this).parent().attr('data-selector');

        var country_id = parseInt($(this).attr('data-value'));
        var starting_city_id = $('#starting_city').val();

        $('.country-city-container').html('<p class="text-center" style="padding: 20px"> <i class="fa blue-txt fa-circle-o-notch fa-spin fa-2x fa-fw" style="margin: 0 auto;"></i>  </p>');

        $('#'+selection_type+'_country').val(country_id);

        if( !isNaN(country_id) ){
          
          eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: country_id }, function( response ) {
            
            var city_options = '<ul class="city-list" data-selector="'+selection_type+'">';
            /*
            | Added by Junfel
            | Conver response to array of objects
            */
            var responseArray = Object.values(response);
            /*
            | Added by Junfel
            | Sort response (array of objects) by name
            */
            var sortedResponse = sortCities(responseArray);
            /*
            | End sorting by junfel
            */

            $.each( sortedResponse, function( key, value ) {
              if(value.id){
                /*
                | Added by Junfel
                | Filter City
                | City is selected as starting City then this city will not be displayed in the destination cities.
                */
                if( selection_type == 'destination' && value.id == starting_city_id ){
                  return;
                }else{
                  city_options += "<li class='city-select btn white-button new-border' data-country='" + country_id + "' data-value='"+value.id+"'>"+value.name+" </li>";
                }
              }
              
            });
            city_options += '</ul>';
            $('.country-city-container').html(city_options).hide().show('slide',{direction: 'right'});
            $('.country-back').show();

          });
        }
    });

    /*
    | append city values.
    */
    $('body').on('click', '.city-select', function(){

      var selection_type = $(this).parent().attr('data-selector');
      var city_id = $(this).attr('data-value');
      var city_text = $(this).text();
      var country = $( this ).data( 'country' );

      $('#'+selection_type+'_city').val(city_id);
      $('#'+selection_type).text(city_text);

      // ADDED BY RGR - CHECK IF MULTI-COUNTRY ITINERARY
      if ( selection_type == 'starting' ) {
        $( '#no-accomdation-to' ).text( city_text );
      }
      $('.country-back').hide();
      $('#country-city-container').modal('hide');
    });

    /*
    | Back to countries w
    */
    $('body').on('click', '.country-back', function(){
      $('.country-city-title').text('Select a Country');

      $('.country-city-container').html('<p class="text-center" style="padding: 20px"> <i class="fa blue-txt fa-circle-o-notch fa-spin fa-2x fa-fw"></i>  </p>');

      $('.coutnry-city-container').find('ul').removeClass('city-list').addClass('country-list');
      
      var content = $('#all-countries-container').html();
      $('.country-city-container').html(content).hide().show('slide', {direction: 'left'});
      $(this).hide();
    });
    // added by rgr
    
    $("#search-form").validate({
      ignore: [],
        rules: {
          start_date: {
            required: true
          },
          starting_city:{
            required: true
          },
          travellers:{
            required: true
          }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {

            form.submit();

        }
    });

    /*
    | Temporary saving for the travel pref
    */
    $('body').on('click', '#save_travel_preferences', function() {

      var btn = $(this).button('loading');

      var nationality_dom = $(".nationality option:selected").text();
      var age_group_dom = $(".age_group option:selected").text();

      var age_group_id = $('.age_group').val();
      var nationality_id = $('.nationality').val();
      var gender = $('.gender').val();

      var nationality = ( isNotUndefined( nationality_dom ) ) ? nationality_dom : [] ;
      var age_group = ( isNotUndefined( age_group_dom ) ) ? age_group_dom : [] ;
      var interestLists = [];
      var accommodations = [];
      var accommodationIds = [];
      var interestListIds = [];
      var roomTypeIds = [];
      var roomTypes = [];
      var transportTypeIds = [];
      var transportTypes = [];

      $('.hotel_category option:selected').each(function(){
        var id = parseInt($(this).val());
        var name = $(this).attr('data-name');
        accommodationIds.push(id);
        accommodations.push(name);
      });

      $('.room_type_options option:selected').each(function(){
        var id = parseInt($(this).val());
        var name = $(this).attr('data-name');
        roomTypeIds.push(id);
        roomTypes.push(name);
      });
      
      $('.transport_type_options option:selected').each(function(){
        var id = parseInt($(this).val());
        var name = $(this).attr('data-name');
        transportTypeIds.push(id);
        transportTypes.push(name);
      });
      
      $('.interest-button-active').each(function(){
        interestLists.push($(this).attr('data-name'));
      });

      var  data = { 
        travel_preference: [{
          accommodation:accommodationIds, 
          accommodation_name: accommodations,
          room_name: roomTypes,
          room: roomTypeIds,
          transport_name: transportTypes,
          transport: transportTypeIds,
          age_group: age_group,
          nationality: nationality,
          gender: gender,
          interestLists: interestLists.join(', '),
          interestListIds: activitySequence
          //interestListIds: interestListIds
        }]
      };
      

      var interestText = interestLists.length > 0 ? interestLists.join(', ') : '  All';
      var accommodation = accommodations.length > 0 ? accommodations.join(', ') : ' All';
      var transport = transportTypes.length > 0 ? transportTypes.join(', ') : ' All';
      var nationalityText = ( nationality ) ? nationality : ' All' ; 
      var ageGroupText = ( age_group ) ? age_group : '  All' ; 

      /*$('#_accommodation').html(' <strong> Accommodation: </strong> '+accommodation);
      $('#_transport').html(' <strong> Transport: </strong> '+transport);
      $('#_nationality').html(' <strong> Nationality: </strong> '+nationalityText);
      $('#_age').html(' <strong> Age: </strong> '+ageGroupText);
      $('#_interests').html(' <strong> Interests: </strong> '+ interestText);*/

      @if (session()->has('user_auth'))
        var post = {
          hotel_categories:accommodationIds, 
          hotel_room_types: roomTypeIds,
          transport_types: transportTypeIds,
          age_group: age_group_id,
          nationality: nationality_id,
          gender: gender,
          interests: activitySequence,
          _token: $('meta[name="csrf-token"]').attr('content'),
          user_id: "{{session()->get('user_auth')['user_id'] }}"
        };
        eroam.ajax('post', 'save/travel-preferences', post, function(response){
          //console.log(post);
        });
      @endif;

      eroam.ajax('post', 'session/travel-preferences', data, function(response){
        setTimeout(function() {
          btn.button('reset');

          eroam.ajax('get', 'session/updatePreferences', '', function(responsedata){
            $("#changePreferences").html(responsedata);
          });

          $('#preferencesModal').modal('hide');
        }, 3000);
      });

    });


  });

  function sortCities(object){
    var byName = object.slice(0);
    byName.sort(function(itemA, itemB) {
      var cityA = itemA.name.toLowerCase();
      var cityB = itemB.name.toLowerCase();
      return cityA < cityB ? -1 : cityA > cityB ? 1 : 0;
    });
    return byName
  }

  $(function() {
      google.maps.event.addDomListener(window, 'load', init);

      function init() {
          // Basic options for a simple Google Map
          // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
          var mapOptions = {
              // How zoomed in you want the map to start at (always required)
              zoom: 12,

              // The latitude and longitude to center the map (always required)
              center: new google.maps.LatLng('{{$latitude}}','{{$longitude}}'), // New York

              // How you would like to style the map.
              // This is where you would paste any style found on Snazzy Maps.
              styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
          };

          // Get the HTML DOM element that will contain your map
          // We are using a div with id="map" seen below in the <body>
          var mapElement = document.getElementById('map');

          // Create the Google Map using our element and options defined above
          var map = new google.maps.Map(mapElement, mapOptions);

          // Let's also add a marker while we're at it
          var marker = new google.maps.Marker({
              //position: new google.maps.LatLng(33.8688, 151.2093),
              position: new google.maps.LatLng('{{$latitude}}','{{$longitude}}'),
              map: map,
              icon:eroam.siteUrl + '/images/eroam-pin-blue.png'
          });
      }
  });
</script>

<script>
  function allowDrop(ev) {
      ev.preventDefault();
  }

  function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
      var data = ev.target.id;

      if($("#"+data).parent().parent().prop('className') == 'drop-list'){
        var interest = data.replace('drag_', 'interest-');
        $(".interestMove").css('opacity','0.5');
        $("#"+interest+".interestMove").css('opacity','');
      }
  }

  function drop(ev) { 
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      var name = data.replace('drag', 'name');
      var inputValue = data.replace('drag', 'inputValue');
      var interest = data.replace('drag_', 'interest-');
      var x = document.getElementById(name);
      var count = 0;
      var text = '';
      var data_value = document.getElementById(data).getAttribute("data-value");
      var data_name = document.getElementById(data).getAttribute("data-name");
      data_value = parseInt(data_value);
      var parentClass = $("#"+data).parent().parent().prop('className');
      var ClassName = $("#"+data).parent().prop('className');

      if(ClassName != "interest-button-active"){
        if($("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
            if (x.style.display === 'none') {
                x.style.display = 'block';
            } else {
                x.style.display = 'none';
            }
            
            var hidden_interest_field = $("#"+data).parent().find('.input-interest');
            if( hidden_interest_field.val() ){
              var interestIndex = activitySequence.indexOf(data_value);
              activitySequence.splice(interestIndex, 1);
              hidden_interest_field.val('');
              hidden_interest_field.attr('name', 'interests[]');
            } else {
              activitySequence.push(data_value);
              var interestIndex = activitySequence.indexOf(data_value);
              hidden_interest_field.val(data_value);
              hidden_interest_field.attr('name', 'interests['+(interestIndex + 1)+']');
            } 

            ev.target.appendChild(document.getElementById(data));
            ev.target.appendChild(document.getElementById(inputValue));

            var parentDiv = $("#"+data).parent();
            parentDiv.attr('id', interest);    
            $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
        }

      } else {
        if(ev.target.getAttribute("class") == 'blankInterest' && $("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
  
            var interestIndex = activitySequence.indexOf(data_value);
            activitySequence.splice(interestIndex, 1);
            activitySequence.push(data_value);

            var hidden_interest_field = $("#"+data).parent().find('.input-interest');
            hidden_interest_field.attr('name', 'interests['+ev.target.getAttribute("data-sequence")+']');  

            ev.target.appendChild(document.getElementById(data));
            ev.target.appendChild(document.getElementById(inputValue));
            $("#"+interest).removeClass('interest-button-active').addClass('blankInterest').removeAttr('data-name');
            $("#"+interest).removeAttr('id');
            $("#"+data).parent().attr('id',interest).attr('data-name',data_name);
            $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
        } else {
          if(parseInt(ev.target.getAttribute("data-value")) == data_value){
          
            var interestIndex = activitySequence.indexOf(data_value);
            activitySequence.splice(interestIndex, 1);

            var hidden_interest_field = $("#"+data).parent().find('.input-interest');
            hidden_interest_field.val('');
            hidden_interest_field.attr('name', 'interests[]');  
            $(".interestMove").css('opacity','');

            ev.target.appendChild(document.getElementById(data));
            ev.target.appendChild(document.getElementById(inputValue));

            $("#"+interest).removeClass('interest-button-active').addClass('blankInterest');
            $("#"+interest).removeAttr('id');
            x.style.display = 'none';
          } 
        }
      } 
  }
</script>
@stop