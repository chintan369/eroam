@extends('layouts.common')
@section('content')
<div class="body_sec">
    <div class="blue modal_outer_man">
        @include('partials.banner')
        @include('partials.search')
    </div>
    <div class="account-block">
        @include('users.partials.sidebar')
        <div class="account-right p-4">
            <h5><i class="ic-explore"></i> View Detailed Itinerary</h5>
            <div class="text-right d-block d-lg-none"><a href="javascript://" class="navbtn menu-btn text-secondary"><i class="ic-menu"></i></a></div>
            <div class="row mt-4">
                <div class="form-group col-md-3 col-sm-6">
                    <div class="fildes_outer">
                        <label>Check-in Date:</label>
                        <input id="checkin-date" type="text" placeholder="{{date('d M Y',strtotime($trip_details['itinerary_order']['from_date']))}}" class="form-control disabled valid" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                        <span class="arrow_down"><i class="ic-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <div class="fildes_outer">
                        <label>Check-out Date:</label>
                        <input id="checkout-date" type="text" placeholder="{{date('d M Y',strtotime($trip_details['itinerary_order']['to_date']))}}" class="form-control disabled valid" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                        <span class="arrow_down"><i class="ic-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Rooms</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="rooms" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                                <option value="1">1</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Adults (18+)</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="num_of_adults" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" data-original-title="Not Available in Pilot">
                                <option value="{{$trip_details['itinerary_order']['num_of_travellers']}}">{{$trip_details['itinerary_order']['num_of_travellers']}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Children (0-17)</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="num_of_children[]" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                                <option value="0">No Children</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 mb-4 row">
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="#" name="" class="btn btn-white btn-block active mb-3 mb-sm-0">VIEW ITINERARY</a>
                </div>
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="#" class="btn btn-white btn-block mb-3 mb-sm-0">PRINT / SHARE ITINERARY</a>
                </div>
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="#" name="" class="btn btn-white btn-block mb-3 mb-sm-0">BOOK ITINERARY</a>
                </div>
            </div>
            <hr>
            <div class="profile-steps payment-steps mt-3">
                <ul class="nav nav-tabs nav-fill border-0">
                    @foreach ($trip_details['itinerary_leg'] as $leg)
                    <li class="nav-item">
                        <a href="javascript://" class="nav-link">
                            <span class="circle d-flex align-items-center justify-content-center">{{$leg['nights']}}</span>
                            <span class="d-block"><strong>{{$leg['from_city_name']}}</strong><br>{{$leg['country_name']}}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="locations_map pt-3 pt-5 pb-5 ">
                <div id="map1" style="height: 400px;"></div>
                <input type="hidden" id="map-data" value="{{json_encode($route)}}">
                <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
            </div>
            <section class="tours-wrapper">
                @foreach ($trip_details['itinerary_leg'] as $key => $leg)
                <div class="tripDetails mb-3">
                    <div class="card panel border-0 p-3">
                        <div class="row">
                            <div class="col-sm-7 col-xl-8">
                                <h5 class="font-weight-bold">{{$leg['from_city_name']}}, {{$leg['country_name']}}</h5>
                                <div class="media">
                                    <i class="ic-calendar mr-2"></i>
                                    <div class="media-body pb-3 mb-0">
                                        {{date('d M Y',strtotime($leg['from_date']))}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-xl-4">
                                <div class="row align-items-end">
                                    <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                        <ul class="list-unstyled mb-0">
                                            @if($leg['hasHotel'])
                                            <li class="list-inline-item"><i class="ic-local_hotel fa-lg"></i></li>
                                            @endif
                                            @if($leg['hasActivities'])
                                            <li class="list-inline-item"><i class="ic-local_activity fa-lg"></i></li>
                                            @endif
                                            @if($leg['hasTransport'])
                                            <li class="list-inline-item"><i class="ic-flight fa-lg"></i></li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                        <h2 class="font-weight-bold mb-0">{{$leg['nights']}}</h2>
                                        Nights
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="clearfix">
                            <div class="accomodation-img">
                                <img src="{{$leg['city_image']}}" alt="" class="img-fluid" />
                            </div>
                            <div class="accomodation-details lh-condensed">
                                <p>{!!$leg['city_description']!!}</p>
                                <p class="mt-5"><a href="javascript://" class="text-dark trip-viewmore" data-id="trip1{{$key}}"><i class="ic-expand_more mr-1"></i> Click to view detailed itinerary</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="tripDetails-wrapper" id="trip1{{$key}}">
                        @if($leg['hasHotel'])
                        <div class="tripDetails-container mb-3">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-local_hotel fa-lg"></i></span> <strong>ACCOMMODATION</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <a href="#" data-toggle="modal" data-target="#editTripModal"><i class="fa fa-pencil"></i></a>
                                        <a href="#"><i class="fa fa-ban"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Booking#</th>
                                                <th>Location</th>
                                                <th>Check In</th>
                                                <th>Check out</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($leg['leg_detail'] as $legDetail)
                                            @if($legDetail['leg_type'] == 'hotel')
                                            @if($legDetail['own_arrangement'] == 1)
                                            <tr class="tourTripName">
                                                <td colspan="6">Own Arrangement</td>
                                            </tr>
                                            @else
                                            <tr class="tourTripName">
                                                <td>{{date('d/m/y',strtotime($legDetail['from_date']))}}</td>
                                                <td>{{$legDetail['leg_name']}}</td>
                                                <td>
                                                @if($legDetail['booking_id'] == null)
                                                    {{'Pending'}}
                                                @else
                                                    {{$legDetail['booking_id']}}
                                                @endif
                                                </td>
                                                <td>{{$legDetail['address']}}</td>
                                                <td>{{date('d/m/y @ H:m',strtotime($legDetail['from_date']))}}</td>
                                                <td>{{date('d/m/y @ H:m',strtotime($legDetail['to_date']))}}</td>
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($leg['hasActivities'])
                        <div class="tripDetails-container mb-3">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-local_activity"></i></span> <strong>TOURS / ACTIVITIES</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <a href="#" class="text-dark"><i class="ic-create"></i></a>
                                        <a href="#" class="text-dark"><i class="ic-block"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Booking#</th>
                                                <th>Location</th>
                                                <th>Start Time</th>
                                                <th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($leg['leg_detail'] as $legDetail)
                                            @if($legDetail['leg_type'] == 'activities')
                                            @if($legDetail['own_arrangement'] == 1)
                                            <tr class="tourTripName">
                                                <td colspan="6">Own Arrangement</td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td>{{date('d/m/y',strtotime($legDetail['from_date']))}}</td>
                                                <td>{{$legDetail['leg_name']}}</td>
                                                <td>
                                                @if($legDetail['booking_id'] == null)
                                                    {{'Pending'}}
                                                @else
                                                    {{$legDetail['booking_id']}}
                                                @endif
                                                </td>
                                                <td>{{$legDetail['address']}}</td>
                                                <td>{{$legDetail['activity_start_time']}}</td>
                                                <td>{{$legDetail['duration']}} Hours</td>
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($leg['hasTransport'])
                        <div class="tripDetails-container">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-directions_bus"></i></span> <strong>TRANSPORT</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <a href="#" class="text-dark"><i class="ic-create"></i></a>
                                        <a href="#" class="text-dark"><i class="ic-block"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Booking#</th>
                                                <th>Departure Time</th>
                                                <th>Arrival / Time</th>
                                                <th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($leg['leg_detail'] as $legDetail)
                                            @if($legDetail['leg_type'] == 'transport')
                                            @if($legDetail['own_arrangement'] == 1)
                                            <tr class="tourTripName">
                                                <td colspan="6">Own Arrangement</td>
                                            </tr>
                                            @else
                                            <tr class="tourTripName">
                                                <td>{{date('d/m/y',strtotime($legDetail['transport_date']))}}</td>
                                                <td>{{$legDetail['leg_name']}}</td>
                                                <td>
                                                @if($legDetail['booking_id'] == null)
                                                    {{'Pending'}}
                                                @else
                                                    {{$legDetail['booking_id']}}
                                                @endif
                                                </td>
                                                <td>{{$legDetail['departure_text']}}</td>
                                                <td>{{$legDetail['arrival_text']}}</td>
                                                <td>{{preg_replace('/[^\p{L}\p{N}\s]/u', '', $legDetail['duration'])}}</td>
                                            </tr>
                                            @endif
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </section>
        </div>
    </div>
</div>
@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
    <script type="text/javascript" src="{{ url('js/draggable-0.1.js') }}"></script>
    <script src="{{url('js/eroam-map_managetrip.js')}}"></script>
    <script src="{{url('js/markerlabel.js') }}"></script>
    <script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    function setupLabel() {
        if ($('.label_radio input').length) {
            $('.label_radio').each(function(){ 
                $(this).removeClass('r_on');
            });
            $('.label_radio input:checked').each(function(){ 
                $(this).parent('label').addClass('r_on');
                var inputValue = $(this).attr("value");
                //alert(inputValue);
                $("." + inputValue).addClass('mode-block');
                $("." + inputValue).siblings().removeClass('mode-block');
            });
        };

        if ($('.label_check input').length) {
            $('.label_check').each(function(){ 
                $(this).removeClass('c_on');
            });
            $('.label_check input:checked').each(function(){ 
                $(this).parent('label').addClass('c_on');
            });                
        };
    };
    $( window ).load( eMap.init );

    $(document).ready(function(){
        setupLabel(); 
        $('.label_check, .label_radio').click(function(){
            setupLabel();
        });
        tripHideShow();
    });

    function jcustomSlider(){
        (function($) {
            $(function() {
                var jcarousel = $('.jcarousel');
                jcarousel.on('jcarousel:reload jcarousel:create', function () {
                    var carousel = $(this),
                        width = carousel.innerWidth();
                    if (width >= 1280) {
                        width = width / 4;
                    } else if (width >= 992) {
                        width = width / 4;
                    }
                    else if (width >= 768) {
                        width = width / 4;
                    }
                    else if (width >= 640) {
                        width = width / 4;
                    }
                    carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                }) .jcarousel({wrap: 'circular'});

                $('.jcarousel-control-prev').jcarouselControl({
                    target: '-=1'
                });

                $('.jcarousel-control-next').jcarouselControl({
                    target: '+=1'
                });

                $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
                    $(this).addClass('active');
                }).on('jcarouselpagination:inactive', 'a', function() {
                    $(this).removeClass('active');
                }).on('click', function(e) {
                    e.preventDefault();
                }).jcarouselPagination({
                    perPage: 1,
                    item: function(page) {
                        return '<a href="#' + page + '">' + page + '</a>';
                    }
                });
            });
        })(jQuery);
    }

    function map(){
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York
                styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
            };

            var mapElement = document.getElementById('map1');

            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map,
                title: 'eRoam!'
            });
        }
    }

    function tripHideShow(){  
        $('.trip-viewmore').click(function(){
            var href_value = $(this).attr('data-id');
            $('#' + href_value).toggle();
            $('#' + href_value).toggleClass("open");
            $(this).toggleClass("active");
        });
    }

    </script>
@endpush
@endsection