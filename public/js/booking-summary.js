
/**
 * Booking summary updater script
 */

var bookingSummary = ( function(  ) {

	function update( searchSession, initialLoad = false, callback ) {

		var url = ( initialLoad ) ? 'update-booking-summary' : 'save-booking';

		eroam.ajax( 'post',url, { search: searchSession }, function( response ) {

			setTimeout( function() {
				
				$( '.booking-summary' ).html( response );
				customReady();
				var page = $('#itinerary-page').val();

				if (page == 'hotels' || page == 'activities' || page == 'transports') {
					var leg = $('#itinerary-leg').val();
					$("#tinerary-panel-leg-"+leg).children('.collapse_click').click();
				}
				if (callback) {
					callback();
				}
			}, 200 );
		}, function() {

				$( '.booking-summary' ).html( '<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>' );	

		} );
	}

	function replaceOwnArrangementsWithApis( searchSession ){
		
	}

	function showMiniLoader(element) {
		element.html(
			'<div class="la-ball-pulse" style="color: #2AA9DF">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
			'</div>'
		);
		element.closest('.itinerary-leg-link').attr('data-loading', 'yes').css('cursor', 'wait');
	}

	function hideMiniLoader(element) {
		// console.log( "hide element", element);
		element.closest('.itinerary-leg-link').attr('data-loading', '').css('cursor', 'pointer');
	}

	function tempUpdate( searchSession, key , position ) {
		var leg = key;
		var pos = position;
		var url = 'save-booking';

		eroam.ajax( 'post',url, { search: searchSession }, function( response ) {

			setTimeout( function() {
				$( '.booking-summary' ).html( response );

				
				var page = $('#itinerary-page').val();
				if(!page){
					page = 'map';
				}
			

				$('.itinerary-leg-container .panel-heading a').addClass('collapsed');
				$('.itinerary-leg-container .panel-heading#heading-'+leg+' a').removeClass('collapsed');

				// OPEN BODY
				$('.itinerary-leg-container .panel-collapse').removeClass('in');
				$('.itinerary-leg-container #collapse-'+ leg).addClass('in');

				// SCROLL ON SELECTED
			
				$('.itinerary-container').scrollTop(pos);

				$('.itinerary-leg-container[data-index="'+leg+'"] .itinerary-leg-link[data-type="'+page+'"]').addClass('active');
				
			}, 200 );

		} );
	}

	return {
		tempUpdate : tempUpdate,
		update : update,
		showMiniLoader : showMiniLoader,
		hideMiniLoader : hideMiniLoader,
	};

} )( jQuery );

$( function() {
	bookingSummary.update();
} );


