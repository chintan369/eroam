<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Session;

class SocialAuthController extends Controller
{
	private $headers = [];

	public function __construct() {
		$this->headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];
		
	}

    public function redirect($service)
    {
        return Socialite::driver($service)->redirect();   
    }   

    public function callback($service)
    {
        $user = Socialite::with( $service )->user();
		
		if(!empty($user))
		{
			$data = array(
				'first_name' 			=> $user->user['name']['givenName'],
				'last_name'  			=> $user->user['name']['familyName'],
				'social' 				=> $service,	
				'email'		 			=> $user->email,
				'social_id'				=> $user->id,
				'image_path' 			=> $user->avatar_original
			);

			$check_user = http( 'post', 'user/check-customer', $data, $this->headers);
			if( $check_user['successful'] == 1 )
		    {
		    	if( isset($check_user['code']) )
		    	{
		    		$user_code = $check_user['code'];
					session()->put('user_auth', $user_code);
					return redirect('profile/step1'); 
		    	}

		        $response_api = http( 'post', 'user/create_customer', $data, $this->headers);
		        if( $response_api['successful'] )
		        {        			        	
					$user_code = $response_api['code'];
					session()->put('user_auth', $user_code);
					return redirect('profile/step1')
		                    ->with('profile_step1_success', 'Thank you for registration!'); 
		        }
		        else
		        {
		        	Session::flash('register_confirm_fail', 'Something went wrong!'); 
		            return redirect('/')
                    ->with('register_confirm_fail', 'Something went wrong!');
		        }	
	        }
	        else
	        {
	        	Session::flash('register_confirm_fail', 'Email address already exist!'); 
	        	return redirect('/')
                    ->with('register_confirm_fail', 'Email address already exist!');
	        }
	    }
    }
}